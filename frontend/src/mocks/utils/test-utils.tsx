import React, { PropsWithChildren } from "react";
import { render } from "@testing-library/react";
import type { RenderOptions } from "@testing-library/react";
import { PreloadedState } from "@reduxjs/toolkit";
import { setupStore } from "@redux_store";
import type { AppStore, RootState } from "@redux_store";
import { Provider } from "react-redux";
import { MemoryRouter, BrowserRouter } from "react-router-dom";

interface ExtendedRenderOptions extends Omit<RenderOptions, "queries"> {
    preloadedState?: PreloadedState<RootState>;
    store?: AppStore;
}

export function renderWithProviders(
    ui: React.ReactElement,
    {
        preloadedState = {},
        // Automatically create a store instance if no store was passed in
        store = setupStore(preloadedState),
        ...renderOptions
    }: ExtendedRenderOptions = {}
): any {
    function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
        return <Provider store={store}>{children}</Provider>;
    }
    return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}

export const renderWithRouterAndRedux = (
    component: React.ReactElement,
    { preloadState = {}, store = setupStore(preloadState), route = "/", ...renderOptions } = {},
) => {
    window.history.pushState({}, "Initial Page", route);

    const Wrapper: any = ({ children } : {children: any}) => (
        <Provider store={store}>
            <BrowserRouter>{children}</BrowserRouter>
        </Provider>
    );
    return render(component, { wrapper: Wrapper, ...renderOptions });
};