import { useAppSelector, useAppDispatch } from "@hooks/Redux/hooks";
import { Teacher } from "@redux_features/Class/TeacherAPI/ClassTeacherAPI";
import { classTeacherActions, classTeacherSelectors } from "../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";
import TeacherList from "../TeacherList";

const TeacherCandidateList = () => {
    const dispatch = useAppDispatch();

    const transientTeachers = useAppSelector(
        classTeacherSelectors.selectTeacherCandidates
    );

    const teachers = Object.values(transientTeachers);

    if (!transientTeachers || teachers.length < 1) {
        return <span> Không có cán bộ nào</span>;
    }

    return (
        <TeacherList
            listData={teachers as Teacher[]}
            onItemClick={(staffId) => { 
                dispatch(classTeacherActions.addNewCandidateToTransient(staffId)); 
            }}
        />
    );
};

export default TeacherCandidateList;
