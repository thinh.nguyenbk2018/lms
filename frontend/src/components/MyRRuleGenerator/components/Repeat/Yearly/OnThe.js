import React from 'react';
import PropTypes from 'prop-types';

import {MONTHS, DAYS} from '../../../constants/index';
import translateLabel from '../../../utils/translateLabel';
import {Col, Input, Row, Select, Form, Typography, Space} from "antd";
import {isNaN} from "lodash";

const RepeatYearlyOnThe = ({
                               id,
                               mode,
                               onThe,
                               hasMoreModes,
                               handleChange,
                               translations
                           }) => {
    const isActive = mode === 'on the';

    return (
        <Row gutter={[5,10]} className={`form-group row d-flex align-items-sm-center ${!isActive && 'opacity-50'}`}>
            <Col span={24}>
                {hasMoreModes && (
                    <Space>
                        <Typography.Text>
                            {translateLabel(translations, 'repeat.yearly.on_the')}
                        </Typography.Text>
                        <Input
                            id={id}
                            type="radio"
                            aria-label="Repeat yearly on the"
                            name="repeat.yearly.mode"
                            checked={isActive}
                            value="on the"
                            onChange={handleChange}
                        />
                    </Space>
                )}
            </Col>
            <Col>
                <Select
                    id={`${id}-which`}
                    name="repeat.yearly.onThe.which"
                    aria-label="Repeat yearly on the which"
                    className="form-control"
                    value={onThe.which}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        const target = {name: "repeat.yearly.onThe.which", value};
                        handleChange({target})
                    }}
                >
                    <Select.Option value="First">{translateLabel(translations, 'numerals.first')}</Select.Option>
                    <Select.Option value="Second">{translateLabel(translations, 'numerals.second')}</Select.Option>
                    <Select.Option value="Third">{translateLabel(translations, 'numerals.third')}</Select.Option>
                    <Select.Option value="Fourth">{translateLabel(translations, 'numerals.fourth')}</Select.Option>
                    <Select.Option value="Last">{translateLabel(translations, 'numerals.last')}</Select.Option>
                </Select>
            </Col>

            <Col>
                <Select
                    id={`${id}-day`}
                    name="repeat.yearly.onThe.day"
                    aria-label="Repeat yearly on the day"
                    className="form-control"
                    value={onThe.day}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        const target = {name: "repeat.yearly.onThe.day", value};
                        handleChange({target})
                    }}
                >
                    {DAYS.map(day => <Select.Option key={day}
                                                    value={day}>{translateLabel(translations, `days.${day.toLowerCase()}`)}</Select.Option>)}
                </Select>
            </Col>

            <Col span={24}>
                {translateLabel(translations, 'repeat.yearly.of')}
            </Col>

            <Col>
                <Select
                    id={`${id}-month`}
                    name="repeat.yearly.onThe.month"
                    aria-label="Repeat yearly on the month"
                    className="form-control"
                    value={onThe.month}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        const target = {name: "repeat.yearly.onThe.day", value};
                        handleChange({target})
                    }}
                >
                    {MONTHS.map(month => <Select.Option key={month}
                                                        value={month}>{translateLabel(translations, `months.${month.toLowerCase()}`)}</Select.Option>)}
                </Select>
            </Col>

        </Row>
    );
};
RepeatYearlyOnThe.propTypes = {
    id: PropTypes.string.isRequired,
    mode: PropTypes.oneOf(['on', 'on the']).isRequired,
    onThe: PropTypes.shape({
        which: PropTypes.oneOf(['First', 'Second', 'Third', 'Fourth', 'Last']).isRequired,
        month: PropTypes.oneOf(MONTHS).isRequired,
        day: PropTypes.oneOf(DAYS).isRequired,
    }).isRequired,
    hasMoreModes: PropTypes.bool.isRequired,
    handleChange: PropTypes.func.isRequired,
    translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default RepeatYearlyOnThe;
