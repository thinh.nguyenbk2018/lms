import {Modal} from "antd";
import {Field, Formik} from "formik";
import * as Yup from "yup";
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {FORM_DEFAULT_MESSAGES} from "@utils/FormUtils/formUtils";
import {
    classDiscussionActions,
    classDiscussionSelectors,
} from "../../../redux/features/Class/ClassDiscussion/ClassDiscussionSlice";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {useParams} from "react-router-dom";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import {Comment, useUpdateCommentMutation,} from "../../../redux/features/Class/ClassDiscussion/ClassDiscussionAPI";

export const commentSchema = Yup.object({
    content: Yup.string().required(FORM_DEFAULT_MESSAGES.REQUIRED),
});

export const initalCommentValues: Partial<Comment> = {
    content: "",
};
const EditCommentModal = () => {
    const {classId} = useParams();

    const isEditingComment = useAppSelector(
        classDiscussionSelectors.selectIsEditingComment
    );
    const dispatch = useAppDispatch();

    const [updateComment] = useUpdateCommentMutation();

    const commentInEdit = useAppSelector(
        classDiscussionSelectors.selectCommentInEdit
    );
    const postOfCommentInEdit = useAppSelector(
        classDiscussionSelectors.selectPostOFCommentInEdit
    );

    if (isEditingComment && !commentInEdit) {
        AntdNotifier.error("Đã có lỗi xaỷ ra. Liên hệ dev team");
        return <></>;
    } else {
        return (
            <>
                <Formik
                    validateOnChange={false}
                    enableReinitialize={true}
                    initialValues={
                        isEditingComment ? commentInEdit ?? {} : initalCommentValues
                    }
                    validationSchema={commentSchema}
                    onSubmit={(values) => {
                        if (isEditingComment) {
                            if (!commentInEdit?.id || !postOfCommentInEdit) {
                                AntdNotifier.error("Không tìm thấy post để chỉnh sửa");
                                return;
                            }
                            AntdNotifier.handlePromise(
                                updateComment({
                                    classId: Number(classId),
                                    postId: postOfCommentInEdit,
                                    commentId: commentInEdit.id,
                                    commentContent: values.content || "",
                                }).unwrap(),
                                "Chỉnh sửa bài đăng",
                                () => {
                                    dispatch(classDiscussionActions.doneEditComment());
                                },
                                () => {
                                    dispatch(classDiscussionActions.doneEditComment());
                                }
                            );
                        }
                    }}
                >
                    {({values, handleSubmit, errors, isValid, dirty}) => (
                        <Modal
                            maskClosable={false}
                            visible={isEditingComment}
                            onOk={() => {
                                handleSubmit();
                            }}
                            onCancel={() => {
                                dispatch(classDiscussionActions.doneEditComment());
                            }}
                        >
                            <Field
                                name={"content"}
                                component={TextEditorField}
                                label={"Nội dung bình luận"}
                            />
                        </Modal>
                    )}
                </Formik>
            </>
        );
    }
};

export default EditCommentModal;
