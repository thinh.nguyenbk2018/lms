import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "../../store/store";

const COURSE_QUIZ_BANK_SLICE_KEY = "courseQuizBank";

const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice()
}

export const courseQuizBankSlice = createSliceEnhancedWithSearch({
    name: COURSE_QUIZ_BANK_SLICE_KEY,
    initialState,
    reducers: {},
});

export const courseQuizBankSelector = {
    selectSearchCriteria: (state: RootState) => state.courseQuizBank.searchCriteria
}

export const {changeCriteria} = courseQuizBankSlice.actions;