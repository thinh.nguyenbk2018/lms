import {useAppDispatch, useAppSelector} from "../Redux/hooks";
import {
	createTestContentActions,
	createTestContentSelectors,
	QuestionDataType,
	QuestionType,
} from "../../redux/features/Quiz/CreateQuizSlice";
import {
	useAddQuestionToQuestionGroupMutation,
	useAddQuestionToTestMutation,
	useGetQuestionQuery,
	useUpdateQuestionInTestMutation,
} from "../../redux/features/Quiz/examAPI";
import {useParams} from "react-router-dom";
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import _ from "lodash";
import {errorArrayHandlingHelper,} from "../../redux/features/CentralAPI";
import API_ERRORS from "../../redux/features/ApiErrors";

const questionUpdateCreateErrorHanlder = (err: any) => {
	const { EXAM_NOT_FOUND, UNSUPPORTED_QUESTION_TYPE } = API_ERRORS.EXAM;
	errorArrayHandlingHelper(
		err,
		[EXAM_NOT_FOUND, "Không tìm thấy bài kiểm tra"],
		[
			UNSUPPORTED_QUESTION_TYPE,
			"Lỗi khi server kiểm tra loại câu hỏi: Không tìm thấy / chưa hỗ trợ loại câu hỏi này",
		]
	);
};

const useQuestionDrawerType = (
	questionType: QuestionType,
	onSuccess?: () => void,
	onFailure?: () => void
) => {
	const dispatch = useAppDispatch();
	const { courseId, testId } = useParams();

	const visible = useAppSelector((state) =>
		createTestContentSelectors.selectIsQuestionDrawerTypeOpening(
			state,
			questionType
		)
	);
	const questionIdInDraft = useAppSelector(
		createTestContentSelectors.selectQuestionIdInDraft
	);
	const isEditing = useAppSelector((state) =>
		createTestContentSelectors.selectIsDrawerEditing(state, questionType)
	);

	const isEditingGroupQuestion = useAppSelector(
		createTestContentSelectors.selectIsEditingQuestionGroup
	);
	const questionGroupIdInEdit = useAppSelector(
		createTestContentSelectors.selectQuestionGroupIdInDraft
	);
	const isEditingQuestionInGroup = useAppSelector(
		createTestContentSelectors.selectIsEditingQuestionInGroup
	);
	const isCreatingGroupQuestion = useAppSelector(
		createTestContentSelectors.selectIsAddingQuestionGroup
	);

	const visibleGroupQuestion = isEditingGroupQuestion || isCreatingGroupQuestion;

	const {
		data: questionDataInDraft,
		isSuccess,
		isError,
		isFetching,
	} = useGetQuestionQuery(
		{ testContentId: Number(testId), questionId: questionIdInDraft },
		{
			skip: !isEditing,
		}
	);

	const testName = useAppSelector(
		createTestContentSelectors.selectCurrentTestName
	);

	const [updateTrigger] = useUpdateQuestionInTestMutation();
	const [createTrigger] = useAddQuestionToTestMutation();

	// * group mutations
	const [addQuestionToGroup] = useAddQuestionToQuestionGroupMutation();

	const closeDrawer = () => {
		if (isEditing) {
			dispatch(createTestContentActions.doneDraftingQuestion());
		}
		dispatch(createTestContentActions.setOpeningDrawerType("NONE"));
		dispatch(createTestContentActions.doneDraftingQuestionGroup());
	};

	const doneHandler = (question: Partial<QuestionDataType>, afterDone? : () => void) => {
		// console.log({question});
		if (isEditingQuestionInGroup) {
			if (!questionGroupIdInEdit || isNaN(questionGroupIdInEdit)) {
				AntdNotifier.error(
					"Bạn không thể chỉnh sửa nhóm câu hoỉ này. Không tìm thấy định danh câu hỏi"
				);
				return;
			}
			let questionToAdd = {
				courseId: Number(courseId),
				testId: Number(testId),
				groupId: questionGroupIdInEdit,
				...question,
			};
			delete questionToAdd.id;
			AntdNotifier.handlePromise(addQuestionToGroup(questionToAdd).unwrap(),
				"Thêm câu hỏi vào nhóm",
				() => {closeDrawer(); afterDone?.();},
				(err) => {
					questionUpdateCreateErrorHanlder(err);
				}
			);
		} else if (isEditing) {
			updateTrigger({
				testId: Number(testId),
				...question,
			})
				.unwrap()
				.then((res) => {
					AntdNotifier.success("Chỉnh sửa câu hỏi thành công");
					onSuccess?.();
					closeDrawer();
					afterDone?.();
				})
				.catch((err) => {
					AntdNotifier.success("Chỉnh sửa câu hỏi thất bại");
					questionUpdateCreateErrorHanlder(err);
					onFailure?.();
				});
		} else {
			const valueWithoutId = _.omit(question, ["id"]);

			createTrigger({
				testId: Number(testId),
				courseId: Number(courseId),
				...valueWithoutId,
			})
				.unwrap()
				.then((res) => {
					AntdNotifier.success("Thêm câu hỏi thành công");
					onSuccess?.();
					closeDrawer();
					afterDone?.();
				})
				.catch((err) => {
					AntdNotifier.error("Thêm câu hỏi thất bại!");
					questionUpdateCreateErrorHanlder(err);
					onFailure?.();
				});
		}
	};

	const openDrawer = () => {
		dispatch(createTestContentActions.setOpeningDrawerType(questionType));
	};

	return {
		closeDrawer,
		openDrawer,
		visible,
		visibleGroupQuestion,
		isEditing,
		questionDataInDraft: isEditing ? questionDataInDraft : undefined,
		isSuccess,
		isError,
		isFetching,
		doneHandler,
		questionIdInDraft,
		testName,
	};
};

export default useQuestionDrawerType;
