// import {useParams} from "react-router-dom";
// import {useAppDispatch, useAppSelector} from "../../hooks/Redux/hooks";
// import {classMemberSelector, setClassId} from "../../redux/features/Class/ClassMemberSlice";
// import {
//     useAddMemberMutation,
//     useGetClassMemberListPaginatedQuery,
//     useGetMemberQuery,
//     useUpdateMemberMutation
// } from "../../redux/features/Class/ClassMemberAPI";
// import {Button, Col, Modal, Row, Space, Spin, Tag} from "antd";
// import {addIconSvg, deleteIconSvg, viewIconSvg} from "../../components/Icons/SvgIcons";
// import LoadingPage from "../UtilPages/LoadingPage";
// import CustomTable from "../../components/CustomTable/CustomTable";
// import ContentHeader from "../../components/ContentHeader/ContentHeader";
// import TableSearch from "../../components/Search/TableSearch";
// import React, {useEffect, useState} from "react";
// import {Formik} from "formik";
// import * as Yup from "yup";
// import {Form, Select, SubmitButton} from "formik-antd";
// import CancelButton from "../../components/ActionButton/CancelButton";
// import './ClassMembers.less';
// import antdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
// import useDebounced from "../../hooks/Debounced/useDebounced";
// import {
//     changeCriteria as changeUserSearchCriteria,
//     userSearchSelector
// } from "../../redux/features/UserSearch/UserSearchSlice";
// import {useGetUserSearchWithKeyWordQuery} from "../../redux/features/UserSearch/UserSearchAPI";
// import _ from "lodash";
// import {changeCriteria as changeMemberSearchCriteria} from "../../redux/features/Class/ClassSlice";
// import {extractPaginationInformation} from "../../components/CustomTable/TableUtils";

// const AddMemberToClassSchema = Yup.object().shape(
//     {
//         memberId: Yup.number().required("Thành viên là trường bắt buộc"),
//         memberRoles: Yup.array().min(1, "Vai trò là trường bắt buộc")
//     }
// )

// const ClassMembers = () => {

//     const {id} = useParams();
//     let [currentForm, setCurrentForm] = useState('');
//     const [isModalVisible, setModalVisible] = useState(false);
//     const classMemberSearchCriteria = useAppSelector(classMemberSelector.selectSearchCriteria);
//     const classId = useAppSelector(classMemberSelector.selectClassId);
//     const dispatch = useAppDispatch();
//     const userSearchCriteria = useAppSelector(userSearchSelector.selectSearchCriteria);
//     const debouncedUserSearchCriteriaChange=  useDebounced((criteria) => dispatch(changeUserSearchCriteria(criteria)));
//     const {data: users, isFetching: usersFetching} = useGetUserSearchWithKeyWordQuery(userSearchCriteria, {skip: currentForm !== 'ADD'});
//     const [addMember] = useAddMemberMutation();
//     const [updateMember] = useUpdateMemberMutation();
//     const [memberId, setMemberId] = useState(-1);

//     const {data: member} = useGetMemberQuery({classId, memberId}, {skip: memberId === -1});

//     useEffect(() => {
//         dispatch(setClassId(id ? Number(id) : -1));
//     }, [id]);

//     const isSkip = (idParam: string | number | undefined) => {
//         return Number(idParam) === -1;
//     }

//     const {data: paginatedResponse, isSuccess, isFetching} = useGetClassMemberListPaginatedQuery({
//         ...classMemberSearchCriteria,
//         classId: classId
//     }, {skip: isSkip(classId)});

//     const getTitleOfModal = () => {
//         switch (currentForm) {
//             case 'ADD':
//                 return 'Thêm thành viên';
//             case 'EDIT':
//                 return 'Chỉnh sửa thành viên';
//             default:
//                 return null;
//         }
//     }

//     const handleOk = () => {

//     }

//     const handleCancel = () => {
//         setModalVisible(false);
//     }

//     const renderForm = () => {
//         return <></>;
//     }

//     const handleUserSearch = (keyword: any) => {
//         const criteria = _.cloneDeep(classMemberSearchCriteria);
//         criteria.keyword = keyword;
//         debouncedUserSearchCriteriaChange(criteria);
//     }

//     const columns = [
//         {
//             title: "Họ và tên",
//             dataIndex: "name",
//             key: "name",
//             width: "40%",
//         },
//         {
//             title: "Tên đăng nhập",
//             dataIndex: "username",
//             key: "username",
//             width: "20%",
//         },
//         {
//             title: "Vai trò",
//             dataIndex: "roles",
//             key: "roles",
//             width: "30%",
//             render: (roles: string[]) => roles.map(role => (<Tag color={"blue"}>{role}</Tag>))
//         },
//         {
//             title: "",
//             key: "action",
//             render: (record: any) => (
//                 <>
//                     <div style={{display: 'flex'}}>
//                         <Button
//                             type={'link'}
//                             onClick={() => {
//                                 setCurrentForm("EDIT");
//                                 setMemberId(record.userId);
//                                 setModalVisible(true);
//                             }}
//                         >
//                             {viewIconSvg}
//                         </Button>
//                         <Button
//                             type={'link'}
//                             className={'delete-item-btn'}
//                             // onClick={() => deleteProgramHelper(record.id)}
//                         >
//                             {deleteIconSvg}
//                         </Button>
//                     </div>
//                 </>
//             ),
//         },
//     ];

//     const renderFunction = () => {
//         let componentToRender: JSX.Element = <></>;
//         if (isFetching) {
//             componentToRender = <LoadingPage/>
//         }
//         if (isSuccess) {
//             const transformedData = paginatedResponse?.listData
//                 .map(item => {
//                     return {
//                         ...item,
//                         name: item?.lastName + ' ' + item?.firstName
//                     };
//                 }) || [];
//             componentToRender = <CustomTable
//                 data={transformedData}
//                 columns={columns}
//                 metaData={extractPaginationInformation(paginatedResponse)}
//                 searchCriteria={{}}
//             />
//         }
//         return <>
//             <ContentHeader
//                 title={"Danh sách thành viên"}
//                 action={
//                     <>
//                         <div className={'search-container'}>
//                             <TableSearch
//                                 searchFields={[{title: 'Từ khóa', key: 'key'}]}
//                                 defaultFilters={{}}
//                                 searchCriteria={classMemberSearchCriteria}
//                                 changeCriteria={changeMemberSearchCriteria}
//                             />
//                             <Button
//                                 type="primary"
//                                 className={'add-program-btn'}
//                                 onClick={() => {
//                                     setCurrentForm('ADD');
//                                     setModalVisible(true);
//                                 }}
//                             >
//                                 {addIconSvg}
//                                 <span>Thêm</span>
//                             </Button>
//                         </div>
//                     </>
//                 }
//             />
//             {componentToRender}
//         </>

//     }

//     const initAddOrUpdateMemberForm = {
//         memberId: undefined,
//         memberRoles: []
//     };

//     const addMemberModal = () => {
//         return (
//             <>
//                 <Formik
//                     initialValues={
//                         memberId !== -1 && member ? {memberId: member?.userId, memberRoles: member?.roles.map(r => MemberRoleEnum[r as keyof typeof MemberRoleEnum] )} : initAddOrUpdateMemberForm
//                     }
//                     enableReinitialize={true}
//                     onSubmit={(values, actions) => {
//                         const dataToBeSent = {
//                             classId: Number(id),
//                             memberId: values.memberId,
//                             memberRoles: values.memberRoles?.map((role: MemberRoleEnum) => {
//                                 return {
//                                     memberRole: role
//                                 }
//                             })
//                         }
//                         if (currentForm === 'ADD') {
//                             addMember(dataToBeSent)
//                                 .unwrap()
//                                 .then(res => {
//                                     antdNotifier.success('Thêm thành viên thành công', 'Thông báo', 1);
//                                     setMemberId(-1);
//                                     setModalVisible(false);
//                                     actions.resetForm();
//                                 })
//                                 .catch(err => {
//                                     if (err.data.error === 'MEMBER_ALREADY_IN_CLASS') {
//                                         antdNotifier.error('Người dùng đã ở trong lớp học', 'Thông báo', 1);
//                                     }
//                                 })
//                         }
//                         else if (currentForm === 'EDIT') {
//                             updateMember(dataToBeSent)
//                                 .unwrap()
//                                 .then(res => {
//                                     antdNotifier.success('Cập nhật thành viên thành công', 'Thông báo', 1);
//                                     setModalVisible(false);
//                                     setMemberId(-1);
//                                     actions.resetForm();
//                                 })
//                                 .catch(err => {

//                                 })
//                         }
//                         actions.setSubmitting(false);
//                     }} validationSchema={AddMemberToClassSchema}>
//                     {
//                         ({isValid, touched, resetForm, dirty, values, setFieldValue, submitForm, isSubmitting}) =>
//                             <>
//                                 <Modal
//                                     title={getTitleOfModal()}
//                                     visible={isModalVisible}
//                                     width={750}
//                                     maskClosable={false}
//                                     closeIcon={<></>}
//                                     footer={null}
//                                 >
//                                     <Form layout={'vertical'}>
//                                         <Row>
//                                             <Col span={11}>
//                                                 <Form.Item
//                                                     label={'Thành viên'}
//                                                     name={'memberId'}
//                                                     required={true}
//                                                 >
//                                                     <Select
//                                                         name={'memberId'}
//                                                         disabled={memberId !== -1}
//                                                         onSearch={handleUserSearch}
//                                                         showSearch
//                                                         filterOption={false}
//                                                         notFoundContent={(usersFetching) ? <Spin size="small" /> : 'Không tìm thấy người dùng'}
//                                                     >
//                                                         {
//                                                             // work around way to show spin each time user types
//                                                             usersFetching ? [] :
//                                                             users?.listData.map(user => {
//                                                                 return <Select.Option
//                                                                     value={user.id}>{`${user.lastName} ${user.firstName}`}</Select.Option>
//                                                             })
//                                                         }
//                                                     </Select>
//                                                 </Form.Item>
//                                             </Col>
//                                             <Col span={11} offset={2}>
//                                                 <Form.Item
//                                                     label={'Vai trò'}
//                                                     name={'memberRoles'}
//                                                     required={true}
//                                                 >
//                                                     <Select mode={'multiple'}
//                                                             name={'memberRoles'}
//                                                     >
//                                                         <Select.Option
//                                                             value={MemberRoleEnum.TEACHER}>{MemberRoleEnum.TEACHER}</Select.Option>
//                                                         <Select.Option
//                                                             value={MemberRoleEnum.TEACHER_ASSISTANT}>{MemberRoleEnum.TEACHER_ASSISTANT}</Select.Option>
//                                                         <Select.Option
//                                                             value={MemberRoleEnum.STUDENT}>{MemberRoleEnum.STUDENT}</Select.Option>
//                                                     </Select>
//                                                 </Form.Item>
//                                             </Col>
//                                         </Row>
//                                         <Row justify={"end"}>
//                                             <Space>
//                                                 <CancelButton onClick={() => {
//                                                     setMemberId(-1);
//                                                     resetForm();
//                                                     handleCancel();
//                                                 }}/>
//                                                 <SubmitButton
//                                                     loading={isSubmitting}
//                                                     disabled={!isValid || !dirty}
//                                                     className={'add-member-btn'}
//                                                 >
//                                                     {addIconSvg}
//                                                     <span>Thêm</span>
//                                                 </SubmitButton>
//                                             </Space>
//                                         </Row>
//                                     </Form>
//                                 </Modal>
//                             </>
//                     }
//                 </Formik>
//             </>
//         );
//     }

//     return (
//         <>
//             {renderFunction()}
//             {addMemberModal()}
//         </>
//     );
// }

// export default ClassMembers;



export {};