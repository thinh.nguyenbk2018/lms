"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
var antd_1 = require("antd");
var react_1 = require("react");
var hooks_1 = require("../../hooks/Redux/hooks");
var GradeFormulaSlice_1 = require("../../redux/features/Grade/GradeFormulaSlice");
var AntdNotifier_1 = require("../../utils/AntdAnnouncer/AntdNotifier");
var GradeFormulaGenerator_1 = require("./GradeFormulaGenerator/GradeFormulaGenerator");
var TagsSelector_1 = require("./TagsSelector/TagsSelector");
var mathjs_1 = require("mathjs");
var math = mathjs_1.create(mathjs_1.all, {
    number: "Fraction",
    predictable: true
});
var GradeFormula = function (_a, ref) {
    var allTags = _a.allTags, isLoading = _a.isLoading, currentStep = _a.currentStep, setCurrentStep = _a.setCurrentStep;
    var dispatch = hooks_1.useAppDispatch();
    var formula = hooks_1.useAppSelector(GradeFormulaSlice_1.gradeFormulaSeletor.formulaSelector);
    var isPublic = hooks_1.useAppSelector(GradeFormulaSlice_1.gradeFormulaSeletor.isPublicSelector);
    react_1.useEffect(function () {
        return function () {
            dispatch(GradeFormulaSlice_1.resetGradeFormula());
        };
    }, [dispatch]);
    react_1.useImperativeHandle(ref, function () { return ({
        getFormula: function () {
            if (!tagTitle) {
                AntdNotifier_1["default"].error("Không được để trống tên cột điểm");
                return false;
            }
            if (formula.length === 0) {
                AntdNotifier_1["default"].error("Công thức tính điểm không đươc để trống");
                return false;
            }
            var expression = formula.map(function (ele) {
                if (ele.type === "Bracket")
                    return ele.value;
                else if (ele.type === "Number")
                    return ele.value.toString();
                else if (ele.type === "Operator")
                    return ele.value;
                else if (ele.type === "Tag")
                    return "tag_" + ele.value.id;
                else
                    return;
            }).join("   ");
            try {
                var node = math.parse(expression);
                if (node.filter(function (n) { return n.type === "FunctionNode"; }).length > 0) {
                    AntdNotifier_1["default"].error("Vui lòng không lượt bớt dấu nhân '*' trong biểu thức");
                    return false;
                }
                var variables = new Set(node.filter(function (n) { return n.type === "SymbolNode"; }).map(function (n) { return n.name; }));
                var mapVariables_1 = {};
                __spreadArrays(variables).forEach(function (v, index) { mapVariables_1[v] = index + 1; });
                var code = node.compile();
                console.log("1: ", code.evaluate(mapVariables_1));
                console.log("2: ", math.simplify(expression));
                return {
                    formula: JSON.stringify(formula),
                    expression: expression,
                    useTags: __spreadArrays(variables).map(function (v) { return parseInt(v.replace("tag_", "")); }),
                    isPublic: isPublic
                };
            }
            catch (e) {
                if (e.message === 'Division by Zero') {
                    AntdNotifier_1["default"].error("Biểu thức có lỗi chia cho 0");
                }
                else {
                    AntdNotifier_1["default"].error("Biểu thức có lỗi toán học (chú ý lỗi chi cho 0), bạn vui lòng kiểm tra lại");
                }
                console.error("Lỗi:", e);
                return false;
            }
        }
    }); });
    var tagTitle = hooks_1.useAppSelector(GradeFormulaSlice_1.gradeFormulaSeletor.titleSelector);
    var steps = [
        {
            title: 'Chọn các cột điểm',
            content: React.createElement(TagsSelector_1["default"], { allTags: allTags, isGettingTags: isLoading })
        },
        {
            title: "Viết công thức",
            content: React.createElement(GradeFormulaGenerator_1["default"], null)
        }
    ];
    return React.createElement(React.Fragment, null,
        React.createElement(antd_1.Row, { justify: "center", style: { margin: "20px 15px" } },
            React.createElement(antd_1.Col, null,
                React.createElement(antd_1.Form.Item, { label: "T\u00EAn c\u1ED9t \u0111i\u1EC3m", required: true },
                    React.createElement(antd_1.Input, { placeholder: "T\u00EAn c\u1ED9t \u0111i\u1EC3m", value: tagTitle, onChange: function (_a) {
                            var value = _a.target.value;
                            return dispatch(GradeFormulaSlice_1.updateTagTitle(value));
                        } })),
                React.createElement(antd_1.Form.Item, { label: "Hi\u1EC3n th\u1ECB cho h\u1ECDc vi\u00EAn" },
                    React.createElement(antd_1.Switch, { checked: isPublic, onChange: function (checked) { return dispatch(GradeFormulaSlice_1.updateIsPublic(checked)); } })))),
        React.createElement(antd_1.Row, { justify: "center", style: { margin: "20px 15px" } },
            React.createElement(antd_1.Col, { span: 16 },
                React.createElement(antd_1.Steps, { current: currentStep, size: "small" }, steps.map(function (item) { return (React.createElement(antd_1.Steps.Step, { key: item.title, title: item.title, onStepClick: setCurrentStep })); })))),
        React.createElement("div", { className: "steps-content" }, steps[currentStep].content));
};
exports["default"] = react_1.forwardRef(GradeFormula);
