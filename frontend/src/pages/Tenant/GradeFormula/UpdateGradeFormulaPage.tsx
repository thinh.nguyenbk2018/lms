import { Button, Card, Row, Space, Spin, Typography } from "antd";
import React from "react";
import { useEffect, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import GradeFormula, { GradeFormulaHandler } from "../../../components/GradeFormula/GradeFormula";
import GradeFormulaGenerator from "../../../components/GradeFormula/GradeFormulaGenerator/GradeFormulaGenerator";
import TagsSelector from "../../../components/GradeFormula/TagsSelector/TagsSelector";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { ScopeTag, useGetAllTagsInScopeQuery, useLazyGetGradeFormulaQuery, useUpdateGradeFormulaMutation } from "../../../redux/features/Grade/GradeApi";
import { gradeFormulaSeletor, resetGradeFormula, updateFormula, updateIsPublic, updateTags, updateTagTitle } from "../../../redux/features/Grade/GradeFormulaSlice";
import { generateGradeManagementUrl } from "../../../redux/utils/createSliceWithSearch/generateGradeManagementUrl";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";


const UpdateGradeFormulaPage = ({ scope }: { scope: ScopeTag }) => {
  let navigate = useNavigate();
  const [scopeId, setScopeId] = useState(0);
  const { classId, courseId, tagId } = useParams();
  const [getGradeFormula, { data: getGradeFormulaResponse, isLoading: isGetting, isError, error }] = useLazyGetGradeFormulaQuery();
  const gradeFormulaRef = useRef<GradeFormulaHandler>(null);

  let dispatch = useAppDispatch();

  useEffect(() => {
    setScopeId(classId ? parseInt(classId) : parseInt(courseId!));
  }, [classId, courseId])

  useEffect(() => {
    if (tagId) {
      getGradeFormula(parseInt(tagId))
    }
  }, [tagId, getGradeFormula])

  useEffect(() => {
    return () => {
      dispatch(resetGradeFormula())
    }
  }, [dispatch])

  let tagTitle = useAppSelector(gradeFormulaSeletor.titleSelector);

  let [currentStep, setCurrentStep] = useState<number>(0);

  let [updateGradeFormula, { isLoading: isUpdating }] = useUpdateGradeFormulaMutation();

  let { data: getAllTagsResponse, isLoading: isGettingTags } = useGetAllTagsInScopeQuery({ scope, scopeId })

  useEffect(() => {
    dispatch(updateTags(getAllTagsResponse?.tags.filter(tag => getGradeFormulaResponse?.useTags.includes(tag.id)) || []));
    dispatch(updateTagTitle(getGradeFormulaResponse?.tagTitle || ""))
    dispatch(updateFormula(JSON.parse(getGradeFormulaResponse?.formula || '[]')));
    dispatch(updateIsPublic(getGradeFormulaResponse?.public || false));
    
  }, [getGradeFormulaResponse, getAllTagsResponse, dispatch])

  const onDone = () => {
    let formulaResult = gradeFormulaRef.current?.getFormula();
    if (formulaResult) {

      updateGradeFormula({
        id: parseInt(tagId!),
        tagTitle: tagTitle,
        formula: formulaResult.formula,
        expression: formulaResult.expression,
        useTags: formulaResult.useTags,
        isPublic: formulaResult.isPublic
      })
        .unwrap()
        .then(_response => {
          AntdNotifier.success("Cập nhật thành công");

          navigate(generateGradeManagementUrl(scope, scopeId));
        })
        .catch(error => {
          AntdNotifier.error(error.data.message);
        })
    }
  }

  const steps = [
    {
      title: 'Chọn các cột điểm',
      content: <TagsSelector allTags={getAllTagsResponse?.tags!} isGettingTags={isGettingTags} />,
    },
    {
      title: "Viết công thức",
      content: <GradeFormulaGenerator />
    }
  ];

  return <>
    <ContentHeader
      title="Công thức tính điểm"
      action={!isError ?
        <Space>
          {currentStep < steps.length - 1 &&
            <Button
              type="dashed"
              onClick={() => setCurrentStep(currentStep + 1)}
            >
              Tiếp
            </Button>
          }
          {currentStep > 0 &&
            <Button
              type="dashed"
              onClick={() => setCurrentStep(currentStep - 1)}
            >
              Lùi
            </Button>
          }
          <Link to={generateGradeManagementUrl(scope, scopeId)}>
            <Button
              danger
              type="primary"
              color="#73d13d"
            >
              Hủy
            </Button>
          </Link>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onDone}
          >
            Xong
          </Button>
        </Space>
        : <></>
      }
    />
    <Spin spinning={isUpdating || isGetting}>
      {isError && error && "data" in error && (error.data as any).code === "PRIMITIVE_TAG_EXCEPTION" ? <Card>
        <Row justify="center">
          <Typography.Text style={{ color: "red" }}>Đây là điểm có được trong quá trình học tập của học viên, không được tính ra từ nguồn nào và không được phép tạo công thức cũng như tính điểm ở đây.</Typography.Text>
        </Row>
        <Row justify="center">
          <Link to={generateGradeManagementUrl(scope, scopeId)}>
            <Button type="primary">Quay lại trang quản lý điểm</Button>
          </Link>
        </Row>
      </Card>
        :
        <GradeFormula ref={gradeFormulaRef} allTags={getAllTagsResponse?.tags.filter(tag => tag.id !== parseInt(tagId!)) || []} isLoading={isGettingTags} currentStep={currentStep} setCurrentStep={setCurrentStep} />
      }
    </Spin>
  </>
}

export default UpdateGradeFormulaPage;