import { Divider, Row, Select as OriginalSelect, Space, Typography } from "antd";
import { FieldArray, useFormikContext } from "formik";
import { Checkbox, Form, FormItem, Input, Select } from "formik-antd";
import React from "react";
import ReactHtmlParser from "html-react-parser";
import COLOR_SCHEME from "../../../constants/ThemeColor";
import { FillInBlankQuestion } from "../../../redux/features/Quiz/CreateQuizSlice";
import BlueBorderedCard from "../../Card/BlueBorderedCard/BlueBorderedCard";
import TextEditor from "../../TextEditor/TextEditor";
import {
    DND_BLANK_SPAN_TAG_CLASS,
    replaceBlankWithSpecialSpan
} from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import './BlankConfigForm.less';
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

interface BlankProps {
    sentence: string,
    blanks: string[]
}


const stripContentFromBlankText = (blankText: string) => {
    if (!blankText) {
        return "";
    }
    return blankText.substring(1, blankText.length - 1);
}


/*
    * Parse the html -> blanks --> replace with blank config components
 */

const { Option: OriginalOption } = OriginalSelect;
const { Option } = Select;


const BlankPlaceHolder = ({ index }: { index: number }) => {
    return <div className={"blank-place-holder"}>
        <div>
            {`BLANK - ${index}`}
        </div>
    </div>
}

const replaceBlankNotationByBLankDropTarget = (questionHTML: string) => {
    const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML)
    let blankCounter = 0;

    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if ((node as any).name === "span" && (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS) {
                blankCounter++;
                return <BlankPlaceHolder index={blankCounter - 1} />
            }
            return node;
        }
    })
}

type QuestionDescriptionFormProps = {
    index: number;
    blanks: string[];
    sentence: string;
}
const QuestionDescriptionForm = ({ index, blanks, sentence }: QuestionDescriptionFormProps) => {
    const blank = blanks[index];
    console.log('blank: ', blank);
    const indexOfBlank = sentence.indexOf(blank);
    // TODO: The two parts below are supposed to be display inside a HTML reviewer
    const stringBeforeBlank = sentence.substring(0, indexOfBlank);
    const stringAfterBlank = sentence.substring(stringBeforeBlank.length + blank.length);

    const string_to_display = stringBeforeBlank.concat(`<b>[BLANK-${index}]</b>`, stringAfterBlank)

    return <TextEditor
        onChange={() => {}}
        value={string_to_display}
        useBubbleTheme={true}
        readOnly={true}
    />;
}

// * Index is the number indicating the order of the blank in the text with its context sentence
// * Intend: Passing the sentence containing the blank for context --> Too hard for me :)
// * Chosen Alternative: Passing in the whole question text :)
const BlanksConfigForms = ({ sentence, blanks }: BlankProps) => {
    const { values, setFieldValue, setFieldTouched, setValues } = useFormikContext<FillInBlankQuestion>();
    return <FieldArray name={`blankList`}
        render={
            arrayHelpers => (
                <div className={"fib-question-config-container primary-colored-border-card"}>
                    <BlueBorderedCard className={"question-previewer"} style={{ width: `80%` }}
                        title={<Typography.Text strong
                            style={{ color: COLOR_SCHEME.primary }}>
                            Xem trước câu hỏi
                        </Typography.Text>}
                    >
                        {
                            JSON.parse(values?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                                <AttachmentRender attachment={a} />
                            </Row>)
                        }
                        <Divider />
                        {
                            replaceBlankNotationByBLankDropTarget(sentence) || <Typography.Text
                                type={"danger"}>
                                Failed to replace / no blank found
                            </Typography.Text>

                        }
                    </BlueBorderedCard>
                    <div className={"blanks-config-container"}>
                        {
                            (values.blankList && values.blankList.length > 0) ?
                                <Space direction={"vertical"}>
                                    {
                                        values.blankList.map((item, index) => <div>
                                            <BlueBorderedCard style={{ minWidth: `500px` }} key={index}>
                                                {/*<Row style={{*/}
                                                {/*    marginBottom: `1em`,*/}
                                                {/*    borderBottom: `1px thin`*/}
                                                {/*}}*/}
                                                {/*    className={"blank-config-box"}>*/}
                                                {/*    <QuestionDescriptionForm index={index}*/}
                                                {/*        blanks={blanks}*/}
                                                {/*        sentence={sentence} />*/}
                                                {/*</Row>*/}
                                                <Row>
                                                    <Form>
                                                        <FormItem label={
                                                            <Typography.Text strong>
                                                                {`[BLANK-${index}]`}
                                                            </Typography.Text>
                                                        } name={`blankList[${index}].expectedAnswer`}>
                                                            <Input
                                                                name={`blankList[${index}].expectedAnswer`} />
                                                        </FormItem>
                                                        {/*<FormItem name={`blankList[${index}].hint`}*/}
                                                        {/*    label={"Gợi ý"}>*/}
                                                        {/*    <Input name={`blankList[${index}].hint`} />*/}
                                                        {/*</FormItem>*/}
                                                        <FormItem label={
                                                            <Typography.Text strong>
                                                                Cách chấm
                                                            </Typography.Text>
                                                        } name={`blankList[${index}].matchStrategy`}>
                                                            <Select
                                                                placeholder="Chọn cách chấm điểm"
                                                                name={`blankList[${index}].matchStrategy`}>
                                                                <Option value="EXACT">Khớp hoàn toàn</Option>
                                                                <Option value="CONTAIN">Bao gồm</Option>
                                                                <Option value="REGEX">Biểu thức chính quy</Option>
                                                            </Select>
                                                        </FormItem>
                                                        {/*{*/}
                                                        {/*    values.blankList[index].matchStrategy !== "REGEX" &&*/}
                                                        {/*    <FormItem*/}
                                                        {/*        name={`blankList[${index}].isCaseSensitive`}>*/}
                                                        {/*        <Checkbox*/}
                                                        {/*            name={`blankList[${index}].isCaseSensitive`}>Case-sensitive</Checkbox>*/}
                                                        {/*    </FormItem>*/}
                                                        {/*}*/}
                                                    </Form>
                                                </Row>
                                            </BlueBorderedCard>
                                        </div>
                                        )
                                    }
                                </Space>
                                : <div>Không có ô trống.</div>
                        }
                    </div>
                </div>

            )
        }
    />
}

export default BlanksConfigForms;










