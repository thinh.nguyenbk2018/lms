import "./ClassQuizResultStudentSession.less";
import useClassSidebar from "../../../../../../hooks/Routing/useClassSidebar";
import {useNavigate, useParams} from "react-router-dom";
import {useAppDispatch, useAppSelector,} from "@hooks/Redux/hooks";
import {testSessionActions, testSessionSelectors,} from "@redux/features/Quiz/TestSessionSlice";
import {
    isPaginatedResponseQuestionList,
    useGetTeacherQuizResultQuery,
    useLazyGetSessionResultQuery,
} from "@redux/features/Quiz/examAPI";
import React, {useEffect} from "react";
import AntdNotifier from "../../../../../../utils/AntdAnnouncer/AntdNotifier";
import {Card, Col, Pagination, Row, Space} from "antd";
import LoadingPage from "../../../../../UtilPages/LoadingPage";
import ApiErrorWithRetryButton from "../../../../../../components/Util/ApiErrorWithRetryButton";
import QuestionResultContainerItem
    from "../../../../../../components/Question/QuestionContainerItem/QuestionResultContainerItem";
import './ClassQuizResultStudentSession.less';
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import BackButton from "@components/ActionButton/BackButton";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../../navigation/ROUTING_CONSTANTS";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {quizResultManagementSelector} from "@redux/features/Quiz/QuizSessionManagementSlice";

const ClassQuizResultStudentSession = () => {
    const [closeClassSibar, classId, courseInfo] = useClassSidebar();

    // * Router States
    const navigate = useNavigate();
    const {quizItemId, testSessionId} = useParams();
    const {testSessionId: sessionId} = useParams();
    // * Redux States
    const dispatch = useAppDispatch();
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const searchCriteria = useAppSelector(
        testSessionSelectors.selectSearchCriteria
    );

    const quizSessionResultManagementCriteria = useAppSelector(
        quizResultManagementSelector.selectSearchCriteria
    );

    const changeSearchCriteria = testSessionActions.changeCriteria;

    const shouldSkip = () => {
        return (
            isNaN(Number(courseInfo?.id)) || isNaN(Number(classId)) || !sessionId
        );
    };

    const [
        trigger,
        {
            data: sessionResultData,
            isFetching,
            isError,
            isSuccess,
            isLoading,
            error
        }] = useLazyGetSessionResultQuery();

    const {
        refetch: refetchQuizResult
    } = useGetTeacherQuizResultQuery({quizId: Number(quizItemId), searchCriteria: quizSessionResultManagementCriteria}, {skip: userInfo?.accountType === 'STUDENT'});

    useEffect(() => {
        if (!sessionId) {
            return;
        }
        trigger({
            classId: Number(classId),
            quizId: Number(quizItemId),
            sessionId: testSessionId || "",
            searchCriteria,
            question: undefined
        });
    }, [sessionId, searchCriteria]);

    if (isSuccess && courseInfo?.id && sessionResultData) {
        const questionsResultData = sessionResultData.questions;
        // @ts-ignore
        return (
            <>
                <ContentHeader
                    title={'Làm bài kiểm tra'}
                    action={
                        <Space>
                            <BackButton
                                type="primary"
                                danger
                                onClick={() => {
                                    if (userInfo?.accountType === 'STAFF') {
                                        refetchQuizResult();
                                        navigate(
                                            replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_MANAGEMENT,
                                                [":classId", classId || ""],
                                                [":quizItemId", quizItemId || ""])
                                        )
                                    } else {
                                        navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                            [":classId", classId || ""],
                                            [":quizItemId", quizItemId || ""]))
                                    }

                                }}
                                className={"back-btn"}
                            />
                        </Space>
                    }
                />
                <Row>
                    <Col sm={16} md={16} lg={16} xl={16}>
                        <Row gutter={[12, 23]}>
                            <Col sm={24} md={24} lg={24} xl={24} xxl={{span: 18, offset: 2}}>
                                <Row gutter={[0, 20]} align={"middle"} justify={"center"}>
                                    {isPaginatedResponseQuestionList(questionsResultData)
                                        ? questionsResultData?.listData.map((item, index) => (
                                            <>
                                                <span id={`question-result-${item.id}`}/>
                                                <Col span={24} key={item.id}>
                                                    <QuestionResultContainerItem
                                                        {...item}
                                                        order={(questionsResultData.page - 1) * searchCriteria.pagination.size + index + 1}
                                                    />
                                                </Col>
                                            </>
                                        ))
                                        : "Không thể hiển thị câu hỏi, vui lòng liên hệ với quản trị viên hệ thống."}
                                </Row>
                                <Row justify={"end"} style={{marginTop: 12}}>
                                    {isPaginatedResponseQuestionList(questionsResultData) && (
                                        <Pagination
                                            showSizeChanger
                                            pageSize={searchCriteria?.pagination?.size || DEFAULT_PAGE_SIZE}
                                            onChange={(page: any, size: any) => {
                                                const payload = {
                                                    ...searchCriteria,
                                                    pagination: {page, size},
                                                };
                                                console.log('payload: ', payload);
                                                dispatch(changeSearchCriteria(payload));
                                            }}
                                            onShowSizeChange={(current, newSize) => {
                                                let newSearchCriteria = {
                                                    ...searchCriteria,
                                                    pagination: {
                                                        size: newSize,
                                                        page: 1,
                                                    },
                                                };
                                                dispatch(
                                                    changeSearchCriteria(newSearchCriteria)
                                                );
                                                window.scrollTo(0, 0);
                                            }}
                                            current={questionsResultData?.page}
                                            total={questionsResultData?.total}
                                        />
                                    )}
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={8} md={8} lg={8} xl={8}>
                        <Card className={'quiz-session-result-wrapper'} title={'Kết quả'}>
                            <div>
                                <Row>
                                    <Col span={10}>Điểm:</Col>
                                    <Col offset={4}
                                         style={{fontWeight: 'bold'}}>{sessionResultData?.score?.toFixed(2)}</Col>
                                </Row>
                                <Row>
                                    <Col span={10}>Thời gian làm bài:</Col>
                                    <Col offset={4}
                                         style={{fontWeight: 'bold'}}>{Math.floor(sessionResultData?.timeTaken / 60)} phút {sessionResultData.timeTaken - Math.floor(sessionResultData.timeTaken / 60) * 60} giây</Col>
                                </Row>
                            </div>
                        </Card>
                        <Card className={'quiz-session-result-wrapper'} title={'Câu trả lời chưa được chấm điểm'}>
                            <div>
                                <ul className={'ungraded-questions-list'}>
                                    {sessionResultData?.questions?.unGradedQuestions?.map(q => {
                                        return (
                                            <a
                                                className={'ungraded-questions-navigate-link'}
                                                onClick={() => {
                                                    trigger({
                                                        classId: Number(classId),
                                                        quizId: Number(quizItemId),
                                                        sessionId: testSessionId || "",
                                                        searchCriteria,
                                                        question: q.questionId
                                                    })
                                                        .unwrap()
                                                        .then((res: any) => {
                                                            const violation = document.getElementById(`question-result-${q.questionId}`);
                                                            window.scrollTo({
                                                                top: violation?.getBoundingClientRect().top,
                                                                behavior: "smooth"
                                                            });
                                                        });
                                                }}
                                            >
                                                <li>
                                                    {`Câu ${q.order}`}
                                                </li>
                                            </a>
                                        )
                                    })}
                                </ul>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    } else if (isLoading) {
        return <LoadingPage/>;
    } else if (isError) {
        console.log(error);
        AntdNotifier.error(`Session failed with ${JSON.stringify(error)}`);
        return <ApiErrorWithRetryButton onRetry={() => {
        }}/>;
    } else {
        return <LoadingPage/>
    }
};

export default ClassQuizResultStudentSession;
