import React from "react";
export function isJsxElement(ele: any): ele is JSX.Element {
    if(!ele) return false; 
    return React.isValidElement(ele); 
} 