import {createEntityAdapter, EntityState, PayloadAction} from "@reduxjs/toolkit";
import {DEFAULT_FRONTEND_ID, ID, PaginatedResponse} from "../../interfaces/types";
import {RootState} from "../../store/store";
import {
    EnhancedSliceWithSearch,
    generateInitialSearchSlice
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {createSliceEnhancedWithSearch} from "./../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {BriefTestItem} from "./examAPI";

export const QUESTION_TYPES = {
    MULTI_CHOICE: "MULTI_CHOICE",
    WRITTEN: "WRITING",
    FILL_IN_BLANK: "FILL_IN_BLANK",
    BLANK_WITH_CHOICES: "FILL_IN_BLANK_WITH_CHOICES",
    DRAG_AND_DROP: "FILL_IN_BLANK_DRAG_AND_DROP",
    GROUP: "group",
};

export type QuestionType =
    | "MULTI_CHOICE"
    | "WRITING"
    | "FILL_IN_BLANK"
    | "FILL_IN_BLANK_WITH_CHOICES"
    | "FILL_IN_BLANK_DRAG_AND_DROP"
    | "GROUP";

export type QuizSessionResponseDto = PaginatedResponse<QuestionDataType> & {
    remainingTime: number; // seconds
    haveTimeLimit: boolean;
}

export type QuestionDataType =
    | MultichoiceQuestion
    | WrittenQuestion
    | FillInBlankQuestion
    | BlankWithChoicesQuestion
    | FillBlankDragAndDropQuestion
    | QuestionGroup;

export type QuestionAnswerPagedResponseDto = PaginatedResponse<QuestionDataTypeWithResult> & {
    unGradedQuestions: UngradedQuestionDto[]
}

export type UngradedQuestionDto = {
    questionId: number;
    order: number;
}

export type QuestionDataTypeWithResult =
    | MultichoiceQuestionWithResult
    | FillInBlankQuestionWithResult
    | BlankWitchChoicesQuestionWithResult
    | FillBlankDragAndDropQuestionWithResult
    | WrittenQuestionWithResult
    | QuestionGroupWithResult;

export function isMultichoiceQuestion(
    question: any
): question is MultichoiceQuestion {
    if (!question) {
        return false;
    }
    return (question as MultichoiceQuestion).type === "MULTI_CHOICE";
}

export function isFIBquestion(question: any): question is FillInBlankQuestion {
    if (!question) {
        return false;
    }
    return (question as FillInBlankQuestion).type === "FILL_IN_BLANK";
}

export function isBlankWithChoicesQuestion(
    question: any
): question is BlankWithChoicesQuestion {
    if (!question) {
        return false;
    }
    return (
        (question as BlankWithChoicesQuestion).type === "FILL_IN_BLANK_WITH_CHOICES"
    );
}

export function isFIB_DND_Question(
    question: any
): question is FillBlankDragAndDropQuestion {
    if (!question) {
        return false;
    }
    return (
        (question as FillBlankDragAndDropQuestion).type ===
        "FILL_IN_BLANK_DRAG_AND_DROP"
    );
}

export function isWritingQuestion(question: any): question is WrittenQuestion {
    if (!question) {
        return false;
    }
    return (question as WrittenQuestion).type === "WRITING";
}

export function isQuestionGroup(question: any): question is QuestionGroup {
    if (!question) {
        return false;
    }
    return (question as QuestionGroup).type === "GROUP";
}

// * GENERAL INFO EACH QUESTION SHOULD HAVE
export interface Question {
    id?: number;
    type: QuestionType;
    note?: string;
    point: number;
    order: number;
    // * The problem statement
    description?: string;
    earnedPoint?: number;
    attachment?: string;
}

/**
 *  Group of question
 */
export interface QuestionGroup extends Question {
    id?: ID;
    type: "GROUP";
    description: string;
    questionList: QuestionDataType[];
}

export interface QuestionGroupWithResult extends Omit<QuestionGroup, "questionList"> {
    resultState: "CORRECT" | "PARTIAL_CORRECT" | "WRONG" | undefined;
    answerList: QuestionDataTypeWithResult[]
}

// * MULTICHOICE
export interface Choice {
    order?: number;
    content: string;
    isCorrect: boolean;
    // * This need not be saved on the backend as its only purpose is to diffrentiate the answer for drag and drop
    answerKey: number | string;
    isChosenByStudent?: boolean;
}

export type ChoiceWithResult = Choice & { id?: ID };

export interface MultichoiceQuestion extends Question {
    type: "MULTI_CHOICE";
    description: string;
    answerList: Choice[];
    isMultipleAnswer: boolean;
}

export interface MultichoiceQuestionWithResult
    extends Omit<MultichoiceQuestion, "answerList"> {
    resultAnswerList: ChoiceWithResult[];
}

// * WRITTEN
export interface WrittenQuestion extends Question {
    type: "WRITING";
    description: string;
    studentAnswer: string;
}

export interface WrittenQuestionWithResult extends WrittenQuestion {
    answer: string;
}

// * FILL IN BLANK
export interface FillInBlankQuestion extends Question {
    type: "FILL_IN_BLANK";
    description: string;
    blankList: BlankWriting[];
}

export interface BlankWriting {
    id?: number;
    hint?: string;
    expectedAnswer: string;
    matchStrategy: "EXACT" | "CONTAIN" | "REGEX";
    isCaseSensitive?: boolean;
    studentAnswer: string;
}

export type BlankWithResult = BlankWriting & {
    studentAnswer: string;
    isCorrect: boolean;
};

export interface FillInBlankQuestionWithResult
    extends Omit<FillInBlankQuestion, "blankList"> {
    resultAnswerList: BlankWithResult[];
}

// * BLANK WITH CHOICES
export interface BlankWithChoicesQuestion extends Question {
    type: "FILL_IN_BLANK_WITH_CHOICES";
    description: string;
    blankList: BlankWithOption[];
}

export interface BlankWithOption {
    hint: string;
    answerList: Choice[];
    correctAnswerKey?: string | number | undefined | null;
    studentAnswer?: number;
}

export type BlankWithOptionWithResult = Omit<BlankWithOption, "answerList"> & {
    id?: number;
    studentAnswerKey: number;
    isCorrect: boolean;
    answerList: (Omit<Choice, "isCorrect"> & { id?: number })[];
};

export interface BlankWitchChoicesQuestionWithResult
    extends Omit<BlankWithChoicesQuestion, "blankList"> {
    resultAnswerList: BlankWithOptionWithResult[];
}

// * Fill in blank drag and drop
export interface FillBlankDragAndDropQuestion extends Question {
    type: "FILL_IN_BLANK_DRAG_AND_DROP";
    answerList: DragAndDropAnswer[];
    blankList: DragAndDropBlank[];
}

export type DragAndDropBlankResult = {
    id?: number;
    hint: string;
    answerKey: number;
    studentAnswerKey: number;
    studentAnswer: string;
    expectedAnswer: string;
    isCorrect: boolean;
};

export interface FillBlankDragAndDropQuestionWithResult
    extends Omit<FillBlankDragAndDropQuestion, "blankList"> {
    resultAnswerList: DragAndDropBlankResult[];
}

type AnswerKey = string | number;

export interface DragAndDropAnswer {
    id?: ID;
    // To be displayed inside a Text editor
    content: string;
    key: AnswerKey;
    order: number;
}

export interface DragAndDropBlank {
    hint: string;
    answerKey: AnswerKey;
    studentAnswer?: string;
}

const questionAdapter = createEntityAdapter<Question>({
    selectId: (question) => question.order,
    sortComparer: (a, b) => {
        if (a.order > b.order) return 1;
        else if (a.order > b.order) return -1;
        return 0;
    },
});

export type TestMeta = {
    timeLimit?: number;
    tag?: string | undefined;
    title: string;
    courseId?: number | undefined;
    description?: string | undefined;
    createdAt?: number;
    updatedAt?: number;
};

export type OpeningDrawerType = QuestionType | "NONE";

type EntitySliceState = EntityState<Question> & {
    testMeta: TestMeta;
    questionIdInDraft: ID;
    testContentList: BriefTestItem[];
} & {
    openingDrawer: OpeningDrawerType;
    currentTestName: string;
    questionGroupDraftingState: QuestionGroupDraftingState;
    questionGroupInDraft: QuestionGroup;
    questionGroupIdInDraft: ID
};

type A = EnhancedSliceWithSearch<EntitySliceState>;

export type QuestionGroupDraftingState = "EDITING" | "CREATING" | "NONE";
export const defaultQuestionGroup: QuestionGroup = {
    id: undefined,
    type: "GROUP",
    questionList: [],
    description: "",
    point: 0,
    order: 0,
};

const initState: A = questionAdapter.getInitialState({
    testMeta: {
        courseId: DEFAULT_FRONTEND_ID,
        title: "",
        timeLimit: DEFAULT_FRONTEND_ID,
        tag: "",
    },
    questionIdInDraft: DEFAULT_FRONTEND_ID,
    testContentList: [],
    openingDrawer: "NONE",
    currentTestName: "",

    // * QuestionGroupRelated
    questionGroupDraftingState: "NONE",
    questionGroupInDraft: defaultQuestionGroup,
    questionGroupIdInDraft: DEFAULT_FRONTEND_ID,

    ...generateInitialSearchSlice(),
});

export const TEST_CONTENT_SLICE_KEY = "testContent";
export const DEFAULT_TEST_CONTENT_NAME = "";
export const quizCreateSlice = createSliceEnhancedWithSearch({
    name: TEST_CONTENT_SLICE_KEY,
    initialState: initState,
    reducers: {
        // * TEST
        updateTestInfo: (
            state,
            action: PayloadAction<{
                courseId?: number;
                title?: string;
                timeLimit?: number;
                tag?: string;
            }>
        ) => {
            state = {...state, ...action.payload};
        },

        // * QUESTION
        // ? Below commented code are invalid: We will push changes gradually to server instead of
        // ? creating transient state and push the questionList to the backend in batch.
        // questionAdded: (state, action: PayloadAction<Question>) => {
        //     questionAdapter.addOne(state, action.payload);
        // },
        // // * THe order of the question removed
        // removeQuestion: (state, action: PayloadAction<number>) => {
        //     questionAdapter.removeOne(state, action.payload)
        // },
        // updateQuestion: questionAdapter.updateOne,

        // * GROUP
        /** Upon receiving backend response of creating GROUP  OR enter edit mode of a GROUP
         * Subsequent question will be added inside this GROUP
         * For ease of coding. We only allow editing one GROUP at a time
         * ! This should be called after the mutation for creating the GROUP reached the server
         * @param state
         * @param action
         *
         */
        setQuestionIdInDraft: (state, action: PayloadAction<number>) => {
            state.questionIdInDraft = action.payload;
        },
        /**
         * ! This should be called when the mutation for uploading the question GROUP success
         * @param state
         */
        doneDraftingQuestion: (state) => {
            state.questionIdInDraft = DEFAULT_FRONTEND_ID;
            state.questionGroupIdInDraft = DEFAULT_FRONTEND_ID;
        },

        setTestContentList: (state, action: PayloadAction<BriefTestItem[]>) => {
            state.testContentList = action.payload;
        },

        setOpeningDrawerType: (state, action: PayloadAction<OpeningDrawerType>) => {
            state.openingDrawer = action.payload;
        },

        closeAnyDrawer: (state) => {
            state.openingDrawer = "NONE";
        },

        editQuestionWithDrawer: (
            state,
            action: PayloadAction<{ questionId: number; type: QuestionType }>
        ) => {
            const {questionId, type} = action.payload;
            state.questionIdInDraft = questionId;
            state.openingDrawer = type;
        },

        setCurrentTestContentName: (state, action: PayloadAction<string>) => {
            state.currentTestName = action.payload;
        },

        addingQuestionGroup: (state) => {
            state.questionGroupDraftingState = "CREATING";
        },

        editQuestionGroup: (state, action: PayloadAction<QuestionGroup>) => {
            state.questionGroupDraftingState = "EDITING";
            state.questionGroupInDraft = action.payload;
            state.questionGroupIdInDraft = action.payload.id || DEFAULT_FRONTEND_ID;
            state.openingDrawer = "GROUP";
        },
        doneDraftingQuestionGroup: (state) => {
            state.questionGroupDraftingState = "NONE";
            state.questionGroupInDraft = defaultQuestionGroup;
            state.questionGroupIdInDraft = DEFAULT_FRONTEND_ID;
            state.questionIdInDraft = DEFAULT_FRONTEND_ID;
            state.openingDrawer = "NONE";
        },
        setQuestionGroupIdInDraft: (state, action: PayloadAction<number>) => {
            state.questionGroupIdInDraft = action.payload;
        }
    },
});

export const createTestContentActions = quizCreateSlice.actions;

export const createTestContentSelectors = {
    testMeta: (state: RootState) => state.testContent.testMeta,
    // * Select the question state in draft => to pick from cache
    // isEditingGroup: (state: RootState) =>
    selectQuestionIdInDraft: (state: RootState) =>
        state.testContent.questionIdInDraft,
    selectIsEditingAQuestion: (state: RootState) =>
        state.testContent.questionIdInDraft !== DEFAULT_FRONTEND_ID,
    selectSearchCriteria: (state: RootState) => state.testContent.searchCriteria,
    selectCurrentTestName: (state: RootState) =>
        state.testContent.currentTestName,
    selectOpeningDrawer: (state: RootState) => state.testContent.openingDrawer,
    selectIsQuestionDrawerTypeOpening: (state: RootState, type: QuestionType) =>
        state.testContent.openingDrawer === type,
    selectIsDrawerEditing: (state: RootState, type: QuestionType) =>
        state.testContent.openingDrawer === type &&
        state.testContent.questionIdInDraft !== DEFAULT_FRONTEND_ID,

    // * Question group related
    selectIsAddingQuestionGroup: (state: RootState) =>
        state.testContent.questionGroupDraftingState === "CREATING",
    selectIsEditingQuestionGroup: (state: RootState) =>
        state.testContent.questionGroupDraftingState === "EDITING",
    selectIsEditingQuestionInGroup: (state: RootState) =>
        state.testContent.questionGroupIdInDraft !== DEFAULT_FRONTEND_ID && !["GROUP", "NONE"].includes(state.testContent.openingDrawer),
    selectQuestionGroupInDraft: (state: RootState) =>
        state.testContent.questionGroupInDraft,
    selectQuestionGroupIdInDraft: (state: RootState) =>
        state.testContent.questionGroupIdInDraft,
    selectIsQuestionGroupBeingEdited: (
        state: RootState,
        groupId: ID | undefined
    ) => {
        if (!groupId) return false;
        return groupId === state.testContent.questionGroupInDraft.id;
    },
};   
