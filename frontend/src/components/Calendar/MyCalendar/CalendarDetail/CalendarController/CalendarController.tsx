/* eslint-disable no-unused-vars */
import {skipToken} from '@reduxjs/toolkit/query';
import {Button, Dropdown, Select} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleLeft} from '@fortawesome/free-solid-svg-icons/faAngleLeft';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import {useAppDispatch, useAppSelector,} from '@hooks/Redux/hooks';
import {getMonthName} from '../../../Utils/CalendarUtils';
import AddNewEventMenu from '../NewEventMenu/NewEventMenu';
import {useGetPersonalCalendarQuery} from '@redux/features/Calendar/CalendarAPI';
import {classTimeTableActions} from '@redux/features/Class/ClassTimeTable/ClassTimeTableSlice';
import {authSelectors} from '@redux/features/Auth/AuthSlice';
import CalendarNewEventFormModal from '../../../Forms/CalendarNewEventFormModal';
import './CalendarController.less';
import {calendarSelector} from '@redux_features/Calendar/CalendarSlice';

const {Option} = Select;

function CalendarController() {
    const selectedDate = useAppSelector(calendarSelector.selectCurrentDate);
    const weekToFocus = useAppSelector(calendarSelector.selectCurrentWeek);
    const dispatch = useAppDispatch();
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);
    const userID = userInfo?.id;
    const {
        data: eventMap = {},
        isSuccess,
        isError,
        isFetching,
        error,
    } = useGetPersonalCalendarQuery(userID ?? skipToken);
    const getCalendarTitleText: () => string = () => {
        // * Calculate the hedaer: a range (week display) | a date Name (date view)
        if (!weekToFocus) {
            return '';
        }
        const firstDateOfWeek = weekToFocus[0];
        const lastDateOfWeek = weekToFocus[6];
        if (firstDateOfWeek.getMonth() === lastDateOfWeek.getMonth()) {
            return `${getMonthName(
                firstDateOfWeek,
            )}, ${firstDateOfWeek.getDate()} - ${lastDateOfWeek.getDate()}`;
        }
        return `${getMonthName(
            firstDateOfWeek,
        )}, ${firstDateOfWeek.getDate()} - ${getMonthName(
            lastDateOfWeek,
        )}, ${lastDateOfWeek.getDate()}`;
    };
    // * On add event handlers:
    const onAddClassSchedule = () => {
        dispatch(classTimeTableActions.addNewClassSchedule());
    };
    const onAddClassSession = () => {
        dispatch(classTimeTableActions.addNewClassSession());
    };

    const onAddPersonalEvent = () => {
        dispatch(classTimeTableActions.addNewPersonalEvetn());
    };

    return (
        <div className="calendar-control-header">
            <div className="calendar-date-name">
                <FontAwesomeIcon
                    className="controller-icon-button backward"
                    icon={faAngleLeft}
                />
                <span className="calendar-title-text">{getCalendarTitleText()}</span>
                <FontAwesomeIcon
                    className="controller-icon-button forward"
                    icon={faAngleRight}
                />
            </div>
            <div className="calendar-control">
                <div className="calendar-mode-container">
                    <Select defaultValue="week">
                        <Option value="month">Month</Option>
                        <Option value="week">Week</Option>
                        <Option value="day">Day</Option>
                        <Option value="list">List</Option>
                    </Select>
                </div>
                <CalendarNewEventFormModal modalProps={{}}/>
                <div className="calendar-add-button">
                    <Dropdown
                        overlay={(
                            <AddNewEventMenu
                                onSelect={(key) => {
                                    switch (key) {
                                        case 'CLASS_SESSION':
                                            onAddClassSession();
                                            break;
                                        case 'PERSONAL':
                                            onAddPersonalEvent();
                                            break;
                                        case 'CLASS_SCHEDULE':
                                            onAddClassSchedule();
                                            break;
                                        default:
                                    }
                                }}
                            />
                        )}
                    >
                        <Button type="primary">Thêm một sự kiện</Button>
                    </Dropdown>
                </div>
            </div>
        </div>
    );
}

export default React.memo(CalendarController);
