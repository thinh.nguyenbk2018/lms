import React from 'react';
import { useNavigate, useParams} from "react-router-dom";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import antdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import LoadingPage from "../../../UtilPages/LoadingPage";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";

const CourseFeedBack = () => {
    // * Class specif routes
    const {courseId} = useParams();
    const {forceCloseSidebar,courseDetail,isCourseDetailError,isCourseDetailFetching,isCourseDetailSuccess} = useCourseSidebar();
    const navigate = useNavigate();



    if (!courseId || isNaN(Number(courseId))) {
        antdNotifier.error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
        return <>
            <ApiErrorWithRetryButton onRetry={() => {
            }}/>
        </>;
    }

    const renderFunction = () => {
        if (isCourseDetailSuccess) {
            return <>
                <ContentHeader
                    title={`Feedback ${courseDetail?.name}`}
                />
            </>
        } else if (isCourseDetailFetching) {
            return <LoadingPage/>
        }
        return <>
        </>
    }
    return (
        renderFunction()
    );

};

export default CourseFeedBack;