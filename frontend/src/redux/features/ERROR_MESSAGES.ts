export const DEFAULT_API_ERROR_MESSAGE =
	"Lỗi khi liên lạc với server. Hãy liên hệ hoặc báo cáo lại lỗi với bộ phận kỹ thuật";

const ERROR_MESSAGES = {
	API: {
		GENERAL: {
			DEFAULT:
				"Lỗi khi liên lạc với server. Hãy liên hệ hoặc báo cáo lại lỗi với bộ phận kỹ thuật",
			UNREACHABLE:
				"Request của bạn không đến được server hãy báo cáo lỗi này với dev team nhé",
			TIME_OUT:
				"Request của bạn đã bị timeout. Hãy liên hệ team dev để  được giải quyết",
		},
	},
};

export default ERROR_MESSAGES;
