import React, { useRef } from "react";
import { ConnectDragSource, useDrag, useDrop } from "react-dnd";
import { Identifier } from "dnd-core";
import { TeachingContentDnDTypes } from "../../../../../constants/DragTypes";
import {
	Activity,
	Course,
} from "../../../../../typescript/interfaces/courses/CourseTypes";
import classNames from "classnames";
import "./DraggableChapter.less";
import { DraggedActivity } from "../Activity/DraggableDroppableActivity";
import { ID } from "../../../../../redux/interfaces/types";

export interface DraggedChapter {
	id: ID;
	order: number;
}

type DropProps = {
	handlerId: Identifier | null;
	isOver: boolean;
	canDrop: boolean;
	didDrop: boolean;
};

type DragProps = {
	isDragging: boolean;
	didDrop: boolean;
};

// * pass the position to the backend so the backend can handle re-placement
export type ChapterPlacementInfo = {
	id: number;
	order: number;
};

export const packageChapterPlacementInfo: (
	id: number,
	order: number
) => ChapterPlacementInfo = (id, order) => {
	return {
		id,
		order,
	};
};

const OVER_TOP_VALUE = -1;
const OVER_BOTTOM_VALUE = -2;

type SpecialPlacement = "OVER_TOP" | "OVER_BOTTOM";
export const getOrderValueFromSpecialPlacement = (
	specialVal: SpecialPlacement
) => {
	if (specialVal === "OVER_BOTTOM") {
		return OVER_BOTTOM_VALUE;
	} else return OVER_TOP_VALUE;
};

export type ActivittyPlacementInfo = {
	id: number;
	chapterId: number;
	itemOrder: number;
};

type DraggableAndDroppableChapterProps = {
	id: ID;
	order: number;
	exchangePlace: (from: ChapterPlacementInfo, to: ChapterPlacementInfo) => void;
	children: JSX.Element;
	renderWithPreviewRef?: (ref: ConnectDragSource) => JSX.Element;
	debugCallBack?: () => void;
	addActivityCallback?: (item: Activity) => void;

	// * Receive item when the chapter is empty
	// * Handle case when the handler is the chapter not the inner activity
	receiveActivity: (
		item: DraggedActivity,
		isOutsideOfSiblings?: SpecialPlacement
	) => void;
};

function isDraggedAcitivity(draggedItem: any): draggedItem is DraggedActivity {
	if (!draggedItem) return false;
	const assertedItem = draggedItem as DraggedActivity;
	return assertedItem.type === "quiz" || assertedItem.type === "unit";
}

export const DraggableAndDroppableChapter = (
	props: DraggableAndDroppableChapterProps
) => {
	const ref = useRef<HTMLDivElement>(null);

	const [{ handlerId, canDrop, isOver }, drop] = useDrop<
		DraggedChapter | DraggedActivity,
		void,
		DropProps
	>({
		accept: [
			TeachingContentDnDTypes.Chapter,
			TeachingContentDnDTypes.Quiz,
			TeachingContentDnDTypes.Unit,
		],
		collect: (monitor) => {
			return {
				handlerId: monitor.getHandlerId(),
				canDrop: monitor.canDrop(),
				isOver: monitor.isOver(),
				didDrop: monitor.didDrop(),
			};
		},
		hover(item, monitor) {
			if (!ref.current) {
				return;
			}
			// * TODO: Display an empty box showing the hover can result in a drop
		},
		drop: (item, monitor) => {
			if (!isDraggedAcitivity(item)) {
				const draggedItemIdx = item.order;
				const hoveredItemIdx = props.order;

				if (draggedItemIdx === hoveredItemIdx) {
					return;
				}
				// * Package chapter swapping information
				const { id: sourceId, order: sourceOrder } = item;
				const { id: targetId, order: targetOrder } = props;
				props.exchangePlace(
					packageChapterPlacementInfo(sourceId, sourceOrder),
					packageChapterPlacementInfo(targetId, targetOrder)
				);
			} else {
				// * Drop Item into the chapter: Add activity in this chapter and remove the activity from its origin
				// AntdNotifier.success(`is activity`)
				// * The items in the chapter already handle the drop --> Do nothing
				if (didDrop) {
					return;
				}
				// * Only need to handle the case where the dropped-into chapter is empty initially
				else {
					const chapterBoundRect = ref.current?.getBoundingClientRect();
					if (!chapterBoundRect) {
						return;
					}
					const chapterTop = chapterBoundRect.top;
					const chapterBottom = chapterBoundRect.bottom;

					const mouseY = monitor?.getClientOffset()?.y || 0;

					const distanceToTop = Math.abs(mouseY - chapterTop);
					const distanceToBottom = Math.abs(mouseY - chapterBottom);

					if (distanceToTop > distanceToBottom) {
						props.receiveActivity(item, "OVER_BOTTOM" as const);
					} else props.receiveActivity(item, "OVER_TOP");
					// * Add activity to this chapter
				}
			}

			if (isOver) {
				console.log(
					"DROP DROP RECT INFO: ",
					ref?.current?.getBoundingClientRect()
				);
			}
		},
	});

	const [{ isDragging, didDrop }, preview, drag] = useDrag<
		DraggedChapter,
		void,
		DragProps
	>({
		type: TeachingContentDnDTypes.Chapter,
		item: () => {
			return {
				id: props.id,
				order: props.order,
			};
		},
		collect: (monitor) => ({
			isDragging: monitor.isDragging(),
			didDrop: monitor.didDrop(),
		}),
	});
	// TODO: Extract class name form isDragging and canDrop to add extra styling to guide the interaction
	drag(drop(ref));
	return (
		<div
			ref={ref}
			className={classNames({
				dropContainer: true,
				droppable: canDrop,
				isHoveringOver: isOver,
				isBeingDragged: isDragging,
			})}
			data-handler-id={handlerId}
			style={{}}
		>
			{props.renderWithPreviewRef
				? props.renderWithPreviewRef(preview)
				: props.children}
		</div>
	);
};
