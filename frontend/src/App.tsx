import React, { useEffect, useState } from "react";
import { BackTop, Card, Layout, notification, Tooltip } from "antd";
import SideBar from "./components/SideBar/SideBar";
import CustomHeader from "./components/CustomHeader/CustomHeader";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import { InAppNavItems } from "./navigation/NavigationItem";
import CustomBreadCrumbs from "./components/BreadCrumbs/CustomBreadCrumbs";
import "./App.less";
import { authSelectors } from "./redux/features/Auth/AuthSlice";
import LoginRegisterPage from "./pages/LoginRegister/LoginRegisterPage";
import { useAppDispatch, useAppSelector } from "./hooks/Redux/hooks";
import { isPathRelatedToClass } from "./hooks/Routing/useClassSidebar";
import { sideBarActions } from "./redux/features/SideBar/SidebarSlice";
import { isPathRelatedToCourse } from "./hooks/Routing/useCourseSidebar";
import { faRocket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ErrorBoundary } from "react-error-boundary";
import * as Sentry from "@sentry/react";
import isDev from "./index";
import { ROUTING_CONSTANTS } from "./navigation/ROUTING_CONSTANTS";
import { ArgsProps } from "@utils/AntdAnnouncer/AntdNotifier";
import { useGetReceivedAnnouncementsQuery } from "@redux/features/Announcement/AnnouncementAPI";
import { toNumber } from "lodash";
import {API_CONSTANT} from "@constants/ApiConfigs";

const Stomp = require("@stomp/stompjs");

const { Content } = Layout;

// TODO: Also show spinning if the socket is not ready just yet
function App() {
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);
    const userLoggedIn = useAppSelector(authSelectors.selectLoginSuccess);
    const location = useLocation();
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        const pathName = location.pathname;
        if (pathName) {
            if (!isPathRelatedToClass(pathName)) {
                dispatch(sideBarActions.closeClassSidebar());
            }
            if (!isPathRelatedToCourse(pathName)) {
                dispatch(sideBarActions.closeCourseSidebar());
            }
        }
    }, [location]);

    useEffect(() => {
        // TODO: change path of socket to use ENV
        const socketClient = new Stomp.Client({
            brokerURL: API_CONSTANT.BASE_SOCKET_URL,
            debug: function (str: string) {
                console.log(str);
            },
            reconnectDelay: 5000,
            heartbeatIncoming: 4000,
            heartbeatOutgoing: 4000,
        });
        let subscription: any;
        socketClient.onConnect = ((frame: any) => {
            subscription = socketClient.subscribe(`topic.user.announcement.${userInfo?.id}`, (message: any) => {
                const enc = new TextDecoder('utf8');
                const msg = JSON.parse(enc.decode(message.binaryBody));
                notification["success"]({
                    message: `Bạn có thông báo mới từ lớp ${msg['className']}`,
                    onClick: () => {
                        if (location.pathname?.endsWith("/announcement")) {
                            console.log('if: pathname', location.pathname);
                            window.location.reload();
                        } else {
                            console.log('else: pathname', location.pathname);
                            navigate(ROUTING_CONSTANTS.CLASS_ANNOUNCEMENT.replace(':classId', msg['classId']));
                        }
                    },
                    duration: 10,
                    key: msg['id'],
                    className: 'announcement-real-time',
                    placement: 'bottomLeft'
                } as ArgsProps)
            });
        })
        socketClient.activate();
        return () => {
            if (subscription != undefined) {
                subscription.unsubscribe();
            }
        }
    }, [userInfo]);

    const username = userInfo?.username;

    const renderError = () => {
        return isDev() ? (
            <ErrorBoundary
                key={location.pathname}
                FallbackComponent={() => {
                    return (
                        <div className="error-panel-container">
                            <p>
                                Đã có lỗi xảy ra khi load trang này. Liên hệ
                                team dev.
                            </p>
                        </div>
                    );
                }}
                onError={(error, info) => {
                    console.log("ERRROR RENDERING", error);
                    console.log("ERRROR RENDERING INFO:", info);
                }}
            >
                <Outlet />
            </ErrorBoundary>
        ) : (
            <Sentry.ErrorBoundary
                fallback={() => (
                    <div className="error-panel-container">
                        <p>
                            Đã có lỗi xảy ra khi load trang này. Liên hệ team
                            dev.
                        </p>
                    </div>
                )}
                showDialog={true}
                key={location.pathname}
                dialogOptions={{
                    title:
                        "Oops, Có lẽ bạn đã gặp lỗi. Team Dev xin nhỗi 🥲",
                    subtitle: "Team dev của chúng tôi đã được thông báo.",
                    subtitle2:
                        "Nếu có thể, bạn hãy mô tả các thao tác bạn đã thực hiện cho đến khi gặp lỗi này. ",
                    labelName: "Tên của bạn",
                    labelComments: "Chuyện gì đã xảy ra ?",
                    labelSubmit: "Báo lỗi",
                    labelClose: "Đóng",
                    successMessage:
                        "Chân thành cảm ơn đóng góp của quý người dùng. Chúng tôi sẽ cố gắng cải thiện hệ thống để tránh nhưng bất tiện như vầy trong tương lai",
                }}
            >
                <Outlet />
            </Sentry.ErrorBoundary>
        )
    }
    return (
        <>
            {username && userLoggedIn ? (
                <Layout className={"lms-layout"}>
                    <CustomHeader />
                    {username && (
                        <Layout hasSider>
                            <SideBar props={InAppNavItems} />
                            <Layout id={"lms-main-content-card"}>
                                <CustomBreadCrumbs />
                                <Card>
                                    <Content
                                        className="_lms-content"
                                        style={{
                                            padding: "0 24px",
                                            minHeight: "80vh",
                                            // borderBottom: "100vh",
                                        }}
                                    >
                                        {renderError()}
                                        <Tooltip title={"Nhấn vào để trở lại đầu trang"}>
                                            <BackTop className="backtop-icon-container">
                                                <FontAwesomeIcon
                                                    className="backtop-icon"
                                                    icon={faRocket}
                                                />
                                            </BackTop>
                                        </Tooltip>
                                    </Content>
                                </Card>
                            </Layout>
                        </Layout>
                    )}
                </Layout>
            ) : (
                <LoginRegisterPage />
            )}
        </>
    );
}

export default App;
