import { Field, Formik, FormikConfig } from "formik";
import React, { useState } from "react";
import { schema } from "normalizr";
import { Button, Form, Typography } from "antd";
import { Input } from "formik-antd";
import * as Yup from "yup";
import TextEditorField from "./TextEditorField";
import COLOR_SCHEME from "../../../constants/ThemeColor";
import { string } from "prop-types";
import TimePickerField from "../CustomTimePicker/CustomTimePickerField";
import JsonDisplay from "../../Util/JsonDisplay";
import { values } from "lodash";
import { getHourAndMinuteFromDate } from "../../Calendar/Utils/CalendarUtils";

// * Notes: Initial values of formik fields should never be undefined --> use Empty string "" or null instead
// *        Reason: undefined values is labeled as uncontrolled by Formik --> Formik will do nothing
const initialValues = {
	title: "",
	articleDetail: "",
	hour: new Date(),
};

/*
 * Demonstration of complicated validation with yup
 * we can use .test(testName,testErrorMessage,testFunction) to implement some really complicated validation
 * (Examples can be found at CalendarNewEventFormModal.tsx - validation of time Range)
 *   - testName: is to identify the test. Later tests with same name will override the former
 *   - testErrorMessage: the error message to display - kept inside the errors object of formik -> errors[fieldName]
 *   - testFunction: (value) => { validateThisValueWithSomeHardCoreLogic}
 *       Return: false if this value fails the test, true otherwise
 */
const validationSchema = Yup.object().shape({
	title: Yup.string()
		.min(4, "Too short!")
		.max(50, "Too Long!")
		.required("Required"),
	articleDetail: Yup.string()
		.min(12, "Too short!")
		.max(1000, "Too Long!")
		.required("Required")
		.test(
			"articleMustContainThinh",
			"Please put Thinh inside the text",
			(article) => {
				if (!article) return false;
				return !!article.includes("Thinh");
			}
		),
	hour: Yup.date().required("KHong de trong"),
});

const TextEditorSampleAndTest = () => {
	// * Display result for testing
	const [result, setResult] = useState("");

	const handleSubmit = (values: any) => {
		console.log("Subited values", values);
		console.log("DATE OF VALUE: ", getHourAndMinuteFromDate(values.hour));
		setResult(values);
	};

	return (
		<div>
			<Formik
				initialValues={initialValues}
				onSubmit={handleSubmit}
				validationSchema={validationSchema}
			>
				{({
					isValid,
					dirty,
					isSubmitting,
					isValidating,
					setSubmitting,
					submitForm,
					submitCount,
					errors,
					touched,
				}) => {
					return (
						<Form>
							<Input name={"title"} />
							<Field
								name={"articleDetail"}
								placeholder={"Type article content"}
								component={TextEditorField}
								label={"ashdiasd"}
							/>
							<Field
								name={"hour"}
								component={TimePickerField}
								label={"ashdiasd"}
							/>
							<Button
								htmlType={"submit"}
								onClick={submitForm}
								disabled={!isValid || !dirty}
							>
								Submit
							</Button>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
};

export default TextEditorSampleAndTest;

							{/* <JsonDisplay cardProps={{ title: "Values" }} obj={values} />
							<JsonDisplay cardProps={{ title: "Errors" }} obj={errors} />
							<JsonDisplay cardProps={{ title: "Touched" }} obj={touched} /> */}