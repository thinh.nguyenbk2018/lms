import SelectSubordinate from "../SelectSubordinate/SelectSubordinate";
import RoleAndPermission from "../RoleAndPermission/RoleAndPermission";

const PermissionContent = () => {

  return <>
    <SelectSubordinate />
    <RoleAndPermission />
  </>
}

export default PermissionContent;
