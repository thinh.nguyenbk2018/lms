import { Dropdown, Menu, Table, Tag } from "antd";
import moment from "moment";
import { Link } from "react-router-dom";
import { ROUTING_CONSTANTS } from "../../navigation/ROUTING_CONSTANTS";
import { ScopeTag, TagDetails, useDeleteGradeTagMutation, useGradeMutation, usePubicGradeMutation } from "../../redux/features/Grade/GradeApi";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { DATE_TIME_FORMAT } from "@utils/TimeStampHelper/TimeStampHelper";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import { useState } from "react";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import { confirmationModal } from "@components/Modal/DeleteConfirmation";

const GradeManagement = ({
    scope,
    scopeId,
    tags,
    isLoading
}: { scope: ScopeTag, scopeId: number, tags: TagDetails[], isLoading: boolean }) => {
    const [grade, { data, isSuccess }] = useGradeMutation();
    const [publicGrade] = usePubicGradeMutation()
    const [deleteGrade] = useDeleteGradeTagMutation();

    const [pageSize, setPageSize] = useState<number>(DEFAULT_PAGE_SIZE);
    const [page, setPage] = useState<number>(1);

    const calculateGrade = (tagId: number) => {
        grade({ tagId })
            .unwrap()
            .then(response => {
                AntdNotifier.success("Tính điểm thành công")
            })
            .catch(error => {
                AntdNotifier.error(error.data.message);
            })
    }

    const publicGradeTag = (tagId: number, isPublic: boolean) => {
        publicGrade({ tagId, isPublic })
            .then(response => {
                AntdNotifier.success(isPublic ? "Hiện điểm thành công" : "Ản điểm thành công")
            })
            .catch(error => {
                AntdNotifier.error(error.data.message);
            })
    }

    const deleteGradeTag = (tagId: number) => {
        deleteGrade(tagId)
            .then(response => {
                AntdNotifier.success("Xóa cột điểm thành công")
            })
            .catch(error => {
                AntdNotifier.error(error.data.message);
            })
    }

    const renderMenu = (tag: TagDetails) => (
        <Menu>
            <PermissionRenderer permissions={[PermissionEnum.UPDATE_GRADE_TAG]}>
                <Menu.Item key={"1"} onClick={() => publicGradeTag(tag.id, !tag.public)}>
                    {tag.public ? "Ẩn điểm" : "Hiện điểm"}
                </Menu.Item>
            </PermissionRenderer>
            <PermissionRenderer permissions={[PermissionEnum.CALCULATE_GRADE_TAG]}>
                <Menu.Item key={"2"} onClick={() => calculateGrade(tag.id)}>
                    Tính điểm
                </Menu.Item>
            </PermissionRenderer>
            {!tag.primitive &&
                <>
                    <PermissionRenderer permissions={[PermissionEnum.VIEW_DETAIL_GRADE_TAG]}>
                        <Menu.Item key={"0"}>
                            <Link to={
                                scope === "CLASS" ?
                                    ROUTING_CONSTANTS.UPDATE_GRADE_FORMULA_IN_CLASS.replace(":classId", scopeId.toString()).replace(":tagId", tag.id.toString()) :
                                    ROUTING_CONSTANTS.UPDATE_GRADE_FORMULA_IN_COURSE.replace(":courseId", scopeId.toString()).replace(":tagId", tag.id.toString())
                            }>
                                Chi tiết
                            </Link>
                        </Menu.Item>
                    </PermissionRenderer>
                    <PermissionRenderer permissions={[PermissionEnum.DELETE_GRADE_TAG]}>
                        <Menu.Item key={"1"} danger onClick={() => confirmationModal("cột điểm", tag.title, () => {
                            deleteGradeTag(tag.id)
                        })}>
                            Xoá cột điểm
                        </Menu.Item>
                    </PermissionRenderer>
                </>
            }

        </Menu>
    );

    const columns = [
        {
            title: "Tên cột điểm",
            dataIndex: "title",
            key: "title",
            width: "20%",
        },
        {
            title: "Loại",
            dataIndex: "primitive",
            key: "type",
            width: "10%",
            align: "center" as const,
            render: (isPrimitive: boolean) => isPrimitive ? <Tag color="blue">Điểm gốc</Tag> :
                <Tag color="green">Điểm tính</Tag>
        },
        {
            title: "Ngày cuối cập nhật công thức",
            dataIndex: "updatedAt",
            key: "updatedAt",
            width: "20%",
            align: "center" as const,
            render: (updatedAt: Date) => updatedAt ? moment(updatedAt).format(DATE_TIME_FORMAT) :
                <Tag color="blue">Điểm gốc, không có công thức</Tag>
        },
        {
            title: "Lần cuối cùng tính điểm",
            dataIndex: "gradedAt",
            key: "gradedAt",
            width: "20%",
            align: "center" as const,
            render: (gradedAt: Date) => gradedAt ? moment(gradedAt).format(DATE_TIME_FORMAT) :
                <Tag color="red">Chưa tính điểm</Tag>
        },
        {
            title: "Hiển thị cho học viên",
            dataIndex: "public",
            key: "isPublic",
            width: "20%",
            align: "center" as const,
            render: (isPublic: boolean) => isPublic ? <Tag color="cyan">Hiển thị</Tag> : <Tag color="orange">Ẩn</Tag>
        },
        {
            title: "",
            key: "aciton",
            with: "5%",
            render: (tag: TagDetails) =>
                <PermissionRenderer permissions={[PermissionEnum.DELETE_GRADE_TAG, PermissionEnum.UPDATE_GRADE_TAG, PermissionEnum.CALCULATE_GRADE_TAG, PermissionEnum.VIEW_DETAIL_GRADE_TAG]}>
                    <Dropdown overlay={renderMenu(tag)} placement="bottomRight">
                        <a className="ant-dropdown-link" href="#" onClick={e => e.preventDefault()}>
                            Hành động
                        </a>
                    </Dropdown>
                </PermissionRenderer>
        }
    ];

    // We use frontend paging here because this page doesn't contain any searching
    return <Table
        loading={isLoading}
        columns={columns || []}
        dataSource={tags}
        rowKey="id"
        pagination={{
            pageSize: pageSize,
            current: page,
            showSizeChanger: true, onShowSizeChange: (current: number, size: number) => {
                setPageSize(size);
                setPage(page);
            },
            onChange: (page: number, size: number) => {
                setPage(page);
            }
        }}
        scroll={{ x: true }}
    />
}

export default GradeManagement;