import { useEffect, useState } from "react";
import TodoComponent, { Todo } from "./TodoComponent";
import axios, { AxiosError } from "axios";

export const todoUrl = "http://localhost:8080/api/v1/todos";

const TodoList = () => {
    const [todoList, setTodoList] = useState<Todo[]>([]);
    const [todoListError, setTodoListError] = useState<string>("");
    const fetchTodoList = async () => {
        try {
            const { data } = await axios.get(todoUrl);
            // console.log("Fetched todos as: ", data);
            setTodoList(data);
        } catch (err: any) {
            // console.log("HUHUHU: ERROR: TODO: ", err);
            setTodoListError(err.message);
        }
    };
    useEffect(() => {
        fetchTodoList();
    }, []);

    return (
        <div>
            <h1
                className="todo-list-title"
                style={{
                    color: "#B25068",
                    fontSize: "2rem",
                    fontWeight: "bolder",
                }}
            >
                List of Todo
            </h1>
            <div
                className="todo-list-container"
                style={{
                    display: "flex",
                    gap: "1rem",
                }}
            >
                {!todoList || todoList.length === 0 ? (
                    <em>No to do available here</em>
                ) : (
                    <div>
                        {todoList.map((item, index) => {
                            return <TodoComponent {...item} key={item.title} />;
                        })}
                    </div>
                )}
            </div>
            {todoListError && (
                <div className="error-contaitner">{todoListError}</div>
            )}
        </div>
    );
};

export default TodoList;
