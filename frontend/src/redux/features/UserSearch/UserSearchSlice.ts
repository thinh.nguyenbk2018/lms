import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SliceStateWithSearch
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "../../store/store";


const USER_SEARCH_SLICE_KEY = "user_search";

const initialState: SliceStateWithSearch = {
    ...generateInitialSearchSlice()
}

export const userSearchSlice = createSliceEnhancedWithSearch({
    name: USER_SEARCH_SLICE_KEY,
    initialState,
    reducers: {},
});

export const userSearchSelector = {
    // * Select the criteria to pass into the query
    selectSearchCriteria: (state: RootState) => state.userSearch.searchCriteria
}

export const {changeCriteria} = userSearchSlice.actions;
