import { Todo } from "@components/Todo/TodoComponent";
import { rest } from "msw";
import { todoUrl } from "@components/Todo/TodoList";

const todoResponse: Todo[] = [
    {
        title: "Task1",
        content: "Complete this first",
    },
    {
        title: "Task2",
        content: "This task after first task",
    },
];

const todoHandlers = [
    rest.get<any, any, Todo[]>(todoUrl, (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(todoResponse));
    }),
];

export default todoHandlers; 
