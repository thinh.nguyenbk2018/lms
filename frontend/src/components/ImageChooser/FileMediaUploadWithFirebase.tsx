import {LoadingOutlined, UploadOutlined} from "@ant-design/icons";
import {Alert, Button, Form, Modal, Space, Spin, Typography} from "antd";
import {getDownloadURL, ref, uploadBytesResumable} from "firebase/storage";
import React, {useEffect, useRef, useState} from "react";
import ReactPlayer from "react-player";
import {useAppSelector} from "../../hooks/Redux/hooks";
import {authSelectors} from "../../redux/features/Auth/AuthSlice";
import {default as antdNotifier, default as AntdNotifier} from "../../utils/AntdAnnouncer/AntdNotifier";
import {storage} from "../../utils/FirebaseSDK/firebase";
import FilePreview from "../FilePreview/FilePreview";
import {GENERAL_FILE_TYPES, getGeneralFileType, isPlayableMedia} from "../FilePreview/Utils/FileUtils";

import {tenantSelector} from "../../redux/features/Tenant/TenantSlice";
import "./FileMediaUploadWithFirebase.less";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

export type FileUploadProps = {
	/** will show smaller version of image as review if true else show icon .png or .jpg,... */
	showPreview?: boolean;
	showImagePreviewInsteadOfIcon?: boolean;
	/** what to do beside uploading the image to firebase for storage. This function will receive a link of the uploaded media*/
	onUploadSuccess?: (fileUrl: string) => void;
	onFileChange?: (file: File | undefined) => void;
	onChange?: (fileOrUrl: File | string) => void;
	onBlur?: Function;
	uploadButtonMessage?: string;
	savePathPrefix?: string;
};
const FileMediaUploadWithFirebase = ({
	uploadButtonMessage,
	showPreview,
	showImagePreviewInsteadOfIcon = true,
	onUploadSuccess,
	onFileChange,
	onBlur,
}: FileUploadProps) => {
	const tenantInfo = useAppSelector(tenantSelector.selectTenantInfo);
	const userInfo = useAppSelector(authSelectors.selectCurrentUser);
	const userSuccess = useAppSelector(authSelectors.selectLoginSuccess);
	const [previewModalVisible, setPreviewModalVisible] = useState(false);
	// * Tenant info so the file will be uploaded to corect bucket
	const domain = tenantInfo.domain;
	// * The Image file
	const [file, setFile] = useState<File | undefined>(undefined);
	const [playable, setPlayable] = useState<boolean>(false);
	const [playURL, setPlayURL] = useState<string>("");
	// * The image URL if uploaded to firebase storage successfully
	const [fileURL, setFileURL] = useState("");
	// * show Spinning state
	const [isLoading, setLoading] = useState(false);

	useEffect(() => {
		handleUpload();
		const isPLayable = isPlayableMedia(file);
		if (isPLayable) {
			AntdNotifier.success("Playable");
			setPlayable(true);
			const playUrl = URL.createObjectURL(file!);
			setPlayURL(playUrl);
		}
		onFileChange?.(file);
		onBlur?.();
		return () => {};
	}, [file]);

	const calculateFileFolder = (file: File) => {
		const fileType = getGeneralFileType(file);
		let inTenantFilePath = "";
		if (fileType === GENERAL_FILE_TYPES.UNKNOWN) {
			inTenantFilePath = "general";
		}
		inTenantFilePath = `${fileType}s`;
		return `${domain}/${
			userSuccess ? userInfo?.username : "default_user"
		}/${inTenantFilePath}/`;
	};

	const fileInputRef = useRef<HTMLInputElement>(null);

	const handleChange:
		| React.ChangeEventHandler<HTMLInputElement>
		| undefined = async (e) => {
		try {
			if (e.target.files?.[0]) {
				await setFile(e.target.files?.[0]);
				setLoading(true);
				return;
			} else {
				antdNotifier.warn(`Empty or Null file`);
				return;
			}
		} catch (e) {
			antdNotifier.warn(`Có lỗi khi xử lý file`);
		}
	};

	const handleUpload = () => {
		if (!file) {
			return;
		}
		console.log(
			`IMAGE INFO NAME | TYPE | SIZE : ${file.name} | ${file.type} | ${file.size}`
		);
		const storageRef = ref(
			storage,
			`${calculateFileFolder(file)}/${file.name}`
		);

		const uploadTask = uploadBytesResumable(storageRef, file);
		uploadTask.on(
			"state_changed",
			(snapshot) => {
				const progress =
					(snapshot.bytesTransferred / snapshot.totalBytes) * 100;
				console.log("Upload is " + progress + "% done");
				switch (snapshot.state) {
					case "paused":
						console.log("Upload is paused");
						break;
					case "running":
						console.log("Upload is running");
						break;
				}
			},
			(error) => {
				AntdNotifier.error("Không load đuợc file", error.message);
			},
			() => {
				getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
					setFileURL(downloadUrl);
					onUploadSuccess?.(downloadUrl);
					setLoading(false);
				});
			}
		);
	};
	return (
		<Form.Item name={"avatar"}>
			{isLoading ? (
				<Spin indicator={antIcon}>
					<Alert
						message="File đang đuợc tải lên"
						description="Đợi chờ là hạnh phúc"
						type="info"
					/>
				</Spin>
			) : (
				<>
					<Space direction={"vertical"}>
						<Space>
							<Button
								type={"primary"}
								icon={<UploadOutlined />}
								onClick={() => fileInputRef.current?.click()}
							>
								{uploadButtonMessage || "Tải file"}{" "}
							</Button>
							{playable && (
								<Button onClick={() => setPreviewModalVisible(true)}>
									Play Preview
								</Button>
							)}
						</Space>
						<input
							ref={fileInputRef}
							type={"file"}
							onChange={handleChange}
							style={{ display: "none" }}
						/>
						{showPreview ? (
							<>
								{
									<FilePreview
										size={50}
										file={file}
										showName={true}
										showImageInsteadOfIcon={showImagePreviewInsteadOfIcon}
									/>
								}
								{
									<Modal
										className={"file-preview-modal-container"}
										title="Basic Modal"
										visible={previewModalVisible}
										onOk={() => setPreviewModalVisible(false)}
										onCancel={() => setPreviewModalVisible(false)}
									>
										{playURL ? (
											<>
												<div className={"file-preview-sub-container"}>
													<ReactPlayer
														url={playURL}
														controls={true}
														width={473}
													/>
												</div>
											</>
										) : (
											<Typography.Text type={"danger"}>
												Review cho loại file này chưa đuợc hỗ trợ
											</Typography.Text>
										)}
									</Modal>
								}
							</>
						) : (
							<></>
						)}
					</Space>
				</>
			)}
		</Form.Item>
	);
};

export default FileMediaUploadWithFirebase;
