import { Checkbox, Space } from "antd";

import "./CalendarChoiceBox.less";

const calendarList = [
	{
		id: "123",
		name: "Class 1",
	},
	{
		id: "a",
		name: "Class 2",
	},
	{
		id: "3",
		name: "class 3",
	},
	{
		id: "avacv",
		name: "Personal",
	},
];

const CalendarChoiceBox = () => {
	const listToDisplay: typeof calendarList = calendarList;
	const onChoiceChange = (chosenCalendarIds: (number | boolean | string)[]) => {
		console.log("Choosen Calendars: ", chosenCalendarIds);
	};
	return (
		<div className="calendar-choice-box__main_container">
			<div className="calendar-choice-box__title">CALENDARS</div>
			<Checkbox.Group
				className="calendar-choice-box__choice__list"
				onChange={onChoiceChange}
			>
				{listToDisplay.map((item, index) => {
					return (
						<div className="calendar-choice-box__item_container" key={item.id}>
							<Checkbox value={item.id}></Checkbox>
							<span>{item.name}</span>
						</div>
					);
				})}
			</Checkbox.Group>
		</div>
	);
};

export default CalendarChoiceBox;
