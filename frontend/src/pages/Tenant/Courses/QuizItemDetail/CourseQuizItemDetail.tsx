import {Row, Space, Typography} from "antd";
import React from 'react';
import {useNavigate, useParams} from "react-router-dom";
import EditButton from "../../../../components/ActionButton/EditButton";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import {useAppDispatch} from "@hooks/Redux/hooks";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";
import {useGetQuizDetailQuery} from "@redux/features/Course/CourseAPI";
import {courseContentActions} from "@redux/features/Course/CourseContentSlice";
import LoadingPage from "../../../UtilPages/LoadingPage";
import {AddQuizItemModal} from "../CourseContent/CourseContentPage";
import "./CourseQuizItemDetail.less";
import InformationRow from "@components/InformationRow/InformationRow";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";

const CourseQuizItemDetailPage = () => {
    const {
        courseDetail,
        isCourseDetailFetching,
        isCourseDetailSuccess,
    } = useCourseSidebar();

    const {quizItemId, chapterId, courseId} = useParams();

    const {
        data: quizData,
        isSuccess,
        isFetching,
    } = useGetQuizDetailQuery(
        {
            courseId: Number(courseId),
            quizId: Number(quizItemId),
        },
        {
            skip:
                isNaN(Number(quizItemId)) ||
                isNaN(Number(courseId)),
        }
    );

    // * Redux store
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    if (isCourseDetailSuccess && isSuccess) {
        return (
            <>
                <ContentHeader
                    title={`${quizData.title}`}
                    action={
                        <Space>
                            <EditButton
                                onClick={() => {
                                    dispatch(
                                        courseContentActions.updatingActivity({
                                            chapterToEdit: Number(chapterId),
                                            activity: quizData,
                                        })
                                    );
                                }}
                            />
                        </Space>
                    }
                />
                <Row justify="center">
                    <div className="quiz-item-info-card quiz-item-info-card-class">
                        <InformationRow title={'Mô tả'} value={quizData.description}/>
                        <InformationRow title={'Đề'} value={<div
                            className="field-value"
                            onClick={() => {
                                navigate(
                                    replacePathParams(
                                        ROUTING_CONSTANTS.COURSE_EXAM_DETAIL,
                                        [":courseId", courseId || ""],
                                        [":testId", quizData?.examId]
                                    )
                                );
                            }}
                        ><Typography.Link>{quizData?.examTitle}</Typography.Link></div>}/>
                        <InformationRow title={'Tên cột điểm'} value={quizData.tag}/>
                    </div>
                </Row>
                <AddQuizItemModal/>
            </>
        );
    } else if (isCourseDetailFetching || isFetching) {
        return <LoadingPage/>;
    } else {
        return <></>;
    }
};

export default CourseQuizItemDetailPage;
