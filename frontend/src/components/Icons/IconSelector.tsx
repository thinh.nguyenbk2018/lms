import {
    ApartmentOutlined,
    BellFilled,
    BellOutlined,
    BellTwoTone,
    BookTwoTone,
    CaretDownOutlined,
    CheckCircleOutlined,
    ClusterOutlined,
    DashboardOutlined,
    DeleteOutlined,
    EditOutlined,
    FormOutlined,
    HighlightOutlined,
    HomeOutlined,
    IdcardOutlined,
    ImportOutlined,
    InboxOutlined,
    MenuOutlined,
    MoneyCollectOutlined,
    PieChartOutlined,
    PlusCircleOutlined,
    PlusOutlined,
    ProfileOutlined,
    QuestionOutlined,
    SearchOutlined,
    SettingFilled,
    SettingOutlined,
    SmileOutlined,
    SnippetsOutlined,
    TableOutlined,
    TabletOutlined,
    UserOutlined,
    WarningOutlined,
    WechatOutlined,
} from '@ant-design/icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import COLOR_SCHEME from "../../constants/ThemeColor";
import {
	faBarsProgress,
	faBook,
	faBookmark,
	faCertificate,
	faCoffee,
	faFlask,
	faFolderClosed,
	faGear,
	faGraduationCap,
	faHome,
	faSitemap,
	faUserTie,
	faAddressCard,
	faListCheck, faUser,
} from "@fortawesome/free-solid-svg-icons";
import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import React from 'react';

const Icons = {
	QuestionOutlined: <QuestionOutlined />,
	DashboardOutlined: <DashboardOutlined />,
	SmileOutlined: (
		<SmileOutlined
			style={{
				color: COLOR_SCHEME.primary,
				fontSize: "1.5em",
				margin: "0 .3em 0 0",
			}}
		/>
	),
	FormOutlined: <FormOutlined />,
	TabletOutlined: <TabletOutlined />,
	ProfileOutlined: <ProfileOutlined />,
	CheckCircleOutlined: <CheckCircleOutlined />,
	WarningOutlined: <WarningOutlined />,
	UserOutlined: <UserOutlined />,
	HighlightOutlined: <HighlightOutlined />,
	TableOutlined: <TableOutlined />,
	HomeOutlined: <FontAwesomeIcon icon={faHome} />,
	StudentOutlined: <FontAwesomeIcon icon={faUser} />,
	CheckSquareOutlined: <FontAwesomeIcon icon={faCoffee} />,
	BellOutlined: <BellOutlined />,
	BellFilled: <BellFilled />,
	SettingFilled: (
		<SettingFilled style={{ fontSize: "1.5em", color: COLOR_SCHEME.primary }} />
	),
	BellTwoTone: <BellTwoTone />,
	DeleteOutLined: (
		<DeleteOutlined style={{ color: "#ff4d4f", fontSize: "1.5em" }} />
	),
	PlusCircleOutlined: (
		<PlusCircleOutlined
			style={{ color: COLOR_SCHEME.primary, fontSize: "1.5em" }}
		/>
	),
	WeChatOutlined: (
		<WechatOutlined
			style={{ color: COLOR_SCHEME.primary, fontSize: "1.5em" }}
		/>
	),
	CaretDownOutlined: <CaretDownOutlined />,
	SearchOutlined: <SearchOutlined />,
	PlusOutlined: <PlusOutlined />,
	PieChartOutlined: <PieChartOutlined />,
	ApartmentOutlined: <ApartmentOutlined />,
	InboxOutlined: <InboxOutlined />,
	SnippetsOutlined: <SnippetsOutlined />,
	ClusterOutlined: <ClusterOutlined />,
	IdcardOutlined: <IdcardOutlined />,
	MenuOutlined: <MenuOutlined />,
	CourseOutlined: <FontAwesomeIcon icon={faBookmark} />,
	ProgramOutlined: <FontAwesomeIcon icon={faBarsProgress} />,
	TextBookOutlined: <FontAwesomeIcon icon={faBook} />,
	ClassOutlined: <FontAwesomeIcon icon={faGraduationCap} />,
	FileManagerOutlined: <FontAwesomeIcon icon={faFolderClosed} />,
	CalendarOutlined: <FontAwesomeIcon icon={faCalendarCheck} />,
	CertificateOutlined: <FontAwesomeIcon icon={faCertificate} />,
	TestPageOutlined: <FontAwesomeIcon icon={faFlask} />,
	FaSettingsOutlined: <FontAwesomeIcon icon={faGear} />,
	SiteMapOutlined: <FontAwesomeIcon icon={faSitemap} />,
	UserTieOutlined: <FontAwesomeIcon icon={faUserTie} />,
	AddressCardOutlined: <FontAwesomeIcon icon={faAddressCard} />,
	ListCheckOutlined: <FontAwesomeIcon icon={faListCheck} />,
	MoneyCollectOutlined: <MoneyCollectOutlined />,
	SettingOutlined: <SettingOutlined />,
	EditOutlined: <EditOutlined />,
	ImportOutlined: <ImportOutlined />
};
const IconSelector = ({
	type,
	onClick,
	...rest
}: {
	type: string;
	onClick?: () => void;
}) => {
	const getIcon = (type: string) => {
		// Default Icon when not found
		let comp = <QuestionOutlined />;

		let typeNew = type;

		// Default is Outlined when no theme was appended (ex: 'smile')
		if (!typeNew.match(/.+(Outlined|Filled|TwoTone)$/i)) {
			typeNew += "Outlined";
		}

		// If found by key then return value which is component
		const found = Object.entries(Icons).find(
			([k]) => k.toLowerCase() === typeNew.toLowerCase()
		);
		if (found) {
			[, comp] = found;
		}

		return comp;
	};

	return (
		<span onClick={() => (onClick ? onClick() : {})} {...rest}>
			{getIcon(type)}
		</span>
	);
};

export default React.memo(IconSelector);
