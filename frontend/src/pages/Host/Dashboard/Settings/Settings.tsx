import ContentHeader from "@components/ContentHeader/ContentHeader"
import { List, Typography } from "antd";
import { HOST_ROUTING_CONSTANT } from "../../../../navigation/ROUTING_CONSTANTS";
import { ReactNode } from "react";
import { Link } from "react-router-dom";

const Settings = () => {

  let dataMenu: { title: string, actions?: ReactNode[] }[] = [
    {
      title: "Cá nhân hóa hệ thống",
      actions: [
        <Link to={HOST_ROUTING_CONSTANT.PERSONALIZED}>
          Chỉnh sửa
        </Link>
      ]
    }
  ]

  return <>
    <ContentHeader title="Cài đặt hệ thống" />
    <List
      className="demo-loadmore-list"
      itemLayout="horizontal"
      dataSource={dataMenu}
      renderItem={item => (
        <List.Item
          actions={item.actions}
        >
          <List.Item.Meta
            title={<Typography.Title level={5}>{item.title}</Typography.Title>}
            description="Chỉnh sửa logo, Tên thương hiệu, ảnh banner."
          />
        </List.Item>
      )}
    />
  </>
}

export default Settings;