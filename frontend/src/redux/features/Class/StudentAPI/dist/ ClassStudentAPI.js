"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.useGetStudentDetailQuery = exports.useGetStudentsQuery = void 0;
var CentralAPI_1 = require("../../CentralAPI");
var TransformParameter_1 = require("../../APIUtils/TransformParameter");
var MemberApiWithInvalidateTags = CentralAPI_1.apiSlice.enhanceEndpoints({
    addTagTypes: ["ClassStudent"]
});
var classStudentApi = MemberApiWithInvalidateTags.injectEndpoints({
    endpoints: function (build) { return ({
        getStudents: build.query({
            query: function (_a) {
                var classId = _a.classId, searchCriteria = _a.searchCriteria;
                return "classes/" + classId + "/students?" + TransformParameter_1.transformSearchQuery(searchCriteria);
            },
            providesTags: function (result) {
                return result
                    ? __spreadArrays(result.listData.map(function (_a) {
                        var userId = _a.userId;
                        return ({ type: "ClassStudent", userId: userId });
                    }), [
                        { type: "ClassStudent", id: "LIST" },
                    ]) : [{ type: "ClassStudent", id: "LIST" }];
            }
        }),
        getStudentDetail: build.query({
            query: function (_a) {
                var classId = _a.classId, votingId = _a.votingId;
                return {
                    url: "classes/" + classId + "/members/" + votingId,
                    method: "GET"
                };
            },
            providesTags: function (_res, _err, args) { return [
                { type: "ClassStudent", id: args.votingId },
            ]; }
        })
    }); },
    overrideExisting: false
});
exports.useGetStudentsQuery = classStudentApi.useGetStudentsQuery, exports.useGetStudentDetailQuery = classStudentApi.useGetStudentDetailQuery;
