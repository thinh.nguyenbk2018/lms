import { Button, Col, Collapse, Input, Modal, Row, } from "antd";
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../../../hooks/Redux/hooks";
import { addRole } from "../../../../../redux/features/Permission/PermissonSlice";
import { Role, useGetAllRolesQuery } from "../../../../../redux/features/Permission/RolesAPI";
import IconSelector from "../../../../Icons/IconSelector";
import CustomEmpty from "../../../../Util/CustomEmpty";
import RoleDetails from "./RoleDetails";

const AddRole = () => {
  let [showModal, setShowModal] = useState(false);

  let { data: getAllRolesResponse } = useGetAllRolesQuery();

  let roles = useAppSelector(state => state.grantPermission.roles);

  let [searchValue, setSearchValue] = useState('');

  let dispatch = useAppDispatch();

  let addableRoles = getAllRolesResponse?.roles.filter(role =>
    role.title.toLowerCase().includes(searchValue.toLowerCase()) && !roles.map(r => r.id).includes(role.id)
  ) || [];

  const renderRoleHeader = (role: Role, index: number) => (
    <Row style={{ width: '100%' }} align="middle">
      <Col lg={20} md={20}>
        {role.title}
      </Col>
      <Col lg={4} md={4}>
        <Row justify="end">
          <Button type="primary" onClick={() => dispatch(addRole({
            ...role,
            validFrom: new Date(),
            expiresAt: new Date()
          }))}>Thêm</Button>
        </Row>
      </Col>
    </Row>
  )


  return <Row justify="center" style={{ marginTop: 15 }}>
    <Button type="dashed" icon={<IconSelector type="PlusOutlined" />} size="middle" onClick={() => setShowModal(true)}>
      Thêm vai trò
    </Button>
    <Modal
      title="Thêm vai trò"
      width={'50%'}
      visible={showModal}
      onOk={() => setShowModal(false)}
      onCancel={() => setShowModal(false)}
      zIndex={1}
    >
      <Input.Search
        placeholder="Nhập role bạn muốn thêm"
        size="large"
        style={{ marginBottom: 15 }}
        onChange={({ currentTarget: { value } }) => setSearchValue(value)}
      />
      <Row justify="center">
        <Collapse style={{ width: '100%' }}>
          {addableRoles.map((role, index) => (
            <Collapse.Panel
              key={index}
              header={renderRoleHeader(role, index)}
            >
              <RoleDetails roleId={role.id} />
            </Collapse.Panel>
          ))}
          {addableRoles.length === 0 && <CustomEmpty />}
        </Collapse>
      </Row>
    </Modal>
  </Row>
}

export default AddRole;