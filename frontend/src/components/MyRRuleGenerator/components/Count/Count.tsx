import React from 'react';
import PropTypes from "prop-types";
import {Col, Input, Row, Space} from "antd";
import numericalFieldHandler from "../../utils/numericalFieldHandler";
import translateLabel from "../../utils/translateLabel";

interface CountProps {
    id: string,
    count: number,
    handleChange: () => {},
    translations: {} | (() => {}),
}

const Count = (props: CountProps) => {
    const {id,count,handleChange,translations} = props;
    return (
        <Col>
            <Row gutter={[5, 10]}>
                <Space>
                    <Input
                        id={`${id}-count`}
                        name="count"
                        aria-label="Count"
                        className="form-control"
                        value={count}
                        onChange={numericalFieldHandler(handleChange)}
                    />
                    {translateLabel(translations, 'count')}
                </Space>
            </Row>
        </Col>
    );
};

export default Count;