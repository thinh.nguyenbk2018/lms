import { Col, Form, Input, Row, Steps, Switch } from "antd";
import { forwardRef, Ref, useEffect, useImperativeHandle } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks/Redux/hooks";
import { gradeFormulaSeletor, resetGradeFormula, Tag, updateIsPublic, updateTagTitle } from "../../redux/features/Grade/GradeFormulaSlice";
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import GradeFormulaGenerator from "./GradeFormulaGenerator/GradeFormulaGenerator";
import TagsSelector from "./TagsSelector/TagsSelector";
import { create, all } from 'mathjs';

const math = create(all, {
  number: "Fraction",
  predictable: true,
})



export interface GradeFormulaHandler {
  getFormula: () => { formula: string, expression: string, useTags: number[], isPublic: boolean } | false
}

interface GradeFormulaProps {
  allTags: Tag[],
  isLoading: boolean,
  currentStep: number,
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>
}

const GradeFormula = ({ allTags, isLoading, currentStep, setCurrentStep }: GradeFormulaProps, ref: Ref<GradeFormulaHandler>) => {

  let dispatch = useAppDispatch();

  let formula = useAppSelector(gradeFormulaSeletor.formulaSelector);
  let isPublic = useAppSelector(gradeFormulaSeletor.isPublicSelector);

  useEffect(() => {
    return () => {
      dispatch(resetGradeFormula())
    }
  }, [dispatch])

  useImperativeHandle(ref, () => ({
    getFormula: () => {
      if (!tagTitle) {
        AntdNotifier.error("Không được để trống tên cột điểm");
        return false;
      }
      if (formula.length === 0) {
        AntdNotifier.error("Công thức tính điểm không đươc để trống");
        return false;
      }
      let expression = formula.map(ele => {
        if (ele.type === "Bracket") return ele.value;
        else if (ele.type === "Number") return ele.value.toString();
        else if (ele.type === "Operator") return ele.value;
        else if (ele.type === "Tag") return "tag_" + ele.value.id;
        else return; 
      }).join("   ");
      try {
        let node = math.parse(expression);
        if (node.filter(n => n.type === "FunctionNode").length > 0) {
          AntdNotifier.error("Vui lòng không lượt bớt dấu nhân '*' trong biểu thức");
          return false;
        }
        let variables = new Set(node.filter(n => n.type === "SymbolNode").map(n => (n as math.SymbolNode).name));
        let mapVariables: any = {};
        [...variables].forEach((v, index) => { mapVariables[v] = index + 1 })
        let code = node.compile();
        console.log("1: ", code.evaluate(mapVariables));
        console.log("2: ", math.simplify(expression));

        return {
          formula: JSON.stringify(formula),
          expression,
          useTags: [...variables].map(v => parseInt(v.replace("tag_", ""))),
          isPublic
        }
      } catch (e) {
        if ((e as any).message === 'Division by Zero') {
          AntdNotifier.error("Biểu thức có lỗi chia cho 0");
        }
        else {
          AntdNotifier.error("Biểu thức có lỗi toán học (chú ý lỗi chi cho 0), bạn vui lòng kiểm tra lại");
        }
        console.error("Lỗi:", e)
        return false;
      }
    }
  }))

  let tagTitle = useAppSelector(gradeFormulaSeletor.titleSelector);

  const steps = [
    {
      title: 'Chọn các cột điểm',
      content: <TagsSelector allTags={allTags} isGettingTags={isLoading} />,
    },
    {
      title: "Viết công thức",
      content: <GradeFormulaGenerator />
    }
  ];

  return <>
    <Row justify="center" style={{ margin: "20px 15px" }}>
      <Col>
        <Form.Item label="Tên cột điểm" required>
          <Input placeholder="Tên cột điểm" value={tagTitle} onChange={({ target: { value } }) => dispatch(updateTagTitle(value))} />
        </Form.Item>
        <Form.Item label="Hiển thị cho học viên">
          <Switch checked={isPublic} onChange={(checked) => dispatch(updateIsPublic(checked))} />
        </Form.Item>
      </Col>
    </Row>
    <Row justify="center" style={{ margin: "20px 15px" }}>
      <Col span={16}>
        <Steps current={currentStep} size="small" >
          {steps.map(item => (
            <Steps.Step key={item.title} title={item.title} onStepClick={setCurrentStep} />
          ))}
        </Steps>
      </Col>
    </Row>
    <div className="steps-content">{steps[currentStep].content}</div>
  </>
}

export default forwardRef(GradeFormula);