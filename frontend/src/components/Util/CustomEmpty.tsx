import {Empty} from "antd";


const CustomEmpty = ({description}:{description?: string}) => {
  return <Empty
    style={{ height: 120, border: "none", display: "flex", justifyContent: "center", flexDirection: "column", alignItems: "center", width: "100%" }}
    image={Empty.PRESENTED_IMAGE_SIMPLE}
    description={description}
  />
}

export default CustomEmpty;