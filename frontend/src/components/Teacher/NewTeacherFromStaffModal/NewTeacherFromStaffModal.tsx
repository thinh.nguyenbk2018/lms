import { useAppSelector, useAppDispatch } from "@hooks/Redux/hooks";
import {
    TeacherAddToClassDto,
    useAddTeachersToClassMutation,
    useGetTeacherCandidatesForClassQuery,
} from "@redux_features/Class/TeacherAPI/ClassTeacherAPI";
import {
    classTeacherActions,
    classTeacherSelectors,
} from "../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";
import { useEffect } from "react";
import ModalWithSection from "@components/GenericComponents/Modals/ModalWithSections";
import TransientteacherCandidatesList from "./TransientTeacherList";
import TeacherCandidateList from "./TeacherCandidateList";
import AlreadyInClassteacherCandidatesList from "./AlreadyInClassTeacherList";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
// TODO: Add admin checking 
// TOOD: When adding new teacher. Should refetch the class candidates 
/**
 * classId - The class we want the model to fetch the data from
 */
const NewTeacherFromStaffModal = ({ classId }: { classId: number }) => {
    // * Redux states
    const visible = useAppSelector(
        classTeacherSelectors.selectIsAssigningTeacher
    );
    const {
        data: teacherCandidatesList,
        isSuccess,
        isError,
        isLoading,
    } = useGetTeacherCandidatesForClassQuery({ classId });

    const [addTeacherMutation] = useAddTeachersToClassMutation();
    const teacherCandidatesToAdd = useAppSelector(
        classTeacherSelectors.selectTeacherToAddToClass
    );

    const dispatch = useAppDispatch();

    // * Handlers
    const addNewTeachersToClass = () => {
        if (!teacherCandidatesToAdd) return;
        AntdNotifier.handlePromise(
            addTeacherMutation({
                classId,
                // * The below list is alredy filtered out the falsy stuffs so it should be ok to type cast with the above nullity check
                teacherIds: teacherCandidatesToAdd as TeacherAddToClassDto[],
            }).unwrap(),
            "Thêm giáo viên vào lớp",
            () => {  
                dispatch(classTeacherActions.clearTeacherCandidates());
            },
            () => {

            }
        );
    };
    const closeModal = () => {
        dispatch(classTeacherActions.closeModal());
    };

    useEffect(() => {
        if (!teacherCandidatesList?.staffs) return;
        dispatch(
            classTeacherActions.setTeacherCandidatesFromList(
                teacherCandidatesList?.staffs
            )
        );
    }, [teacherCandidatesList, isSuccess]);

    return (
        <ModalWithSection
            title="Thêm giáo viên"
            visible={visible}
            sections={[
                {
                    title: "1. Danh sách nhân viên",
                    body: <TeacherCandidateList />,
                },
                {
                    title: "2. Danh sách giáo viên sẽ được thêm vào",
                    body: <TransientteacherCandidatesList classId={classId}/>,
                },
                {
                    title: "3. Danh sách giáo viên hiện tại trong lớp học",
                    body: <AlreadyInClassteacherCandidatesList classId={classId}/>,
                },
            ]}
            onCancel={closeModal}
            onOk={addNewTeachersToClass}
            modalWidth={"large"}
            okText={'Cập nhật'}
            cancelText={'Hủy'}
        />
    );
};

export default NewTeacherFromStaffModal;
