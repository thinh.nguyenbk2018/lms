import {Avatar, Dropdown, Menu, Modal, Tag} from "antd";
import React, {useState} from "react";
import {Link, Navigate} from "react-router-dom";
import {STATUS_CODE_RESPONSE} from "../../../constants/StatusCode";
import {
    Staff,
    useDeleteAccountMutation,
    useDeleteStaffMutation,
    useGetAllStaffsQuery
} from "../../../redux/features/Staff/StaffAPI";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import CustomTable from "../../CustomTable/CustomTable";
import AccountStaffModal from "./AccountStaffModal";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {changeCriteria, staffsManagementSelector} from "@redux/features/Staff/StaffsManagementSlice";
import {extractPaginationInformation} from "@components/CustomTable/TableUtils";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import PermissionRenderer, {useHasPermission} from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {emailActions, emailSelector} from "@redux/features/Email/EmailSlice";
import {Form, FormItem, Input} from "formik-antd";
import {Field, Formik} from "formik";
import TextEditorField from "@components/CustomFields/TextEditorField/TextEditorField";
import GeneralFileUploadField from "@components/CustomFields/FileUploadField/GeneralFileUploadField";
import {useSendEmailMutation} from "@redux/features/Email/EmailAPI";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {defaultMail, SendEmailSchema} from "@components/Students/StudentsManagement";

const StaffsManagement = () => {

    const searchCriteria = useAppSelector(staffsManagementSelector.selectSearchCriteria);

    let { data: getAllStaffsResponse, isLoading, isError, error } = useGetAllStaffsQuery(searchCriteria);
    let [deleteAccount] = useDeleteAccountMutation();
    let [deleteStaff] = useDeleteStaffMutation();
    let [visibleAccountModal, setVisibleAccountModal] = useState(false);
    let [selectedStaff, setSelectedStaff] = useState<Staff>();

    let hasPermissionWithStaff = useHasPermission([PermissionEnum.VIEW_DETAIL_STAFF, PermissionEnum.DELETE_STAFF, PermissionEnum.ALLOCATE_ACCOUNT_STAFF, PermissionEnum.DELETE_ACCOUNT_STAFF])

    const dispatch = useAppDispatch();

    const showModal = useAppSelector(emailSelector.selectShowModal);

    const [chosenStaff, setChosenStaff] = useState({name: '', emailAddress: ''});

    const [sendEmail] = useSendEmailMutation();

    const deleteStaffAccount = (staff: Staff) => {
        setSelectedStaff(staff);
        Modal.confirm({
            title: `Bạn có chắc muốn xóa tài khoản của ${staff.firstName}`,
            content: `Sau khi xác nhận tài khoản của ${staff.firstName} sẽ bị xóa. Bạn có thể cấp tài khoản mới sau đó.`,
            okText: "Xác nhận",
            onOk: () => {
                return deleteAccount(staff.id)
                    .unwrap()
                    .then(result => {
                        AntdNotifier.success("Cập nhật tài khoản thành công")
                    })
                    .catch(error => {
                        console.log(error)
                        if (error?.data) {
                            AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá xóa tài khoản. Thử lại sau.")
                        } else {
                            AntdNotifier.error("Có lỗi trong quá xóa tài khoản. Thử lại sau.")
                        }
                    })
            }
        })
    }

    const onClickDeleteStaff = (staff: Staff) => {
        setSelectedStaff(staff);
        confirmationModal('nhân viên', `${staff.lastName} ${staff.firstName}`, () => {
            return deleteStaff(staff.id)
                .unwrap()
                .then(result => {
                    AntdNotifier.success(`Xóa nhân viên ${staff.lastName} ${staff.firstName} thành công`)
                })
                .catch(error => {
                    console.log(error)
                    if (error?.data) {
                        AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá xóa. Thử lại sau.")
                    } else {
                        AntdNotifier.error("Có lỗi trong quá xóa. Thử lại sau.")
                    }
                })
        });
    }

    const renderMenu = (staff: Staff) => (
        <Menu>
            <PermissionRenderer permissions={[PermissionEnum.VIEW_DETAIL_STAFF]}>
                <Menu.Item key="0">
                    <Link to={`/staffs/${staff.id}`}>
                        Chi tiết
                    </Link>
                </Menu.Item>
            </PermissionRenderer>
            <PermissionRenderer permissions={[PermissionEnum.ALLOCATE_ACCOUNT_STAFF]}>
            <Menu.Item key="1" onClick={() => {
                setSelectedStaff(staff);
                setVisibleAccountModal(true);
            }}>
                Cấp/Sửa tài khoản
            </Menu.Item>
            </PermissionRenderer>
            <PermissionRenderer permissions={[PermissionEnum.DELETE_ACCOUNT_STAFF]}>
            {staff.username && <>
                <Menu.Item key="3" onClick={() => deleteStaffAccount(staff)}>
                    Xóa tài khoản
                </Menu.Item>
            </>
            }
            </PermissionRenderer>
            <PermissionRenderer permissions={[PermissionEnum.DELETE_STAFF]}>
            <Menu.Item danger color="red" onClick={() => onClickDeleteStaff(staff)}>
                Xóa nhân viên
            </Menu.Item>
            </PermissionRenderer>
            <Menu.Item
                key="1"
                onClick={() => {
                    setChosenStaff({name: staff.lastName + ' ' + staff.firstName, emailAddress: staff.email});
                    dispatch(emailActions.setShowModal(true));
                }}
            >
                Gửi email
            </Menu.Item>
        </Menu>
    );

    const columns = [
        // {
        //     title: "Mã",
        //     dataIndex: "id",
        //     key: "code",
        //     width: "5%",
        //     search: true,
        //     // render: (staffId: number) => "NV_" + staffId
        // },
        {
            title: "Avatar",
            dataIndex: "avatar",
            key: "avatar",
            width: "5%",
            render: (avatarUrl: string) => <Avatar src={avatarUrl} />
        },
        {
            title: "Họ và tên đệm",
            dataIndex: "lastName",
            key: "name",
            width: "25%",
        },
        {
            title: "Tên",
            dataIndex: "firstName",
            key: "firstName",
            width: "15%",
            sorter: {
                multiple: 1
            },
            sortDirections: ["descend", "ascend"],
            sortOrder:
                searchCriteria.sort.filter((x) => x.field === "firstName").length > 0 &&
                searchCriteria.sort.filter((x) => x.field === "firstName")[0].order,
        },
        {
            title: "Tên tài khoản",
            dataIndex: "username",
            key: "username",
            width: "20%",
            search: true,
            render: (username: string) => (username || <Tag color={"red"}>Không có tài khoản</Tag>)
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "email",
            width: "20%",
            search: true
        },
        {
            title: "Số điện thoại",
            dataIndex: "phone",
            key: "phone",
            width: "25%",
            search: true
        },
        {
            title: "",
            key: "action",
            with: "5%",
            render: (staff: Staff) => <Dropdown overlay={renderMenu(staff)} placement="bottomRight">
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                    Hành động
                </a>
            </Dropdown>
        },
    ];

    if (isError && (error as any)?.data.code === STATUS_CODE_RESPONSE.UNAUTHORIZED) {
        return <Navigate to={ROUTING_CONSTANTS.UNAUTHORIZED} />
    } else
        return <>
            <CustomTable loading={isLoading}
                columns={hasPermissionWithStaff ? columns : columns.filter(col => col.key !== "action")}
                data={getAllStaffsResponse?.listData || []}
                metaData={extractPaginationInformation(getAllStaffsResponse)}
                searchCriteria={searchCriteria}
                changeSearchActionCreator={changeCriteria}
            />
            {selectedStaff &&
                <AccountStaffModal staff={selectedStaff} visible={visibleAccountModal}
                    setVisible={setVisibleAccountModal} />
            }
            <Formik
                enableReinitialize={true}
                initialValues={defaultMail}
                validateOnChange={false}
                validationSchema={SendEmailSchema}
                onSubmit={(values, {resetForm, setSubmitting}) => {
                    console.log('values: ', values);
                    sendEmail({emailAddress: chosenStaff.emailAddress, body: values})
                        .unwrap()
                        .then(res => {
                            antdNotifier.success("Gửi email đến học viên thành công");
                            setSubmitting(false);
                            resetForm({values: defaultMail});
                            dispatch(emailActions.setShowModal(false));
                        });
                }}
            >{({setFieldValue, values, isValid, handleSubmit, resetForm, isSubmitting}) => {
                return (
            <Modal
                destroyOnClose={true}
                visible={showModal}
                title={<span>Gửi email cho nhân viên <b>{chosenStaff.name}</b></span>}
                okText={'Gửi'}
                cancelText={'Hủy'}
                width={700}
                onCancel={() => {
                    resetForm({values: defaultMail});
                    dispatch(emailActions.setShowModal(false));
                }}
                onOk={() => {
                    handleSubmit();
                }}
                maskClosable={false}
                okButtonProps={{loading: isSubmitting, disabled: isSubmitting}}
            >
                <Form layout={'vertical'}>
                    <FormItem name={'subject'} label={'Tiêu đề'} required={true}>
                        <Input name={'subject'} />
                    </FormItem>
                    {/*<FormItem name={'content'} label={'Nội dung'} required={true}>*/}
                    {/*    <Input name={'content'} />*/}
                    {/*</FormItem>*/}
                    <Field
                        name={`content`}
                        placeholder={""}
                        component={TextEditorField}
                        label={"Nội dung"}
                        showValidateSuccess={true}
                        required={true}
                    />
                    <Form.Item name={"attachment"} label={"Đính kèm tệp"}>
                        {/* TODO: add prefix so the files will be saved on firebase as: /textbooks/... */}
                        <Field
                            name={"attachment"}
                            placeholder={""}
                            component={GeneralFileUploadField}
                            showImagePreviewInsteadOfIcon={false}
                            showPreview={true}
                            displayedLayout={'row'}
                        />
                        {/* <Input name={ATTACHMENT_FIELD}/> */}
                    </Form.Item>
                </Form>
            </Modal>
                );
            }
            }
            </Formik>
        </>
}

export default StaffsManagement;