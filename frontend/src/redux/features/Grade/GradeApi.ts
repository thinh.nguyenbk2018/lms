import { BaseResponse } from "../../interfaces/types";
import { apiSlice } from "../CentralAPI";
import { Tag } from "./GradeFormulaSlice";

const gradeFormulaApiInvalidatesTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["Grade_formula", "Grade_tag_details", "Grade_results", "GradeTags"],
});

interface CreateGradeFormulaRequest {
    tagTitle: string;
    formula: string;
    expression: string;
    useTags: number[];
    scope: "COURSE" | "CLASS";
    scopeId: number;
    isPublic: boolean;
}

interface CreateGradeFormulaResponse extends BaseResponse {
    tagId: number;
}

interface UpdateGradeFormulaRequest {
    id: number;
    tagTitle: string;
    formula: string;
    expression: string;
    useTags: number[];
    isPublic: boolean;
}

type UpdateGradeFormulaResponse = CreateGradeFormulaResponse;

interface GetGradeFormulaResponse extends BaseResponse {
    id: number;
    tagTitle: string;
    formula: string;
    useTags: number[];
    public: boolean;
}

export type ScopeTag = "COURSE" | "CLASS";

interface GetAllTagsInScopeRequest {
    scope: ScopeTag;
    scopeId: number;
}

export interface ExportGradeTags {
    gradeTagsId: number[]
}

interface GetAllTagsInScopeResponse {
    tags: Tag[];
}

type GetAllGradeTagsDetailsRequest = GetAllTagsInScopeRequest;

export interface TagDetails {
    id: number;
    title: string;
    hasGraded: boolean;
    gradedAt: Date;
    updatedAt: Date;
    primitive: boolean;
    public: boolean;
}

interface GetAllGradeTagsDetailsResponse extends BaseResponse {
    tags: TagDetails[];
}

interface GradeRequest {
    tagId: number;
}

interface GradeResponse extends BaseResponse {}

interface GetGradeResultsRequest {
    scope: ScopeTag;
    scopeId: number;
}

export interface GradeTagDto {
    id: number;
    title: string;
}

export interface GradeResult {
    studentId: number;
    studentName: string;
    avatar: string;
    grades: {
        tagId: number;
        tagTitle: string;
        grade: number;
    }[];
}

interface GetGradeResultsResponse extends BaseResponse {
    tags: Tag[];
    gradeResults: GradeResult[];
}

const gradeAPI = gradeFormulaApiInvalidatesTags.injectEndpoints({
    endpoints: (build) => ({
        getGradeFormula: build.query<GetGradeFormulaResponse, number>({
            query: (tagId) => "/gradeFormula/" + tagId,
            providesTags: (_, __, tagId) => [
                { type: "Grade_formula", id: tagId },
            ],
        }),
        getAllTagsInScope: build.query<
            GetAllTagsInScopeResponse,
            GetAllTagsInScopeRequest
        >({
            query: (body) => `/tag?scope=${body.scope}&scopeId=${body.scopeId}`,
            providesTags: [{ type: "Grade_formula", id: "List" }],
        }),
        createGradeFormula: build.mutation<
            CreateGradeFormulaResponse,
            CreateGradeFormulaRequest
        >({
            query: (body) => ({
                url: "/gradeFormula",
                method: "post",
                body,
            }),
        }),
        updateGradeFormula: build.mutation<
            UpdateGradeFormulaResponse,
            UpdateGradeFormulaRequest
        >({
            query: (body) => ({
                url: "/gradeFormula",
                method: "PUT",
                body,
            }),
        }),
        getAllGradeTagsDetails: build.query<
            GetAllGradeTagsDetailsResponse,
            GetAllGradeTagsDetailsRequest
        >({
            query: (params) => ({
                url: "/grade-tags-details",
                params,
            }),
            providesTags: [{ type: "Grade_tag_details", id: "List" }],
        }),
        grade: build.mutation<GradeResponse, GradeRequest>({
            query: (body) => ({
                url: "/tags/grade",
                method: "post",
                body,
            }),
            invalidatesTags: [{ type: "Grade_tag_details", id: "List" }],
        }),
        getGradeResults: build.query<
            GetGradeResultsResponse,
            GetGradeResultsRequest
        >({
            query: (params) => ({
                url: "/grades/result",
                params,
            }),
            providesTags: [{ type: "Grade_results", id: "List" }]
        }),
        getGradeTagsByClass: build.query<GradeTagDto[], number>({
            query: (classId) => ({
                url: `/classes/${classId}/grade_tag`
            }),
            providesTags: [{type: "GradeTags", id: "List"}]
        }),
        pubicGrade: build.mutation<
            BaseResponse,
            { tagId: number; isPublic: boolean }
        >({
            query: ({ tagId, isPublic }) => ({
                url: `/tags/${tagId}/public/${isPublic}`,
                method: "POST",
            }),
            invalidatesTags: [{ type: "Grade_tag_details", id: "List" }],
        }),
        deleteGradeTag: build.mutation<BaseResponse, number>({
            query: (tagId) => ({
                url: `/tags/${tagId}`,
                method: "delete",
            }),
            invalidatesTags: [{ type: "Grade_tag_details", id: "List" }, { type: "Grade_tag_details", id: "List" }],
        }),
    }),
    overrideExisting: false,
});

export const {
    useCreateGradeFormulaMutation,
    useLazyGetGradeFormulaQuery,
    useGetAllTagsInScopeQuery,
    useUpdateGradeFormulaMutation,
    useLazyGetAllGradeTagsDetailsQuery,
    useGradeMutation,
    useLazyGetGradeResultsQuery,
    usePubicGradeMutation,
    useDeleteGradeTagMutation,
    useLazyGetGradeTagsByClassQuery
} = gradeAPI;
