import React from "react";
import {useNavigate, useParams} from "react-router-dom";
import {Button, Checkbox, Col, Collapse, Form, Pagination, Row, Space, Typography,} from "antd";
import {replacePathParams} from "../../../../../utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import useClassSidebar from "../../../../../hooks/Routing/useClassSidebar";
import {
	isPaginatedResponseQuestionList,
	useGetQuizOverviewStudentQuery,
	useInitTestSessionMutation,
} from "../../../../../redux/features/Quiz/examAPI";
import {useAppDispatch, useAppSelector,} from "../../../../../hooks/Redux/hooks";
import {
	testWithConfigInfoActions,
	testWithConfigInfoSelectors,
} from "../../../../../redux/features/Quiz/TestWithConfigInfoSlice";
import COLOR_SCHEME from "../../../../../constants/ThemeColor";
import {formatRelative} from "date-fns";
import {parseDate} from "../../../../../components/Calendar/Utils/CalendarUtils";
import {vi} from "date-fns/locale";
import UserInfoNavigateOnClick from "../../../../../components/UserInfoNavigateOnClick/UserInfoNavigateOnClick";
import QuestionContainerItem from "../../../../../components/Question/QuestionContainerItem/QuestionContainerItem";
import QuizConfigurationDrawer from "./QuizConfigurationDrawer";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorWithRetryButton from "../../../../../components/Util/ApiErrorWithRetryButton";
import LoadingPage from "../../../../UtilPages/LoadingPage";
import StageTag from "../../../../../components/StateTag/StageTag";

// * Check if the the user is the teacher: Yes ? Show question list : Hide the question:q
// * Render the central page for user to view the test info and a button to initizlize a test session
const TestWithConfigInfo = () => {
	const navigate = useNavigate();
	const [closer, classId, courseInfo] = useClassSidebar();

	const dispatch = useAppDispatch();
	const { testWithConfigId } = useParams();
	const searchCriteria = useAppSelector(
		testWithConfigInfoSelectors.selectSearchCriteria
	);

	const [initSession] = useInitTestSessionMutation();
	// TODO: Check if the user can view the questions
	const canAccess = true;

	const { data: testData,
		isSuccess,
		isFetching,
		isUninitialized,
		isError,
		error,
		isLoading,
	} = useGetQuizOverviewStudentQuery({
		testWithConfigId: Number(testWithConfigId),
		classId,
	});
	// * Check for permission to preview the questions
	const renderQuestionListIfHaveAccess = () => {
		if ( canAccess && testData?.questionList && isPaginatedResponseQuestionList(testData.questionList)) { return ( <> <Row>
						{testData?.questionList.listData.map((item, index) => {
							return (
								<QuestionContainerItem
									key={item.id}
									questionData={item}
									mode={"view"}
								/>
							);
						})}
					</Row>
					<Row justify={"end"}>
						<Pagination
							showSizeChanger
							pageSizeOptions={[5, 10, 20, 50]}
							defaultPageSize={5}
							onChange={(value) => {
								let newSearchCriteria = {
									...searchCriteria,
									pagination: {
										...searchCriteria.pagination,
										page: value,
									},
								};

								dispatch(
									testWithConfigInfoActions.changeCriteria(newSearchCriteria)
								);
							}}
							onShowSizeChange={(current, newSize) => {
								let newSearchCriteria = {
									...searchCriteria,
									pagination: {
										...searchCriteria.pagination,
										size: newSize,
										page: current,
									},
								};

								newSearchCriteria.pagination.size = newSize;
								console.log(
									"CHANGED CRITERIA",
									newSearchCriteria,
									"new Size",
									newSize
								);
								dispatch(
									testWithConfigInfoActions.changeCriteria(newSearchCriteria)
								);
							}}
							defaultCurrent={3}
							total={testData?.questionList.total}
						/>
					</Row>
				</>
			);
		} else return <></>;
	};
	if (isSuccess) {
		const { config } = testData;
		return (
			<div>
				<div className={"test-instance-config-container"}>
					<Row gutter={[0, 24]} style={{ marginTop: 12 }}>
						<Col span={24}>
							<Row
								justify={
									"end"
								} /*style={{zIndex: "12", position: "fixed", top: "10px", right: "10px"}} */
							>
								<Space>
									<QuizConfigurationDrawer
										config={testData?.config}
										classId={classId}
										courseId={courseInfo?.id || 1}
									/>
								</Space>
							</Row>
						</Col>
						<Col sm={24} md={24} lg={16} xl={14} xxl={{ span: 12, offset: 6 }}>
							<Collapse collapsible="header" defaultActiveKey={["1"]}>
								<Collapse.Panel
									header={
										<Typography.Text style={{ color: "white" }}>
											{"Thông tin chung của bài kiểm tra"}
										</Typography.Text>
									}
									key="1"
									style={{ backgroundColor: COLOR_SCHEME.primary }}
								>
									<Row style={{ marginTop: "1em" }}>
										<Col span={24}>
											<Row justify={"center"}>
												<Typography.Text strong>
													{testData?.title}
												</Typography.Text>
											</Row>
										</Col>
										<Col
											sm={24}
											md={{ span: 14, offset: 8 }}
											xxl={{ span: 14, offset: 8 }}
										>
											<Form.Item label={"Description"}>
												{testData?.description || "No description"}
											</Form.Item>
										</Col>
										{testData && (
											<Col
												sm={24}
												md={{ span: 14, offset: 8 }}
												xxl={{ span: 14, offset: 8 }}
											>
												<Form.Item label={"Thuộc khóa"}>
													<Typography.Link
														onClick={() => {
															navigate(
																replacePathParams(
																	ROUTING_CONSTANTS.COURSE_DETAIL,
																	[":id", testData.courseId]
																)
															);
														}}
													>
														{testData.courseName || "No course found"}
													</Typography.Link>
												</Form.Item>
											</Col>
										)}
										<Col
											sm={24}
											md={{ span: 14, offset: 8 }}
											xxl={{ span: 14, offset: 8 }}
										>
											<Form.Item label={"Trạng thái:"}>
												<StageTag type={config.state}></StageTag>
											</Form.Item>
										</Col>
										<Col
											sm={24}
											md={{ span: 14, offset: 8 }}
											xxl={{ span: 14, offset: 8 }}
										>
											<Form.Item label={"Tạo vào lúc: "}>
												{testData?.createdAt &&
													formatRelative(
														parseDate(testData.createdAt),
														new Date(),
														{ locale: vi }
													)}
											</Form.Item>
											<Form.Item label={"Bởi: "}>
												{testData?.createdBy && (
													<UserInfoNavigateOnClick {...testData.createdBy} />
												)}
											</Form.Item>
										</Col>
										<Col
											sm={24}
											md={{ span: 14, offset: 8 }}
											xxl={{ span: 14, offset: 8 }}
										>
											<Form.Item label={"Chỉnh sửa lần cuối vào: "}>
												{testData?.updatedAt &&
													formatRelative(
														parseDate(testData.updatedAt),
														new Date(),
														{ locale: vi }
													)}
											</Form.Item>
											<Form.Item label={"Bởi:: "}>
												{testData?.updatedBy && (
													<UserInfoNavigateOnClick {...testData.updatedBy} />
												)}
											</Form.Item>
										</Col>
									</Row>
								</Collapse.Panel>
							</Collapse>
						</Col>
						<Col sm={24} md={24} lg={16} xl={14} xxl={{ span: 12, offset: 6 }}>
							<Row gutter={[0, 20]} align={"middle"} justify={"center"}>
								{isPaginatedResponseQuestionList(testData?.questionList) &&
									testData?.questionList.listData.map((item, index) => (
										<Col span={24} key={item.id}>
											<QuestionContainerItem
												questionData={item}
												canEdit={false}
												mode={"view"}
												order={(testData.questionList.page-1) * searchCriteria.pagination.size + index + 1}
											/>
										</Col>
									))}
							</Row>
						</Col>
					</Row>
					<Col sm={24} md={24} lg={16} xl={14} xxl={{ span: 12, offset: 6 }}>
						<Collapse collapsible="header" defaultActiveKey={["1"]}>
							<Collapse.Panel
								header={
									<Typography.Text style={{ color: "white" }}>
										{"Cấu hình của bài kiểm tra"}
									</Typography.Text>
								}
								key="1"
								style={{ backgroundColor: COLOR_SCHEME.primary }}
							>
								<Row style={{ marginTop: "1em" }}>
									<Col
										sm={24}
										md={{ span: 14, offset: 8 }}
										xxl={{ span: 14, offset: 8 }}
									>
										<Form.Item label={"Giới hạn thời gian"}>
											{config.haveTimeLimit ? config.timeLimit : "Không"}
										</Form.Item>
									</Col>
									<Col
										sm={24}
										md={{ span: 14, offset: 8 }}
										xxl={{ span: 14, offset: 8 }}
									>
										<Form.Item label={"Cho phép quay lại"}>
											<Checkbox checked={config.canNavigateBackward || true} />
										</Form.Item>
									</Col>
									<Col
										sm={24}
										md={{ span: 14, offset: 8 }}
										xxl={{ span: 14, offset: 8 }}
									>
										<Form.Item label={"Được phép thoát giữa chừng"}>
											<Checkbox checked={config.allowDisconnect} />
										</Form.Item>
									</Col>
									<Col
										sm={24}
										md={{ span: 14, offset: 8 }}
										xxl={{ span: 14, offset: 8 }}
									>
										<Form.Item label={"Điểm đạt"}>
											{config.passScore ? config.passScore : "Không"}
										</Form.Item>
									</Col>
									<Col
										sm={24}
										md={{ span: 14, offset: 8 }}
										xxl={{ span: 14, offset: 8 }}
									>
										<Form.Item label={"Số lần làm bài tối đa"}>
											{config.maxAttempt || 1}
										</Form.Item>
									</Col>
								</Row>
							</Collapse.Panel>
						</Collapse>
					</Col>
					<Row>{renderQuestionListIfHaveAccess()}</Row>
				</div>
				<Button
					onClick={() => {
						initSession({
							testWithConfigId: Number(testWithConfigId),
							classId,
						})
							.unwrap()
							.then((res) => {
								AntdNotifier.success("Session created: " + res.id);
								navigate(
									replacePathParams(
										ROUTING_CONSTANTS.CLASS_QUIZ_SESSION,
										[":quizId", Number(testWithConfigId)],
										[":classId", Number(classId)],
										[":testSessionId", res.id]
									)
								);
							})
							.catch((err) => {
								AntdNotifier.error("Cant init session");
							});
					}}
				>
					Bắt đầu làm bài
				</Button>
			</div>
		);
	} else if (isLoading) {
		return <LoadingPage />;
	} else {
		return <ApiErrorWithRetryButton onRetry={() => {}} />;
	}
};

export default TestWithConfigInfo;
