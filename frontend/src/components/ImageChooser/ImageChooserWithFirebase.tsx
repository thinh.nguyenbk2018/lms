import { LoadingOutlined } from '@ant-design/icons';
import { Alert, Button, Card, Form, Image, Space, Spin } from "antd";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";
import React, { useEffect, useRef, useState } from 'react';
import { useAppSelector } from "../../hooks/Redux/hooks";
import { authSelectors } from "../../redux/features/Auth/AuthSlice";
import { tenantSelector } from '../../redux/features/Tenant/TenantSlice';
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { storage } from '@utils/FirebaseSDK/firebase';

const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;


type FileUploadProps = {
    /** Either a file uploader (accept image only and will show preview) or an general file upload (accept everything and will try to show an icon preresenting the file) */
    mode?: "image" | "general_file";
    /** will show default image if not null else will show text prompting the user to choose the file */
    defaultImageUrl?: "",
    /** what to do beside uploading the image to firebase for storage. This function will receive a link of the uploaded media*/
    callback?: Function,
}

// * The callback is passed from parent component: Used for setting the state on the parent or do something else
export const LocalImageChooserWithFirebase = ({defaultImageUrl, callback}: FileUploadProps) => {
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);
    const userSuccess = useAppSelector(authSelectors.selectLoginSuccess);

    // * The Image file
    const [image, setImage] = useState<File | undefined>(undefined);
    // * The image URL if uploaded to firebase storage successfully
    const [imageUrl, setImageUrl] = useState("");
    // * show Spinning state
    const [isLoading, setLoading] = useState(false);

    const tenantInfo = useAppSelector(tenantSelector.selectTenantInfo);
    const tenantSubdomain = tenantInfo.domain;
    useEffect(() => {
        handleUpload(); 
        return () => {

        }
    }, [image])


    const fileInputRef = useRef<HTMLInputElement>(null);


    const handleChange: React.ChangeEventHandler<HTMLInputElement> | undefined = async (e) => {
        try {
            if (e.target.files?.[0]) {
                await setImage(e.target.files?.[0]);
                setLoading(true);
                return;
            } else {
                antdNotifier.warn(`Empty or Null file`)
                return;
            }
        } catch (e) {
            antdNotifier.warn(`Có lỗi khi xử lý file`)
        }
    }

    const handleUpload = () => {
        if (!image) {
            return;
        }
        console.log(`IMAGE INFO NAME | TYPE | SIZE : ${image.name} | ${image.type} | ${image.size}`);
        // TODO: Add tenant id
        const storageRef = ref(storage, `${tenantSubdomain}/${userSuccess ? userInfo?.username : "default_user"}/images/${image.name}`);

        const uploadTask = uploadBytesResumable(storageRef, image)
        uploadTask.on('state_changed',
            (snapshot) => {
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                    case 'paused':
                        console.log('Upload is paused');
                        break;
                    case 'running':
                        console.log('Upload is running');
                        break;
                }
            },
            (error) => {
                console.log("UNABLE TO UPLOAD FILE", error.message);
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref)
                    .then((downloadUrl) => {
                        setImageUrl(downloadUrl);
                        callback?.(downloadUrl)
                        setLoading(false);
                    })
            }
        )
    };
    return (
        <Form.Item
            name={"avatar"}
        >
            {
                isLoading ? <Spin indicator={antIcon}>
                        <Alert
                            message="Ảnh đang được tải lên!"
                            description="Đợi chờ là hạnh phúc"
                            type="info"
                        />
                    </Spin> :
                    <>
                        <Space direction={"vertical"}>
                            <Button type={"default"} onClick={() => fileInputRef.current?.click()}>Tải ảnh</Button>
                            <input ref={fileInputRef} type={"file"} onChange={handleChange} style={{display: "none"}} accept={"image/*"}/>
                            {
                                imageUrl ? <Image
                                    width={200}
                                    src={imageUrl || defaultImageUrl}
                                    style={{marginLeft: "1rem"}}
                                /> : <Card>
                                    Nhấn nút "Tải Ảnh" và chọn một tệp ảnh
                                </Card>
                            }
                        </Space>
                    </>
            }
        </Form.Item>
    );
};


/*
            <Form.Item
            name={"avatar"}
        >
            <Button type={"default"} onClick={() => fileInputRef.current?.click()}>Choose Image</Button>
            <input ref={fileInputRef} type={"file"} onChange={async () => {
                // console.log(fileInputRef?.current?.files![0] ? fileInputRef?.current?.files[0] : defaultUrl);
                const fileBlob = fileInputRef?.current?.files![0];
                const imageEncoded = await getImageasText(fileBlob);
                const image = imageEncoded as string || defaultUrl;
                console.log("IMAGE URL IS: ", imageEncoded);
                setImage(image);
                callback?.(image);
            }} style={{display: "none"}}/>
            <Image
                width={200}
                src={image || defaultUrl}
            />
        </Form.Item>
*/


// export class Statistics {
//     /**
//      * Returns the average of two numbers.
//      *
//      * @remarks
//      * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
//      *
//      * @param x - The first input number
//      * @param y - The second input number
//      * @returns The arithmetic mean of `x` and `y`
//      *
//      * @beta
//      */
//     public static getAverage(x: number, y: number): number {
//         return (x + y) / 2.0;
//     }
// }
