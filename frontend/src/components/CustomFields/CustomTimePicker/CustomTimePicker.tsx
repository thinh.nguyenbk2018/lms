import dateFnsGenerateConfig from "rc-picker/lib/generate/dateFns";
import generatePicker from "antd/es/date-picker/generatePicker";
import "antd/es/date-picker/style/index";
import { format } from "date-fns";

const DatePicker = generatePicker<Date>(dateFnsGenerateConfig);

type CustomTimePickerProps = {
	onChange?: (e: Date | null) => void;
	onBlur?: (e: any) => void;
};
const CustomTimePicker = (props: CustomTimePickerProps) => (
	<DatePicker
		picker="time"
		onChange={(e) => {
			if (!e) {
				return;
			}
			console.log("PICKED", format(e, "HH:mm"));
			props.onChange?.(e);
		}}
		onBlur={props.onBlur}
	/>
);
export default CustomTimePicker;
