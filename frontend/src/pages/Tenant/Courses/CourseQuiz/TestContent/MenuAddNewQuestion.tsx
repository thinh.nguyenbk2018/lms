import { PlusCircleOutlined, FileAddOutlined, CheckSquareTwoTone, HighlightTwoTone, ProfileTwoTone, ScheduleTwoTone, SwapOutlined } from "@ant-design/icons"
import COLOR_SCHEME from "@constants/ThemeColor"
import { QUESTION_TYPES } from "@redux/features/Quiz/CreateQuizSlice";
import { Dropdown, Button, Menu, Row, Space } from "antd"

const MenuAddNewQuestion = ({ handleMenuClick }: { handleMenuClick: (e: any) => void }) => {
  const newQuestionChoiceMenu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item
        key={QUESTION_TYPES.MULTI_CHOICE}
        icon={<CheckSquareTwoTone />}
      >
        Câu hỏi trắc nghiệm
      </Menu.Item>
      <Menu.Item key={QUESTION_TYPES.FILL_IN_BLANK} icon={<ScheduleTwoTone />}>
        Câu hỏi điền khuyết
      </Menu.Item>
      <Menu.Item
        key={QUESTION_TYPES.BLANK_WITH_CHOICES}
        icon={<ProfileTwoTone />}
      >
        Điền khuyết có lựa chọn
      </Menu.Item>
      <Menu.Item key={QUESTION_TYPES.WRITTEN} icon={<HighlightTwoTone />}>
        Câu hỏi tự luận
      </Menu.Item>
      <Menu.Item
        key={QUESTION_TYPES.DRAG_AND_DROP}
        icon={<SwapOutlined style={{ color: COLOR_SCHEME.primary }} />}
      >
        Câu hỏi kéo thả
      </Menu.Item>
    </Menu>
  );
  return (
    <Space>
      <Dropdown
        overlay={newQuestionChoiceMenu}
        placement={"bottomLeft"}
        arrow
      >
        <Button>
          <PlusCircleOutlined style={{ color: COLOR_SCHEME.primary }} />
          Thêm câu hỏi
        </Button>
      </Dropdown>
    </Space>);
}

export default MenuAddNewQuestion;