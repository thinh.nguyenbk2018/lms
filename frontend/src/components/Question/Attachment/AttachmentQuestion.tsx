import { FileTextOutlined } from "@ant-design/icons";
import GeneralFileUploadField from "@components/CustomFields/FileUploadField/GeneralFileUploadField";
import FileManager from "@components/FileManager/FileManager";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { Button, Col, Modal, Row, Space } from "antd";
import { FieldProps } from "formik";
import { CSSProperties, useEffect, useState } from "react";

type AttachmentType = "Video" | "Audio" | "Document";

export interface Attachment {
  type: AttachmentType,
  url: string,
  name?: string,
}

type CustomFieldProps =
  FieldProps<any>
  & { label?: string, style?: CSSProperties, required?: boolean };

const AttachmentQuestion = ({ field,
  form: {
    touched,
    errors,
    setFieldValue,
    setFieldError,
    handleBlur,
    handleChange,
    setTouched,
    setFieldTouched
  },
  ...otherProps
}: CustomFieldProps) => {

  let { name, value } = field;

  const [showFileBrowser, setShowFileBrowser] = useState(false);
  const [attachmentType, setAttachmentType] = useState<AttachmentType>("Video");
  const [attachments, setAttachments] = useState<Attachment[]>(value ? JSON.parse(value) : []);

  useEffect(() => {
    console.log("attachment", value)
    let a = JSON.parse(value || "[]");
    console.log("attachment", a)
    setAttachments(a)
  }, [value])

  const addAttachment = (type: AttachmentType, url: string, fileName: string) => {
    let newAttachments = [...attachments, { type, url, name: fileName }]
    setAttachments(newAttachments);
    setFieldValue(name, JSON.stringify(newAttachments));
  }

  const deleteAttachment = (attachment: Attachment) => {
    let newAttachments = attachments.filter(a => a.url != attachment.url);
    setAttachments(newAttachments);
    setFieldValue(name, JSON.stringify(newAttachments));
  }
  const renderVideo = (attachment: Attachment) => {
    return <Space>
      <video height={400} width={600} controls>
        <source src={attachment.url} />
      </video>
      <Button type="primary" danger onClick={() => deleteAttachment(attachment)}>Xóa</Button>
    </Space>
  }

  const renderAudio = (attachment: Attachment) => {
    return <Space>
      <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
        <Space>
          <audio controls>
            <source src={attachment.url} />
          </audio>
          <Button type="primary" danger onClick={() => deleteAttachment(attachment)}>Xóa</Button>
        </Space>
        <span>{attachment.name}</span>
      </div>
    </Space>
  }

  const renderDocument = (attachment: Attachment) => {
    return <Row justify="start">
      <Col span={24}>
        <Space align="start">
          <a href={attachment.url} download={attachment.name}>
            <FileTextOutlined />
            {attachment.name}
          </a>
          <Button type="primary" danger onClick={() => deleteAttachment(attachment)}>Xóa</Button>
        </Space>
      </Col>
    </Row>
  }

  return <>
    <Row justify="end" style={{ marginBottom: 15 }}>
      <Space align="end">
        <Button type="primary" onClick={() => {
          setAttachmentType("Video");
          setShowFileBrowser(true);
        }}>
          Thêm video
        </Button>
        <Button type="primary" onClick={() => {
          console.log('set audio');
          setAttachmentType("Audio");
          setShowFileBrowser(true);
        }}>
          Thêm audio
        </Button>
        <Button type="primary" onClick={() => {
          setAttachmentType("Document");
          setShowFileBrowser(true);
        }}>
          Thêm tài liệu
        </Button>
      </Space>
    </Row>
    {
      attachments.map(attachment => (
        <Row justify="center">
          {attachment.type == "Video" && renderVideo(attachment)}
          {attachment.type == "Audio" && renderAudio(attachment)}
          {attachment.type == "Document" && renderDocument(attachment)}
        </Row>))
    }
    <Modal visible={showFileBrowser}
           width={"80%"}
           footer={null}
           onCancel={() => setShowFileBrowser(false)}
           bodyStyle={{overflowY: 'auto', maxHeight: 'calc(100vh - 150px)'}}
           destroyOnClose={true}
    >
      <div style={{ padding: "25px 20px 0px 20px" }}>
        <FileManager onSelectFile={({ fileUrl, type, name }) => {
          console.log("Định dạng file:", type)
          if (attachmentType == "Video" && ["mp4", "avi", "mov", "wmv"].includes(type.toLocaleLowerCase())) {
            setShowFileBrowser(false);
            addAttachment(attachmentType, fileUrl, name);
            return;
          }
          console.log('attachment type: ', attachmentType);
          if (attachmentType == "Audio" && ["mp3", "wma", "wav", "aac"].includes(type.toLocaleLowerCase())) {
            setShowFileBrowser(false);
            addAttachment(attachmentType, fileUrl, name);
            return;
          }
          if (attachmentType == "Document" && ["pdf", "doc", "docx"].includes(type.toLocaleLowerCase())) {
            setShowFileBrowser(false);
            addAttachment(attachmentType, fileUrl, name);
            return;
          }
          AntdNotifier.error("Định dạng file không đúng!");
        }} />
      </div>
    </Modal>
  </>
}

export default AttachmentQuestion;