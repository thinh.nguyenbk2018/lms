import React from 'react';
import {Button, Space, Typography} from "antd";

interface Props {
    message?: string,
    onRetry: () => void
    retryText?: string
}
const ApiErrorWithRetryButton = (props: Props) => {
    return <>
        <Space direction={"vertical"} align={"center"}>
            <Typography.Text>
                {props.message || `Lỗi kết nối với server. Thử lại sau`}
            </Typography.Text>
            <Button type={"primary"} onClick={props.onRetry}>
                {props.retryText || "Thử lại"}
            </Button>
        </Space>
    </>
};

export default ApiErrorWithRetryButton;


