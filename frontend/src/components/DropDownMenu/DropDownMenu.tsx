import React from 'react';
import {Button, Dropdown, Menu} from "antd";
import {EllipsisOutlined} from "@ant-design/icons";

/*
    * itemDisplay: How an menu item / option looks like ?
    * callback: What to do when the menu item is clicked ?
 */
export interface MenuItemProp {
    itemDisplay: JSX.Element,
    callback: () => void
}
// * Trigger is the component that will render the menu items when clicked / hover on
const DropDownMenu = ({props, trigger, ...rest}: { props: MenuItemProp[], trigger?: JSX.Element }) => (
    <Dropdown key="more" {...rest} overlay={
        <Menu>
            {props.map((item, index) => {
                return <Menu.Item onClick={item.callback}>
                    {item.itemDisplay}
                </Menu.Item>
            })}
        </Menu>
    }>
        <Button
            style={{
                border: 'none',
                padding: 0,
            }}
        >
            {
                trigger || <EllipsisOutlined
                    style={{
                        fontSize: 20,
                        verticalAlign: 'top',
                    }}
                />

            }
        </Button>
    </Dropdown>
);

export default DropDownMenu;
