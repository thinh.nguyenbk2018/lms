// This file is used for configurating the msw that work in the integration testing with a WebDriver (that actually open a browser and test it) 
// We are using Cypress for integration testing  
import { setupWorker } from "msw"; 
import  handlers  from "./handlers/handlers"; 

export const worker = setupWorker(...handlers); 