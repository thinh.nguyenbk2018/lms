import {cloneDeep} from "lodash";
import {RRule} from "rrule";
import {setEventStartTime} from "../../../components/Calendar/Utils/CalendarUtils";
import {CalendarEvent, EventDTO} from "./CalendarAPI";
import {EventHashMap, putToMap} from "./CalendarSlice";

export type EventDtoListToHashMapTransformer = (eventList: EventDTO[]) => EventHashMap;
export const calendarEventDtoListToHashMap: EventDtoListToHashMapTransformer = (
    evenList
) => {
    const newMap = {};
    evenList.forEach((item) => {
        // * Check if there are recurences
        // const { recurrence } = item.event;
        // if (recurrence && recurrence.length != 0) {
        // 	// * multiplies the events
        // 	const dates = RRule.fromString(recurrence[0]).all();
        // 	console.log("RECURRERENCE WITH : ", dates);
        // 	dates.forEach((date) => {
        // 		const clonedEvent = cloneDeep<CalendarEvent>(item.event);
        // 		setEventStartTime(clonedEvent, date);
        // 		putToMap(newMap, clonedEvent);
        // 	});
        // }
        // putToMap(newMap, item.event);

        const clonedItem = cloneDeep(item);
        const {event} = clonedItem;
        event.eventType = clonedItem.eventType;
        putToMap(newMap, event);
    });
    return newMap;
};

export type EventListToHashMapTransformer = (
    eventList: CalendarEvent[]
) => EventHashMap;
export const calendarEventListToHashMap: EventListToHashMapTransformer = (
    evenList
) => {
    const newMap = {};
    evenList.forEach((item) => {
        // * Check if there are recurences
        const {recurrence} = item;
        if (recurrence && recurrence.length != 0) {
            // * multiplies the events
            const dates = RRule.fromString(recurrence[0]).all();
            console.log("RECURRERENCE WITH : ", dates);
            dates.forEach((date: any) => {
                const clonedEvent = cloneDeep<CalendarEvent>(item);
                setEventStartTime(clonedEvent, date);
                putToMap(newMap, clonedEvent);
            });
        }
        putToMap(newMap, item);
    });
    return newMap;
};
