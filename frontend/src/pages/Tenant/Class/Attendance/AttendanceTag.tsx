import { green, red, yellow } from "@ant-design/colors";
import { Menu, Tag } from "antd";
import { AttendanceState } from "../../../../redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import { ReactNode } from "react";
import React from "react";

type AttendanceValueToColorMapper = {
	[Key in AttendanceState]: string;
};

const attendanceColorBoldness = 6;
const ATTENDANCE_VALUE_TO_COLOR_MAP: AttendanceValueToColorMapper = {
	PRESENT: green[attendanceColorBoldness],
	LATE: yellow[attendanceColorBoldness],
	ABSENT: red[attendanceColorBoldness],
	NONE: "#C5C5C5",
};


type AttendanceTagProps = {
	state: AttendanceState;
	title: string;
};

const AttendanceTag = (props: AttendanceTagProps) => {
	const { state, title } = props;
	return <Tag color={ATTENDANCE_VALUE_TO_COLOR_MAP[state]}>{title}</Tag>;
};

export default React.memo(AttendanceTag);
