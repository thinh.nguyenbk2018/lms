import React from 'react';
import {Card, CardProps} from "antd";
import COLOR_SCHEME from "../../../constants/ThemeColor";


const BlueBorderedCard = (props: CardProps) => {
    return (
        <Card {...props} headStyle={{borderColor: COLOR_SCHEME.primary}} bodyStyle={{borderColor: COLOR_SCHEME.primary}}>

        </Card>
    );
};

export default BlueBorderedCard;