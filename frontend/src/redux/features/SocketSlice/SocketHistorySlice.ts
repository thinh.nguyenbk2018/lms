import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../store/store";

export type SocketHistory = {
    isSocketReady: boolean,
    messages: string[]
}

const initialState : SocketHistory = {
    isSocketReady: false,
    messages: []
}

export const socketHistorySlice = createSlice({
    name: "socketHistory",
    initialState,
    reducers: {
        addNewMessage(state, action: PayloadAction<string>) {
            state.messages.push(action.payload);
        },
    },
    // extraReducers: (builder) => {
    // },

})

export const socketHistorySelector= {
    selectFullHistory: (state: RootState) => state.socketHistorySlice.messages,

};

export const socketAction = socketHistorySlice.actions;
