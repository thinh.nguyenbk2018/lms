
import { Field } from 'formik';
import { Form, FormItem, Input } from 'formik-antd';
import { getFieldNameWithNamespace } from '../../../utils/FormUtils/formUtils';
import TextEditorField from '../../CustomFields/TextEditorField/TextEditorField';

export const THREAD_FIELD_NAMES = {
		TITLE: "title",
		CONTENT: "content",
};

export const NewThreadForm = ({ namespace }: {namespace?: string}) => {
	const getFieldName = (name: string) => {
		return getFieldNameWithNamespace(namespace, name);
	};
	return (
		<Form layout="vertical">
			<FormItem
				name={`${getFieldName(THREAD_FIELD_NAMES.TITLE)}`}
				showValidateSuccess={true}
				label={"Tiêu đề"}
				required={true}
			>
				<Input
					name={`${getFieldName(THREAD_FIELD_NAMES.TITLE)}`}
					fast={true}
				/>
			</FormItem>
			<Field
				name={`${getFieldName(THREAD_FIELD_NAMES.CONTENT)}`}
				component={TextEditorField}
				label={"Nội dung"}
				showValidateSuccess={true}
				required={true}
			/>
		</Form>
	);
};


export default NewThreadForm;