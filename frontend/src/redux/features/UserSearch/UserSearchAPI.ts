import {User} from "../Auth/AuthSlice";
import {apiSlice} from "../CentralAPI";
import {SearchCriteria} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {transformSearchQuery} from "../APIUtils/TransformParameter";
import {PaginatedResponse} from "../../interfaces/types";


export type UserSearchedItem = Pick<User, "id" | "username" | "firstName" | "lastName" | "avatar">;
export type UserSearchListDto = PaginatedResponse<UserSearchedItem>;


const userSearchApiWithInvalidateTags = apiSlice.enhanceEndpoints({addTagTypes: ['UserSearch']});
const userSearchAPI = userSearchApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        getUserSearchWithKeyWord: build.query<UserSearchListDto, SearchCriteria>({
            query: (criteria) => `users/?${transformSearchQuery(criteria)}`,
            providesTags: (result) =>
                result
                    ?
                    [
                        ...result.listData.map(({id}) => ({type: 'UserSearch', id} as const)),
                        {type: 'UserSearch', id: 'LIST'},
                    ]
                    :
                    [{type: 'UserSearch', id: 'LIST'}]
        })
    }),
    overrideExisting: false
})


export const {
    useGetUserSearchWithKeyWordQuery
} = userSearchAPI;


// * Provide tags for cache invalidation when code splitting
// * https://github.com/reduxjs/redux-toolkit/issues/1510

