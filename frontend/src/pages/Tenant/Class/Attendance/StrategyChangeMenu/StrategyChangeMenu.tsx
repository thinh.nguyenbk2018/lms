import { Menu, Tag } from "antd";
import {
	AttendanceState,
	SessionOverallAttendanceRateStrategy,
} from "../../../../../redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import { useMemo } from "react";
import AttendanceTag from "../AttendanceTag";
import React from "react";
import {keyTitleAttendanceStrategy, StrategyTypeTag} from "../MeetingAttendance/MeetingAttendance";

type AttendanceRateStrategyMenuItemType = {
	label: JSX.Element;
	key: SessionOverallAttendanceRateStrategy;
};
type StrategyChangeMenuProps = {
	onSelect: (key: SessionOverallAttendanceRateStrategy) => void;
};

const StrategyChangeMenu = (props: StrategyChangeMenuProps) => {
	const { onSelect } = props;
	const menuItems: AttendanceRateStrategyMenuItemType[] = [
		{
			label: <StrategyTypeTag type={"ANY"} title={keyTitleAttendanceStrategy["ANY"]}/>,
			key: "ANY" as const,
		},
		{
			label: <StrategyTypeTag type={"LAST_TIME"} title={keyTitleAttendanceStrategy["LAST_TIME"]}/>,
			key: "LAST_TIME" as const,
		},
	];
	return (
		<Menu
			items={menuItems}
			onClick={({ key }) => {
				onSelect(key as SessionOverallAttendanceRateStrategy);
			}}
		/>
	);
};

export default React.memo(StrategyChangeMenu);
