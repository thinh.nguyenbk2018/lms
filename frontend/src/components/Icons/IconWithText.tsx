import React from "react";
import {Space} from "antd";
import {
	AliwangwangFilled,
	AliwangwangOutlined,
	DislikeOutlined,
	DislikeTwoTone,
	EyeOutlined,
	LikeOutlined,
	LikeTwoTone,
	ShareAltOutlined,
} from "@ant-design/icons";
import COLOR_SCHEME from "../../constants/ThemeColor";
import './IconWithText.less';

interface Props {
	text?: string;
	onClick?: () => void; //What to do when the icon is clicked
	style?: React.CSSProperties;
}

export const IconText = (props: Props) => <Space>{props.text}</Space>;

interface UpvotesProps extends Props {
	upvoted: boolean;
	numOfUpvotes: number;
}

export const UpvoteIconWithState = (props: UpvotesProps) => {
	return (
		<div className={`upvote-icon ${props.upvoted ? 'upvoted' : ''}`} onClick={props.onClick} style={props.style}>
			<Space>
				{React.createElement(props.upvoted ? LikeTwoTone : LikeOutlined)}
				{props.numOfUpvotes}
			</Space>
		</div>
	);
};

interface DownvoteProps extends Props {
	downvoted: boolean;
	numOfDownvotes: number;
}

export const DownvoteIconWithState = (props: DownvoteProps) => {
	return (
		<div className={`downvote-icon ${props.downvoted ? 'downvoted' : ''}`} onClick={props.onClick} style={props.style}>
			<Space>
				{/*{React.createElement(*/}
				{/*	props.downvoted ? DislikeTwoTone : DislikeOutlined*/}
				{/*)}*/}
				{props.downvoted ? <DislikeTwoTone twoToneColor={"#c91c00"}/> : <DislikeOutlined />}
				{props.numOfDownvotes}
			</Space>
		</div>
	);
};

interface ReplyIconProps extends Props {
	numOfDiscussion: number;
	isOpening: boolean;
}

export const ReplyIconsWithText = (props: ReplyIconProps) => {
	return (
		<div className={'reply-icon'} onClick={props.onClick} style={props.style}>
			<Space>
				{props.isOpening ? (
					<span style={{ color: COLOR_SCHEME.primary }}>
						{React.createElement(AliwangwangFilled)}
					</span>
				) : (
					React.createElement(AliwangwangOutlined)
				)}
				{props.numOfDiscussion}
			</Space>
		</div>
	);
};

interface ViewIconProps extends Props {
	numOfView: number;
}

export const ViewIconWithText = (props: ViewIconProps) => {
	return (
		<div onClick={props.onClick} style={props.style}>
			<Space>
				<EyeOutlined />
				{props.numOfView}
			</Space>
		</div>
	);
};

interface ShareIconProps extends Omit<Props, "onClick"> {
	onClickAction?: (e: any) => void;
}

export const ShareIcon = (props: ShareIconProps) => {
	return (
		<div onClick={props.onClickAction} style={props.style}>
			<Space>
				<ShareAltOutlined />
			</Space>
		</div>
	);
};
