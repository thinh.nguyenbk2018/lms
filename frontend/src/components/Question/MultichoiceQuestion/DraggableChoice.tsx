import React, { FC, useRef } from 'react'
import {Choice, DragAndDropAnswer, MultichoiceQuestion} from './../../../redux/features/Quiz/CreateQuizSlice';
import { useDrag, useDrop } from 'react-dnd';
import { QuizDragTypes } from '../../../constants/DragTypes';
import type { XYCoord, Identifier } from 'dnd-core'
import {Button} from "antd";

// * What the monitor need to know when swapping / dropping the  target
export type MultiChoiceOptionProps = {
    children: JSX.Element;
    index: number;
    move: (from: number, to: number) => void,
    answerKey: Choice["answerKey"]
}
// * What we need to know a bout the item being dragged and how to differentiate them
export interface DragAnswer {
    index: number;
    type: string;
    answerKey: Choice["answerKey"],
    content: string
}
const DraggableChoice = (props: MultiChoiceOptionProps) => {
    const ref = useRef<HTMLDivElement>(null);

    const [{ handlerId }, drop] = useDrop<DragAnswer, DragAnswer, { handlerId: Identifier | null }>({
        accept: QuizDragTypes.Multichoice.Choice,
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId(),
            }
        },
        hover(item, monitor) {
            if(!ref.current) {
                return;
            }
            const dragIdx = item.index;
            const hoverIdx = props.index;
            if (dragIdx == hoverIdx) {
                return;
            }
            props.move(dragIdx, hoverIdx);
            console.log("Hovering ", dragIdx, hoverIdx);
            item.index = hoverIdx;
        },
        drop: (item) => {
            return item
        }
    })


    const [{isDragging,didDrop,endResult}, drag] = useDrag({
        type: QuizDragTypes.Multichoice.Choice,
        item: () => {
            return {answerKey: props.answerKey , index: props.index}
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
            didDrop: monitor.didDrop(),
            endResult: monitor.getDropResult<DragAnswer>()
        }),

    })
    const opacity = isDragging ? 0.5: 1;
    drag(drop(ref))
    return (
        <div ref={ref} className={"draggable-option"} data-hanlder-id={handlerId} style={{opacity}}>
            {
                props.children
            }
        </div>
    )
}

export default DraggableChoice;


