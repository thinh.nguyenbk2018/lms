import React from 'react';
import {isPlayableMedia} from "../Utils/FileUtils";
import ReactPlayer from "react-player";
import {Typography} from "antd";
import {ReactPlayerProps} from "react-player";
type PlayerProps = {
    // ** A playable video file or a link that can be used to retrieve the file */
    fileOrURL: string | File,
    playerProps?: ReactPlayerProps
}
// export const sampleFileUrl = 'https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/videos%2FThinhTestAccount%2Ffile_example_MP4_480_1_5MG.mp4?alt=media&token=43b53808-e786-43ad-b23b-9187270334b8';
export const sampleFileUrl = 'https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/videos%2FThinhTestAccount%2F2022-04-06%2000-58-58.mkv?alt=media&token=64e9f4bf-7142-491a-8c04-9d3b84cf5a38';
/** General purpose media player */
const CustomPlayer = (props: PlayerProps) => {
    const {fileOrURL,playerProps} = props;
    if ((typeof fileOrURL === "string") && ReactPlayer.canPlay(fileOrURL)) {
        return <ReactPlayer {...playerProps} url={fileOrURL} controls={true} width={473}/>;
    }
    else if ((fileOrURL instanceof File && isPlayableMedia(fileOrURL))) {
        const playURL = URL.createObjectURL(fileOrURL);
        return <ReactPlayer {...playerProps} url={playURL} controls={true} width={473}/>;
    }
    else {
        // * Last resort : is URL string but not playable: Try to download the file
        // const downloadedFile =
        return <Typography.Text type={"danger"}>
            "Can't Play this file"
        </Typography.Text>
    }
};

export default CustomPlayer;