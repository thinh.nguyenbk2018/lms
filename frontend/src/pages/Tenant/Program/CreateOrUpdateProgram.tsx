import "./CreateOrUdpateProgram.less";

import { Button, Col, Row, Space } from "antd";
import { Formik, FormikProps, FormikValues } from "formik";
import { Checkbox, Form, FormItem, Input } from "formik-antd";
import { toNumber } from "lodash";
import React, { useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";

import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import { backIconSvg, okeSvgIcon } from "../../../components/Icons/SvgIcons";
import { LocalImageChooserWithFirebase } from "../../../components/ImageChooser/ImageChooserWithFirebase";
import TextEditor from "../../../components/TextEditor/TextEditor";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import API_ERRORS from "../../../redux/features/ApiErrors";
import { isApiError } from "../../../redux/features/CentralAPI";
import {
	useCreateProgramMutation,
	useGetProgramQuery,
	useUpdateProgramMutation,
} from "../../../redux/features/Program/ProgramAPI";
import {
	programSelector,
	setCoursesInProgram,
} from "../../../redux/features/Program/ProgramSlice";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import { DEFAULT_ERROR_MESSAGE } from "../../../utils/RequestHelper";
import ApiErrorRetryPage from "../../UtilPages/ApiErrorRetryPage";
import LoadingPage from "../../UtilPages/LoadingPage";
import SortableCourse from "./SortableCourse";

const ProgramSchema = Yup.object().shape({
	name: Yup.string().required("Tên là trường bắt buộc"),
	code: Yup.string().required("Mã là trường bắt buộc"),
});

const CreateOrUpdateProgram = () => {
	const { id } = useParams();

	const navigate = useNavigate();

	const dispatch = useAppDispatch();

	const [createProgram] = useCreateProgramMutation();

	const [updateProgram] = useUpdateProgramMutation();

	const selectCoursesInProgram = useAppSelector((state) =>
		programSelector.selectCoursesInProgram(state)
	);

	const formikRef = useRef<FormikProps<FormikValues>>(null);

	const getTitleOfPage = () => {
		return id ? "Thông tin chương trinh" : "Tạo chương trình mới";
	};

	const getTitleOfButton = () => {
		return id ? "Cập nhật" : "Tạo mới";
	};

	const isSkip = (idParam: string | undefined) => {
		return isNaN(Number(idParam));
	};

	const { data, isError, isFetching, isSuccess, error } =
		useGetProgramQuery(toNumber(id), { skip: isSkip(id) });

	useEffect(() => {
		dispatch(setCoursesInProgram(data?.courses || []));
	}, [data]);

	if (isSuccess && data) {
		return (
			<>
				{
					<>
						<Formik
							innerRef={formikRef}
							enableReinitialize={true}
							initialValues={data || {}}
							onSubmit={(values, actions) => {
								const dataToBeSent = {
									name: values.name,
									code: values.code,
									description: values.description,
									isStrict: !!values.isStrict,
									isPublished: !!values.isPublished,
									courses: selectCoursesInProgram,
								};
								console.log("data to be sent: ", dataToBeSent);
								if (id) {
									const programUpdated = { ...dataToBeSent, id: toNumber(id) };
									updateProgram(programUpdated)
										.unwrap()
										.then((_result) => {
											AntdNotifier.success(
												"Cập nhật chương trình thành công",
												"Thông báo",
												1
											);
											setTimeout(() => dispatch(setCoursesInProgram([])), 0);
											navigate(ROUTING_CONSTANTS.PROGRAM_LIST);
										})
										.catch((_err) => {});
								} else {
									createProgram(dataToBeSent)
										.unwrap()
										.then((_result) => {
											AntdNotifier.success(
												"Tạo chương trình thành công",
												"Thông báo",
												1
											);
											setTimeout(() => dispatch(setCoursesInProgram([])), 0);
											navigate(ROUTING_CONSTANTS.PROGRAM_LIST);
										})
										.catch((err) => {
											if (
												err.data.error ===
												API_ERRORS.PROGRAM.PROGRAM_CODE_ALREADY_EXISTS
											) {
												AntdNotifier.error(
													"Mã của chương trình đã tồn tại",
													"Thông báo",
													1
												);
											}
										});
								}
								actions.setSubmitting(false);
							}}
							validationSchema={ProgramSchema}
						>
							{({
								isValid,
								isSubmitting,
								dirty,
								values,
								setFieldValue,
								submitForm,
							}) => (
								<>
									<Form layout={"vertical"} initialValues={data}>
										<ContentHeader
											title={getTitleOfPage()}
											action={
												<Space>
													<Button
														type="primary"
														danger
														onClick={() => {
															setTimeout(
																() => dispatch(setCoursesInProgram([])),
																0
															);
															navigate(`${ROUTING_CONSTANTS.PROGRAM_LIST}`);
														}}
														className={"back-btn"}
													>
														{backIconSvg}
														<span>Quay lại</span>
													</Button>
													<Button
														type="primary"
														color="#73d13d"
														onClick={() => submitForm()}
														className={"update-btn"}
														disabled={!dirty || !isValid}
														loading={isSubmitting}
													>
														{okeSvgIcon}
														<span>{getTitleOfButton()}</span>
													</Button>
												</Space>
											}
										/>

										<Row>
											<Col span={12}>
												<FormItem label="Tên" name="name" required={true}>
													<Input name={"name"} />
												</FormItem>
											</Col>
											<Col span={6} offset={1}>
												<FormItem
													label="Mã chương trình (duy nhất)"
													name="code"
													required={true}
												>
													<Input name={"code"} disabled={!isNaN(Number(id))} />
												</FormItem>
											</Col>
										</Row>
										<Row>
											<Col span={24}>
												<FormItem label="Mô tả khóa học" name="description">
													<TextEditor
														id={"program-information"}
														onChange={(d: string) =>
															setFieldValue("description", d)
														}
														value={values.description}
													/>
												</FormItem>
											</Col>
										</Row>
										<Row>
											<Col span={10}>
												<FormItem
													className={"strict-checkbox"}
													name="isStrict"
													label="Áp dụng học theo lộ trình"
												>
													<Checkbox name="isStrict" />
												</FormItem>
											</Col>
											<Col span={10} offset={4}>
												<FormItem
													className={"published-checkbox"}
													name="isPublished"
													label="Công khai"
												>
													<Checkbox name="isPublished" />
												</FormItem>
											</Col>
										</Row>
										<Row>
											<FormItem label="Ảnh nền" name="background">
												<LocalImageChooserWithFirebase
													callback={(url: string) =>
														setFieldValue("background", url)
													}
												/>
											</FormItem>
										</Row>
									</Form>
									<SortableCourse
										setField={setFieldValue}
										data={values.courses}
									/>
								</>
							)}
						</Formik>
					</>
				}
			</>
		);
	} else if (isFetching) {
		return <LoadingPage />;
	} else if (isError) {
		if (isApiError(error)) {
			let errorMessage = DEFAULT_ERROR_MESSAGE;
			const {
				response: {
					data: { code},
				},
			} = error;
			if (code === API_ERRORS.PROGRAM.PROGRAM_NOT_FOUND) {
				errorMessage = "Không tìm thấy program";
			}
			AntdNotifier.apiError(errorMessage);
		}
		return <ApiErrorRetryPage />;
	} else return <></>;
};

export default CreateOrUpdateProgram;
