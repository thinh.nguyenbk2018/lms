import {
    createSliceEnhancedWithSearch,
    EnhancedSliceWithSearch,
    generateInitialSearchSlice
} from '../../utils/createSliceWithSearch/createSliceEnhancedWithSearch';
import {RootState} from "../../store/store";
import {PayloadAction} from '@reduxjs/toolkit';


export type SessionID = string;

type WrappedSliceState = {
    currentTestSessionId: SessionID
}


type TestWithConfigInfoSliceState = EnhancedSliceWithSearch<WrappedSliceState>;

export const DEFAULT_SESSION_ID = "";

const initState: TestWithConfigInfoSliceState = {
    currentTestSessionId: DEFAULT_SESSION_ID,
    ...generateInitialSearchSlice()
}

export const TEST_SESSION_SLICE_KEY = "testSession";

export const testSessionSlice = createSliceEnhancedWithSearch({
    name: TEST_SESSION_SLICE_KEY,
    initialState: initState,
    reducers: {
        endTestSession: (state) => {
            state.currentTestSessionId = DEFAULT_SESSION_ID;
        },
        setCurrentTestSessionId: (state, action: PayloadAction<SessionID>) => {
            state.currentTestSessionId = action.payload;
        }
    }
});

export const testSessionActions = testSessionSlice.actions;


export const testSessionSelectors = {
    selectSearchCriteria: (state: RootState) => state.testSession.searchCriteria,
    selectCurrentSessionId: (state: RootState) => state.testSession.currentTestSessionId
};


