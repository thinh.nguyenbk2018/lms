import { Button, Card, Col, Form, Input, Modal, Row, Typography } from "antd";
import React, { forwardRef, Ref, useEffect, useImperativeHandle, useState } from "react";
import TextEditor from "../../TextEditor/TextEditor";
import { LocalImageChooserWithFirebase } from "../../ImageChooser/ImageChooserWithFirebase";
import FileManager from "../../FileManager/FileManager";

export interface HandleSubmitCourseInformation {
    getCourseInformation: () => { courseInformation: ICourseInformation };
    validateCourseContent: () => Promise<boolean>;
    clear: () => void
}

export interface ICourseInformation {
    id: number;
    name: string;
    code: string;
    level: string;
    price?: number;
    background?: string;
    description?: string;
    createdAt?: Date;
    programs?: string;
}

const initialCourseInformation: ICourseInformation = {
    id: 0,
    name: '',
    code: '',
    level: ''
}

interface CourseInformationProps {
    initialData?: ICourseInformation
}

const CourseInformation = ({ initialData }: CourseInformationProps, ref: Ref<HandleSubmitCourseInformation>) => {
    const [courseInformationForm] = Form.useForm();
    const [courseInformation, setCourseInformation] = useState(initialData ? initialData : initialCourseInformation);
    const [showFileBrowser, setShowFileBrowser] = useState(false);
    useEffect(() => setCourseInformation(initialData || initialCourseInformation), [initialData]);
    useEffect(() => courseInformationForm.resetFields(), [initialData]);
    useImperativeHandle(ref, () => ({
        getCourseInformation: () => {
            return { courseInformation };
        },
        validateCourseContent: () => {
            return courseInformationForm.validateFields()
                .then(() => true)
                .catch(() => false);
        },
        clear: () => {
            courseInformationForm.resetFields();
        }
    }));

    const onValuesChange = (data: any) => {
        setCourseInformation({ ...courseInformationForm.getFieldsValue() });
    }

    const setBackground = (url: string) => {
        setCourseInformation({ ...courseInformation, background: url });
    }

    const textEditorOnChange = (data: any) => {
        setCourseInformation({
            ...courseInformation,
            description: data
        });
    };

    return (
        <div className={'course-information-container'}>
            <Card
                title={
                    <Typography.Title level={5}>Thông tin cơ bản khóa học</Typography.Title>
                }
                bodyStyle={{ marginTop: "-1rem" }}
            >
                <Form
                    form={courseInformationForm}
                    layout={'vertical'}
                    onValuesChange={onValuesChange}
                    initialValues={initialData}
                >
                    <Row>
                        <Col span={14}>
                            <Form.Item
                                label='Tên'
                                name='name'
                                rules={[{ required: true, message: 'Tên là phần bắt buộc!' }]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col span={9} offset={1}>
                            <Form.Item
                                label='Mã khóa học'
                                name='code'
                                rules={[{ required: true, message: 'Mã khóa học là phần bắt buộc!' }]}
                            >
                                <Input disabled={initialData?.id != null && initialData.id !== 0} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={7}>
                            <Form.Item
                                label='Trình độ'
                                name='level'
                                required={true}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col span={6} offset={1}>
                            <Form.Item
                                label='Giá (VND)'
                                name='price'
                            >
                                <Input type={"number"} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Form.Item
                                label="Mô tả khóa học"
                                name="description"
                            // initialValue={initialData?.content}
                            >
                                <TextEditor id={'course-information'} onChange={textEditorOnChange} value={courseInformation?.description} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Form.Item
                                label='Ảnh nền'
                            >
                                <LocalImageChooserWithFirebase callback={setBackground} />
                                <Button onClick={() => setShowFileBrowser(true)}>
                                    Chọn ảnh
                                </Button>
                                <Modal visible={showFileBrowser} width={"80%"} footer={null}>
                                    <div style={{ padding: "25px 20px 0px 20px" }}>
                                        <FileManager onSelectFile={({ fileUrl }) => {
                                            setShowFileBrowser(false);
                                            console.log(fileUrl);
                                        }} />
                                    </div>
                                </Modal>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Card>
        </div>
    );
}

export default forwardRef(CourseInformation);