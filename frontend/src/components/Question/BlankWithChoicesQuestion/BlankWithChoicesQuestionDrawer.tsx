import {ExclamationCircleOutlined, SettingOutlined} from "@ant-design/icons";
import { Button, Card, Col, Drawer, Modal, Row, Select as AntdSelect, Space, Typography, } from "antd";
import React, { useEffect, useRef, useState } from "react";
import CancelButton from "../../ActionButton/CancelButton";
import DoneButton from "../../ActionButton/DoneButton";
import {
    BlankWithChoicesQuestion,
    BlankWithOption,
    Choice,
    isBlankWithChoicesQuestion,
} from "@redux/features/Quiz/CreateQuizSlice";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import { Field, Formik, useFormikContext } from "formik";
import * as Yup from "yup";
import { choiceSchema } from "../MultichoiceQuestion/MultiChoiceQuestionDrawer";
import { Form, FormItem, Input, Select } from "formik-antd";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import {
    getContentInsideSpecialSpanNode,
    replaceBlankWithSpecialSpanButKeepContent,
} from "./BlankWithChoicesQuestionDisplay";
import COLOR_SCHEME from "../../../constants/ThemeColor";
import "./BlankWithChoicesQuestionDrawer.less";
import { DEFAULT_FRONTEND_ID } from "@redux/interfaces/types";
import { baseCreateQuestionYupSchema, baseEditQuestionYupSchema, } from "../QuestionContainerItem/baseQuestionSchema";
import useQuestionDrawerType from "../../../hooks/QuizDrawer/useQuestionDrawerType";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import ReactHtmlParser from "html-react-parser";
import { DND_BLANK_SPAN_TAG_CLASS } from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import QUESTION_LABEL_CONSTANT from "../constant/QuestionLabelConstant";
import AttachmentQuestion, { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

const QUESTION_STAGE = {
    STAGE_ONE: 1,
    STAGE_TWO: 2,
};
// * Find blank regex: /\[.*?\]/g
// * Dealing with nested brackets: (\[(?:\[??[^\[]*?\]))
// * Credits: https://stackoverflow.com/questions/2403122/regular-expression-to-extract-text-between-square-brackets
// * Extracting the whole sentence containing the blank RegEx:  /[^.?!]*(?<=[.?\s!])flung(?=[\s.?!])[^.?!]*[.?!]/g
// * Credits: https://stackoverflow.com/questions/26081820/regular-expression-to-extract-whole-sentences-with-matching-word/48533652

const findTextsInsideBrackets = (string: string) => {
    return string.match(/\[.*?\]/g);
};

const { Option } = Select;

const BlankWithChoicesInDrawer = (
    props: BlankWithOption & { index: number; mode: "create" | "doing" }
) => {
    const { index, hint = "", answerList, correctAnswerKey } = props;
    useEffect(() => {
        if (props.mode === "create") {
            setFieldValue(`blankList[${index}]`, {
                hint,
                answerList,
                correctAnswerKey,
            });
        }
    }, []);
    const { setFieldValue } =
        useFormikContext<BlankWithChoicesQuestionDrawerFormValues>();
    return (
        <>
            <Select
                allowClear={true}
                placeholder={"Chọn đáp án"}
                name={`blankList[${index}].correctAnswerKey`}
                onChange={(value, option) => {
                    // console.log("CHANGED VALUE AND QUESTION",value,option);
                    setFieldValue(`blankList[${index}].correctAnswerKey`, value);
                }}
            >
                {answerList.map((item, index) => (
                    <Option key={item.answerKey} value={item.answerKey}>
                        {item.content}
                    </Option>
                ))}
            </Select>
        </>
    );
};

const replaceBlankNotationByBlankWithChoicesInDrawer = (
    questionHTML: string,
    callbakOnAnswerList?: Function,
    blankList?: BlankWithOption[]
) => {
    const [questionReviewHTML, count] =
        replaceBlankWithSpecialSpanButKeepContent(questionHTML);
    let blankCounter = 0;

    const parseAnswersInBlank = (question: string) => {
        return question.substring(1, question.length - 1).split(";");
    };
    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if (
                (node as any).name === "span" &&
                (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
            ) {
                // const answers = parseAnswersInBlank();
                // console.log("Span node sample: ", node);
                blankCounter++;
                const blankIndex = blankCounter - 1;
                const answerTexts = parseAnswersInBlank(
                    getContentInsideSpecialSpanNode(node)
                );
                const answerList: Choice[] = answerTexts.map((item, index) => {
                    return {
                        content: item,
                        answerKey: index,
                        isCorrect: false,
                        order: index,
                        isChosenByStudent: false
                    };
                });
                callbakOnAnswerList?.(blankIndex, answerList);
                // console.log('blankList: ', blankList);
                // console.log('answerList: ', answerList);
                const correctAnswerKey = blankList == undefined || blankIndex >= blankList.length ? undefined : blankList?.[blankIndex].correctAnswerKey;
                return (
                    <BlankWithChoicesInDrawer
                        hint={""}
                        answerList={answerList}
                        correctAnswerKey={correctAnswerKey}
                        index={blankIndex}
                        mode={"create"}
                    />
                );
            }
            return node;
        },
    });
};

const BlankWithChoicesQuestionDrawer = () => {
    const [stage, setStage] = useState(QUESTION_STAGE.STAGE_ONE);

    // * The drawer state
    const {
        visible,
        closeDrawer,
        openDrawer,
        isEditing,
        questionDataInDraft,
        doneHandler,
        isFetching,
        isError,
        isSuccess,
        questionIdInDraft,
        testName,
    } = useQuestionDrawerType("FILL_IN_BLANK_WITH_CHOICES");

    const drawerRef = useRef<HTMLDivElement | null>(null);

    function closeConfirm(onOk: () => void, onCancel: () => void) {
        Modal.confirm({
            title: "Xác nhận hủy bỏ câu hỏi",
            icon: <ExclamationCircleOutlined />,
            content:
                "Chúng tôi nhận thấy rằng bạn đã soạn phần nào câu hỏi. Chọn xác nhận sẽ xóa hết tất cả.",
            okText: "Xác nhận",
            cancelText: "Tiếp tục soạn",
            onOk: onOk,
            onCancel: onCancel,
            getContainer: drawerRef.current!!,
        });
    }

    const renderBlankConfigForms = (question: string, blankList: BlankWithOption[]) => {
        return (
            <Card
                className={"question-previewer"}
                style={{ width: `100%` }}
                title={
                    <Typography.Text strong style={{ color: COLOR_SCHEME.primary }}>
                        Xem trước câu hỏi
                    </Typography.Text>
                }
            >
                {replaceBlankNotationByBlankWithChoicesInDrawer(question, () => {
                }, blankList) || (
                        <Typography.Text type={"danger"}>
                            Không tìm thấy ô trống
                        </Typography.Text>
                    )}
            </Card>
        );
    };

    const setStageTwo = () => {
        setStage(QUESTION_STAGE.STAGE_TWO);
    };
    const setStageOne = () => {
        // TODO: Check if each blank contains at least two options
        setStage(QUESTION_STAGE.STAGE_ONE);
    };
    const contentByStage = (stage: number, values: any, setFieldValue: (key: string, value: any) => void, errors: any, touched: any, validateForm: any) => {
        const errorsMsg = touched['blankList'] && errors['blankList'];
        switch (stage) {
            case QUESTION_STAGE.STAGE_ONE:
                return (
                    <>
                        <div ref={drawerRef}/>
                        <Row align={"top"} justify={"end"}>
                            <Col>
                                <FormItem name={"point"} label={QUESTION_LABEL_CONSTANT.POINT}
                                    initialValue={values.point}>
                                    <Input type={"number"} name={"point"} value={values.point}
                                        onChange={({ currentTarget: { value } }) => {
                                            // console.log('point', value);
                                            setFieldValue("point", Number(value))
                                        }} />
                                </FormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Typography.Paragraph>
                                Để định dạng bài kiểm tra của bạn, đóng ngoặc vuông cho các câu
                                trả lời và ngăn cách giữa các lựa chọn với dấu ";". Ví dụ: Con
                                chó của Lan có tên là [Jack; Lucy; Lyce]. Sẽ được hiển thị như
                                sau trong bài kiểm tra:
                                {
                                    <Card>
                                        <Space>
                                            <Typography.Paragraph>
                                                Con chó của Lan có tên là
                                            </Typography.Paragraph>
                                            <AntdSelect
                                                defaultValue="lucy"
                                                style={{ width: 120 }}
                                                onChange={() => {
                                                }}
                                            >
                                                <Option value="jack">Jack</Option>
                                                <Option value="lucy">Lucy</Option>
                                                <Option value="Lyce">Lyce</Option>
                                            </AntdSelect>
                                        </Space>
                                    </Card>
                                }
                                <br />
                                <Typography.Text type={"danger"} style={{color: "#c91c00"}}>
                                    * Lưu ý: Mỗi ô trống phải có từ 2 lựa chọn trở lên
                                </Typography.Text>
                            </Typography.Paragraph>
                        </Row>
                        <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                            <Col span={24}>
                                <Field
                                    name={"attachment"}
                                    placeholder={"Type article content"}
                                    component={AttachmentQuestion}
                                    label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                                />
                            </Col>
                        </Row>
                        <Row justify={"center"}>
                            <div style={{ display: "flex", flexDirection: "column", width: "100%" }}>
                                <Row className={'question-drawer-text-editor-wrapper'}>
                                    <Field
                                        name={"description"}
                                        placeholder={""}
                                        component={TextEditorField}
                                        label={"Mô tả đề"}
                                    />
                                </Row>
                                <Button
                                    // style={{ margin: "1.3em" }}
                                    onClick={() => {
                                        // validateForm()
                                        //     .then((res: any) => {
                                        //         // console.log('validate: ', res);
                                        //         if (res["blankList"] == undefined) {
                                        //
                                        //         }
                                        //     })
                                        setStageTwo();
                                    }}
                                    type={"primary"}
                                    style={{
                                        alignSelf: "end",
                                    }}
                                    icon={<SettingOutlined />}
                                >
                                    Cấu hình cho các ô trống
                                </Button>
                            </div>
                        </Row>
                    </>
                );
            case QUESTION_STAGE.STAGE_TWO:
                return (
                    <>
                        <div className={"blanks-with-choices-config-container"}>
                            {
                                JSON.parse(values.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
                                    <AttachmentRender attachment={a} />
                                </Row>)
                            }
                            {renderBlankConfigForms(values.description, values.blankList)}
                            <span style={{color: "#c91c00"}}>{errorsMsg != undefined && errorsMsg}</span>
                            <div style={{display: 'flex', justifyContent: 'flex-end', width: "100%"}}>
                                <Button style={{marginTop: "1rem"}} onClick={setStageOne}>Quay lại</Button>
                            </div>
                        </div>
                    </>
                );
            default:
                return <h1>Đã xảy ra lỗi, vui lòng tải lại trang</h1>;
        }
    };

    if (questionDataInDraft && !isBlankWithChoicesQuestion(questionDataInDraft)) {
        AntdNotifier.error(
            "Lỗi khi load câu hỏi, vui lòng refresh lại hoặc contact chúng tôi"
        );
        return null;
    }

    let initialValues = {
        id: questionDataInDraft?.id || DEFAULT_FRONTEND_ID,
        description:
            questionDataInDraft?.description || DEFAULT_TEXT_EDITOR_HTML,
        point: questionDataInDraft?.point || 0,
        note: questionDataInDraft?.note || "",
        blankList: questionDataInDraft?.blankList || [],
        attachment: questionDataInDraft?.attachment || '[]'
    };
    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={initialValues}
                validateOnChange={false}
                validationSchema={Yup.object({
                    blankList: Yup.array().of(blankWithOptionSchema)
                        .test("Check question", "Tất cả ô trống phải có nhiều hơn 2 sự lựa chọn", list => {
                            console.log("blankList: ", list);
                            return !(list == undefined || list.filter(x => x.answerList == undefined || x.answerList.length < 2).length > 0);
                        })
                        .test("Check answer", "Tất cả ô trống phải được cấu hình đáp án đúng", list => {
                            // console.log('list: ', list);
                            return !(list == undefined || list.filter(x => x['correctAnswerKey'] == undefined || x['correctAnswerKey'] < 0).length > 0);
                        })
                }).concat(
                    isEditing ? baseEditQuestionYupSchema : baseCreateQuestionYupSchema
                )}
                onSubmit={(values, {resetForm}) => {
                    const valuesToSubmit: BlankWithChoicesQuestion = {
                        type: "FILL_IN_BLANK_WITH_CHOICES",
                        order: 0,
                        ...values,
                    };
                    doneHandler(valuesToSubmit);
                    resetForm({values: initialValues});
                    setStageOne();
                    // console.log("BWCHOICES_Question values: ", valuesToSubmit);
                }}
            >
                {({
                    dirty,
                    isValid,
                    handleSubmit,
                    setFieldValue,
                    values,
                    errors,
                    resetForm,
                    touched,
                    validateForm
                }) => {
                    return (
                        <Form>
                            <Drawer
                                maskClosable={false}
                                closable={false}
                                title={
                                    <Space direction={"vertical"}>
                                        <Typography.Text>
                                            {`${testName || "Không thể hiển thị tên đề kiểm tra"}`}
                                        </Typography.Text>
                                        <Typography.Text strong>Điền vào chỗ trống với các sự lựa chọn</Typography.Text>
                                    </Space>
                                }
                                placement="right"
                                size={"large"}
                                onClose={() => {
                                    // * Check if there was data and add for confirmation
                                    const question = values.description;

                                    if (
                                        (question && question !== "<p><br></p>") ||
                                        stage !== QUESTION_STAGE.STAGE_ONE
                                    ) {
                                        closeConfirm(
                                            () => {
                                                closeDrawer();
                                                setStage(QUESTION_STAGE.STAGE_ONE);
                                                setFieldValue("description", "");
                                            },
                                            () => {
                                            }
                                        );
                                    } else {
                                        closeDrawer();
                                        setFieldValue("description", "");
                                    }
                                }}
                                visible={visible}
                                extra={
                                    <Space>
                                        <CancelButton
                                            onClick={() => {
                                                if (dirty) {
                                                    closeConfirm(
                                                        () => {
                                                            closeDrawer();
                                                            setStage(QUESTION_STAGE.STAGE_ONE);
                                                            setFieldValue("description", "");
                                                        },
                                                        () => {
                                                        }
                                                    );
                                                } else {
                                                    closeDrawer();
                                                    setFieldValue("description", "");
                                                }
                                            }
                                            }
                                        />
                                        <DoneButton
                                            disabled={stage === QUESTION_STAGE.STAGE_ONE || !dirty}
                                            onClick={() => handleSubmit()}
                                        />
                                    </Space>
                                }
                            >
                                {contentByStage(stage, values, setFieldValue, errors, touched, validateForm)}
                            </Drawer>
                        </Form>
                    );
                }}
            </Formik>
        </>
    );
};

const blankWithOptionSchema = Yup.object({
    hint: Yup.string(),
    answerList: Yup.array().of(choiceSchema)
});
export type BlankWithChoicesQuestionDrawerFormValues = Pick<BlankWithChoicesQuestion,
    "description" | "blankList">;

export default BlankWithChoicesQuestionDrawer;
