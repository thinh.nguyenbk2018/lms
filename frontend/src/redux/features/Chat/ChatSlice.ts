import {ID} from "../../interfaces/types";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../store/store";


export type ChatBoxState = "hidden" | "active" | "minimized";

export type ChatRoomMetaState = {
    id: ID,
    state: ChatBoxState
}


export interface Entity<T> {
    [id: ID]: T;
}

type InitialState = {
    showChatSearchBox: boolean;
    entities: Entity<ChatRoomMetaState>;
    uninitializedChatRooms?: Entity<ChatRoomMetaState>;
}
const initialState : InitialState = {
    showChatSearchBox: false,
    entities: {
    }
};
/*
    * CLick on user found
    *   - Create new transient chat box
    *   - If user conv existed --> Receive Real Active Chat --> Add to chat slice in active state
    *   - If user conv not existed --> Receive Fake Active Chat --> With no Chat Room Id and only user id and empty list message
    *
 */
// ? Redux reduces can modify state directly (No need to spread object / array if we know key / index)
export const chatSlice = createSlice({
    name: 'chat',
    initialState,
    reducers: {
        showChatSearchBox: (state) => {
            state.showChatSearchBox = true;
        },
        hideChatSearchBox: (state) => {
            state.showChatSearchBox = false;
        },
        minimizeChat: (state, action: PayloadAction<ID>) => {
            state.entities[action.payload].state = "minimized";
        },
        hideChat: (state, action: PayloadAction<ID>) => {
            state.entities[action.payload].state = "hidden";
        },
        showChat: (state, action: PayloadAction<ID>) => {
            state.entities[action.payload].state = "active";
        },
        setChatState: (state,action: PayloadAction<{id:ID, state: ChatBoxState}>) =>{
            const {id,state:newChatState} = action.payload;
            state.entities[id].state = newChatState;
        },
        setChatList: (state, action: PayloadAction<ID[] | undefined>) => {
            const {payload} = action;
            const newEntities = {};
            if (!payload) {
                return;
            }
            payload.forEach((item) => {
                // @ts-ignore
                newEntities[item] = {
                    id: item,
                    state: "hidden",
                }
            });
            state.entities = newEntities;
        },
        addTemporaryChatWithUserId: (state, action: PayloadAction<ID>) => {
           const userID = action.payload;
            state.entities[userID] = {
                id: userID,
                state: "active",
            }
        },
        removeTemporaryChatWithUserId: (state, action: PayloadAction<ID>) => {
            const userID = action.payload;
            delete state.entities[userID];
        }

    },
});

export const chatSliceSelector = {
    selectActiveChats: (state: RootState) => Object.values(state.chat.entities).filter(item => item.state === "active"),
    selectMinimizedChats: (state: RootState) => Object.values(state.chat.entities).filter(item => item.state === "minimized"),
    selectHiddenChats: (state: RootState) => Object.values(state.chat.entities).filter(item => item.state === "hidden"),

    selectShowChatSearchBox: (state: RootState) => state.chat.showChatSearchBox,
}

export const {minimizeChat, hideChat, showChat, setChatState, showChatSearchBox,hideChatSearchBox,setChatList,
    addTemporaryChatWithUserId,removeTemporaryChatWithUserId} = chatSlice.actions;

export default chatSlice.reducer;





