import {Button, Col, Collapse, Divider, Form, Input, Modal, Row, Spin, Typography} from "antd";
import LimitOfPermission from "../Permission/LimitOfPermission/LimitOfPermission";
import CustomEmpty from "../../Util/CustomEmpty";
import AddPermission from "../Permission/AddPermission/AddPermission";
import React, {forwardRef, Ref, useImperativeHandle, useState} from "react";
import {
    PermissionOfRole,
    RoleResponse,
    useCreateRoleMutation,
    useUpdateRoleMutation
} from "../../../redux/features/Permission/RolesAPI";
import {OriginalPermission} from "../../../redux/features/Permission/PermissionAPI";
import LIMIT_VALUE from "../../../constants/LimitValue";
import PERMISSION_MAPPING from "../../../constants/LimitPermissionMapping";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {DeleteOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import './RoleForm.less';


const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
};

export interface CreateRoleFormHandle {
    submitCreateForm: () => any,
    submitEditForm: () => any
}

const RoleForm = ({ role }: { role?: RoleResponse }, ref: Ref<CreateRoleFormHandle>) => {

    let [form] = Form.useForm();

    let [permissions, setPermissions] = useState<PermissionOfRole[]>(
        role?.permissions
            ? role?.permissions.map(permission => ({ ...permission }))
            : []
    );

    let [createRole, { isLoading: isCreating }] = useCreateRoleMutation();
    let [updateRole, { isLoading: isUpdating }] = useUpdateRoleMutation();

    const deletePermission = (permissionId: number) => {
        setPermissions(prevState => prevState.filter(permission => permission.id !== permissionId));
    }

    const onAddPermission = (permission: OriginalPermission) => {
        setPermissions(prevState => [
            ...prevState,
            {
                id: permission.id,
                title: permission.title,
                code: permission.code,
                description: permission.description,
                isLimitByBranch: permission.hasLimitByBranch ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
                isLimitByDean: permission.hasLimitByDean ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
                isLimitByLearn: permission.hasLimitByLearn ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
                isLimitByManager: permission.hasLimitByManager ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
                isLimitByTeaching: permission.hasLimitByTeaching ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
            }
        ])
    }

    useImperativeHandle(ref, () => ({
        submitCreateForm: () => {
            return createRole({
                title: form.getFieldValue("title"),
                description: form.getFieldValue("description"),
                permissions: permissions
            })
        },
        submitEditForm: () => {
            return updateRole({
                roleId: role!.id,
                title: form.getFieldValue("title"),
                description: form.getFieldValue("description"),
                permissions: permissions
            })
        }
    }))

    const onEditLimit = (key: string, value: boolean, index: number) => {
        let newPermissions = [...permissions];
        newPermissions[index][key as keyof typeof PERMISSION_MAPPING] = value ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.UNLIMIT;
        setPermissions(newPermissions);
    }

    const renderPermissionHeader = (permission: PermissionOfRole) => (
        <Row style={{ width: '100%' }} align="middle">
            <Col span={16}>
                {permission.code} - {permission.title}
            </Col>
            <Col span={8}>
                <Row justify="end">
                    <DeleteOutlined
                        className={'delete-item-btn'}
                        onClick={(e) => {
                            e.stopPropagation();
                            Modal.confirm({
                                wrapClassName: 'confirm-delete-modal-wrapper',
                                title: (
                                    <span className={'confirm-delete-modal-title-name'}>Xóa quyền khỏi vai trò. Bạn có chắc chắn?</span>
                                ),
                                content: (
                                    <span>Bạn đang xóa quyền <span
                                        className={'confirm-delete-modal-subject-name'}>{permission.title}</span> khỏi vai trò.</span>
                                ),
                                onOk: () => {
                                    deletePermission(permission.id);
                                },
                                icon: <ExclamationCircleOutlined/>,
                                okText: `Xóa quyền`,
                                cancelText: 'Hủy',
                                okButtonProps: {className: 'confirm-delete-modal-ok-btn'},
                                cancelButtonProps: {className: 'confirm-delete-modal-cancel-btn'}
                            })
                        }}
                    />
                </Row>
            </Col>
        </Row>
    )

    return <Spin spinning={isCreating || isUpdating}>
        <Divider orientation="left" orientationMargin={5}><Typography.Title level={5}>Thông tin chung</Typography.Title></Divider>
        <Row justify={"center"}>
            <Col lg={12} md={18} sm={24}>
                <Form form={form} {...layout}>
                    <Form.Item
                        name={"title"}
                        label={"Tên vai trò"}
                        initialValue={role?.title || ""}
                        required={true}
                    >
                        <Input placeholder={"Nhập tên vai trò..."} />
                    </Form.Item>
                    <Form.Item
                        name={"description"}
                        label={"Mô tả"}
                        initialValue={role?.description || ""}
                    >
                        <Input.TextArea placeholder={"Nhập mô tả..."} />
                    </Form.Item>
                </Form>
            </Col>
        </Row>
        <Divider orientation="left" orientationMargin={5}><Typography.Title level={5}>Các quyền vai trò đang có</Typography.Title></Divider>
        <Row justify={"center"}>
            <Collapse style={{ width: '90%' }}>
                {permissions.map((permission, index) => (
                    <Collapse.Panel
                        key={index}
                        header={renderPermissionHeader(permission)}
                    >
                        <div style={{ paddingTop: 10, paddingLeft: 20 }}>
                            <Typography.Text><b>Mô tả:</b> {permission.description}</Typography.Text>
                            <LimitOfPermission permission={permission}
                                onEditLimit={(key, value) => onEditLimit(key, value, index)} />
                        </div>
                    </Collapse.Panel>
                ))}
            </Collapse>
            {permissions.length === 0 && <CustomEmpty description={"Quyền chưa có vai trò"} />}
        </Row>
        <AddPermission hadPermissions={permissions} onAddPermission={onAddPermission} />
    </Spin>
}

export default forwardRef(RoleForm);