import React from "react";
import {Card, Space, Typography} from "antd";
import {CalendarEvent} from "../../../redux/features/Calendar/CalendarAPI";
import {format} from "date-fns";
import {getEventEndTime, getEventStartTime} from "../Utils/CalendarUtils";
import "./EventDetail.less";
import CalendarNewEventFormModal from "../Forms/CalendarNewEventFormModal";
import {vi} from "date-fns/locale";

const TextFieldWithTitle = ({
                                title,
                                content,
                            }: {
    title: JSX.Element | string;
    content: JSX.Element | string;
}) => {
    return (
        <>
            <Typography.Text strong>{title}:</Typography.Text>
            <Typography.Text>{content}</Typography.Text>
        </>
    );
};

const getType = (type: string) => {
    if (type == 'CLASS_SESSION') {
        return 'Buổi học';
    }
    if (type == 'QUIZ_DEADLINE') {
        return 'Kết thúc bài kiểm tra'
    }
    if (type == 'QUIZ_OPEN') {
        return 'Bắt đầu bài kiểm tra';
    }
    return '';
}

// * This is the form that PopOver when the user hover the event on the calendar
const EventDetail = (eventInfo: CalendarEvent) => {
    const [showUpdateForm, setShowUpdateForm] = React.useState(false);
    return (
        <div className={"event-detail-container"}>
            <Card
                title={getType(eventInfo.eventType)}
                extra={
                    <Space>
                        <CalendarNewEventFormModal/>
                        {/*<Button*/}
                        {/*    onClick={() => {*/}
                        {/*        setShowUpdateForm(true);*/}
                        {/*    }}*/}
                        {/*>*/}
                        {/*    Update*/}
                        {/*</Button>*/}
                    </Space>
                }
            >
                {eventInfo.eventType != 'CLASS_SESSION' && <p>{'Bài kiểm tra: ' + eventInfo.summary}</p>}
                <Typography.Text>
                    <Typography.Text strong>Bắt đầu:</Typography.Text>{" "}
                    {format(getEventStartTime(eventInfo), 'P HH:mm', {locale: vi})}
                </Typography.Text>
                <br/>
                <Typography.Text>
                    <Typography.Text strong>Kết thúc:</Typography.Text>{" "}
                    {format(getEventEndTime(eventInfo), 'P HH:mm', {locale: vi})}
                </Typography.Text>
                {/*<Divider/>*/}
                {/*<Typography.Text>*/}
                {/*    /!*Created At: {formatRelative(new Date(eventInfo.created), new Date())}*!/*/}
                {/*</Typography.Text>*/}
                {/*<br/>*/}
                {/*<Typography.Text>*/}
                {/*    By: {JSON.stringify(eventInfo.creator)}*/}
                {/*</Typography.Text>*/}
            </Card>
        </div>
    );
};

export default EventDetail;
