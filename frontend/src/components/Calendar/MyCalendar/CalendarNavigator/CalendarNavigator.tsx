import React, { useEffect, useState } from "react";
import { Button, Tooltip, Typography } from "antd";
import { BulbTwoTone } from "@ant-design/icons";
import classNames from "classnames";
import { useAppSelector, useAppDispatch } from "../../../../hooks/Redux/hooks";
import {
	calendarSelector,
	calendarActions,
} from "../../../../redux/features/Calendar/CalendarSlice";
import {
	DateMatrix,
	DateArray,
	takeMonth,
	getLastMonthDate,
	getMonthName,
	getNextMonthDate,
	weekNames,
} from "../../Utils/CalendarUtils";
import "./CalendarNavigator.less";
import isToday from "date-fns/isToday";

const CalendarNavigator = () => {
	const [month, setMonth] = useState<DateMatrix>([]);
	const [week, setWeek] = useState<DateArray>([]);
	const [dateNuclear, setDateNuclear] = useState<Date>(new Date());

	const selectedDate = useAppSelector(calendarSelector.selectCurrentDate);
	const dispatch = useAppDispatch();

	useEffect(() => {
		const monthGetter = takeMonth(dateNuclear);
		setMonth(monthGetter());
		return () => {};
	}, [dateNuclear]);
	console.log("IS TODAY WORK ?: ", isToday(new Date()));
	return (
		<div>
			<div className="calendar-navigator__title">CALENDAR NAVIGATOR</div>
			<div className={"calendar-container"}>
				<div className={"calendar-header-container"}>
					<div className={"calendar-month-year-header"}>
						<Button
							className={"calendar-nav-button"}
							type={"link"}
							ghost
							onClick={() => setDateNuclear(getLastMonthDate(dateNuclear))}
						>
							Backward
						</Button>
						<Typography.Text strong className={"month-year-title"}>
							{getMonthName(dateNuclear)}, {dateNuclear.getFullYear()}
							{
								<Tooltip title={"Go to selected date"}>
									<BulbTwoTone onClick={() => setDateNuclear(selectedDate)} />
								</Tooltip>
							}
						</Typography.Text>
						<Button
							className={"calendar-nav-button"}
							type={"link"}
							ghost
							onClick={() => setDateNuclear(getNextMonthDate(dateNuclear))}
						>
							Forward
						</Button>
					</div>
					<div className={"calendar-date-of-week-header"}>
						{weekNames.map((item, index) => (
							<div key={index} className={"date-of-week-title"}>
								<Typography.Text strong>{item}</Typography.Text>
							</div>
						))}
					</div>
				</div>
				<div className={"calendar-content"}>
					{month.flat(2).map((date, index) => {
						if (isToday(date)) {
							console.log("Date found as today: ", date);
						}

						return (
							<div
								className={classNames({
									"calendar-date": true,
									"date-is-today": isToday(date),
									"selected-date":
										date.getMonth() === selectedDate.getMonth() &&
										date.getDate() === selectedDate.getDate(),
								})}
								onClick={() => {
									console.log("Selecting date: ", date);
									dispatch(calendarActions.setSelectedDate(date));
								}}
							>
								{/* <div className={"calendar-date-indicator-number"}> */}
								<div
									className={classNames({
										"calendar-date-indicator-number": true,
									})}
								>
									{date.getDate()}
								</div>
							</div>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default CalendarNavigator;
