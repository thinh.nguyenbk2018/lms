import { Checkbox, Col, Row } from "antd";
import PERMISSION_MAPPING from "../../../../constants/LimitPermissionMapping";
import LIMIT_VALUE from "../../../../constants/LimitValue";
import { useAppDispatch, useAppSelector } from "../../../../hooks/Redux/hooks";
import { PermissionOfUser } from "../../../../redux/features/Permission/PermissionAPI";
import { editPermissionLimit } from "../../../../redux/features/Permission/PermissonSlice";
import { PermissionOfRole } from "../../../../redux/features/Permission/RolesAPI";

export interface LimitOfPermissionProps {
    permission: PermissionOfUser | PermissionOfRole,
    onEditLimit: (key: string, value: boolean) => void
}

const LimitOfPermission = ({ permission, onEditLimit }: LimitOfPermissionProps) => {

    const onChangeLimit = (key: string, value: boolean) => onEditLimit(key, value)

    return (
        <div style={{ margin: 15 }}>
            {
                Object.keys(PERMISSION_MAPPING).map(key => (
                    permission[key as keyof Omit<PermissionOfUser, "id" | "title" | "description" | "validFrom" | "expiresAt">] !== LIMIT_VALUE.DONT_CARE &&
                    <Row justify="space-between">
                        <Col span={23}><span>{PERMISSION_MAPPING[key as keyof typeof PERMISSION_MAPPING]}</span></Col>
                        <Col span={1}>
                            <Checkbox
                                checked={permission[key as keyof Omit<PermissionOfUser, "id" | "title" | "description" | "validFrom" | "expiresAt">] === LIMIT_VALUE.LIMIT}
                                onChange={({ target: { checked } }) => onChangeLimit(key, checked)}
                            />
                        </Col>
                    </Row>
                ))
            }
        </div>
    )
}


export default LimitOfPermission;