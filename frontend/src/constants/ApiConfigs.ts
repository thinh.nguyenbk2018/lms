export const API_CONSTANT = {
    BASE_STATIC_URL: process.env.REACT_APP_BASE_URL,
    BASE_URL: process.env.REACT_APP_BASE_URL + "/api/v1",
    BASE_SOCKET_URL: "ws://" + process.env.REACT_APP_DOMAIN + "/api/ws",
    GCLOUD_API_KEY: process.env.REACT_APP_GCLOUD_API_KEY,
    GCLOUD_CLIENT_ID: process.env.REACT_APP_GCLOUD_CLIENT_ID,
}
