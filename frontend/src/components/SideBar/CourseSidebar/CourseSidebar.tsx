import {useAppSelector} from "../../../hooks/Redux/hooks";
import {useEffect, useState} from "react";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";
import classNames from "classnames/bind";
import {useNavigate} from "react-router-dom";
import "../SubSidebar.less";
import {SubSidebarTab, useActiveSubSideBar} from "../SubSidebar";
import {sideBarSelector} from "@redux/features/SideBar/SidebarSlice";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {authSelectors} from "@redux/features/Auth/AuthSlice";

const courseSidebar: SubSidebarTab[] = [
    {
        displayName: "Thông tin chung",
        navigationRoute: ROUTING_CONSTANTS.COURSE_DASHBOARD,
        require_permissions: [PermissionEnum.VIEW_DETAIL_COURSE],
        key: "dashboard"
    },
    {
        displayName: "Lớp học",
        navigationRoute: ROUTING_CONSTANTS.COURSE_CLASS_LIST,
        require_permissions: [PermissionEnum.VIEW_LIST_CLASS],
        key: "classes"
    },
    {
        displayName: "Nội dung giảng dạy",
        navigationRoute: ROUTING_CONSTANTS.COURSE_CONTENT,
        require_permissions: [PermissionEnum.VIEW_LEARNING_CONTENT],
        key: "content"
    },
    {
        displayName: "Bài học",
        navigationRoute: ROUTING_CONSTANTS.COURSE_UNIT_LIST_PAGE,
        require_permissions: [],
        key: "units"
    },
    {
        displayName: "Bài kiểm tra",
        navigationRoute: ROUTING_CONSTANTS.COURSE_QUIZ,
        require_permissions: [PermissionEnum.VIEW_DETAIL_COURSE],
        key: "quizzes"
    },
    {
        displayName: "Đề kiểm tra",
        navigationRoute: ROUTING_CONSTANTS.COURSE_EXAM,
        require_permissions: [PermissionEnum.VIEW_DETAIL_COURSE],
        key: "exam"
    },
    {
        displayName: "Giáo trình",
        navigationRoute: ROUTING_CONSTANTS.COURSE_TEXTBOOK_MANAGEMENT,
        require_permissions: [],
        key: "textbooks"
    }
];

const CourseSidebar = () => {
    const navigate = useNavigate();
    const courseIdInFocus = useAppSelector(sideBarSelector.selectCourseIdInFocus);
    const classIdInFocus = useAppSelector(sideBarSelector.selectClassIdInFocus);

    const permissionsMap = useAppSelector(authSelectors.selectUserPermissions);
    // * Selected tab: default to first tab in the list
    const [selectedTab, setSelectedTab] = useState(courseSidebar[0].key);

    const currentNavigationRoute = useActiveSubSideBar(courseSidebar);

    useEffect(() => {
        setSelectedTab((courseSidebar.find(tab => tab.navigationRoute === currentNavigationRoute) || courseSidebar[0]).key)
    }, [setSelectedTab, currentNavigationRoute]);

    if (!courseIdInFocus || classIdInFocus) {
        return <></>;
    }
    return (
        <div className={"sub-sidebar-tab-container"}>
            {courseSidebar.filter(tab => tab.displayName).filter(tab => tab.require_permissions?.length === 0 || (permissionsMap && tab.require_permissions?.some(permission => permissionsMap[permission]))).map((tab, index) => {
                let clsNames: string = classNames({
                    "sub-sidebar-tab": true,
                    "sub-sidebar-tab-selected": tab.key === selectedTab,
                });

                return (
                    <div key={tab.key}>
                        <a
                            className={clsNames}
                            onClick={() => {
                                setSelectedTab(tab.key);
                                const pathReplacedWithCourseId = tab.navigationRoute.replace(
                                    ":courseId",
                                    courseIdInFocus.toString()
                                );
                                navigate(pathReplacedWithCourseId);
                            }}
                            tabIndex={index}
                        >
                            {tab.displayName}
                        </a>
                    </div>
                );
            })}
        </div>
    );
};

export default CourseSidebar;
