import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "@redux_store";

const EMAIL_SLICE_KEY = "email";

const initialState = {
    showModal: false
}

export const emailSlice = createSlice({
    name: EMAIL_SLICE_KEY,
    initialState,
    reducers: {
        setShowModal: (state, action: PayloadAction<boolean>): void => {
            console.log('show: ', action.payload);
            state.showModal = action.payload;
        },
    },
});

export const emailSelector = {
    selectShowModal: (state: RootState) => state.email.showModal
}

export const emailActions = emailSlice.actions;