import { Button, Space } from "antd";
import React from "react";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import StudentForm, { StudentFormHandle } from "../../../components/Students/StudentForm";
import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";

const AddStudentPage = () => {
  const studentFormRef = useRef<StudentFormHandle>(null);
  const navigate = useNavigate();
  const onDone = () => {
    studentFormRef.current?.createStudent()
      .then((result: any) => {
        if (!result) return;
        if ("code" in result && result.code === "SUCCESS") {
          navigate(ROUTING_CONSTANTS.STUDENTS_MANAGEMENT)
        }
      })
      .catch((error: any) => {
        if (error?.data?.message) {
          AntdNotifier.error(error.data.message);
          return;
        }
        AntdNotifier.error("Có lỗi xảy ra trong quá trình lưu, thử lại sau");
      })
  }

  const onCancel = () => {
    navigate(ROUTING_CONSTANTS.STUDENTS_MANAGEMENT)
  }
  return <>
    <ContentHeader
      title="Thêm học viên"
      action={
        <Space>
          <Button
            danger
            type="primary"
            color="#73d13d"
            onClick={onCancel}
          >
            Hủy
          </Button>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onDone}
          >
            Xong
          </Button>
        </Space>
      }
    />
    <StudentForm ref={studentFormRef} />
  </>
}

export default AddStudentPage;