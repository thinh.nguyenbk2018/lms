import {ICourseInformation} from "@components/Courses/CourseInformation/CourseInformation";
import {ActivityPlacementInfo} from "@pages/Tenant/Courses/CourseDetail/Activity/DraggableDroppableActivity";
import {ChapterPlacementInfo} from "@pages/Tenant/Courses/CourseDetail/Chapter/DraggableChapter";
import {
    Activity,
    Chapter,
    ChapterDto,
    ChapterGroupByTextbook,
    ChapterGroupByTextbookDto,
    Course,
    CourseDto,
    QuizItem,
    UnitGroupByTextbook,
    UnitItem
} from "@typescript/interfaces/courses/CourseTypes";
import {BaseResponse, PaginatedResponse} from "../../interfaces/types";
import {SearchCriteria} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {transformSearchQuery} from "../APIUtils/TransformParameter";
import {apiSlice} from "../CentralAPI";
import {TextBook} from "@redux/features/Textbook/TextbookType";
import {ClassListDto} from "@redux/features/Class/ClassType";

export type CourseItem = Pick<ICourseInformation,
    "id" | "name" | "code" | "programs" | "createdAt">;

const courseApiInvalidatesTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["Courses", "CourseContent", "CourseInformation", "CourseTextbook", "CourseClasses", "CourseUnits", "CourseQuizzes"],
});

type CourseResponse = PaginatedResponse<CourseItem>;


export const DEFAULT_CHAPTER_NAME = "Chuơng mới";
type ChapterDtoToChapterTransformer = (chapterDto: ChapterDto) => Chapter;
type ChapterDtoToChapterGroupByTextbookTransformer = (dto: ChapterGroupByTextbookDto) => ChapterGroupByTextbook;
export const transformChapterDtoToChapter: ChapterDtoToChapterTransformer = (
    chapterDto
) => {
    const {quizzes = [], units = [], votes = [], ...chapterMeta} = chapterDto;
    const activityList: Activity[] = [
        ...quizzes.map((item) => ({...item, type: "quiz" as const})),
        ...units.map((item) => ({...item, type: "unit" as const})),
        ...votes.map((item) => ({...item, type: "vote" as const})),
    ].sort((a, b) => a.order - b.order);
    return {
        ...chapterMeta,
        activities: activityList,
    };
};

export const transformChapterDtoToChapterGroupByTextbook: ChapterDtoToChapterGroupByTextbookTransformer = (
    dto
) => {
    return {
        textbookId: dto.textbookId,
        textbookName: dto.textbookName,
        chapters: dto.chapters.map(x => transformChapterDtoToChapter(x))
            .slice(0)
            .sort((a, b) => a.order - b.order)
    }
};

const courseAPI = courseApiInvalidatesTags.injectEndpoints({
    endpoints: (build) => ({
        getCourseList: build.query<CourseResponse, string>({
            query: (keyword) => `courses?page=1&size=0&keyword=${keyword}`,
            providesTags: (result) =>
                result
                    ? // successful query
                    [
                        ...result.listData.map(
                            ({id}) => ({type: "Courses", id} as const)
                        ),
                        {type: "Courses", id: "LIST"},
                    ]
                    : // an error occurred, but we still want to refetch this query when `{ type: 'Posts', id: 'LIST' }` is invalidated
                    [{type: "Courses", id: "LIST"}],
        }),
        getCourseListPaginated: build.query<CourseResponse, SearchCriteria>({
            query: (args) => `courses/?${transformSearchQuery(args)}`,
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.listData.map(
                            ({id}) => ({type: "Courses", id} as const)
                        ),
                        {type: "Courses", id: "LIST"},
                    ]
                    : // an error occurred, but we still want to refetch this query when `{ type: 'Posts', id: 'LIST' }` is invalidated
                    [{type: "Courses", id: "LIST"}],
        }),

        getAllCourseList: build.query<CourseResponse, SearchCriteria>({
            query: (args) => `courses/search/?${transformSearchQuery(args)}`
        }),

        // * Course Information
        getCourseInformation: build.query<Course, number>({
            query: (id) => `courses/${id}`,
            transformResponse: (res: CourseDto, _meta, _args) => {
                console.log("RES BEFORE TRansform:", res);
                const {chapters = [], ...others} = res;
                const transformedChapters: Chapter[] = [];
                const transformed: Course = {
                    ...others,
                    chapters: transformedChapters,
                };
                return transformed;
            },
            providesTags: (result) => [{type: "Courses", id: result?.id}],
        }),

        getCourseClassListPaginated: build.query<ClassListDto, {courseId: number, criteria: SearchCriteria}>({
            query: ({courseId, criteria}) => `courses/${courseId}/classes/?${transformSearchQuery(criteria)}`,
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.listData.map(
                            ({ id }) => ({ type: "CourseClasses", id } as const)
                        ),
                        { type: "CourseClasses", id: "LIST" },
                    ]
                    : // an error occurred, but we still want to refetch this query when `{ type: 'Posts', id: 'LIST' }` is invalidated
                    [{ type: "CourseClasses", id: "LIST" }],
        }),

        addCourse: build.mutation<BaseResponse, Omit<Course, "id">>({
            query(body) {
                return {
                    url: `courses`,
                    method: "POST",
                    body,
                };
            },
            invalidatesTags: [{type: "Courses", id: "LIST"}],
        }),

        deleteCourse: build.mutation<number, number>({
            query(id) {
                return {
                    url: `courses/${id}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: (_result, _error, args) => [{type: "Courses", id: args}],
        }),

        updateCourse: build.mutation<Course, Course>({
            query(course) {
                return {
                    url: `courses/${course.id}`,
                    method: "PUT",
                    body: course,
                };
            },
            // async onQueryStarted(patch, { dispatch, queryFulfilled }) {
            // 	// try {
            // 	const { data: updatedPost } = await queryFulfilled;
            // 	const patchResult = dispatch(
            // 		courseAPI.util.updateQueryData(
            // 			"getCourseInformation",
            // 			patch.id,
            // 			(draft) => {
            // 				Object.assign(draft, patch);
            // 			}
            // 		)
            // 	);
            // 	// }
            // 	// catch {}
            // },
        }),
        // * Course Content
        getCourseContent: build.query<Chapter[], number>({
            query: (courseId) => {
                return {
                    url: `courses/${courseId}/learning_content`,
                    method: "GET",
                };
            },
            transformResponse: (res: ChapterDto[], _meta, _args) => {
                return res
                    .map(transformChapterDtoToChapter)
                    .slice(0)
                    .sort((a, b) => a.order - b.order);
            },
            providesTags: (_res, _err, args) => [{type: "CourseContent", id: args}],
        }),

        getCourseContentGroupByTextbooks: build.query<ChapterGroupByTextbook[], number>({
            query: (courseId) => {
                return {
                    url: `courses/${courseId}/learning_content/textbooks`,
                    method: "GET",
                };
            },
            transformResponse: (res: ChapterGroupByTextbookDto[], _meta, _args) => {
                return res.map(transformChapterDtoToChapterGroupByTextbook);
            },
            providesTags: (_res, _err, args) => [{type: "CourseContent", id: args}],
        }),

        // ***** Add chapter and activity to Course Content
        // Chapter: Only need the title
        createChapter: build.mutation<Chapter,
            { courseId: number; chapterInfo: Partial<Chapter> }>({
            query: ({courseId, chapterInfo}) => {
                return {
                    url: `courses/${courseId}/learning_content/chapters`,
                    method: "POST",
                    body: chapterInfo,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),

        updateChapter: build.mutation<Chapter,
            { courseId: number; chapterId: number; chapterInfo: Partial<Chapter> }>({
            query: ({courseId, chapterId, chapterInfo}) => {
                return {
                    url: `courses/${courseId}/learning_content/chapters/${chapterId}`,
                    method: "PUT",
                    body: chapterInfo,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),

        deleteChapter: build.mutation<number,
            { courseId: number; chapterId: number }>({
            query: ({courseId, chapterId}) => {
                return {
                    url: `courses/${courseId}/learning_content/chapters/${chapterId}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),

        // ** Item placement
        chapterPlacement: build.mutation<void,
            { courseId: number; from: ChapterPlacementInfo; to: ChapterPlacementInfo }>({
            query: ({courseId, from, to}) => {
                return {
                    url: `courses/${courseId}/learning_content/reorder/chapters`,
                    method: "PUT",
                    body: {
                        from,
                        to,
                    },
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),

        // ? Unit
        getUnitDetail: build.query<UnitItem,
            { courseId: number; unitId: number }>({
            query: ({courseId, unitId}) => {
                return {
                    url: `courses/${courseId}/learning_content/units/${unitId}`,
                    method: "GET",
                };
            },
            transformResponse: (res: any, _meta, _args) => {
                return {
                    ...res,
                    type: "unit" as const,
                };
            },
            providesTags: [{type: "CourseContent", id: "UNIT_DETAIL"}],
        }),
        createUnit: build.mutation<UnitItem,
            { courseId: number; chapterId: number; unitItem: Partial<UnitItem> }>({
            query: ({courseId, chapterId, unitItem}) => {
                return {
                    url: `courses/${courseId}/learning_content/chapters/${chapterId}/units`,
                    method: "POST",
                    body: unitItem,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),
        updateUnit: build.mutation<UnitItem,
            { courseId: number; unitItem: Partial<UnitItem> }>({
            query: ({courseId, unitItem}) => {
                return {
                    url: `courses/${courseId}/learning_content/units/${unitItem.id}`,
                    method: "PUT",
                    body: unitItem,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
                {type: "CourseContent", id: "UNIT_DETAIL"},
                {type: "CourseUnits", id: "LIST"}
            ],
        }),
        deleteUnit: build.mutation<void,
            { courseId: number; unitId: number }>({
            query: ({courseId, unitId}) => {
                return {
                    url: `courses/${courseId}/learning_content/units/${unitId}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
                {type: "CourseContent", id: "UNIT_DETAIL"},
                { type: "CourseUnits", id: "LIST" }
            ],
        }),
        // ? Quiz
        getQuizDetail: build.query<QuizItem,
            { courseId: number; quizId: number }>({
            query: ({courseId, quizId}) => {
                return {
                    url: `courses/${courseId}/learning_content/quizzes/${quizId}`,
                    method: "GET",
                };
            },
            transformResponse: (res: any, _meta, _args) => {
                return {
                    ...res,
                    type: "quiz" as const,
                };
            },
            providesTags: [{type: "CourseContent", id: "QUIZ_DETAIL"}],
        }),
        createQuiz: build.mutation<QuizItem,
            { courseId: number; chapterId: number; quizItem: Partial<QuizItem> }>({
            query: ({courseId, chapterId, quizItem}) => {
                return {
                    url: `courses/${courseId}/learning_content/chapters/${chapterId}/quizzes`,
                    method: "POST",
                    body: quizItem,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),
        updateQuiz: build.mutation<QuizItem,
            { courseId: number; quizItem: Partial<QuizItem> }>({
            query: ({courseId, quizItem}) => {
                return {
                    url: `courses/${courseId}/learning_content/quizzes/${quizItem.id}`,
                    method: "PUT",
                    body: quizItem,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
                {type: "CourseContent", id: "QUIZ_DETAIL"},
            ],
        }),
        deleteQuiz: build.mutation<void,
            { courseId: number; quizId: number }>({
            query: ({courseId, quizId}) => {
                return {
                    url: `courses/${courseId}/learning_content/quizzes/${quizId}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
                {type: "CourseContent", id: "QUIZ_DETAIL"},
            ],
        }),

        // * Handle item placement
        changeItemPlacement: build.mutation<Chapter[],
            {
                courseId: number;
                from: ActivityPlacementInfo;
                to: ActivityPlacementInfo;
            }>({
            query: ({courseId, from, to}) => {
                return {
                    url: `courses/${courseId}/learning_content/reorder/actions`,
                    method: "PUT",
                    body: {
                        from,
                        to,
                    },
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "CourseContent", id: args.courseId},
            ],
        }),
        getTextbooksInCourse: build.query<TextBook[], { courseId: number, keyword: string }>({
            query: ({courseId, keyword}) => `courses/${courseId}/resources/textbooks?keyword=${keyword}`,
            providesTags: [{type: "CourseTextbook", id: "LIST"}],
        }),
        getTextbooksNotInCourse: build.query<TextBook[], { keyword: string, courseId: number }>({
            query: ({keyword, courseId}) => `courses/${courseId}/resources/textbooks/not?keyword=${keyword}`
        }),
        addTextbooksToCourse: build.mutation<void, { courseId: number, idList: number[] }>({
            query: ({courseId, idList}) => ({
                url: `courses/${courseId}/resources/textbooks`,
                method: "POST",
                body: {idList},
            }),
            invalidatesTags: [{type: "CourseTextbook", id: "LIST"}],
        }),
        deleteTextbooksFromCourse: build.mutation<void, { courseId: number, textbookId: number }>({
            query: ({courseId, textbookId}) => ({
                url: `courses/${courseId}/resources/textbooks/${textbookId}`,
                method: "DELETE",
            }),
            invalidatesTags: [{type: "CourseTextbook", id: "LIST"}],
        }),
        countTextbooksInCourse: build.query<number, { courseId: number }>({
            query: ({courseId}) => `courses/${courseId}/resources/textbooks/count`
        }),
        getQuizCourse: build.query<QuizItem[], { courseId: number }>({
            query: ({ courseId }) => `courses/${courseId}/learning_content/quizzes`,
            providesTags: [{ type: "CourseQuizzes", id: "LIST" }],
        }),
        getUnitCourse: build.query<UnitItem[], { courseId: number }>({
            query: ({ courseId }) => `courses/${courseId}/learning_content/units`,
            providesTags: [{ type: "CourseUnits", id: "LIST" }],
        }),
        getUnitCourseGroupByTextbooks: build.query<UnitGroupByTextbook[], { courseId: number }>({
            query: ({ courseId }) => `courses/${courseId}/learning_content/units/textbooks`,
            providesTags: [{ type: "CourseUnits", id: "LIST" }],
        }),
    }),
    overrideExisting: false,
});

export const {
    // * Coures List
    useGetCourseListQuery,
    useLazyGetCourseListPaginatedQuery,

    // * Course Information
    useGetCourseInformationQuery,
    useAddCourseMutation,
    useUpdateCourseMutation,
    useDeleteCourseMutation,

    // * Course Content
    useGetCourseListPaginatedQuery,
    useGetCourseContentQuery,
    useCreateChapterMutation,
    useUpdateChapterMutation,
    useDeleteChapterMutation,
    useChapterPlacementMutation,
    // ? Chapter content
    // * Unit
    useCreateUnitMutation,
    useUpdateUnitMutation,
    useDeleteUnitMutation,
    useGetUnitDetailQuery,

    // * Quiz
    useCreateQuizMutation,
    useUpdateQuizMutation,
    useDeleteQuizMutation,
    useGetQuizDetailQuery,

    useChangeItemPlacementMutation,
    useGetTextbooksInCourseQuery,
    useAddTextbooksToCourseMutation,
    useLazyGetTextbooksNotInCourseQuery,
    useDeleteTextbooksFromCourseMutation,
    useCountTextbooksInCourseQuery,
    useGetCourseClassListPaginatedQuery,
    useGetUnitCourseQuery,
    useLazyGetUnitCourseGroupByTextbooksQuery,
    useLazyGetCourseContentGroupByTextbooksQuery,
    useGetQuizCourseQuery,
    useGetAllCourseListQuery
} = courseAPI;

// * Provide tags for cache invalidation when code splitting
// * https://github.com/reduxjs/redux-toolkit/issues/1510