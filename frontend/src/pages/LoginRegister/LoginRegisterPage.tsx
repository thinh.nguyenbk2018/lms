import React, { useEffect, useState } from 'react';
import { Card, Row, Space, Image, Spin } from 'antd';
import LoginForm from "../../components/LoginForm/LoginForm";
import RegisterForm from "../../components/RegisterForm/RegisterForm";
import { useAppSelector } from "../../hooks/Redux/hooks";
import { authSelectors } from "../../redux/features/Auth/AuthSlice";
import { useNavigate } from 'react-router-dom';
import { ROUTING_CONSTANTS } from "../../navigation/ROUTING_CONSTANTS";
import { useGetTenantCustomInfoQuery } from '@redux/features/Tenant/TenantAPI';
import { tenantUtils } from '@utils/TenantHelper/TenantUtils';

import logo from "@assets/logo/logo.png";
import banner from "@assets/images/anhtrang.jpg";
import LOCAL_STORAGE_KEYS from "@constants/LocalStorageKey";
import LoadingPage from "@pages/UtilPages/LoadingPage";

const Tabs = [
    {
        key: "login",
        tab: "Đăng nhập"
    },
    // {
    //     key: "register",
    //     tab: "Đăng ký"
    // }
]

interface TabContent extends Record<string, any> {
    login: React.ReactElement,
    register: React.ReactElement
}

const defaultData = { logo, name: "BKLMS", banner, tenantId: "BKLMS" };


const LoginRegisterPage = () => {
    const [activeTab, setActiveTab] = useState('login');
    const navigate = useNavigate();
    const userLoggedIn = useAppSelector(authSelectors.selectLoginSuccess);
    const onTabChange = (key: string) => {
        setActiveTab(key);
    };
    if (userLoggedIn) {
        navigate(ROUTING_CONSTANTS.DASHBOARD);
        return null;
    }


    const tenantId = tenantUtils.getTenantIdFromHostname();
    let { data, isFetching, isSuccess, isLoading } = useGetTenantCustomInfoQuery(tenantId!);

    useEffect(() => {
        if (data) {
            localStorage.setItem(LOCAL_STORAGE_KEYS.LOGO, data.logo);
            localStorage.setItem(LOCAL_STORAGE_KEYS.BANNER, data.banner);
            localStorage.setItem(LOCAL_STORAGE_KEYS.TENANT_NAME, data.name);
        }
    }, [data]);

    return (
        <>
            {isSuccess && <Row justify={"center"} align={"middle"} style={{ height: "100vh" }}>
                <Space align="center" direction="horizontal" style={{ border: "1px solid #f0f0f0" }}>
                    <Card
                        headStyle={{ display: 'flex', justifyContent: `center` }}
                        tabList={Tabs}
                        activeTabKey={activeTab}
                    // onTabChange={onTabChange}
                    >
                        {/* {TabContents[activeTab]} */}
                        <LoginForm customData={data || defaultData} />,
                    </Card>
                    <div style={{ height: 560, maxWidth: 390 }}>
                        <Spin spinning={isLoading}>
                            <img src={data?.banner} alt="Ảnh banner"
                                style={{ objectFit: "cover", width: "100%", height: "100%" }} />
                        </Spin>
                    </div>
                </Space>
            </Row>}
            {isFetching && <LoadingPage />}
        </>
    );

};

export default LoginRegisterPage;
