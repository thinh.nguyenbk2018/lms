import {blue, green, red, volcano, yellow} from "@ant-design/colors";
import {Card, Col, Row, Space, Tooltip, Typography} from "antd";
import {TextBook} from "@redux/features/Textbook/TextbookType";
import './TextbookCard.less';
import {useNavigate, useParams} from "react-router-dom";
import COLOR_SCHEME from "@constants/ThemeColor";
import {testSvg} from "@components/Icons/SvgIcons";
import {DeleteOutlined} from "@ant-design/icons";
import React from "react";
import {faBook, faPenToSquare} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";

const boldness = 6;
const colors = [blue[boldness], green[boldness], red[boldness], yellow[boldness], volcano[boldness]];

const hashString = (s: string) => {
    return s.split("").reduce((a, b) => {
        a = (a << 5) - a + b.charCodeAt(0);
        return a & a;
    }, 0);
};

const getRandomColorForString = (text: string) => {
    console.log(colors[hashString(text) % (colors.length)], hashString(text));
    const hashed = hashString(text);
    const number = hashed > 0 ? hashed : -hashed;
    return colors[number % (colors.length)];
};

type BookCardProps = Partial<TextBook> & { onEdit?: () => void; onDelete?: () => void };

const TextbookCard = (props: BookCardProps) => {
    const {id, name, author, description, attachment, onEdit, onDelete} = props;
    const {classId, courseId} = useParams();
    const navigate = useNavigate();

    const navigateToTextbookDetail = () => {
        if (classId != undefined) {
            navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_TEXTBOOK_DETAIL, [":classId", classId], [":textbookId", id || ""]))
        } else if (courseId != undefined) {
            navigate(replacePathParams(ROUTING_CONSTANTS.COURSE_TEXTBOOK_DETAIL, [":courseId", courseId], [":textbookId", id || ""]))
        } else {
            navigate(replacePathParams(ROUTING_CONSTANTS.CENTRAL_TEXTBOOK_DETAIL, [":textbookId", id || ""]))
        }
    }

    return (
        // <div className="book-card-container">
        //     <div className="card-background" style={{backgroundColor: getRandomColorForString(name || "Random")}}>
        //         <p className="name">{name}</p>
        //     </div>
        //     <div className="card-meta">
        //         <p className="author">by {author}</p>
        //         <p className="description">{description}</p>
        //         <a href={attachment || "#"}>
        //             {attachment &&
        //                 <FileIconPreviewByFileName filename={getFileNameFromFirebaseUrl(attachment)} showName={true}
        //                                            layout={'column'}/>}
        //         </a>
        //     </div>
        //     <div className="card-action">
        //         <div
        //             className="card-action-item card-edit-action"
        //             onClick={() => onEdit?.()}
        //             style={{display: classId != undefined || courseId != undefined ? 'none' : ''}}
        //         >
        //             <Space>
        //                 <FontAwesomeIcon icon={faPenToSquare}/>
        //                 Chỉnh sửa
        //             </Space>
        //         </div>
        //         <div
        //             className="card-action-item card-delete-action"
        //             onClick={() => onDelete?.()}
        //             style={{display: classId != undefined ? 'none' : ''}}
        //         >
        //             <Space>
        //                 <FontAwesomeIcon icon={faCircleXmark}/>
        //                 Xóa
        //             </Space>
        //         </div>
        //     </div>
        // </div>
        <Card
            className="class-quiz-card-item"
            title={
                <Space align="center">
                    <FontAwesomeIcon
                        icon={faBook}
                        style={{ margin: "0 .5em 0 0" , color: blue.primary}}
                    />
                    <p style={{margin: 0, padding: 0}}>
                        {name || "Không có thông tin"}
                    </p>
                </Space>
            }
            extra={
                <Space>
                    <div className="action-container">
                        <Tooltip title={"Chi tiết"}>
                            <Typography.Link
                                className={'flex'}
                                style={{marginRight: '0.25rem', color: COLOR_SCHEME.primary}}
                                onClick={() => {
                                    navigateToTextbookDetail();
                                }}
                            >
                                {testSvg}
                            </Typography.Link>
                        </Tooltip>
                        {courseId == undefined && classId == undefined && <Typography.Link
                            className={'flex'}
                            style={{marginRight: '0.25rem', color: COLOR_SCHEME.primary}}
                            onClick={() => {
                                onEdit?.();
                            }}
                        >
                            <FontAwesomeIcon icon={faPenToSquare}/>
                        </Typography.Link>}
                        {/*<PermissionRenderer permissions={[PermissionEnum.DELETE_QUIZ_CLASS]}>*/}
                        {classId == undefined && <DeleteOutlined
                            className="delete"
                            onClick={() => {
                                onDelete && onDelete();
                            }}
                        />}
                        {/*</PermissionRenderer>*/}
                    </div>
                </Space>
            }
        >
            <Row>
                <Col className={'course-unit-page-label course-unit-page-label-left'} span={2}>Tác giả</Col>
                <Col span={19}>{author}</Col>
            </Row>
        </Card>
    );
};

export default TextbookCard;
