import { Button, Space } from "antd";

interface ActionButtionProps {
    title: String;
    state: boolean;
    onAction: () => void;
    onApprove: () => void;
    onCancel: () => void;
}

const ActionButton = ({
    title,
    state,
    onAction,
    onApprove,
    onCancel,
}: ActionButtionProps) => {
    return (
        <>
            {state ? (
                <Space>
                    <Button type="primary" danger onClick={onCancel}>
                        Hủy
                    </Button>
                    <Button type="primary" color="#73d13d" onClick={onApprove}>
                        Xong
                    </Button>
                </Space>
            ) : (
                <Button type="primary" onClick={onAction}>
                    {title}
                </Button>
            )}
        </>
    );
};

export default ActionButton;
