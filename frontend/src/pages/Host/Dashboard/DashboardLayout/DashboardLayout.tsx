import { LogoutOutlined } from "@ant-design/icons";
import { Button, Image, Layout, Menu, Space, Typography } from "antd";
import { Content, Header } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import { Link, Navigate, Outlet, useNavigate } from "react-router-dom";
import logo from "@assets/logo/logo.png";
import CustomBreadCrumbs from "../../../../components/BreadCrumbs/CustomBreadCrumbs";
import LOCAL_STORAGE_KEYS from "../../../../constants/LocalStorageKey";
import useRoutesTabs from "../../../../hooks/Routing/useRoutesTabs";
import { DashboardTenantAppNavItems, isTabNav } from "../../../../navigation/NavigationItem";
import { HOST_ROUTING_CONSTANT } from "../../../../navigation/ROUTING_CONSTANTS";
import './DashboardLayout.less';



const DashboardLayout = () => {
  const routes = useRoutesTabs(DashboardTenantAppNavItems);
  const navigate = useNavigate();

  const logout = ()=>{
    localStorage.removeItem(LOCAL_STORAGE_KEYS.TOKEN);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.USER_AUTH);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.TENANT);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.REFRESH_TOKEN);
    navigate(HOST_ROUTING_CONSTANT.ADMIN)
  }

  return localStorage.getItem(LOCAL_STORAGE_KEYS.TOKEN) ?
    <Layout className="dashboard-layout__container" style={{ display: "flex" }}>
      <Header className="header">
        <div className="logo" >
          <Link to={HOST_ROUTING_CONSTANT.DASHBOARD}>
            <Space align="end">
              <Image src={logo} preview={false} height={50} />
              <Typography.Title level={3} style={{ marginLeft: 5 }}>BKLMS</Typography.Title>
            </Space>
          </Link>
        </div>
        <Space>
          <Typography.Text>{localStorage.getItem(LOCAL_STORAGE_KEYS.TENANT)}</Typography.Text>
          <Menu theme="light" mode="horizontal" defaultSelectedKeys={['2']}>
            <Menu.Item>
              <Button onClick={logout} shape="circle" icon={<LogoutOutlined />} />
            </Menu.Item>
          </Menu>
        </Space>
      </Header>
      <Layout style={{ display: "flex", flexDirection: "row", flex: '1 1' }}>
        <Sider width={220} className="site-layout-background">
          <Menu
            mode="inline"
            defaultSelectedKeys={["DASHBOARD"]}
            defaultOpenKeys={['sub1']}
            style={{
              height: '100%',
              borderRight: 0,
            }}
          >
            {routes}
          </Menu>
        </Sider>
        <Layout
          style={{
            padding: '0 24px 24px',
          }}
        >
          <Space style={{ margin: 16 }}>
            <CustomBreadCrumbs />
          </Space>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: "85vh",
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </Layout> : <Navigate to={HOST_ROUTING_CONSTANT.LOGIN} />
}

export default DashboardLayout;