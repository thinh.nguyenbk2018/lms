import { useEffect } from "react";
import { useDrag } from "react-dnd";
import { useAppDispatch } from "../../../hooks/Redux/hooks";
import { ElementFormula, setDraggingStatus } from "../../../redux/features/Grade/GradeFormulaSlice";
import BracketComponent from "./BracketComponent/BracketComponent";
import NumberComponent from "./NumberComponent/NumberComponent";
import OperatorComponent from "./OperatorComponent/OperatorComponent";
import TagComponent from "./TagComponent/TagComponent";

interface ElementFormulaComponentProps {
  element: ElementFormula,
  index: number
}

const ElementFormulaComponent = ({ element, index }: ElementFormulaComponentProps) => {
  const dispatch = useAppDispatch()
  let [{ isDragging }, drag] = useDrag(() => ({
    type: "ElementFormula",
    item: { element, sortIndex: index },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }));
  useEffect(() => {
    dispatch(setDraggingStatus(isDragging));
  }, [isDragging, dispatch])
  const renderElement = (element: ElementFormula) => {
    switch (element.type) {
      case "Tag":
        return <TagComponent value={element.value} index={index} />
      case "Number":
        return <NumberComponent value={element.value} index={index} />
      case "Bracket":
        return <BracketComponent value={element.value} />
      case "Operator":
        return <OperatorComponent value={element.value} />
    }
  }

  return <span ref={drag} style={{ margin: "2px 1px" }} onMouseDown={(e) => e.stopPropagation()}>
    {renderElement(element)}
  </span>
}


export default ElementFormulaComponent;