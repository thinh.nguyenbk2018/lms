import { CheckOutlined } from "@ant-design/icons";
import { Button, Space } from "antd";
import React from "react";
import { ButtonProps } from "antd";

const DoneButton = (props: ButtonProps) => {
	return (
		<Button type={"primary"} {...props}>
			<Space>
				<CheckOutlined />
				{props.title || "Xong"}
			</Space>
		</Button>
	);
};

export default DoneButton;
