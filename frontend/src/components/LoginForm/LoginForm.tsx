import React from "react";
import {Button, Form, Image, Input, Space, Typography,} from "antd";
import {loginUser,} from "@redux/features/Auth/AuthSlice";
import {useAppDispatch} from "@hooks/Redux/hooks";
import './LoginForm.less';
import {TenantCustomInfo} from "@redux/features/Tenant/TenantAPI";


const LoginForm = ({customData}:{customData: TenantCustomInfo}) => {
    const dispatch = useAppDispatch();

    const onFinish = (values: any) => {
        // console.log("Success:", values);
        // TODO: Check API Response and  Update login state accordingly (if success, navigate to dashboard)
        //  --> Persist the login token if needed (with Localstorage API)
        dispatch(loginUser(values));
    };

    return (
        <Space direction={"vertical"} align={"center"}>
            <Image width={200} height={200} style={{objectFit:"cover"}} src={customData.logo} preview={false}/>
            <Typography.Title>{customData.name}</Typography.Title>
            <Form
                name="basic"
                labelCol={{
                    span: 9,
                }}
                wrapperCol={{
                    span: 15,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Form.Item
                    label="Tên đăng nhập"
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: "Hãy nhập tài khoản của bạn",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Nhập mật khẩu vào",
                        },
                    ]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 9,
                        span: 15,
                    }}
                    className={'login-form-login-btn'}
                >
                    <Button type="primary" htmlType="submit">
                        Đăng nhập
                    </Button>
                </Form.Item>
            </Form>
        </Space>
    );
};

export default LoginForm;
