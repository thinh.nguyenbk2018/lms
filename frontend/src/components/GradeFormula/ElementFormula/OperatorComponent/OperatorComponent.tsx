import { Operator } from "../../../../redux/features/Grade/GradeFormulaSlice";
import "./OperatorComponent.less"

const OperatorComponent = ({ value }: { value: Operator }) => {
  return <span className="operator-component">
    {value}
  </span>
}

export default OperatorComponent;