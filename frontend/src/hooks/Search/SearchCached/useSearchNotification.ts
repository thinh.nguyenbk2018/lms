// import _ from "lodash";
// import React from "react";
// import { useCallback, useState } from "react";
// import { useGetAllNotificatoinQuery, useGetTestResultQuery } from "../../../redux/features/Notification/NotificationAPI";
// import { useGetUserSearchWithKeyWordQuery } from '../../../redux/features/UserSearch/UserSearchAPI';
// // import { useGetUserSearchWithKeyWordQuery } from '../../../redux/features/UserSearch/UserSearchAPI';
// import { skipToken } from '@reduxjs/toolkit/query/react'
// import BeautifulLogger from './../../../utils/BeautifulLogger/BeautifulLogger';

// // ? Template to write a cached search with RTK Query Debounced
// // ! In this case: We have no choice but to call the query hooks ourself --> Each search type = 1 new hook defined

// /**
//  * 
//  * @param interval - debounce timer 
//  * @param minLengthToTriggerSearch - request only fire if length bigger than this value 
//  * @returns [inputVal,setInputVal, results] 
//  * @returns results - List of data found returned from the  server
//  * @example : <input type="text" value={inputVal} onChange={e => setInputVal(e.target.value)}> 
//  */
// const useSearchNotification = (interval: number = 650, minLengthToTriggerSearch: number = 1,debug: boolean=false) => {
//     const [searchText, setSearchText] = useState('');
//     const [laggedSearchText, setLaggedSearchText] = useState('');
//     // TODO: Implements Condition to skip the query 
//     // *    - Shouhld return true if we want to skip (Not reach timeout yet)
//     const skipQueryCondition : any = () => {
//         if(searchText?.length < minLengthToTriggerSearch) return true;
//         // if()
//         return null;  
//     }
//     // * Call your search query here with laggedSearchText 
//     const val = !skipQueryCondition() || skipToken; 
//     const result = useGetTestResultQuery(val); 
    
//     BeautifulLogger.logFocus(`isSkipping: ${skipQueryCondition()}`)
//     const debounceFn = useCallback(_.debounce((inputValue: string) => {
//         if(debug){
//             console.log("Search with Lagged: RTK Query Cache: ",laggedSearchText); 
//         }
//         if (inputValue.length > minLengthToTriggerSearch) {
//             setLaggedSearchText(inputValue);
//         }
//     }, interval), []);
    
//     const setSearchTextHandler: React.ChangeEventHandler<HTMLInputElement> = (e) => {
//         const {target: {value}} = e; 
//         setSearchText(value);
//         debounceFn(value); 
//     }

//     return [
//         searchText,
//         setSearchTextHandler,
//         result,
//     ] as const
// }

// export default useSearchNotification; 
export {} ;