import React from "react";
import ApiErrorWithRetryButton from "../../components/Util/ApiErrorWithRetryButton";

interface Props {
	message?: string;
	onRetry?: () => void;
	retryText?: string;
}
const ApiErrorRetryPage = (props: Props) => {
	const { onRetry } = props;
	return (
		<div style={{ position: "absolute", top: "50%", left: "50%" }}>
			<ApiErrorWithRetryButton
				{...props}
				onRetry={
					onRetry
						? onRetry
						: () => {
								window.location.reload();
						  }
				}
			/>
		</div>
	);
};

export default ApiErrorRetryPage;
