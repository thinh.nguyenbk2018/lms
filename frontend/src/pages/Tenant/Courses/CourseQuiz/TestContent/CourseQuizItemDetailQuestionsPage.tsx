import "./CourseQuizItemDetailQuestionsPage.less";

import {GroupOutlined,} from "@ant-design/icons";
import {Avatar, Button, Col, Collapse, Pagination, Row, Space, Typography,} from "antd";
import {formatRelative} from "date-fns";
import {vi} from "date-fns/locale";
import React, {useEffect} from "react";
import {useNavigate, useParams} from "react-router-dom";

import {parseDate} from "@components/Calendar/Utils/CalendarUtils";
import ContentHeader from "../../../../../components/ContentHeader/ContentHeader";
import BlankWithChoicesQuestionDrawer
    from "../../../../../components/Question/BlankWithChoicesQuestion/BlankWithChoicesQuestionDrawer";
import DragAndDropQuestionDrawer
    from "../../../../../components/Question/DragAndDropQuestion/DragAndDropQuestionDrawer";
import FillInBlankConfigurationDrawer
    from "../../../../../components/Question/FillBlankQuestion/FillInBlankConfigurationDrawer";
import MultiChoiceQuestionDrawer
    from "../../../../../components/Question/MultichoiceQuestion/MultiChoiceQuestionDrawer";
import QuestionContainerItem from "../../../../../components/Question/QuestionContainerItem/QuestionContainerItem";
import WrittenQuestionDrawer from "../../../../../components/Question/WrittenQuestion/WrittenQuestionDrawer";
import COLOR_SCHEME from "../../../../../constants/ThemeColor";
import {useAppDispatch, useAppSelector,} from "@hooks/Redux/hooks";
import useCourseSidebar from "../../../../../hooks/Routing/useCourseSidebar";
import API_ERRORS from "../../../../../redux/features/ApiErrors";
import {errorHandlingHelper,} from "@redux/features/CentralAPI";
import {
    createTestContentActions,
    createTestContentSelectors,
    DEFAULT_TEST_CONTENT_NAME,
    QuestionType,
} from "@redux/features/Quiz/CreateQuizSlice";
import {
    isPaginatedResponseQuestionList,
    useGetTestQuery,
    useRemoveQuestionsFromTestMutation,
} from "@redux/features/Quiz/examAPI";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorRetryPage from "../../../../UtilPages/ApiErrorRetryPage";
import QuestionGroupDrawer from "@components/Question/QuestionGroup/QuestionGroupDrawer";
import MenuAddNewQuestion from "./MenuAddNewQuestion";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import LoadingPage from "@pages/UtilPages/LoadingPage";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import BackButton from "@components/ActionButton/BackButton";

const CourseQuizItemDetailQuestionsPage = () => {
    // * Get the test id from the path
    const {testId} = useParams();
    // * Expected props / store data:
    const {courseId} = useCourseSidebar();

    // * Redux states and hooks
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    // * Select the id of the question being edited
    const searchCriteria = useAppSelector(
        createTestContentSelectors.selectSearchCriteria
    );
    const [removeQuestion] = useRemoveQuestionsFromTestMutation();

    const shouldSkip = () => {
        return isNaN(Number(testId)) || isNaN(courseId);
    };

    const {
        data: testData,
        isSuccess,
        isFetching,
        isError,
        error,
    } = useGetTestQuery(
        {
            courseId: Number(courseId),
            testId: Number(testId),
            ...searchCriteria,
        },
        {
            skip: shouldSkip(),
        }
    );

    useEffect(() => {
        if (testData) {
            dispatch(
                createTestContentActions.setCurrentTestContentName(
                    testData.title ?? DEFAULT_TEST_CONTENT_NAME
                )
            );
        }
        return function cleanUp() {
            dispatch(
                createTestContentActions.setCurrentTestContentName(
                    DEFAULT_TEST_CONTENT_NAME
                )
            );
        };
    }, [isSuccess]);

    function handleMenuClick(e: any) {
        let choice: QuestionType = e.key;
        dispatch(createTestContentActions.setOpeningDrawerType(choice));
    }

    const handleCreateQuestionGroup = () => {
        if (shouldSkip()) {
            AntdNotifier.error(
                "Không tìm thấy khóa và đề để tạo nhóm câu hỏi. Hãy thử load lại trang"
            );
            return;
        }
        dispatch(createTestContentActions.setOpeningDrawerType("GROUP"));
        dispatch(createTestContentActions.addingQuestionGroup());
    };

    if (isError && error) {
        errorHandlingHelper(
            error,
            API_ERRORS.EXAM.EXAM_NOT_FOUND,
            "Không tìm thấy bài kiểm tra"
        );
        return <ApiErrorRetryPage/>;
    }

    if (isSuccess) {
        return (<div className={"create-new-test-page"}>
            <ContentHeader title={testData?.title || ""}
                action={
                    <Space>
                        <BackButton
                            type="primary"
                            danger
                            onClick={() => {
                                navigate(
                                    replacePathParams(ROUTING_CONSTANTS.COURSE_EXAM, [":courseId", courseId || ""])
                                );
                            }}
                            className={"back-btn"}
                        />
                    </Space>
                }
            />
            <MultiChoiceQuestionDrawer/>
            <FillInBlankConfigurationDrawer/>
            <BlankWithChoicesQuestionDrawer/>
            <DragAndDropQuestionDrawer/>
            <WrittenQuestionDrawer/>
            <QuestionGroupDrawer/>

            <Row gutter={[0, 24]} style={{marginTop: 12}}>
                <Col span={24}>
                    <Row
                        justify={
                            "end"
                        }
                    >
                        <Space>
                            <Button
                                onClick={() => {
                                    handleCreateQuestionGroup();
                                }}
                            >
                                <GroupOutlined style={{color: COLOR_SCHEME.primary}}/>
                                Thêm nhóm câu hỏi
                            </Button>
                            <MenuAddNewQuestion handleMenuClick={handleMenuClick}/>
                        </Space>
                    </Row>
                </Col>
            </Row>
            <Row justify="center"
                 style={{marginTop: 12}}
                 className={'course-quiz-item-detail-wrapper'}
            >
                <Col sm={24} md={24} lg={20}>
                    <Collapse collapsible="header" defaultActiveKey={["1"]}>
                        <Collapse.Panel
                            header={
                                <Typography.Text style={{color: "white"}}>
                                    {`${testData?.title}`}
                                </Typography.Text>
                            }
                            key="1"
                            style={{backgroundColor: COLOR_SCHEME.primary}}
                        >
                            {/*<Row style={{margin: "1em"}}>*/}
                            {/*    <Col span={24}>*/}
                            {/*        <Typography.Text strong>{}</Typography.Text>*/}
                            {/*    </Col>*/}
                            {/*</Row>*/}
                            <Row className={'course-quiz-item-information-row'}>
                                <Col
                                    sm={{span: 11}}
                                    md={{span: 11}}
                                    xxl={{span: 11}}
                                    className={'course-quiz-item-information-title'}
                                >
                                    Mô tả
                                </Col>
                                <Col
                                    sm={{span: 12}}
                                    md={{span: 12}}
                                    xxl={{span: 12}}
                                >
                                    {testData?.description || "Không có mô tả"}
                                </Col>
                            </Row>
                            <Row className={'course-quiz-item-information-row'}>
                                <Col
                                    sm={{span: 11}}
                                    md={{span: 11}}
                                    xxl={{span: 11}}
                                    className={'course-quiz-detail-created course-quiz-item-information-title'}
                                >
                                    Tạo vào lúc
                                </Col>
                                <Col
                                    sm={{span: 12}}
                                    md={{span: 12}}
                                    xxl={{span: 12}}
                                    className={'course-quiz-detail-created'}
                                >
                                    {testData?.createdAt &&
                                        formatRelative(
                                            parseDate(testData.createdAt),
                                            new Date(),
                                            {locale: vi}
                                        )} <span style={{padding: "0 0.5rem"}}>bởi</span>
                                    {testData?.createdBy && (
                                        <>
                                            <Avatar src={testData?.createdBy?.avatar}
                                                    alt={testData?.createdBy?.firstName}/>
                                            <span
                                                style={{padding: "0 0.5rem"}}>{testData?.createdBy.lastName} {testData?.createdBy.firstName}</span>
                                        </>
                                    )}
                                </Col>
                            </Row>

                            <Row className={'course-quiz-item-information-row'}>
                                <Col
                                    sm={{span: 11}}
                                    md={{span: 11}}
                                    xxl={{span: 11}}
                                    className={'course-quiz-detail-created course-quiz-item-information-title'}
                                >
                                    Cập nhật lần cuối vào lúc
                                </Col>
                                <Col
                                    sm={{span: 12}}
                                    md={{span: 12}}
                                    xxl={{span: 12}}
                                    className={'course-quiz-detail-created'}
                                >
                                    {testData?.updatedAt &&
                                        formatRelative(
                                            parseDate(testData.updatedAt),
                                            new Date(),
                                            {locale: vi}
                                        )} <span style={{padding: "0 0.5rem"}}>bởi</span>
                                    {testData?.createdBy && (
                                        <>
                                            <Avatar src={testData?.updatedBy?.avatar}
                                                    alt={testData?.updatedBy?.firstName}/>
                                            <span
                                                style={{padding: "0 0.5rem"}}>{testData?.updatedBy?.lastName} {testData?.updatedBy?.firstName}</span>
                                        </>
                                    )}
                                </Col>
                            </Row>
                            <Row className={'course-quiz-item-information-row'}>
                                <Col
                                    sm={{span: 11}}
                                    md={{span: 11}}
                                    xxl={{span: 11}}
                                    className={'course-quiz-detail-created course-quiz-item-information-title'}
                                >
                                    Điểm
                                </Col>
                                <Col
                                    sm={{span: 12}}
                                    md={{span: 12}}
                                    xxl={{span: 12}}
                                    className={'course-quiz-detail-created'}
                                >
                                    {testData?.totalGrade}
                                </Col>
                            </Row>
                            {/*</Row>*/}
                        </Collapse.Panel>
                    </Collapse>
                </Col>
            </Row>
            <Row justify="center" style={{marginTop: 12}}>
                <Col sm={24} md={24} lg={20}>
                    <Row gutter={[0, 20]} align={"middle"} justify={"center"}>
                        {isPaginatedResponseQuestionList(testData?.questionList) &&
                            testData?.questionList.listData.map((item, index) => (
                                <Col span={24} key={item.id}>
                                    <QuestionContainerItem
                                        questionData={item}
                                        canEdit={true}
                                        order={(testData.questionList.page - 1) * searchCriteria.pagination.size + index + 1}
                                        mode={"view"}
                                        onEditCallBack={() => {
                                            dispatch(
                                                createTestContentActions.setQuestionIdInDraft(item?.id!)
                                            );
                                        }}
                                        onDeleteCallBack={() => {
                                            confirmationModal('câu hỏi', ``, () => {
                                                if (item && item.id) {
                                                    removeQuestion({
                                                        testId: Number(testId),
                                                        questionId: item.id,
                                                    })
                                                        .unwrap()
                                                        .then(result => {
                                                            AntdNotifier.success(`Xóa câu hỏi thành công`)
                                                        })
                                                        .catch(error => {
                                                            console.log(error)
                                                            if (error?.data) {
                                                                AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá xóa. Thử lại sau.")
                                                            } else {
                                                                AntdNotifier.error("Có lỗi trong quá xóa. Thử lại sau.")
                                                            }
                                                        })
                                                }
                                            });
                                        }}
                                    />
                                </Col>
                            ))}
                    </Row>
                </Col>
            </Row>
            <Row className={'course-quiz-add-question-btns'}>
                <Space>
                    <Button
                        onClick={() => {
                            handleCreateQuestionGroup();
                        }}
                    >
                        <GroupOutlined style={{color: COLOR_SCHEME.primary}}/>
                        Thêm nhóm câu hỏi
                    </Button>
                    <MenuAddNewQuestion handleMenuClick={handleMenuClick}/>
                </Space>
            </Row>
            <Row justify={"center"} style={{marginTop: 20}}>
                <Col sm={24} md={24} lg={20} style={{display: 'flex', justifyContent: 'flex-end'}}>
                    {isPaginatedResponseQuestionList(testData?.questionList) && (
                        <Pagination
                            showSizeChanger
                            pageSize={searchCriteria?.pagination?.size || DEFAULT_PAGE_SIZE}
                            onChange={(page: any, size: any) => {
                                const payload = {
                                    ...searchCriteria,
                                    pagination: {page, size},
                                };
                                dispatch(createTestContentActions.changeCriteria(payload));
                            }}
                            onShowSizeChange={(current, newSize) => {
                                let newSearchCriteria = {
                                    ...searchCriteria,
                                    pagination: {
                                        size: newSize,
                                        page: 1,
                                    },
                                };
                                console.log('ac');
                                dispatch(
                                    createTestContentActions.changeCriteria(newSearchCriteria)
                                );
                                window.scrollTo(0, 0);
                            }}
                            current={testData?.questionList.page}
                            total={testData?.questionList.total}
                        />
                    )}
                </Col>
            </Row>
        </div>)
    } else {
        return <LoadingPage/>
    }
};

export default CourseQuizItemDetailQuestionsPage;
