import { Carousel, Image } from "antd";
import "./HomePage.less";
import banner1 from "@assets/images/banners/banner4.webp";
import banner2 from "@assets/images/banners/banner2.jpg";
import banner3 from "@assets/images/banners/banner3.jpg";

const HomePage = () => {
  return <Carousel autoplay>
    <div className="center">
      <Image src={banner1} className="banner" preview={false} />
    </div>
    <div className="center">
      <Image src={banner2} className="banner" preview={false} />
    </div>
    <div className="center">
      <Image src={banner3} className="banner" preview={false} />
    </div>
  </Carousel>
}

export default HomePage;