import { useAppSelector } from "@hooks/Redux/hooks";
import { authSelectors } from "@redux/features/Auth/AuthSlice";
import { useCheckPermissionMutation } from "@redux/features/Permission/PermissionAPI";
import LimitPermission from "@typescript/interfaces/generated/LimitPermission";
import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import { ReactNode, useEffect } from "react";

type PermissionRendererProps = {
    permissions: PermissionEnum[]
    message?: ReactNode;
    resourceId?: number
};

const useCheckPermission = (permissionName: PermissionEnum, limitPermission: LimitPermission, resourceId: number) => {
    const [checkPermisisonAPI, { data }] = useCheckPermissionMutation();
    useEffect(() => {
        checkPermisisonAPI({
            permissionName,
            permission: limitPermission,
            resourceId
        })
    })
    return data?.hasPermission
}

export const useHasPermission = (permissions: PermissionEnum[], resourceId?: number) => {
    const permissionsMap = useAppSelector(authSelectors.selectUserPermissions);
    if (!permissionsMap) return false;
    if (!resourceId) return permissions.some(p => !!permissionsMap[p]);

    let checkingPermissions = permissions.filter(p => permissionsMap[p]);
    return checkingPermissions.map(p => useCheckPermission(p, permissionsMap[p], resourceId))
        .some(value => value === true);

}

const PermissionRenderer = (props: React.PropsWithChildren<PermissionRendererProps>) => {
    const isHasPermission = useHasPermission(props.permissions, props?.resourceId || undefined);
    return <>{isHasPermission ? props.children : props.message}</>;
};


export default PermissionRenderer;