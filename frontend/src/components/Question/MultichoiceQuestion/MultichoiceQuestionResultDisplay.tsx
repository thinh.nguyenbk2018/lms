import { green } from "@ant-design/colors";
import {faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card, Checkbox, Col, Divider, Radio, Row, Space, Typography, } from "antd";
import { MultichoiceQuestionWithResult } from "../../../redux/features/Quiz/CreateQuizSlice";
import TextEditor from "../../TextEditor/TextEditor";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";
import './MultichoiceQuestionResultDisplay.less';
import COLOR_SCHEME from "@constants/ThemeColor";

const MultichoiceQuestionResultDisplay = (
    props: MultichoiceQuestionWithResult
) => {
    const { resultAnswerList } = props;
    return (
        <>
            <Card>
                {
                    JSON.parse(props?.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
                        <AttachmentRender attachment={a} />
                    </Row>)
                }
                <Row className={'writing-question-result-display-wrapper'}>
                    <Col span={24} className={"question-box"}>
                        <TextEditor
                            onChange={() => {
                            }}
                            value={props.description}
                            readOnly={true}
                            useBubbleTheme
                        />
                    </Col>
                    <Divider />
                    <Col className="answer-box">
                        {resultAnswerList.map((item, index) => {
                            return props.isMultipleAnswer ? (
                                <Space>
                                    <Checkbox
                                        key={item.content}
                                        checked={
                                            resultAnswerList &&
                                            resultAnswerList[index].isChosenByStudent
                                        }
                                    >
                                        <div className={'flex align-items-center'}>
                                            <Typography.Text style={{ marginRight: '0.25rem' }}>
                                                <TextEditor
                                                    value={item.content}
                                                    onChange={() => {
                                                    }}
                                                    readOnly={true}
                                                    useBubbleTheme={true}
                                                />
                                            </Typography.Text>
                                            {item.isCorrect && (
                                                <FontAwesomeIcon
                                                    icon={faCheck}
                                                    style={{ color: green[6] }}
                                                />
                                            )}
                                        </div>
                                    </Checkbox>
                                </Space>
                            ) : (
                                <Space>
                                    <Radio
                                        key={item.content}
                                        checked={
                                            resultAnswerList &&
                                            resultAnswerList[index].isChosenByStudent
                                        }
                                    >
                                        <div className={'flex align-items-center'}>
                                            <Typography.Text style={{ marginRight: '0.25rem' }}>
                                                <TextEditor
                                                    value={item.content}
                                                    onChange={() => {
                                                    }}
                                                    readOnly={true}
                                                    useBubbleTheme={true}
                                                />
                                            </Typography.Text>
                                            {item.isCorrect && item.isChosenByStudent && (
                                                <FontAwesomeIcon
                                                    icon={faCheck}
                                                    style={{ color: green[6] }}
                                                />
                                            )}
                                            {!item.isCorrect && item.isChosenByStudent && (
                                                <FontAwesomeIcon icon={faTimes} style={{ color: COLOR_SCHEME.error }} />
                                            )}
                                        </div>
                                    </Radio>
                                </Space>
                            );
                        })}
                    </Col>
                </Row>
            </Card>
        </>
    );
};

export default MultichoiceQuestionResultDisplay;
