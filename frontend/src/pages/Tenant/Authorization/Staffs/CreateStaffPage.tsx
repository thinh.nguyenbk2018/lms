import { Button, Space } from "antd";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import StaffForm, { StaffFormHandle } from "../../../../components/Authorization/Staff/StaffForm";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import { useGetStaffDetailsQuery } from "../../../../redux/features/Staff/StaffAPI";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import { ROUTING_CONSTANTS } from "../../../../navigation/ROUTING_CONSTANTS";

const CreateStaffPage = () => {
  let navigate = useNavigate();

  let staffFormRef = useRef<StaffFormHandle>(null)

  const onCancel = () => {
    navigate(ROUTING_CONSTANTS.STAFFS_MANAGEMENT);
  }

  const onDone = () => {
    staffFormRef.current?.createStaff()
      .then((result: any) => {
        if (!result) return;
        if ("code" in result && result.code === "SUCCESS") {
          navigate(ROUTING_CONSTANTS.STAFFS_MANAGEMENT)
        }
      })
      .catch((error: any) => {
        if (error?.data?.message) {
          AntdNotifier.error(error.data.message);
          return;
        }
        AntdNotifier.error("Có lỗi xảy ra trong quá trình lưu, thử lại sau");
      })
  }

  return <>
    <ContentHeader
      title="Thêm nhân viên"
      action={
        <Space>
          <Button
            danger
            type="primary"
            color="#73d13d"
            onClick={onCancel}
          >
            Hủy
          </Button>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onDone}
          >
            Xong
          </Button>
        </Space>
      }
    />
    <StaffForm ref={staffFormRef} />
  </>
}

export default CreateStaffPage;