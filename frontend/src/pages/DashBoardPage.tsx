import React from "react";
import {useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import './DashboardPage.less';
import {Button, Col, Row} from "antd";
import IMAGE_CONSTANTS from "@constants/DefaultImage";
import LOCAL_STORAGE_KEYS from "@constants/LocalStorageKey";

const DashBoardPage = () => {

	const userInfo = useAppSelector(authSelectors.selectCurrentUser);

	return (
		<>
			<div className={'class-dash-board-container'}>
				<div className="class-dash-board-greeting">
					<p className="greeting-text">
						{`Chào mừng bạn quay trở lại với ${localStorage.getItem(LOCAL_STORAGE_KEYS.TENANT_NAME)}, ${
							userInfo?.firstName || "học viên thân yêu"
						}`}
					</p>
				</div>
				<Row className="class-background-container" justify="center">
					<Col span={24}>
						<img
							className="class-background-image"
							src={
								localStorage.getItem(LOCAL_STORAGE_KEYS.BANNER) || IMAGE_CONSTANTS.DEFAULT_BACKGROUND_IMAGE
							}
							alt="Ảnh nền"
						/>
					</Col>
				</Row>
				{userInfo?.accountType == 'STAFF' && <div className={'short-cut'}>
					<div className={`class-dash-board-upcoming-sessions`}>
						<p className="class-dash-board-upcoming-sessions__title">
							Lối tắt
						</p>
						<div className={'flex short-cut-btn-wrapper'}>
							<Button type={"primary"} ghost danger>Thiết lập trung tâm</Button>
							<Button type={"primary"} ghost>Tạo khóa học</Button>
							<Button type={"primary"} ghost>Tạo lớp học</Button>
							<Button type={"primary"} ghost>Thêm giáo trình</Button>
							<Button type={"primary"} ghost>Thêm học viên</Button>
							<Button type={"primary"} ghost>Thêm nhân viên</Button>
						</div>
					</div>
				</div>}
			</div>
		</>
	);
};

export default DashBoardPage;