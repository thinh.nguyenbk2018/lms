import CalendarDetail from "../../../components/Calendar/MyCalendar/CalendarDetail/CalendarDetail";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import "./CalendarPage.less";
import useClassSidebar from "@hooks/Routing/useClassSidebar";

const CalendarPage = () => {
    //    const userCalendar = useAppSelector(calendarSelector.)
    const [closeSidebar] = useClassSidebar();

    return (
        <div className={"calendar-page-container"}>
            <ContentHeader additionalStyle={
                {
                    display: "flex",
                    justifyContent: "flex-start",
                    gap: "2em",
                }
            } title={"Lịch"}
            />
            <div className="calendar-page-main-container">
                {/*<div className="calendar-navigator-container">*/}
                {/*    <CalendarNavigator/>*/}
                {/*    <CalendarChoiceBox/>*/}
                {/*</div>*/}
                <div className="calendar-page-main-calendar-container">
                    <CalendarDetail/>
                </div>
            </div>
        </div>
    );
};

export default CalendarPage;
