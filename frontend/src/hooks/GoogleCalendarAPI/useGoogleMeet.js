import React, {useEffect, useState} from "react";
import {API_CONSTANT} from "../../constants/ApiConfigs";


const API_KEY = API_CONSTANT.GCLOUD_API_KEY;
const CLIENT_ID = API_CONSTANT.GCLOUD_CLIENT_ID;
// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

const SCOPES = "https://www.googleapis.com/auth/userinfo.email " +
    "https://www.googleapis.com/auth/calendar.readonly " +
    "https://www.googleapis.com/auth/calendar.events " +
    "https://www.googleapis.com/auth/calendar";



const useGoogleMeet = (start, end) => {

    const [events, setEvents] = useState(null);

    // * Connect to gcloud Calendar API
    useEffect(() => {
        const script = document.createElement("script");
        script.async = true;
        script.defer = true;
        script.src = "https://apis.google.com/js/api.js";

        document.body.appendChild(script);

        console.log("Loading calendar");
        script.addEventListener("load", () => {
            if (window.gapi) {
                handleClientLoad()
                console.log("Loaded client api");
            };
        });
    }, []);


    const handleClientLoad = () => {
        window.gapi.load("client:auth2", initClient);
    };

    /**
     *  Initializes the API client library and sets up sign-in state
     *  listeners.
     */
    function initClient() {
        window.gapi.client.init({
            apiKey: API_KEY,
            clientId: CLIENT_ID,
            discoveryDocs: DISCOVERY_DOCS,
            scope: SCOPES
        }).then(function () {
            // Listen for sign-in state changes.
            window.gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

            // Handle the initial sign-in state.
            updateSigninStatus(window.gapi.auth2.getAuthInstance().isSignedIn.get());
            handleAuthClick();
        }, function(error) {
            console.log(JSON.stringify(error, null, 2));
        });
    }

    function handleAuthClick(event) {
        window.gapi.auth2.getAuthInstance().signIn();
    }


    /**
     *  Called when the signed in status changes, to update the UI
     *  appropriately. After a sign-in, the API is called.
     */
    function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
            console.log("GOOGLE CALENDAR API: SIGNED IN");
            listUpcomingEvents();
        } else {
            console.log("GOOGLE CALENDAR API: NOT SIGNED IN YET");
        }
    }

    /**
     * Print the summary and start datetime/date of the next ten events in
     * the authorized user's calendar. If no events are found an
     * appropriate message is printed.
     */
    function listUpcomingEvents() {
        window.gapi.client.calendar.events.list({
            'calendarId': 'primary',
            'timeMin': (new Date()).toISOString(),
            'showDeleted': false,
            'singleEvents': true,
            'maxResults': 10,
            'orderBy': 'startTime'
        }).then(function(response) {
            var events = response.result.items;
            console.log('Upcoming events:');

            if (events.length > 0) {
                for (let i = 0; i < events.length; i++) {
                    var event = events[i];
                    var when = event.start.dateTime;
                    if (!when) {
                        when = event.start.date;
                    }
                    console.log(event.summary + ' (' + when + ')')
                }
            } else {
                console.log('No upcoming events found.');
            }
        });
    }

    return events;

}

export default useGoogleMeet;

