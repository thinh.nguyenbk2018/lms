import {
    Action,
    configureStore,
    PreloadedState,
    ThunkAction,
} from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";
import { apiSlice } from "../features/CentralAPI";
import { rtkQueryErrorLogger } from "../middleware/ErrorToasterMiddleware";

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware()
            .concat(apiSlice.middleware)
            .concat(rtkQueryErrorLogger);
    },
});

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
    return configureStore({
        reducer: rootReducer,
        preloadedState,
    });
};

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;

export type AppStore = ReturnType<typeof setupStore>; 