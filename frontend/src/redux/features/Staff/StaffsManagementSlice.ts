import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "../../store/store";

const STAFFS_MANAGEMENT_SLICE_KEY = "staffsManagement";


const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice(),

}

export const staffsManagementSlice = createSliceEnhancedWithSearch({
    name: STAFFS_MANAGEMENT_SLICE_KEY,
    initialState,
    reducers: {},
});

export const staffsManagementSelector = {
    selectSearchCriteria: (state: RootState) => state.staffsManagement.searchCriteria
}

export const {changeCriteria} = staffsManagementSlice.actions;