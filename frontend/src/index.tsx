import React from "react";
import "antd/dist/antd.min.css";
import "./App.less";
import "./locales/i18n";
import { BrowserRouter } from "react-router-dom";
import { RouterConfig } from "./navigation/RouterConfig";
import { store } from "./redux/store/store";
import "react-quill/dist/quill.bubble.css";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import { API_CONSTANT } from "./constants/ApiConfigs";
import { Provider } from "react-redux";
import ReactDOM from 'react-dom';
import GlobalDndContext from "@components/GlobalDndContext/GlobalDndContext";
const development: boolean =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development";

export default function isDev(): boolean {
    return development;
}
if (isDev()) {
    // * Do setup that works only on the development environment
    console.log("PROCESS.ENV is: ", process.env);
    console.log("API PROPS:", API_CONSTANT);

    // * Setting up api mock service worker in development  mode for testing 
    // worker.start();
} else {
    // * Do production stuff:
    // *    + Set up sentry
    Sentry.init({
        dsn: "https://b7803fd768d144daa522b289741c79f1@o1236946.ingest.sentry.io/6387283",
        integrations: [new BrowserTracing()],
        // Set tracesSampleRate to 1.0 to capture 100%
        // of transactions for performance monitoring.
        // We recommend adjusting this value in production
        tracesSampleRate: 0.1,
        environment: "developement",
        // release: process.env.REACT_APP_SENTRY_RELEASE,
    });
}
Object.assign(global, { WebSocket: require("websocket").w3cwebsocket });

const container = document.getElementById("root");

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <GlobalDndContext>
                <BrowserRouter>
                    <RouterConfig />
                </BrowserRouter>
            </GlobalDndContext>
        </Provider>
    </React.StrictMode>
    , container
);                 
