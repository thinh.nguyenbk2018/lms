import { Checkbox, Form } from "formik-antd";
import React, { useEffect } from "react";
import { QuestionViewAndTakingTestFormProps } from "../interface/question.interface";
import { Choice, MultichoiceQuestion, } from "../../../redux/features/Quiz/CreateQuizSlice";
import { FormikProps, useFormikContext, withFormik } from "formik";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import * as Yup from "yup";
import { DEFAULT_FRONTEND_ID } from "../../../redux/interfaces/types";
import { QuestionAnswer } from "../../../redux/features/Quiz/examAPI";
import { replaceBlankWithSpecialSpan } from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import { Card, Radio as OriginalRadio, Row, Typography } from "antd";
import TextEditor from "../../TextEditor/TextEditor";
import { baseEditQuestionYupSchema } from "../QuestionContainerItem/baseQuestionSchema";
import QuestionSubmissionButton from "../../ActionButton/QuestionSubmissionButton";
import './MultichoiceQuestionDisplay.less'
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

type AnswerBoxProps = {
	questionHTML: string;
	answerList: (Partial<Choice> | undefined)[];
	isMultipleAnswer: boolean;
};

const QuestionAnswerBox = ({
	questionHTML,
	answerList,
	isMultipleAnswer,
}: AnswerBoxProps) => {
	const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML);
	let blankCounter = 0;
	const { values, setFieldValue, handleSubmit, errors } =
		useFormikContext<QuestionAnswer>();

	useEffect(() => {
		if (!isMultipleAnswer) {
			setFieldValue('answers', [answerList.filter(x => x?.isChosenByStudent)[0]?.answerKey])
		} else {
			const arr = []
			for (let i = 0; i < answerList.length; i++) {
				if (answerList[i]?.isChosenByStudent) {
					arr.push(answerList[i]?.answerKey);
				}
			}
			setFieldValue('answers', arr)
		}
	}, [answerList])

	// console.log('errors: ', errors);

	return (
		<>
			<div className={"MULTI_CHOICE-question-container answer-box"}>
				{answerList && answerList.length > 0 ? (
					!isMultipleAnswer ? (
						<div className={"sub-option-list-container"}>
							{answerList.map(
								(item, index) =>
									item && (
										<OriginalRadio
											// checked={item.isChosenByStudent ?  values.answers[0] === item.answerKey}
											checked={values.answers[0] === item.answerKey}
											// value={item.answerKey}
											onChange={(e) => {
												const value = e.target.checked;
												const answersCopy = [item.answerKey];
												// console.log('answer: ', answersCopy);
												setFieldValue(`answers`, answersCopy, true);
												// console.log('answers: ', answersCopy);
												handleSubmit();
											}}
										>
											<span
												onClick={(e) => {
													e.stopPropagation();
													e.preventDefault();
												}}
											>
												<TextEditor
													onChange={() => { }}
													readOnly={true}
													value={answerList[index]?.content}
													useBubbleTheme={true}
												/>
											</span>
										</OriginalRadio>
									)
							)}
						</div>
					) : (
						// TODO: Add on checked change method to set the answers[index] = item.answerKey
						<div className={"sub-option-list-container"}>
							{answerList.map(
								(item, index) =>
									item && (
										<Checkbox checked={values.answers.find(x => x === item?.answerKey) != undefined} name={`answers[${index}]`}>
											<span
												onClick={(e) => {
													e.stopPropagation();
													e.preventDefault();
												}}
												onChange={(checkedValue) => {
													const answersCopy = [...values.answers];
													if (!item || !item.answerKey) {
														return;
													}
													const answerKeyIndex = answersCopy.indexOf(
														item.answerKey
													);
													if (checkedValue && answerKeyIndex === -1) {
														answersCopy.push(item.answerKey);
													}
													if (!checkedValue && answerKeyIndex !== -1) {
														answersCopy.splice(answerKeyIndex, 1);
													}
													setFieldValue(`answers`, answersCopy);
													handleSubmit();
												}}
											>
												<TextEditor
													onChange={() => { }}
													readOnly={true}
													value={answerList[index]?.content}
													useBubbleTheme={true}
												/>
											</span>
										</Checkbox>
									)
							)}
						</div>
					)
				) : (
					<Typography.Text type={"danger"}>
						Chưa có lựa chọn nào, hãy nhấn vào nút "Thêm câu trở lời mới" để bắt đầu soạn lựa chọn cho câu hỏi.
					</Typography.Text>
				)}
			</div>
		</>
	);
};

const QuestionViewBox = ({
	questionHTML,
	answerList,
	isMultipleAnswer,
}: AnswerBoxProps) => {
	const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML);
	let blankCounter = 0;
	const { values, setFieldValue } = useFormikContext<QuestionAnswer>();

	return (
		<>
			<div className={"MULTI_CHOICE-question-container"}>
				{answerList && answerList.length > 0 ? (
					!isMultipleAnswer ? (
						<div className={"sub-option-list-container"}>
							{answerList.map(
								(item, index) =>
									item && (
										<OriginalRadio
											checked={values.answers[0] === item.answerKey}
										>
											<span
												onClick={(e) => {
													e.stopPropagation();
													e.preventDefault();
												}}
											>
												<TextEditor
													onChange={() => { }}
													readOnly={true}
													value={answerList[index]?.content}
													useBubbleTheme={true}
												/>
											</span>
										</OriginalRadio>
									)
							)}
						</div>
					) : (
						// TODO: Add on checked change method to set the answers[index] = item.answerKey
						<div className={"sub-option-list-container"}>
							{answerList.map(
								(item, index) =>
									item && (
										<Checkbox name={`answers[${index}]`}>
											<span
												onClick={(e) => {
													e.stopPropagation();
													e.preventDefault();
												}}
												onChange={(checkedValue) => {
													const answersCopy = [...values.answers];
													if (!item || !item.answerKey) {
														return;
													}
													const answerKeyIndex = answersCopy.indexOf(
														item.answerKey
													);
													if (checkedValue && answerKeyIndex === -1) {
														answersCopy.push(item.answerKey);
													}
													if (!checkedValue && answerKeyIndex !== -1) {
														answersCopy.splice(answerKeyIndex, 1);
													}
													setFieldValue(`answers`, answersCopy);
												}}
											>
												<TextEditor
													onChange={() => { }}
													readOnly={true}
													value={answerList[index]?.content}
													useBubbleTheme={true}
												/>
											</span>
										</Checkbox>
									)
							)}
						</div>
					)
				) : (
					<Typography.Text type={"danger"}>
						Chưa có lựa chọn nào, hãy nhấn vào nút "Thêm câu trở lời mới" để bắt đầu soạn lựa chọn cho câu hỏi.
					</Typography.Text>
				)}
			</div>
		</>
	);
};
/** Display the test for review the question content and possibly retrieve the answer */
const WrappedMultichoiceDisplay = (
	props: MultichoiceViewHocProps & FormikProps<QuestionAnswer>
) => {
	// * Question info to display the requirements
	const { description, answerList = [], isMultipleAnswer = true, attachment } = props;

	// * Form State from formik
	const {
		values,
		dirty,
		touched,
		isValid,
		handleSubmit,
		setFieldValue,
		errors,
	} = props;

	return (
		<>
			<Form>
				<Card className={'multi-choice-question-display-wrapper'}>
					{
						JSON.parse(attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
							<AttachmentRender attachment={a} />
						</Row>)
					}
					<TextEditor
						onChange={() => { }}
						value={description}
						readOnly={true}
						useBubbleTheme
					/>
					<QuestionAnswerBox
						answerList={answerList}
						questionHTML={description}
						isMultipleAnswer={isMultipleAnswer}
					/>
					{/*<Row style={{ margin: "1em 0" }} justify="end">*/}
					{/*	<QuestionSubmissionButton*/}
					{/*		onClick={() => handleSubmit()}*/}
					{/*		title={"Gửi đáp án"}*/}
					{/*	/>*/}
					{/*</Row>*/}
				</Card>
			</Form>
		</>
	);
};

// * Question configuration props with Formik HOC
const multiChoiceAnswerSchema = Yup.object({
	// answers: Yup.array().of(
	// 	Yup.string().required("Câu trả lời cho một ô trống không được để trống")
	// ),
}).concat(baseEditQuestionYupSchema);

type MultichoiceViewHocProps =
	QuestionViewAndTakingTestFormProps<MultichoiceQuestion>;
/** A question displayer need the Partial<QuestionDataType> to display the question statement
 *  ? QuestionAnswer to record student choice quen taking the test
 *
 * */
const MultichoiceDisplayWithForm = withFormik<
	MultichoiceViewHocProps,
	QuestionAnswer
>({
	mapPropsToValues: (props) => {
		return {
			id: props.id || DEFAULT_FRONTEND_ID,
			description: props.description || DEFAULT_TEXT_EDITOR_HTML,
			point: props.point || 0,
			answerList: props.answerList || [],
			isMultipleAnswer: props.isMultipleAnswer || true,
			answers: [],
			optionalStudentNote: "",
		};
	},
	validationSchema: multiChoiceAnswerSchema,
	handleSubmit: (values, formikBag) => {
		const valuesToSubmit: QuestionAnswer = {
			...values,
		};
		// console.log('submit: ', valuesToSubmit);
		formikBag.props.onConfirmSubmit?.(valuesToSubmit);
		// console.log("Multichoice answer values is: ", values);
	},
})(WrappedMultichoiceDisplay);
export default MultichoiceDisplayWithForm;
