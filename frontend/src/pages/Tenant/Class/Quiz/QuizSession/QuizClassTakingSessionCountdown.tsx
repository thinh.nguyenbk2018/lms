import Countdown from "react-countdown";
import React from "react";
import {Simulate} from "react-dom/test-utils";


const renderer = ({hours, minutes, seconds}: { hours: number, minutes: number, seconds: number }) => {

    return <span>{hours * 60 + minutes} phút {seconds} giây</span>;
};

const QuizClassTakingSessionCountdown = React.memo(({remainingTime, submitSession, haveTimeLimit}: {haveTimeLimit: boolean | undefined, remainingTime: number | undefined, submitSession: () => void}) => {
    if (haveTimeLimit == false || remainingTime == undefined) {
        return <span>Không giới hạn thời gian</span>
    } else {
        return (
            <Countdown
                renderer={renderer}
                daysInHours={true}
                date={Date.now() + remainingTime * 1000}
                onComplete={() => {
                    submitSession();
                }}
            />);
    }
})

export default QuizClassTakingSessionCountdown;