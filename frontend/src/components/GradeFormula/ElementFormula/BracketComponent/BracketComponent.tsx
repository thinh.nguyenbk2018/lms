import { Bracket } from "../../../../redux/features/Grade/GradeFormulaSlice"
import "./BracketComponent.less"

const BracketComponent = ({ value }: { value: Bracket }) => {
  return <span className="bracket-component">
    {value}
  </span>
}

export default BracketComponent;