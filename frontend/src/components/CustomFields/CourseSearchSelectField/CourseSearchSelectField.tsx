import React, {CSSProperties} from 'react';
import {FieldProps} from "formik";
import {FormItem} from "formik-antd";
import {Typography} from "antd";
import CourseSearchSelect from "../../Courses/Forms/CourseSearchSelect/CourseSearchSelect";


export type SearchSelectFieldProps =
    FieldProps<any>
    & { label?: string, style?: CSSProperties, required?: boolean, disabled?: boolean, onSelect?: () => void, showErrorMessage?: boolean };

const CourseSearchSelectField = ({
                                     field,
                                     form: {
                                         touched,
                                         errors,
                                         setFieldValue
                                     },
                                     ...otherProps
                                 }: SearchSelectFieldProps) => {
    const {name, value, onChange, onBlur} = field;
    const errorMsg = touched[name] && errors[name];

    return (
        <>
            <FormItem required={otherProps?.required} label={otherProps?.label || ""} name={name}>
                <CourseSearchSelect
                    {...field}
                    {...otherProps}
                    onSelect={(courseID) => {
                        setFieldValue(name, courseID, true);
                    }}
                    onChange={(value) => {
                        setFieldValue(name, value, true);
                    }}
                />
                <Typography.Text type={"danger"}>
                    <>
                        {errorMsg}
                    </>
                </Typography.Text>
            </FormItem>
        </>
    );
};

export default CourseSearchSelectField;