import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import { useMatch, useResolvedPath } from "react-router";

export type SubSidebarTab = {
    // * What to show on the UI
    displayName?: string;
    // * Where to go to when clicked on
    navigationRoute: string;
    require_permissions?: PermissionEnum[];
    key: string
};

export function useActiveSubSideBar(subSideBarTabs: SubSidebarTab[]) {
    function useIsActive(subSideBarTab: SubSidebarTab) {
        let resolved = useResolvedPath(subSideBarTab.navigationRoute);
        // console.log('resolved: ', resolved);
        let match = useMatch({ path: resolved.pathname, end: false });
        return match ? subSideBarTab.navigationRoute : null;
    }

    return subSideBarTabs.map(useIsActive).find(subSideBarTab => !!subSideBarTab)
}
