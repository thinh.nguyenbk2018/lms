import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { useGetClassInformationQuery } from '../../redux/features/Class/ClassAPI';
import { sideBarActions } from '../../redux/features/SideBar/SidebarSlice';
import AntdNotifier from '../../utils/AntdAnnouncer/AntdNotifier';
import { useAppDispatch } from '../Redux/hooks';

const useClassCourseInformation = (classId: number) => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const classIdInNumber: number = Number(classId);

    const { data: classInformation, isSuccess, isError, isFetching, isUninitialized } = useGetClassInformationQuery(classId);

    const courseInformation = classInformation?.course;


    React.useEffect(
        () => {
            if (isSuccess) {
                if (!classInformation.id || !classInformation.course?.id) {
                    AntdNotifier.error("Lỗi xảy ra khi lấy thông tin lớp. Liên hệ dev team");
                }
            }
        },
        [isSuccess]
    );
    return {
        classId: classInformation?.id,
        courseId: classInformation?.course?.id,
        classInfo: classInformation,
        isFetchingClass: isFetching,
        isSuccessClass: isSuccess,
        isErrorClass: isError
    }
}


export default useClassCourseInformation;