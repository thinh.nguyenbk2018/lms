import {ID} from "../../../redux/interfaces/types";
import {useGetQuestionQuery} from "../../../redux/features/Quiz/examAPI";
import QuestionContainerItem from "./QuestionContainerItem";
import {Spin, Typography} from "antd";

const QuestionItemWithId = ({questionId,testContentId} : {questionId: ID,testContentId: ID}) => {
    const {data: questionData,isSuccess,isError,isFetching, error} = useGetQuestionQuery({testContentId,questionId});

    if (!questionId || !questionData || !questionData.type) {
        return null;
    }
    return (
        <div className={"question-item-with-id-wrapper"}>
            {isSuccess && <QuestionContainerItem questionData={questionData} mode={"view"}/>}
            {isFetching && <Spin/>}
            <Typography.Text type={"danger"}>
                {isError && JSON.stringify(error)}
            </Typography.Text>
        </div>
    );
};

export default QuestionItemWithId;