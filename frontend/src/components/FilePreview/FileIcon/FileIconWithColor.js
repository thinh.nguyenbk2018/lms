import {FileIcon, defaultStyles} from "./index";
import {ReactComponent as CustomFileSvg} from "./images/file_icon.svg";
import PropStyle from 'prop-types';
import {styleDefObj} from "./style-customize";
import React from "react";
import {GENERAL_FILE_TYPES} from "../Utils/FileUtils";

const extensionList = Object.keys(defaultStyles);

const DEFAULT_FILE_ICON = {
    ext: GENERAL_FILE_TYPES.UNKNOWN,
    Component: CustomFileSvg
}

const FileIconWithColor = ({width = 50, height = undefined, extension}) => {
    const ext = extension;
    const customDefaultLabelColor = styleDefObj[ext]
        ? styleDefObj[ext]["labelColor"] ?? "#777"
        : "#777";

    // Library defined default labelCOlor
    const libDefaultGlyphColor =
        defaultStyles[ext] && defaultStyles[ext]["labelColor"];

    return (
        <>
            <div style={{width: `${width}px`, height: `${height}`}}>
                {
                    (ext === DEFAULT_FILE_ICON.ext) ?
                        <DEFAULT_FILE_ICON.Component style={{width: "100%", height: "auto"}}/> : <FileIcon
                            extension={ext}
                            glyphColor={libDefaultGlyphColor ?? customDefaultLabelColor}
                            labelColor={customDefaultLabelColor}
                            {...defaultStyles[ext]}
                            {...styleDefObj[ext]}
                        />
                }
            </div>
        </>
    );
};

FileIconWithColor.propsType = {
    width: PropStyle.number,
    height: PropStyle.number,
    extension: PropStyle.string
}
export default FileIconWithColor;


// Define custom icon images
// const customIcons = [
//     {
//         ext: "hwp",
//         Component: IcFileHwp
//     }
// ];


//  return (
//     <div className="App">
//         {customIcons.map((icon) => {
//             const { ext, Component } = icon;
//             return (
//                 <div className="icon">

//                 </div>
//             );
//         })}
//     </div>
// </div>


