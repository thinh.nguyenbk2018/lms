/* eslint-disable no-sparse-arrays */
import {Avatar, Button, Input, List, Modal, Skeleton, Space, Tooltip, Typography,} from "antd";
import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import CustomTable from "../../../../components/CustomTable/CustomTable";
import {addIconSvg, testSvg,} from "../../../../components/Icons/SvgIcons";
import TableSearch from "../../../../components/Search/TableSearch";
import {useAppSelector} from "../../../../hooks/Redux/hooks";
import {classStudentSelector, classStudentSlice,} from "../../../../redux/features/Class/StudentAPI/ClassStudentSlice";
import LoadingPage from "../../../UtilPages/LoadingPage";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {DeleteOutlined} from "@ant-design/icons";
import './ClassStudentPage.less';
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import {
    useAddStudentsToClassMutation,
    useGetStudentsQuery,
    useLazyGetStudentsNotInClassQuery
} from "@redux/features/Class/StudentAPI/ ClassStudentAPI";
import {Student, useDeleteStudentFromClassMutation} from "@redux/features/Student/StudentAPI";
import PermissionRenderer, {useHasPermission} from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";


const ClassStudentPage = () => {
    const navigate = useNavigate();
    const [closeSidebar] = useClassSidebar();
    const { classId } = useParams();
    const searchCriteria = useAppSelector(
        classStudentSelector.selectSearchCriteria
    );

    const { classInfo } = useClassCourseInformation(Number(classId));

    const hasPermissionWithStudent = useHasPermission([PermissionEnum.REMOVE_STUDENT_FROM_CLASS, PermissionEnum.VIEW_DETAIL_STUDENT])

    const [
        getStudentsNotInClass,
        { data: studentsNotInClass, isLoading: isGetting },
    ] = useLazyGetStudentsNotInClassQuery();

    const [addStudentsToClass] = useAddStudentsToClassMutation();
    const [showModal, setShowModal] = useState<boolean>(false);
    const [selectedStudents, setSelectedStudents] = useState<Omit<Student, "username" | "address">[]>([]);
    let timeout: any;

    const shouldSkip = () => {
        return isNaN(Number(classId));
    };
    // * Redux state for advanced searching the students
    const changeCriteria = classStudentSlice.actions.changeCriteria;

    useEffect(() => {
        if (showModal) {
            getStudentsNotInClass({ keyword: "", classId: Number(classId) });
        }
    }, [showModal]);

    const {
        data: paginatedResponse,
        isSuccess,
        isFetching,
    } = useGetStudentsQuery(
        { classId: Number(classId), searchCriteria },
        {
            skip: shouldSkip(),
        }
    );

    const [deleteStudent] = useDeleteStudentFromClassMutation();

    const columns = [
        {
            title: "Avatar",
            dataIndex: "avatar",
            width: "10%",
            render: (avatar: string) => <Avatar src={avatar}>User ava</Avatar>,
            align: "center" as const,
        },
        {
            title: "Họ",
            dataIndex: "lastName",
            width: "10%",
        },
        {
            title: "Tên",
            dataIndex: "firstName",
            key: "code",
            width: "10%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
            },
            sortDirections: ["descend", "ascend"],
            sortOrder:
                searchCriteria.sort.filter((x: any) => x.field === "firstName").length > 0 &&
                searchCriteria.sort.filter((x: any) => x.field === "firstName")[0].order,
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "status",
            width: "10%",
            render: (email: string) => (
                <a href={`mailto:${email}`}>{email || "Không"}</a>
            ),
        },
        {
            title: "Số điện thoại",
            dataIndex: "phone",
            width: "20%",
        },
        {
            title: "Địa chỉ",
            dataIndex: "address",
            width: "25%",
        },
        {
            title: "",
            key: "action",
            render: (record: any) => (
                <Space>
                    <PermissionRenderer permissions={[PermissionEnum.VIEW_DETAIL_STUDENT]}>
                        <Tooltip title={"Xem thông tin chi tiết của học viên"}>
                            <Typography.Link
                                className={'flex'}
                                onClick={() => {
                                    navigate(ROUTING_CONSTANTS.CLASS_STUDENT_DETAILS.replace(":classId", classId!).replace(":studentId", record.userId));
                                }}
                            >
                                {testSvg}
                            </Typography.Link>
                        </Tooltip>
                    </PermissionRenderer>
                    {classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.REMOVE_STUDENT_FROM_CLASS]}>
                        <Tooltip title={"Xóa học viên ra khỏi lớp học"}>
                            <DeleteOutlined
                                className={'delete-item-btn'}
                                onClick={() => {
                                    confirmationModal('học viên', `${record.lastName} ${record.firstName}`, () => {
                                        if (!classId || !record.userId) {
                                            console.log('classId: ', classId, record.id);
                                            return;
                                        } else {
                                            deleteStudent({classId: Number(classId), studentId: record.userId})
                                                .unwrap()
                                                .then((res: any) => {
                                                    AntdNotifier.success(`Xóa học viên ${record.lastName} ${record.firstName} thành công`);
                                                })
                                                .catch((res: any) => {
                                                    AntdNotifier.error('Đã có lỗi xảy ra trong xóa trình xóa học viên, vui lòng thử lại sau');
                                                })
                                        }
                                    }, 'ra khỏi lớp học');
                                }}
                            />
                        </Tooltip>
                    </PermissionRenderer>}
                </Space>
            ),
        },
    ];

    const renderFunction = () => {
        let componentToRender: JSX.Element = <></>;
        if (isFetching) {
            componentToRender = <LoadingPage />;
        }
        if (isSuccess) {
            componentToRender = (
                <CustomTable
                    data={paginatedResponse?.listData || []}
                    columns={hasPermissionWithStudent ? columns : columns.filter(col => col.key !== 'action')}
                    metaData={paginatedResponse}
                    searchCriteria={searchCriteria}
                    changeSearchActionCreator={changeCriteria}
                />
            );
        }
        return componentToRender;
    };

    const addStudents = () => {
        return addStudentsToClass({
            classId: Number(classId),
            studentIds: selectedStudents.map((s) => s.id),
        })
            .unwrap()
            .then((result: any) => {
                if (!result) return;
                if ("code" in result && result.code === "SUCCESS") {
                    setSelectedStudents([]);
                    setShowModal(false);
                    AntdNotifier.success(
                        "Thêm học viên thành công."
                    );
                } else {
                    AntdNotifier.error(
                        "Có lỗi xảy ra trong quá trình cập nhật dữ liệu, thử lại sau"
                    );
                }
            })
            .catch((error: any) => {
                if (error?.data?.message) {
                    AntdNotifier.error(error.data.message);
                    return;
                }
                AntdNotifier.error(
                    "Có lỗi xảy ra trong quá trình cập nhật dữ liệu, thử lại sau"
                );
            });
    };

    const searchStudents = (keyword: string) => {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            getStudentsNotInClass({ keyword, classId: Number(classId) });
        }, 300);
    };

    return (
        <>
            <ContentHeader
                title={"Học viên"}
                action={
                    <>
                        <div className={"search-container"}>
                            <TableSearch
                                searchFields={[{ title: "Từ khóa", key: "key" }]}
                                defaultFilters={[]}
                                changeCriteria={changeCriteria}
                                searchCriteria={searchCriteria}
                            />
                            {classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.ADD_STUDENT_TO_CLASS]}>
                                <Button
                                    type="primary"
                                    className={"button-with-icon"}
                                    onClick={() => setShowModal(true)}
                                >
                                    {addIconSvg}
                                    <span>Thêm</span>
                                </Button>
                            </PermissionRenderer>}
                            <Modal
                                maskClosable={false}
                                title="Thêm học viên"
                                visible={showModal}
                                onCancel={() => {
                                    setShowModal(false);
                                    setSelectedStudents([]);
                                }}
                                onOk={addStudents}
                                okText="Thêm học viên"
                                cancelText="Hủy"
                                width={800}
                                destroyOnClose={true}
                                closable
                            >
                                <Input.Search
                                    placeholder="Tìm học viên"
                                    size="large"
                                    style={{ marginBottom: 15 }}
                                    onChange={({ currentTarget: { value } }) =>
                                        searchStudents(value)
                                    }
                                />
                                <div
                                    style={{
                                        overflowY: "auto",
                                        height: 225,
                                        padding: 2,
                                        border: "1px solid #d9d9d9",
                                    }}
                                >
                                    <List
                                        header={null}
                                        footer={null}
                                        locale={{emptyText: 'Không có dữ liệu'}}
                                        dataSource={studentsNotInClass?.studentInfos.filter(
                                            (s) => !selectedStudents.map((st) => st.id).includes(s.id)
                                        )}
                                        bordered={
                                            !!studentsNotInClass?.studentInfos.filter(
                                                (s) =>
                                                    !selectedStudents.map((st) => st.id).includes(s.id)
                                            ).length
                                        }
                                        renderItem={(student) =>
                                            selectedStudents
                                                .map((s) => s.id)
                                                .includes(student.id) || (
                                                <Skeleton
                                                    avatar
                                                    title={false}
                                                    loading={isGetting}
                                                    active
                                                >
                                                    <List.Item
                                                        extra={
                                                            <Button
                                                                type="primary"
                                                                onClick={() =>
                                                                    setSelectedStudents((prev) => [
                                                                        ...prev,
                                                                        student,
                                                                    ])
                                                                }
                                                                className={'button-with-icon'}
                                                            >
                                                                Thêm
                                                            </Button>
                                                        }
                                                    >
                                                        <List.Item.Meta
                                                            avatar={<Avatar src={student.avatar} />}
                                                            title={`${student.lastName} ${student.firstName}`}
                                                            description={`${student.email} - ${student.phone}`}
                                                        />
                                                    </List.Item>
                                                </Skeleton>
                                            )
                                        }
                                    />
                                </div>
                                <Typography.Title level={5} style={{ marginTop: 10 }}>
                                    Danh sách thêm
                                </Typography.Title>
                                <div
                                    style={{
                                        overflowY: "auto",
                                        height: 225,
                                        padding: 2,
                                        border: "1px solid #d9d9d9",
                                    }}
                                >
                                    <List
                                        header={null}
                                        footer={null}
                                        dataSource={selectedStudents}
                                        bordered={selectedStudents.length != 0}
                                        locale={{emptyText: 'Không có dữ liệu'}}
                                        renderItem={(student) => (
                                            <Skeleton avatar title={false} loading={isGetting} active>
                                                <List.Item
                                                    extra={
                                                        <Button
                                                            type="primary"
                                                            danger
                                                            onClick={() =>
                                                                setSelectedStudents((prev) =>
                                                                    prev.filter((s) => s.id != student.id)
                                                                )
                                                            }
                                                        >
                                                            Xóa
                                                        </Button>
                                                    }
                                                >
                                                    <List.Item.Meta
                                                        avatar={<Avatar src={student.avatar} />}
                                                        title={`${student.lastName} ${student.firstName}`}
                                                        description={`${student.email} - ${student.phone}`}
                                                    />
                                                </List.Item>
                                            </Skeleton>
                                        )}
                                    />
                                </div>
                            </Modal>
                        </div>
                    </>
                }
            />
            {renderFunction()}
        </>
    );
};

export default ClassStudentPage;
