import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "@redux_store";

const COURSE_CLASS_SLICE_KEY = "course_class";

const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice()
}

export const courseClassListSlice = createSliceEnhancedWithSearch({
    name: COURSE_CLASS_SLICE_KEY,
    initialState,
    reducers: {},
});

export const courseClassListSelector = {
    selectSearchCriteria: (state: RootState) => state.courseClassList.searchCriteria
}

export const {changeCriteria} = courseClassListSlice.actions;