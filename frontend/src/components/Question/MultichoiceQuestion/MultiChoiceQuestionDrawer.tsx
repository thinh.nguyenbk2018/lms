import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Col, Drawer, Modal, Row, Space, Typography } from "antd";
import React, { useRef } from "react";
import CancelButton from "../../ActionButton/CancelButton";
import DoneButton from "../../ActionButton/DoneButton";
import { isMultichoiceQuestion, MultichoiceQuestion, } from "@redux/features/Quiz/CreateQuizSlice";
import * as Yup from "yup";
import { Field, Formik } from "formik";
import { Form, FormItem, Input } from "formik-antd";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import OptionsAsRadioOrCheckBox from "./OptionsAsRadioOrCheckBox";
import { DEFAULT_FRONTEND_ID } from "@redux/interfaces/types";
import { baseCreateQuestionYupSchema, baseEditQuestionYupSchema, } from "../QuestionContainerItem/baseQuestionSchema";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import "./MultichoiceQuestionDisplay.less";
import { useAppDispatch } from "@hooks/Redux/hooks";
import useQuestionDrawerType from "../../../hooks/QuizDrawer/useQuestionDrawerType";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import QUESTION_LABEL_CONSTANT from "../constant/QuestionLabelConstant";
import './MultiChoiceQuestionDrawer.less';
import AttachmentQuestion, { Attachment } from "../Attachment/AttachmentQuestion";

export const choiceSchema = Yup.object({
    content: Yup.string().required("Lựa chọn là thông tin bắt buộc"),
    isCorrect: Yup.bool(),
});

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

let initialValues: Partial<MultichoiceQuestion> = {
    id: DEFAULT_FRONTEND_ID,
    description: DEFAULT_TEXT_EDITOR_HTML,
    point: 0,
    note: "",
    answerList: [],
    isMultipleAnswer: true,
    attachment: "[]"
};

const MultiChoiceQuestionDrawer = () => {
    // * The drawer state
    const {
        visible,
        closeDrawer,
        openDrawer,
        isEditing,
        questionDataInDraft,
        doneHandler,
        isFetching,
        isError,
        isSuccess,
        questionIdInDraft,
        testName,
    } = useQuestionDrawerType("MULTI_CHOICE");

    const dispatch = useAppDispatch();
    //* The container for the modal to pop up
    const drawerRef = useRef<HTMLDivElement | null>(null);

    function closeConfirm(onOk: () => void, onCancel: () => void) {
        Modal.confirm({
            title: "Xác nhận hủy bỏ câu hỏi",
            icon: <ExclamationCircleOutlined />,
            content:
                "Chúng tôi nhận thấy rằng bạn đã soạn phần nào câu hỏi. Chọn xác nhận sẽ xóa hết tất cả.",
            okText: "Xác nhận",
            cancelText: "Tiếp tục soạn",
            onOk: onOk,
            onCancel: onCancel,
            getContainer: drawerRef.current!!,
        });
    }

    if (questionDataInDraft && !isMultichoiceQuestion(questionDataInDraft)) {
        AntdNotifier.error(
            "Lỗi khi hiển thị câu hỏi, vui lòng refresh lại hoặc liên hệ chúng tôi."
        );
        return null;
    }

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={
                    {
                        id: questionDataInDraft?.id || DEFAULT_FRONTEND_ID,
                        description:
                            questionDataInDraft?.description || DEFAULT_TEXT_EDITOR_HTML,
                        point: questionDataInDraft?.point || 0,
                        note: questionDataInDraft?.note || "",
                        answerList: questionDataInDraft?.answerList || [],
                        isMultipleAnswer: questionDataInDraft?.isMultipleAnswer == undefined ? true : questionDataInDraft?.isMultipleAnswer,
                        attachment: questionDataInDraft?.attachment ? questionDataInDraft?.attachment : "[]"
                    } ?? initialValues
                }
                validationSchema={Yup.object({
                    description: Yup.string().required("Mô tả câu hỏi là thông tin bắt buộc")
                        .test("required field", "Mô tả câu hỏi là thông tin bắt buộc", (value: any) => {
                            return !(value === '<p><br></p>' || value === '' || value == undefined);

                        }),
                    answerList: Yup.array()
                        .of(choiceSchema)
                        .min(2, "Trắc nghiệm phải có ít nhất 2 lựa chọn")
                        .test(
                            "Check if there are at least one answer",
                            "Ít nhất phải có một lựa chọn đúng ",
                            (value) => {
                                if (!value) {
                                    return false;
                                }
                                const correctAnswers = value.map((item) => item.isCorrect);
                                if (!correctAnswers) return false;
                                return true;
                            }
                        ),
                }).concat(
                    isEditing ? baseEditQuestionYupSchema : baseCreateQuestionYupSchema
                )}
                onSubmit={(values) => {
                    const valuesToSubmit: Omit<MultichoiceQuestion, "order"> = {
                        type: "MULTI_CHOICE",
                        ...values,
                        answerList: values.answerList.map((answer, index) => ({ ...answer, order: index + 1 })),
                    };
                    doneHandler(valuesToSubmit);
                }}
            >
                {({
                    dirty,
                    isValid,
                    handleSubmit,
                    setFieldValue,
                    values,
                    errors,
                    resetForm,
                    initialValues
                }) => {
                    return (
                        <Form>
                            <Drawer
                                maskClosable={false}
                                closable={false}
                                title={
                                    <Space direction={"vertical"}>
                                        <Typography.Text>
                                            {`${testName || "Không thể hiển thị tên đề kiểm tra"}`}
                                        </Typography.Text>
                                        <Typography.Text strong>
                                            Câu hỏi trắc nghiệm
                                        </Typography.Text>
                                    </Space>
                                }
                                placement="right"
                                size={"large"}
                                visible={visible}
                                extra={
                                    <Space>
                                        <CancelButton
                                            onClick={() => {
                                                if (dirty) {
                                                    closeConfirm(
                                                        () => {
                                                            closeDrawer();
                                                            setFieldValue("description", "");
                                                        },
                                                        () => {
                                                        }
                                                    );
                                                } else {
                                                    closeDrawer();
                                                }
                                            }
                                            }
                                        />
                                        <DoneButton
                                            disabled={!dirty}
                                            onClick={() => handleSubmit()}
                                        />
                                    </Space>
                                }
                                className={"MULTI_CHOICE-drawer"}
                            >
                                <>
                                    <div ref={drawerRef} />
                                    <Row align={"top"} justify={"end"}>
                                        <Col>
                                            <FormItem
                                                name={"point"}
                                                label={QUESTION_LABEL_CONSTANT.POINT}
                                            >
                                                <Input type={"number"} name={"point"} />
                                            </FormItem>
                                        </Col>
                                    </Row>
                                    <Row className={'question-drawer-instruction'}>
                                        <Typography.Paragraph>
                                            Sau khi tạo các câu hỏi. Hãy đánh dấu (các) câu trả lời đúng.
                                        </Typography.Paragraph>
                                        <Typography.Paragraph className={'question-drawer-note'}>
                                            *Lưu ý: Mỗi câu hỏi trắc nghiệm phải có ít nhất 2 lựa chọn.
                                        </Typography.Paragraph>
                                    </Row>

                                    <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                                        <Col span={24}>
                                            <Field
                                                name={"attachment"}
                                                placeholder={"Type article content"}
                                                component={AttachmentQuestion}
                                                label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                                            />
                                        </Col>
                                    </Row>

                                    <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                                        <Field
                                            name={"description"}
                                            placeholder={"Type article content"}
                                            component={TextEditorField}
                                            label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                                        />
                                    </Row>

                                    <OptionsAsRadioOrCheckBox />
                                </>
                            </Drawer>
                        </Form>
                    );
                }}
            </Formik>
        </>
    );
};

export default MultiChoiceQuestionDrawer;
