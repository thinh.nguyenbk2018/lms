import {
	faBan,
	faRocket,
	faSkull,
	faStar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ButtonProps, Button } from "antd";

const QuestionSubmissionButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			type={"primary"}
			icon={
				<FontAwesomeIcon icon={faRocket} style={{ margin: "0 .5em 0 0" }} />
			}
		>
			{`${props.title || "Nộp bài"}`}
		</Button>
	);
};

export default QuestionSubmissionButton;
