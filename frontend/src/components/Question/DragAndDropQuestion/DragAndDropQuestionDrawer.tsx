import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Button, Col, Drawer, Form, Modal, Row, Space, Typography } from 'antd';
import React, { useRef, useState } from 'react';
import CancelButton from "../../ActionButton/CancelButton";
import DoneButton from "../../ActionButton/DoneButton";
import { FillBlankDragAndDropQuestion, isFIB_DND_Question } from './../../../redux/features/Quiz/CreateQuizSlice';
import { Field, FieldArray, Formik, useFormikContext } from 'formik';
import * as Yup from 'yup';
import { FormItem, Input } from 'formik-antd';
import TextEditorField from './../../CustomFields/TextEditorField/TextEditorField';
import DraggableAnswer from './DraggableAnswer';
import QuestionStatementWithBlanks from "./QuestionStatementWithBlanks";
import COLOR_SCHEME from "../../../constants/ThemeColor";

import './DragAndDropQuestionDrawer.less'
import BlueBorderedCard from '../../Card/BlueBorderedCard/BlueBorderedCard';
import { DEFAULT_FRONTEND_ID } from "../../../redux/interfaces/types";
import { baseCreateQuestionYupSchema, baseEditQuestionYupSchema, } from "../QuestionContainerItem/baseQuestionSchema";
import useQuestionDrawerType from "../../../hooks/QuizDrawer/useQuestionDrawerType";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import QUESTION_LABEL_CONSTANT from "../constant/QuestionLabelConstant";
import AttachmentQuestion from '../Attachment/AttachmentQuestion';

const QUESTION_STAGE = {
    STAGE_ONE: 1,
    STAGE_TWO: 2,
}
// * Find blank regex: /\[.*?\]/g
// * Dealing with nested brackets: (\[(?:\[??[^\[]*?\]))
// * Credits: https://stackoverflow.com/questions/2403122/regular-expression-to-extract-text-between-square-brackets
// * Extracting the whole sentence containing the blank RegEx:  /[^.?!]*(?<=[.?\s!])flung(?=[\s.?!])[^.?!]*[.?!]/g
// * Credits: https://stackoverflow.com/questions/26081820/regular-expression-to-extract-whole-sentences-with-matching-word/48533652

const blankAnswerSchema = Yup.object({
    content: Yup.string().required("câu trả lời không được để trống")
})

const blankSchema = Yup.object({
    answerKey: Yup.number().required("Blank must have an answer")
})


type FormValues = Pick<FillBlankDragAndDropQuestion, "answerList" | "blankList" | "description" | "point" | "note">


export function* generateNextKey(start?: number) {
    let i = start || 0;
    while (true) {
        yield i++;
    }
}

const counter = generateNextKey();
export const DEFAULT_TEXT_EDITOR_HTML = '';


const DrawerContent = ({ stage, isEditing }: { stage: number, isEditing: boolean }) => {
    const { values, setFieldValue } = useFormikContext<FillBlankDragAndDropQuestion>();

    const contentByStage = (stage: number) => {
        switch (stage) {
            case QUESTION_STAGE.STAGE_ONE:
                return <>
                    <Row>
                        <Typography.Paragraph>
                            Để định dạng bài kiểm tra của bạn, đóng ngoặc vuông để tạo các ô trống. Sau đó, thêm các lựa
                            chọn trả lời cho câu hỏi.
                            Số câu trả lời phải lớn hơn hoặc bằng số ô trống.

                            Ở bước 2, bạn hãy kéo các câu trả lời vào đúng vị trí và bầm lưu để thực hiện tạo câu hỏi.
                        </Typography.Paragraph>
                    </Row>
                    <FieldArray name="answerList"
                        render={
                            arrayHelpers => (
                                <>
                                    <Row align={"top"} justify={"end"}>
                                        <Col>
                                            <Space align={"baseline"}>
                                                <Button type="primary" onClick={() => {
                                                    // * The counter act as key and initial order of the answer in the list
                                                    const nextCounterValue = counter.next().value as number;
                                                    let newVar = {
                                                        content: "",
                                                        key: nextCounterValue.toString(),
                                                        order: nextCounterValue
                                                    };
                                                    console.log('new counter: ', newVar);
                                                    arrayHelpers.push(newVar);
                                                }}>
                                                    Thêm một lựa chọn trả lời
                                                </Button>
                                                <Col>
                                                    <FormItem name={"point"}
                                                        label={QUESTION_LABEL_CONSTANT.POINT}>
                                                        <Input type={"number"} name={"point"} />
                                                    </FormItem>
                                                </Col>
                                            </Space>
                                        </Col>
                                    </Row>
                                    <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                                        <Col span={24}>
                                            <Field
                                                name={"attachment"}
                                                placeholder={"Type article content"}
                                                component={AttachmentQuestion}
                                                label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <div className='dnd-answers-container'>
                                            <BlueBorderedCard className={'dnd-answers-card'} title={
                                                <Typography.Text style={{ color: COLOR_SCHEME.primary }}>
                                                    Các lựa chọn
                                                </Typography.Text>
                                            }
                                                extra={
                                                    <Space>
                                                        {
                                                            (values.answerList && values.answerList.length > 0) &&
                                                            <Button onClick={() => {
                                                                setFieldValue(`answerList`, [])
                                                            }
                                                            }>
                                                                Xóa hết các lựa chọn
                                                            </Button>

                                                        }
                                                    </Space>
                                                }>
                                                {
                                                    (values.answerList && values.answerList.length > 0) ? (
                                                        values.answerList.map((item, index) =>
                                                            <DraggableAnswer index={index} key={item.key}
                                                                answerKey={item.key}
                                                                move={arrayHelpers.move}>
                                                                <FormItem name={`answerList[${index}].content`}>
                                                                    <Input name={`answerList[${index}].content`}
                                                                        allowClear suffix={<Button
                                                                            onClick={() => arrayHelpers.remove(index)}
                                                                            danger type={"primary"}>
                                                                            {/* {item.order} Xóa */}
                                                                            Xóa
                                                                        </Button>} />
                                                                </FormItem>
                                                            </DraggableAnswer>
                                                        )) :
                                                        <Typography.Text type={"danger"}>Chưa có câu trả lời
                                                            nào</Typography.Text>
                                                }
                                            </BlueBorderedCard>
                                        </div>
                                    </Row>
                                </>
                            )
                        }
                    />
                    <Row justify={"center"} style={{ marginTop: "1.3em" }}>
                        <Space direction={"vertical"} align={"end"}>
                            <Field name={"description"} placeholder={"Type article content"}
                                component={TextEditorField} />
                        </Space>
                    </Row>
                    <Row justify={"center"}>
                        <BlueBorderedCard className={"question-statement-card"} title={
                            <Typography.Text style={{ color: COLOR_SCHEME.primary }}>
                                Xem trước câu hỏi
                            </Typography.Text>
                        }
                            extra={
                                <Space>
                                    <CancelButton onClick={() => {
                                        setFieldValue("description", DEFAULT_TEXT_EDITOR_HTML);
                                    }
                                    } />
                                </Space>
                            }
                            style={{
                                margin: "1em"
                            }}
                        >
                            <QuestionStatementWithBlanks
                                questionHTML={values.description || ""}
                                answerList={values.answerList}
                                isCreateOrUpdate={true}
                                mode={"edit"}
                            />
                        </BlueBorderedCard>

                    </Row>
                </>
            default:
                return <h1>????</h1>;
        }
    }


    return <>
        {contentByStage(stage)}
    </>
};

const DragAndDropQuestionDrawer = () => {

    const [stage, setStage] = useState(QUESTION_STAGE.STAGE_ONE);


    // * The drawer state
    const {
        visible,
        closeDrawer,
        openDrawer,
        isEditing,
        questionDataInDraft,
        doneHandler,
        isFetching,
        isError,
        isSuccess,
        questionIdInDraft,
        testName
    } = useQuestionDrawerType("FILL_IN_BLANK_DRAG_AND_DROP");


    const drawerRef = useRef<HTMLDivElement | null>(null);

    function closeConfirm(onOk: () => void, onCancel: () => void) {
        Modal.confirm({
            title: 'Xác nhận hủy bỏ câu hỏi',
            icon: <ExclamationCircleOutlined />,
            content: 'Chúng tôi nhận thấy rằng bạn đã soạn phần nào câu hỏi. Chọn xác nhận sẽ xóa hết tất cả.',
            okText: 'Xác nhận',
            cancelText: 'Tiếp tục soạn',
            onOk: onOk,
            onCancel: onCancel,
            getContainer: drawerRef.current!!
        });
    }

    if (questionDataInDraft && !isFIB_DND_Question(questionDataInDraft)) {
        AntdNotifier.error("Lỗi khi load câu hỏi, vui lòng refresh lại hoặc contact chúng tôi")
        return null;
    }


    return (
        <>

            <Formik enableReinitialize={true} initialValues={{
                id: questionDataInDraft?.id || DEFAULT_FRONTEND_ID,
                description: questionDataInDraft?.description || DEFAULT_TEXT_EDITOR_HTML,
                point: questionDataInDraft?.point || 0,
                note: questionDataInDraft?.note || "",
                answerList: questionDataInDraft?.answerList || [],
                blankList: questionDataInDraft?.blankList || [],
                attachment: questionDataInDraft?.attachment || '[]'
            }}
                validationSchema={Yup.object({
                    title: Yup.string().min(5, "Quá ngắn"),
                    answerList: Yup.array().of(blankAnswerSchema),
                    blankList: Yup.array().of(blankSchema),
                }).concat(isEditing ? baseEditQuestionYupSchema : baseCreateQuestionYupSchema)}
                onSubmit={(values, formikBag) => {
                    const valuesToSubmit: FillBlankDragAndDropQuestion = {
                        order: 0, type: "FILL_IN_BLANK_DRAG_AND_DROP",
                        ...values
                    }
                    doneHandler(valuesToSubmit);
                    // console.log("DND Question values:", valuesToSubmit);
                }}>
                {
                    ({ dirty, isValid, handleSubmit, setFieldValue, values, errors, resetForm }) => {
                        return <Form>
                            <Drawer
                                maskClosable={false}
                                closable={false}
                                title={<Space direction={"vertical"}>
                                    <Typography.Text>
                                        {`${testName || "Test Nhẹ kiến thức"}`}
                                    </Typography.Text>
                                    <Typography.Text strong>
                                        Kéo thả
                                    </Typography.Text>
                                </Space>}
                                placement="right"
                                size={"large"}
                                onClose={() => {
                                    // * Check if there was data and add for confirmation
                                    if (values.description && values.description !== DEFAULT_TEXT_EDITOR_HTML || stage !== QUESTION_STAGE.STAGE_ONE) {
                                        closeConfirm(() => {
                                            closeDrawer()
                                            resetForm()
                                            setStage(QUESTION_STAGE.STAGE_ONE);
                                        }, () => {
                                        });
                                    } else {
                                        closeDrawer();
                                    }
                                }
                                }
                                visible={visible}
                                extra={
                                    <Space>
                                        <Space>
                                            <CancelButton
                                                onClick={() => {
                                                    if (dirty) {
                                                        closeConfirm(
                                                            () => {
                                                                resetForm()
                                                                setStage(QUESTION_STAGE.STAGE_ONE);
                                                            },
                                                            () => {
                                                            }
                                                        );
                                                    } else {
                                                        closeDrawer();
                                                    }
                                                }
                                                }
                                            />
                                            <DoneButton disabled={!dirty}
                                                onClick={() => {
                                                    handleSubmit()
                                                }} />
                                        </Space>
                                    </Space>
                                }
                            >
                                <>
                                    <div ref={drawerRef} />
                                    <DrawerContent stage={stage} isEditing={isEditing} />
                                </>
                            </Drawer>
                        </Form>;

                    }
                }
            </Formik>
        </>
    );
};


export default DragAndDropQuestionDrawer;

// const QuestionDrawer = withFormik<QuestionDrawerProps<FillBlankDragAndDropQuestion>, FormValues>({
//     mapPropsToValues: (props) => {
//         const {initQuestionState} = props;
//         return {
//             id: initQuestionState?.id || DEFAULT_FRONTEND_ID,
//             description: initQuestionState?.description || DEFAULT_TEXT_EDITOR_HTML,
//             point: initQuestionState?.point || 0,
//             note: initQuestionState?.note || "",
//             answerList: initQuestionState?.answerList || [],
//             blankList: initQuestionState?.blankList || []
//         }
//     },
//     validationSchema: (props: any) => Yup.object({
//         title: Yup.string().min(5, "Quá ngắn"),
//         answerList: Yup.array().of(blankAnswerSchema),
//         blankList: Yup.array().of(blankSchema),
//     }).concat(props.mode === "edit" ? baseEditQuestionYupSchema : baseCreateQuestionYupSchema)
//     ,
//     handleSubmit: (values, formikBag) => {
//         const valuesToSubmit: FillBlankDragAndDropQuestion = {
//             id: DEFAULT_FRONTEND_ID, order: 0, type: "FILL_IN_BLANK_DRAG_AND_DROP",
//             ...values
//         }
//         formikBag.props.onDoneSubmit?.(valuesToSubmit);
//         console.log("DND Question values:", valuesToSubmit);
//     }
// })(DragAndDropQuestionDrawer);
//
// export default QuestionDrawer;


// * Old option list
