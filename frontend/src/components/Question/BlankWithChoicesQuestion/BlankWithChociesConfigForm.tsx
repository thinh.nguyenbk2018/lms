import React from "react";
import {Card, Form, Input, Row, Select, Typography} from "antd";
import {CheckboxChangeEvent} from "antd/lib/checkbox";
import TextEditor from "../../TextEditor/TextEditor";

interface BlankProps {
    sentence: string,
    blank: string,
    index: number
}


const {Option} = Select;

const stripContentFromBlankText = (blankText: string) => {
    if (!blankText) {
        return "";
    }
    return blankText.substring(1, blankText.length - 1);
}

export const parseAnswersInBlank = (question: string) => {
    return question.substring(1, question.length - 1).split(";");
}

// * Index is the number indicating the order of the blank in the text with its context sentence
// * Intend: Passing the sentence containing the blank for context --> Too hard for me :)
// * Chosen Alternative: Passing in the whole question text :)
const BlankWithChociesConfigForm = ({sentence, blank, index}: BlankProps) => {
    const indexOfBlank = sentence.indexOf(blank);

    const answerOptions = parseAnswersInBlank(blank);

    const stringBeforeBlank = sentence.substring(0, indexOfBlank);
    const stringAfterBlank = sentence.substring(stringBeforeBlank.length + blank.length);

    const onCorrectAnswerChange = (val: any) => {
        console.log("Change correct answer to: ", val);
    }

    const onCaseRequiredChange: ((e: CheckboxChangeEvent) => void) | undefined = (e) => {
        console.log("Checked box value is: ", e.target.value);
    }

    const string_to_display = stringBeforeBlank.concat(`<b>[BLANK-${index}]</b>`, stringAfterBlank)
    return <Card style={{minWidth: `500px`}}>
        <Row style={{marginBottom: `1em`, borderBottom: `1px thin`}}>
            {
                <TextEditor onChange={() => {
                }} value={string_to_display} readOnly={true} useBubbleTheme/>
            }
        </Row>
        <Row>
            <Form>
                <Form.Item label={
                    <Typography.Text strong>
                        {`[BLANK-${index}]`}
                    </Typography.Text>
                }>
                    <Input value={stripContentFromBlankText(blank)}/>
                </Form.Item>
                <Form.Item label={
                    <Typography.Text strong>
                        Đáp án đúng
                    </Typography.Text>
                }>
                    <Select
                        placeholder="Hãy chọn đáp án đúng cho ô trống này"
                        onChange={onCorrectAnswerChange}
                        allowClear
                    >
                        {
                            answerOptions.map(item =>
                                <Option value={item} key={item}>
                                    <Typography.Text>
                                        {item}
                                    </Typography.Text>
                                </Option>
                            )
                        }
                    </Select>
                </Form.Item>
            </Form>
        </Row>
    </Card>;
}

export default BlankWithChociesConfigForm;
