import { Card, Checkbox, Col, Divider, Row, Spin, Typography } from "antd";
import { useGetRoleQuery } from "../../../../../redux/features/Permission/RolesAPI";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import LimitOfPermission from "../../../Permission/LimitOfPermission/LimitOfPermission";
import CustomEmpty from "../../../../Util/CustomEmpty";
import "./RoleDetails.less";

const RoleDetails = ({ roleId }: { roleId: number }) => {
  let { data: getRoleResponse, isLoading } = useGetRoleQuery({ roleId });

  const disableCheckbox = () => {
    AntdNotifier.warn("Bạn không đươc phép chỉnh sửa ở đây", "Cảnh báo");
    return false;
  }

  return (
    <Row justify="center" style={{ padding: 10 }} gutter={10}>
      {isLoading ? <Spin /> : getRoleResponse?.role.permissions.map(permission => (
        <Col lg={8} md={12} sm={24} style={{ marginBottom: 10 }}>
          <Card title={`${permission.title}`}
            size="small"
            className="permission-of-role"
            headStyle={{ borderColor: "#1890ff" }}>
            <Typography.Text><strong>Mô tả:</strong> {permission.description}</Typography.Text>
            <Divider style={{ margin: 5 }} />
            <LimitOfPermission permission={permission}
              onEditLimit={(key, value) => disableCheckbox()} />
          </Card>
        </Col>
      ))}
      {getRoleResponse?.role.permissions.length === 0 && <CustomEmpty />}
    </Row>
  );
}

export default RoleDetails;