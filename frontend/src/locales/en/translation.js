import {base_keys} from "../base_keys";

export const TRANSLATIONS_EN = {
    // Date time format
    [base_keys.date_format_one]: "{{-date, MM/DD/YYYY}}",
    // Language Choice
    [base_keys.language_choice.english]: "English",
    [base_keys.language_choice.vietnamese]: "Vietnamese",
    // form
    [base_keys.form.email]: "Email",
    [base_keys.form.password_prompt]: "Enter your email",
    [base_keys.form.email_valid_prompt]: "Enter a valid email",
    [base_keys.form.email_required_prompt]: "Email is required",

    [base_keys.form.password]: "Password",
    [base_keys.form.password_prompt]: "Enter your password",
    [base_keys.form.password_required_prompt]: "Password is required",
    [base_keys.form.password_min_8_requirement]: "Password length must be greater than 8",
    [base_keys.form.username]: "Username",
    [base_keys.form.username_prompt]: "Enter your username",
    [base_keys.form.username_required_prompt]: "Username is required",
    [base_keys.form.username_min_8_requirement]: "Username length must be greater than 8",

    [base_keys.form.confirm_password]: "Confirm Password",
    [base_keys.form.confirm_password_correct_prompt]: "Password must match",
    [base_keys.form.full_name]: "Full Name",
    [base_keys.form.full_name_prompt]: "Enter your fullname",
    [base_keys.form.user_name]: "Username",
    [base_keys.form.user_name_prompt]: "Enter your username",
    [base_keys.form.address]: "Address",
    [base_keys.form.address_prompt]: "Your current address",
    [base_keys.form.phone_number]: "Phone Number",
    [base_keys.form.phone_number_prompt]: "Your phone number",
    [base_keys.form.date_of_birth]: "Confirm Password",
    [base_keys.form.data_of_birth_prompt]: "Password must match",


    [base_keys.test.test_page1]:"Page 1",
    [base_keys.test.test_page2]:"Page 2",
    [base_keys.test.navigate_page1]:"Navigate to Page 1",
    [base_keys.test.navigate_page2]:"Navigate to Page 2",
};