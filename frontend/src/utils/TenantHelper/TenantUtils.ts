

export const tenantUtils = {
    getTenantIdFromHostname: () => {
        const hostName = window.location.hostname;
        return hostName.split(".").length === 3 ? hostName.split(".")[0] : null;
    }
}
