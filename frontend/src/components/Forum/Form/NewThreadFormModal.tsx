import { Modal } from "antd";
import { Formik } from "formik";
import * as Yup from "yup";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { FORM_DEFAULT_MESSAGES } from "@utils/FormUtils/formUtils";
import {
	classDiscussionActions,
	classDiscussionSelectors,
} from "../../../redux/features/Class/ClassDiscussion/ClassDiscussionSlice";
import {
	Post,
	useCreatePostMutation,
	useUpdatePostMutation,
} from "../../../redux/features/Class/ClassDiscussion/ClassDiscussionAPI";
import { values } from "lodash";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import NewThreadForm from "./NewThreadForm";
import { useParams } from "react-router-dom";

export const threadSchema = Yup.object({
	title: Yup.string().required("Tiêu đề là thông tin bắt buộc"),
	content: Yup.string().required("Nội dung là thông tin bắt buộc"),
});

export const initalThreadValues: Partial<Post> = {
	title: "",
	content: "",
};
const NewThreadFormModal = () => {
	const { classId } = useParams();

	const isCreatingPost = useAppSelector(
		classDiscussionSelectors.selectIsAddingNewPost
	);
	const isEditingPost = useAppSelector(
		classDiscussionSelectors.selectIsEditingPost
	);
	const dispatch = useAppDispatch();

	const [createPost] = useCreatePostMutation();
	const [updatePost] = useUpdatePostMutation();

	const postInEdit = useAppSelector(classDiscussionSelectors.selectPostInEdit);
	if (isEditingPost && !postInEdit) {
		AntdNotifier.error("Đã có lỗi xaỷ ra. Liên hệ dev team");
		return <></>;
	} else {
		return (
			<>
				<Formik
					validateOnChange={false}
					enableReinitialize={true}
					initialValues={isEditingPost ? postInEdit ?? {} : initalThreadValues}
					validationSchema={threadSchema}
					onSubmit={(values) => {
						if (isEditingPost) {
							if (!postInEdit?.id) {
								AntdNotifier.error("Không tìm thấy post để chỉnh sửa");
								return;
							}
							AntdNotifier.handlePromise(
								updatePost({
									classId: Number(classId),
									postId: postInEdit.id,
									postInfo: values,
								}).unwrap(),
								"Chỉnh sửa bài đăng",
								() => {
									dispatch(classDiscussionActions.closeModal());
								},
								() => {
									dispatch(classDiscussionActions.closeModal());
								}
							);
						} else if (isCreatingPost) {
							AntdNotifier.handlePromise(
								createPost({ classId: Number(classId), postInfo: values }).unwrap(),
								"Đăng bài viết",
								() => {
									dispatch(classDiscussionActions.closeModal());
								},
								() => {
									dispatch(classDiscussionActions.closeModal());
								}
							);
						}
					}}
				>
					{({ values, handleSubmit, errors, isValid, dirty }) => (
						<Modal
							maskClosable={false}
							visible={isCreatingPost || isEditingPost}
							wrapClassName={'content-page-modal-wrapper'}
							onOk={() => {
								handleSubmit();
							}}
							onCancel={() => {
								dispatch(classDiscussionActions.closeModal());
							}}
							okText={isEditingPost ? 'Cập nhật' : 'Đăng bài'}
							cancelText={'Hủy'}
						>
							<NewThreadForm />
						</Modal>
					)}
				</Formik>
			</>
		);
	}
};

export default NewThreadFormModal;
