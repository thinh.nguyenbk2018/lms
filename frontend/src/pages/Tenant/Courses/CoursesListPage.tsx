import './CourseListPage.less';

import {Button, Tooltip, Typography} from 'antd';
import Space from 'antd/es/space';
import React from 'react';
import {Link, useNavigate} from 'react-router-dom';

import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import CustomTable from '../../../components/CustomTable/CustomTable';
import {extractPaginationInformation} from '../../../components/CustomTable/TableUtils';
import {addIconSvg, testSvg, viewIconSvg} from '../../../components/Icons/SvgIcons';
import TableSearch from '../../../components/Search/TableSearch';
import COLOR_SCHEME from '../../../constants/ThemeColor';
import {useAppSelector} from '../../../hooks/Redux/hooks';
import {ROUTING_CONSTANTS} from '../../../navigation/ROUTING_CONSTANTS';
import {useDeleteCourseMutation, useGetCourseListPaginatedQuery} from '../../../redux/features/Course/CourseAPI';
import {changeCriteria as changeCourseSearchCriteria, courseSelector} from '../../../redux/features/Course/CourseSlice';
import AntdNotifier from '../../../utils/AntdAnnouncer/AntdNotifier';
import {replacePathParams} from '../../../utils/PathPatcherHepler/PathPatcher';
import {DATE_FORMAT} from '../../../utils/TimeStampHelper/TimeStampHelper';
import LoadingPage from '../../UtilPages/LoadingPage';
import moment from "moment";
import {DeleteOutlined} from "@ant-design/icons";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import PermissionRenderer, {useHasPermission} from '@components/Util/PermissionRender/PermissionRender';
import {PermissionEnum} from '@typescript/interfaces/generated/PermissionMap';
import useCourseSidebar from "@hooks/Routing/useCourseSidebar";

const CoursesListPage = () => {
    const navigate = useNavigate();
    const {

    } = useCourseSidebar();

    const [deleteCourse] = useDeleteCourseMutation();
    const courseSearchCriteria = useAppSelector(courseSelector.selectSearchCriteria);
    const { data: paginatedResponse, isSuccess, isFetching } = useGetCourseListPaginatedQuery(courseSearchCriteria);

    const hasPermissionWithCourse = useHasPermission([PermissionEnum.CREATE_COURSE, PermissionEnum.VIEW_DETAIL_COURSE, PermissionEnum.DELETE_COURSE]);

    const deleteCourseHelper = (id: number) => {
        deleteCourse(id)
            .unwrap()
            .then(_res => {
                AntdNotifier.success('Xóa khóa học thành công', '', 1);
            })
            .catch(err => {
                if ("message" in err.data) {
                    return AntdNotifier.error(err.data.message);
                }
                AntdNotifier.error('Xóa khóa học thất bại', 'Thông báo', 1);
            })
    }

    const columns = [
        {
            title: "Tên",
            dataIndex: "name",
            key: "title",
            width: "30%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
            },
            sortDirections: ['descend', 'ascend'],
            sortOrder: courseSearchCriteria.sort.filter(x => x.field === 'name').length > 0 && courseSearchCriteria.sort.filter(x => x.field === 'name')[0].order || null
        },
        {
            title: "Mã khóa",
            dataIndex: "code",
            key: "code",
            width: "20%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
                // multiple: 2
            },
            sortDirections: ['descend', 'ascend'],
            sortOrder: courseSearchCriteria.sort.filter(x => x.field === 'code').length > 0 && courseSearchCriteria.sort.filter(x => x.field === 'code')[0].order || null
        },
        {
            title: "Trình độ",
            dataIndex: "level",
            key: "level",
            width: "20%",
        },
        {
            title: "Thời gian tạo",
            dataIndex: "createdAt",
            key: "createdAt",
            width: "20%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
                // multiple: 3
            },
            sortDirections: ['descend', 'ascend'],
            sortOrder: courseSearchCriteria.sort.filter(x => x.field === 'createdAt').length > 0 && courseSearchCriteria.sort.filter(x => x.field === 'createdAt')[0].order || null,
            render: (startedAt: Date) => startedAt ? moment(startedAt).format(DATE_FORMAT) : ""
        },
        {
            title: "",
            key: "action",
            render: (record: any) => (
                <>
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Space>
                            <PermissionRenderer permissions={[PermissionEnum.UPDATE_COURSE]}>
                                <Tooltip title={"Chỉnh sửa thông tin khóa học"}>
                                    <Typography.Link className={'flex'} style={{ color: COLOR_SCHEME.primary }}
                                        onClick={() => navigate(`${replacePathParams(ROUTING_CONSTANTS.EDIT_COURSE, [":courseId", record.id])}`)}>
                                        {viewIconSvg}
                                    </Typography.Link>
                                </Tooltip>
                            </PermissionRenderer>
                            <PermissionRenderer permissions={[PermissionEnum.VIEW_DETAIL_COURSE]}>
                                <Tooltip title={"Vào trang khóa học"}>
                                    <Typography.Link className={'flex'}
                                        style={{ marginRight: '1rem', color: COLOR_SCHEME.primary }}
                                        onClick={() => {
                                            navigate(replacePathParams(ROUTING_CONSTANTS.COURSE_DASHBOARD, [":courseId", record.id]))
                                        }}>
                                        {
                                            testSvg
                                        }
                                    </Typography.Link>
                                </Tooltip>
                            </PermissionRenderer>
                            <PermissionRenderer permissions={[PermissionEnum.DELETE_COURSE]}>
                                <Tooltip title={"Xóa khóa học"}>
                                    <DeleteOutlined
                                        className={'delete-item-btn'}
                                        onClick={() => {
                                            confirmationModal('khóa học', record.name, () => {
                                                deleteCourseHelper(record.id);
                                            });
                                        }}
                                    />
                                </Tooltip>
                            </PermissionRenderer>
                        </Space>
                    </div>
                </>
            ),
        },
    ];

    const renderFunction = () => {
        let componentToRender: JSX.Element = <></>;
        if (isFetching) {
            componentToRender = <LoadingPage />
        }
        if (isSuccess) {
            componentToRender = <CustomTable
                data={paginatedResponse?.listData || []}
                columns={hasPermissionWithCourse ? columns : columns.filter(col => col.key !== "action")}
                metaData={extractPaginationInformation(paginatedResponse)}
                searchCriteria={courseSearchCriteria}
                changeSearchActionCreator={changeCourseSearchCriteria}
            />
        }
        return <>
            <ContentHeader
                title={"Khóa học"}
                action={
                    <div className={'search-container'}>
                        <TableSearch
                            searchFields={[{ title: 'Từ khóa', key: 'key' }]}
                            defaultFilters={[]}
                            searchCriteria={courseSearchCriteria}
                            changeCriteria={changeCourseSearchCriteria}
                        />
                        <PermissionRenderer permissions={[PermissionEnum.CREATE_COURSE]}>
                            <Link to={ROUTING_CONSTANTS.NEW_COURSE}>
                                <Button type="primary" className={'button-with-icon'}>
                                    {addIconSvg}
                                    <span>Thêm</span>
                                </Button>
                            </Link>
                        </PermissionRenderer>
                    </div>
                }
            />
            {componentToRender}
        </>

    }


    return (
        <>
            {renderFunction()}
        </>
    );
};

export default CoursesListPage;
