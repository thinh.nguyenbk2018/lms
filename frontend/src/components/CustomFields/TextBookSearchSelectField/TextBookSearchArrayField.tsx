import {MinusCircleOutlined} from "@ant-design/icons";
import {Button, Form, Space, Typography} from "antd";
import {FieldArray, FieldProps} from "formik";
import {CSSProperties} from "react";
import {DEFAULT_FRONTEND_ID} from "@redux/interfaces/types";
import TextbookSearchSelect from "../../Courses/Forms/TextBookSearchSelect/TextBookSearchSelect";

type TextBookSearchSelectFieldProps = FieldProps<any> & {
    scopeIdToSearch: number;
    scopeToSearch: string;
    label?: string;
    style?: CSSProperties;
    required?: boolean;
    onSelect?: () => void;
    showErrorMessage?: boolean;
};
const TextBookSearchArrayField = ({
                                      field,
                                      scopeIdToSearch,
                                      scopeToSearch,
                                      form: {
                                          touched,
                                          errors,
                                          setFieldValue,
                                          values,
                                      },
                                      ...otherProps
                                  }: TextBookSearchSelectFieldProps) => {
    const {name, value, onChange, onBlur} = field;
    const errorMsg = touched[name] && errors[name];
    return (
        <>
            <FieldArray
                name={name}
                render={(arrayHelper) => {
                    return (
                        <>
                            <Form.Item label={otherProps.label}>
                                <Space>
                                    {values &&
                                        values[`${name}`].map((item: number, index: number) => {
                                            return (
                                                <Space>
                                                    <TextbookSearchSelect
                                                        onSelect={(textBookId) =>
                                                            setFieldValue(`${name}[${index}]`, textBookId)
                                                        }
                                                        textbooks={[]}
                                                    />
                                                    <MinusCircleOutlined
                                                        onClick={() => {
                                                            arrayHelper.remove(index);
                                                        }}
                                                    />
                                                </Space>
                                            );
                                        })}
                                    <Typography.Text type={"danger"}>
                                        <>
                                            {errorMsg}
                                        </>
                                    </Typography.Text>
                                </Space>
                            </Form.Item>
                            <Button
                                onClick={() => {
                                    arrayHelper.push(DEFAULT_FRONTEND_ID);
                                }}
                                danger
                            >
                                Thêm giáo trình
                            </Button>
                        </>
                    );
                }}
            />
        </>
    );
};
export default TextBookSearchArrayField;
