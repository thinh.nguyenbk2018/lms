import { apiSlice } from "../CentralAPI";
import { BaseResponse, PaginatedResponse } from "../../interfaces/types";
import { SearchCriteria } from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import { transformSearchQuery } from "@redux/features/APIUtils/TransformParameter";

const breadcrumbApiInvalidatesTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["Breadcrumbs"],
});

export interface GetBreadcrumbResponse extends BaseResponse {
    title: string;
}

export interface GetBreadcrumbRequest {
    type: string;
    id: number;
    scope: string;
}

const breadcrumbAPI = breadcrumbApiInvalidatesTags.injectEndpoints({
    endpoints: (build) => ({
        getBreadcrumbTitle: build.query<
            GetBreadcrumbResponse,
            GetBreadcrumbRequest
        >({
            query: (body) => ({
                url: `/breadcrumb`,
                method: "POST",
                body,
            }),
            providesTags: ["Breadcrumbs"],
        }),
    }),
    overrideExisting: false,
});

export const { useGetBreadcrumbTitleQuery } = breadcrumbAPI;
