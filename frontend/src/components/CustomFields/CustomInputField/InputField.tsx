import React from 'react';
import {FieldProps} from "formik";
import TextEditor, {TextEditorProps} from "../../TextEditor/TextEditor";
import {CSSProperties} from 'react';
import {FormItem, Input} from 'formik-antd';
import {InputProps, Typography} from "antd";

// * FieldProps = {value,name,onBlur,onChange}
// * {} : Custom props this custom field will take
type CustomFieldProps =
    FieldProps<any>
    & InputProps
    & { label?: string, style?: CSSProperties, required?: boolean };

const InputField = ({
                        field,
                        form: {
                            touched,
                            errors,
                            setFieldValue,
                            setFieldError,
                            handleBlur,
                            handleChange,
                            setTouched,
                            setFieldTouched
                        },
                        ...otherProps
                    }: CustomFieldProps) => {
    // * Below is what you can get from the field props
    const {name, value, onChange, onBlur} = field;

    // * Take error from the Validate prop of formik (from yup validation schema)
    const errorMsg = touched[name] && errors[name];
    // * Need to wrap inside Form.Item is because TextEditor doesn't accept the stuffs such as handleChange,handleBlue,onBlur,...
    return (
        <>
            <Input name={field.name} {...otherProps} onBlur={() => {
                setFieldValue(name, value);
            }}/>
            <Typography.Text>
            </Typography.Text>
        </>
    );
};

export default InputField;