"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
require("./CourseListPage.less");
var antd_1 = require("antd");
var space_1 = require("antd/es/space");
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var ContentHeader_1 = require("../../../components/ContentHeader/ContentHeader");
var CustomTable_1 = require("../../../components/CustomTable/CustomTable");
var TableUtils_1 = require("../../../components/CustomTable/TableUtils");
var SvgIcons_1 = require("../../../components/Icons/SvgIcons");
var TableSearch_1 = require("../../../components/Search/TableSearch");
var ThemeColor_1 = require("../../../constants/ThemeColor");
var hooks_1 = require("../../../hooks/Redux/hooks");
var ROUTING_CONSTANTS_1 = require("../../../navigation/ROUTING_CONSTANTS");
var CourseAPI_1 = require("../../../redux/features/Course/CourseAPI");
var CourseSlice_1 = require("../../../redux/features/Course/CourseSlice");
var AntdNotifier_1 = require("../../../utils/AntdAnnouncer/AntdNotifier");
var PathPatcher_1 = require("../../../utils/PathPatcherHepler/PathPatcher");
var TimeStampHelper_1 = require("../../../utils/TimeStampHelper/TimeStampHelper");
var LoadingPage_1 = require("../../UtilPages/LoadingPage");
var CoursesListPage = function () {
    var navigate = react_router_dom_1.useNavigate();
    var deleteCourse = CourseAPI_1.useDeleteCourseMutation()[0];
    var courseSearchCriteria = hooks_1.useAppSelector(CourseSlice_1.courseSelector.selectSearchCriteria);
    var _a = CourseAPI_1.useGetCourseListPaginatedQuery(courseSearchCriteria), paginatedResponse = _a.data, isSuccess = _a.isSuccess, isFetching = _a.isFetching;
    var deleteCourseHelper = function (id) {
        deleteCourse(id)
            .unwrap()
            .then(function (_res) {
            AntdNotifier_1["default"].success('Xóa chương trình thành công', 'Thông báo', 1);
        })["catch"](function (_err) {
            AntdNotifier_1["default"].error('Xóa chương trình thất bại', 'Thông báo', 1);
        });
    };
    var columns = [
        {
            title: "Tên",
            dataIndex: "name",
            key: "title",
            width: "50%",
            sorter: {
                multiple: 1
            },
            sortDirections: ['descend', 'ascend'],
            sortOrder: courseSearchCriteria.sort.filter(function (x) { return x.field === 'name'; }).length > 0 && courseSearchCriteria.sort.filter(function (x) { return x.field === 'name'; })[0].order || null
        },
        {
            title: "Mã khóa",
            dataIndex: "code",
            key: "code",
            width: "20%",
            sorter: {
                multiple: 2
            },
            sortDirections: ['descend', 'ascend'],
            sortOrder: courseSearchCriteria.sort.filter(function (x) { return x.field === 'code'; }).length > 0 && courseSearchCriteria.sort.filter(function (x) { return x.field === 'code'; })[0].order || null
        },
        {
            title: "Thời gian tạo",
            dataIndex: "createdAt",
            key: "createdAt",
            width: "20%",
            sorter: {
                multiple: 3
            },
            sortDirections: ['descend', 'ascend'],
            sortOrder: courseSearchCriteria.sort.filter(function (x) { return x.field === 'createdAt'; }).length > 0 && courseSearchCriteria.sort.filter(function (x) { return x.field === 'createdAt'; })[0].order || null
        },
        {
            title: "",
            key: "action",
            render: function (record) { return (react_1["default"].createElement(react_1["default"].Fragment, null,
                react_1["default"].createElement("div", { style: { display: 'flex', justifyContent: 'center', alignItems: 'center' } },
                    react_1["default"].createElement(space_1["default"], null,
                        react_1["default"].createElement(antd_1.Tooltip, { title: "Chỉnh sửa thông tin chung của lớp" },
                            react_1["default"].createElement("span", { style: { marginRight: '1rem', color: ThemeColor_1["default"].primary }, onClick: function () { return navigate("" + PathPatcher_1.replacePathParams(ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.EDIT_COURSE, [":courseId", record.id])); } }, SvgIcons_1.viewIconSvg)),
                        react_1["default"].createElement(antd_1.Tooltip, { title: "Vào trang khóa học" },
                            react_1["default"].createElement(antd_1.Typography.Link, { onClick: function () {
                                    navigate(PathPatcher_1.replacePathParams(ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.COURSE_DASHBOARD, [":courseId", record.id]));
                                } }, SvgIcons_1.testSvg)),
                        react_1["default"].createElement(antd_1.Tooltip, { title: "Xóa khóa học" },
                            react_1["default"].createElement(antd_1.Button, { type: "link", className: 'delete-item-btn', onClick: function () { return deleteCourseHelper(record.id); } }, SvgIcons_1.deleteIconSvg)))))); }
        },
    ];
    var renderFunction = function () {
        var componentToRender = react_1["default"].createElement(react_1["default"].Fragment, null);
        if (isFetching) {
            componentToRender = react_1["default"].createElement(LoadingPage_1["default"], null);
        }
        if (isSuccess) {
            var transformedData = (paginatedResponse === null || paginatedResponse === void 0 ? void 0 : paginatedResponse.listData.map(function (item) {
                return __assign(__assign({}, item), { createdAt: TimeStampHelper_1["default"].formatDateTimeInTable(item.createdAt) });
            })) || [];
            componentToRender = react_1["default"].createElement(CustomTable_1["default"], { data: transformedData, columns: columns, metaData: TableUtils_1.extractPaginationInformation(paginatedResponse), searchCriteria: courseSearchCriteria, changeSearchActionCreator: CourseSlice_1.changeCriteria });
        }
        return react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement(ContentHeader_1["default"], { title: "Danh sách khóa học", action: react_1["default"].createElement("div", { className: 'search-container' },
                    react_1["default"].createElement(TableSearch_1["default"], { searchFields: [{ title: 'Từ khóa', key: 'key' }], defaultFilters: {}, searchCriteria: courseSearchCriteria, changeCriteria: CourseSlice_1.changeCriteria }),
                    react_1["default"].createElement(react_router_dom_1.Link, { to: ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.NEW_COURSE },
                        react_1["default"].createElement(antd_1.Button, { type: "primary", className: 'add-course-btn' },
                            SvgIcons_1.addIconSvg,
                            react_1["default"].createElement("span", null, "Th\u00EAm")))) }),
            componentToRender);
    };
    return (react_1["default"].createElement(react_1["default"].Fragment, null, renderFunction()));
};
exports["default"] = CoursesListPage;
