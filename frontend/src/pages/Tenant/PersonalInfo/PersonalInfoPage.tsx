import ContentHeader from "@components/ContentHeader/ContentHeader";
import {useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {useChangePasswordMutation, useGetCurrentUserInfoQuery} from "@redux/features/User/UserSlice";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {
    Avatar,
    Row,
    Space,
    Spin,
    Typography,
    Image,
    Descriptions,
    List,
    Button,
    Divider,
    Modal,
    Form,
    Input
} from "antd";
import {useState} from "react";
import {useParams} from "react-router";

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 9},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 15},
    },
};

const PersonalInfoPage = () => {
    const {userIdPath} = useParams();
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);
    const {
        data: getUserInfoResponse,
        isLoading
    } = useGetCurrentUserInfoQuery(userIdPath ? Number(userIdPath) : currentUser!.id);
    const [showChangePasswordModal, setShowChangePasswordModal] = useState<boolean>(false);
    const [changePassword, {data: changePasswordResponse, isLoading: isChangingPassword}] = useChangePasswordMutation();
    const [form] = Form.useForm();

    const changePasswordHandle = () => {
        form
            .validateFields()
            .then(values => {
                form.resetFields();
                changePassword({
                    password: values['password'],
                    newPassword: values['newPassword'],
                    confirmPassword: values['confirmPassword']
                })
                    .unwrap()
                    .then(res => {
                        setShowChangePasswordModal(false);
                        AntdNotifier.success("Đổi mật khẩu thành công");
                    })
                    .catch((error: any) => {
                        if (error?.data?.message) {
                            AntdNotifier.error(error.data.message);
                            return;
                        }
                        AntdNotifier.error("Có lỗi xảy ra trong quá trình cập nhật dữ liệu, vui lòng thử lại");
                    })
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
    }

    return <>
        <ContentHeader
            title="Thông tin cá nhân"
        />
        <Spin spinning={isLoading}>
            <Row justify="center">
                <Space direction="vertical" align="center">
                    <Avatar size={150} src={<Image src={getUserInfoResponse?.userInfo.avatar}/>}/>
                    <Typography.Title
                        level={3}>{getUserInfoResponse?.userInfo?.lastName} {getUserInfoResponse?.userInfo.firstName}</Typography.Title>
                    <Typography.Text>NV_{getUserInfoResponse?.userInfo.id}</Typography.Text>
                </Space>
            </Row>
            <Divider orientation="left"><Typography.Title level={4}>Thông tin chung</Typography.Title></Divider>
            <Descriptions column={2}>
                <Descriptions.Item
                    label="Họ và tên"
                >
                    {getUserInfoResponse?.userInfo?.lastName + " " + getUserInfoResponse?.userInfo?.firstName}
                </Descriptions.Item>
                <Descriptions.Item
                    label="Email"
                >
                    {getUserInfoResponse?.userInfo?.email}
                </Descriptions.Item>
                <Descriptions.Item
                    label="Số điện thoại"
                >
                    {getUserInfoResponse?.userInfo?.phone}
                </Descriptions.Item>
                <Descriptions.Item
                    label="Địa chỉ"
                >
                    {getUserInfoResponse?.userInfo?.address}
                </Descriptions.Item>
            </Descriptions>
            <Divider orientation="left"><Typography.Title level={4}>Quản lý tài khoản</Typography.Title></Divider>
            <List bordered>
                <List.Item actions={[<Button onClick={() => setShowChangePasswordModal(true)}>Đổi mật khẩu</Button>]}>
                    Bảo mật
                </List.Item>
            </List>
            <Modal
                visible={showChangePasswordModal}
                title="Đổi mật khẩu"
                okButtonProps={{loading: isChangingPassword}}
                onOk={changePasswordHandle}
                onCancel={() => setShowChangePasswordModal(false)}
                okText={'Đổi mật khẩu'}
                cancelText={'Hủy'}
            >
                <Form {...formItemLayout} form={form}>
                    <Form.Item
                        label="Mật khẩu hiện tại"
                        rules={[{required: true, message: "Đây là trường bắt buộc"}]}
                        name="password"
                    >
                        <Input.Password visibilityToggle placeholder=" Nhập mật khẩu hiện tại..."/>
                    </Form.Item>
                    <Form.Item
                        label="Mật khẩu mới"
                        name="newPassword"
                        rules={[{required: true, message: "Đây là trường bắt buộc"}]}
                    >
                        <Input.Password visibilityToggle placeholder="Nhập mật khẩu mới..."/>
                    </Form.Item>
                    <Form.Item
                        label="Nhập lại mật khẩu mới"
                        name="confirmPassword"
                        rules={[
                            {required: true, message: "Đây là trường bắt buộc"},
                            ({getFieldValue}) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('newPassword') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('Không khớp vơi mật khẩu vừa nhập ở trên.'));
                                },
                            })
                        ]}
                    >
                        <Input.Password visibilityToggle placeholder="Nhập mật khẩu mới..."/>
                    </Form.Item>
                </Form>
            </Modal>
        </Spin>
    </>
}

export default PersonalInfoPage;