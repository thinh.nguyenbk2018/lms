import React from "react";
import {DragAndDropAnswer, FillBlankDragAndDropQuestion,} from "../../../redux/features/Quiz/CreateQuizSlice";
import {Card, Divider, Input, Row, Space} from "antd";
import './DragAndDropQuestionDisplay.less';
import QuestionStatementWithBlanks from "@components/Question/DragAndDropQuestion/QuestionStatementWithBlanks";
import {QuestionViewAndTakingTestFormProps} from "@components/Question/interface/question.interface";
import {FormikProps, withFormik} from "formik";
import {QuestionAnswer} from "@redux/features/Quiz/examAPI";
import {DEFAULT_FRONTEND_ID} from "@redux/interfaces/types";
import {DEFAULT_TEXT_EDITOR_HTML} from "@components/Question/DragAndDropQuestion/DragAndDropQuestionDrawer";
import {Attachment} from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";
import {values} from "lodash";

const DragAndDropQuestionDisplayWrapper = (props: DragAndDropViewHocProps & FormikProps<QuestionAnswer>) => {
    // * Question info to display the requirements
    const {description, blankList, answerList, values} = props;
    const answerListDefined = answerList?.filter(
        (item) => item
    ) as DragAndDropAnswer[];
    if (!description || !answerList || !answerListDefined) {
        return null;
    }
    console.log('outer values: ', values);

    return (
        <>

            <Card>
                {
                    JSON.parse(props?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center"
                                                                                                     key={index}>
                        <AttachmentRender attachment={a}/>
                    </Row>)
                }
                <Divider/>
                <div className={"dnd-question-content-container"}>
                    <div className={"answer-list-container"}>
                        <Space>
                            {
                                answerListDefined.map((item, index) =>
                                    <Input defaultValue={item.content} disabled={true}
                                           className={'fill-in-blank-drag-and-drop-answer-pool'}/>)

                            }
                        </Space>
                    </div>
                    <Divider>
                    </Divider>
                    <QuestionStatementWithBlanks
                        questionHTML={description}
                        answerList={answerListDefined}
                        mode={"view"}
                    />
                </div>
            </Card>
        </>
    );
};

type DragAndDropViewHocProps =
    Omit<QuestionViewAndTakingTestFormProps<FillBlankDragAndDropQuestion>, "mode">;

const DragAndDropQuestionDisplay = withFormik<DragAndDropViewHocProps,
    QuestionAnswer>({
    enableReinitialize: true,
    mapPropsToValues: (props) => {
        console.log('map props to values: ', props);
        return {
            id: props.id || DEFAULT_FRONTEND_ID,
            description: props.description || DEFAULT_TEXT_EDITOR_HTML,
            point: props.point || 0,
            blankList: props.blankList ? [...props.blankList] : [],
            answers: props.blankList?.map(b => b?.answerKey || "") || [],
            optionalStudentNote: "",
            answerList: props.answerList
        };
    },
    handleSubmit: (values, formikBag) => {
        const valuesToSubmit: QuestionAnswer = {
            ...values,
        };
        formikBag.props.onConfirmSubmit?.(valuesToSubmit);
        console.log("FIB_Question answer values is: ", values);
    },
})(DragAndDropQuestionDisplayWrapper);
export default DragAndDropQuestionDisplay;
