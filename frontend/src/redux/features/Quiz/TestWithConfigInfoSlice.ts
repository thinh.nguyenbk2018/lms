import {createSliceEnhancedWithSearch} from '../../utils/createSliceWithSearch/createSliceEnhancedWithSearch';
import {RootState} from "../../store/store";
import {
    EnhancedSliceWithSearch,
    generateInitialSearchSlice
} from '../../utils/createSliceWithSearch/createSliceEnhancedWithSearch';



type WrappedSliceState = {
    currentTestInstanceName: string
}


type TestWithConfigInfoSliceState = EnhancedSliceWithSearch<WrappedSliceState>;

export const DEFAULT_TEST_INSTANCE_NAME = ""

const initState: TestWithConfigInfoSliceState = {
    currentTestInstanceName: DEFAULT_TEST_INSTANCE_NAME,
    ...generateInitialSearchSlice()
}

export const TEST_INSTANCE_SLICE_KEY = "testWithConfigInfo";


export const testWithConfigInfoSlice = createSliceEnhancedWithSearch({
    name: TEST_INSTANCE_SLICE_KEY,
    initialState: initState,
    reducers: {

    }
});

export const testWithConfigInfoActions = testWithConfigInfoSlice.actions;


export const testWithConfigInfoSelectors = {
    selectSearchCriteria: (state: RootState) => state.testWithConfigInfo.searchCriteria,
};


