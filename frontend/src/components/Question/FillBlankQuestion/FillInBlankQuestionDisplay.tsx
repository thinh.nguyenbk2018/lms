import React from "react";
import { Card, Divider, Input, Row, Tooltip } from "antd";
import {
    BlankWriting,
    FillInBlankQuestion,
    FillInBlankQuestionWithResult,
} from "../../../redux/features/Quiz/CreateQuizSlice";
import {
    DND_BLANK_SPAN_TAG_CLASS,
    replaceBlankWithSpecialSpan,
} from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import ReactHtmlParser from "html-react-parser";
import './FillInBlankQuestionDisplay.less';
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

type InputChangeHandler =
    | React.ChangeEventHandler<HTMLInputElement>
    | undefined;
export const blankRegex = /\[.*?\]/g;

const BlankWrittenWithHint = (blankWriting: BlankWriting) => {
    return (
        <Tooltip title={`Gợi ý: ${blankWriting.hint}`}>
            <Input defaultValue={blankWriting.expectedAnswer} disabled={true} className={'fill-in-blank-input'} />
        </Tooltip>
    );
};
const replaceBlankWithInputAndHint = (
    questionHTML: string,
    blankList: BlankWriting[]
) => {
    const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML);
    let blankCounter = 0;

    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if (
                (node as any).name === "span" &&
                (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
            ) {
                blankCounter++;
                const blankIndex = blankCounter - 1;
                return <BlankWrittenWithHint {...blankList[blankIndex]} />;
            }
            return node;
        },
    });
};

const FillInBlankQuestionDisplay = ({
    props,
}: {
    props: FillInBlankQuestion | FillInBlankQuestionWithResult;
}) => {
    // * The splitted string with Input to be inserted in between the elements
    let text;
    if ("blankList" in props) {
        console.log('props: ', props);
        text = replaceBlankWithInputAndHint(props.description, props.blankList);

    } else {
        text = replaceBlankWithInputAndHint(props.description, (props as FillInBlankQuestionWithResult).resultAnswerList);
    }

    return (
        <Card>
            {
                JSON.parse(props?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                    <AttachmentRender attachment={a} />
                </Row>)
            }
            <Divider />
            {text}
        </Card>
    );
};

export default FillInBlankQuestionDisplay;

// const text = "This is a test tring [ass] with brackets ir[here]versable";
// --> ["<p>This is a test tring  ", " with brackets ir",versable </p>]
// const res = _.split(text, /\[.*?\]/);

// TODO: Add logic and remove the default test data from the props
// const FillInBlankQuestionDisplay = ({props}: { props: FillInBlankQuestion }) => {
//     // * The splitted string with Input to be inserted in between the elements
//     const textArray = splitQuestionTextByBlanks(props.description);
//     const textReplacedBlank = replaceBlankInQuestionWithInput(props.description);
//     const numOfBlanks = textArray.length - 1;
//     return (
//         <>
//             <Card>
//                 <Col span={24} className={"question-box"}>
//                     <TextEditor onChange={() => {
//                     }} value={textReplacedBlank} readOnly={true} useBubbleTheme/>
//                 </Col>
//                 {
//                     Array.from({length: numOfBlanks}, (_, i) => i + 1).map(item => {
//                         return <Form key={item}>
//                             <Form.Item label={
//                                 <Typography.Text strong>
//                                     {`BLANK-${item}`}
//                                 </Typography.Text>
//                             }>
//                                 <Input></Input>
//                             </Form.Item>
//                         </Form>
//                     })
//                 }
//             </Card>
//         </>
//     );
// };
