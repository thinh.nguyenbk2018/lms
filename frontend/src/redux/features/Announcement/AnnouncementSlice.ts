import {RootState} from "../../store/store";
import {
    createSliceEnhancedWithSearch,
    EnhancedSliceWithSearch,
    generateInitialSearchSlice,
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";


const ANNOUNCEMENT_SLICE_KEY = "announcement"

type NotificationId = {
    classId: number,
    receiverId: number
}

const initialState: EnhancedSliceWithSearch<NotificationId> = {
    classId: -1,
    receiverId: -1,
    ...generateInitialSearchSlice()
}


export const announcementSlice = createSliceEnhancedWithSearch({
    name: ANNOUNCEMENT_SLICE_KEY,
    initialState,
    reducers: {},
});

export const announcementSelector = {
    selectSearchCriteria: (state: RootState) => state.announcement.searchCriteria
}

export const {changeCriteria} = announcementSlice.actions;
