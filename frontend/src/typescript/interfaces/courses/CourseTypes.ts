import {ID} from "../../../redux/interfaces/types";
import {Voting} from "../../../redux/features/Class/ClassAPI";
import {TestConfig, TestGradingState} from "../../../redux/features/Quiz/examAPI";
import {UserInfoForClickNavigation} from "@redux/features/Auth/AuthSlice";

export type Course = {
	id: number;
	name: string;
	code: string;
	level: string;
	price?: number;
	background?: string;
	description?: string;
	createdAt?: Date;
	programs?: ID[];
	chapters: Chapter[];
	updatedAt?: Date;
	createdBy?: UserInfoForClickNavigation;
	updatedBy?: UserInfoForClickNavigation;
};

export type CourseDto = Omit<Course, "chapters"> & { chapters: ChapterDto[] };

export type ChapterGroupByTextbookDto = {
	textbookId: number;
	textbookName: string;
	chapters: ChapterDto[]
}

export type ChapterGroupByTextbook = {
	textbookId: number;
	textbookName: string;
	chapters: Chapter[];
};

export type Chapter = {
	id: number;
	title: string;
	activities: Activity[];
	order: number;
};

export type ChapterDto = {
	id: number;
	title: string;
	quizzes: QuizItem[];
	units: UnitItem[];
	order: number;
	votes: VotingItem[];
};

export type Activity = QuizItem | UnitItem | VotingItem;
export type ActivityType = "unit" | "quiz" | "vote";

export type PublishableAcitivty = Exclude<Activity, VotingItem>;

export function isPublishableActivity(
	activity: any
): activity is PublishableAcitivty {
	if (!activity) return false;
	return (activity as PublishableAcitivty).state !== undefined;
}

export type ActivityBase = {
	id: number;
	type: ActivityType;
	order: number;
};

export type AccessibilityState = "PUBLIC" | "PRIVATE";
// * Place holder for teaching content in course or class
export interface QuizItem extends ActivityBase {
	type: "quiz";
	title: string;
	description: string;
	tag: string;
	examId: ID;
	state?: AccessibilityState;
	isAllowedToInitSession?: boolean;
	isAllowedToContinueLastSession?: boolean;
	examTitle?: string;
	totalGrade?: number;
	unEndedSessionId?: string;
	isFromCourse?: boolean;
	config?: TestConfig;
}

export interface UnitGroupByTextbook {
	textbookId: number;
	name: string;
	units: UnitItem[];
}

export interface UnitItem extends ActivityBase {
	type: "unit";
	title: string;
	textbooks: TextbookUnit[];
	content: string;
	state?: AccessibilityState;
	gradedState?: TestGradingState;
	isFromCourse?: boolean;
	attachment: string;
}

export type VotingItem = Omit<
	Voting,
	"createdAt" | "createdBy" | "updatedAt" | "updatedBy" | "choices"
> &
	ActivityBase & { type: "vote" };

export interface TextbookUnit {
	textbookId: number;
	name?: string;
	note?: string;
}

export interface TextbookRef {
	textbookId: number;
	name?: string;
	note?: string;
}
