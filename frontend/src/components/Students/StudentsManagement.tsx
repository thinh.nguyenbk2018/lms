import {Avatar, Dropdown, Menu, Modal} from 'antd';
import {Link, Navigate} from 'react-router-dom';
import AntdNotifier from '@utils/AntdAnnouncer/AntdNotifier';
import antdNotifier from '@utils/AntdAnnouncer/AntdNotifier';
import {Form, FormItem, Input} from "formik-antd";
import {STATUS_CODE_RESPONSE} from '../../constants/StatusCode';
import {useAppDispatch, useAppSelector} from '../../hooks/Redux/hooks';
import {ROUTING_CONSTANTS} from '../../navigation/ROUTING_CONSTANTS';
import {
    Student,
    useDeleteStudentMutation,
    useGetStudentListPaginatedQuery
} from '../../redux/features/Student/StudentAPI';
import {changeCriteria, studentSelector} from '../../redux/features/Student/StudentSlice';
import CustomTable from '../CustomTable/CustomTable';
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import PermissionRenderer, {useHasPermission} from '@components/Util/PermissionRender/PermissionRender';
import {PermissionEnum} from '@typescript/interfaces/generated/PermissionMap';
import {Field, Formik} from 'formik';
import {emailActions, emailSelector} from "@redux/features/Email/EmailSlice";
import TextEditorField from "@components/CustomFields/TextEditorField/TextEditorField";
import GeneralFileUploadField from "@components/CustomFields/FileUploadField/GeneralFileUploadField";
import React, {useState} from "react";
import {SendEmailDto, useSendEmailMutation} from "@redux/features/Email/EmailAPI";
import * as Yup from "yup";
import {validateTextEditor} from "@components/CustomFields/TextEditorField/ValidateTextEditor";


export const SendEmailSchema = Yup.object().shape({
    subject: Yup.string().required("Tiêu đề là trường bắt buộc"),
    content: Yup.string().test(
        "content",
        "Nội dung là trường bắt buộc",
        (text) => {
            return validateTextEditor(text);
        }
    ),
});

export  const defaultMail : SendEmailDto = {
    subject: '',
    content: '',
    attachment: ''
}

const StudentsManagement = () => {
    const searchCriteria = useAppSelector(studentSelector.selectSearchCriteria);
    const [deleteStudent, {isLoading: isDeleting}] = useDeleteStudentMutation();

    const dispatch = useAppDispatch();

    const showModal = useAppSelector(emailSelector.selectShowModal);

    const [chosenStudent, setChosenStudent] = useState({name: '', emailAddress: ''});

    const [sendEmail] = useSendEmailMutation();

    let {
        data: getStudentsPaginated,
        isLoading,
        isError,
        error,
    } = useGetStudentListPaginatedQuery(searchCriteria);

    const hasPermissionWithStudent = useHasPermission([PermissionEnum.VIEW_DETAIL_STUDENT, PermissionEnum.DELETE_STUDENT]);

    const renderMenu = (student: Student) => (
        <Menu>
            <PermissionRenderer permissions={[PermissionEnum.VIEW_DETAIL_STUDENT]}>
                <Menu.Item key="0">
                    <Link to={`/students/${student.id}`}>Chi tiết</Link>
                </Menu.Item>
            </PermissionRenderer>
            <PermissionRenderer permissions={[PermissionEnum.DELETE_STUDENT]}>
                <Menu.Item
                    key="3"
                    danger
                    onClick={() => {
                        confirmationModal('học viên', `${student.lastName} ${student.firstName}`, () => {
                            return deleteStudent(student.id)
                                .unwrap()
                                .then(() => {
                                    AntdNotifier.success(`Xóa học viên ${student.lastName} ${student.firstName} thành công`)
                                })
                                .catch(error => {
                                    if (error?.data) {
                                        AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá xóa. Thử lại sau.")
                                    } else {
                                        AntdNotifier.error("Có lỗi trong quá xóa. Thử lại sau.")
                                    }
                                })
                        });
                    }}
                >
                    Xóa tài khoản
                </Menu.Item>
            </PermissionRenderer>
            <Menu.Item
                key="1"
                onClick={() => {
                    setChosenStudent({name: student.lastName + ' ' + student.firstName, emailAddress: student.email});
                    dispatch(emailActions.setShowModal(true));
                }}
            >
                Gửi email
            </Menu.Item>
        </Menu>
    );

    const sortOrder = (fieldName: string) =>
        searchCriteria.sort.filter((x: any) => x.field === fieldName).length > 0 &&
        searchCriteria.sort.filter((x: any) => x.field === fieldName)[0].order;

    const columns = [
        {
            title: "Tên đăng nhập",
            dataIndex: "username",
            key: "username",
            width: "15%",
            search: true,
        },
        {
            title: "Avatar",
            dataIndex: "avatar",
            key: "avatar",
            width: "5%",
            render: (avatarUrl: string) => <Avatar src={avatarUrl}/>,
        },
        {
            title: "Họ và tên đệm",
            dataIndex: "lastName",
            key: "name",
            width: "20%",
            search: true,
        },
        {
            title: "Tên",
            dataIndex: "firstName",
            key: "firstName",
            width: "15%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
                multiple: 1,
            },
            sortDirections: ["descend", "ascend"],
            sortOrder: sortOrder("firstName"),
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "programs",
            width: "20%",
            // sorter: {
            // 	compare: (a: string, b: string) => a.length - b.length,
            // 	multiple: 1,
            // },
            // sortDirections: ["descend", "ascend"],
            // sortOrder: sortOrder("email"),
        },
        {
            title: "Số điện thoại",
            dataIndex: "phone",
            key: "programs",
            width: "20%",
            // sorter: {
            // 	compare: (a: string, b: string) => a.length - b.length,
            // 	multiple: 1,
            // },
            // sortDirections: ["descend", "ascend"],
            // sortOrder: sortOrder("firstName"),
        },
        {
            title: "Hành động",
            key: "action",
            with: "5%",
            render: (student: Student) => (
                <Dropdown overlay={renderMenu(student)} placement="bottomRight">
                    <a
                        className="ant-dropdown-link"
                        href="#"
                        onClick={(e) => e.preventDefault()}
                    >
                        Hành động
                    </a>
                </Dropdown>
            ),
        },
    ];



    if (
        isError &&
        (error as any)?.data.code === STATUS_CODE_RESPONSE.UNAUTHORIZED
    ) {
        return <Navigate to={ROUTING_CONSTANTS.UNAUTHORIZED}/>;
    } else
        return (
            <>
                <CustomTable
                    loading={isLoading}
                    columns={hasPermissionWithStudent ? columns : columns.filter(col => col.key !== "action")}
                    data={getStudentsPaginated?.data.listData || []}
                    searchCriteria={searchCriteria}
                    metaData={getStudentsPaginated?.data}
                    changeSearchActionCreator={changeCriteria}
                />
                <Formik
                    enableReinitialize={true}
                    initialValues={defaultMail}
                    validateOnChange={false}
                    validationSchema={SendEmailSchema}
                    onSubmit={(values, {resetForm, setSubmitting}) => {
                        console.log('values: ', values);
                        sendEmail({emailAddress: chosenStudent.emailAddress, body: values})
                            .unwrap()
                            .then(res => {
                                antdNotifier.success("Gửi email đến học viên thành công");
                                setSubmitting(false);
                                resetForm({values: defaultMail});
                                dispatch(emailActions.setShowModal(false));
                            });
                    }}
                >{({setFieldValue, values, isValid, handleSubmit, resetForm, isSubmitting}) => {
                    return (
                        <Modal
                            destroyOnClose={true}
                            visible={showModal}
                            title={<span>Gửi email cho học viên <b>{chosenStudent.name}</b></span>}
                            okText={'Gửi'}
                            cancelText={'Hủy'}
                            width={700}
                            onCancel={() => {
                                resetForm({values: defaultMail});
                                dispatch(emailActions.setShowModal(false));
                            }}
                            onOk={() => {
                                handleSubmit();
                            }}
                            maskClosable={false}
                            okButtonProps={{loading: isSubmitting, disabled: isSubmitting}}
                        >
                            <Form layout={'vertical'}>
                                <FormItem name={'subject'} label={'Tiêu đề'} required={true}>
                                    <Input name={'subject'} />
                                </FormItem>
                                {/*<FormItem name={'content'} label={'Nội dung'} required={true}>*/}
                                {/*    <Input name={'content'} />*/}
                                {/*</FormItem>*/}
                                <Field
                                    name={`content`}
                                    placeholder={""}
                                    component={TextEditorField}
                                    label={"Nội dung"}
                                    showValidateSuccess={true}
                                    required={true}
                                />
                                <Form.Item name={"attachment"} label={"Đính kèm tệp"}>
                                    {/* TODO: add prefix so the files will be saved on firebase as: /textbooks/... */}
                                    <Field
                                        name={"attachment"}
                                        placeholder={""}
                                        component={GeneralFileUploadField}
                                        showImagePreviewInsteadOfIcon={false}
                                        showPreview={true}
                                        displayedLayout={'row'}
                                    />
                                    {/* <Input name={ATTACHMENT_FIELD}/> */}
                                </Form.Item>
                            </Form>
                        </Modal>
                    );
                }
                }
                </Formik>
            </>
        );
};

export default StudentsManagement;
