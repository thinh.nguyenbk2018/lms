import {initializeApp} from "firebase/app";
import {getStorage} from "firebase/storage";
// const firebaseConfig = {

//     apiKey: "AIzaSyAGMohl1GSIVc87eNfkjNobqNONpTexO4Y",

//     authDomain: "bklms-47f9b.firebaseapp.com",

//     projectId: "bklms-47f9b",

//     storageBucket: "bklms-47f9b.appspot.com",

//     messagingSenderId: "183593738888",

//     appId: "1:183593738888:web:3f758e538b5489e91bb53d",

//     measurementId: "G-3114L1F0LX"

// };
const firebaseConfig = {

    apiKey: process.env.REACT_APP_GCLOUD_API_KEY,

    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,

    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,

    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,

    messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,

    appId: process.env.REACT_APP_FIREBASE_APP_ID,

    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
};
// console.log("FIREBASE CONFIG:",firebaseConfig);
// ? Firebase storage free quota and limits : https://firebase.google.com/docs/firestore/quotas
// and https://firebase.google.com/pricing/
const firebaseApp = initializeApp(firebaseConfig);
export const storage = getStorage(firebaseApp);

