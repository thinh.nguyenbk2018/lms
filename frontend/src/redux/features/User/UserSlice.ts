import { apiSlice } from "../CentralAPI";
import { BaseResponse } from "../../interfaces/types";

const userApiInvalidatesTags = apiSlice.enhanceEndpoints({
  addTagTypes: ["UserInfo"],
});

export interface UserInfo {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  avatar: string;
  address: string;
}

interface GetUserInfoResponse extends BaseResponse {
  userInfo: UserInfo
}

interface ChangePasswordRequest {
  password: string,
  newPassword: string,
  confirmPassword: string
}

interface ChangePasswordResponse extends BaseResponse {

}

const userAPI = userApiInvalidatesTags.injectEndpoints({
  endpoints: (build) => ({
    getCurrentUserInfo: build.query<GetUserInfoResponse, number>({
      query: (userId)=>`users/${userId}/getUserInfo`,
      providesTags: ["UserInfo"]
    }),
    changePassword: build.mutation<ChangePasswordResponse, ChangePasswordRequest>({
      query: (data)=>({
        url: "users/changePassword",
        method: "post",
        body: data
      })
    })
  }),
  overrideExisting: false,
});

export const {
  useGetCurrentUserInfoQuery,
  useChangePasswordMutation
} = userAPI;
