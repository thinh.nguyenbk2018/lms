import {Upload} from "antd";
import Modal from "antd/lib/modal/Modal";
import {UploadChangeParam} from "antd/lib/upload";
import {RcFile, UploadFile} from "antd/lib/upload/interface";
import {ref as fbRef, StorageReference, uploadBytes} from "firebase/storage";
import React, {forwardRef, Ref, useImperativeHandle, useState} from "react";
import IconSelector from '../Icons/IconSelector';
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";

const { Dragger } = Upload;

function getBase64(file: RcFile): Promise<string> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result!?.toString());
        reader.onerror = error => reject(error);
    });
}

export interface UploadFormProps {
    storageRef: StorageReference,
}

export interface UploadFormHandle {
    clearForm: () => void,
    upload: ({ onSuccess, onError }:
                 { onSuccess?: () => void, onError?: () => void }
    ) => void
}

const UploadFormWithIcon = ({ storageRef }: UploadFormProps, ref: Ref<UploadFormHandle>) => {

    let [fileList, setFileList] = useState<UploadFile<any>[]>([]);

    let [previewInfo, setPreviewInfo] = useState<{
        visible: boolean,
        image: string,
        title: string
    }>({
        visible: false,
        image: '',
        title: ''
    });

    const props = {
        name: 'file',
        multiple: true,
        onChange(info: UploadChangeParam<UploadFile<any>>) {
            setFileList(info.fileList);
        },
        onDrop(e: React.DragEvent<HTMLDivElement>) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };

    const handlePreview = async (file: UploadFile<any>) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj!);
        }

        setPreviewInfo({
            image: file.url! || file.preview!,
            visible: true,
            title: file.name || file.url!?.substring(file.url.lastIndexOf('/') + 1),
        });
    };

    useImperativeHandle(ref, () => ({
        clearForm: () => setFileList([]),
        upload: ({ onSuccess }) => {
            fileList.forEach(async file => {
                let fileArrayBuffer = await file.originFileObj?.arrayBuffer();
                uploadBytes(fbRef(storageRef, file.name), fileArrayBuffer!)
                    .then((value) => {
                        console.log(value.ref)
                        if (onSuccess) {
                            onSuccess();
                        }
                    })
                    .catch(reason => {
                        console.log(reason);
                        AntdNotifier.error(`Xảy ra lỗi trong quá trình upload file ${file.name}`, "Lỗi Upload")
                    })
                ;
            })
        }
    }))

    return <>
        <Dragger {...props}
                 listType="picture-card"
                 fileList={fileList}
                 customRequest={({ file, onSuccess }) => {
                     setTimeout(() => {
                         onSuccess!("ok");
                     }, 0);
                 }}
                 onPreview={handlePreview}
        >
            <p className="ant-upload-drag-icon">
                <IconSelector type='InboxOutlined' />
            </p>
            <p className="ant-upload-text">Click hoặc kéo thả vào đây để tải lên</p>
        </Dragger>
        <Modal
            visible={previewInfo?.visible}
            title={previewInfo.title}
            footer={null}
            onCancel={() => setPreviewInfo({
                visible: false,
                image: '',
                title: ''
            })}
        >
            <img alt="example" style={{ width: '100%' }} src={previewInfo.image} />
        </Modal>
    </>
}

export default forwardRef(UploadFormWithIcon);