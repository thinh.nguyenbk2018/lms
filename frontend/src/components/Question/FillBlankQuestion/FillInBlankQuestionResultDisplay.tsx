import { Card, Input, Row, Space, Tooltip } from "antd";
import { BlankWithResult, FillInBlankQuestionWithResult, } from "../../../redux/features/Quiz/CreateQuizSlice";
import {
	DND_BLANK_SPAN_TAG_CLASS,
	replaceBlankWithSpecialSpan,
} from "../DragAndDropQuestion/QuestionStatementWithBlanks";

import ReactHtmlParser from "html-react-parser";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { green } from "@ant-design/colors";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import './FillInBlankQuestionResultDisplay.less'
import COLOR_SCHEME from "@constants/ThemeColor";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

export const blankRegex = /\[.*?\]/g;

const BlankWrittenWithHint = (blankWriting: BlankWithResult) => {
	return (
		<Tooltip title={`Gợi ý: ${blankWriting.hint}`}>
			<Input
				style={{ display: "inline", width: "clamp(4em,6em,10em)" }}
				value={blankWriting.studentAnswer}
				disabled={true}
				className={'fill-in-blank-with-choices-input fill-in-blank-view-result'}
			/>
		</Tooltip>
	);
};
const replaceBlankWithInputAndHint = (
	questionHTML: string,
	resultAnswerList: BlankWithResult[]
) => {
	const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML);
	let blankCounter = 0;

	return ReactHtmlParser(questionReviewHTML, {
		replace: (node) => {
			if (
				(node as any).name === "span" &&
				(node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
			) {
				blankCounter++;
				const blankIndex = blankCounter - 1;
				return (
					<Space>
						<BlankWrittenWithHint {...resultAnswerList[blankIndex]} />
						{resultAnswerList[blankIndex] &&
							resultAnswerList[blankIndex].isCorrect && (
								<FontAwesomeIcon icon={faCheck} style={{ color: green[6] }} />
							)}
						{resultAnswerList[blankIndex] &&
							!resultAnswerList[blankIndex].isCorrect && (
								<FontAwesomeIcon icon={faTimes} style={{ color: COLOR_SCHEME.error }} />
							)}
					</Space>
				);
			}
			return node;
		},
	});
};

const FillInBlankQuestionResultDisplay = (
	props: FillInBlankQuestionWithResult
) => {
	const text = replaceBlankWithInputAndHint(
		props.description,
		props.resultAnswerList
	);
	return <Card>
		{
			JSON.parse(props?.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
				<AttachmentRender attachment={a} />
			</Row>)
		}
		{text}
	</Card>;
};

export default FillInBlankQuestionResultDisplay;
