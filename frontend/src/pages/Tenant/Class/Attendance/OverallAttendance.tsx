import {Avatar, Card, Col, Dropdown, Modal, Row, Table, Tooltip, Typography,} from "antd";
import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {testSvg} from "../../../../components/Icons/SvgIcons";
import {
    AttendanceState,
    getStudentListFromAttendanceDate,
    useChangeStudentAttendanceBatchByDateMutation,
    useChangeStudentAttendanceMutation,
    useGetClassAttendancesQuery,
    useRefetchOverallAttendanceMutation,
} from "@redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import {ID} from "@redux/interfaces/types";
import AttendanceTag from "./AttendanceTag";
import AttendanceStatisticsDisplay from "./AtttendanceStatisticsDisplay/AttendanceStatisticsDisplay";
import {useAppDispatch} from "@hooks/Redux/hooks";
import ChangeAttendanceMenu from "./ChangeAttendanceMenu/ChangeAttendanceMenu";
import {
    closeBatchSessionFocus,
    closeStudentSessionFocus,
    setSessionBatchInfocus,
    setStudentSessionInFocus,
} from "@redux/features/Class/ClassCheckin/ClassAttendanceSlice";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorRetryPage from "../../../UtilPages/ApiErrorRetryPage";
import LoadingPage from "../../../UtilPages/LoadingPage";
import {format, formatRelative} from "date-fns";
import {vi} from "date-fns/locale";
import {parseDate} from "@components/Calendar/Utils/CalendarUtils";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import './OverralAttendance.less';
import {ExclamationCircleOutlined} from "@ant-design/icons";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

export const studentInfoColumns = [
    {
        title: "Ảnh đại diện",
        dataIndex: "avatar",
        fixed: "left" as const,
        width: 150,
        render: (avatar: string) => <Avatar src={avatar}>User ava</Avatar>,
        align: "center" as const,
    },
    {
        title: "Họ và tên",
        dataIndex: "name",
        width: 200,
        fixed: "left" as const,
        sorter: {
            compare: (a: string, b: string) => a.length - b.length,
            multiple: 1,
        },
        sortDirections: ["descend", "ascend"],
        // sortOrder:
        //  TODO: Route to the student detail page on click using the "studentId" field
        render: (name: string) => <Typography.Link> {name} </Typography.Link>,
    },
];

const DateColumnHeader = React.memo(
    ({dateName, sessionId, finishedAt}: { dateName: string; sessionId: ID; finishedAt: Date }) => {
        const {classId} = useParams();

        const dispatch = useAppDispatch();
        const navigate = useNavigate();
        const [changeBatch] = useChangeStudentAttendanceBatchByDateMutation();
        return (
            <div className={'flex align-items-center'}>
                <Dropdown
                    className={'overrall-attendance-header'}
                    onVisibleChange={(visible) => {
                        if (visible) {
                            dispatch(setSessionBatchInfocus(sessionId));
                        } else {
                            dispatch(closeBatchSessionFocus());
                        }
                    }}
                    overlay={
                        <ChangeAttendanceMenu
                            onSelect={function (key: AttendanceState): void {
								if ((new Date(finishedAt)).getTime() < (new Date()).getTime()) {
									Modal.confirm({
										wrapClassName: 'confirm-delete-modal-wrapper',
										title: (
											<span className={'confirm-delete-modal-title-name'}>Thay đổi điểm danh của buổi học đã kết thúc. Bạn có chắc chắn?</span>
										),
										content: (
											<span>Bạn đang thay đổi kết quả điểm danh ngày <span
												className={'confirm-delete-modal-subject-name'}>{format(new Date(finishedAt), "PP", {locale: vi})}</span>.</span>
										),
										onOk: () => {
											if (!sessionId) {
												AntdNotifier.error("Không tìm thấy ngày để điểm danh");
											}

                                            changeBatch({
                                                classId: Number(classId),
                                                sessionId,
                                                batchState: key,
                                            })
                                                .unwrap()
                                                .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                                .catch(error => {
                                                    console.log('error: ', error);
                                                    if (error?.data) {
                                                        AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    } else {
                                                        AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    }
                                                });
										},
										icon: <ExclamationCircleOutlined/>,
										okText: `Đồng ý`,
										cancelText: 'Hủy',
										okButtonProps: {className: 'confirm-delete-modal-ok-btn'},
										cancelButtonProps: {className: 'confirm-delete-modal-cancel-btn'}
									})
									return;
								} else {
                                    console.log('key: ', key);
                                    if (!sessionId) {
                                        AntdNotifier.error("Không tìm thấy ngày để điểm danh");
                                    }

                                    changeBatch({
                                        classId: Number(classId),
                                        sessionId,
                                        batchState: key,
                                    }).unwrap()
                                        .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                        .catch(error => {
                                            console.log('error: ', error);
                                            if (error?.data) {
                                                AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                            } else {
                                                AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                            }
                                        });
                                }
                            }}
                        />
                    }
                >
                    <Row justify="space-between">
						<span>{`${formatRelative(parseDate(dateName), new Date(), {
                            locale: vi,
                        })}`}</span>
                    </Row>
                </Dropdown>
                <Tooltip style={{marginLeft: '1rem'}} title={"Vào trang lớp học"}>
                    <Typography.Link
                        onMouseEnter={(e) => {
                            e.stopPropagation();
                        }}
                        onClick={() => {
                            navigate(
                                replacePathParams(
                                    ROUTING_CONSTANTS.CLASS_CHECK_IN_SESSION_DETAIL,
                                    [":classId", Number(classId)],
                                    [":sessionId", Number(sessionId)]
                                )
                            );
                        }}
                    >
                        {testSvg}
                    </Typography.Link>
                </Tooltip>
            </div>
        );
    }
);

const buildAttendanceDateColumn = (dateName: string, sessionId: ID, finishedAt: Date) => ({
    title: () => <DateColumnHeader finishedAt={finishedAt} dateName={dateName} sessionId={sessionId}/>,
    dataIndex: dateName,
    // width: "10%",
    // TODO: Add sorter + Filter for AttendanceState: --> Create a map to PRESENT > LATE > ABSENT
    render: (attendanceState: AttendanceState, record: any) => {
        return (
            <AttendanceStateWrapper
                studentId={record.studentId}
                sessionId={sessionId}
                state={attendanceState}
                finishedAt={finishedAt}
            />
        );
    },
});

export const stateMap = {
    "ABSENT": "Vắng",
    "NONE": "N/A",
    "PRESENT": "Có mặt",
    "LATE": "Trễ"
}

// * We need this wrapper because we need to access hooks that allow us to access and modify the Redux Store
const AttendanceStateWrapper = React.memo(
    ({
         studentId,
         sessionId,
         state,
         finishedAt
     }: {
        studentId: number;
        sessionId: number;
        state: AttendanceState;
        finishedAt: Date;
    }) => {
        const dispatch = useAppDispatch();
        const [changeSingleAttendance] = useChangeStudentAttendanceMutation();
        const {classId} = useParams();
        return (
            <>
                <Dropdown
                    onVisibleChange={(visible) => {
                        if (visible) {
                            dispatch(setStudentSessionInFocus({studentId, sessionId}));
                        } else {
                            dispatch(closeStudentSessionFocus());
                        }
                    }}
                    overlay={
                        <ChangeAttendanceMenu
                            onSelect={function (key: AttendanceState): void {
                                if ((new Date(finishedAt)).getTime() < (new Date()).getTime()) {
                                    Modal.confirm({
                                        wrapClassName: 'confirm-delete-modal-wrapper',
                                        title: (
                                            <span className={'confirm-delete-modal-title-name'}>Thay đổi điểm danh của buổi học đã kết thúc. Bạn có chắc chắn?</span>
                                        ),
                                        content: (
                                            <span>Bạn đang thay đổi kết quả điểm danh ngày <span
                                                className={'confirm-delete-modal-subject-name'}>{format(new Date(finishedAt), "PP", {locale: vi})}</span>.</span>
                                        ),
                                        onOk: () => {
                                            if (!sessionId || !studentId) {
                                                AntdNotifier.error(
                                                    "Lỗi hệ thống. tạm thời không tìm thấy ngày hoặc học viên để điểm danh. Hãy thử load lại trang"
                                                );
                                                return;
                                            }

                                            changeSingleAttendance({
                                                classId: Number(classId),
                                                sessionId,
                                                studentId,
                                                newState: key,
                                            }).unwrap()
                                                .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                                .catch(error => {
                                                    console.log('error: ', error);
                                                    if (error?.data) {
                                                        AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    } else {
                                                        AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    }
                                                });
                                        },
                                        icon: <ExclamationCircleOutlined/>,
                                        okText: `Đồng ý`,
                                        cancelText: 'Hủy',
                                        okButtonProps: {className: 'confirm-delete-modal-ok-btn'},
                                        cancelButtonProps: {className: 'confirm-delete-modal-cancel-btn'}
                                    })
                                    return;
                                } else {
                                    if (!sessionId) {
                                        AntdNotifier.error("Không tìm thấy ngày để điểm danh");
                                        return;
                                    }

                                    changeSingleAttendance({
                                        classId: Number(classId),
                                        sessionId,
                                        studentId,
                                        newState: key,
                                    }).unwrap()
                                        .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                        .catch(error => {
                                            console.log('error: ', error);
                                            if (error?.data) {
                                                AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                            } else {
                                                AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                            }
                                        });
                                }
                            }}
                        />
                    }
                >
					<span>
						<AttendanceTag state={state} title={stateMap[state]}/>
					</span>
                </Dropdown>
            </>
        );
    }
);

const OverallAttendance = () => {
    const {classId} = useParams();

    const [pageSize, setPageSize] = useState<number>(DEFAULT_PAGE_SIZE);
    const [page, setPage] = useState<number>(1);

    const [refetchOverrallAttendance] = useRefetchOverallAttendanceMutation();

    useEffect(() => {
        refetchOverrallAttendance();
    }, []);

    const {
        data: overallData,
        isSuccess,
        isFetching,
        isError,
    } = useGetClassAttendancesQuery(
        {classId: Number(classId)},
        {
            skip: isNaN(Number(classId)),
        }
    );

    const actionColumn = {
        title: "Có mặt",
        key: "present",
        fixed: "right" as const,
        width: 100,
        render: (record: any) => {
            return (
                <span>{record.present + "/" + overallData?.happenedSession || 0}</span>
            );
        }
    };

    // * RTK QUERIES AND MUTATIONS:
    const [changeStudentAttendanceBatch] =
        useChangeStudentAttendanceBatchByDateMutation();
    const [changeStudentAttendance] = useChangeStudentAttendanceMutation();

    if (isSuccess && overallData) {
        const {attendanceInfo, statistics} = overallData;
        const datesInfo = Object.values(attendanceInfo);
        const firstDate = datesInfo[0];
        if (!firstDate) {
            return (
                <Row justify="center">
                    <Col span={24}>
                        <Card title="Chưa có học viên hoặc thời khóa biểu buổi học để điểm danh.">
                            {"Vui lòng thêm học viên vào lớp học hoặc thiết lập thời khóa biểu để bắt đầu làm việc với trang điểm danh."}
                        </Card>
                    </Col>
                </Row>
            );
        }
        const studentList = getStudentListFromAttendanceDate(
            firstDate.studentAttendance
        );

        const buildColumnsAndData = () => {
            let columns: any[] = [];

            const dates = overallData.attendanceInfo; // Date = sid:{StudentInfo}[]

            const middleColumnsMap: Record<string, AttendanceState[]> = {};
            const dateKeys = Object.keys(dates).sort();
            // console.log('dateKeys: ', dateKeys.sort());
            for (const dateKey of dateKeys) {
                const studentAttendanceOfTheDate = dates[dateKey].studentAttendance;
                const columnData = Object.values(studentAttendanceOfTheDate).map(
                    (item, index) => {
                        return item.state;
                    }
                );
                middleColumnsMap[dateKey] = columnData;
            }

            columns = [
                ...studentInfoColumns,
                ...Object.keys(middleColumnsMap).map((dateKey) => {
                    const sessionId = dates[dateKey].metaInfo.id;
                    if (!sessionId) {
                        AntdNotifier.error("Không tìm thấy id của lớp để hiển thị");
                        return <></>;
                    }
                    return buildAttendanceDateColumn(dateKey, sessionId, dates[dateKey].metaInfo.finishedAt);
                }),
                actionColumn,
            ];

            const dataSource = studentList.map((item, index) => {
                let studentDates: { [K: string]: AttendanceState } = {};
                for (const dateKey in middleColumnsMap) {
                    const date = middleColumnsMap[dateKey];
                    studentDates[dateKey] = date[index];
                }

                return {
                    name: item.name,
                    avatar: item.avatar,
                    studentId: item.studentId,
                    present: overallData?.presentCount[item.studentId],
                    ...studentDates,
                };
            });
            dataSource.sort((a, b) => a.studentId - b.studentId);
            return [columns, dataSource];
        };

        const [columnList, data] = buildColumnsAndData();

        return (
            <>
                <Table
                    columns={columnList as any}
                    dataSource={data as any}
                    title={() => (
                        <AttendanceStatisticsDisplay
                            {...statistics}
                            numberOfStudent={studentList.length || 1}
                        />
                    )}
                    scroll={{x: 3000}}
                    pagination={{
                        pageSize: pageSize,
                        current: page,
                        showSizeChanger: true, onShowSizeChange: (current: number, size: number) => {
                            setPageSize(size);
                            setPage(page);
                        },
                        onChange: (page: number, size: number) => {
                            setPage(page);
                        }
                    }}
                />
            </>
        );
    } else if (isFetching) {
        return <LoadingPage/>;
    } else {
        return <ApiErrorRetryPage/>;
    }
};

export default OverallAttendance;
