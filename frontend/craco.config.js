const CracoLessPlugin = require("craco-less");
const path = require("path");

const antdModifiedLessVars = {
    "@primary-color": "red",
};

module.exports = {
    plugins: [{
        plugin: CracoLessPlugin,
        options: {
            lessLoaderOptions: {
                lessOptions: {
                    antdModifiedLessVars,
                    sourceMap: true,
                    javascriptEnabled: true,
                    paths: [
                        path.resolve(__dirname, "src"),
                        path.resolve(__dirname, "src", "styles"),
                    ],
                },
            },
        },
    }, ],
    webpack: {
        alias: {
            "@": path.resolve(__dirname, "src"),
            "@pages": path.resolve(__dirname, "src", "pages"),
            "@components": path.resolve(__dirname, "src", "components"),
            "@hooks": path.resolve(__dirname, "src", "hooks"),
            "@utils": path.resolve(__dirname, "src", "utils"),
            "@redux": path.resolve(__dirname, "src", "redux"),
            "@redux_features": path.resolve(
                __dirname,
                "src",
                "redux",
                "features"
            ),
            "@assets": path.resolve(__dirname, "src", "assets"),
            "@constants": path.resolve(__dirname, "src", "constants"),
            "@typescript": path.resolve(__dirname, "src", "typescript"),
        },
    },
    jest: {
        configure: {
            moduleNameMapper: {
                "^@components/(.*)$": "<rootDir>/src/components/$1",
                "^@mocks/(.*)$": "<rootDir>/src/mocks/$1",
                "^@redux_store": "<rootDir>/src/redux/store/store.ts",
                "^@hooks/(.*)$": "<rootDir>/src/hooks/$1",
                "^@redux_features/(.*)$": "<rootDir>/src/redux/features/$1",
            },
        },
    },
    // * Tu rn below lines on for sentry
    // webpack: {
    // 	devtool: "source-map",
    // 	plugins: [
    // 		new SentryWebpackPlugin({
    // 			configFile: ".sentryclirc",
    // 			include: "build",
    // 			ignore: ["node_modules", "webpack.config.js"],
    // 		}),
    // 	],
    // },
};