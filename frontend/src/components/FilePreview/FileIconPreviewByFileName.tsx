import React from 'react';
import {Space, Tooltip, Typography} from "antd";
import {getFileExtensionFromFileName} from "./Utils/FileUtils";
import FileIconWithColor from "./FileIcon/FileIconWithColor";
import {MAX_NAME_LENGTH} from './FilePreview';
import './FileIconPreviewByFileName.less';


type FilePreviewProps = {
    filename: string | undefined,
    size?: number,
    showName: boolean,
    layout?: string
}
const FileIconPreviewByFileName = ({filename, size, showName, layout}: FilePreviewProps) => {
    const previewSize = size || 50;
    const nameLen = filename?.length || 0;
    const renderPreview = () => {
        if (!filename) {
            return <Typography.Text className={'file-icon'} type={"danger"}>
                <FileIconWithColor extension={undefined}/>
            </Typography.Text>
        } else {
            const extBacked = getFileExtensionFromFileName(filename);
            return <>
                {/*<Tooltip title={filename}>*/}
                {/*    <span>*/}
                {/*    <FileIconWithColor width={previewSize} extension={extBacked} height={undefined}/>*/}
                {/*    </span>*/}
                {/*</Tooltip>*/}
                <div className={`file-preview-wrapper ${layout ? layout : 'row'}`}>
                    <span style={{marginRight: '0.5rem'}} className={'file-icon'}>
                        <FileIconWithColor width={previewSize} extension={extBacked} height={undefined}/>
                    </span>
                    {(showName && nameLen <= MAX_NAME_LENGTH) && <span>{filename}</span> || <span>Chưa có file được tải lên</span>}
                </div>
            </>;
        }
    }
    return (
        <Space align='center' direction={"vertical"}>
            {renderPreview()}
        </Space>
    );
};

export default FileIconPreviewByFileName;