import {TEST_SESSION_SLICE_KEY, testSessionSlice,} from "./../features/Quiz/TestSessionSlice";
import {TEST_CONTENT_SLICE_KEY} from "./../features/Quiz/CreateQuizSlice";
import {combineReducers} from "@reduxjs/toolkit";
import {counterSlice} from "../features/Counter/CounterSlice";
import {mockUserSlice} from "../features/MockUser/MockUserSlice";
import {apiSlice} from "../features/CentralAPI";
import {quizCreateSlice} from "../features/Quiz/CreateQuizSlice";
import {courseSlice} from "../features/Course/CourseSlice";
import {authSlice} from "../features/Auth/AuthSlice";
import {tenantSlice} from "../features/Tenant/TenantSlice";
import {grantPermissionSlice} from "../features/Permission/PermissonSlice";
import {programSlice} from "../features/Program/ProgramSlice";
import {socketHistorySlice} from "../features/SocketSlice/SocketHistorySlice";
import {chatSlice} from "../features/Chat/ChatSlice";
import {calendarSlice} from "../features/Calendar/CalendarSlice";
import {classSlice} from "../features/Class/ClassSlice";
import {classMemberSlice} from "../features/Class/ClassMemberSlice";
import {userSearchSlice} from "../features/UserSearch/UserSearchSlice";
import {textbookSlice} from "../features/Textbook/TextbookSlice";
import {announcementSlice} from "@redux_features/Announcement/AnnouncementSlice";
import SidebarSlice from "../features/SideBar/SidebarSlice";
import {studentSlice} from '../features/Student/StudentSlice';
import {gradeFormulaSlice} from '../features/Grade/GradeFormulaSlice';
import {extendServiceSlice} from '../features/Host/ExtendService/extendServiceSlice';
import {TEST_INSTANCE_SLICE_KEY, testWithConfigInfoSlice,} from "../features/Quiz/TestWithConfigInfoSlice";
import {CLASS_TEACHER_SLICE_KEY, classTeacherSlice,} from "../features/Class/TeacherAPI/ClassTeacherSlice";
import {COURSE_CONTENT_SLICE_KEY, courseContentSlice,} from "../features/Course/CourseContentSlice";
import {CLASS_CONTENT_SLICE_KEY, classContentSlice,} from "../features/Class/ClassContent/ClassContentSlice";
import {
	CLASS_DISCUSSION_SLICE_KEY,
	classDiscussionSlice,
} from "../features/Class/ClassDiscussion/ClassDiscussionSlice";
import {CLASS_TIME_TABLE_SLICE_KEY, classTimeTableSlice,} from "../features/Class/ClassTimeTable/ClassTimeTableSlice";
import {CLASS_STUDENT_SLICE_KEY, classStudentSlice,} from "../features/Class/StudentAPI/ClassStudentSlice";
import {CLASS_ATTENDANCE_SLICE_KEY, classAttendanceSlice,} from "../features/Class/ClassCheckin/ClassAttendanceSlice";
import {TODO_SLICE_KEY, todoSlice} from "@components/Todo/todoSlice/todoSlice";
import {gradeResultSlice} from "@redux/features/Grade/GradeResultSlice";
import {courseQuizBankSlice} from "@redux/features/Quiz/CourseQuizBankSlice";
import {gradeManagementSlice} from "@redux/features/Grade/GradeManagementSlice";
import {staffsManagementSlice} from "@redux/features/Staff/StaffsManagementSlice";
import {quizResultManagementSlice} from "@redux/features/Quiz/QuizSessionManagementSlice";
import {courseClassListSlice} from "@redux/features/Course/CourseClassListSlice";
import {emailSlice} from "@redux/features/Email/EmailSlice";

const rootReducer = combineReducers({
	auth: authSlice.reducer,

	tenant: tenantSlice.reducer,
	grantPermission: grantPermissionSlice.reducer,

	sidebar: SidebarSlice.reducer,

	counter: counterSlice.reducer,
	mockUser: mockUserSlice.reducer,

	socketHistorySlice: socketHistorySlice.reducer,
	chat: chatSlice.reducer,
	calendar: calendarSlice.reducer,

	[TEST_CONTENT_SLICE_KEY]: quizCreateSlice.reducer,
	[TEST_INSTANCE_SLICE_KEY]: testWithConfigInfoSlice.reducer,
	[TEST_SESSION_SLICE_KEY]: testSessionSlice.reducer,
	program: programSlice.reducer,

	course: courseSlice.reducer,
	email: emailSlice.reducer,

	class: classSlice.reducer,
	courseClassList: courseClassListSlice.reducer,
	classMember: classMemberSlice.reducer,
	student: studentSlice.reducer,
	gradeResult: gradeResultSlice.reducer,
	gradeManagement: gradeManagementSlice.reducer,
	courseQuizBank: courseQuizBankSlice.reducer,
	staffsManagement: staffsManagementSlice.reducer,
	quizResultManagement: quizResultManagementSlice.reducer,

	[CLASS_TEACHER_SLICE_KEY]: classTeacherSlice.reducer,
	[COURSE_CONTENT_SLICE_KEY]: courseContentSlice.reducer,
	[CLASS_CONTENT_SLICE_KEY]: classContentSlice.reducer,
	[CLASS_DISCUSSION_SLICE_KEY]: classDiscussionSlice.reducer,
	[CLASS_TIME_TABLE_SLICE_KEY]: classTimeTableSlice.reducer,
	[CLASS_STUDENT_SLICE_KEY]: classStudentSlice.reducer,
	[CLASS_ATTENDANCE_SLICE_KEY]: classAttendanceSlice.reducer,
	userSearch: userSearchSlice.reducer,
	textbook: textbookSlice.reducer,

	announcement: announcementSlice.reducer,
    gradeFormula: gradeFormulaSlice.reducer,
    extendService: extendServiceSlice.reducer,
    [apiSlice.reducerPath]: apiSlice.reducer, 

	// * Todo example on how to integrate testing with redux toolkit 
	[TODO_SLICE_KEY]: todoSlice.reducer, 
})

export default rootReducer;
