"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.classDiscussionActions = exports.classDiscussionSelectors = exports.classDiscussionSlice = exports.CLASS_DISCUSSION_SLICE_KEY = void 0;
var createSliceEnhancedWithSearch_1 = require("../../../utils/createSliceWithSearch/createSliceEnhancedWithSearch");
var initialState = __assign(__assign({}, createSliceEnhancedWithSearch_1.generateInitialSearchSlice()), { openingThreadModal: "NONE", threadInEdit: undefined, commentInEdit: undefined, postOfCommentInEdit: undefined });
exports.CLASS_DISCUSSION_SLICE_KEY = "classDicussion";
exports.classDiscussionSlice = createSliceEnhancedWithSearch_1.createSliceEnhancedWithSearch({
    name: exports.CLASS_DISCUSSION_SLICE_KEY,
    initialState: initialState,
    reducers: {
        addingNewPost: function (state) {
            state.openingThreadModal = "CREAT";
        },
        editPost: function (state, action) {
            state.openingThreadModal = "EDIT";
            state.threadInEdit = action.payload;
        },
        closeModal: function (state) {
            state.openingThreadModal = "NONE";
        },
        editComment: function (state, action) {
            var _a = action.payload, commentToEdit = _a.commentToEdit, postIdOfComment = _a.postIdOfComment;
            state.commentInEdit = commentToEdit;
            state.postOfCommentInEdit = postIdOfComment;
        },
        doneEditComment: function (state) {
            state.commentInEdit = undefined;
        }
    }
});
exports.classDiscussionSelectors = {
    selectSearchCriteria: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].searchCriteria;
    },
    selectIsAddingNewPost: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].openingThreadModal === "CREAT";
    },
    selectIsEditingPost: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].openingThreadModal === "EDIT";
    },
    selectIsEditingComment: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].commentInEdit !== undefined;
    },
    selectPostInEdit: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].threadInEdit;
    },
    selectCommentInEdit: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].commentInEdit;
    },
    selectPostOFCommentInEdit: function (state) {
        return state[exports.CLASS_DISCUSSION_SLICE_KEY].postOfCommentInEdit;
    }
};
exports.classDiscussionActions = exports.classDiscussionSlice.actions;
