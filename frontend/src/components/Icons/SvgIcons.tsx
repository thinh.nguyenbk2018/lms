import React from "react";

const searchIconSvg = (
    <svg className="h-8 w-8 text-red-500" width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <circle cx="10" cy="10" r="7"/>
        <line x1="21" y1="21" x2="15" y2="15"/>
    </svg>);

const courseIconSvg = (<svg className="h-8 w-8 text-red-500" width="16px" height="16px" viewBox="0 0 24 24"
                            strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
                            strokeLinejoin="round">
    <path stroke="none" d="M0 0h24v24H0z"/>
    <polyline points="12 4 4 8 12 12 20 8 12 4"/>
    <polyline points="4 12 12 16 20 12"/>
    <polyline points="4 16 12 20 20 16"/>
</svg>);

const viewIconSvg = (<svg className="h-8 w-8 text-teal-500" width="16px" height="16px" viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg" fill="none" stroke="currentColor" strokeWidth="2"
                          strokeLinecap="round" strokeLinejoin="round">
    <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"/>
    <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"/>
</svg>);

const detailIconSvg = (
    <svg className="h-8 w-8 text-red-500" width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <circle cx="12" cy="12" r="2"/>
        <path d="M2 12l1.5 2a11 11 0 0 0 17 0l1.5 -2"/>
        <path d="M2 12l1.5 -2a11 11 0 0 1 17 0l1.5 2"/>
    </svg>
);

export const testSvg = (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" width={"16px"} height={"16px"}>
        <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"/>
        <path fill-rule="evenodd"
              d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z"
              clip-rule="evenodd"/>
    </svg>);


const deleteIconSvg = (
    <svg className="h-8 w-8 text-red-500" width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <line x1="4" y1="7" x2="20" y2="7"/>
        <line x1="10" y1="11" x2="10" y2="17"/>
        <line x1="14" y1="11" x2="14" y2="17"/>
        <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"/>
        <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"/>
    </svg>);

const addIconSvg = (<svg className="h-8 w-8 text-teal-500" width="16px" height="16px"
                         viewBox="0 0 24 24"
                         strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
                         strokeLinejoin="round">
    <path stroke="none" d="M0 0h24v24H0z"/>
    <circle cx="12" cy="12" r="9"/>
    <line x1="9" y1="12" x2="15" y2="12"/>
    <line x1="12" y1="9" x2="12" y2="15"/>
</svg>);

const backIconSvg = (
    <svg className="h-4 w-4 text-white" width="1.25rem" height="1.25rem" viewBox="0 0 24 24"
         strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
         strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <line x1="5" y1="12" x2="19" y2="12"/>
        <line x1="5" y1="12" x2="9" y2="16"/>
        <line x1="5" y1="12" x2="9" y2="8"/>
    </svg>
);

const updateIconSvg = (
    <svg className="h-8 w-8 text-white" width="16px" height="16px" viewBox="0 0 24 24"
         strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
         strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <path d="M5 12l5 5l10 -10"/>
    </svg>
);

const warningIconSvg = (
    <svg className="h-4 w-4 text-yellow-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <path d="M12 9v2m0 4v.01"/>
        <path d="M5.07 19H19a2 2 0 0 0 1.75 -2.75L13.75 4a2 2 0 0 0 -3.5 0L3.25 16.25a2 2 0 0 0 1.75 2.75"/>
    </svg>
);

const reloadIconSvg = (
    <svg className="h-8 w-8 text-red-500" width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <path d="M15 4.55a8 8 0 0 0 -6 14.9m0 -4.45v5h-5"/>
        <path d="M13 19.95a8 8 0 0 0 5.3 -12.8" strokeDasharray=".001 4.13"/>
    </svg>
);

const fullScreenIconSvg = (
    <svg className="h-8 w-8 text-red-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2"
         strokeLinecap="round" strokeLinejoin="round">
        <polyline points="15 3 21 3 21 9"/>
        <polyline points="9 21 3 21 3 15"/>
        <line x1="21" y1="3" x2="14" y2="10"/>
        <line x1="3" y1="21" x2="10" y2="14"/>
    </svg>
);

const fullScreen1IconSvg = (
    <svg className="svg-inline--fa fa-expand fa-w-14 fa-fw" aria-hidden="true" focusable="false" data-prefix="fas"
         data-icon="expand" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 550 550" data-fa-i2svg="">
        <path fill="currentColor"
              d="M0 180V56c0-13.3 10.7-24 24-24h124c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H64v84c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12zM288 44v40c0 6.6 5.4 12 12 12h84v84c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12V56c0-13.3-10.7-24-24-24H300c-6.6 0-12 5.4-12 12zm148 276h-40c-6.6 0-12 5.4-12 12v84h-84c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h124c13.3 0 24-10.7 24-24V332c0-6.6-5.4-12-12-12zM160 468v-40c0-6.6-5.4-12-12-12H64v-84c0-6.6-5.4-12-12-12H12c-6.6 0-12 5.4-12 12v124c0 13.3 10.7 24 24 24h124c6.6 0 12-5.4 12-12z">
        </path>
    </svg>
);

const dragIconSvg = (
    <svg className="h-8 w-8 text-red-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <circle cx="9" cy="5" r="1"/>
        <circle cx="9" cy="12" r="1"/>
        <circle cx="9" cy="19" r="1"/>
        <circle cx="15" cy="5" r="1"/>
        <circle cx="15" cy="12" r="1"/>
        <circle cx="15" cy="19" r="1"/>
    </svg>
);

const arrowOpenInCollapseSvgIcon = (
    <span role="img" aria-label="right" className="anticon anticon-right ant-collapse-arrow">
        <svg
            viewBox="64 64 896 896" focusable="false" data-icon="right" width="1em" height="1em"
            fill="currentColor" aria-hidden="true" style={{transform: "rotate(90deg)"}}>
            <path
                d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z">

            </path>
        </svg>
    </span>
);

const arrowCloseInCollapseSvgIcon = (
    <span role="img" aria-label="right" className="anticon anticon-right ant-collapse-arrow">
        <svg
            viewBox="64 64 896 896" focusable="false" data-icon="right" width="1em" height="1em"
            fill="currentColor" aria-hidden="true" style={{}}>
            <path
                d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z">

            </path>
        </svg>
    </span>
);

const okeSvgIcon = (
    <svg className="h-8 w-8 text-white" width="16px" height="16px" viewBox="0 0 24 24"
         strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
         strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <path d="M5 12l5 5l10 -10"/>
    </svg>
);

const clockSvgIcon = (
    <svg className="h-8 w-8 text-red-500" width="16px" height="16px" viewBox="0 0 24 24" fill="none"
         stroke="currentColor" stroke-width="2"
         stroke-linecap="round" stroke-linejoin="round">
        <circle cx="12" cy="12" r="10"/>
        <polyline points="12 6 12 12 16 14"/>
    </svg>
);

const exclamationSvgIcon = (
    <svg className="h-8 w-8 text-red-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
              d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
    </svg>
);

const flagSvgIcon = (
    <svg className="h-8 w-8 text-red-500" width="16" height="16" viewBox="0 0 24 24" strokeWidth="2"
         stroke="currentColor" fill="none" strokeLinecap="round" stroke-linejoin="round">
        <path stroke="none" d="M0 0h24v24H0z"/>
        <line x1="5" y1="5" x2="5" y2="21"/>
        <line x1="19" y1="5" x2="19" y2="14"/>
        <path d="M5 5a5 5 0 0 1 7 0a5 5 0 0 0 7 0"/>
        <path d="M5 14a5 5 0 0 1 7 0a5 5 0 0 0 7 0"/>
    </svg>
);

export {
    searchIconSvg,
    courseIconSvg,
    viewIconSvg,
    deleteIconSvg,
    addIconSvg,
    backIconSvg,
    updateIconSvg,
    warningIconSvg,
    reloadIconSvg,
    fullScreenIconSvg,
    fullScreen1IconSvg,
    dragIconSvg,
    arrowOpenInCollapseSvgIcon,
    arrowCloseInCollapseSvgIcon,
    detailIconSvg,
    okeSvgIcon,
    clockSvgIcon,
    exclamationSvgIcon,
    flagSvgIcon
};
