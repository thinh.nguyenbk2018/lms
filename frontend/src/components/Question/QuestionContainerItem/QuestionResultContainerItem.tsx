import {EditOutlined} from "@ant-design/icons";
import {Card, Input, Space, Tooltip, Typography} from "antd";
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {QuestionDataTypeWithResult,} from "../../../redux/features/Quiz/CreateQuizSlice";
import {testSessionSelectors} from "../../../redux/features/Quiz/TestSessionSlice";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import FillInBlankQuestionResultDisplay from "../FillBlankQuestion/FillInBlankQuestionResultDisplay";
import MultichoiceQuestionResultDisplay from "../MultichoiceQuestion/MultichoiceQuestionResultDisplay";
import BlankWithChoicesQuestionResultDisplay from "../BlankWithChoicesQuestion/BlankWithChoicesQuestionResultDisplay";
import WrittenQuestionResultDisplay from "../WrittenQuestion/WrittenQuestionResultDisplay";
import DragAndDropQuestionResultDisplay from "../DragAndDropQuestion/DragAndDropQuestionResultDisplay";
import {useState} from "react";
import {useUpdatePointForWritingQuestionMutation} from "../../../redux/features/Quiz/examAPI";
import useClassCourseInformation from "../../../hooks/Routing/useClassCourseInformation";
import {useParams} from "react-router-dom";
import QuestionGroupDisplayWithResult from "@components/Question/QuestionGroup/QuestionGroupDisplayWithResult";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";

type QuestionResultContainerItemProps = { order: number } & QuestionDataTypeWithResult;

const QuestionResultContainerItem = (
    resultData: QuestionResultContainerItemProps
) => {
    const {classId: classIdFromPath, quizItemId, testSessionId} = useParams();
    const dispatch = useAppDispatch();
    const sessionId = useAppSelector(testSessionSelectors.selectCurrentSessionId);

    const [updatePoint] = useUpdatePointForWritingQuestionMutation();
    const [editPoint, setEditPoint] = useState(false);
    const [value, setValue] = useState(0);

    const {classId, courseId, isErrorClass, isFetchingClass, isSuccessClass} =
        useClassCourseInformation(Number(classIdFromPath));

    function renderQuestion(question: QuestionDataTypeWithResult | undefined) {
        if (!question) {
            return <></>;
        }
        switch (question.type) {
            case "MULTI_CHOICE":
                return <MultichoiceQuestionResultDisplay {...question} />;
            case "FILL_IN_BLANK":
                return <FillInBlankQuestionResultDisplay {...question} />;
            case "FILL_IN_BLANK_WITH_CHOICES":
                return <BlankWithChoicesQuestionResultDisplay {...question} />;
            case "WRITING":
                return <WrittenQuestionResultDisplay {...question} />;
            case "FILL_IN_BLANK_DRAG_AND_DROP":
                return <DragAndDropQuestionResultDisplay {...question} />;
            case "GROUP":
                return <QuestionGroupDisplayWithResult {...question} />;
        }
    }

    // @ts-ignore
    // @ts-ignore
    return (
        <span className={`question-card`}>
			<Card
                type="inner"
                title={
                    <Typography.Text className={'question-card-title'}>
                        {`Câu hỏi ${resultData.order}`}
                    </Typography.Text>
                }
                extra={
                    <Space>
                        <Typography.Text className={'question-card-title'}>
                            {!editPoint ? (
                                <>
                                    {resultData.earnedPoint == null ?
                                        "Chưa chấm điểm " :
                                        `${resultData.earnedPoint % 1 == 0 ? resultData.earnedPoint : resultData.earnedPoint?.toFixed(2)} `}
                                    / {resultData?.point ?? "0"} điểm
                                </>
                            ) : (
                                <>
                                    <Input
                                        type={"number"}
                                        max={resultData.point}
                                        min={0}
                                        value={value}
                                        onChange={(e) => {
                                            setValue(Number(e.target.value));
                                        }}
                                        onBlur={() => {
                                            if (
                                                !isSuccessClass ||
                                                !testSessionId ||
                                                !resultData.id ||
                                                isNaN(Number(courseId)) ||
                                                isNaN(Number(classId)) ||
                                                isNaN(Number(quizItemId)) ||
                                                isNaN(Number(resultData.id))
                                            ) {
                                                AntdNotifier.error(
                                                    "Bạn tạm thời không thể chấm điểm cho câu hỏi này."
                                                );
                                                return;
                                            }
                                            updatePoint({
                                                courseId: Number(courseId),
                                                classId: Number(classId),
                                                quizId: Number(quizItemId),
                                                sessionId: testSessionId,
                                                questionId: resultData.id,
                                                gradingDto: {
                                                    point: value,
                                                    type: "WRITING",
                                                    resultState: 0,
                                                },
                                            }).unwrap()
                                                .then(res => {
                                                    AntdNotifier.success("Chấm điểm cho học viên thành công.");
                                                    setEditPoint(false);
                                                })
                                                .catch(err => {
                                                    if (err.data) {
                                                        AntdNotifier.error(err.data.message ? err.data?.message : "Có lỗi xảy ra khi chấm bài cho học viên.")
                                                    } else {
                                                        AntdNotifier.error("Có lỗi xảy ra khi chấm bài cho học viên.")
                                                    }
                                                });
                                        }}
                                    />
                                </>
                            )}
                        </Typography.Text>
                        <PermissionRenderer permissions={[PermissionEnum.GRADE_QUIZ]}>
                            {resultData.type === "WRITING" && (
                                <Tooltip title={"Chỉnh sửa câu hỏi"}>
                                    <EditOutlined
                                        className={'question-card-title'}
                                        onClick={() => {
                                            if (!resultData || !resultData?.id) {
                                                AntdNotifier.error(
                                                    "Lỗi khi hệ thống cố gắng sửa câu hỏi: Không tìm thấy"
                                                );
                                            } else {
                                                setEditPoint(true);
                                            }
                                        }}
                                    />
                                </Tooltip>
                            )}
                        </PermissionRenderer>
                    </Space>
                }
                bodyStyle={{display: "none"}}
            />
            {renderQuestion(resultData)}
		</span>
    );
};

export default QuestionResultContainerItem;
