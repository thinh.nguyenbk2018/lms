import React, {useEffect, useState} from 'react'
import {useDrop} from 'react-dnd'
import {QuizDragTypes} from '../../../constants/DragTypes'
import {
    DragAndDropBlank,
    FillBlankDragAndDropQuestion,
    QuestionDataType
} from './../../../redux/features/Quiz/CreateQuizSlice';
import {DragAnswer} from './DraggableAnswer';
import type {Identifier} from 'dnd-core'
import {useFormikContext} from 'formik';
import './BlankDropTarget.less';
import {QuestionAnswer} from "../../../redux/features/Quiz/examAPI";


export type BlankDropTargetProps = DragAndDropBlank & {
    blankIndex: number; keyToContent: (key: number | string) => string;
    isCreateOrUpdate?: boolean;
    mode?: string
}

// * What to take from the monitor 
type DropProps = { handlerId: Identifier | null, canDrop: boolean; isOver: boolean; }
// * Drop returned back to the dragged item 
type DropRetuned = { droppedAtBlank: number; answerDroppedKey: string | number; }

function isQuestionAnswer(value: QuestionDataType | QuestionAnswer): value is QuestionAnswer {
    return (value as QuestionAnswer).answers !== undefined;
}

const BlankDropTarget = (props: BlankDropTargetProps) => {
    const {hint, answerKey, blankIndex, keyToContent, isCreateOrUpdate, mode} = props;
    const {values, setFieldValue, handleSubmit} = useFormikContext<FillBlankDragAndDropQuestion | QuestionAnswer>();
    const [answerGotten, setAnswerGotten] = useState<number | string | null>();
    const [content, setContent] = useState("");

    useEffect(() => {
        // console.log('values formik: ', values);
        // if (mode === "doing" && values['answers'] != undefined) {
        //     setAnswerGotten(values['answers'][blankIndex]);
        // }
        console.log('mode: ', mode);
        if (mode === "view") {
            setContent(keyToContent(values['blankList'][blankIndex]?.answerKey));
            return;
        }
        if (mode === "doing") {
            if (values['answers'] != undefined && values['answers'][blankIndex] !== "") {
                setContent(keyToContent(values['answers'][blankIndex]));
                return;
            }
            setContent(`BLANK-${blankIndex}`);
            return;
        }
        if (mode === "edit") {
            if (values['blankList'][blankIndex]?.answerKey == undefined) {
                setContent(`BLANK-${blankIndex}`);
                return;
            }
            setContent(keyToContent(values['blankList'][blankIndex].answerKey));
            return;
        }
        setContent("");
    }, [values])
    // * ------------------------ The Item Dropped in -- Return back to dragged -- State of drag and drop 
    const [{canDrop, isOver}, drop] = useDrop<DragAnswer, DropRetuned, DropProps>({
        accept: QuizDragTypes.FillInBlankDnD.DnDAnswer,
        collect: (monitor) => {
            return {
                handlerId: monitor.getHandlerId(),
                canDrop: monitor.canDrop(),
                isOver: monitor.isOver(),
            }
        },
        drop: (item, monitor) => {
            // setAnswerGotten(item.answerKey);
            if (isQuestionAnswer(values)) {
                setFieldValue(`answers[${blankIndex}]`, item.answerKey);
            } else setFieldValue(`blankList[${blankIndex}].answerKey`, item.answerKey);
            // console.log("Blank received item: ", item);
            if (!isCreateOrUpdate) {
                handleSubmit();
            }
            return {droppedAtBlank: props.blankIndex, answerDroppedKey: item.answerKey}
        },

    })

    // const getKeyToAnswer = () => {
    //     console.log('key: ', values['blankList'][blankIndex]?.answerKey);
    //     console.log('blankList: ',  values['blankList']);
    //     console.log('answerList: ',  values['answerList']);
    //     if (mode === "view") {
    //         return keyToContent(values['blankList'][blankIndex]?.answerKey);
    //     }
    //     if (mode === "doing") {
    //         if (answerGotten == undefined) {
    //             return "";
    //         }
    //         return keyToContent(answerGotten);
    //     }
    //     if (mode === "edit") {
    //         if (values['blankList'][blankIndex]?.answerKey == undefined) {
    //             return `BLANK-${blankIndex}`;
    //         }
    //         return keyToContent(values['blankList'][blankIndex].answerKey);
    //     }
    //     return "";
    // }
    // style={{width: `100px`,height:`100px`,backgroundColor: "red"}}
    // const keyToAnswer = (answerGotten === 0) ? 0 : (answerGotten ?? 0);
    // console.log('key to answer: ', keyToAnswer);
    return (
        <div ref={drop} className={"blank-drop-target"} style={{backgroundColor: `${isOver ? "yellow" : "white"}`}}>
            {/*{keyToAnswer === 0 ? keyToContent(keyToAnswer) :  (keyToAnswer && keyToContent(keyToAnswer)) }*/}
            {/*{(!answerGotten && answerGotten !==0) && `BLANK-${blankIndex}`}*/}
            {/*{getKeyToAnswer()}*/}
            {content}
        </div>
    );
}

export default BlankDropTarget;


// const BlankDropTargetDisplay = (props: BlankDropTargetProps) => {
//     const {hint, answerKey, blankIndex, keyToContent} = props;
//     const [answerGotten, setAnswerGotten] = useState<number | string | null>("");
//     // * ------------------------ The Item Dropped in -- Return back to dragged -- State of drag and drop
//     const [{canDrop, isOver}, drop] = useDrop<DragAnswer, DropRetuned, DropProps>({
//         accept: QuizDragTypes.FillInBlankDnD.DnDAnswer,
//         collect: (monitor) => {
//             return {
//                 handlerId: monitor.getHandlerId(),
//                 canDrop: monitor.canDrop(),
//                 isOver: monitor.isOver(),
//             }
//         },
//         drop: (item, monitor) => {
//             setAnswerGotten(item.answerKey);
//             if (isQuestionAnswer(values)) {
//                 setFieldValue(`answers[${blankIndex}]`, item.answerKey);
//             } else setFieldValue(`blankList[${blankIndex}].answerKey`, item.answerKey);
//             console.log("Blank received item: ", item);
//             return {droppedAtBlank: props.blankIndex, answerDroppedKey: item.answerKey}
//         },
//
//     })
//     // style={{width: `100px`,height:`100px`,backgroundColor: "red"}}
//     return (
//         <div ref={drop} className={"blank-drop-target"} style={{backgroundColor: `${isOver ? "yellow" : "white"}`}}>
//             {answerGotten && keyToContent(answerGotten)}
//             {!answerGotten && `BLANK-${blankIndex}`}
//         </div>
//     );
// }




