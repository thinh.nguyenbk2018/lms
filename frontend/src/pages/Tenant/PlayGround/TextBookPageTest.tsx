import { Space } from "antd";
import React from "react";
import AddButton from "../../../components/ActionButton/AddButton";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import TextBookList from "../../../components/TextBook/TextBookList";
import { useAppDispatch } from "../../../hooks/Redux/hooks";
import { createTextBook } from "../../../redux/features/Textbook/TextbookSlice";

const TextBookPageTest = () => {
	const dispatch = useAppDispatch();
	return (
		<>
			<ContentHeader
				title="giáo trình"
				action={
					<Space>
						<AddButton
							type="primary"
							onClick={() => {
								dispatch(createTextBook());
							}}
						/>
					</Space>
				}
			/>
			<div>
				<TextBookList />
				{/* <TextBookListTest/> */}
			</div>
		</>
	);
};

export default TextBookPageTest;
