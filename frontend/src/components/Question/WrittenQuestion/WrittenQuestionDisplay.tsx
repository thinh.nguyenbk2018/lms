import React, { useState } from 'react';
import { Card, Col, Divider, Row } from "antd";
import TextEditor from "../../TextEditor/TextEditor";
import { WrittenQuestion } from "../../../redux/features/Quiz/CreateQuizSlice";
import './WrittenQuestionDisplay.less';
import { Attachment } from '../Attachment/AttachmentQuestion';
import AttachmentRender from '../Attachment/AttachmentRender';

const WrittenQuestionDisplay = ({ props, mode }: { props: WrittenQuestion, mode?: string }) => {
    const [answer, setAnswer] = useState("");
    return (
        <Card className={'writing-question-display-wrapper'}>
            {
                JSON.parse(props?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                    <AttachmentRender attachment={a} />
                </Row>)
            }
            <Divider />
            <Row>
                <Col span={24} className={"question-box"}>
                    <TextEditor onChange={() => {
                    }} value={props.description} readOnly={true} useBubbleTheme />
                </Col>
            </Row>
            {mode != "view" && <Row>
                <Col span={24} className={"answer-box"}>
                    <TextEditor onChange={setAnswer} value={answer}/>
                </Col>
            </Row>}
        </Card>
    );
};

export default WrittenQuestionDisplay;