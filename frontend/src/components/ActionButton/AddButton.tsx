import { faCirclePlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, ButtonProps } from "antd";
const AddButton = (props: ButtonProps) => {
	return (
		<Button
			type={"primary"}
			{...props}
			icon={
				<FontAwesomeIcon style={{ margin: "0 .5em 0 0" }} icon={faCirclePlus} />
			}
		>
			{props.title || "Thêm"}
		</Button>
	);
};

export default AddButton;
