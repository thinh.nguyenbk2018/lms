import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import { Button, Space } from "antd";
import PermissionContent from "../../../components/Authorization/GrantPermisson/PermissonContent/PermissionContent";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { backupRolesAndPermissions, cancelEdit, toggleEidtState } from "../../../redux/features/Permission/PermissonSlice";
import { useGrantPermissionMutation } from "../../../redux/features/Permission/PermissionAPI";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";

const PermissionPage = () => {
  let grantPermissionState = useAppSelector(state => state.grantPermission);

  let [grantPermission] = useGrantPermissionMutation();


  let dispatch = useAppDispatch();
  const onEdit = () => {
    dispatch(backupRolesAndPermissions())
    dispatch(toggleEidtState());
  }

  const onCancel = () => {
    dispatch(cancelEdit());
  }

  const onDone = () => {
    grantPermission(grantPermissionState)
      .then(response => {
        if ('data' in response) {
          AntdNotifier.success("Cấp quyền thành công");
          dispatch(toggleEidtState());
        } else {
          AntdNotifier.error("Có lỗi hệ thống");
          console.log(response)
        }
      })
      .catch(reason => {
        AntdNotifier.error("Có lỗi hệ thống");
        console.log(reason)
      })
  }

  const renderActions = () => {
    if (grantPermissionState.userId !== 0)
      return !grantPermissionState.isEditing ?
        <Space>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onEdit}
          >
            Chỉnh sửa
          </Button>
        </Space>
        :
        <Space>
          <Button
            danger
            type="primary"
            color="#73d13d"
            onClick={onCancel}
          >
            Hủy
          </Button>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onDone}
          >
            Xong
          </Button>
        </Space>;
    return <> </>;
  }
  return <>
    <ContentHeader
      title="Phân quyền"
      action={renderActions()}
    />
    <PermissionContent />
  </>
}

export default PermissionPage;