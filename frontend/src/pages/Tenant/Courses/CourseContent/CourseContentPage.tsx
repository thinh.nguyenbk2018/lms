import {useNavigate, useParams} from "react-router-dom";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import antdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import LoadingPage from "../../../UtilPages/LoadingPage";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";
import {Formik} from "formik";
import {DeleteOutlined} from "@ant-design/icons";
import {
    faArrowRightFromBracket,
    faBan,
    faBook,
    faBookOpen,
    faEye,
    faGripVertical,
    faPenToSquare,
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon, FontAwesomeIconProps,} from "@fortawesome/react-fontawesome";
import {Button, Collapse, Dropdown, Form, Modal, Row, Space, Switch, Tooltip,} from "antd";
import {addIconSvg} from "../../../../components/Icons/SvgIcons";
import NewActivityMenu, {QuizIcon, UnitIcon, VotingIcon,} from "../../../../components/TeachingContent/NewActivityMenu";
import {QuizItemForm, UnitItemForm,} from "../CourseDetail/Activity/ActivityWithForm";
import DraggableDroppableActivity, {
    packageActivityPlacementInfo,
} from "../CourseDetail/Activity/DraggableDroppableActivity";
import {
    ChapterPlacementInfo,
    DraggableAndDroppableChapter,
    getOrderValueFromSpecialPlacement,
} from "../CourseDetail/Chapter/DraggableChapter";
import {
    DEFAULT_CHAPTER_NAME,
    useChangeItemPlacementMutation,
    useChapterPlacementMutation,
    useCreateChapterMutation,
    useCreateQuizMutation,
    useCreateUnitMutation,
    useDeleteChapterMutation,
    useDeleteQuizMutation,
    useDeleteUnitMutation,
    useGetCourseContentQuery,
    useGetQuizDetailQuery,
    useGetUnitDetailQuery,
    useLazyGetCourseContentGroupByTextbooksQuery, useLazyGetUnitCourseGroupByTextbooksQuery,
    useUpdateChapterMutation,
    useUpdateQuizMutation,
    useUpdateUnitMutation,
} from "../../../../redux/features/Course/CourseAPI";
import {
    ActivityType,
    ChapterGroupByTextbook,
    TextbookUnit
} from "../../../../typescript/interfaces/courses/CourseTypes";
import {useAppDispatch, useAppSelector} from "../../../../hooks/Redux/hooks";
import {courseContentActions, courseContentSelectors,} from "../../../../redux/features/Course/CourseContentSlice";
import {DEFAULT_FRONTEND_ID, ID} from "../../../../redux/interfaces/types";
import {quizCourseSchema, quizSchema, unitSchema,} from "../CourseDetail/CreateOrUpdateCoursePage1";
import "./CourseContentPage.less";
import * as Yup from "yup";
import {FormItem, Input} from "formik-antd";
import {useDispatch} from "react-redux";
import {replacePathParams} from "../../../../utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import React, {useEffect, useState} from "react";
import ERROR_MESSAGE from "../../../../redux/features/ApiErrors";
import {teachingContentErrorHandler} from "../../Class/Content/ClassContentPage";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {mapType} from "@pages/UtilPages/MappingUtils";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {blue} from "@ant-design/colors";
import {classContentActions} from "@redux/features/Class/ClassContent/ClassContentSlice";

const {Panel} = Collapse;

export const AddQuizItemModal = () => {
    const {courseId} = useParams()

    const isAddingQuiz = useAppSelector(
        courseContentSelectors.selectIsAddingQuiz
    );

    const dispatch = useAppDispatch();

    const quizItemInEdit = useAppSelector(
        courseContentSelectors.selectQuizInUpdate
    );
    const chapterToUpdate = useAppSelector(
        courseContentSelectors.selectChapterInEdit
    );

    const [createQuiz] = useCreateQuizMutation();
    const [updateQuiz] = useUpdateQuizMutation();

    const defaultQuiz = {
        id: -1,
        title: "",
        description: "",
        tag: "",
        type: "quiz" as const,
        order: -1,
    }

    const [internalQuiz, setInternalQuiz] = useState(defaultQuiz);

    const {
        data: quizData,
        isSuccess,
        isFetching,
    } = useGetQuizDetailQuery(
        {
            courseId: Number(courseId),
            quizId: Number(quizItemInEdit?.id),
        },
        {
            skip:
                isNaN(Number(courseId)) ||
                isNaN(Number(quizItemInEdit?.id)),
        }
    );

    useEffect(() => {
        if (isAddingQuiz || !!quizItemInEdit) {
            if (quizItemInEdit != undefined && quizData != undefined) {
                setInternalQuiz(quizData)
            } else {
                setInternalQuiz(defaultQuiz);
            }
        }
    }, [quizData, isAddingQuiz]);

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={
                    internalQuiz
                }
                validateOnChange={false}
                validationSchema={quizCourseSchema}
                onSubmit={(values, {resetForm, setSubmitting}) => {
                    const courseToUpdate = Number(courseId);
                    if (isNaN(courseToUpdate) || !chapterToUpdate) {
                        AntdNotifier.error(
                            "Tạm thời không thêm chỉnh sửa nội dung được. Vui lòng thử lại sau hoặc liên hệ bộ phận bảo trì"
                        );
                        return;
                    }
                    if (quizItemInEdit) {
                        updateQuiz({
                            courseId: courseToUpdate,
                            quizItem: values,
                        }).unwrap()
                            .then(res => {
                                antdNotifier.success("Cập nhật bài kiểm tra thành công");
                                dispatch(courseContentActions.doneAddingActivity());
                                resetForm({values: defaultQuiz});
                                setSubmitting(false);
                            })
                            .catch(err => {
                                if (err?.data) {
                                    AntdNotifier.error(err?.data.message ? err?.data?.message : "Có lỗi trong quá trình cập nhật bài kiểm tra.")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình cập nhật bài kiểm tra.")
                                }
                                setSubmitting(false);
                            });
                    } else {
                        createQuiz({
                            courseId: courseToUpdate,
                            chapterId: chapterToUpdate,
                            quizItem: values,
                        }).unwrap()
                            .then(res => {
                                antdNotifier.success("Thêm bài kiểm tra thành công");
                                dispatch(courseContentActions.doneAddingActivity());
                                resetForm({values: defaultQuiz});
                                setSubmitting(false);
                            })
                            .catch(err => {
                                if (err?.data) {
                                    AntdNotifier.error(err?.data.message ? err?.data?.message : "Có lỗi trong quá trình thêm bài kiểm tra.")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình thêm bài kiểm tra.")
                                }
                                setSubmitting(false);
                            });
                    }
                }}
            >
                {({values, isSubmitting, errors, handleSubmit, submitForm, isValid, dirty, resetForm}) => {
                    return (
                        <Modal
                            title={!!quizItemInEdit ? "Cập nhật bài kiểm tra" : "Thêm bài kiểm tra"}
                            okText={!!quizItemInEdit ? "Cập nhật" : "Thêm bài kiểm tra"}
                            cancelText={"Hủy"}
                            visible={isAddingQuiz || !!quizItemInEdit}
                            onOk={() => {
                                handleSubmit();
                                // resetForm({values: defaultQuiz});
                            }}
                            okButtonProps={{
                                disabled: !dirty || isSubmitting,
                                loading: isSubmitting
                            }}
                            onCancel={() => {
                                dispatch(courseContentActions.doneAddingActivity());
                                resetForm({values: defaultQuiz});
                                setInternalQuiz(defaultQuiz);
                            }}
                            maskClosable={false}
                        >
                            <QuizItemForm
                                type={"quiz"}
                                courseIdToSearch={Number(courseId) || 0}
                                isUpdating={!!quizItemInEdit}
                            />
                            {/*<JsonDisplay cardProps={{ title: "Values" }} obj={values} />*/}
                            {/*<JsonDisplay cardProps={{ title: "Errors" }} obj={errors} />*/}
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

export const AddUnitItemModal = () => {
    const {courseId} = useParams();
    const isAddingUnit = useAppSelector(
        courseContentSelectors.selectIsAddingUnit
    );

    const unitItemInEdit = useAppSelector(
        courseContentSelectors.selectUnitInUpdate
    );

    const chapterToUpdate = useAppSelector(
        courseContentSelectors.selectChapterInEdit
    );
    const [createUnit] = useCreateUnitMutation();
    const [updateUnit] = useUpdateUnitMutation();

    const dispatch = useAppDispatch();

    const defaultUnit = {
        type: "unit" as const,
        title: "",
        textbooks: [] as TextbookUnit[],
        content: "",
        id: DEFAULT_FRONTEND_ID,
        order: -1,
        attachment: ""
    };

    const [internalUnit, setInternalUnit] = useState(defaultUnit);

    const {
        data: unitData,
        isSuccess,
        isFetching
    } = useGetUnitDetailQuery(
        {
            courseId: Number(courseId),
            unitId: Number(unitItemInEdit?.id)
        },
        {
            skip:
                isNaN(Number(unitItemInEdit?.id)) ||
                isNaN(Number(courseId)),
        }
    );

    const [fetchUnitCourseGroupByTextbook, {

    }] = useLazyGetUnitCourseGroupByTextbooksQuery();

    useEffect(() => {
        if (isAddingUnit || !!unitItemInEdit) {
            if (unitItemInEdit != undefined && unitData != undefined) {
                setInternalUnit(unitData)
            } else {
                setInternalUnit(defaultUnit);
            }
        }
    }, [unitData, isAddingUnit]);

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={
                    internalUnit
                }
                validateOnChange={false}
                validationSchema={unitSchema}
                onSubmit={(values, {resetForm}) => {
                    const courseToUpdate = Number(courseId);
                    if (isNaN(courseToUpdate) || !chapterToUpdate) {
                        AntdNotifier.error(
                            "Tạm thời không thêm unit được. Vui lòng thử lại sau hoặc liên hệ bộ phận bảo trì"
                        );
                        return;
                    }
                    if (unitItemInEdit) {
                        AntdNotifier.handlePromise(
                            updateUnit({
                                courseId: courseToUpdate,
                                unitItem: values,
                            }).unwrap(),
                            "Cập nhật bài học",
                            () => {
                                dispatch(courseContentActions.doneAddingActivity());
                                resetForm({values: defaultUnit});
                                if (courseId != undefined) {
                                    fetchUnitCourseGroupByTextbook({courseId: Number(courseId)});
                                }
                            },
                            (err) => {
                                teachingContentErrorHandler(err);
                                dispatch(courseContentActions.doneAddingActivity());
                            }
                        );
                    } else {
                        AntdNotifier.handlePromise(
                            createUnit({
                                courseId: courseToUpdate,
                                chapterId: chapterToUpdate,
                                unitItem: values,
                            }).unwrap(),
                            "Thêm bài học",
                            () => {
                                dispatch(courseContentActions.doneAddingActivity());
                                resetForm({values: defaultUnit});
                            },
                            (err) => {
                                teachingContentErrorHandler(err);
                                dispatch(courseContentActions.doneAddingActivity());
                            }
                        );
                    }
                }}
            >
                {({values, errors, handleSubmit, resetForm, dirty, isValid}) => {
                    return (
                        <Modal
                            bodyStyle={{ overflowY: 'auto', maxHeight: 'calc(100vh - 250px)' }}
                            title={!!unitItemInEdit ? 'Cập nhật bài học' : 'Thêm bài học'}
                            visible={isAddingUnit || !!unitItemInEdit}
                            onOk={() => {
                                handleSubmit();
                            }}
                            onCancel={() => {
                                dispatch(courseContentActions.doneAddingActivity());
                                resetForm({values: defaultUnit});
                                setInternalUnit(defaultUnit);
                            }}
                            wrapClassName={'content-page-modal-wrapper'}
                            maskClosable={false}
                            okText={!!unitItemInEdit ? 'Cập nhật' : 'Thêm bài học'}
                            cancelText={'Hủy'}
                            okButtonProps={{disabled: !dirty}}
                        >
                            <UnitItemForm type={"unit"}/>
                            {/*<JsonDisplay cardProps={{ title: "values" }} obj={values} />*/}
                            {/*<JsonDisplay cardProps={{ title: "values" }} obj={errors} />*/}
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

const EditChapterTitleModal = () => {
    const [updateChapter] = useUpdateChapterMutation();
    const {courseId} = useParams();
    const dispatch = useDispatch();
    const {isChapterInEdit, chapterTitle} = useAppSelector(
        courseContentSelectors.selectIsChapterInfoBeingEdited
    );
    const chapterIdInEdit = useAppSelector(
        courseContentSelectors.selectChapterInEdit
    );

    return (
        <Formik
            enableReinitialize={true}
            initialValues={{'title': chapterTitle}}
            validationSchema={Yup.object({
                title: Yup.string().required("Tên của chương là trường bắt buộc"),
            })}
            onSubmit={(values, {resetForm}) => {
                if (!chapterIdInEdit || isNaN(Number(courseId))) {
                    AntdNotifier.error(
                        "Tạm thời không update thông tin chapter được. Liên hệ bộ phận bảo trì"
                    );
                    return;
                }
                AntdNotifier.handlePromise(
                    updateChapter({
                        courseId: Number(courseId),
                        chapterId: chapterIdInEdit,
                        chapterInfo: {id: chapterIdInEdit, title: values.title},
                    }).unwrap(),
                    "Cập nhật chương",

                    () => {
                        dispatch(courseContentActions.doneEditingChapter());
                        resetForm({values: {'title': ''}});
                    },
                    (err) => {
                        teachingContentErrorHandler(err);
                        dispatch(courseContentActions.doneEditingChapter());
                    }
                );
            }}
        >
            {({handleSubmit}) => {
                return (
                    <Modal
                        title={"Tên chương"}
                        visible={isChapterInEdit}
                        onOk={() => {
                            handleSubmit();
                        }}
                        onCancel={() => {
                            dispatch(courseContentActions.doneEditingChapter());
                        }}
                        okText={'Cập nhật'}
                        cancelText={'Hủy'}
                    >
                        <Form>
                            <FormItem required={true} name="title" showValidateSuccess label={"Tiêu đề"}>
                                <Input name="title"/>
                            </FormItem>
                        </Form>
                    </Modal>
                );
            }}
        </Formik>
    );
};

export const RenderActivityIcon = React.memo(
    ({type}: { type: ActivityType }) => {
        switch (type) {
            case "quiz":
                return <QuizIcon/>;
            case "unit":
                return <UnitIcon/>;
            case "vote":
                return <VotingIcon/>;
            default:
                return <></>;
        }
    }
);

type FontAwesomeIconPropsRelaxed = Omit<FontAwesomeIconProps, "icon">;

export const PublishIcon = (props: FontAwesomeIconPropsRelaxed) => {
    return <FontAwesomeIcon {...props} icon={faArrowRightFromBracket}/>;
};

export const HideIcon = (props: FontAwesomeIconPropsRelaxed) => {
    return <FontAwesomeIcon {...props} icon={faBan}/>;
};

const CourseContentPage = () => {
    // * Class specif routes
    const {courseId} = useParams();
    // * RTK-Query
    const {
        forceCloseSidebar,
        courseDetail,
        isCourseDetailError,
        isCourseDetailFetching,
        isCourseDetailSuccess,
    } = useCourseSidebar();
    const navigate = useNavigate();

    const {
        data: courseContent,
        isSuccess,
        isLoading,
        isError,
        isFetching,
        isUninitialized,
        error,
    } = useGetCourseContentQuery(Number(courseId), {
        skip: isNaN(Number(courseId)),
    });

    const [deleteChapter] = useDeleteChapterMutation();
    const [createChatper] = useCreateChapterMutation();
    const [changeChapterPlacement] = useChapterPlacementMutation();

    // * Quiz
    const [deleteQuiz] = useDeleteQuizMutation();

    // * Unit
    const [deleteUnit] = useDeleteUnitMutation();

    const [changeItemPlacement] = useChangeItemPlacementMutation();

    const [textbookViewMode, setTextbookViewMode] = useState(false);

    const onExchangePlace = (
        from: ChapterPlacementInfo,
        to: ChapterPlacementInfo
    ) => {
        AntdNotifier.handlePromise(
            changeChapterPlacement({courseId: Number(courseId), from, to}).unwrap(),
            "Thay đổi vị trí chương",
            () => {
            },
            (err) => {
                // teachingContentErrorHandler(err);
                if (err.data.code) {
                    const code = err.data.code;
                    if (
                        code ===
                        ERROR_MESSAGE.COURSE_CONTENT
                            .DRAG_AND_DROP_IS_NOT_ALLOWED
                    ) {
                        console.log("HANDLING THE ERROR");
                        AntdNotifier.error(
                            "Bạn không thể kéo thay đổi  nội dung chương vì sẽ ảnh hưởng đến các lớp đang học. Bạn chỉ có thể thay đổi chương trình giảng dạy của các khóa học vừa tạo nhưng chưa tổ chức thành các lớp"
                        );
                    } else {
                        AntdNotifier.error(
                            "Không thể thay đổi nội dung của chương học. Liên hệ quản trị viên để xử lý"
                        );
                    }
                }
            }
        );
    };
    // * Redux store state
    const dispatch = useAppDispatch();
    const isAddingActivity = useAppSelector(
        courseContentSelectors.selectIsAddingQuiz
    );
    const isAddingUnit = useAppSelector(
        courseContentSelectors.selectIsAddingUnit
    );

    const onCreateDefaultChapter = () => {
        if (isNaN(Number(courseId))) {
            antdNotifier.error("Tạm thời không được thêm chương mới");
            return;
        }
        createChatper({
            courseId: Number(courseId),
            chapterInfo: {
                title: DEFAULT_CHAPTER_NAME,
            },
        })
        .unwrap()
        .then(res => antdNotifier.success("Thêm chương mới thành công"));
    };

    // * Add activity immediately, user can edit the tree later on
    const handleAddingActivity = (type: ActivityType, chapterId: ID) => {
        dispatch(
            courseContentActions.addingActivity({chapterToEdit: chapterId, type})
        );
    };

    if (!courseId || isNaN(Number(courseId))) {
        antdNotifier.error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
        return (
            <>
                <ApiErrorWithRetryButton onRetry={() => {
                }}/>
            </>
        );
    }

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const [fetchCourseContentGroupByTextbook, {
        data: courseContentGroupByTextbook,
        isLoading: courseContentGroupByTextbookLoading,
        isSuccess: courseContentGroupByTextbookSuccess
    }] = useLazyGetCourseContentGroupByTextbooksQuery();

    useEffect(() => {
        if (textbookViewMode && courseId) {
            fetchCourseContentGroupByTextbook(Number(courseId));
        }
    }, [textbookViewMode]);

    if (isCourseDetailSuccess && isSuccess) {
        return (
            <>
                <ContentHeader
                    title={
                        "Nội dung giảng dạy"
                    }
                    action={<>
                        <Switch
                            onClick={(checked: boolean) => setTextbookViewMode(checked)}
                            style={{marginRight: "1rem"}}
                            checked={textbookViewMode}
                        />
                        <span>Xem theo giáo trình</span>
                    </>}
                />
                {userInfo?.accountType === 'STAFF' &&
                    <div className={'class-content-instruction'}>*Các nội dung được cập nhật sẽ chỉ tác động lên các lớp có
                        trạng thái <i>Mới mở</i> hoặc <i>Đang diễn ra</i>.</div>}
                {!textbookViewMode && <div className={`course-content-container`}>
                    <>
                        {courseContent?.map((chapterItem, chapterIdx) => {
                            return (
                                <>
                                    <DraggableAndDroppableChapter
                                        key={chapterIdx}
                                        exchangePlace={onExchangePlace}
                                        addActivityCallback={(activity) => {
                                        }}
                                        id={chapterItem.id}
                                        order={chapterItem.order}
                                        receiveActivity={(item, specialPlacement) => {
                                            const {order, parentChapterIndex, originalIndex} = item;

                                            const from = packageActivityPlacementInfo(
                                                originalIndex,
                                                parentChapterIndex,
                                                order
                                            );

                                            const to = packageActivityPlacementInfo(
                                                undefined,
                                                chapterItem.id,
                                                specialPlacement
                                                    ? getOrderValueFromSpecialPlacement(specialPlacement)
                                                    : undefined
                                            );

                                            AntdNotifier.handlePromise(
                                                changeItemPlacement({
                                                    courseId: Number(courseId),
                                                    from,
                                                    to,
                                                }).unwrap(),
                                                "Thay đổi vị trí chương",
                                                () => {
                                                },
                                                (err) => {
                                                    console.log("HANDLING THE ERROR");
                                                    if (err.data.code) {
                                                        const code = err.data.code;
                                                        if (
                                                            code ===
                                                            ERROR_MESSAGE.COURSE_CONTENT
                                                                .DRAG_AND_DROP_IS_NOT_ALLOWED
                                                        ) {
                                                            console.log("HANDLING THE ERROR");
                                                            AntdNotifier.error(
                                                                "Bạn không thể kéo thay đổi  nội dung chương vì sẽ ảnh hưởng đến các lớp đang học. Bạn chỉ có thể thay đổi chương trình giảng dạy của các khóa học vừa tạo nhưng chưa tổ chức thành các lớp"
                                                            );
                                                        } else {
                                                            AntdNotifier.error(
                                                                "Không thể thay đổi nội dung của chương học. Liên hệ quản trị viên để xử lý"
                                                            );
                                                        }
                                                    }
                                                }
                                            );
                                        }}
                                        children={<></>}
                                        renderWithPreviewRef={(previewRef) => {
                                            return (
                                                <Collapse className="chapter-wrapper">
                                                    <Panel
                                                        className="chapter-panel"
                                                        showArrow={true}
                                                        key={chapterIdx}
                                                        extra={
                                                            <Space>
                                                                <Button
                                                                    onClick={(event) => {
                                                                        event.stopPropagation();
                                                                        // TODO: Add chapter delete form modal
                                                                        // renderConfirmDeleteModal();
                                                                    }}
                                                                    type="link"
                                                                    danger
                                                                >
                                                                    <div className="chapter-action-container">
                                                                        <Tooltip title={"Chỉnh sửa"}>
                                                                            <FontAwesomeIcon
                                                                                className={"edit-chapter-button"}
                                                                                icon={faPenToSquare}
                                                                                onClick={() => {
                                                                                    dispatch(
                                                                                        courseContentActions.editingChapter(
                                                                                            {
                                                                                                id: chapterItem.id,
                                                                                                title: chapterItem.title
                                                                                            }
                                                                                        )
                                                                                    );
                                                                                }}
                                                                            />
                                                                        </Tooltip>
                                                                        <DeleteOutlined
                                                                            onClick={() => {
                                                                                confirmationModal('chương', chapterItem.title, () => {
                                                                                    if (!courseDetail?.id) {
                                                                                        antdNotifier.error(
                                                                                            "Tam thời  không thể xóa chương. Không tìm thấy khóa"
                                                                                        );

                                                                                        return;
                                                                                    }
                                                                                    AntdNotifier.handlePromise(
                                                                                        deleteChapter({
                                                                                            courseId: courseDetail.id,
                                                                                            chapterId: chapterItem.id,
                                                                                        }).unwrap(),
                                                                                        "Xóa chương"
                                                                                    );
                                                                                });
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </Button>
                                                            </Space>
                                                        }
                                                        header={
                                                            <>
                                                                <div
                                                                    ref={previewRef}
                                                                    className="chapter-drag-grip-container"
                                                                >
                                                                    <FontAwesomeIcon
                                                                        className="chapter-drag-grip"
                                                                        icon={faGripVertical}
                                                                    />
                                                                </div>
                                                                <p className={"chapter-title"}>
                                                                    {chapterItem.title}
                                                                </p>
                                                            </>
                                                        }
                                                    >
                                                        <>
                                                            <div className="activity-list-container">
                                                                {chapterItem.activities.map((item, index) => {
                                                                    const namespace = `chapters[${chapterIdx}].activities[${index}]`;

                                                                    return (
                                                                        <>
                                                                            <DraggableDroppableActivity
                                                                                type={item.type}
                                                                                order={item.order}
                                                                                id={item.id}
                                                                                chapterId={chapterItem.id}
                                                                                exchangeHandler={(from, to) => {
                                                                                    AntdNotifier.handlePromise(
                                                                                        changeItemPlacement({
                                                                                            courseId: Number(courseId),
                                                                                            from,
                                                                                            to,
                                                                                        }).unwrap(),
                                                                                        "Thay đổi vị trí chương",
                                                                                        () => {
                                                                                        },
                                                                                        (err) => {
                                                                                            console.log("HANDLING THE ERROR");
                                                                                            if (err.data.code) {
                                                                                                const code = err.data.code;
                                                                                                if (
                                                                                                    code ===
                                                                                                    ERROR_MESSAGE.COURSE_CONTENT
                                                                                                        .DRAG_AND_DROP_IS_NOT_ALLOWED
                                                                                                ) {
                                                                                                    console.log(
                                                                                                        "HANDLING THE ERROR"
                                                                                                    );
                                                                                                    AntdNotifier.error(
                                                                                                        "Bạn không thể kéo thay đổi nội dung chương học vì sẽ ảnh hưởng đến các lớp đang học. Bạn chỉ có thể thay đổi chương trình giảng dạy của các khóa học vừa tạo nhưng chưa tổ chức thành các lớp"
                                                                                                    );
                                                                                                } else {
                                                                                                    AntdNotifier.error(
                                                                                                        "Không thể thay đổi nội dung của chương học. Liên hệ quản trị viên để xử lý"
                                                                                                    );
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    );
                                                                                }}
                                                                            >
                                                                                <div
                                                                                    className={"activity-item-container"}
                                                                                >
                                                                                    <Space>
                                                                                        <RenderActivityIcon
                                                                                            type={item.type}
                                                                                        />
                                                                                        <p className={"activity-title"}>
                                                                                            {item.title}
                                                                                        </p>
                                                                                    </Space>
                                                                                    <div className={"action-container"}>
                                                                                        <Tooltip title={"Chi tiết"}>
                                                                                            <FontAwesomeIcon
                                                                                                onClick={() => {
                                                                                                    switch (item.type) {
                                                                                                        case "quiz":
                                                                                                            navigate(
                                                                                                                replacePathParams(
                                                                                                                    ROUTING_CONSTANTS.COURSE_QUIZ_ITEM_DETAIL_PAGE,
                                                                                                                    [
                                                                                                                        ":courseId",
                                                                                                                        Number(courseId),
                                                                                                                    ],
                                                                                                                    [
                                                                                                                        ":chapterId",
                                                                                                                        chapterItem.id,
                                                                                                                    ],
                                                                                                                    [
                                                                                                                        ":quizItemId",
                                                                                                                        item.id,
                                                                                                                    ]
                                                                                                                )
                                                                                                            );
                                                                                                            return;
                                                                                                        case "unit":
                                                                                                            navigate(
                                                                                                                replacePathParams(
                                                                                                                    ROUTING_CONSTANTS.COURSE_UNIT_ITEM_DETAIL_PAGE,
                                                                                                                    [
                                                                                                                        ":courseId",
                                                                                                                        Number(courseId),
                                                                                                                    ],
                                                                                                                    [
                                                                                                                        ":chapterId",
                                                                                                                        chapterItem.id,
                                                                                                                    ],
                                                                                                                    [
                                                                                                                        ":unitItemId",
                                                                                                                        item.id,
                                                                                                                    ]
                                                                                                                )
                                                                                                            );
                                                                                                            return;
                                                                                                        default:
                                                                                                            return;
                                                                                                    }
                                                                                                }}
                                                                                                className={"icon detail action-icon-activity"}
                                                                                                icon={faEye}
                                                                                            />
                                                                                        </Tooltip>
                                                                                        <Tooltip title={"Chỉnh sửa"}>
                                                                                            <FontAwesomeIcon
                                                                                                className={"icon edit action-icon-activity"}
                                                                                                icon={faPenToSquare}
                                                                                                onClick={() => {
                                                                                                    dispatch(
                                                                                                        courseContentActions.updatingActivity(
                                                                                                            {
                                                                                                                chapterToEdit:
                                                                                                                chapterItem.id,
                                                                                                                activity: item,
                                                                                                            }
                                                                                                        )
                                                                                                    );
                                                                                                }}
                                                                                            />
                                                                                        </Tooltip>
                                                                                        <Tooltip title={"Xóa"}>
                                                                                            <DeleteOutlined
                                                                                                onClick={() => {
                                                                                                    confirmationModal(mapType(item.type), item.title, () => {
                                                                                                        switch (item.type) {
                                                                                                            case "quiz":
                                                                                                                AntdNotifier.handlePromise(
                                                                                                                    deleteQuiz({
                                                                                                                        courseId:
                                                                                                                            Number(courseId),
                                                                                                                        quizId: item.id,
                                                                                                                    }).unwrap(),
                                                                                                                    "Xóa bài kiểm tra",
                                                                                                                    () => {
                                                                                                                    },
                                                                                                                    (err) => {
                                                                                                                        teachingContentErrorHandler(
                                                                                                                            err
                                                                                                                        );
                                                                                                                    }
                                                                                                                );
                                                                                                                return;
                                                                                                            case "unit":
                                                                                                                AntdNotifier.handlePromise(
                                                                                                                    deleteUnit({
                                                                                                                        courseId:
                                                                                                                            Number(courseId),
                                                                                                                        unitId: item.id,
                                                                                                                    }).unwrap(),
                                                                                                                    "Xóa bài học",
                                                                                                                    () => {
                                                                                                                    },
                                                                                                                    (err) => {
                                                                                                                        teachingContentErrorHandler(
                                                                                                                            err
                                                                                                                        );
                                                                                                                    }
                                                                                                                );
                                                                                                                return;
                                                                                                            default:
                                                                                                                return;
                                                                                                        }
                                                                                                    });
                                                                                                }}
                                                                                                className={"icon delete action-icon-activity"}
                                                                                            />
                                                                                        </Tooltip>
                                                                                    </div>
                                                                                </div>
                                                                            </DraggableDroppableActivity>
                                                                        </>
                                                                    );
                                                                })}
                                                            </div>
                                                            <Row
                                                                justify="center"
                                                                style={{margin: "1.3em 2em"}}
                                                            >
                                                                <Dropdown
                                                                    overlay={
                                                                        <NewActivityMenu
                                                                            onChoose={(key) => {
                                                                                handleAddingActivity(
                                                                                    key,
                                                                                    chapterItem.id
                                                                                );
                                                                            }}
                                                                        />
                                                                    }
                                                                >
                                                                    <Button
                                                                        className={"button-with-icon"}
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                        }}
                                                                        icon={addIconSvg}
                                                                        type="default"
                                                                    >
                                                                        Thêm hoạt động
                                                                    </Button>
                                                                </Dropdown>
                                                            </Row>
                                                        </>
                                                    </Panel>
                                                </Collapse>
                                            );
                                        }}
                                    />
                                </>
                            );
                        })}
                        <Row justify="center" style={{margin: "1em 1em"}}>
                            <Button
                                className="add-button"
                                onClick={() => {
                                    onCreateDefaultChapter();
                                }}
                                icon={
                                    <FontAwesomeIcon
                                        style={{margin: "0 .3em"}}
                                        icon={faBookOpen}
                                    />
                                }
                                type={"primary"}
                            >
                                Thêm chương mới
                            </Button>
                        </Row>
                        {/* // * Activity Adding model */}
                        <AddQuizItemModal/>
                        <AddUnitItemModal/>
                        <EditChapterTitleModal/>
                    </>
                </div>}
                {textbookViewMode && <>
                    {courseContentGroupByTextbook?.map((textbook: ChapterGroupByTextbook, index: number) => {
                        return <Collapse
                                         className={'textbook-wrapper course-content-group-by-textbook-wrapper course-content-container'}>
                            <Panel
                                header={<>
                                    <FontAwesomeIcon
                                        icon={faBook}
                                        style={{margin: "0 .5em 0 0", color: blue.primary}}
                                    />
                                    <p className={"chapter-title"}>
                                        {textbook.textbookName}
                                    </p></>
                                }
                                key={textbook.textbookId}
                            >
                                {textbook.chapters?.map((chapterItem, chapterIdx) => {
                                    return (
                                        <>
                                            <DraggableAndDroppableChapter
                                                key={chapterIdx}
                                                exchangePlace={onExchangePlace}
                                                addActivityCallback={(activity) => {
                                                }}
                                                id={chapterItem.id}
                                                order={chapterItem.order}
                                                receiveActivity={(item, specialPlacement) => {
                                                    // do not allow drag and drop
                                                }}
                                                children={<></>}
                                                renderWithPreviewRef={(previewRef) => {
                                                    return (
                                                        <Collapse className="chapter-wrapper">
                                                            <Panel
                                                                className="chapter-panel-group-by-textbook"
                                                                showArrow={true}
                                                                key={chapterIdx}
                                                                extra={
                                                                    <Space>
                                                                        <Button
                                                                            onClick={(event) => {
                                                                                event.stopPropagation();
                                                                                // TODO: Add chapter delete form modal
                                                                                // renderConfirmDeleteModal();
                                                                            }}
                                                                            type="link"
                                                                            danger
                                                                        >
                                                                            <div className="chapter-action-container">
                                                                                <Tooltip title={"Chỉnh sửa"}>
                                                                                    <FontAwesomeIcon
                                                                                        className={"edit-chapter-button"}
                                                                                        icon={faPenToSquare}
                                                                                        onClick={() => {
                                                                                            dispatch(
                                                                                                courseContentActions.editingChapter(
                                                                                                    {
                                                                                                        id: chapterItem.id,
                                                                                                        title: chapterItem.title
                                                                                                    }
                                                                                                )
                                                                                            );
                                                                                        }}
                                                                                    />
                                                                                </Tooltip>
                                                                                <DeleteOutlined
                                                                                    onClick={() => {
                                                                                        confirmationModal('chương', chapterItem.title, () => {
                                                                                            if (!courseDetail?.id) {
                                                                                                antdNotifier.error(
                                                                                                    "Tam thời  không thể xóa chương. Không tìm thấy khóa"
                                                                                                );

                                                                                                return;
                                                                                            }
                                                                                            AntdNotifier.handlePromise(
                                                                                                deleteChapter({
                                                                                                    courseId: courseDetail.id,
                                                                                                    chapterId: chapterItem.id,
                                                                                                }).unwrap(),
                                                                                                "Xóa chương"
                                                                                            );
                                                                                        });
                                                                                    }}
                                                                                />
                                                                            </div>
                                                                        </Button>
                                                                    </Space>
                                                                }
                                                                header={
                                                                    <>
                                                                        <p className={"chapter-title"}>
                                                                            {chapterItem.title}
                                                                        </p>
                                                                    </>
                                                                }
                                                            >
                                                                <>
                                                                    <div className="activity-list-container">
                                                                        {chapterItem.activities.map((item, index) => {
                                                                            return (
                                                                                <>

                                                                                    <div
                                                                                        className={"activity-item-container"}
                                                                                    >
                                                                                        <Space>
                                                                                            <RenderActivityIcon
                                                                                                type={item.type}
                                                                                            />
                                                                                            <p className={"activity-title"}>
                                                                                                {item.title}
                                                                                            </p>
                                                                                        </Space>
                                                                                        <div
                                                                                            className={"action-container"}>
                                                                                            <Tooltip title={"Chi tiết"}>
                                                                                                <FontAwesomeIcon
                                                                                                    onClick={() => {
                                                                                                        switch (item.type) {
                                                                                                            case "quiz":
                                                                                                                navigate(
                                                                                                                    replacePathParams(
                                                                                                                        ROUTING_CONSTANTS.COURSE_QUIZ_ITEM_DETAIL_PAGE,
                                                                                                                        [
                                                                                                                            ":courseId",
                                                                                                                            Number(courseId),
                                                                                                                        ],
                                                                                                                        [
                                                                                                                            ":chapterId",
                                                                                                                            chapterItem.id,
                                                                                                                        ],
                                                                                                                        [
                                                                                                                            ":quizItemId",
                                                                                                                            item.id,
                                                                                                                        ]
                                                                                                                    )
                                                                                                                );
                                                                                                                return;
                                                                                                            case "unit":
                                                                                                                navigate(
                                                                                                                    replacePathParams(
                                                                                                                        ROUTING_CONSTANTS.COURSE_UNIT_ITEM_DETAIL_PAGE,
                                                                                                                        [
                                                                                                                            ":courseId",
                                                                                                                            Number(courseId),
                                                                                                                        ],
                                                                                                                        [
                                                                                                                            ":chapterId",
                                                                                                                            chapterItem.id,
                                                                                                                        ],
                                                                                                                        [
                                                                                                                            ":unitItemId",
                                                                                                                            item.id,
                                                                                                                        ]
                                                                                                                    )
                                                                                                                );
                                                                                                                return;
                                                                                                            default:
                                                                                                                return;
                                                                                                        }
                                                                                                    }}
                                                                                                    className={"icon detail action-icon-activity"}
                                                                                                    icon={faEye}
                                                                                                />
                                                                                            </Tooltip>
                                                                                            <Tooltip
                                                                                                title={"Chỉnh sửa"}>
                                                                                                <FontAwesomeIcon
                                                                                                    className={"icon edit action-icon-activity"}
                                                                                                    icon={faPenToSquare}
                                                                                                    onClick={() => {
                                                                                                        dispatch(
                                                                                                            courseContentActions.updatingActivity(
                                                                                                                {
                                                                                                                    chapterToEdit:
                                                                                                                    chapterItem.id,
                                                                                                                    activity: item,
                                                                                                                }
                                                                                                            )
                                                                                                        );
                                                                                                    }}
                                                                                                />
                                                                                            </Tooltip>
                                                                                            <Tooltip title={"Xóa"}>
                                                                                                <DeleteOutlined
                                                                                                    onClick={() => {
                                                                                                        confirmationModal(mapType(item.type), item.title, () => {
                                                                                                            switch (item.type) {
                                                                                                                case "quiz":
                                                                                                                    AntdNotifier.handlePromise(
                                                                                                                        deleteQuiz({
                                                                                                                            courseId:
                                                                                                                                Number(courseId),
                                                                                                                            quizId: item.id,
                                                                                                                        }).unwrap(),
                                                                                                                        "Xóa bài kiểm tra",
                                                                                                                        () => {
                                                                                                                        },
                                                                                                                        (err) => {
                                                                                                                            teachingContentErrorHandler(
                                                                                                                                err
                                                                                                                            );
                                                                                                                        }
                                                                                                                    );
                                                                                                                    return;
                                                                                                                case "unit":
                                                                                                                    AntdNotifier.handlePromise(
                                                                                                                        deleteUnit({
                                                                                                                            courseId:
                                                                                                                                Number(courseId),
                                                                                                                            unitId: item.id,
                                                                                                                        }).unwrap(),
                                                                                                                        "Xóa bài học",
                                                                                                                        () => {
                                                                                                                        },
                                                                                                                        (err) => {
                                                                                                                            teachingContentErrorHandler(
                                                                                                                                err
                                                                                                                            );
                                                                                                                        }
                                                                                                                    );
                                                                                                                    return;
                                                                                                                default:
                                                                                                                    return;
                                                                                                            }
                                                                                                        });
                                                                                                    }}
                                                                                                    className={"icon delete action-icon-activity"}
                                                                                                />
                                                                                            </Tooltip>
                                                                                        </div>
                                                                                    </div>
                                                                                </>
                                                                            );
                                                                        })}
                                                                    </div>
                                                                </>
                                                            </Panel>
                                                        </Collapse>
                                                    );
                                                }}
                                            />
                                        </>
                                    );
                                })}
                                {textbook.chapters.length == 0 ? <div style={{padding: '1rem'}}>Chưa có bài học hay bài kiểm tra nào sử dụng giáo trình này</div> : ''}
                            </Panel>
                        </Collapse>
                    })}
                    <EditChapterTitleModal/>
                    <AddQuizItemModal/>
                    <AddUnitItemModal/>
                </>}
            </>
        );
    } else if (isCourseDetailFetching || courseContentGroupByTextbookLoading) {
        return <LoadingPage/>;
    } else return <></>;
};
export default CourseContentPage;
