import {apiSlice} from "../CentralAPI";
import {SearchCriteria} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {Class, ClassListDto} from "./ClassType";
import {transformSearchQuery} from "../APIUtils/TransformParameter";
import {
	Chapter,
	ChapterDto,
	ChapterGroupByTextbook,
	ChapterGroupByTextbookDto,
	QuizItem,
	UnitGroupByTextbook,
	UnitItem,
} from "../../../typescript/interfaces/courses/CourseTypes";
import {transformChapterDtoToChapter, transformChapterDtoToChapterGroupByTextbook} from "../Course/CourseAPI";
import {ChapterPlacementInfo} from "../../../pages/Tenant/Courses/CourseDetail/Chapter/DraggableChapter";
import {ActivityPlacementInfo} from "../../../pages/Tenant/Courses/CourseDetail/Activity/DraggableDroppableActivity";
import {ID} from "../../interfaces/types";
import {DatePresentation} from "../../../components/Calendar/Utils/CalendarUtils";
import {UserInfoForClickNavigation} from "../Auth/AuthSlice";
import {TestConfig} from "../Quiz/examAPI";
import {TextBook} from "@redux/features/Textbook/TextbookType";

export type Voting = {
	id: number;
	title: string;
	description: string;
	isAllowedToAddChoice: boolean;
	choices: VotingOption[];
	isFromCourse?: boolean;
	type: "vote";
	createdAt?: DatePresentation;
	createdBy?: UserInfoForClickNavigation;
	updatedAt?: DatePresentation;
	updatedBy?: UserInfoForClickNavigation;
};
export type VotingOption = {
	id?: number;
	content: string;
	percent: string;
	numbersOfChosen: number;
	createdAt?: DatePresentation;
	createdBy?: UserInfoForClickNavigation;
	updatedAt?: DatePresentation;
	updatedBy?: UserInfoForClickNavigation;
};
const classApiInvalidatesTags = apiSlice.enhanceEndpoints({
	addTagTypes: ["Classes", "ClassContent", "ClassVoting", "ClassTextbook", "ClassUnits"],
});

const classAPI = classApiInvalidatesTags.injectEndpoints({
	endpoints: (build) => ({
		// * Class list : For table view
		getClassListPaginated: build.query<ClassListDto, SearchCriteria>({
			query: (criteria) => `classes/?${transformSearchQuery(criteria)}`,
			providesTags: (result) =>
				// is result available?
				result
					? // successful query
					  [
							...result.listData.map(
								({ id }) => ({ type: "Classes", id } as const)
							),
							{ type: "Classes", id: "LIST" },
					  ]
					: // an error occurred, but we still want to refetch this query when `{ type: 'Posts', id: 'LIST' }` is invalidated
					  [{ type: "Classes", id: "LIST" }],
		}),

		//* Class Information
		getClassInformation: build.query<Class, number>({
			query: (id) => `classes/${id}`,
			providesTags: (result) => [{ type: "Classes", id: result?.id }],
		}),
		createClass: build.mutation<Class, Partial<Class>>({
			query(body) {
				return {
					url: `classes`,
					method: "POST",
					body,
				};
			},
			invalidatesTags: [{ type: "Classes", id: "LIST" }],
		}),
		updateClassInformation: build.mutation<
			Class,
			{ classInfo: Class; classId: number }
		>({
			query({ classInfo, classId }) {
				return {
					url: `classes/${classId}`,
					method: "PUT",
					body: classInfo,
				};
			},
			invalidatesTags: [ "Classes"]
		}),
		deleteClass: build.mutation<number, number>({
			query(id) {
				return {
					url: `classes/${id}`,
					method: "DELETE",
				};
			},
			invalidatesTags: (_result, _error, args) => [{ type: "Classes", id: args }],
		}),

		// * Class Content
		getClassContent: build.query<Chapter[], number>({
			query: (classId) => {
				return {
					url: `classes/${classId}/learning_content`,
					method: "GET",
				};
			},
			transformResponse: (res: ChapterDto[], _meta, _args) => {
				return res
					.map(transformChapterDtoToChapter)
					.slice(0)
					.sort((a, b) => a.order - b.order);
			},
			providesTags: (_res, _err, args) => [{ type: "ClassContent", id: args }],
		}),

		getClassContentGroupByTextbooks: build.query<ChapterGroupByTextbook[], number>({
			query: (classId) => {
				return {
					url: `classes/${classId}/learning_content/textbooks`,
					method: "GET",
				};
			},
			transformResponse: (res: ChapterGroupByTextbookDto[], _meta, _args) => {
				return res.map(transformChapterDtoToChapterGroupByTextbook);
			},
			providesTags: (_res, _err, args) => [{type: "ClassContent", id: args}],
		}),

		// ***** Add chapter and activity to Class Content
		// Chapter: Only need the title
		createClassChapter: build.mutation<
			Chapter,
			{ classId: number; chapterInfo: Partial<Chapter> }
		>({
			query: ({ classId, chapterInfo }) => {
				return {
					url: `classes/${classId}/learning_content/chapters`,
					method: "POST",
					body: chapterInfo,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),

		updateClassChapter: build.mutation<
			Chapter,
			{ classId: number; chapterId: number; chapterInfo: Partial<Chapter> }
		>({
			query: ({ classId, chapterId, chapterInfo }) => {
				return {
					url: `classes/${classId}/learning_content/chapters/${chapterId}`,
					method: "PUT",
					body: chapterInfo,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),

		deleteClassChapter: build.mutation<
			number,
			{ classId: number; chapterId: number }
		>({
			query: ({ classId, chapterId }) => {
				return {
					url: `classes/${classId}/learning_content/chapters/${chapterId}`,
					method: "DELETE",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),

		// ** Item placement
		classChapterPlacement: build.mutation<
			void,
			{
				classId: number;
				from: ChapterPlacementInfo;
				to: ChapterPlacementInfo;
			}
		>({
			query: ({ classId, from, to }) => {
				return {
					url: `classes/${classId}/learning_content/reorder/chapters`,
					method: "PUT",
					body: {
						from,
						to,
					},
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),

		// ? Unit
		getClassUnitItems: build.query<UnitItem[], { classId: number }>({
			query: ({ classId }) => `classes/${classId}/learning_content/units`,
			providesTags: [{ type: "ClassUnits", id: "LIST" }],
		}),
		getClassUnitDetail: build.query<
			UnitItem,
			{ classId: number; unitId: number }
		>({
			query: ({ classId, unitId }) => {
				return {
					url: `classes/${classId}/learning_content/units/${unitId}`,
					method: "GET",
				};
			},
			transformResponse: (res: any, _meta, _args) => {
				return {
					...res,
					type: "unit" as const,
				};
			},
			providesTags: [{ type: "ClassContent", id: "UNIT_DETAIL" }],
		}),
		createClassUnit: build.mutation<
			UnitItem,
			{ classId: number; chapterId: number; unitItem: Partial<UnitItem> }
		>({
			query: ({ classId, chapterId, unitItem }) => {
				return {
					url: `classes/${classId}/learning_content/chapters/${chapterId}/units`,
					method: "POST",
					body: unitItem,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),
		updateClassUnit: build.mutation<
			UnitItem,
			{ classId: number; unitItem: Partial<UnitItem> }
		>({
			query: ({ classId, unitItem }) => {
				return {
					url: `classes/${classId}/learning_content/units/${unitItem.id}`,
					method: "PUT",
					body: unitItem,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "UNIT_DETAIL" },
				{ type: "ClassUnits", id: "LIST"}
			],
		}),
		publishUnit: build.mutation<
			QuizItem,
			{ classId: number; unitId: number }
		>({
			query: ({ classId, unitId }) => {
				return {
					url: `classes/${classId}/learning_content/units/${unitId}/publish`,
					method: "POST",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "UNIT_DETAIL" },
			],
		}),
		hideUnit: build.mutation<
			QuizItem,
			{ classId: number; unitId: number }
		>({
			query: ({ classId, unitId }) => {
				return {
					url: `classes/${classId}/learning_content/units/${unitId}/hide`,
					method: "POST",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "UNIT_DETAIL" },
			],
		}),
		deleteClassUnit: build.mutation<
			void,
			{ classId: number; unitId: number }
		>({
			query: ({ classId, unitId }) => {
				return {
					url: `classes/${classId}/learning_content/units/${unitId}`,
					method: "DELETE",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassUnits", id: "LIST"}
				// { type: "ClassContent", id:UNIT_DETAIL" },
			],
		}),
		// ? Quiz
		getClassQuizItems: build.query<QuizItem[], { classId: number }>({
			query: ({ classId }) => `classes/${classId}/learning_content/quizzes`,
			providesTags: [{ type: "ClassContent", id: "QUIZ_ITEM_LIST" }],
		}),
		getClassQuizDetail: build.query<
			QuizItem,
			{ classId: number; quizId: number }
		>({
			query: ({ classId, quizId }) => {
				return {
					url: `classes/${classId}/learning_content/quizzes/${quizId}`,
					method: "GET",
				};
			},
			transformResponse: (res: any, _meta, _args) => {
				return {
					...res,
					type: "quiz" as const,
				};
			},
			providesTags: [{ type: "ClassContent", id: "QUIZ_DETAIL" }],
		}),
		createClassQuiz: build.mutation<
			QuizItem,
			{ classId: number; chapterId: number; quizItem: Partial<QuizItem> }
		>({
			query: ({ classId, chapterId, quizItem }) => {
				return {
					url: `classes/${classId}/learning_content/chapters/${chapterId}/quizzes`,
					method: "POST",
					body: quizItem,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),
		updateClassQuiz: build.mutation<
			QuizItem,
			{ classId: number; quizItem: Partial<QuizItem> }
		>({
			query: ({ classId, quizItem }) => {
				return {
					url: `classes/${classId}/learning_content/quizzes/${quizItem.id}`,
					method: "PUT",
					body: quizItem,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "QUIZ_DETAIL" },
			],
		}),
		deleteClassQuiz: build.mutation<
			void,
			{ classId: number; quizId: number }
		>({
			query: ({ classId, quizId }) => {
				return {
					url: `classes/${classId}/learning_content/quizzes/${quizId}`,
					method: "DELETE",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "QUIZ_DETAIL" },
				{ type: "ClassContent", id: "QUIZ_ITEM_LIST"}
			],
		}),
		configQuiz: build.mutation<
			QuizItem,
			{
				classId: number;
				quizId: number;
				config: Partial<TestConfig>;
			}
		>({
			query: ({ classId, quizId, config }) => {
				return {
					url: `classes/${classId}/learning_content/quizzes/${quizId}/config`,
					method: "POST",
					body: config,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "QUIZ_DETAIL" },
			],
		}),
		publishQuiz: build.mutation<
			QuizItem,
			{ classId: number; quizId: number }
		>({
			query: ({ classId, quizId }) => {
				return {
					url: `classes/${classId}/learning_content/quizzes/${quizId}/publish`,
					method: "POST",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "QUIZ_DETAIL" },
			],
		}),
		hideQuiz: build.mutation<
			QuizItem,
			{ classId: number; quizId: number }
		>({
			query: ({ classId, quizId }) => {
				return {
					url: `classes/${classId}/learning_content/quizzes/${quizId}/hide`,
					method: "POST",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
				{ type: "ClassContent", id: "QUIZ_DETAIL" },
			],
		}),
		// * Handle item placement
		changeClassItemPlacement: build.mutation<
			Chapter[],
			{
				classId: number;
				from: ActivityPlacementInfo;
				to: ActivityPlacementInfo;
			}
		>({
			query: ({ classId, from, to }) => {
				return {
					url: `classes/${classId}/learning_content/reorder/actions`,
					method: "PUT",
					body: {
						from,
						to,
					},
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassContent", id: args.classId },
			],
		}),
		// * Class Voting
		getClassVotingDetail: build.query<
			Voting,
			{ classId: number; votingId: number }
		>({
			query: ({ classId, votingId }) => {
				return {
					url: `classes/${classId}/learning_content/voting/${votingId}`,
					method: "GET",
				};
			},
			providesTags: (_res, _err, args) => [
				{ type: "ClassVoting", id: args.votingId },
			],
		}),
		createClassVoting: build.mutation<
			Partial<Voting>,
			{ classId: ID; chapterId: ID; votingInfo: Partial<Voting> }
		>({
			query: ({ classId, chapterId, votingInfo }) => {
				return {
					url: `classes/${classId}/learning_content/chapters/${chapterId}/voting`,
					method: "POST",
					body: votingInfo,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassVoting" },
				{ type: "ClassContent", id: args.classId },
			],
		}),
		updateClassVoting: build.mutation<
			Partial<Voting>,
			{
				votingId: number;
				classId: number;
				votingInfo: Partial<Voting>;
			}
		>({
			query: ({ votingId, classId, votingInfo }) => ({
				url: `classes/${classId}/learning_content/voting/${votingId}`,
				method: "PUT",
				body: votingInfo,
			}),
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassVoting", id: args.votingId },
			],
		}),
		deleteClassVoting: build.mutation<
			Partial<Voting>,
			{ classId: number; votingId: number }
		>({
			query: ({ classId, votingId }) => ({
				url: `classes/${classId}/learning_content/voting/${votingId}`,
				method: "DELETE",
			}),
			invalidatesTags: (_res, _err, _args) => [{ type: "ClassVoting" }, "ClassContent"],
		}),
		// * Voting choice related endpoints
		makeVotingChoice: build.mutation<
			string,
			{ classId: number; votingId: number; choiceId: number }
		>({
			query: ({ classId, votingId, choiceId }) => {
				return {
					url: `classes/${classId}/learning_content/voting/${votingId}/choices/${choiceId}`,
					method: "PATCH",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassVoting", id: args.votingId },
			],
		}),
		changeChoiceContent: build.mutation<
			string,
			{
				classId: number;
				votingId: number;
				choiceId: number;
				votingInfo: Partial<Voting>;
			}
		>({
			query: ({ classId, votingId, choiceId, votingInfo }) => {
				return {
					url: `classes/${classId}/learning_content/voting/${votingId}/choices/${choiceId}`,
					method: "PUT",
					body: votingInfo,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassVoting", id: args.votingId },
			],
		}),

		deleteVotingChoice: build.mutation<
			void,
			{ classId: number; votingId: number; choiceId: number }
		>({
			query: ({ classId, votingId, choiceId }) => {
				return {
					url: `classes/${classId}/learning_content/voting/${votingId}/choices/${choiceId}`,
					method: "DELETE",
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassVoting", id: args.votingId },
			],
		}),
		addVotingOption: build.mutation<
			void,
			{
				classId: number;
				votingId: number;
				votingOption: Partial<VotingOption>;
			}
		>({
			query: ({ classId, votingId, votingOption }) => {
				return {
					url: `classes/${classId}/learning_content/voting/${votingId}/choices`,
					method: "POST",
					body: votingOption,
				};
			},
			invalidatesTags: (_res, _err, args) => [
				{ type: "ClassVoting", id: args.votingId },
			],
		}),

		// * Class Member
		getMembers: build.query<number, number>({
			query: (id) => `classes/${id}/members`,
		}),
		getTextbooksInClass: build.query<TextBook[], { classId: number, keyword: string }>({
			query: ({classId, keyword}) => `classes/${classId}/resources/textbooks?keyword=${keyword}`,
			providesTags: [{type: "ClassTextbook", id: "LIST"}],
		}),
		countTextbooksInClass: build.query<number, { classId: number }>({
			query: ({classId}) => `classes/${classId}/resources/textbooks/count`
		}),
		getUnitClassGroupByTextbooks: build.query<UnitGroupByTextbook[], { classId: number }>({
			query: ({ classId }) => `classes/${classId}/learning_content/units/textbooks`,
			providesTags: [{ type: "ClassUnits", id: "LIST" }],
		}),
		refetchClassInformation: build.mutation<any,  {classId: number}>({
			queryFn: ({classId: number}) => ({ data: null }),
			invalidatesTags: (_res, _err, args) => [{type: "Classes", id: args.classId}]
		})
	}),
	overrideExisting: false,
});

export const {
	// * Class List
	useGetClassListPaginatedQuery,

	// * Class item
	useGetClassInformationQuery,
	useCreateClassMutation,
	useUpdateClassInformationMutation,
	useDeleteClassMutation,

	// * Class Content
	useGetClassContentQuery,
	// ? Chapter
	useCreateClassChapterMutation,
	useUpdateClassChapterMutation,
	useDeleteClassChapterMutation,

	useClassChapterPlacementMutation,
	// ? Unit

	useGetClassUnitDetailQuery,
	useCreateClassUnitMutation,
	useUpdateClassUnitMutation,
	useDeleteClassUnitMutation,
	usePublishUnitMutation,
	useHideUnitMutation,
	// ? Quiz
	useGetClassQuizItemsQuery,
	useLazyGetClassQuizItemsQuery,

	useGetClassQuizDetailQuery,
	useCreateClassQuizMutation,
	useUpdateClassQuizMutation,
	useDeleteClassQuizMutation,
	usePublishQuizMutation,
	useHideQuizMutation,
	useConfigQuizMutation,
	// ? Voting
	useGetClassVotingDetailQuery,
	useCreateClassVotingMutation,
	useUpdateClassVotingMutation,
	useDeleteClassVotingMutation,
	useAddVotingOptionMutation,
	useChangeChoiceContentMutation,
	useMakeVotingChoiceMutation,
	useDeleteVotingChoiceMutation,

	useChangeClassItemPlacementMutation,

	// * Class member
	useGetMembersQuery,

	// *Textbooks
	useGetTextbooksInClassQuery,
	useCountTextbooksInClassQuery,
	useLazyGetClassContentGroupByTextbooksQuery,
	useGetClassUnitItemsQuery,
	useLazyGetUnitClassGroupByTextbooksQuery,
	useRefetchClassInformationMutation
} = classAPI;
