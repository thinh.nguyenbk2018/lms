import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "../../store/store";

const GRADE_RESULT_SLICE_KEY = "gradeResult";

const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice()
}

export const gradeResultSlice = createSliceEnhancedWithSearch({
    name: GRADE_RESULT_SLICE_KEY,
    initialState,
    reducers: {

    },
});

export const gradeResultSelector = {
    selectSearchCriteria: (state: RootState) => state.gradeResult.searchCriteria
}

export const {changeCriteria} = gradeResultSlice.actions;