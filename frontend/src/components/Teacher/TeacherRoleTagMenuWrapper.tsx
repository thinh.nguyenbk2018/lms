import {useChangeRoleOfTeacherMutation} from "@redux_features/Class/TeacherAPI/ClassTeacherAPI";
import {TeacherRole} from "@redux_features/Class/TeacherAPI/ClassTeacherSlice";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {Dropdown} from "antd";
import TeacherRoleMenu from "./TeacherRoleMenu";
import TeacherRoleTag from "./TeacherRoleTag";
import {ID} from "@redux/interfaces/types";

type TeacherRoleTagMenuWrapperProps = {
    id: ID;
    role: TeacherRole;
    classId: ID;
};

const TeacherRoleTagMenuWrapper = (props: TeacherRoleTagMenuWrapperProps) => {
    const { id, role, classId } = props;
    const [changeTeacherRole] = useChangeRoleOfTeacherMutation();
    const handleChangeRole = (newRole: TeacherRole) => {
        AntdNotifier.handlePromise(
            changeTeacherRole({
                teacherId: id,
                teacherRole: newRole,
                classId,
            }).unwrap(),
            "Thay đổi vai trò",
            () => {},
            () => {}
        );
    };
    return (
        <Dropdown
            overlay={
                <TeacherRoleMenu
                    onSelect={(r) => {
                        if (r === role) {
                            return;
                        }
                        handleChangeRole(r);
                    }}
                />
            }
        >
            <span>
                <TeacherRoleTag role={role} />
            </span>
        </Dropdown>
    );
};

export default TeacherRoleTagMenuWrapper;
