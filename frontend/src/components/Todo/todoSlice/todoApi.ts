import { apiSlice } from "@redux_features/CentralAPI";
import type { Todo } from "../TodoComponent";

const TodoApiWithInvalidateTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["Todos"],
});

const todosApi = TodoApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        getTodoList: build.query<Todo[], void>({
            query: () => "/todos",
            providesTags: ["Todos"],
        }),
    }),
    overrideExisting: true,
});

export const { useGetTodoListQuery } = todosApi;
