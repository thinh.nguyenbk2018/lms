import NewTeacherFromStaffModal from "@components/Teacher/NewTeacherFromStaffModal/NewTeacherFromStaffModal";
import TeacherRoleTagMenuWrapper from "@components/Teacher/TeacherRoleTagMenuWrapper";
import { Space } from "antd";
import { useParams } from "react-router-dom";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import TeacherList from "../../../../components/Teacher/TeacherList";
import { useAppDispatch } from "../../../../hooks/Redux/hooks";
import { useGetTeacherForClassQuery } from "../../../../redux/features/Class/TeacherAPI/ClassTeacherAPI";
import { classTeacherActions } from "../../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";
import LoadingPage from "../../../UtilPages/LoadingPage";
import EditButton from "@components/ActionButton/EditButton";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import TeacherRoleTag from "@components/Teacher/TeacherRoleTag";
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";

const ClassTeacherPage = () => {
    const { classId } = useParams();

    const dispatch = useAppDispatch();

    useClassSidebar();

    const { classInfo } = useClassCourseInformation(Number(classId));

    const {
        data: teacherList,
        isSuccess,
        isLoading,
        isFetching,
        isUninitialized,
        isError,
        error,
    } = useGetTeacherForClassQuery(
        { classId: Number(classId) },
        {
            skip: isNaN(Number(classId)),
        }
    );
    if (isSuccess) {
        return (
            <>
                <ContentHeader
                    title={"Giáo viên"}
                    action={
                        <Space>
                            {classInfo?.status != 'ENDED' && <PermissionRenderer
                                permissions={[PermissionEnum.ADD_TEACHER, PermissionEnum.UPDATE_TEACHER, PermissionEnum.DELETE_TEACHER]}>
                                <EditButton
                                    onClick={() => {
                                        dispatch(
                                            classTeacherActions.openTeacherAssignmenModal()
                                        );
                                    }}
                                />
                            </PermissionRenderer>}
                        </Space>
                    }
                />
                <TeacherList
                    listData={teacherList?.listData || []}
                    teacherRoleTag={(item) => {
                        const itemId = item.id || item.userId;
                        if (!item.role || !itemId) {
                            return <></>;
                        }
                        return (
                            <TeacherRoleTag
                                role={item.role}
                            />
                        );
                    }}
                />
                <NewTeacherFromStaffModal classId={Number(classId)} />
            </>
        );
    } else if (isLoading) {
        return <LoadingPage />;
    } else return <></>;
};

export default ClassTeacherPage;
