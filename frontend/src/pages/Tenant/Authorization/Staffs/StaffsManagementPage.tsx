import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import { Button, Dropdown, Menu, Space } from "antd";
import { Link } from "react-router-dom";
import StaffsManagement from "../../../../components/Authorization/Staff/StaffsManagement";
import { ROUTING_CONSTANTS } from "../../../../navigation/ROUTING_CONSTANTS";
import { addIconSvg } from "@components/Icons/SvgIcons";
import React, { useRef, useState } from "react";
import './StaffsManagementPage.less';
import TableSearch from "@components/Search/TableSearch";
import { changeCriteria, staffsManagementSelector, } from "@redux/features/Staff/StaffsManagementSlice";
import { useAppSelector } from "@hooks/Redux/hooks";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import IconSelector from "@components/Icons/IconSelector";
import { API_CONSTANT } from "@constants/ApiConfigs";
import menu from "antd/lib/menu";
import { useRefetchStaffsMutation } from "@redux/features/Staff/StaffAPI";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

const StaffsManagementPage = () => {

  const searchCriteria = useAppSelector(staffsManagementSelector.selectSearchCriteria);

  let importInputRef = useRef<HTMLInputElement>(null);
  let downloadReportRef = useRef<HTMLAnchorElement>(null);

  let [importing, setImporting] = useState(false);

  let [refetchStaffs] = useRefetchStaffsMutation();

  const uploadFile = (file: File) => {
    if (file.type !== "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
      AntdNotifier.error("Loại file vừa chọn không đúng, vui lòng thử lại.");
      return;
    }
    setImporting(true);
    let formData = new FormData();
    formData.append("file", file);
    fetch(API_CONSTANT.BASE_URL + "/staffs/import", {
      method: "POST",
      headers: {
        authorization: "Bearer " + localStorage.getItem("token")
      },
      body: formData
    }).then(res => {
      if (res.ok) {
        return res.blob()
      } else {
        throw new Error("File không hợp lệ vui lòng kiểm tra lại")
      }
    }).then(blob => {
      let src = URL.createObjectURL(blob)
      downloadReportRef.current!.href = src;
      downloadReportRef.current!.click();
      setImporting(false);
      refetchStaffs();
    }).catch(error => {
      AntdNotifier.error(error.message);
      setImporting(false);
    })
  }

  const menu = (
    <Menu>
      <Menu.Item key={1} onClick={() => console.log("File import mẫu")}>
        <a href={API_CONSTANT.BASE_STATIC_URL + "/api/staffs.xlsx"} download={"staffs.xlsx"}>
          Tải file mẫu
        </a>
      </Menu.Item>
    </Menu>
  );

  return <>
    <ContentHeader
      title="Nhân viên"
      action={
        <Space>
          <TableSearch
            searchFields={[{ title: 'Từ khóa', key: 'key' }]}
            defaultFilters={[]}
            searchCriteria={searchCriteria}
            changeCriteria={changeCriteria}
          />
          <PermissionRenderer permissions={[PermissionEnum.CREATE_STAFF]}>
            <Dropdown.Button
              type="primary"
              style={{ backgroundColor: "#52c41a", borderColor: "#52c41a", marginRight: 10 }}
              className={'button-with-icon'}
              loading={importing}
              onClick={() => {
                importInputRef.current?.click();
              }}
              overlay={menu}
            >
              <IconSelector type="ImportOutlined" />
              <span>Nhập từ files</span>
              <input
                placeholder="#"
                ref={importInputRef}
                type={"file"}
                name="file"
                style={{ visibility: "hidden", width: 1 }}
                accept={".xlsx,.xlsm,.xls"}
                onChange={({ target: { files } }) => {
                  if (files)
                    uploadFile(files[0]);
                }}
              />
            </Dropdown.Button>
          </PermissionRenderer>
          <PermissionRenderer permissions={[PermissionEnum.CREATE_STAFF]}>
            <Link to={ROUTING_CONSTANTS.CREATE_STAFF}>
              <Button type="primary" className={'button-with-icon'}>
                {addIconSvg}
                <span>Thêm</span>
              </Button>
            </Link>
          </PermissionRenderer>
        </Space>
      }
    />
    <StaffsManagement />
    <a ref={downloadReportRef} download={"import_staffs_report.xlsx"} href="#" />
  </>
}

export default StaffsManagementPage;