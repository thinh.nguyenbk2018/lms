// ? This constant is extracted to a new file to avoid Circular Dependency with WebPack

import {ROUTING_PATH_PARAM_KEY} from "./ROUTING_PARAM_KEY";

export const ROUTING_CONSTANTS = {
	ROOT: "/",
	DASHBOARD: "/dashboard",
	PROGRAM_LIST: "/programs",
	PROGRAM_NEW: "/programs/new",
	PROGRAM_DETAIL: "/programs/:id",

	TEST_PAGE_2: "/testpage2",
	TEST_PAGE: "/test",
	// ! Fallback route for failed routes
	NOT_FOUND: "/not_found",
	UNAUTHORIZED: "/unauthorized",
	FORUM: "/forum",
	NEW_FORUM_THREAD: "/forum/new",
	LOGIN_PAGE: "/user/login",
	// * Voting
	VOTING: "/voting",

	//files
	FILES: "/files",

	//staffs
	STAFFS_MANAGEMENT: "/staffs",
	CREATE_STAFF: "/staffs/new",
	EDIT_STAFF: "/staffs/:staffId",
	// authorizations
	ROLES_MANAGEMENT: "/authorizaton/roles",
	CREATE_ROLE: "/authorizaton/roles/new",
	EDIT_ROLE: "/authorizaton/roles/:roleId",

	// students
	STUDENTS_MANAGEMENT: "/students",
	CREATE_STUDENT: "/students/new",
	EDIT_STUDENT: "/students/:studentId",
	// grade formula
	GRADE_MANAGEMENT_IN_CLASS: "/classes/:classId/grades",
	CREATE_GRADE_FORMULA_IN_CLASS: "/classes/:classId/grades/new",
	UPDATE_GRADE_FORMULA_IN_CLASS: "/classes/:classId/grade-formula/:tagId",
	VIEW_GRADE_IN_CLASS: "/classes/:classId/grades/result",
	GRADE_MANAGEMENT_IN_COURSE: "/courses/:courseId/grades",
	CREATE_GRADE_FORMULA_IN_COURSE: "/courses/:courseId/grade-formula/create",
	UPDATE_GRADE_FORMULA_IN_COURSE: "/courses/:courseId/grade-formula/:tagId",
	VIEW_GRADE_IN_COURSE: "/courses/:courseId/grades/result",
	// * Create new Tenant
	NEW_TENANT: "/tenant/new",

	// * Voting
	NEW_VOTING: "/voting/new",
	VOTING_DETAIL: "/voting/:id",

	// * Quiz
	QUIZ: "/quiz",
	NEW_QUIZ: "/quiz/new",
	TEST_INSTANCE_DETAIL: "/test_instance/:testInstanceId",

	// * Calendar
	PERSONAL_CALENDAR: "/calendar",
	CLASS_CALENDAR: "/classes/:classId/calendar",

	// * Test Content Bank
	TEST_CONTENT_LIST: "/test_content/",

	// * Course
	COURSE_MANAGEMENT: "/courses",
	COURSE_DETAIL: "/courses/:courseId",
	NEW_COURSE: "/courses/new",
	EDIT_COURSE: "/courses/edit/:courseId",
	//  Course Sidebar
	COURSE_DASHBOARD: "/courses/:courseId/dashboard",
	COURSE_CLASS_LIST: "courses/:courseId/classes",
	COURSE_CLASS_VIEW_DETAILS: "/courses/:courseId/classes/:classId",
	COURSE_CLASS_NEW: "/courses/:courseId/classes/new",
	COURSE_CLASS_EDIT: "/courses/:courseId/classes/:id/edit",
	COURSE_CONTENT: "/courses/:courseId/content",
	COURSE_EXAM: "/courses/:courseId/exams",
	COURSE_QUIZ: "/courses/:courseId/quizzes",
	COURSE_EXAM_DETAIL: "/courses/:courseId/exams/:testId",
	COURSE_STUDENT: "/courses/:courseId/student",
	COURSE_TEACHER: "/courses/:courseId/teacher",
	COURSE_FEED_BACK: "/courses/:courseId/feedback",

	COURSE_QUIZ_ITEM_DETAIL_PAGE:
		"/courses/:courseId/quizzes/:quizItemId",
	COURSE_UNIT_ITEM_DETAIL_PAGE:
		"/courses/:courseId/units/:unitItemId",
	COURSE_UNIT_LIST_PAGE: "/courses/:courseId/units",

	// * Class
	CLASS_MANAGEMENT: "/class",
	CLASS_LIST: "/classes",
	CLASS_NEW: "/classes/new",
	CLASS_DETAIL_INFORMATION: "/classes/:id/information",
	CLASS_DETAIL_MEMBERS: "/classes/:id/members",
	CLASS_ANNOUNCEMENT: "/classes/:classId/announcement",
	CLASS_DETAIL_ANNOUNCEMENT_CREATE: "/classes/:classId/announcement/new",
	CLASS_DETAIL_ANNOUNCEMENT_DETAIL: "/announcement/:announcementId",
	// Class Sidebar
	CLASS_DASHBOARD: "/classes/:classId/dashboard",
	CLASS_STUDENT: "/classes/:classId/student",
	CLASS_TEACHER: "/classes/:classId/teacher",
	CLASS_CONTENT: "/classes/:classId/content",
	// ? CLASS ATTENDANCE PAGES
	CLASS_CHECK_IN: "/classes/:classId/attendances",
	CLASS_CHECK_IN_SESSION_DETAIL: "/classes/:classId/attendances/:sessionId",

	CLASS_TIME_TABLE: "/classes/:classId/scheduler",
	// ? CLASS QUIZ
	CLASS_QUIZ: "/classes/:classId/quizzes",
	CLASS_QUIZ_SESSION: `/classes/:classId/quizzes/:quizId/session/${ROUTING_PATH_PARAM_KEY.TEST_SESSION_ID}`,
	CLASS_DISCUSSION: "/classes/:classId/discussion",
	CLASS_QUIZ_INFO_AND_CONFIG: `/classes/:classId/test_config/${ROUTING_PATH_PARAM_KEY.TEST_WITH_CONFIG}`,
	CLASS_QUIZ_RESULT_MANAGEMENT: `/classes/:classId/quizzes/:quizItemId/result`,

	// **** Teacher nav to this page to update the point for questions that can't be graded automactically
	CLASS_QUIZ_RESULT_STUDENT_SESSION: `/classes/:classId/quizzes/:quizItemId/session/${ROUTING_PATH_PARAM_KEY.TEST_SESSION_ID}/result`,
	// ? CLASS ACTIVITY DETAIL
	CLASS_QUIZ_ITEM_DETAIL_PAGE:
		"/classes/:classId/quizzes/:quizItemId",
	CLASS_UNIT_ITEM_DETAIL_PAGE:
		"/classes/:classId/units/:unitItemId",
	CLASS_UNIT_LIST_PAGE: "/classes/:classId/units",
	CLASS_VOTING_ITEM_DETAIL_PAGE:
		"/classes/:classId/content/voting/:votingItemId",

	CLASS_STUDENT_DETAILS: "/classes/:classId/students/:studentId",
	// * Text book
	TEXTBOOK_MANAGEMENT: "/textbooks",
	CENTRAL_TEXTBOOK_DETAIL: "/textbooks/:textbookId",
	COURSE_TEXTBOOK_DETAIL: "/courses/:courseId/textbooks/:textbookId",
	CLASS_TEXTBOOK_DETAIL: "/classes/:classId/textbooks/:textbookId",
	CLASS_TEXTBOOK_MANAGEMENT: "/classes/:classId/textbooks",
	COURSE_TEXTBOOK_MANAGEMENT: "/courses/:courseId/textbooks",
	// PersonalInfo
	PERSONAL_INFORMATION: "/me"
};

export const HOST_ROUTING_CONSTANT = {
	ROOT: "/",
	ADMIN: "/admin",
	HOME_PAGE: "/",
	REGISTER: "/register",
	REGISTER_SUCCESS: "/register/success",
	LOGIN: "/login",
	DASHBOARD: "/admin/dashboard",
	EXTEND: "/admin/extend",
	SETTINGS: "/admin/settings",
	PERSONALIZED: "/admin/settings/personalized",
}
