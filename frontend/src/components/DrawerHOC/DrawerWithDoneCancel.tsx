import React, {useState} from 'react'
import {Button, ButtonProps, Drawer, DrawerProps, Space} from 'antd';
import CancelButton from '../ActionButton/CancelButton';
import DoneButton from '../ActionButton/DoneButton';


type DrawerHocProps = {
    // * The trigger that will open the drawrer
    trigger: JSX.Element;
    // * The content of the drawer 
    content: JSX.Element;
    onCancel?: Function;
    onDone?: Function;
    doneButtonProps?: ButtonProps;
    cancelButtonProps?: ButtonProps;
    onCloseCallback?: Function;
    closeable?: boolean;
    maskClosable?: boolean;
    closable?: boolean;
} & DrawerProps;

const DrawerWithDoneCancel = (props: DrawerHocProps) => {
    const {
        trigger,
        content,
        onCancel,
        onDone,
        onCloseCallback,
        doneButtonProps,
        cancelButtonProps,
        maskClosable,
        closable,
        ...drawerProps
    } = props;
    const [visible, setVisible] = useState(false);
    const finalTrigger = trigger || <Button onClick={() => setVisible(true)}> Open Drawer</Button>

    const closeDrawer = () => {
        setVisible(false);
    }
    return (
        <>
            <Drawer {...drawerProps} onClose={() => {
                if (onCloseCallback) onCloseCallback();
                setVisible(false);
            }}
                    maskClosable={maskClosable}
                    closable={closable}
                    visible={visible}
                    width={720}
                    bodyStyle={{paddingBottom: 80}}
                    extra={
                        <Space>
                            <CancelButton {...cancelButtonProps} onClick={() => {
                                if (onCancel) {
                                    onCancel();
                                } else {
                                    closeDrawer();
                                }
                            }}/>
                            <DoneButton {...doneButtonProps} onClick={
                                () => {
                                    onDone?.()
                                    closeDrawer()
                                }
                            }/>
                        </Space>
                    }
            >
                {content}
            </Drawer>
            <span onClick={() => setVisible(true)}>
                {trigger}
            </span>
        </>
    )
}

export default DrawerWithDoneCancel