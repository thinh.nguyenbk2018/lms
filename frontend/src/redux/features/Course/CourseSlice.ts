import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "../../store/store";

const COURSE_SLICE_KEY = "course";


const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice(),

}

export const courseSlice = createSliceEnhancedWithSearch({
    name: COURSE_SLICE_KEY,
    initialState,
    reducers: {

    },
});

export const courseSelector = {
    selectSearchCriteria: (state: RootState) => state.course.searchCriteria
}

export const {changeCriteria} = courseSlice.actions;