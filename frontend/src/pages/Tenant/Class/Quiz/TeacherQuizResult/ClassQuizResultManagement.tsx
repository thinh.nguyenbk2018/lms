import {CheckOutlined, InfoOutlined, UserAddOutlined,} from "@ant-design/icons";
import {Button, Card, Col, Row, Space, Tag, Typography} from "antd";
import React, {useState} from "react";
import COLOR_SCHEME from "../../../../../constants/ThemeColor";
import {
    StudentSessionResult,
    useGetQuizOverviewStudentQuery,
    useGetTeacherQuizResultQuery,
} from "@redux/features/Quiz/examAPI";
import SectionHeader from "../../../../../components/SectionHeader/SectionHeader";
import {Link, useNavigate, useParams} from "react-router-dom";
import LoadingPage from "../../../../UtilPages/LoadingPage";
import ApiErrorWithRetryButton from "../../../../../components/Util/ApiErrorWithRetryButton";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import './ClassQuizResultManagement.less';
import timeStampHelper from "@utils/TimeStampHelper/TimeStampHelper";
import CustomTable from "@components/CustomTable/CustomTable";
import BackButton from "@components/ActionButton/BackButton";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {changeCriteria, quizResultManagementSelector} from "@redux/features/Quiz/QuizSessionManagementSlice";
import {extractPaginationInformation} from "@components/CustomTable/TableUtils";
import {okeSvgIcon} from "@components/Icons/SvgIcons";
import TableSearch from "@components/Search/TableSearch";
import _ from "lodash";
import DEFAULT_PAGE_SIZE, {Sort} from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {faDownload} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import download from 'downloadjs';
import LOCAL_STORAGE_KEYS from "@constants/LocalStorageKey";
import {API_CONSTANT} from "@constants/ApiConfigs";

type QuizInfoCardProps = {
    label: string;
    value: number | string;
    icon: JSX.Element;
};
const QuizInfoCardItem = (props: QuizInfoCardProps) => {
    const {label, value, icon} = props;
    return (
        <Card className={'quiz-info-card-item'} style={{minWidth: `200px`}}>
            {icon}
            <span>{label}:<span className={'quiz-info-card-item-value'}>{value}</span></span>
        </Card>
    );
};
// ** This page will allows the teacher to view the whole list of studetns submission and addition test aggregated details
const ClassQuizResultManagement = () => {
        // const [forceCloseSidebar, courseInfo] = useClassSidebar();
        // TODO: Check the param name and extract the info
        const {quizItemId, classId} = useParams();
        const navigate = useNavigate();

        const [isDownloadingExport, setIsDownloadingExport] = useState(false);

        const shouldSkipFetch = () => {
            return isNaN(Number(classId)) ||
                isNaN(Number(quizItemId));

        };

        const dispatch = useAppDispatch();

        const quizSessionResultManagementCriteria = useAppSelector(
            quizResultManagementSelector.selectSearchCriteria
        );

        // console.log('criteria; ', quizSessionResultManagementCriteria);

        const {data: quiz, isSuccess, isLoading, isFetching} =
            useGetQuizOverviewStudentQuery(
                {
                    classId: Number(classId),
                    testWithConfigId: Number(quizItemId),
                },
                {
                    skip: shouldSkipFetch(),
                }
            );

        const {
            data: teacherAggregatedResult,
            isSuccess: isTeacherResultSuccess,
            isFetching: isTeacherResultLoading,
        } = useGetTeacherQuizResultQuery({quizId: Number(quizItemId), searchCriteria: quizSessionResultManagementCriteria});

        const changeFilter = (field: string, value: string) => {
            const criteria = _.cloneDeep(quizSessionResultManagementCriteria);
            let filter = criteria.filter;
            let payload;
            const index = filter.map(x => x.field).indexOf(field);
            const defaultPagination = {
                page: 1,
                size: DEFAULT_PAGE_SIZE,
            };
            if (index != -1) {
                let values = filter[index].values;
                const indexValue = values.indexOf(value);
                if (indexValue != -1) {
                    filter.splice(index, 1);
                    payload = {
                        ...criteria,
                        pagination: defaultPagination,
                        filter: filter
                    }
                } else {
                    filter[index].values = [value]
                    payload = {
                        ...criteria,
                        pagination: defaultPagination,
                        filter: filter
                    }
                }
            } else {
                filter.splice(filter.length - 1, 0, {field: field, values: [value]});
                payload = {
                    ...criteria,
                    pagination: defaultPagination,
                    filter: filter
                }
            }
            dispatch(changeCriteria(payload));
        }

        const [questionQuizStatistic, setQuestionQuizStatistic] = useState([]);

        const columns = [
                {
                    title: "Lần cuối cùng",
                    width: "10%",
                    key: "index",
                    fixed: "left" as const,
                    align: "center" as const,
                    render: (text: any, record: any, index: any) => record.isLastSession ?
                        <span className={'last-session-flag'}>{okeSvgIcon}</span> : ''
                },
                {
                    title: "Họ và tên",
                    width: "15%",
                    key: "index",
                    fixed: "left" as const,
                    render: (text: any, record: any, index: any) => `${record.user?.lastName} ${record.user?.firstName}`
                },
                {
                    title: "Tình trạng",
                    width: "10%",
                    align: "center" as const,
                    render: (session: StudentSessionResult) => {
                        if (session.submittedAt) {
                            return <Tag color={'teal'}>Đã hoàn thành</Tag>
                        }
                        return <Tag color={'#c91c00'}>Chưa hoàn thành</Tag>
                    }
                },
                {
                    title: "Thời gian kết thúc",
                    dataIndex: "submittedAt",
                    key: "submittedAt",
                    width: "15%",
                    align: "center" as const,
                    render: (submittedAt: Date | undefined) => {
                        if (!submittedAt) {
                            return <></>
                        }
                        return timeStampHelper.formatDateTime(submittedAt);
                    }
                },
                {
                    title: "Thời gian làm bài",
                    dataIndex: "timeTaken",
                    key: "timeTaken",
                    width: "15%",
                    align: "center" as const,
                    render: (timeTaken: number) => `${Math.floor(timeTaken / 60)} phút ${timeTaken - Math.floor(timeTaken / 60) * 60} giây`
                },
                {
                    title: `Điểm / ${quiz?.totalGrade}`,
                    // title: `Điểm / ${quizItemData?.totalGrade}`,
                    dataIndex: "score",
                    key: "score",
                    width: "10%",
                    align: "center" as const,
                    render: (s: number) => s?.toFixed(2),
                    sorter: {
                        compare: (a: string, b: string) => a.length - b.length,
                        multiple: 1,
                    },
                    sortDirections: ["descend", "ascend"],
                    sortOrder:
                        quizSessionResultManagementCriteria.sort.filter((x: Sort) => x.field === "score").length > 0 &&
                        quizSessionResultManagementCriteria.sort.filter((x: Sort) => x.field === "score")[0].order,
                },
                {
                    title: `Trạng thái chấm điểm`,
                    dataIndex: "gradedState",
                    key: "gradedState",
                    width: "15%",
                    align: "center" as const,
                    render: (state: any) => {
                        if (state && state == 'WAITING') {
                            return <Tag color={"#c91c00"}>Chưa xong</Tag>
                        }
                        return <Tag color={COLOR_SCHEME.success}>Xong</Tag>
                    }
                },
                {
                    title: `Kết quả`,
                    dataIndex: "finalVerdict",
                    key: "finalVerdict",
                    width: "10%",
                    align: "center" as const,
                    render: (state: any) => {
                        if (state && state == 'FAILED') {
                            return <Tag color={"#c91c00"}>Chưa đạt</Tag>
                        }
                        if (state && state == 'PASSED') {
                            return <Tag color={COLOR_SCHEME.success}>Đạt</Tag>
                        }
                        return <></>
                    }
                },
                {
                    title: "",
                    width: "10%",
                    align: "center" as const,
                    render: (session: StudentSessionResult) => {
                        if (session.submittedAt == undefined) {
                            return <></>
                        }
                        return <Link to={replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_STUDENT_SESSION,
                            [":classId", classId || ""],
                            [":quizItemId", quizItemId || ""],
                            [":testSessionId", session.sessionId])}>Xem</Link>
                    }
                }
            ];

        if (
            !shouldSkipFetch() &&
            teacherAggregatedResult &&
            isSuccess &&
            quiz
        ) {
            return (
                <>
                    <ContentHeader
                        title={'Tổng hợp kết quả'}
                        action={
                            <Space>
                                <BackButton
                                    type="primary"
                                    danger
                                    onClick={() => {
                                        navigate(
                                            replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                                [":classId", classId || ""],
                                                [":quizItemId", quizItemId || ""])
                                        )
                                    }
                                    }
                                    className={"back-btn"}
                                />
                                <Button
                                    loading={isDownloadingExport}
                                    type="primary"
                                    onClick={() => {
                                        setIsDownloadingExport(true);
                                        axios.get(`${API_CONSTANT.BASE_URL}/statistic_quiz/${quizItemId}`,
                                            {
                                                responseType: 'arraybuffer',
                                                headers: {
                                                    'Authorization': `Bearer ${localStorage.getItem(LOCAL_STORAGE_KEYS.TOKEN)}`
                                                }
                                            })
                                            .then(res => {
                                                setIsDownloadingExport(false);
                                                download(res.data, `quiz.xlsx`, res.headers['content-type'])
                                            })
                                    }}
                                    className={"back-btn"}
                                >
                                    <FontAwesomeIcon icon={faDownload}/>
                                    Tải thống kê
                                </Button>
                            </Space>
                        }
                    />
                    <div>
                        <SectionHeader title={"Thống kê"}/>
                        <Card>
                            <Row justify={"space-between"}>
                                <Col span={8}>
                                    <Space direction="vertical">
                                        <Typography.Title level={5}>
                                            {`Bài kiểm tra: `}
                                            <Typography.Link
                                                className={'quiz-result-management-quiz-link'}
                                                onClick={() => navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                                    [":classId", classId || ""],
                                                    [":quizItemId", quizItemId || ""]))}
                                            >{quiz.title}</Typography.Link>
                                        </Typography.Title>
                                    </Space>
                                </Col>
                                <Col>
                                    <Space direction="vertical">
                                        <Space style={{marginBottom: '1rem'}}>
                                            {quiz.config?.haveTimeLimit ? (
                                                <Tag color="#87d068">{quiz.config?.timeLimit} phút</Tag>
                                            ) : (
                                                <Tag color="#f50">
                                                    Không giới hạn thời gian làm bài
                                                </Tag>
                                            )}
                                            <Tag>
                                                <Space>
                                                    <CheckOutlined style={{color: COLOR_SCHEME.success}}/>
                                                    <Typography.Text>
                                                        {`Thang điểm: ${quiz.totalGrade ?? 0}`}
                                                    </Typography.Text>
                                                    <Typography.Text>
                                                        {`Điểm đạt: ${quiz.config?.passScore || 0}`}
                                                    </Typography.Text>
                                                </Space>
                                            </Tag>
                                        </Space>
                                    </Space>
                                </Col>
                                <Col span={24}>
                                    <Row justify="space-around" gutter={[4, 10]}>
                                        <QuizInfoCardItem
                                            label="Số học viên tham gia"
                                            value={teacherAggregatedResult.totalParticipation}
                                            icon={
                                                <UserAddOutlined style={{color: COLOR_SCHEME.primary}}/>
                                            }
                                        />
                                        <QuizInfoCardItem
                                            label="Điểm trung bình"
                                            value={teacherAggregatedResult.averageScore?.toFixed(2) || 'Chưa có bài làm nào được hoàn tất chấm điểm'}
                                            icon={
                                                <InfoOutlined style={{color: COLOR_SCHEME.primary}}/>
                                            }
                                        />
                                        <QuizInfoCardItem
                                            label="Tổng số học viên đạt"
                                            value={teacherAggregatedResult.totalNumberOfPassedStudent}
                                            icon={
                                                <CheckOutlined style={{color: COLOR_SCHEME.success}}/>
                                            }
                                        />
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                        <SectionHeader title={"Bài làm của học viên"}/>
                        <span className={'class-quiz-result-instruction'}>*Lưu ý: Điểm cuối cùng của học viên trong bài kiểm tra là điểm của lần làm bài cuối cùng</span>
                        <div className={'class-quiz-management-search-filter-wrapper'}>
                            <div className={'class-quiz-management-filter-wrapper'}>
                                <Button
                                    className={`quiz-class-management-btn ${quizSessionResultManagementCriteria
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'gradedState') != undefined
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'gradedState')?.values.indexOf("DONE") != -1 ? 'quiz-class-management-btn-active' : 'quiz-class-management-btn-inactive'}`}
                                    onClick={(e) => {
                                        changeFilter("gradedState", "DONE");
                                    }}
                                >
                                    Chấm điểm
                                </Button>
                                <Button
                                    className={`quiz-class-management-btn ${quizSessionResultManagementCriteria
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'gradedState') != undefined
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'gradedState')?.values.indexOf("WAITING") != -1 ? 'quiz-class-management-btn-active' : 'quiz-class-management-btn-inactive'}`}
                                    onClick={() => {
                                        changeFilter("gradedState", "WAITING");
                                    }}
                                >
                                    Chưa chấm điểm
                                </Button>
                                <Button
                                    className={`quiz-class-management-btn ${quizSessionResultManagementCriteria
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'finalVerdict') != undefined
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'finalVerdict')?.values.indexOf("PASSED") != -1 ? 'quiz-class-management-btn-active' : 'quiz-class-management-btn-inactive'}`}
                                    onClick={() => {
                                        changeFilter("finalVerdict", "PASSED");
                                    }}
                                >
                                    Đạt
                                </Button>
                                <Button
                                    className={`quiz-class-management-btn ${quizSessionResultManagementCriteria
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'finalVerdict') != undefined
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'finalVerdict')?.values.indexOf("FAILED") != -1 ? 'quiz-class-management-btn-active' : 'quiz-class-management-btn-inactive'}`}
                                    onClick={() => {
                                        changeFilter("finalVerdict", "FAILED");
                                    }}
                                >
                                    Chưa đạt
                                </Button>
                                <Button
                                    className={`quiz-class-management-btn ${quizSessionResultManagementCriteria
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'lastSession') != undefined
                                    && quizSessionResultManagementCriteria.filter.find(x => x.field == 'lastSession')?.values.indexOf("true") != -1 ? 'quiz-class-management-btn-active' : 'quiz-class-management-btn-inactive'}`}
                                    onClick={() => {
                                        changeFilter("lastSession", "true");
                                    }}
                                >
                                    Lần cuối cùng
                                </Button>
                            </div>
                            <div className={'class-quiz-management-search-wrapper'}>
                                <TableSearch
                                    searchFields={[{title: "Từ khóa", key: "key"}]}
                                    defaultFilters={[]}
                                    changeCriteria={changeCriteria}
                                    searchCriteria={quizSessionResultManagementCriteria}
                                />
                            </div>
                        </div>
                        <>
                            <CustomTable
                                columns={columns}
                                data={teacherAggregatedResult.sessionsResult.listData}
                                searchCriteria={quizSessionResultManagementCriteria}
                                metaData={extractPaginationInformation(teacherAggregatedResult.sessionsResult)}
                                changeSearchActionCreator={changeCriteria}
                                scroll={{x: 1000}}
                                notUseFilter={true}
                            />
                        </>
                    </div>
                </>
            );
        } else if (isLoading || isFetching || isTeacherResultLoading) {
            return <LoadingPage/>;
        } else
            return (
                <>
                    <span>{JSON.stringify(teacherAggregatedResult)}</span>
                    <ApiErrorWithRetryButton
                        onRetry={() => {
                            window.location.reload();
                        }}
                    />
                </>
            );
    }
;

export default ClassQuizResultManagement;
