import { Form, Input, Modal, Spin, Typography } from 'antd';
import {
  ChonkyActions,
  ChonkyIconName,
  FileActionData,
  FileActionHandler,
  FileArray,
  FileData,
  FullFileBrowser,
  setChonkyDefaults
} from 'chonky';
import { ChonkyIconFA } from 'chonky-icon-fontawesome';
import { MouseClickFilePayload, MoveFilesPayload, OpenFilesPayload } from 'chonky/dist/types/action-payloads.types';
import { deleteObject, getBytes, getDownloadURL, listAll, ref, uploadBytes } from 'firebase/storage';
import { useCallback, useEffect, useRef, useState } from 'react';
import { WritableProps } from 'tsdef';
import LOCAL_STORAGE_KEYS from '../../constants/LocalStorageKey';
import { useCreateShareFileTokenMutation, useDecodeShareFileTokenMutation } from '../../redux/features/Files/FileAPI';
import { useAppSelector } from '../../hooks/Redux/hooks';
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { MoveFiles, PasteFiles, ReceiveFiles, ShareFiles } from './CustomAction';
import { storage } from "@utils/FirebaseSDK/firebase";
import UploadForm, { UploadFormHandle } from '../UploadForm/UploadForm';
import { tenantSelector } from '../../redux/features/Tenant/TenantSlice';
import { authSelectors } from '../../redux/features/Auth/AuthSlice';

setChonkyDefaults({ iconComponent: ChonkyIconFA, disableDragAndDrop: true})

const NAME_FOLDER_FIELD = "folder_name";
const SHARE_FILETOKEN_FIELD = "folder_name";

interface FileManagerProps {
  onSelectFile?: (file: { fileUrl: string, type: string, name: string }) => void
}

const FileManager = ({ onSelectFile }: FileManagerProps) => {

  // * Prefix for the file directory
  let tenantInfo = useAppSelector(tenantSelector.selectTenantInfo);
  let userInfo = useAppSelector(authSelectors.selectCurrentUser);
  let fileDiskPrefix = `${tenantInfo.domain}/${userInfo?.username}`

  let [currentStorageRef, setCurrentStorageRef] = useState(ref(storage, fileDiskPrefix));
  let [isLoading, setLoading] = useState(false);

  let [createShareToken] = useCreateShareFileTokenMutation();
  let [decodeShareFileToken] = useDecodeShareFileTokenMutation();

  const [files, setFiles] = useState<FileArray>([]);
  const [folderChain, setFolderChain] = useState<FileArray>([{ id: fileDiskPrefix, name: 'Home', isDir: true }]);

  const [copyingFiles, setCopyingFiles] = useState<FileArray>([]);
  const [movingFiles, setMovingFiles] = useState<FileArray>([]);

  const [showUploadModal, setShowUploadModal] = useState<boolean>(false);
  const [showCreateFolderModal, setShowCreateFolderModal] = useState<boolean>(false);
  const [showReceiveFilesModal, setShowReceiveFilesModal] = useState<boolean>(false);
  const [showShareFilesModal, setShowShareFilesModal] = useState<boolean>(false);
  const [shareFileToken, setShareFileToken] = useState<string>("");

  const uploadFormRef = useRef<UploadFormHandle>(null);
  const downloadRef = useRef<HTMLAnchorElement>(null);
  const [form] = Form.useForm();

  const loadCurrentFolder = useCallback(() => {
    listAll(currentStorageRef)
      .then(res => {
        let folders: FileArray = res.prefixes.map(prefix => ({
          id: prefix.fullPath,
          name: prefix.name,
          isDir: true
        }));
        let newFiles: FileArray = res.items.map(item => ({
          id: item.fullPath,
          name: item.name,
          isDir: false,
        }));
        setFiles([...folders, ...newFiles]);
      })
      .catch(err => AntdNotifier.error(err));
  }, [currentStorageRef]);

  const thumnailsGenerator = (file: FileData) => {
    return Promise.resolve()
      .then(() => !file.isDir ? getDownloadURL(ref(storage, file.id)) : null)
  }

  useEffect(() => {
    loadCurrentFolder();
  }, [loadCurrentFolder])

  const actions = [
    ChonkyActions.OpenSelection,
    MoveFiles,
    ChonkyActions.CopyFiles,
    PasteFiles,
    ChonkyActions.DownloadFiles,
    ChonkyActions.UploadFiles,
    ChonkyActions.DeleteFiles,
    ChonkyActions.CreateFolder,
    ShareFiles,
    ReceiveFiles
  ];

  const onpenFolderByFolderChain = useCallback((data: FileActionData<WritableProps<{
    readonly id: "open_files";
    readonly __payloadType: OpenFilesPayload;
  }>>
  ) => {
    if (data.payload.targetFile) {
      setCurrentStorageRef(ref(storage, data.payload.targetFile?.id))
      setFolderChain(prev => {
        return prev.slice(0, prev.findIndex(file => file?.id === data.payload.targetFile?.id) + 1)
      })
    } else {
      setCurrentStorageRef(ref(storage, data.payload.files[0].id));
      setFolderChain([...folderChain, data.payload.files[0]]);
    }
  }, [folderChain]);

  const openFolder = useCallback((data: FileActionData<WritableProps<{
    readonly id: "mouse_click_file";
    readonly __payloadType: MouseClickFilePayload;
  }>>) => {
    // open folder
    setCurrentStorageRef(ref(storage, data.payload.file.id));
    setFolderChain([...folderChain, data.payload.file]);
  }, [folderChain])

  const deleteFiles = useCallback((data: FileActionData<WritableProps<{
    readonly id: "delete_files";
    readonly requiresSelection: true;
    readonly hotkeys: readonly ["delete"];
    readonly button: {
      readonly name: "Delete files";
      readonly toolbar: true;
      readonly contextMenu: true;
      readonly group: "Actions";
      readonly icon: ChonkyIconName.trash;
    };
  }>>) => {
    data.state.selectedFiles.forEach(file => {
      deleteObject(ref(storage, file.id))
        .then(value => {
          console.log("delete success");
          loadCurrentFolder();
        })
        .catch(reason => { })
    })
  }, [loadCurrentFolder]);

  const downloadFiles = (data: FileActionData<WritableProps<{
    readonly id: "download_files";
    readonly requiresSelection: true;
    readonly button: {
      readonly name: "Download files";
      readonly toolbar: true;
      readonly contextMenu: true;
      readonly group: "Actions";
      readonly icon: ChonkyIconName.download;
    };
  }>>) => {
    data.state.selectedFiles.forEach(file => {
      downloadRef.current!.download = file.name;
      getDownloadURL(ref(storage, file.id))
        .then(url => fetch(url))
        .then(function (response) {
          return response.blob()
        })
        .then(function (blob) {
          var urlCreator = window.URL || window.webkitURL;
          downloadRef.current!.href = urlCreator.createObjectURL(blob);
          downloadRef.current?.click();
        });
    })
  }

  const handleSelectFile = (data: FileActionData<WritableProps<{
    readonly id: "open_files";
    readonly __payloadType: OpenFilesPayload;
  }>>) => {
    setLoading(true);
    data.state.selectedFiles.forEach(async file => {
      console.log('start to handle file: ', file);
      let fileBytes = await getBytes(ref(storage, file.id));
      let systemStorageRef = ref(storage, "system");
      let fileCloned = await uploadBytes(ref(systemStorageRef, file.name), fileBytes);
      console.log('clone file finished');
      getDownloadURL(ref(systemStorageRef, fileCloned.metadata.name))
        .then(url => {
          console.log("File selected:", file)
          onSelectFile!({ fileUrl: url, type: file.ext || file.name.split('.').pop() || "unknown", name: file.name });
          setLoading(false);
        })
    })
  }

  const moveFiles = useCallback((data: FileActionData<WritableProps<{
    readonly id: "move_files";
    readonly __payloadType: MoveFilesPayload;
  }>>) => {
    data.payload.files.forEach(async file => {
      let fileBytes = await getBytes(ref(storage, file.id));
      await uploadBytes(ref(storage, `${data.payload.destination.id}/${file.name}`), fileBytes);
      await deleteObject(ref(storage, file.id));
      loadCurrentFolder();
    })
  }, [loadCurrentFolder])

  const copyFiles = (data: FileActionData<WritableProps<{
    readonly id: "copy_files";
    readonly requiresSelection: true;
    readonly hotkeys: readonly ["ctrl+c"];
    readonly button: {
      readonly name: "Copy selection";
      readonly toolbar: true;
      readonly contextMenu: true;
      readonly group: "Actions";
      readonly icon: ChonkyIconName.copy;
    };
  }>>) => {
    setMovingFiles([]);
    setCopyingFiles(data.state.selectedFiles);
  }

  const createFolder = () => {
    let folderName = form.getFieldValue(NAME_FOLDER_FIELD);
    setFiles(prev => [...prev, {
      id: `${currentStorageRef.fullPath}/${folderName}`,
      name: folderName,
      isDir: true
    }])
    form.resetFields();
    setShowCreateFolderModal(false);
  }

  const pasteFiles = useCallback((data: any) => {
    let desRef = currentStorageRef;
    if (data.state.selectedFiles.length) {
      desRef = ref(storage, data.state.selectedFiles[0].id);
    }
    if (movingFiles.length > 0) {
      Promise.all(
        movingFiles.map(async (file: any) => {
          let fileBytes = await getBytes(ref(storage, file.id));
          await uploadBytes(ref(desRef, file.name), fileBytes);
          await deleteObject(ref(storage, file.id));
          loadCurrentFolder();
        })
      ).then(() => setMovingFiles([]));
    }
    if (copyingFiles.length > 0) {
      copyingFiles.forEach(async (file: any) => {
        let fileBytes = await getBytes(ref(storage, file.id));
        await uploadBytes(ref(desRef, file.name), fileBytes);
        loadCurrentFolder();
      })
    }
  }, [copyingFiles, currentStorageRef, loadCurrentFolder, movingFiles])

  const createShareFileToken = useCallback(async (files: FileData[]) => {
    try {
      for (let file of files) {
        if (file.isDir) {
          AntdNotifier.error("Bạn không được chia sẻ thư mục", "Lỗi");
          return;
        }
      }
      let createShareTokenRes = await createShareToken({
        fullpaths: files.map(file => file.id)
      }).unwrap();
      // await navigator.clipboard.writeText(createShareTokenRes.token);
      setShareFileToken(createShareTokenRes.token);
      setShowShareFilesModal(true);
    }
    catch (e) {
      console.log(e);
    }
  }, [createShareToken])

  const receiveFiles = async () => {
    let token = form.getFieldValue(SHARE_FILETOKEN_FIELD);
    decodeShareFileToken({ token }).unwrap()
      .then(res => {
        res.fullpaths.forEach(async fullpath => {
          let fileBytes = await getBytes(ref(storage, fullpath));
          await uploadBytes(ref(currentStorageRef, fullpath.split('/').reverse()[0]), fileBytes);
          loadCurrentFolder();
        })
        form.resetFields();
        setShowReceiveFilesModal(false);
      })
      .catch(err => {
        if (err.data.code === 'EXPIRED_SHARE_FILE_TOKEN') {
          AntdNotifier.error("Mã chia sẻ đã hết hạn!", "Lỗi");
        }
        else if (err.data.code === 'INVALID_SHARE_FILE_TOKEN') {
          AntdNotifier.error("Mã chia sẻ không hợp lệ!", "Lỗi");
        }
      });
  }

  const fileActionsHandler = useCallback<FileActionHandler>(data => {
    if (data.id === ChonkyActions.OpenFiles.id) {
      if (onSelectFile && !data.payload.targetFile?.isDir) {
        handleSelectFile(data);
        console.log("select file")
      } else {
        onpenFolderByFolderChain(data);
        console.log("open folder file")
      }
    }
    if (data.id === ChonkyActions.MouseClickFile.id) {
      if (data.payload.clickType === 'double' && data.payload.file.isDir) {
        openFolder(data);
      }
    }
    if (data.id === ChonkyActions.UploadFiles.id) {
      setShowUploadModal(true);
    }
    if (data.id === ChonkyActions.DeleteFiles.id) {
      deleteFiles(data);
    }
    if (data.id === ChonkyActions.DownloadFiles.id) {
      downloadFiles(data);
    }
    if (data.id === ChonkyActions.MoveFiles.id) {
      moveFiles(data);
    }
    if (data.id === ChonkyActions.CopyFiles.id) {
      copyFiles(data);
    }
    if (data.id === PasteFiles.id) {
      pasteFiles(data);
    }
    if (data.id === MoveFiles.id) {
      setCopyingFiles([]);
      setMovingFiles(data.state.selectedFiles);
    }
    if (data.id === ChonkyActions.CreateFolder.id) {
      setShowCreateFolderModal(true);
    }
    if (data.id === ShareFiles.id) {
      let files = data.state.selectedFiles;
      createShareFileToken(files);
    }
    if (data.id === ReceiveFiles.id) {
      setShowReceiveFilesModal(true);
    }
  }, [onpenFolderByFolderChain, openFolder, deleteFiles, moveFiles, pasteFiles, createShareFileToken]);


  return <Spin spinning={isLoading}>
    <div style={{ height: 700, margin: "0px -24px" }}>
      <FullFileBrowser
        files={files}
        folderChain={folderChain}
        fileActions={actions}
        onFileAction={fileActionsHandler}
        thumbnailGenerator={thumnailsGenerator}
        disableDragAndDrop={true}
      />
      <Modal
        visible={showUploadModal}
        title="Tải lên"
        okText="Tải lên"
        cancelText="Hủy"
        onCancel={() => {
          setShowUploadModal(false);
          uploadFormRef.current?.clearForm();
          setLoading(false);
        }}
        onOk={() => {
          uploadFormRef.current?.upload({
            onSuccess: loadCurrentFolder
          });
          setShowUploadModal(false);
        }}>
        <UploadForm ref={uploadFormRef} storageRef={currentStorageRef} />
      </Modal>
      <Form form={form}>
        <Modal
          title="Tạo folder mới"
          okText="Tạo"
          cancelText="Hủy"
          visible={showCreateFolderModal}
          onOk={createFolder}
          onCancel={() => {
            form.resetFields();
            setShowCreateFolderModal(false);
          }}
        >
          <Typography.Text><b>Lưu ý</b>: Folder sẽ bị xóa đi sau đó nếu không chứa bất cứ gì.</Typography.Text>
          <Form.Item name={NAME_FOLDER_FIELD} rules={[{ required: true, message: "Vui lòng nhập tên foldder" }]}>
            <Input placeholder='Tên folder' />
          </Form.Item>
        </Modal>
        <Modal
          title="Nhận file chia sẻ"
          okText="Nhận"
          cancelText="Hủy"
          visible={showReceiveFilesModal}
          onOk={receiveFiles}
          onCancel={() => {
            form.resetFields();
            setShowReceiveFilesModal(false);
          }}
        >
          <Form.Item name={NAME_FOLDER_FIELD} rules={[{ required: true, message: "Vui lòng nhập tên foldder" }]}>
            <Input placeholder='Nhập share token' />
          </Form.Item>
        </Modal>
        <Modal
          title="Mã chia sẻ"
          okText="Xong"
          cancelText="Hủy"
          visible={showShareFilesModal}
          onOk={()=>{
            setShareFileToken("")
            setShowShareFilesModal(false);
          }}
          onCancel={()=>{
            setShareFileToken("")
            setShowShareFilesModal(false);
          }}
          
        >
          <Typography.Text>
            Copy mã bên dưới và gửi cho người nhận để chia sẻ file.
          </Typography.Text>
          <Typography.Paragraph copyable ellipsis>
            {shareFileToken}
          </Typography.Paragraph>
        </Modal>
      </Form>
      <a href="https://d1hjkbq40fs2x4.cloudfront.net/2016-01-31/files/1045.jpg" download ref={downloadRef}></a>
    </div >
  </Spin>
}


export default FileManager;