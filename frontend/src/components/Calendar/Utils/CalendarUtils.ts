import {
    addDays,
    endOfMonth,
    endOfWeek,
    format,
    formatISO,
    isValid,
    startOfDay,
    startOfMonth,
    startOfWeek,
    toDate,
} from "date-fns";
import {CalendarEvent} from "../../../redux/features/Calendar/CalendarAPI";
import {RRule} from "rrule";

export type DateArray = Date[];
export type DateMatrix = DateArray[];

export const weekNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];

export const monthNamesMap = [
    "Tháng một",
    "Tháng hai",
    "Tháng ba",
    "Tháng tư",
    "Tháng năm",
    "Tháng sáu",
    "Tháng bảy",
    "Tháng tám",
    "Tháng chín",
    "Tháng mười",
    "Tháng mười một",
    "Tháng mười hai",
];

export const dayOfTheWeekNames = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
];

export const DAY_OF_WEEK_NAMES = [
    "SUNDAY",
    "MONDAY",
    "TUESDAY",
    "WEDNESDAY",
    "THURSDAY",
    "FRIDAY",
    "SATURDAY",
];

export const DAY_OF_WEEK_VN = [
    "Chủ nhật",
    "Thứ hai",
    "Thứ ba",
    "Thứ tư",
    "Thứ năm",
    "Thứ sáu",
    "Thứ bảy",
]

export const DAY_OF_WEEK_NAMES_MAP = {
    "SUNDAY": "Chủ nhật",
    "MONDAY": "Thứ hai",
    "TUESDAY": "Thứ ba",
    "WEDNESDAY": "Thứ tư",
    "THURSDAY": "Thứ năm",
    "FRIDAY": "Thứ sáu",
    "SATURDAY": "Thứ bảy",
}

export type DayOfWeekName =
    | "SUNDAY"
    | "MONDAY"
    | "TUESDAY"
    | "WEDNESDAY"
    | "THURSDAY"
    | "FRIDAY"
    | "SATURDAY";

export const getLastMonthDate = (date: Date) => {
    const lastMonthTs = date.setDate(0);
    const firstDate = new Date(lastMonthTs).setDate(1);
    console.log("LASTMONT_DATE: ", firstDate);
    return new Date(firstDate);
};

export const getNextMonthDate = (date: Date) => {
    return addDays(endOfMonth(date), 1);
};

export const calculateHourDiff = (start: Date, end: Date) => {
    const diff = end.valueOf() - start.valueOf();
    const diffInHours = diff / 1000 / 60 / 60; // Convert milliseconds to Hours
    return diffInHours;
};
export const getMonthName = (date: Date) => {
    return monthNamesMap[date.getMonth()];
};

export const getDaysName = (date: Date) => {
    return DAY_OF_WEEK_VN[date.getDay()];
};

export const getEventStartTime = (event: Pick<CalendarEvent, "start">) => {
    const {
        start: {dateTime},
    } = event;
    if (typeof dateTime === "string") {
        return parseDate(dateTime);
    }
    return parseDate(dateTime.value);
};

export const getEventEndTime = (event: Pick<CalendarEvent, "end">) => {
    const {
        end: {dateTime},
    } = event;
    if (typeof dateTime === "string") {
        return parseDate(dateTime);
    }
    return parseDate(dateTime.value);
};

export const setEventStartTime = (
    event: Pick<CalendarEvent, "start">,
    date: Date
) => {
    if (typeof event.start.dateTime === "string") {
        event.start.dateTime = format(date, "PPPPpppp");
    } else {
        event.start.dateTime.value = date.getTime();
    }
};

export type DatePresentation = Date | string | number;

function isDate(date: Date | string | number): date is Date {
    return (date as Date).getHours !== undefined;
}

// * Make sure something passed in is a date (or convertable to a date)
export const parseDate: (datePresentation: Date | string | number) => Date = (
    datePresentation: Date | string | number
) => {
    if (isDate(datePresentation)) {
        return datePresentation;
    } else {
        const dateNumber = Number(datePresentation);
        if (isValid(dateNumber)) {
            return toDate(dateNumber);
        }
        return new Date(datePresentation);
    }
};
export const getHourFromTimeStamp: (date: Date | string) => number = (
    date: Date | string
) => {
    return parseDate(date).getHours();
};
// * Take in a date ==> HH:00  -> Used in calculating the event map key for more efficient rendering
// * Example:  getPaddedHourKeyFromTimeStamp(new Date()) => "01:54"
export const getPaddedHourKeyFromTimeStamp: (
    date: Date | string | number
) => string = (date: Date | string | number) => {
    return parseDate(date).toLocaleTimeString("en-GB").substring(0, 5);
};

// * Take in a date ==> YY:MM:DD:HH:00  -> Used in calculating the event map key for more efficient rendering
// * Example:  getPaddedHourKeyFromTimeStamp(new Date()) => "01:54"
export const getPaddedDateAndHourFromTimeStamp: (
    date: Date | string | number
) => string = (date: Date | string | number) => {
    // return parseDate(date).toISOString().substring(0,16).slice(0,-2).concat("00");
    return formatISO(parseDate(date).setMinutes(0));
};

export const getHourAndMinuteFromDate = (date: Date) => {
    return format(date, "HH:mm");
};
export const getDateFromString: (str: string) => Date = (str) => {
    return new Date(str);
};

// date.toLocaleTimeString('en-GB').substring(0,5)

function takeWeek(start = new Date()): (start?: Date) => DateArray {
    let date = startOfWeek(startOfDay(start));

    return function (): Date[] {
        const week = [...Array(7)].map((_, i) => addDays(date, i));
        date = addDays(week[6], 1);
        return week;
    };
}

//* Get days of month containing the date passed in
function takeMonth(start = new Date()): (start?: Date) => DateMatrix {
    let month: DateMatrix = [];
    let date = start;

    function lastDayOfRange(range: DateMatrix) {
        return range[range.length - 1][6];
    }

    return function (): DateMatrix {
        const weekGen = takeWeek(startOfMonth(date));
        const endDate = startOfDay(endOfWeek(endOfMonth(date)));
        month.push(weekGen());

        while (lastDayOfRange(month) < endDate) {
            month.push(weekGen());
        }

        const range = month;
        month = [];
        date = addDays(lastDayOfRange(range), 1);

        return range;
    };
}

type RruleStringModifier = (rrule: string, date: Date) => string;
export const concat_BY_HOUR_toRrule: RruleStringModifier = (rrule, date) => {
    return rrule.concat(`;BYHOUR=${date.getHours()}`);
};
export const concat_BY_MINUTE_toRrule: RruleStringModifier = (
    rrule: string,
    date: Date
) => {
    return rrule.concat(`;BYMINUTE=${date.getMinutes()}`);
};

export const addHourAndMinuteToRRuleString: RruleStringModifier = (
    rrule,
    date
) => {
    const rruleWithHour = concat_BY_HOUR_toRrule(rrule, date);
    const rrulwWithHourAndMinute = concat_BY_MINUTE_toRrule(rruleWithHour, date);
    return rrulwWithHourAndMinute;
};

export const getDatesFromRrule = (rrule: string) => {
    return RRule.fromString(rrule).all();
};

export {takeMonth, takeWeek};
