// @ts-ignore
import React from "react";
import { Button, Result, Space } from "antd";
import { useNavigate } from "react-router-dom";
import { ROUTING_CONSTANTS } from "../../navigation/ROUTING_CONSTANTS";
import BackButton from "../../components/ActionButton/BackButton";

const UnAuthorizedPage = () => {
	const navigate = useNavigate();
	return (
		<Result
			status="403"
			title="403"
			subTitle="Bạn đã cố gắng truy cập vào một trang chưa được cấp phép. Có khả năng ai đó với quyền quản trị đã thay đổi quyền hạn của bạn (Hãy đăng nhập lại để chắc chắn hơn). Nếu bạn nghĩ đây là lỗi hệ thống. Hãy liên hệ với quản trị viên của bạn để được giúp đỡ nhé :) "
			extra={
				<Space>
					<BackButton
						type="primary"
						onClick={() => navigate(ROUTING_CONSTANTS.DASHBOARD)}
					>
						Quay trở lại
					</BackButton>

					<Button
						type="primary"
						onClick={() => navigate(ROUTING_CONSTANTS.LOGIN_PAGE)}
					>
						Login
					</Button>
				</Space>
			}
		/>
	);
};

// @ts-ignore
export default UnAuthorizedPage;
