import {utcToZonedTime} from "date-fns-tz";
import {format} from "date-fns";
import {DatePresentation, parseDate,} from "../../components/Calendar/Utils/CalendarUtils";
import formatRelative from "date-fns/formatRelative";
import vi from "date-fns/locale/vi";
import {toNumber} from "lodash";

const VN_TIME_ZONE = "Asia/Ho_Chi_Minh";

export const HOUR_MINUTE_PATTERN = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;

export const DATE_TIME_FORMAT = "DD-MM-yyyy, HH:mm:ss";
export const DATE_FORMAT = "DD-MM-yyyy";

const formatDateTimeInTable = (date: Date | number | undefined | string) => {
    if (!date) return "";
    return format(new Date(date), "dd-MM-yyyy");
};

const formatDateTimeByDash = (date: Date | number | undefined | string) => {
	if (!date) return "";
	return format(new Date(date), "dd/MM/yyyy");
};

const formatDateTime = (date: Date | number | undefined | string) => {
	if (!date) return "";
	return format(new Date(date), "dd-MM-yyyy HH:mm:ss");
};


export const getHourMinute = (date: Date | undefined) => {
    if (date == undefined) {
        return "";
    }
    return new Date(date).toLocaleTimeString('vi-VN', {hour: '2-digit', minute: '2-digit'});
}

const timeStampHelper = {
	getCurrentTimestampVNTimezone: () =>
		utcToZonedTime(new Date(), "Europe/Berlin"),
	formatDateTimeInTable,
	formatDateTime,
	fromUtcDateToHoChiMinhTimeZone: (date: Date) =>
		utcToZonedTime(date, "Europe/Berlin"),
	convertToMatchTimeStampBackend: (date: DatePresentation) => {
		const timeZone = VN_TIME_ZONE;
		const zonedDate = utcToZonedTime(parseDate(date), timeZone);
		return zonedDate;
	},
	displayRelativeTime: (date: DatePresentation) => {
		return formatRelative(parseDate(date), new Date(), { locale: vi });
	},
    createDateTime: (date: Date, hourMinute: String) => {
        const d = new Date(date);
        return  new Date(Date.UTC(d.getFullYear(),
            d.getMonth(),
            d.getDate(),
            toNumber(hourMinute.split(":")[0]),
            toNumber(hourMinute.split(":")[1])));
    },
	formatDateTimeByDash
};

export default timeStampHelper;
