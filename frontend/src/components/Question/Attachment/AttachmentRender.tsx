import { FileTextOutlined } from "@ant-design/icons";
import { Col, Row, Space } from "antd";
import { Attachment } from "./AttachmentQuestion";

const AttachmentRender = ({ attachment }: { attachment: Attachment }) => {
  const renderVideo = (attachment: Attachment) => {
    return <Space style={{ maxWidth: '80%' }}>
      <video width={600} style={{ maxWidth: '100%' }} controls>
        <source src={attachment.url} />
      </video>
    </Space>
  }

  const renderAudio = (attachment: Attachment) => {
    return <Space style={{display: "flex", flexDirection: "column"}}>
      <audio controls>
        <source src={attachment.url} />
      </audio>
      <span>{attachment.name}</span>
    </Space>
}

  const renderDocument = (attachment: Attachment) => {
    return <Row justify="start">
    <Col span={24}>
      <Space align="start">
        <a href={attachment.url} download={attachment.name}>
          <FileTextOutlined />
          {attachment.name}
        </a>
      </Space>
    </Col>
  </Row>
  }

  if (attachment.type == "Video") return renderVideo(attachment);
  if (attachment.type == "Audio") return renderAudio(attachment);
  return renderDocument(attachment);
}

export default AttachmentRender;