import React, { CSSProperties, useEffect, useState } from "react";
import { FieldProps } from "formik";
import { FileUploadProps, } from "../../ImageChooser/FileMediaUploadWithFirebase";
import { FormItem } from "formik-antd";
import { Button, Modal, Typography } from "antd";
import {DeleteOutlined, UploadOutlined} from "@ant-design/icons";
import FileManager from "@components/FileManager/FileManager";
import './GeneralFileUploadField.less';
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {getFileNameFromFirebaseUrl} from "@components/FilePreview/Utils/FileUtils";
import FileIconPreviewByFileName from "@components/FilePreview/FileIconPreviewByFileName";

type FileUploadFieldProp = FieldProps<any> &
    FileUploadProps & {
        label?: string;
        style?: CSSProperties;
        required?: boolean;
        displayedLayout?: string;
        allowedType?: string[];
    };
const GeneralFileUploadField = ({
    field,
    displayedLayout,
    allowedType,
    form: {
        touched,
        errors,
        setFieldValue,
        setFieldError,
        handleBlur,
        handleChange,
        setTouched,
        setFieldTouched,
        values
    },
    ...otherProps
}: FileUploadFieldProp) => {
    const { name, value, onChange, onBlur } = field;
    const errorMsg = touched[name] && errors[name];
    const [showFileBrowser, setShowFileBrowser] = useState(false);
    const [file, setFile] = useState('');

    const deleteChosenFile = () => {
        setFile('');
        setFieldValue(name, '');
    }

    useEffect(() => {
        if (values[name]) {
            setFile(values[name]);
        }
    }, values);

    return (
        <>
            {/* <FastFild> */}
            <FormItem
                required={otherProps?.required}
                label={otherProps?.label || ""}
                name={name}
                className={displayedLayout ? 'preview-icon-file-wrapper-row-layout' : ''}
            >
                <Button icon={<UploadOutlined />} onClick={() => setShowFileBrowser(true)}>
                    Chọn tệp tải lên
                </Button>
                {allowedType == undefined && file !== '' && <><FileIconPreviewByFileName
                    filename={getFileNameFromFirebaseUrl(file)}
                    showName={true}
                    layout={displayedLayout ? displayedLayout : 'column'}
                    size={24}
                /><DeleteOutlined
                    className={'delete-icon-wrapper'}
                    onClick={() => {
                        deleteChosenFile();
                    }}/>
                </>
                }
                <Modal visible={showFileBrowser} width={"80%"} footer={null} onCancel={() => setShowFileBrowser(false)}>
                    <div style={{ padding: "25px 20px 0px 20px" }}>
                        <FileManager onSelectFile={({ fileUrl, type }) => {
                            if ((allowedType != undefined && allowedType.includes(type.toLocaleLowerCase())) || allowedType == undefined) {
                                setShowFileBrowser(false);
                                console.log(fileUrl);
                                setFieldValue(name, fileUrl);
                                setFile(fileUrl);
                            } else {
                                AntdNotifier.error("Chỉ hỗ trợ các loại file: " + allowedType.join(", "));
                            }
                        }} />
                    </div>
                </Modal>
                <Typography.Text type={"danger"}>
                    <>
                        {errorMsg}
                    </>
                </Typography.Text>
            </FormItem>
        </>
    );
};

export default GeneralFileUploadField;