import {Card, Col, Row, Space} from "antd";
import React, {useEffect} from "react";
import {useParams} from "react-router-dom";

import AddButton from "../../../../components/ActionButton/AddButton";
import EditButton from "../../../../components/ActionButton/EditButton";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import useClassCourseInformation from "../../../../hooks/Routing/useClassCourseInformation";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import {
    useGetClassScheduleAddedByHandQuery,
    useGetClassScheduleQuery,
    useGetClassSessionsQuery,
    useGetFullListSessionForStudentQuery, useRefetchClassSchedulerMutation,
} from "@redux/features/Class/ClassTimeTable/ClassTimeTableAPI";
import {
    classTimeTableActions,
    classTimeTableSelectors,
    classTimeTableSlice
} from "@redux/features/Class/ClassTimeTable/ClassTimeTableSlice";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import LoadingPage from "../../../UtilPages/LoadingPage";
import ClassSessionCard from "./ClassSessionCard/ClassSessionCard";
import ClassScheduleFormModal from "./Form/ClassScheduleFormModal";
import ClassSessionFormModal from "./Form/ClassSessionFormModal";
import {DEFAULT_LOAD_MORE} from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";

import './ClassTimeTable.less';
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import SectionHeader from "../../../../components/SectionHeader/SectionHeader";
import InformationRow from "@components/InformationRow/InformationRow";
import {DAY_OF_WEEK_NAMES_MAP} from "@components/Calendar/Utils/CalendarUtils";
import {useRefetchClassInformationMutation} from "@redux/features/Class/ClassAPI";

const ClassTimeTable = () => {
    const { classId } = useParams();
    const dispatch = useAppDispatch();

    const [closeSidebar] = useClassSidebar();
    const { classInfo, isFetchingClass, isSuccessClass, isErrorClass } =
        useClassCourseInformation(Number(classId));
    const shouldSkip = () => {
        return isNaN(Number(classId));
    };

    const [refetchClassInformation] = useRefetchClassInformationMutation();
    const [refetchClassScheduler] = useRefetchClassSchedulerMutation();

    const dateTime = useAppSelector(classTimeTableSelectors.selectDateTime);
    const numsOfPrevious = useAppSelector(classTimeTableSelectors.selectNumsOfPrevious);
    const numsOfAfter = useAppSelector(classTimeTableSelectors.selectNumsOfAfter);

    // * Teacher related stuffs

    const isTeacherOfThisClass = true;
    const {
        data: classSchedule,
        isSuccess,
        isError,
        isFetching,
    } = useGetClassScheduleQuery(
        { classId: Number(classId) },
        {
            skip: shouldSkip(),
        }
    );

    const {
        data: manuallyAddedClassSessions,
        isSuccess: manuallyAddedClassSessionSuccess,
        isFetching: manuallyAddedClassSessionFetching,
        isError: manuallyAddedClassSessionError,
    } = useGetClassScheduleAddedByHandQuery(
        { classId: Number(classId) },
        {
            skip: shouldSkip(),
        }
    );
    const {
        data: classSessions,
        isSuccess: classSessionSuccess,
        isFetching: classSessionFetching,
        isError: classSessionError,
    } = useGetClassSessionsQuery(
        {
            classId: Number(classId),
            dateTime: dateTime,
            numsOfPrevious: numsOfPrevious,
            numsOfAfter: numsOfAfter
        },
        {
            skip: shouldSkip(),
        }
    );

    useEffect(() => {
        refetchClassInformation({classId: Number(classId)});
        refetchClassScheduler();
    }, []);


    // * Student related stuff
    const isStudentOfThisClass = false;
    const {
        data: studentClassSessions,
        isSuccess: studentClassSessionSuccess,
        isFetching: studentClassSessionFetching,
        isError: studentClassSessionError,
    } = useGetFullListSessionForStudentQuery(
        { classId: Number(classId) },
        {
            skip: shouldSkip(),
        }
    );

    if (isFetchingClass) {
        return <LoadingPage />;
    } else if (isErrorClass) {
        return <ApiErrorWithRetryButton onRetry={() => {
        }} />;
    } else if (
        isTeacherOfThisClass ||
        isSuccessClass ||
        isSuccess ||
        manuallyAddedClassSessionSuccess ||
        classSessionSuccess
    ) {
        return (
            <div>
                <ContentHeader
                    title={`Thời khóa biểu`}
                    action={
                        <Space>
                            {classInfo?.status != 'ENDED'  && <><PermissionRenderer permissions={[PermissionEnum.UPDATE_CLASS_SCHEDULER]}>
                                <EditButton
                                    title={"Thiết lập thời khóa biểu"}
                                    onClick={() => {
                                        if (!classSchedule) {
                                            AntdNotifier.error(
                                                "Thời khóa biểu của lớp chưa đươc khỏi tạo"
                                            );
                                            return;
                                        }
                                        dispatch(
                                            classTimeTableSlice.actions.editClassSchedule(classSchedule)
                                        );
                                    }}
                                />
                            </PermissionRenderer>
                            <PermissionRenderer permissions={[PermissionEnum.CREATE_CLASS_SESSION]}>
                                <AddButton
                                    title="Thêm buổi học"
                                    onClick={() => {
                                        dispatch(classTimeTableSlice.actions.addNewClassSession());
                                    }}
                                />
                            </PermissionRenderer></>}
                        </Space>
                    }
                />
                {
                    classSessions?.hasPrevious || classSessions?.hasAfter || (classSessions?.sessions != undefined && classSessions?.sessions.length > 0)
                    ? <>
                            <SectionHeader title={"Lịch học hàng tuần"}/>
                            <Row justify="center">
                                {classSessions?.daysOfWeek != undefined ? <InformationRow title={'Ngày'} value={"Giờ"}/> : 'Chưa có thời khóa biểu'}
                                {classSessions?.daysOfWeek?.split("[]").map(session => <InformationRow title={DAY_OF_WEEK_NAMES_MAP[session.split("|")[0]]} value={session.split("|")[1]}/>)}
                            </Row>
                            <SectionHeader title={"Danh sách buổi học"}/>
                            {
                                <Row>
                                    <Col
                                        span={24}
                                        style={{
                                            textAlign: "center",
                                        }}
                                    >
                                        {classSessions?.hasPrevious ? (
                                                <a className={'load-more-btn-wrapper'}>
                                                    <button
                                                        className={'load-more-btn'}
                                                        onClick={() => dispatch(classTimeTableActions.changeNumsOfPrevious(DEFAULT_LOAD_MORE))}
                                                    >
                                                        Xem thêm buổi học trước
                                                    </button>
                                                </a>
                                            ) :
                                            <div className={'load-more-default-text'}>Không còn buổi học nào để hiển thị</div>
                                        }
                                        <Space
                                            direction="vertical"
                                            size={"large"}
                                            style={{ width: '90%' }}
                                        >
                                            {classSessions?.sessions.map((item, index) => {
                                                // console.log('item', item);
                                                return <ClassSessionCard {...item} type="AUTOMATIC" />;
                                            })}

                                            {/*{manuallyAddedClassSessions?.map((item, index) => {*/}
                                            {/*    return <ClassSessionCard {...item} type="MANUAL" />;*/}
                                            {/*})}*/}
                                        </Space>
                                        {classSessions?.hasAfter ? (
                                                <a className={'load-more-btn-wrapper'}>
                                                    <button
                                                        className={'load-more-btn'}
                                                        onClick={() => dispatch(classTimeTableActions.changeNumsOfAfter(DEFAULT_LOAD_MORE))}
                                                    >
                                                        Xem thêm buổi học sau
                                                    </button>
                                                </a>
                                            ) :
                                            <div className={'load-more-default-text'}>Không còn buổi học nào để hiển thị</div>
                                        }
                                    </Col>
                                </Row>
                            }
                      </>
                    : (
                            <Row justify="center">
                                <Col span={24}>
                                    <Card title="Lớp học chưa được thiết lập thời khóa biểu.">
                                        <span>
                                            <PermissionRenderer permissions={[PermissionEnum.UPDATE_CLASS_SCHEDULER]}>
                                                Chọn <b>Thiết lập thời khóa biểu</b> để thiết lập thời khóa biểu cho lớp học hoặc <b>Thêm</b> để thêm từng buổi học.
                                            </PermissionRenderer>
                                        </span>
                                    </Card>
                                </Col>
                            </Row>
                        )
                }
                <ClassScheduleFormModal />
                <ClassSessionFormModal />
            </div>
        );
    } else if (isStudentOfThisClass || studentClassSessionSuccess) {
        return (
            <>
                {studentClassSessions?.map((item, index) => {
                    return (
                        <>
                            <ClassSessionCard {...item} type={"AUTOMATIC"} />
                        </>
                    );
                })}
            </>
        );
    } else {
        return <>?????</>;
    }
};

export default ClassTimeTable;
