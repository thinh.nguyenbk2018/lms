import React from 'react';
import PropTypes from 'prop-types';

import {DAYS} from '../../../constants/index';
import translateLabel from '../../../utils/translateLabel';
import {Col, Input, Row, Select, Space} from "antd";
import {isNaN} from "lodash";

const RepeatMonthlyOnThe = ({
                                id,
                                mode,
                                onThe,
                                hasMoreModes,
                                handleChange,
                                translations
                            }) => {
    const isActive = mode === 'on the';

    return (
        <Row gutter={[5,10]} className={`form-group row d-flex align-items-sm-center ${!isActive && 'opacity-50'}`}>
            <Col span={24}>
                <Space>
                    {translateLabel(translations, 'repeat.monthly.on_the')}
                    {hasMoreModes && (
                        <Input
                            id={id}
                            type="radio"
                            name="repeat.monthly.mode"
                            aria-label="Repeat monthly on the"
                            value="on the"
                            checked={isActive}
                            onChange={handleChange}
                        />
                    )}
                </Space>
            </Col>

            <Col>
                <Select
                    id={`${id}-which`}
                    name="repeat.monthly.onThe.which"
                    aria-label="Repeat monthly on the which"
                    className="form-control"
                    value={onThe.which}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        // Convert input from a string to a number
                        const target = {name: "repeat.monthly.onThe.which", value};
                        handleChange({target})
                    }}
                >
                    <Select.Option value="First">{translateLabel(translations, 'numerals.first')}</Select.Option>
                    <Select.Option value="Second">{translateLabel(translations, 'numerals.second')}</Select.Option>
                    <Select.Option value="Third">{translateLabel(translations, 'numerals.third')}</Select.Option>
                    <Select.Option value="Fourth">{translateLabel(translations, 'numerals.fourth')}</Select.Option>
                    <Select.Option value="Last">{translateLabel(translations, 'numerals.last')}</Select.Option>
                </Select>
            </Col>

            <Col>
                <Select
                    id={`${id}-day`}
                    name="repeat.monthly.onThe.day"
                    aria-label="Repeat monthly on the day"
                    className="form-control"
                    value={onThe.day}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        // Convert input from a string to a number
                        const target = {name: "repeat.monthly.onThe.day", value};
                        handleChange({target})
                    }}
                >
                    {DAYS.map(day => <Select.Option key={day}
                                             value={day}>{translateLabel(translations, `days.${day.toLowerCase()}`)}</Select.Option>)}
                </Select>
            </Col>

        </Row>
    );
};
RepeatMonthlyOnThe.propTypes = {
    id: PropTypes.string.isRequired,
    mode: PropTypes.oneOf(['on', 'on the']).isRequired,
    onThe: PropTypes.shape({
        which: PropTypes.oneOf(['First', 'Second', 'Third', 'Fourth', 'Last']).isRequired,
        day: PropTypes.oneOf(DAYS).isRequired,
    }).isRequired,
    hasMoreModes: PropTypes.bool.isRequired,
    handleChange: PropTypes.func.isRequired,
    translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default RepeatMonthlyOnThe;
