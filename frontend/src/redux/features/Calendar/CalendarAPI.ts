import {apiSlice} from '../CentralAPI';
import {EventHashMap} from './CalendarSlice';
import {calendarEventDtoListToHashMap, calendarEventListToHashMap} from './Utils';

type Calendar = {
	kind: string;
	etag: string;
	id: string;
	summary: string;
	timeZone: string;
};

type CalendarTime = {
	dateTime:
		| string
		| {
				value: number;
				dateOnly: boolean;
				timeZoneShift: number;
		  };
	timeZone: string;
};
type EventCreateRequest = {
	calendarId: string;
	id?: string;
	start: CalendarTime;
	end: CalendarTime;
	summary: string;
	location?: string;
	description: string;
};

export type EventType = "CLASS_SESSION" | "QUIZ_DEADLINE" | "PERSONAL" | "QUIZ_OPEN";

export type CalendarEvent = {
	id: string;
	kind: string;
	etag: string;
	status: string;
	htmlLink: string;
	creator: {};
	organizer: {};
	start: CalendarTime;
	end: CalendarTime;
	//* Below times are timestamp as 1996-12-19T16:39:57-08:00 - ISO Standard string
	created: string | {};
	updated: string | {};
	summary: string;
	description: string;
	location: string;
	iCalUID?: any;
	reminders?: any;
	sequence?: any;
	eventType: EventType;
	recurrence?: string[];
	// * We can have other nice things such as attachments, meet room links
};

//  * The event is not always formed the same way multiple times.
export type OptionalEvent = Partial<CalendarEvent>;

export type EventDTO = {
	event: CalendarEvent;
	eventType: EventType;
};

const calendarApiWithInvalidateTags = apiSlice.enhanceEndpoints({
	addTagTypes: ["Calendar", "Event", "ClassCalendar"],
});

const calendarAPI = calendarApiWithInvalidateTags.injectEndpoints({
	endpoints: (build) => ({
		getCalendarInformation: build.query<Calendar, string>({
			query: (id) => `calendars/${id}`,
			providesTags: ["Calendar"],
		}),
		getCalendarEvents: build.query<EventHashMap, string>({
			query: (calendarId) => `calendars/${calendarId}/events`,
			transformResponse: (
				baseQueryReturnValue: CalendarEvent[],
				_meta: {} | undefined,
				_arg
			) => {
				return calendarEventListToHashMap(baseQueryReturnValue);
			},
			providesTags: [{ type: "Event", id: "LIST_EVENT" }],
		}),
		getPersonalCalendar: build.query<EventHashMap, number>({
			query: (userId) => `calendars/user/${userId}`,
			transformResponse: (
				baseQueryReturnValue: EventDTO[],
				_meta: {} | undefined,
				_arg
			) => {
				return calendarEventDtoListToHashMap(baseQueryReturnValue);
			},
			providesTags: ["Calendar"],
		}),
		getClassCalendarEvent: build.query<EventHashMap, number>({
			query: (classId) => `/classes/${classId}/calendars`,
			transformResponse: (
				baseQueryReturnValue: EventDTO[],
				_meta: {} | undefined,
				_arg
			) => {
				return calendarEventDtoListToHashMap(baseQueryReturnValue);
			},
			providesTags: ["Calendar"],
		}),
		addEvent: build.mutation<
			{},
			Partial<CalendarEvent> & { calendarId: string }
		>({
			query(body) {
				return {
					url: `calendars/${body.calendarId}/events`,
					method: "POST",
					body,
				};
			},
			invalidatesTags: [{ type: "Event", id: "LIST_EVENT" }],
		}),
		updateEvent: build.mutation<{}, Partial<EventCreateRequest>>({
			query(body) {
				return {
					url: `calendars/${body.calendarId}/events/${body.id}`,
					method: "PUT",
					body,
				};
			},
			invalidatesTags: [{ type: "Event", id: "LIST_EVENT" }],
		}),
		refetchEvents: build.mutation<any,  void>({
			queryFn: () => ({ data: null }),
			invalidatesTags: ["Calendar"]
		})
	}),
	overrideExisting: false,
});

export const {
	useGetCalendarInformationQuery,
	useGetCalendarEventsQuery,
	useAddEventMutation,
	useUpdateEventMutation,
	useGetPersonalCalendarQuery,
	useGetClassCalendarEventQuery,
	useRefetchEventsMutation
} = calendarAPI;
