// import React from 'react';
// import {Col, Row} from "antd";
// import {Select} from "antd/es";
// import moment from "moment";
// import momentTimezone from "moment-timezone";
//
// const {Option,OptGroup} = Select;
// const TimeZoneSelector = () => {
//     const renderTimeZoneOptions = () => {
//         const timezones = moment.tz.names();
//         let mappedValues = {};
//         let regions : any[]= [];
//
//         timezones.map(timezone => {
//             const splitTimezone = timezone.split("/");
//             const region = splitTimezone[0];
//             if (!mappedValues[region]) {
//                 mappedValues[region] = [];
//                 regions.push(region);
//             }
//             mappedValues[region].push(timezone);
//         });
//         return regions.map(region => {
//             const options = mappedValues[region].map(timezone => {
//                 return <Option key={timezone}>{timezone}</Option>;
//             });
//             return (
//                 <OptGroup
//                     key={region}
//                     title={<div style={{ fontSize: 30 }}>{region}</div>}
//                  label={"tz-GROUP"}>
//                     {options}
//                 </OptGroup>
//             );
//         });
//     }
//     return (
//         <div className="App">
//             <h1>Timezone!</h1>
//             <h2>{`Your Timezone: ${momentTimezone.tz.guess()}`}</h2>
//             <Row>
//                 <Col span={6} className="row-title">
//                     Timezone List:
//                 </Col>
//                 <Col span={18}>
//                     <Select
//                         style={{ width: 400 }}
//                         defaultValue={momentTimezone.tz.guess()}
//                     >
//                         {renderTimeZoneOptions()}
//                     </Select>
//                 </Col>
//             </Row>
//         </div>
//     );
// }};
//
// export default TimeZoneSelector;


export {}