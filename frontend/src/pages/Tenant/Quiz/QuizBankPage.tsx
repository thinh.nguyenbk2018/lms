// import React, {useState} from 'react';
// import ContentHeader from "../../components/ContentHeader/ContentHeader";
// import {Button, Checkbox, Modal, Row, Space} from "antd";
// import {Link, useNavigate} from "react-router-dom";
// import CustomTable from "../../components/CustomTable/CustomTable";
// import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";
// import {
//     Test,
//     TEST_CONTENT_PATH_ITEM,
//     useCreateTestContentMutation,
//     useDeleteTestMutation,
//     useGetTestContentListQuery,
//     useLazyGetTestContentListQuery
// } from "../../redux/features/Quiz/examAPI";
// import {Field, Formik} from 'formik';
// import {Form, FormItem, Input, Select} from 'formik-antd';
// import * as Yup from 'yup';
// import {replacePathParams} from "../../utils/PathPatcherHepler/PathPatcher";
// import CourseSearchSelectField from "../../components/CustomFields/CourseSearchSelectField/CourseSearchSelectField";
// import ApiErrorWithRetryButton from "../../components/Util/ApiErrorWithRetryButton";
// import {addIconSvg, deleteIconSvg, viewIconSvg} from '../../components/Icons/SvgIcons';
// import AntdNotifier from '../../utils/AntdAnnouncer/AntdNotifier';
// import _ from 'lodash';
// import {useAppSelector} from '../../hooks/Redux/hooks';
// import {createTestContentActions, createTestContentSelectors} from '../../redux/features/Quiz/CreateQuizSlice';
// import TableSearch from '../../components/Search/TableSearch';
// import {changeCriteria} from '../../redux/features/Course/CourseSlice';
// import LoadingPage from '../UtilPages/LoadingPage';
// import {extractPaginationInformation} from '../../components/CustomTable/TableUtils';
// import {Typography} from 'antd/es';
// import {number} from 'prop-types';
//
// export const testContentMetaSchema = Yup.object({
//     title: Yup.string().required("Đừng để trống tên của đề (Cần thiết để nhanh chóng tìm ra đề)"),
//     description: Yup.string().required("Vui lòng mô tả ngắn gọn đề kiểm tra này"),
//     courseId: Yup.number().required("Vui lòng chọn khóa mà đề thuộc vào"),
// })
//
//
// const MODEL_KEY = {
//     CREATE_TEST: "CREATE_TEST" as const,
//     CONFIRM_DELETE_TEST: "DELETE_TEST" as const
// }
//
//
// const QuizBankPage = () => {
//     // * States
//     const [modals, setModals] = useState<{
//         [MODEL_KEY.CREATE_TEST]: boolean,
//         [MODEL_KEY.CONFIRM_DELETE_TEST]: boolean
//     }>({
//         [MODEL_KEY.CREATE_TEST]: false,
//         [MODEL_KEY.CONFIRM_DELETE_TEST]: false,
//     });
//     const searchCriteria = useAppSelector(createTestContentSelectors.selectSearchCriteria)
//     // * Queries
//     const [triggerFecthList, {
//         data: testContentList,
//         isSuccess: isContentListFetched,
//         isFetching: isContentListFetching,
//         isError: isContentFetchError
//     }] = useLazyGetTestContentListQuery();
//
//     const [deleteTestContent] = useDeleteTestMutation();
//     const [createNewTestContent, {
//         data: testContentCreated,
//         isSuccess,
//         isError,
//         isLoading
//     }] = useCreateTestContentMutation();
//
//
//     const changModalVisibility = (key: keyof typeof MODEL_KEY, value: boolean) => {
//         const clonedState = _.cloneDeep(modals);
//         clonedState[MODEL_KEY[key]] = value;
//         setModals(clonedState);
//     }
//
//
//     const deleteTestContentHelper = (id: number, cascadeQuestion: boolean = false) => {
//         deleteTestContent({testIdToDelete: id})
//             .unwrap()
//             .then(res => {
//                 AntdNotifier.success("Xóa Đề thành công", "Thông báo", 1);
//             })
//             .catch(err => {
//                 AntdNotifier.error("Xóa chương trình thất bại", "Thông báo", 1);
//             })
//     }
//
//
//     // * Hooks
//     const navigate = useNavigate();
//
//     React.useEffect(() => {
//         triggerFecthList(searchCriteria);
//
//         return () => {
//
//         }
//     }, [searchCriteria])
//
//
//     // * Quiz List Table State
//     const columns = [
//         {
//             title: "Tên",
//             key: "title",
//             dataIndex: "title",
//             width: "20%",
//         },
//         {
//             title: "Mô tả",
//             key: "description",
//             dataIndex: "description",
//             width: "20%"
//         },
//         {
//             title: "Thuộc khóa",
//             key: "courseName",
//             dataIndex: "courseName",
//             width: "40%",
//             render: (data: any, record: any, index: any) => {
//                 return <>
//                     <Typography.Link onClick={() => {
//                         navigate(replacePathParams(ROUTING_CONSTANTS.COURSE_DETAIL, [":id", record.id]))
//                     }}>
//                         {data}
//                     </Typography.Link>
//
//                 </>
//             }
//         },
//         {
//             title: "",
//             key: "action",
//             render: (record: any) => (
//                 <>
//                     <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
//                         <Link style={{marginRight: '1rem'}}
//                               to={`${replacePathParams(ROUTING_CONSTANTS.TEST_CONTENT_DETAIL, [":testId", record.id])}`}>
//                             {viewIconSvg}
//                         </Link>
//                         <Button
//                             type={"link"}
//                             className={'delete-item-btn'}
//                             onClick={() => changModalVisibility("CONFIRM_DELETE_TEST", true)}
//                         >
//                             {deleteIconSvg}
//                         </Button>
//                     </div>
//                 </>
//             ),
//         },
//     ];
//
//     // if (isSuccess) {
//     //     if (!testContentCreated || isNaN(Number(testContentCreated))) {
//     //         return <Row align={"middle"} justify={"center"}>
//     //             <ApiErrorWithRetryButton onRetry={() => { }} />;
//     //         </Row>
//     //     }
//     //     navigate(replacePathParams(ROUTING_CONSTANTS.TEST_CONTENT_DETAIL, [`:${TEST_CONTENT_PATH_ITEM}`, Number(testContentCreated)]));
//     // }
//
//
//     return (
//         <>
//             <ContentHeader
//                 title={"Ngân hàng đề"}
//                 action={
//                     <Space>
//                         <div className={'search-container'}>
//                             <TableSearch
//                                 searchFields={[{title: 'Từ khóa', key: 'key'}]}
//                                 defaultFilters={{'isPublished': null}}
//                                 searchCriteria={searchCriteria}
//                                 changeCriteria={createTestContentActions.changeCriteria}
//                             />
//                             <Button type="primary" onClick={() => {
//                                 changModalVisibility("CREATE_TEST", true)
//                             }}> Thêm một đề mới</Button>
//                         </div>
//                     </Space>
//                 }
//             />
//
//             <Formik initialValues={{}} validationSchema={testContentMetaSchema} onSubmit={(values, hepler) => {
//                 console.log("Tried creating test: ", values);
//                 const valuesToSubmit = {
//                     ...values,
//                     courseId: Number(courseId)
//                 }
//                 createNewTestContent(values);
//             }}>
//                 {({values, errors, touched, handleSubmit, dirty, isValid, resetForm}) => {
//                     return <Form>
//                         <Modal title="Thông tin cơ bản cho đề" visible={modals.CREATE_TEST} onOk={() => {
//                             createNewTestContent(values);
//                         }}
//                                onCancel={() => {
//                                    resetForm();
//                                    changModalVisibility("CREATE_TEST", false);
//                                }} okButtonProps={{disabled: !isValid || !dirty}}>
//
//                             <FormItem name={"title"} label={"Tiêu đề"}>
//                                 <Input name={"title"}/>
//                             </FormItem>
//                             <FormItem name={"description"} label={"Mô tả"}>
//                                 <Input name={"description"}/>
//                             </FormItem>
//                             <Field name={"courseId"} placeholder={"Type article content"}
//                                    component={CourseSearchSelectField} label={"Khoá"}/>
//                         </Modal>
//                     </Form>;
//                 }}
//             </Formik>
//             {
//                 isContentListFetched && <CustomTable
//                     columns={columns}
//                     data={testContentList?.listData || []}
//                     searchCriteria={searchCriteria}
//                     metaData={extractPaginationInformation(testContentList)}
//
//                 />
//             }
//             {
//                 isContentListFetching && <LoadingPage/>
//             }
//             {
//                 isContentFetchError && <Row align={"middle"} justify={"center"}>
//                 <ApiErrorWithRetryButton onRetry={() => {}} />;
//                 </Row>
//
//             }
//         </>
//     );
// };
//
// export default QuizBankPage;

export {}