"use strict";
exports.__esModule = true;
/* eslint-disable @typescript-eslint/no-unused-vars */
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var ContentHeader_1 = require("../../../../components/ContentHeader/ContentHeader");
var ApiErrorWithRetryButton_1 = require("../../../../components/Util/ApiErrorWithRetryButton");
var useCourseSidebar_1 = require("../../../../hooks/Routing/useCourseSidebar");
var AntdNotifier_1 = require("../../../../utils/AntdAnnouncer/AntdNotifier");
var CourseTeacher = function () {
    // * Class specif routes
    var courseId = react_router_dom_1.useParams().courseId;
    var _a = useCourseSidebar_1["default"](), forceCloseSidebar = _a.forceCloseSidebar, courseDetail = _a.courseDetail, isCourseDetailError = _a.isCourseDetailError, isCourseDetailFetching = _a.isCourseDetailFetching, isCourseDetailSuccess = _a.isCourseDetailSuccess;
    var navigate = react_router_dom_1.useNavigate();
    if (!courseId || isNaN(Number(courseId))) {
        AntdNotifier_1["default"].error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
        return (react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement(ApiErrorWithRetryButton_1["default"], { onRetry: function () { } })));
    }
    return react_1["default"].createElement(ContentHeader_1["default"], { title: "Teacher of " + (courseDetail === null || courseDetail === void 0 ? void 0 : courseDetail.name) });
};
exports["default"] = CourseTeacher;
