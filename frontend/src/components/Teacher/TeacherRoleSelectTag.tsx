import { TeacherRole } from "@redux_features/Class/TeacherRole.1";
import { Menu, Tag } from "antd";

type TeacherRoleMenuTagItem = {
    label: JSX.Element;
    key: TeacherRole;
};

type RoleChangeMenuProps = {
    onSelect: (role: TeacherRole) => void;
};

const TeacherRoleSelectTagMenu = (props: RoleChangeMenuProps) => {
    const { onSelect } = props;
    const menuItems: TeacherRoleMenuTagItem[] = [
        {
            label: <Tag color="blue"> Teacher </Tag>,
            key: "TEACHER" as const,
        },
        {
            label: <Tag color="orange"> Teacher Assistant</Tag>,
            key: "TEACHER_ASSISTANT" as const,
        },
    ];
    return (
        <Menu
            items={menuItems}
            onClick={({ key }) => {
                onSelect(key as TeacherRole);
            }}
        />
    );
};

export default TeacherRoleSelectTagMenu;
