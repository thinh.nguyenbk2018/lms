import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "@redux_store";

const CLASS_SLICE_KEY = "class";

const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice()
}

export const classSlice = createSliceEnhancedWithSearch({
    name: CLASS_SLICE_KEY,
    initialState,
    reducers: {},
});

export const classSelector = {
    selectSearchCriteria: (state: RootState) => state.class.searchCriteria
}

export const {changeCriteria} = classSlice.actions;