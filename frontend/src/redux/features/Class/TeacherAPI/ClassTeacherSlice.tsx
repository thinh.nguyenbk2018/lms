import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Staff } from "@redux_features/Staff/StaffAPI";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { ID } from "redux/interfaces/types";
import { RootState } from "../../../store/store";
import { ClassMemberType } from "../ClassType";
import { Teacher } from "./ClassTeacherAPI";

type ClassTeacherOpeningModal =
    | "ADD_TEACHER_TO_CLASS"
    | "ASSIGN_TEACHER_TO_SESSION"
    | "NONE";

type ClassTeacherSlice = {
    openingModal: ClassTeacherOpeningModal;
    classTeacherDrafts: {
        // * Real teachers in class from api last fetch
        alreadyInClassTeachers: Record<ID, Teacher>;
        // * This is currently the available staff in the system
        teacherCandidates: Record<ID, Partial<Teacher>>;
        // * Transient teacher that is added to class when the user confirm
        transientCandidates: Record<ID, Partial<Teacher>>;
    };
};

export type TeacherRole = Exclude<ClassMemberType, "STUDENT">;
const candidateDefaultRole: TeacherRole = "TEACHER_ASSISTANT";
const initialState: ClassTeacherSlice = {
    openingModal: "NONE",
    classTeacherDrafts: {
        alreadyInClassTeachers: {},
        teacherCandidates: {},
        transientCandidates: {},
    },
};

export const TEACHER_ROLE_TITLE_HASHMAP: { [P in TeacherRole]: string } = {
    TEACHER: "GIÁO VIÊN",
    TEACHER_ASSISTANT: "TRỢ GIẢNG",
};
export const CLASS_TEACHER_SLICE_KEY = "classTeacherSlice";
export const classTeacherSlice = createSlice({
    name: CLASS_TEACHER_SLICE_KEY,
    initialState,
    reducers: {
        closeModal: (state) => {
            state.openingModal = "NONE";
        },
        openTeacherAssignmenModal: (state) => {
            state.openingModal = "ADD_TEACHER_TO_CLASS";
        },
        openTeacherSessionAssigmentModal: (state) => {
            state.openingModal = "ASSIGN_TEACHER_TO_SESSION";
        },
        clearTeacherCandidates: (state) => {
            state.classTeacherDrafts.transientCandidates = {};
        },
        setTeacherCandidatesFromList: (
            state,
            action: PayloadAction<Partial<Staff>[]>
        ) => {
            const listCandidates = action.payload;
            listCandidates.forEach((item) => {
                if (!item.id) return;
                state.classTeacherDrafts.teacherCandidates[item.id] = {
                    ...item,
                    role: candidateDefaultRole,
                };
            });
        },
        changeTransientCandidateRole: (
            state,
            action: PayloadAction<{ id: ID; role: TeacherRole }>
        ) => {
            const { id, role } = action.payload;
            state.classTeacherDrafts.transientCandidates[id].role = role;
        },
        addNewCandidateToTransient: (state, action: PayloadAction<ID>) => {
            const id = action.payload;
            const candidateToAdd =
                state.classTeacherDrafts.teacherCandidates[id];
            if (!id || !candidateToAdd) {
                AntdNotifier.error("Invalid candidate");
                return;
            }
            // * Remove from selectable candidate (staffs)
            delete state.classTeacherDrafts.teacherCandidates[id];
            // * Add to transient list with default role
            state.classTeacherDrafts.transientCandidates[id] = {
                ...candidateToAdd,
                role: candidateDefaultRole,
            };
        },
        removeCandidateFromTransient: (state, action: PayloadAction<ID>) => {
            const candidateIdToRemove = action.payload;
            if (
                !state.classTeacherDrafts.transientCandidates[
                    candidateIdToRemove
                ]
            ) {
                return;
            }
            const teacherCandidatesToRemove =
                state.classTeacherDrafts.transientCandidates[
                    candidateIdToRemove
                ];
            // * Remove from transient list
            delete state.classTeacherDrafts.transientCandidates[
                candidateIdToRemove
            ];
            // * Add back to the candidate list (removing the role - maybe unneeded)
            state.classTeacherDrafts.teacherCandidates[candidateIdToRemove] =
                teacherCandidatesToRemove;
        },
    },
});

export const classTeacherSelectors = {
    selectIsAssigningTeacher: (state: RootState) =>
        state[CLASS_TEACHER_SLICE_KEY].openingModal === "ADD_TEACHER_TO_CLASS",
    selectIsAssigningTeacherToSession: (state: RootState) =>
        state[CLASS_TEACHER_SLICE_KEY].openingModal ===
        "ASSIGN_TEACHER_TO_SESSION",
    selectTeacherCandidates: (state: RootState) =>
        state[CLASS_TEACHER_SLICE_KEY].classTeacherDrafts.teacherCandidates,
    selectTeacherInTransient: (state: RootState) =>
        state[CLASS_TEACHER_SLICE_KEY].classTeacherDrafts.transientCandidates,
    selectCurrentlyInClassTeachers: (state: RootState) =>
        state[CLASS_TEACHER_SLICE_KEY].classTeacherDrafts
            .alreadyInClassTeachers,
    selectTeacherToAddToClass: (state: RootState) => {
        const transientTeachers = Object.values(
            state[CLASS_TEACHER_SLICE_KEY].classTeacherDrafts
                .transientCandidates
        );
        if (!transientTeachers) return undefined;
        return transientTeachers
            .map((item) => ({
                id: item.id,
                role: item.role,
            }))
            .filter((item) => item.id || item.role);
    },
};

export const classTeacherActions = classTeacherSlice.actions;
