import {MinusCircleOutlined} from "@ant-design/icons";
import {Button, Col, Row, Typography} from "antd";
import {FieldArray, FieldProps} from "formik";
import {FormItem, Input} from "formik-antd";
import React, {CSSProperties} from "react";
import TextbookSearchSelect from "../../../Courses/Forms/TextBookSearchSelect/TextBookSearchSelect";
import {TextbookUnit} from "@typescript/interfaces/courses/CourseTypes";
import {addIconSvg} from "../../../Icons/SvgIcons";
import {useAppSelector} from "@hooks/Redux/hooks";
import {textbookSelector} from "@redux/features/Textbook/TextbookSlice";
import {useCountTextbooksInCourseQuery, useGetTextbooksInCourseQuery} from "@redux/features/Course/CourseAPI";
import {useCountTextbooksInClassQuery, useGetTextbooksInClassQuery} from "@redux/features/Class/ClassAPI";
import './TextBookSearchArrayFieldWithNote.less';

type TextBookSearchSelectFieldProps = FieldProps<any> & {
    label?: string;
    style?: CSSProperties;
    required?: boolean;
    onSelect?: () => void;
    showErrorMessage?: boolean;
    scopeIdToSearch: number;
    scopeToSearch: string;
};
const TextBookSearchArrayFieldWithNote = ({
                                              field,
                                              scopeIdToSearch,
                                              scopeToSearch,
                                              form: {
                                                  touched,
                                                  errors,
                                                  setFieldValue,
                                                  setFieldError,
                                                  handleBlur,
                                                  handleChange,
                                                  setTouched,
                                                  setFieldTouched,
                                                  values,
                                              },
                                              ...otherProps
                                          }: TextBookSearchSelectFieldProps) => {
    const {name, value, onChange, onBlur} = field;
    const errorMsg = touched[name] && errors[name];

    // const searchQuery = jk
    const textbookSearchCriteria = useAppSelector(
        textbookSelector.selectSearchCriteria
    );

    const {
        data: textbooksCourse,
        isSuccess,
        isFetching,
    } = useGetTextbooksInCourseQuery({
        keyword: textbookSearchCriteria.keyword,
        courseId: Number(scopeIdToSearch)
    }, {skip: scopeToSearch != 'COURSE'});

    const {
        data: countTextbooksInCourse
    } = useCountTextbooksInCourseQuery({
        courseId: Number(scopeIdToSearch)
    }, {skip: scopeToSearch != 'COURSE'});

    const {
        data: textbooksClass,
        isSuccess: isGetTextbooksInClassSuccess,
        isFetching: isGetTextbooksInClassFetching,
    } = useGetTextbooksInClassQuery({
        keyword: textbookSearchCriteria.keyword,
        classId: Number(scopeIdToSearch)
    }, {skip: scopeToSearch != 'CLASS'});

    const {
        data: countTextbooksInClass
    } = useCountTextbooksInClassQuery({
        classId: Number(scopeIdToSearch)
    }, {skip: scopeToSearch != 'CLASS'});

    return (
        <div className={"textbook-array-outter-container"}>
            <FieldArray
                {...field}
                name={name}
                render={(arrayHelper) => {
                    return (
                        <>
                            {(scopeToSearch == 'CLASS' && (countTextbooksInClass == undefined || countTextbooksInClass == 0))
                            || (scopeToSearch == 'COURSE' && (countTextbooksInCourse == undefined || countTextbooksInCourse == 0))
                                ? <span className={'textbook-search-array-note'}>*Chưa có giáo trình nào được thêm vào khóa học/lớp học.
                            Hãy bổ sung nếu bạn muốn thêm giáo trình cho bài học/đề kiểm tra.</span>
                                : <>
                                    {/*<Row>*/}
                                    {/*    {`Giáo trình`}*/}
                                    {/*</Row>*/}
                                    <Row className={"flex align-items-center"} style={{margin: "0.5em 0"}}>
                                        {values &&
                                            values[name].map((item: TextbookUnit, index: number) => {
                                                return (
                                                    <>
                                                        <Col span={9}>
                                                            <FormItem label={'Giáo trình'} name={`${name}[${index}].textbookId`}>
                                                                <TextbookSearchSelect
                                                                    onSelect={(textBookId) => {
                                                                        setFieldValue(`${name}[${index}].textbookId`, textBookId, true);
                                                                    }}
                                                                    onBlur={handleBlur}
                                                                    value={item.textbookId}
                                                                    width={'100%'}
                                                                    textbooks={scopeToSearch == 'CLASS' ? (textbooksClass || []) : (textbooksCourse || [])}
                                                                />
                                                            </FormItem>
                                                        </Col>
                                                        <Col span={9} offset={1}>
                                                            <FormItem label={'Chú thích giáo trình'} name={`${name}[${index}].note`}>
                                                                <Input
                                                                    className="textbook-note-input"
                                                                    name={`${name}[${index}].note`}
                                                                    style={{width: "100%"}}
                                                                />
                                                            </FormItem>
                                                        </Col>
                                                        <Col span={4} offset={1}>
                                                            <MinusCircleOutlined
                                                                style={{color: 'red'}}
                                                                onClick={() => {
                                                                    arrayHelper.remove(index);
                                                                }}
                                                            />
                                                        </Col>
                                                        <Row style={{width: '100%'}}>
                                                            <Typography.Text type={"danger"}>
                                                                {errorMsg && errorMsg[index]?.textbookId}
                                                            </Typography.Text>
                                                        </Row>
                                                    </>
                                                );
                                            })}
                                    </Row>
                                    <span style={{color: '#c91c00'}}>{errors['textbooks']}</span>
                                    <Button
                                        className={"button-with-icon align-center-parent-relative"}
                                        onClick={(e) => {
                                            arrayHelper.push({
                                                textbookId: undefined,
                                                note: "",
                                            });
                                            // console.log('values: ', values[name]);
                                        }}
                                        icon={addIconSvg}
                                        type="default"
                                    >
                                        Thêm giáo trình
                                    </Button>
                                </>}
                        </>
                    );
                }}
            />
        </div>
    );
};
export default TextBookSearchArrayFieldWithNote;
