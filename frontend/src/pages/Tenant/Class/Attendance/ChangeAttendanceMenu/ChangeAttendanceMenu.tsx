import { Menu } from "antd";
import { AttendanceState } from "../../../../../redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import AttendanceTag from "../AttendanceTag";
import React from "react";

type StateAttendanceMenuItemType = {
    label: JSX.Element;
    key: AttendanceState;
};
type StateChangeMenuProps = {
    onSelect: (key: AttendanceState) => void;
};

const StateChangeMenu = (props: StateChangeMenuProps) => {
    const { onSelect } = props;
    const menuItems: StateAttendanceMenuItemType[] = [
        {
            label: <AttendanceTag state={"PRESENT"} title={"Có mặt"} />,
            key: "PRESENT" as const,
        },
        {
            label: <AttendanceTag state={"LATE"} title={"Trễ"}/>,
            key: "LATE" as const,
        },
        {
            label: <AttendanceTag state={"ABSENT"} title={"Vắng"} />,
            key: "ABSENT" as const,
        },
        {
            label: <AttendanceTag state={"NONE"} title={"N/A"} />,
            key: "NONE" as const,
        },
    ];
    return (
        <Menu
            items={menuItems}
            onClick={({ key }) => {
                onSelect(key as AttendanceState);
            }}
        />
    );
};

export default React.memo(StateChangeMenu);
