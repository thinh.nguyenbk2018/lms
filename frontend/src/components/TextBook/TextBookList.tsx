import {Modal, Pagination} from "antd";
import {Formik} from "formik";
import React from "react";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import LoadingPage from "../../pages/UtilPages/LoadingPage";
import {
    useAddTextbookMutation,
    useDeleteTextbookMutation,
    useGetTextbooksQuery,
    useUpdateTextbookMutation,
} from "@redux/features/Textbook/TextbookAPI";
import {
    changeCriteria,
    setTextBookInEdit,
    textbookSelector,
    textbookSlice,
} from "@redux/features/Textbook/TextbookSlice";
import TextbookCard from "../Card/CustomCard/BookCard/TextbookCard";
import ApiErrorWithRetryButton from "../Util/ApiErrorWithRetryButton";
import TextBookForm, {textBookInitialValues,} from "./TextBookForm/TextBookForm";
import "./TextBookList.less";
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import {textBookErrorsHandler} from "@pages/Tenant/TextBook/TextBookPage";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {useParams} from "react-router-dom";
import {useDeleteTextbooksFromCourseMutation, useGetTextbooksInCourseQuery} from "@redux/features/Course/CourseAPI";
import {useGetTextbooksInClassQuery} from "@redux/features/Class/ClassAPI";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {DEFAULT_FRONTEND_ID} from "@redux/interfaces/types";
import * as Yup from "yup";

export const TextbookSchema = Yup.object().shape({
    name: Yup.string().required("Tên giáo trình là thông tin bắt buộc")
});

const TextBookList = () => {
    const searchCriteria = useAppSelector(textbookSelector.selectSearchCriteria);
    const {classId, courseId} = useParams();

    const {
        data: textBookList,
        isSuccess,
        isLoading,
        isError,
        isFetching,
        isUninitialized,
    } = useGetTextbooksQuery(searchCriteria, {skip: !isNaN(Number(courseId)) || !isNaN(Number(classId))});

    const dispatch = useAppDispatch();
    const textBookInEdit = useAppSelector(textbookSelector.selectTextBookInEdit);
    const isCreating = useAppSelector(textbookSelector.selectIsCreatingTextBook);

    const [addTextBook] = useAddTextbookMutation();
    const [updateTextbook] = useUpdateTextbookMutation();
    const [deleteTextbook] = useDeleteTextbookMutation();
    const [deleteTextbookFromCourse] = useDeleteTextbooksFromCourseMutation();

    const {
        isSuccess: isGetTextbooksInCourseSuccess,
        data: textbooksInCourse,
        isLoading: isTextbooksInCourseLoading
    } = useGetTextbooksInCourseQuery({courseId: Number(courseId), keyword: ""}, {skip: isNaN(Number(courseId))});

    const {
        isSuccess: isGetTextbooksInClassSuccess,
        data: textbooksInClass,
        isLoading: isTextbooksInClassLoading
    } = useGetTextbooksInClassQuery({classId: Number(classId), keyword: ""}, {skip: isNaN(Number(classId))});

    if ((!isNaN(Number(courseId)) && isTextbooksInCourseLoading)
        || (!isNaN(Number(classId)) && isTextbooksInClassLoading)
        || (isNaN(Number(courseId)) && isNaN(Number(classId)) && isLoading)) {
        return <LoadingPage/>;
    } else if (isError) {
        return (
            <ApiErrorWithRetryButton
                onRetry={() => {
                    window.location.reload();
                }}
            />
        );
    } else if (isSuccess || isGetTextbooksInClassSuccess || isGetTextbooksInCourseSuccess) {
        const textBookDataInEdit = !textBookInEdit
            ? undefined
            : textBookList?.listData.find((item) => item.id === textBookInEdit);

        return (
            <>
                <div className="textbook-grid">
                    {((courseId != undefined && classId == undefined) ? textbooksInCourse : classId != undefined ? textbooksInClass : textBookList?.listData)?.map((item, index) => {
                        return (
                            <>
                                <TextbookCard
                                    {...item}
                                    key={item.id}
                                    onEdit={() => {
                                        dispatch(setTextBookInEdit(item.id));
                                    }}
                                    onDelete={() => {
                                        confirmationModal('giáo trình', `${item.name}`, () => {
                                            if (!item.id) return;
                                            if (courseId != undefined) {
                                                deleteTextbookFromCourse({
                                                    courseId: Number(courseId),
                                                    textbookId: item.id
                                                })
                                                    .unwrap()
                                                    .then(res => {
                                                        AntdNotifier.success("Xóa tài liệu " + item.name + " khỏi khóa học thành công");
                                                    })
                                                    .catch((err) => {
                                                        AntdNotifier.error(
                                                            "Lỗi khi xảy ra tài liệu " + item.name
                                                        );
                                                    });

                                            } else {
                                                deleteTextbook(item.id)
                                                    .unwrap()
                                                    .then((res) => {
                                                        AntdNotifier.success("Xóa tài liệu " + item.name + " thành công");
                                                    })
                                                    .catch((err) => {
                                                        AntdNotifier.error(
                                                            "Lỗi khi xảy ra tài liệu " + item.name
                                                        );
                                                    });
                                            }
                                        });

                                    }}
                                />
                            </>
                        );
                    })}
                </div>
                <div style={{
                    display: !isNaN(Number(courseId)) || !isNaN(Number(classId)) ? 'none' : '',
                    justifyContent: "flex-end",
                    marginTop: "2rem"
                }}
                     className={'flex'}
                >
                    <Pagination
                        showSizeChanger
                        pageSize={searchCriteria?.pagination?.size || DEFAULT_PAGE_SIZE}
                        onChange={(page: any, size: any) => {
                            const payload = {
                                ...searchCriteria,
                                pagination: {page, size},
                            };
                            dispatch(changeCriteria(payload));
                        }}
                        onShowSizeChange={(current, newSize) => {
                            let newSearchCriteria = {
                                ...searchCriteria,
                                pagination: {
                                    size: newSize,
                                    page: 1,
                                },
                            };
                            dispatch(
                                changeCriteria(newSearchCriteria)
                            );
                            window.scrollTo(0, 0);
                        }}
                        current={textBookList?.page}
                        total={textBookList?.total}
                    />
                </div>
                <Formik
                    enableReinitialize={true}
                    validationSchema={TextbookSchema}
                    initialValues={
                        (textBookInEdit && textBookDataInEdit) || textBookInitialValues
                    }
                    onSubmit={(values) => {
                        if (textBookInEdit && textBookInEdit > 0) {
                            updateTextbook({id: textBookInEdit, ...values})
                                .unwrap()
                                .then((res) => {
                                    dispatch(setTextBookInEdit(undefined));
                                    AntdNotifier.success("Cập nhật giáo trình thành công");
                                })
                                .catch((err) => {
                                    textBookErrorsHandler(err);
                                });
                        } else {
                            addTextBook(values)
                                .unwrap()
                                .then((res) => {
                                    dispatch(setTextBookInEdit(undefined));
                                    AntdNotifier.success("Thêm giáo trình thành công");
                                })
                                .catch((err) => {
                                    AntdNotifier.error("Thêm giáo trình thất bại");
                                });
                        }
                    }}
                >
                    {({handleSubmit, resetForm, isValid, dirty}) => (
                        <Modal
                            title={(textBookInEdit && textBookInEdit != DEFAULT_FRONTEND_ID) ? "Cập nhật giáo trình" : "Thêm giáo trình"}
                            visible={!!textBookInEdit || isCreating}
                            okButtonProps={{
                                disabled: !dirty,
                            }}
                            onOk={() => {
                                handleSubmit();
                            }}
                            afterClose={() => {
                                resetForm();
                                dispatch(textbookSlice.actions.setTextBookInEdit(undefined));
                            }}
                            onCancel={() => {
                                dispatch(setTextBookInEdit(undefined));
                            }}
                            destroyOnClose={true}
                            maskClosable={false}
                            okText={(textBookInEdit && textBookInEdit != DEFAULT_FRONTEND_ID) ? "Cập nhật" : "Thêm"}
                            cancelText={"Hủy"}
                        >
                            <TextBookForm
                                namespace={undefined}
                                labels={{
                                    NAME: "Tên giáo trình",
                                    AUTHOR: "Tên tác giả",
                                    DESCRIPTION: "Mô tả",
                                    ATTACHMENT: "Tệp đính kèm",
                                }}
                            />
                        </Modal>
                    )}
                </Formik>
            </>
        );
    } else {
        return <></>;
    }
};

export default TextBookList;
