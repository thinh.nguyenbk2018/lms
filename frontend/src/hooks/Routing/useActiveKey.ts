import { NavigationItem } from "navigation/NavigationItem";
import { matchPath, useLocation, useMatch, useResolvedPath } from "react-router";

const useActiveKey = (routes: NavigationItem[]): (string | undefined) => {
    function useIsActiveKey(route: NavigationItem) {
        let resolved = useResolvedPath("path" in route ? route.path : "");
        let match = useMatch({ path: resolved.pathname, end: true });

        let childrenActiveKey = useActiveKey(route.children ? route.children : [])
        
        return match ? route.key : childrenActiveKey;
    }

    return routes.map(useIsActiveKey).find(route => !!route)
};

export default useActiveKey;
