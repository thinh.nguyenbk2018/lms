import { Input } from "antd";
import { useAppDispatch, useAppSelector } from "../../../../hooks/Redux/hooks";
import { gradeFormulaSeletor, updateFormula } from "../../../../redux/features/Grade/GradeFormulaSlice";

const NumberComponent = ({ value, index }: { value: number, index: number }) => {
  let formula = useAppSelector(gradeFormulaSeletor.formulaSelector);
  const dispatch = useAppDispatch();

  const onChange = (num: number) => {
    let newFormula = [...formula.map(ele => ({ ...ele }))];
    newFormula[index].value = num;
    dispatch(updateFormula(newFormula));
  }

  return <Input style={{ display: "inline-block", width: 100, textAlign: "center" }} type={"number"} value={value} onChange={({ currentTarget: { value: num } }) => onChange(parseInt(num))} />
}

export default NumberComponent;