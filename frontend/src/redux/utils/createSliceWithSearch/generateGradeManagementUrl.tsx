import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import { ScopeTag } from "../../features/Grade/GradeApi";

export const generateGradeManagementUrl = (scope: ScopeTag, scopeId: number) =>
  scope === "CLASS" ? ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_CLASS.replace(":classId", scopeId.toString()) : ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_COURSE.replace(":courseId", scopeId.toString())