import {Role, useDeleteRoleMutation, useGetAllRolesQuery,} from "../../../redux/features/Permission/RolesAPI";
import {Button, Col, Collapse, Row, Typography} from "antd";
import RoleDetails from "../GrantPermisson/RoleAndPermission/Role/RoleDetails";
import CustomEmpty from "../../Util/CustomEmpty";
import {Link} from "react-router-dom";
import {DEFAULT_ROLE_IDS} from "@constants/RoleDefaultIds";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

const RolesManagement = ({ searchKeyword }: { searchKeyword: string }) => {
    let { data: getAllRolesResponse } = useGetAllRolesQuery();
    let [deleteRole] = useDeleteRoleMutation();

    let showedRoles =
        getAllRolesResponse?.roles.filter((role) =>
            role.title.toLowerCase().includes(searchKeyword.toLowerCase())
        ) || [];

    const renderRoleHeader = (role: Role) => (
        <Row style={{ width: "100%" }} align="middle">
            <Col lg={20} md={20}>
                {role.title}
            </Col>
            <Col lg={4} md={4}>
                <Row justify="end" gutter={[8, 16]}>
                    <PermissionRenderer permissions={[PermissionEnum.UPDATE_ROLE]}>
                        <Col>
                            <Link to={`/authorizaton/roles/${role.id}`}>
                                <Button type="primary">Cập nhật</Button>
                            </Link>
                        </Col>
                    </PermissionRenderer>
                    {!DEFAULT_ROLE_IDS.includes(role.id) && (
                        <PermissionRenderer permissions={[PermissionEnum.DELETE_ROLE]}>
                            <Col>
                                <span onClick={(e) => e.stopPropagation()}>
                                    <Button
                                        type="primary"
                                        danger
                                        onClick={() => {
                                            deleteRole(role.id).unwrap().then(res => {
                                                AntdNotifier.success("Xóa vai trò thành công")
                                            })
                                            .catch(error => {
                                                AntdNotifier.error(error.data.message)
                                            })
                                        }}
                                    >
                                        Xóa
                                    </Button>
                                </span>
                            </Col>
                        </PermissionRenderer>
                    )}
                </Row>
            </Col>
        </Row>
    );

    return (
        <Row justify="center">
            <Collapse
                      style={{ width: "100%" }}
            >
                {showedRoles.map((role, index) => (
                    <Collapse.Panel key={index} header={renderRoleHeader(role)}>
                        <PermissionRenderer
                            permissions={[PermissionEnum.VIEW_DETAIL_ROLE]}
                            message={
                                <Row justify="center">
                                <Typography.Text type="danger">Bạn không có quyền xem chi tiết vai trò.</Typography.Text>
                                </Row>
                            }
                        >
                            <RoleDetails roleId={role.id} />
                        </PermissionRenderer>
                    </Collapse.Panel>
                ))}
                {showedRoles.length === 0 && <CustomEmpty description={'Không có dữ liệu'}/>}
            </Collapse>
        </Row>
    );
};

export default RolesManagement;
