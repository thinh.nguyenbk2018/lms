import { Avatar, Card, Col, Divider, Form, Image, Input, Row, Space, Typography } from "antd";
import { re } from "mathjs";
import { forwardRef, Ref, useEffect, useImperativeHandle, useState } from "react";
import { Staff, useCreateStaffMutation, useUpdateStaffMutation } from "../../../redux/features/Staff/StaffAPI";
import TextArea from "antd/es/input/TextArea";

export interface StaffFormHandle {
  updateStaff: () => any,
  createStaff: () => any,
}

const StaffForm = ({ staff }: { staff?: Staff }, ref: Ref<StaffFormHandle>) => {

  let [form] = Form.useForm();

  let [updateStaff] = useUpdateStaffMutation();
  let [createStaff] = useCreateStaffMutation();

  useImperativeHandle(ref, () => ({
    createStaff: async () => {
      try {
        let values = await form.validateFields();
        return createStaff(values).unwrap();
      } catch (error) {
        console.log(error)
        return null;
      }
    },
    updateStaff: async () => {
      try {
        let values = await form.validateFields();
        return updateStaff({
          id: staff?.id,
          ...values
        }).unwrap();
      } catch (error) {
        console.log(error)
        return null;
      }
    }
  }))

  useEffect(() => form.resetFields(), [staff, form]);

  return <>
    {staff &&
      <Row justify="center">
        <Space direction="vertical" align="center">
          <Avatar size={150} src={<Image src={staff?.avatar} />} />
          <Typography.Title level={3}>{staff?.lastName} {staff?.firstName}</Typography.Title>
          <Typography.Text>NV_{staff?.id}</Typography.Text>
        </Space>
      </Row>
    }
    <Divider orientation="left" orientationMargin={5}><Typography.Title level={5}>Thông tin chung</Typography.Title></Divider>
    <Card>
      <Row justify="center">
        <Col lg={12} sm={24}>
          <Form layout="vertical"
            labelAlign="left"
            form={form}
          >
            <Row gutter={[16, 16]}>
              <Col span={16}>
                <Form.Item
                  label="Họ và tên đệm"
                  name={"lastName"}
                  initialValue={staff?.lastName}
                  required
                  rules={[{
                    required: true,
                    message: "Họ và tên đệm là thông tin bắt buộc"
                  }]}
                >
                  <Input placeholder="Nhập họ và tên đệm" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  label="Tên"
                  name={"firstName"}
                  initialValue={staff?.firstName}
                  required
                  rules={[{
                    required: true,
                    message: "Tên là thông tin bắt buộc"
                  }]}
                >
                  <Input placeholder="Nhập tên" />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item
              label="Email"
              name={"email"}
              initialValue={staff?.email}
              required
              rules={[{
                required: true,
                message: "Vui lòng điền Email"
              }]}
            >
              <Input placeholder="Nhập email" />
            </Form.Item>
            <Form.Item
              label="Số điện thoại"
              name={"phone"}
              initialValue={staff?.phone}
              required
              rules={[{
                required: true,
                message: "Vui lòng điền Họ và tên đệm"
              }]}
            >
              <Input placeholder="Nhập số điện thoại" />
            </Form.Item>
            <Form.Item
                label="Mô tả"
                name={"description"}
                initialValue={staff?.description}
            >
              <TextArea rows={4} placeholder="Nhập mô tả" />
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Card>
  </>
}

export default forwardRef(StaffForm);