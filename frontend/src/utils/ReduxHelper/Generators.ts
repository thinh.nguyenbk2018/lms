import {AsyncStatus} from "../../redux/interfaces/types";
import {DEFAULT_ERROR_MESSAGE} from "../RequestHelper";

export function generateName(actionType:string) {
    const actionName = actionType
        .split('_')
        .map((elem, index) => {
            elem = elem.toLowerCase();
            if (index === 0) {
                return elem;
            }
            return elem.charAt(0).toUpperCase() + elem.slice(1);
        })
        .join('');
    return actionName;
}


export function generateStatus() : AsyncStatus{
    return {
        isSuccess: false,
        isError: false,
        isLoading: false,
        errors: {}
    };
}

export function loading() : AsyncStatus {
    return {
        isLoading: true,
        isSuccess: false,
        isError: false,
        errors: {}
    };
}

export function success(): AsyncStatus {
    return {
        isSuccess: true,
        isError: false,
        isLoading: false,
        errors: {},
    };
}


// * Redux toolkit internal error type
interface SerializedError {
    name?: string;
    message?: string;
    stack?: string;
    code?: string;
}



export function error(errors?: SerializedError) : AsyncStatus{
    return {
        isSuccess: false,
        isError: true,
        isLoading: false,
        errors: errors?.message ?? {
            message: DEFAULT_ERROR_MESSAGE
        }
    };
}

// /*
//     * This function return the names for sagas to put to the store
//     * Example:
//     *   Input: 'getUserProfile'
//     *   Output:
//     *         {
//     *               success: 'getUserProfile.success'
//     *               loading :'getUserProfile.loading'
//     *               error: 'getUserProfile.error'
//     *         }
//  */
// export function generateSagaLifecycleNames(actionType){
//     const actionName = actionType;
//     return{
//         success: `${actionName}.success`,
//         loading: `${actionName}.loading`,
//         error: `${actionName}.error`
//     }
// }