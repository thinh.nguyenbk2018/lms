import React, {useState} from 'react';
import {Drawer, List, Space, Tooltip, Typography} from "antd";
import IconSelector from "../../Icons/IconSelector";
import COLOR_SCHEME from "../../../constants/ThemeColor";
import ChatItem from "./ChatItem";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useGetChatsBriefInfoQuery, useLazyGetChatsBriefInfoQuery} from "../../../redux/features/Chat/ChatAPI";
import LoadingPage from "../../../pages/UtilPages/LoadingPage";
import {useAppDispatch} from "../../../hooks/Redux/hooks";
import {setChatList} from "../../../redux/features/Chat/ChatSlice";
import ApiErrorWithRetryButton from '../../Util/ApiErrorWithRetryButton';


// ? Chat room messages and messages configuration + All Read + Clear
const ChatDrawer = () => {
    const [showChatRoom, setShowChatRoom] = useState(false);
    const dispatch = useAppDispatch();
    const [ trigger, {
        data,
        isFetching,
        isSuccess,
        error,
        isError
    }] = useLazyGetChatsBriefInfoQuery();

    const toggleChats = () => {
        setShowChatRoom(!showChatRoom);
    }
    const closeChatRoom = () => {
        setShowChatRoom(false);
    }
    if (isSuccess) {
        dispatch(setChatList(data?.listData.map(item => item.chatRoomId)));
    }
    return (
        <>
            <Tooltip placement="topLeft" title="Tin nhắn">
                <IconSelector type={"WeChatOutlined"} onClick={() => {
                    toggleChats();
                }}/>
            </Tooltip>
            <Drawer size={"default"} getContainer={false} title={
                <Space align={"center"}>
                    <Typography.Text strong style={{color: COLOR_SCHEME.primary}}> Tin nhắn </Typography.Text>
                    <Tooltip title={"Cài đặt tin nhắn"}>
                        <IconSelector type={"SettingFilled"}/>
                    </Tooltip>
                </Space>
            } placement="right" onClose={closeChatRoom} visible={showChatRoom}
                    extra={
                        // <Space size={"middle"}>
                        //     <Typography.Text style={{color:COLOR_SCHEME.primary}}> Đã đọc tất cả </Typography.Text>
                        //     <Typography.Text type={"danger"}> Xóa hết tin nhắn</Typography.Text>
                        // </Space>
                        <Space size={"middle"}>
                            <Tooltip title={"Tắt thông báo trò chuyện"}>
                                {/* <FontAwesomeIcon icon="comment-slash"
                                                 style={{color: COLOR_SCHEME.primary, fontSize: "1.5em"}}/> */}
                            </Tooltip>
                            <Tooltip title={"Xóa hết tin nhắn"}>
                                <IconSelector type={`DeleteOutLined`}/>
                            </Tooltip>
                        </Space>
            }>
                {
                    isSuccess ? <>
                        <Typography.Text strong>
                            Danh sách trò chuyện
                        </Typography.Text>
                        <List
                            itemLayout="horizontal"
                            dataSource={data?.listData || []}
                            renderItem={item => (
                                <ChatItem key={item.chatRoomId} props={item} callback={closeChatRoom}/>
                            )}
                        />
                    </> : <></>
                }
                {
                    isFetching ? <LoadingPage></LoadingPage> : <></>
                }
                {
                    isError ? <ApiErrorWithRetryButton message={`Đã có lỗi xảy ra trong quá trình load các tin nhắn`} onRetry={() => {
                        console.log("Retry loading messages",error);
                        trigger();
                    }}></ApiErrorWithRetryButton> : null 
                }
            </Drawer>
        </>
    );
};

export default ChatDrawer;
