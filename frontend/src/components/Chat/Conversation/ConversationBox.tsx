import React, {useState} from 'react';
import {Avatar, Button, Card, Col, Form, Input, Popconfirm, Row, Space, Tooltip, Typography} from "antd";
import DropDownMenu from "../../DropDownMenu/DropDownMenu";
import IconSelector from "../../Icons/IconSelector";
import {CloseCircleTwoTone, MinusCircleTwoTone} from "@ant-design/icons";
import './ConversationBox.less'
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {authSelectors} from "../../../redux/features/Auth/AuthSlice";
import {ChatRoomMetaState, hideChat, minimizeChat} from "../../../redux/features/Chat/ChatSlice";
import {Message, useGetActiveChatMessagesQuery} from "../../../redux/features/Chat/ChatAPI";
import LoadingPage from "../../../pages/UtilPages/LoadingPage";
import COLOR_SCHEME from './../../../constants/ThemeColor';
import IMAGE_CONSTANTS from './../../../constants/DefaultImage';
import InfiniteScroll from 'react-infinite-scroll-component';


const MessageInputForm = ({ chatRoomId }: { chatRoomId: number }) => {
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);
    const [message, setMessage] = useState("");
    const [caretPosition, setCaretPostion] = useState(0);
    const [showPicker, setShowPicker] = useState(false);
    const onEmojiClick = (event: any, emojiObject: any) => {
        console.log(emojiObject);
        insertTextAtCaret(event, emojiObject.emoji);
    };

    const insertTextAtCaret = (event: any, text: string) => {
        let cursorPosition = caretPosition;
        let textBeforeCursorPosition = message.substring(0, cursorPosition)
        let textAfterCursorPosition = message.substring(cursorPosition, message.length)
        setMessage(textBeforeCursorPosition + text + textAfterCursorPosition);
    }

    const togglePicker = () => {
        setShowPicker(!showPicker);
    }

    const onSendMessage = () => {
        console.log("Sending message: ", message)
        // * Send the message to server
        // * Receive response
        // * Append to message list if success
        const messageSent: Message = {
            chatRoomId: chatRoomId,
            content: message,
            senderId: currentUser?.id!!,
        }
        //!! StompUtils.publishToTopic("/chat/send", JSON.stringify(messageSent));
    }

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setMessage(e.target.value);
        setCaretPostion(e.target.selectionStart || e.target.selectionEnd || e.target.value.length);
    }

    const handleInputClick = (e: any) => {
        setCaretPostion(e.target.selectionStart);
    }

    return (
        <>
            <Row justify={"end"} align={"middle"}>
                <Col span={4} style={{ backgroundColor: "white" }}>
                    {/*<Picker onEmojiClick={onEmojiClick}></Picker>*/}
                    {/* I removed the picker: Please use somethign else to display the emoji */}
                    <Popconfirm placement="top" title={<></>}
                        onConfirm={() => setShowPicker(false)}>
                        <IconSelector type={"SmileOutlined"} onClick={togglePicker} />
                    </Popconfirm>
                    <IconSelector type={"PlusCircleOutlined"} />
                </Col>
                <Col span={18}>
                    <Form
                        name="customized_form_controls"
                        layout="inline"
                        onFinish={(values) => onSendMessage()}
                        initialValues={{
                            message: ""
                        }}
                    >
                        <Space>
                            <Input name={"message"} size={"large"} value={message} onChange={handleInputChange}
                                onClick={handleInputClick} />
                            <Form.Item>
                                <Button type="primary" htmlType="submit" size={"large"}>
                                    Submit
                                </Button>
                            </Form.Item>

                        </Space>
                    </Form>
                </Col>
            </Row>
        </>
    );
}


// * Floating conversation
const ConversationBox = (props: ChatRoomMetaState) => {
    const { id, state } = props;

    const [visible, setVisible] = useState(true);
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);
    const dispatch = useAppDispatch();

    const [page, setPage] = useState(1);
    const [size, setSize] = useState(10);

    const {
        data: activeChatRoom,
        isFetching,
        isSuccess,
        isLoading,
        error,
        isError,
        refetch
    } = useGetActiveChatMessagesQuery({ id, page, size });

    const onClose = () => {
        dispatch(hideChat(id));
    }

    const onMinimize = () => {
        dispatch(minimizeChat(id));
    }

    const fetchOlderMessage = () => {
        setPage(page + 1);
    }

    const renderFunction = () => {
        if (!activeChatRoom) {
            return <LoadingPage></LoadingPage>;
        } else {
            return <Card
                title={
                    <Space>
                        <Avatar src={activeChatRoom?.image || IMAGE_CONSTANTS.DEFAULT_AVATAR} />
                        <Typography.Text strong className={"chat-box-title"}> {activeChatRoom.chatRoomName}</Typography.Text>
                        <DropDownMenu props={[
                            {
                                itemDisplay: <Typography.Text> Tắt thông báo </Typography.Text>,
                                callback: () => console.log("MUTE NOTIFICATION FOR CHAT", id)
                            },
                            {
                                itemDisplay: <Typography.Text> Chặn người này </Typography.Text>,
                                callback: () => console.log("MUTE NOTIFICATION FOR CHAT", id)
                            },
                            {
                                itemDisplay: <Typography.Text> Báo xấu </Typography.Text>,
                                callback: () => console.log("MUTE NOTIFICATION FOR CHAT", id)
                            }
                        ]} />
                    </Space>
                } extra={
                    <Space>
                        <Tooltip title={"Minimize"}>
                            <MinusCircleTwoTone style={{ fontSize: "2em" }} onClick={onMinimize} />
                        </Tooltip>
                        <Tooltip title={"Close"}>
                            <CloseCircleTwoTone style={{ fontSize: "2em" }} onClick={onClose} />
                        </Tooltip>
                    </Space>
                }
                actions={[
                    <MessageInputForm chatRoomId={props.id} />
                ]
                }
                headStyle={{ background: COLOR_SCHEME.primary }}
                style={{
                    minWidth: `360px`,
                    maxWidth: `500px`,
                    zIndex: `9999`,
                    display: visible ? "static" : "none",
                    maxHeight: `55vh`,
                }}>
                <Row gutter={[0, 10]} style={{ height: `36vh`, overflow: `auto` }}>
                    <div id={"chat-scroll-div"}>
                        <InfiniteScroll next={fetchOlderMessage} hasMore={page * size >= activeChatRoom.messages.total} loader={<p>...Loading</p>} dataLength={activeChatRoom.messages.total} scrollableTarget="chat-scroll-div">
                            {activeChatRoom?.messages?.listData.map((item, index) => {
                                return <Col span={24} flex={`space-between`} className={'chat-message'}>
                                    <Row justify={item.senderId === currentUser?.id ? "end" : "start"}>
                                        <Tooltip title={new Date().toLocaleTimeString()}>
                                            {/*item.sender === currentUser?.username ? `#3e4042` : `#1890ff`*/}
                                            <Card
                                                className={`chat-bubble ${item.senderId === currentUser?.id ? "my-chat-message" : "other-chat-message"}`}
                                                hoverable
                                                style={{
                                                    maxWidth: 500,
                                                    borderRadius: `1.5em`,
                                                    // backgroundColor: item.sender === currentUser?.username ? `#1890ff` : `#3e4042`
                                                }}
                                                key={index}
                                                bodyStyle={{ color: `white`, wordWrap: "normal" }}
                                            >
                                                <Typography.Text style={{ color: "white" }}>{item.content}</Typography.Text>
                                            </Card>
                                        </Tooltip>
                                    </Row>
                                </Col>;
                            })
                            }
                        </InfiniteScroll>
                    </div>
                </Row>
            </Card>
        }
    }


    return (
        renderFunction()
    );
};

export default ConversationBox;
