import './CalendarDetail.less';
import {Select} from 'antd';
import React from 'react';
import {calendarSelector} from "../../../../redux/features/Calendar/CalendarSlice";
import {useAppSelector} from "../../../../hooks/Redux/hooks";
import CalendarDayDetail from "./CalendarDayDetail/CalendarDayDetail";
import CalendarWeekDetail from "./CalendarWeekDetail/CalendarWeekDetail";
import CalendarMonthDetail from "./CalendarMonthDetail/CalendarMonthDetail";

export const dateHours: string[] = ["00:00",
    "01:00",
    "02:00",
    "03:00",
    "04:00",
    "05:00",
    "06:00",
    "07:00",
    "08:00",
    "09:00",
    "10:00",
    "11:00",
    "12:00",
    "13:00",
    "14:00",
    "15:00",
    "16:00",
    "17:00",
    "18:00",
    "19:00",
    "20:00",
    "21:00",
    "22:00",
    "23:00",
];
export const eventColors: string[] = [
    "#fc3141",
    "#fda666",
    "#f7fd80",
    "#26eccb",
    "#5ca7fb",
    "#9086e3",
];
export const getRandomColors: () => string = () => {
    return eventColors[Math.round(Math.random() * (eventColors.length - 1))];
};
const {Option} = Select;


// * Parent container: Should keep state to render dates event into day view or weekview
const CalendarDetail = () => {
    const currentView = useAppSelector(calendarSelector.currentCalendarView);
    switch (currentView) {
        case "DAY":
            return <CalendarDayDetail/>
        case "WEEK":
            return <CalendarWeekDetail/>
        case "MONTH":
            return <CalendarMonthDetail/>
        case "LIST":
            // TODO: replace null with react component
            return null;
    }
};

export default React.memo(CalendarDetail);
