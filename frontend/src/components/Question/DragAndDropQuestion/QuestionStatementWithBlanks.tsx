import React from 'react';
import {blankRegex} from "../FillBlankQuestion/FillInBlankQuestionDisplay";
import _ from "lodash";
import ReactHtmlParser from "html-react-parser";
import {Typography} from "antd";
import BlankDropTarget from "./BlankDropTarget";
import {DragAndDropAnswer} from "../../../redux/features/Quiz/CreateQuizSlice";


export const DND_BLANK_SPAN_TAG_CLASS = "dnd-question-blank-tag"
export const replaceBlankWithSpecialSpan: (questionText: string) => [string, number] = (questionText: string) => {
    const count = questionText.match(blankRegex)?.length || 0;
    return [_.replace(questionText, blankRegex, `
            <span class="${DND_BLANK_SPAN_TAG_CLASS}">xxx</span>
        `), count];
}

// const [markedString, count] = replaceBlankWithSpecialSpan(htmlString);

const replaceBlankNotationByBLankDropTarget = (questionHTML: string, count: number = 0, keyToContent: (key: number | string) => string, isCreateOrUpdate?: boolean, mode?: string) => {
    let blankCounter = 0;
    return ReactHtmlParser(questionHTML, {
        replace: (node) => {
            if ((node as any).name === "span" && (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS) {
                blankCounter++;
                return <BlankDropTarget hint={""}
                                        answerKey={""}
                                        blankIndex={blankCounter - 1}
                                        keyToContent={keyToContent}
                                        isCreateOrUpdate={isCreateOrUpdate}
                                        mode={mode}
                />
            }
            return node;
        }
    })
}

interface DndQuestionStatementProps {
    // * HTML text marked blanks as []
    questionHTML: string,
    answerList: DragAndDropAnswer[],
    isCreateOrUpdate?: boolean,
    mode?:string
}

const QuestionStatementWithBlanks = (props: DndQuestionStatementProps) => {
    const [questionHTMLReplaced, count] = replaceBlankWithSpecialSpan(props.questionHTML);
    // console.log(questionHTMLReplaced);
    return (
        <div>
            {replaceBlankNotationByBLankDropTarget(questionHTMLReplaced, count, (key: number | string) => {
                    // console.log("SEACHING FOR KEY : ", key, props.answerList?.find(item => item.key == key)?.content);
                    // console.log('Prop answer list: ', props.answerList);
                    return props.answerList?.find(item => item.key == key)?.content || ""
                }, props.isCreateOrUpdate, props.mode) ||
                <Typography.Text type={"danger"}>
                    Nhập câu hỏi vào html editor ở trên để tạo đề cho câu hỏi
                </Typography.Text>
            }
        </div>
    );
};


export default QuestionStatementWithBlanks;