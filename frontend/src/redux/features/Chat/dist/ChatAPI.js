"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.useTestChatRoomExistMutation = exports.useGetActiveChatMessagesQuery = exports.useDeleteChatRoomMutation = exports.useUpdateChatRoomMutation = exports.useCheckRoomAsSeenMutation = exports.useCreateChatRoomMutation = exports.useAddNewMessageToChatRoomMutation = exports.useLazyGetChatsBriefInfoQuery = exports.useGetChatsBriefInfoQuery = void 0;
var CentralAPI_1 = require("../CentralAPI");
var chatApiWithInvalidateTags = CentralAPI_1.apiSlice.enhanceEndpoints({ addTagTypes: ['Chat'] });
var chatAPI = chatApiWithInvalidateTags.injectEndpoints({
    endpoints: function (build) {
        return ({
            // * Get list of brief information of the chat rooms
            getChatsBriefInfo: build.query({
                query: function () { return 'chat'; },
                providesTags: [{ type: 'Chat', id: 'LIST' }]
            }),
            // * Get all the message for the specific chat gruop
            // * A chat GROUP should contain at least 2 member (2 is still OK)
            getActiveChatMessages: build.query({
                query: function (args) { return "chat/" + args.id + "?page=" + args.page + "&size=" + args.size; },
                onCacheEntryAdded: function (_arg, _a) {
                    var cacheDataLoaded = _a.cacheDataLoaded, cacheEntryRemoved = _a.cacheEntryRemoved;
                    return __awaiter(this, void 0, void 0, function () {
                        var _b;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    _c.trys.push([0, 2, , 3]);
                                    // wait for the initial query to resolve before proceeding
                                    return [4 /*yield*/, cacheDataLoaded
                                        // when data is received from the socket connection to the server,
                                        // if it is a message and for the appropriate channel,
                                        // update our query result with the received message
                                        // * We should receive the subscription for ease of subscription
                                        //!! subscription = StompUtils.addListenerToTopic(`chat/${arg.id}`, listener);
                                    ];
                                case 1:
                                    // wait for the initial query to resolve before proceeding
                                    _c.sent();
                                    return [3 /*break*/, 3];
                                case 2:
                                    _b = _c.sent();
                                    return [3 /*break*/, 3];
                                case 3: 
                                // cacheEntryRemoved will resolve when the cache subscription is no longer active
                                return [4 /*yield*/, cacheEntryRemoved
                                    // perform cleanup steps once the `cacheEntryRemoved` promise resolves
                                    // * In our case, the socket connection is persisted throughout each session
                                    // * So, we only need to remove the listener
                                    //!! subscription?.unsubscribe();
                                ];
                                case 4:
                                    // cacheEntryRemoved will resolve when the cache subscription is no longer active
                                    _c.sent();
                                    return [2 /*return*/];
                            }
                        });
                    });
                }
            }),
            createChatRoom: build.mutation({
                invalidatesTags: function (_result, _err) { return [{ type: 'Chat', id: 'LIST' }]; },
                query: function (body) {
                    return {
                        url: "chat/room",
                        method: 'POST',
                        body: body
                    };
                }
            }),
            checkRoomAsSeen: build.mutation({
                invalidatesTags: function (_result, _err) { return [{ type: 'Chat', id: 'LIST' }]; },
                query: function (chatRoomId) {
                    return {
                        url: "chat/seen/" + chatRoomId,
                        method: 'POST'
                    };
                }
            }),
            // * id: Chat room id, message :
            addNewMessageToChatRoom: build.mutation({
                invalidatesTags: function (_result, _err, args) { return [{ type: 'Chat', id: 'LIST' }, {
                        type: 'Chat',
                        id: args.chatRoomId
                    }]; },
                query: function (body) {
                    return {
                        url: "chat/send",
                        method: 'POST',
                        body: body
                    };
                }
            }),
            testChatRoomExist: build.mutation({
                invalidatesTags: function (_result, _err, _arg) { return [{ type: 'Chat', id: 'LIST' }]; },
                query: function (body) {
                    return {
                        url: "/chat/test",
                        method: "POST",
                        body: body
                    };
                }
            }),
            // ! Below methods are not implemented --> Don't test these yet
            updateChatRoom: build.mutation({
                invalidatesTags: function (_result, _err, arg) { return [{ type: 'Chat', id: arg.id }, { type: 'Chat', id: 'LIST' }]; },
                query: function (body) {
                    return {
                        url: "chat/room",
                        method: 'PATCH',
                        body: body
                    };
                }
            }),
            deleteChatRoom: build.mutation({
                invalidatesTags: function (_result, _err, id) { return [{ type: 'Chat', id: id }, { type: 'Chat', id: 'LIST' }]; },
                query: function (body) {
                    return {
                        url: "chat/room",
                        method: 'DELETE',
                        body: body
                    };
                }
            })
        });
    },
    overrideExisting: false
});
exports.useGetChatsBriefInfoQuery = chatAPI.useGetChatsBriefInfoQuery, exports.useLazyGetChatsBriefInfoQuery = chatAPI.useLazyGetChatsBriefInfoQuery, exports.useAddNewMessageToChatRoomMutation = chatAPI.useAddNewMessageToChatRoomMutation, exports.useCreateChatRoomMutation = chatAPI.useCreateChatRoomMutation, exports.useCheckRoomAsSeenMutation = chatAPI.useCheckRoomAsSeenMutation, exports.useUpdateChatRoomMutation = chatAPI.useUpdateChatRoomMutation, exports.useDeleteChatRoomMutation = chatAPI.useDeleteChatRoomMutation, exports.useGetActiveChatMessagesQuery = chatAPI.useGetActiveChatMessagesQuery, exports.useTestChatRoomExistMutation = chatAPI.useTestChatRoomExistMutation;
// * Provide tags for cache invalidation when code splitting
// * https://github.com/reduxjs/redux-toolkit/issues/1510
