import {Col, Row} from 'antd';
import React from 'react'
import {DrawerProps} from 'antd';
import SettingButton from '../../ActionButton/SettingButton';
import DrawerWithDoneCancel from '../../DrawerHOC/DrawerWithDoneCancel';
import {Field, Formik, useFormikContext} from 'formik';
import {Form, FormItem, Input} from 'formik-antd';
import {Test, useGetTestQuery, useUpdateTestMutation} from "../../../redux/features/Quiz/examAPI";
import {useAppSelector} from "../../../hooks/Redux/hooks";
import {createTestContentSelectors} from "../../../redux/features/Quiz/CreateQuizSlice";
import {useParams} from "react-router-dom";
import {UseQueryResult} from '../../../redux/interfaces/types';
import ApiErrorWithRetryButton from "../../Util/ApiErrorWithRetryButton";
import LoadingPage from "../../../pages/UtilPages/LoadingPage";
import CourseSearchSelectField from "../../CustomFields/CourseSearchSelectField/CourseSearchSelectField";
import InputField from "../../CustomFields/CustomInputField/InputField";
import antdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import { testContentMetaSchema } from '../../../pages/Tenant/Courses/CourseQuiz/CourseQuizBank';

type Props = DrawerProps & {
    onCancelButtonClick?: Function,
    onDoneButtonClick?: Function
}





const TestSettingsDrawerWithForm = ({courseId} : {courseId: number}) => {
    const {testId} = useParams();
    const questionSearchCriteria = useAppSelector(createTestContentSelectors.selectSearchCriteria);
    const {
        data: testConfigurationOptions,
        isSuccess,
        isError,
        isFetching,
        isUninitialized
    } = useGetTestQuery({testId: Number(testId), courseId,...questionSearchCriteria}, {
        selectFromResult: (result: UseQueryResult<Test>) => {
            const {isError, isSuccess, isFetching, isLoading, isUninitialized} = result;
            if (!result.data) {
                return {};
            }
            const {questionList = [], ...configurations} = result.data;
            // console.log("QUERY RESULT IS:", configurations);
            return {
                data: configurations as Omit<Test, "questionList">,
                isError,
                isSuccess,
                isFetching,
                isLoading,
                isUninitialized
            };
        }
    });

    const [updateTest] = useUpdateTestMutation()
    const testContentId = Number(testId);
    if (isError || isUninitialized) {
        return <Row justify={"center"} align={"middle"}>
            <ApiErrorWithRetryButton onRetry={() => {
            }}/>
        </Row>
    }
    if (isFetching) {
        return <LoadingPage/>
    }
    if (isSuccess) {
        return <Formik initialValues={{...testConfigurationOptions}} onSubmit={(values) => {
            const valueToSubmit = {
                ...values,
                id: testContentId,
                courseId
            }
            // @ts-ignore
            updateTest(valueToSubmit)
                .unwrap()
                .then(res => {
                    antdNotifier.success("Success")
                })
                .catch(err => {
                    antdNotifier.success("Somethign went wrong")

                });
        }}
                       validationSchema={testContentMetaSchema}
        >
            {
                ({values, dirty, touched, isValid, resetForm,handleSubmit}) => {
                    return <>
                        <DrawerWithDoneCancel trigger={<SettingButton onClick={() => {
                        }}/>}
                                              content={
                                                  <>
                                                      <Form layout="vertical" hideRequiredMark>
                                                          <Row gutter={16}>
                                                              <Col span={12}>
                                                                  <FormItem
                                                                      name="title"
                                                                      label="Tiêu đề"
                                                                      rules={[{
                                                                          required: true,
                                                                          message: 'Tên của bộ câu hỏi này'
                                                                      }]}
                                                                  >
                                                                      <Field name={"title"}
                                                                             placeholder={"Type article content"}
                                                                             component={InputField}/>
                                                                  </FormItem>
                                                              </Col>
                                                              <Field name={"courseId"}
                                                                     placeholder={"Type article content"}
                                                                     component={CourseSearchSelectField}
                                                                     label={"Khoá"}/>

                                                              <Col span={24}>
                                                                  <FormItem
                                                                      name="description"
                                                                      label="Mô tả"
                                                                  >
                                                                      <Input.TextArea
                                                                          rows={4}
                                                                          style={{width: '100%'}}
                                                                          placeholder="Mô tả của bộ câu hỏi này"
                                                                          name={"description"}
                                                                      />
                                                                  </FormItem>
                                                              </Col>
                                                          </Row>
                                                          {/*{*/}
                                                          {/*    JSON.stringify(values)*/}
                                                          {/*}*/}
                                                      </Form>

                                                  </>
                                              } onCancel={
                            () => {
                                resetForm();
                            }
                        } onDone={
                            () => {
                                handleSubmit()
                                console.log("Done on setting form")
                            }
                        } title="Cài đặt cho bộ câu hỏi" doneButtonProps={{disabled: !isValid || !dirty}}
                        />
                    </>;
                }

            }
        </Formik>;
    }
    return <>
    </>
}

export default TestSettingsDrawerWithForm;

