import {Avatar, Space, Table} from "antd";
import {useEffect, useState} from "react";
import {GradeResult as GradeResultType} from "../../redux/features/Grade/GradeApi";
import {Tag} from "../../redux/features/Grade/GradeFormulaSlice";

const baseColumns = [
    {
        title: "Học viên",
        key: "studentName",
        fixed: 'left',
        width: 250,
        render: (gradeResult: GradeResultType) => <Space><Avatar src={gradeResult.avatar}/>{gradeResult.studentName}
        </Space>
    }]

const GradeResult = ({
                         gradeResults,
                         tags,
                         isLoading
                     }: { gradeResults: GradeResultType[], tags: Tag[], isLoading: boolean }) => {

    let [columns, setColumns] = useState<any>(baseColumns)

    const [pageSize, setPageSize] = useState<number>(10);
    const [page, setPage] = useState<number>(1);

    useEffect(() => {
        setColumns([
            ...baseColumns,
            ...(tags.map((tag, index) => ({
                title: tag.title,
                key: "tag_" + tag.id,
                align: "center",
                sorter: (a: any, b: any) => {
                    if (typeof a === "string" && typeof b === "string") {
                        return 0;
                    }
                    if (typeof a === "string") return -1;
                    if (typeof b === "string") return 1;
                    return a.grades.find((g: any) => g.tagId === tag.id)?.grade - b.grades.find((g: any) => g.tagId === tag.id)?.grade;
                },
                render: (gradeResult: GradeResultType) => <>{gradeResult.grades.find(grade => grade.tagId === tag.id)?.grade?.toFixed(2) || "Chưa có điểm"}</>
            })) || []),
        ]);
    }, [tags]);

    // We use frontend paging here because this page doesn't contain any searching
    return <Table
        loading={isLoading}
        columns={columns}
        dataSource={gradeResults}
        rowKey="id"
        locale={{emptyText: 'Không có dữ liệu'}}
        pagination={{
            pageSize: pageSize,
            current: page,
            showSizeChanger: true,
            onShowSizeChange: (current: number, size: number) => {
                setPageSize(size);
                setPage(page);
            },
            onChange: (page: number, size: number) => {
                setPage(page);
            }
        }}
        scroll={{x: true}}
    />
}

export default GradeResult;