import { apiSlice } from "../../CentralAPI";
import { BaseResponse } from "../../../interfaces/types";

const hostAuthApiInvalidatesTags = apiSlice.enhanceEndpoints({
  addTagTypes: ["HostAuth"],
});

interface LoginHostResponse extends BaseResponse {
  user: {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
  };
  token: string;
  refreshToken: string;
  tenant: string;
}

const hostAuthAPI = hostAuthApiInvalidatesTags.injectEndpoints({
  endpoints: (build) => ({
    loginHost: build.mutation<
      LoginHostResponse,
      { username: string; password: string }
    >({
      query: (body) => ({
        url: "/tenant/login",
        method: "POST",
        body
      }),
    }),
  }),
  overrideExisting: false,
});

export const { useLoginHostMutation } = hostAuthAPI;
