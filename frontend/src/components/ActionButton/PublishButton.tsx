import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, ButtonProps } from "antd";
import {
	HideIcon,
	PublishIcon,
} from "@pages/Tenant/Courses/CourseContent/CourseContentPage";
const PublishButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			className="publish-button"
			type={"primary"}
			icon={<PublishIcon style={{ margin: "0 .5em 0 0" }} />}
		>
			{`${props.title || "Hiện bài kiểm tra cho học viên"}`}
		</Button>
	);
};

export default PublishButton;
