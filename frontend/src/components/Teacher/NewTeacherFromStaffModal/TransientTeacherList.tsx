import { faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useAppSelector, useAppDispatch } from "@hooks/Redux/hooks";
import { useChangeRoleOfTeacherMutation } from "@redux_features/Class/TeacherAPI/ClassTeacherAPI";
import { TeacherRole } from "@redux_features/Class/TeacherRole.1";
import { Dropdown } from "antd";
import { ID } from "redux/interfaces/types";
import {
    classTeacherActions,
    classTeacherSelectors,
} from "../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";
import TeacherList from "../TeacherList";
import TeacherRoleMenu from "../TeacherRoleMenu";
import TeacherRoleTag from "../TeacherRoleTag";
import TeacherRoleTagMenuWrapper from "../TeacherRoleTagMenuWrapper";
import "./TransientTeacherList.less";
// TODO: haveAdmin PERMISSION CHECK
const TransientTeacherList = ({ classId }: { classId: ID }) => {
    const dispatch = useAppDispatch();
    const haveAdminPermission = true;
    const transientTeachers = useAppSelector(
        classTeacherSelectors.selectTeacherInTransient
    );
    const teachers = Object.values(transientTeachers);
    if (!transientTeachers || teachers.length < 1) {
        return <span> Không có giáo viên nào </span>;
    }
    const handleChangeCandidateRole = (id: ID, newRole: TeacherRole) => {
        dispatch(
            classTeacherActions.changeTransientCandidateRole({
                id,
                role: newRole,
            })
        );
    };
    return (
        <TeacherList
            listData={teachers}
            additionalItemActions={({ itemId }) => {
                return (
                    <div
                        className="card-action-item card-delete-action"
                        onClick={() =>
                            dispatch(
                                classTeacherActions.removeCandidateFromTransient(
                                    itemId
                                )
                            )
                        }
                    >
                        <FontAwesomeIcon icon={faCircleXmark} />
                    </div>
                );
            }}
            teacherRoleTag={(item) => {
                const itemId = item.id || item.userId;
                if (!item.role || !itemId) {
                    return <></>;
                }
                return (
                    <Dropdown
                        overlay={
                            <TeacherRoleMenu
                                onSelect={(role) => {
                                    handleChangeCandidateRole(itemId, role);
                                }}
                            />
                        }
                    >
                        <span>
                            <TeacherRoleTag role={item.role} />
                        </span>
                    </Dropdown>
                );
            }}
        />
    );
};

export default TransientTeacherList;
