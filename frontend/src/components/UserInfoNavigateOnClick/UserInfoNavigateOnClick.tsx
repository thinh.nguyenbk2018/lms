import React from 'react';
import {UserInfoForClickNavigation} from "../../redux/features/Auth/AuthSlice";
import {Avatar, Tooltip} from "antd";

const UserInfoNavigateOnClick = (props: UserInfoForClickNavigation) => {
    return (
        <div>
            <Tooltip title={`${props.firstName} ${props.lastName}`}>
                <Avatar src={props.avatar} alt={props.lastName}>
                    {props.firstName}
                </Avatar>
            </Tooltip>
        </div>
    );
};

export default UserInfoNavigateOnClick;