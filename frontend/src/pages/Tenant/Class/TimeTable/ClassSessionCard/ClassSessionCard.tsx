import {blue, red} from "@ant-design/colors";
import {Avatar, Col, Dropdown, Menu, Row, Space, Tooltip, Typography} from "antd";
import {
    ClassSession,
    ClassSessionResponse,
    useDeleteClassSessionMutation,
} from "../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableAPI";
import "./ClassSessionCard.less";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faCheckCircle, faCircleUser,
    faCircleXmark,
    faClock,
    faHouse,
    faPenToSquare,
    faStickyNote, faUserCheck,
} from "@fortawesome/free-solid-svg-icons";
import {useNavigate, useParams} from "react-router-dom";
import ApiErrorWithRetryButton from "../../../../../components/Util/ApiErrorWithRetryButton";
import IMAGE_CONSTANTS from "../../../../../constants/DefaultImage";
import {format, formatRelative} from "date-fns";
import {parseDate} from "../../../../../components/Calendar/Utils/CalendarUtils";
import {useAppDispatch, useAppSelector} from "../../../../../hooks/Redux/hooks";

import {vi} from "date-fns/locale";
import {classTimeTableActions} from "../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableSlice";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import React, {useMemo} from "react";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {UnitIcon} from "@components/TeachingContent/NewActivityMenu";
import {authSelectors} from "@redux/features/Auth/AuthSlice";

type ActionMenuProps = {
    onSelect?: (key: string) => void;
};
const ActionMenu = React.memo((props: ActionMenuProps) => {
    const {onSelect} = props;
    const menuItems = useMemo(() => {
        return [
            {
                label: "Cập nhật",
                icon: (
                    <FontAwesomeIcon
                        icon={faPenToSquare}
                        style={{color: blue.primary}}
                    />
                ),
                key: "EDIT",
            },
            {
                label: "Xóa",
                icon: (
                    <FontAwesomeIcon icon={faCircleXmark} style={{color: red[6]}}/>
                ),
                key: "DELETE",
            },
        ];
    }, []);

    return (
        <>
            <Menu
                items={menuItems}
                onClick={({key}) => {
                    onSelect?.(key);
                }}
            />
        </>
    );
});

const ClassSessionCard = (
    props: ClassSessionResponse & { type: "AUTOMATIC" | "MANUAL" }
) => {
    const dispatch = useAppDispatch();

    const {id, teacher, unit, startedAt, finishedAt, room, note} = props;
    const {type, ...session} = props;
    const {classId} = useParams();
    const navigate = useNavigate();
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const mapToSession = (dto: ClassSessionResponse): ClassSession => {
        return {...dto, teacherId: dto.teacher?.id, unitId: dto.unit?.id, date: dto.startedAt, notifyWithEmail: false}
    }
    const [deleteSession] = useDeleteClassSessionMutation();

    if (true) {
        return (
            <>
                <Row className="class-session-card-container">
                    <Col span={7} className={"teacher-information"}>
                        <div className="teacher-decal">Giáo viên giảng dạy</div>
                        {teacher && <Avatar
                            style={{backgroundColor: blue[2], verticalAlign: "middle"}}
                            size="large"
                            gap={2}
                            src={teacher?.avatar || IMAGE_CONSTANTS.DEFAULT_AVATAR}
                            className="teacher-avatar"
                        >
                            {`${teacher?.firstName}, ${teacher?.lastName}`}
                        </Avatar>}
                        {teacher ? (
                            <p className="teacher-name">
                                {`${teacher?.lastName} ${teacher?.firstName}`}
                            </p>
                        ) : (
                            <Typography.Text>Buổi học chưa được phân công giáo viên.</Typography.Text>
                        )}
                    </Col>
                    <Col span={9} className="session-information-container">
                        <div className="unit-decal"> Thông tin bài học</div>
                        <div className={"unit-wrapper"}>
                            <UnitIcon/>
                            {unit?.title
                                ? (<Typography.Link onClick={() => {
                                    navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE, [":classId", classId || ""], [":unitItemId", unit?.id || ""]));
                                }}>
                                    <p className="session-unit-name">{unit?.title}</p>
                                </Typography.Link>)
                                : (<Typography.Text>Chưa có bài học nào được thêm.</Typography.Text>)
                            }
                        </div>
                        <div className={"note-wrapper"}>
                            <FontAwesomeIcon icon={faStickyNote} className={"clock-icon"}/>
                            <div className="session-duration-container">
                                <p className="session-note">
                                    {note ? note : 'Không có ghi chú nào được thêm.'}
                                </p>
                            </div>
                        </div>
                        {userInfo?.accountType === 'STAFF' && <div className={"attendance-wrapper"}>
                            <Space>
                                <FontAwesomeIcon color={blue[5]} icon={faUserCheck}/>
                                <Typography.Link
                                    onClick={() => navigate(replacePathParams(replacePathParams(ROUTING_CONSTANTS.CLASS_CHECK_IN_SESSION_DETAIL, [":classId", classId || ""], [":sessionId", id || ""])))}>
                                    Điểm danh
                                </Typography.Link>
                            </Space>
                        </div>}
                        {/*<p className="session-information-note room">{room ? `Phòng học: ${room}` : ''}</p>*/}
                        {/*<p className="session-information-note">{note ? `Ghi chú: ${note}` : ''}</p>*/}
                    </Col>
                    <Col span={8} className="session-time-information">
                        <div className="session-date-container">
                            <Space align="center">
                                <Tooltip
                                    title={
                                        formatRelative(parseDate(startedAt), new Date(), {
                                            locale: vi,
                                        }) || "No info"
                                    }
                                >
                                    <p className="session-date" style={{marginBottom: "0"}}>
                                        {format(parseDate(startedAt), "PP", {locale: vi})}
                                    </p>
                                </Tooltip>
                                <PermissionRenderer permissions={[PermissionEnum.UPDATE_CLASS_SCHEDULER]}>
                                    <Dropdown
                                        overlay={
                                            <ActionMenu
                                                onSelect={(key) => {
                                                    switch (key) {
                                                        case "EDIT":
                                                            dispatch(
                                                                classTimeTableActions.editClassSession(mapToSession(session))
                                                            );
                                                            return;
                                                        case "DELETE":
                                                            if (!id) {
                                                                AntdNotifier.error(
                                                                    "Bạn không được xóa buổi học này. Liên hệ team quản trị"
                                                                );
                                                                return;
                                                            }
                                                            confirmationModal('buổi học', format(parseDate(startedAt), "PP", {locale: vi}), () => {
                                                                AntdNotifier.handlePromise(
                                                                    deleteSession({
                                                                        classId: Number(classId),
                                                                        sessionId: id,
                                                                    }).unwrap(),
                                                                    "Xóa buổi học"
                                                                );
                                                            });
                                                            return;
                                                        default:
                                                            return;
                                                    }
                                                }}
                                            />
                                        }
                                    >
                                        <FontAwesomeIcon
                                            icon={faPenToSquare}
                                            className={`icon edit`}
                                        />
                                    </Dropdown>
                                </PermissionRenderer>
                            </Space>
                        </div>
                        <div className={"session-time-wrapper"}>
                            <FontAwesomeIcon icon={faClock} className={"clock-icon"}/>
                            <div className="session-duration-container">
                                <p className="session-time session-time-start">
                                    {format(parseDate(startedAt), "HH:mm")}
                                </p>
                                <p className={"session-time dash"}> - </p>
                                <p className="session-time session-time-end">
                                    {format(parseDate(finishedAt), "HH:mm")}
                                </p>
                            </div>
                        </div>
                        <div className={"session-time-wrapper"}>
                            <FontAwesomeIcon icon={faHouse} className={"clock-icon"}/>
                            <div className="session-duration-container">
                                <p className="session-time session-time-start">
                                    {room ? room : 'Chưa có phòng học được thêm.'}
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>
            </>
        );
    }
        // else if (isFetchingTeacher || isFetchingUnit) {
        // 	return <LoadingPage />;
    // }
    else {
        return (
            <ApiErrorWithRetryButton
                onRetry={function (): void {
                    window.location.reload();
                }}
            />
        );
    }
};

export default ClassSessionCard;