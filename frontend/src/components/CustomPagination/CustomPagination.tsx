import {Pagination} from "antd";

type CustomPaginationProps = {
    total: number;
    onChange: (page: number, size: number) => void
    current: number;
    size: number;
    onShowSizeChange: (current: number, size: number) => void
}

const CustomPagination = ({total, onChange, current, size, onShowSizeChange}: CustomPaginationProps) => {
    return (
        <Pagination
            showSizeChanger
            onShowSizeChange={(current: number, size: number) => {onShowSizeChange(current, size)}}
            onChange={onChange}
            total={total}
            current={current}
            pageSize={size}
        />
    );
}

export default CustomPagination;
