import React, { useRef } from "react";
import { Identifier } from "dnd-core";
import { useDrag, useDrop } from "react-dnd";
import { TeachingContentDnDTypes } from "../../../../../constants/DragTypes";
import { ActivityType } from "../../../../../typescript/interfaces/courses/CourseTypes";

export type ActivityPlacementInfo = {
	activityId: number;
	chapterId: number;
	order: number;
};


export const packageActivityPlacementInfo = (
	activityId: number | undefined,
	chapterId: number,
	itemOrder: number | undefined
) => {
	return {
		activityId: activityId,
		chapterId: chapterId,
		order: itemOrder,
	} as ActivityPlacementInfo;
};
export interface DraggedActivity {
	originalIndex: number;
	parentChapterIndex: number;
	order: number;

	type: ActivityType;
}
type DropProps = {
	handlerId: Identifier | null;
	isOver: boolean;
	canDrop: boolean;
};

type DragProps = {
	isDragging: boolean;
	didDrop: boolean;
};

type DraggableDroppableActivityProps = {
	id: number;
	chapterId: number;
	order: number;
	type: ActivityType;
	// * Helper for this activity
	children: JSX.Element;

	exchangeHandler: (
		from: ActivityPlacementInfo,
		to: ActivityPlacementInfo
	) => void;
};
const DraggableDroppableActivity = (props: DraggableDroppableActivityProps) => {
	const ref = useRef<HTMLDivElement>(null);
	const [{ handlerId, canDrop, isOver }, drop] = useDrop<
		DraggedActivity,
		void,
		DropProps
	>({
		accept: TeachingContentDnDTypes.Chapter,
		collect: (monitor) => {
			return {
				handlerId: monitor.getHandlerId(),
				canDrop: monitor.canDrop(),
				isOver: monitor.isOver(),
			};
		},
		hover(item) {
			if (!ref.current) {
				return;
			}
			const draggedItemId = item.originalIndex;
			const hoveredItemId = props.id;
			if (draggedItemId === hoveredItemId) {
				return;
			}
			// * TODO: Display an empty box showing the hover can result in a drop
		},
		drop: (item, monitor) => {
			const sourceItemId = item.originalIndex;
			const sourceChapterId = item.parentChapterIndex;
			const sourceOrder = item.order;

			const targetChatperId = props.chapterId;
			const targetItemId = props.id;
			const targetOrder = props.order;

			// * Call api to handle the drop action
			props.exchangeHandler(
				packageActivityPlacementInfo(
					sourceItemId,
					sourceChapterId,
					sourceOrder
				),
				packageActivityPlacementInfo(
					targetItemId,
					targetChatperId,
					targetOrder
				)
			);
		},
	});

	const [{ isDragging, didDrop }, drag] = useDrag<
		DraggedActivity,
		void,
		DragProps
	>({
		type: TeachingContentDnDTypes.Chapter,
		item: () => {
			return {
				originalIndex: props.id,
				type: props.type,
				parentChapterIndex: props.chapterId,
				order: props.order,
			};
		},
		collect: (monitor) => ({
			isDragging: monitor.isDragging(),
			didDrop: monitor.didDrop(),
		}),
	});
	// TODO: Extract class name form isDragging and canDrop to add extra styling to guide the interaction
	drag(drop(ref));
	return (
		<div ref={ref} className={""} data-handler-id={handlerId} style={{}}>
			{props.children}
		</div>
	);
};
export default DraggableDroppableActivity;
