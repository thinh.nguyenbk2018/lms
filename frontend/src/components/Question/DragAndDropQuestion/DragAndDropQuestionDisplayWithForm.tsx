import { Form } from "formik-antd";
import React from "react";
import { QuestionViewAndTakingTestFormProps } from "../interface/question.interface";
import { DragAndDropAnswer, FillBlankDragAndDropQuestion, } from "../../../redux/features/Quiz/CreateQuizSlice";
import { FormikProps, withFormik } from "formik";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import * as Yup from "yup";
import { DEFAULT_FRONTEND_ID } from "../../../redux/interfaces/types";
import { QuestionAnswer } from "../../../redux/features/Quiz/examAPI";
import QuestionStatementWithBlanks from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import { DraggableAnswerInViewMode } from "./DraggableAnswer";
import { Card, Divider, Row, Space, Typography } from "antd";
import { baseEditQuestionYupSchema } from "../QuestionContainerItem/baseQuestionSchema";
import "./DragAndDropQuestionDisplayWithForm.less";
import QuestionSubmissionButton from "../../ActionButton/QuestionSubmissionButton";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

/** Display the test for review the question content and possibly retrieve the answer */
const WrappedDragAndDropQuestionDisplay = (
	props: DragAndDropViewHocProps & FormikProps<QuestionAnswer>
) => {
	// * Question info to display the requirements
	const { description, blankList, answerList, mode, attachment } = props;
	const answerListDefined = answerList?.filter(
		(item) => item
	) as DragAndDropAnswer[];
	// * Form State from formik
	const {
		values,
		dirty,
		touched,
		isValid,
		handleSubmit,
		setFieldValue,
		errors,
	} = props;

	if (!description || !answerList || !answerListDefined) {
		return null;
	}

	return (
		<>
			<Form>
				<Card>
					<div className={"dnd-question-content-container"}>
						{
							JSON.parse(attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
								<AttachmentRender attachment={a} />
							</Row>)
						}
						<Divider />
						<div className={"answer-list-container"}>
							<Space>
								{answerListDefined.map((item, index) => {
									return (
										<DraggableAnswerInViewMode
											index={index}
											answerKey={item.key}
										>
											<Typography.Text
												type={"secondary"}
												style={{ padding: "1em", margin: "1em" }}
											>
												{item.content}
											</Typography.Text>
										</DraggableAnswerInViewMode>
									);
								})}
							</Space>
						</div>
						<Divider>
							<Typography.Title level={5}>
								Kéo và thả các lựa chọn ở trên vào các ô trống bên dưới
							</Typography.Title>
						</Divider>
						<QuestionStatementWithBlanks
							questionHTML={description}
							answerList={answerListDefined}
							mode={"doing"}
						/>
					</div>
					{/*{mode === "doing" && (*/}
					{/*	<Row style={{ margin: "1em 0" }} justify="end">*/}
					{/*		<QuestionSubmissionButton*/}
					{/*			onClick={() => handleSubmit()}*/}
					{/*			title={"Gửi đáp án"}*/}
					{/*		/>*/}
					{/*	</Row>*/}
					{/*)}*/}
				</Card>
			</Form>
		</>
	);
};

// * Question configuration props with Formik HOC
const fillInBlankAnswerSchema = Yup.object({
	/*answers: Yup.array().of(
		Yup.string().required("Câu trả lời cho một ô trống không được để trống")
	),*/
}).concat(baseEditQuestionYupSchema);

type DragAndDropViewHocProps =
	QuestionViewAndTakingTestFormProps<FillBlankDragAndDropQuestion>;
/** A question displayer need the Partial<QuestionDataType> to display the question statement
 *  ? QuestionAnswer to record student choice quen taking the test
 *
 * */
const DragAndDropQuestionDisplayWithForm = withFormik<
	DragAndDropViewHocProps,
	QuestionAnswer
>({
	mapPropsToValues: (props) => {
		return {
			id: props.id || DEFAULT_FRONTEND_ID,
			description: props.description || DEFAULT_TEXT_EDITOR_HTML,
			point: props.point || 0,
			blankList: props.blankList || [],
			answers: props.blankList?.map(b => b?.studentAnswer || "") || [],
			optionalStudentNote: "",
		};
	},
	validationSchema: fillInBlankAnswerSchema,
	handleSubmit: (values, formikBag) => {
		const valuesToSubmit: QuestionAnswer = {
			...values,
		};
		formikBag.props.onConfirmSubmit?.(valuesToSubmit);
		// console.log("FIB_Question answer values is: ", values);
	},
})(WrappedDragAndDropQuestionDisplay);
export default DragAndDropQuestionDisplayWithForm;
