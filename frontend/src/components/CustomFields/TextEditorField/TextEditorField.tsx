import React, {CSSProperties, useCallback} from 'react';
import {FieldProps} from "formik";
import TextEditor, {TextEditorProps} from "../../TextEditor/TextEditor";
import {FormItem} from 'formik-antd';

// * FieldProps = {value,name,onBlur,onChange}
// * {} : Custom props this custom field will take
type CustomFieldProps =
    FieldProps<any>
    & TextEditorProps
    & { label?: string, style?: CSSProperties, required?: boolean };

const TextEditorField = ({ field,
    form: {
        touched,
        errors,
        setFieldValue,
        setFieldError,
        handleBlur,
        handleChange,
        setTouched,
        setFieldTouched
    },
    ...otherProps
}: CustomFieldProps) => {
    // * Below is what you can get from the field props
    const { name, value, onChange, onBlur } = field;

    // * Take error from the Validate prop of formik (from yup validation schema)
    const errorMsg = touched[name] && errors[name];

    const changeHandler = useCallback((htmlText: string) => {
        setFieldValue(name, htmlText, false)
    }, [])
    // * Need to wrap inside Form.Item is because TextEditor doesn't accept the stuffs such as handleChange,handleBlue,onBlur,...
    return (
        <>
            <FormItem required={otherProps?.required} label={otherProps?.label || ""} name={name}>
                <TextEditor {...field} {...otherProps} onChange={changeHandler} onBlur={() => {
                    setFieldTouched(name, true, true);
                }} />
            </FormItem>
        </>
    );
};

export default TextEditorField;