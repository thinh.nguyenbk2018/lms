import React from "react";
import {FillInBlankQuestion, Question, QuestionDataType} from "../../../redux/features/Quiz/CreateQuizSlice";
import {RecursivePartial} from "../../../typescript/typehelper/RecursivePartial";
import {PartialExcept} from "../../../typescript/typehelper/RecursivePartialExcept";
import {QuestionAnswer} from "../../../redux/features/Quiz/examAPI";
import {PartialBy} from "../../../typescript/typehelper/PartialBy";

export interface QuestionDrawerProps<T extends QuestionDataType> {
    trigger?: JSX.Element;
    mRef?: React.ForwardedRef<HTMLDivElement> | null;
    /** Used for updating operations */
    initQuestionState?: (PartialBy<T,"id"> ) | undefined;
    // * This should be either a create or upsdate mutaiotn
    onDoneSubmit?: (values:QuestionDataType) => void;
    mode: "create" | "edit";
    testId: number
    /** Additional information so we can map initial values for formik
     * Can we use a naive check ? does id exist or passed in ???
     *
     * */
}


export type QuestionConfigurationProps = {
    canRetry: boolean,
    timeLimit: number,
    // * Some minor configuration for each question
}

/** additional callback: Useful for calling the mutation to submit the answer of student */
export interface QuestionDisplayFormProp {
    onConfirmSubmit?: (values: QuestionAnswer) => void
}

export type QuestionViewAndTakingTestFormProps<T extends Question> =
    QuestionDisplayFormProp & Partial<QuestionConfigurationProps>
    & PartialExcept<T, "description"> &{ mode: "view" | "doing";};

