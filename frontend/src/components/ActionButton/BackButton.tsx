import {ArrowLeftOutlined, CheckOutlined} from '@ant-design/icons';
import {Button, ButtonProps, Space} from 'antd';
import React from 'react';

const BackButton = (props : ButtonProps) => {
    return (
        <Button type={"primary"} {...props} icon={<ArrowLeftOutlined/>}>
            Quay lại
        </Button>
    );
};

export default BackButton;