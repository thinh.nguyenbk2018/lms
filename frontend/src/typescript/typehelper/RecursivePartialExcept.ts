import {RecursivePartial} from "./RecursivePartial";

/** ake everything optional except for the keys specified in the K parameter */
export type PartialExcept<T, K extends keyof T> = RecursivePartial<T> & Pick<T, K>;