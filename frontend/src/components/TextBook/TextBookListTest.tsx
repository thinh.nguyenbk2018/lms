import React from "react";
import { useAppSelector } from "../../hooks/Redux/hooks";
import LoadingPage from "../../pages/UtilPages/LoadingPage";
import { useGetTextbooksQuery } from "../../redux/features/Textbook/TextbookAPI";
import { textbookSelector } from "../../redux/features/Textbook/TextbookSlice";
import { changeCriteria } from "../../redux/features/Textbook/TextbookSlice";
import { TextBook } from "../../redux/features/Textbook/TextbookType";
import { PaginatedResponse } from "../../redux/interfaces/types";
import TextbookCard from "../Card/CustomCard/BookCard/TextbookCard";
import ApiErrorWithRetryButton from "../Util/ApiErrorWithRetryButton";
import "./TextBookList.less";

const textBookData: PaginatedResponse<TextBook> = {
	page: 1,
	total: 5,
	perPage: 3,
	totalPages: 3,
	listData: [
		{
			id: 1,
			name: "Pre Ielts 1 By Coincidence",
			author: "Hong Minh",
			attachment:
				"https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/audios%2Fthinh%40gmail.com%2FLight%20Switch%20-%20Charlie%20Puth.mp3?alt=media&token=8d6c9178-062f-4b01-8ced-1a5ae3be90c4",
			description: "Text book 1 is the best",
		},
		{
			id: 2,
			name: "Barron TOEIC",
			author: "Hong Minh",
			attachment:
				"https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/audios%2Fthinh%40gmail.com%2FLight%20Switch%20-%20Charlie%20Puth.mp3?alt=media&token=8d6c9178-062f-4b01-8ced-1a5ae3be90c4",
			description: "Text book 1 is the best",
		},
		{
			id: 3,
			name: "Prepare for the IELTS Test",
			author: "Yen Ngoc",
			attachment:
				"https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/audios%2Fthinh%40gmail.com%2FLight%20Switch%20-%20Charlie%20Puth.mp3?alt=media&token=8d6c9178-062f-4b01-8ced-1a5ae3be90c4",
			description: "Text book 1 is the best",
		},
	],
};
const TextBookListTest = () => {
	return (
		<div className="textbook-grid">
			{textBookData.listData.map((item, index) => {
				return (
					<>
						<TextbookCard {...item} key={index}/>
					</>
				);
			})}
		</div>
	);
};

export default TextBookListTest;
