import { BaseResponse } from "../../../interfaces/types";
import { apiSlice } from "../../CentralAPI";

const extendServiceApiInvalidatesTags = apiSlice.enhanceEndpoints({
  addTagTypes: ["expire_times", "packages"],
});

// localhost:8080/payment?vnp_OrderInfo=Gia hạn dịch vụ cho tenant test&amount=25000&language=vn
interface GetPaymentURLRequest {
  gateway: "MOMO" | "VNPAY";
  packageId: number;
}

interface GetPaymentURLResponse extends BaseResponse {
  paymentUrl: string;
}

interface GetExpireTimeResponse extends BaseResponse {
  expireTime: Date;
}

interface Package {
  id: number,
  numberOfMonths: number,
  price: number
}

interface GetPackageResponse extends BaseResponse {
  packages: Package[]
}

const extendServiceAPI = extendServiceApiInvalidatesTags.injectEndpoints({
  endpoints: (build) => ({
    getPaymentUrl: build.mutation<GetPaymentURLResponse, GetPaymentURLRequest>({
      query: (body) => ({
        url: "/payment",
        method: "post",
        body,
      }),
      invalidatesTags: ["expire_times"],
    }),
    getExpireTime: build.query<GetExpireTimeResponse, void>({
      query: () => "/tenant/expire",
      providesTags: ["expire_times"],
    }),
    getPackages: build.query<GetPackageResponse, void>({
      query: () => "packages",
      providesTags: ["packages"],
    })
  }),
  overrideExisting: false,
});

export const { useGetPaymentUrlMutation, useGetExpireTimeQuery, useGetPackagesQuery } = extendServiceAPI;
