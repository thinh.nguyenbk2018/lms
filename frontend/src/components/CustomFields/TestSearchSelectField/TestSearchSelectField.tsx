import React, {CSSProperties} from 'react';
import {FieldProps} from "formik";
import {Typography} from "antd";
import TestSearchSelect, {TestSeachSelectProps} from '../../Courses/Forms/TestSeachSelect/TestSearchSelect';
import { FormItem } from 'formik-antd';


type SearchSelectFieldProps =
    FieldProps<any> & TestSeachSelectProps
    & { label?: string, style?: CSSProperties, required?: boolean, onSelect?: () => void, showErrorMessage?: boolean };

const TestSearchSelectField = ({
                                   field,
                                   form: {
                                       touched,
                                       errors,
                                       setFieldValue,
                                       setFieldError,
                                       handleBlur,
                                       handleChange,
                                       setTouched,
                                       setFieldTouched
                                   },
                                   ...otherProps
                               }: SearchSelectFieldProps) => {
    const {name, value, onChange, onBlur} = field;
    const errorMsg = touched[name] && errors[name];

    return (
        <>
            {/*<FormItem required={otherProps?.required} label={otherProps?.label || ""} name={name}>*/}
                <TestSearchSelect
                    {...field}
                    {...otherProps}
                    onSelect={(testId) => {
                        // console.log('testId', name, testId);
                        setFieldValue(name, testId, true);
                    }}
                    onChange={(value) => {
                        // console.log('on changed', name, value);
                        setFieldValue(name, value, true);
                    }}
                    width={'100%'}
                />
                <Typography.Text type={"danger"}>
                    <>
                        {errorMsg}
                    </>
                </Typography.Text>
            {/*</FormItem>*/}
        </>
    );
};

export default TestSearchSelectField;