import { faBan, faSkull, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ButtonProps, Button } from "antd";

const TestSubmissionButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			type={"primary"}
			icon={<FontAwesomeIcon icon={faSkull} style={{ margin: "0 .5em 0 0" }} />}
		>
			{`${props.title || "Nộp bài"}`}
		</Button>
	);
};

export default TestSubmissionButton;
