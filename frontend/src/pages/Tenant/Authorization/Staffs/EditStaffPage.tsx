import { Button, Space, Spin } from "antd";
import { useRef } from "react";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import StaffForm, { StaffFormHandle } from "../../../../components/Authorization/Staff/StaffForm";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import { STATUS_CODE_RESPONSE } from "../../../../constants/StatusCode";
import { useGetStaffDetailsQuery } from "../../../../redux/features/Staff/StaffAPI";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import { ROUTING_CONSTANTS } from "../../../../navigation/ROUTING_CONSTANTS";

const EditStaffPage = () => {
  let navigate = useNavigate();
  let { staffId } = useParams();
  let { data: getStaffDetailsResponse, isLoading, isError, error } = useGetStaffDetailsQuery(parseInt(staffId!));

  let staffFormRef = useRef<StaffFormHandle>(null)

  const onCancel = () => {
    navigate(ROUTING_CONSTANTS.STAFFS_MANAGEMENT);
  }

  const onDone = () => {
    staffFormRef.current?.updateStaff()
      .then((result: any) => {
        if (!result) return;
        if ("code" in result && result.code === "SUCCESS") {
          navigate(ROUTING_CONSTANTS.STAFFS_MANAGEMENT)
        }
      })
      .catch((error: any) => {
        if (error?.data?.message) {
          AntdNotifier.error(error.data.message);
          return;
        }
        AntdNotifier.error("Có lỗi xảy ra trong quá trình cập nhật dữ liệu, thử lại sau");
      })
  }

  if (isError && (error as any)?.data.code === STATUS_CODE_RESPONSE.UNAUTHORIZED) {
    return <Navigate to={ROUTING_CONSTANTS.UNAUTHORIZED} />
  }
  else
    return <>
      <ContentHeader
        title="Chi tiết nhân viên"
        action={
          <Space>
            <Button
              danger
              type="primary"
              color="#73d13d"
              onClick={onCancel}
            >
              Quay lại
            </Button>
            <Button
              type="primary"
              color="#73d13d"
              onClick={onDone}
            >
              Cập nhật
            </Button>
          </Space>
        }
      />
      {/* {isError ? JSON.stringify(getStaffDetailsResponse) : ""} */}
      <Spin spinning={isLoading}>
        <StaffForm staff={getStaffDetailsResponse?.staff} ref={staffFormRef} />
      </Spin>

    </>
}

export default EditStaffPage;