import React from "react";
import {Pagination} from "antd";
import ForumThreadItem from "./ForumThreadItem";
import "./ForumThreadList.less";
import {useParams} from "react-router-dom";
import useDebounced from "../../hooks/Debounced/useDebounced";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import useClassCourseInformation from "../../hooks/Routing/useClassCourseInformation";
import LoadingPage from "../../pages/UtilPages/LoadingPage";
import {
    classDiscussionActions,
    classDiscussionSelectors,
} from "@redux/features/Class/ClassDiscussion/ClassDiscussionSlice";
import {useGetAllPostForClassQuery} from "@redux/features/Class/ClassDiscussion/ClassDiscussionAPI";
import ApiErrorWithRetryButton from "../Util/ApiErrorWithRetryButton";
import API_ERRORS from "../../redux/features/ApiErrors";
import {errorArrayHandlingHelper} from "@redux/features/CentralAPI";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";

export const classDiscussionErrorsHanlder = (err: any) => {
    const {POST_NOT_FOUND, COMMENT_NOT_FOUND} = API_ERRORS.CLASS.DISCUSSION;
    errorArrayHandlingHelper(
        err,
        [POST_NOT_FOUND, "Không tìm thấy bài đăng"],
        [COMMENT_NOT_FOUND, "Không tìm thấy bình luận"]
    );
    return;
};

const threadList = [
    {
        id: 1,
        title: `Serious Title`,
        avatar: `https://joeschmoe.io/api/v1/random`,
        username: "Test User1",
        time: 1642403770,
        content: `'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',`,
        upVotesCount: 12,
        downVotesCount: 20,
        repliesCount: 33,
        personalPreference: "upvoted",
    },
    {
        id: 2,
        title: `Serious Title`,
        avatar: `https://joeschmoe.io/api/v1/random`,
        username: "Test User1",
        time: 1642403770,
        content: `'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',`,
        upVotesCount: 12,
        downVotesCount: 20,
        repliesCount: 33,
        personalPreference: "upvoted",
    },
    {
        id: 3,
        title: `Serious Title`,
        avatar: `https://joeschmoe.io/api/v1/random`,
        username: "Test User1",
        time: 1642403770,
        content: `'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',`,
        upVotesCount: 12,
        downVotesCount: 20,
        repliesCount: 33,
        personalPreference: "upvoted",
    },
];
const ForumThreadList = () => {
    const {classId: classIdFromPath} = useParams();
    // * Redux Toolkit Queries
    const {classId, classInfo, isSuccessClass, isErrorClass, isFetchingClass} =
        useClassCourseInformation(Number(classIdFromPath));

    // * Redux Slice
    // * Searching and pagination
    const dispatch = useAppDispatch();
    const searchCriteria = useAppSelector(
        classDiscussionSelectors.selectSearchCriteria
    );
    const changeSearchCriteriaAction = classDiscussionActions.changeCriteria;

    const debouncedCriteriaChange = useDebounced((criteria) =>
        dispatch(changeSearchCriteriaAction(criteria))
    );

    const {
        data: postList,
        isSuccess,
        isLoading,
        isError,
        error,
    } = useGetAllPostForClassQuery(
        {classId: Number(classId), searchCriteria: searchCriteria},
        {skip: isNaN(Number(classId))}
    );

    if (isSuccessClass && isSuccess) {
        const {listData: threadList} = postList;
        return (
            <>
                {threadList && threadList.length > 0 && (
                    // <Collapse
                    // 	defaultActiveKey={threadList[0].id}
                    // 	onChange={callback}
                    // 	className={"forum-collapse"}
                    // >
                    threadList.map((item) => (
                        <div style={{marginBottom: "1rem"}}>
                            <ForumThreadItem key={item.id} {...item}  />
                        </div>
                    ))
                    // </Collapse>
                )}
                {postList && postList.total > 0 && <div className={'flex'} style={{justifyContent: "flex-end", marginTop: "2rem"}}>
                    <Pagination
                        showSizeChanger
                        pageSize={searchCriteria?.pagination?.size || DEFAULT_PAGE_SIZE}
                        onChange={(page: any, size: any) => {
                            const payload = {
                                ...searchCriteria,
                                pagination: {page, size},
                            };
                            dispatch(changeSearchCriteriaAction(payload));
                        }}
                        onShowSizeChange={(current, newSize) => {
                            let newSearchCriteria = {
                                ...searchCriteria,
                                pagination: {
                                    size: newSize,
                                    page: 1,
                                },
                            };
                            dispatch(
                                changeSearchCriteriaAction(newSearchCriteria)
                            );
                            window.scrollTo(0, 0);
                        }}
                        current={postList?.page}
                        total={postList?.total}
                    />
                </div>}
                {(threadList && threadList.length == 0) && <span>Chưa có bài viết được thêm.</span>}
            </>
        );
    } else if (isLoading || isFetchingClass) {
        return <LoadingPage/>;
    } else {
        if (isError || isErrorClass || error) {
            classDiscussionErrorsHanlder(error);
        }
        return (
            <ApiErrorWithRetryButton
                onRetry={function (): void {
                    window.location.reload();
                }}
            />
        );
    }
};

export default ForumThreadList;
