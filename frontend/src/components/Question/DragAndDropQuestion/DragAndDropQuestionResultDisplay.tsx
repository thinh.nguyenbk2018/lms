import { Card, Divider, Input, Row, Space, Tooltip } from "antd";
import {
    DragAndDropBlankResult,
    FillBlankDragAndDropQuestionWithResult,
} from "../../../redux/features/Quiz/CreateQuizSlice";
import { DND_BLANK_SPAN_TAG_CLASS, replaceBlankWithSpecialSpan, } from "./QuestionStatementWithBlanks";

import ReactHtmlParser from "html-react-parser";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { green } from "@ant-design/colors";
import COLOR_SCHEME from "@constants/ThemeColor";
import React from "react";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

const BlankDragAndDropWithResult = (
    blankDragAndDropWithResult: DragAndDropBlankResult
) => {
    const {
        id,
        hint,
        answerKey,
        expectedAnswer,
        studentAnswer,
        studentAnswerKey,
        isCorrect,
    } = blankDragAndDropWithResult;
    return (
        <Tooltip title={`Gợi ý: ${blankDragAndDropWithResult.hint}`}>
            <Space>
                <Input
                    style={{ display: "inline", width: "clamp(4em,6em,10em)" }}
                    value={studentAnswer}
                />
                {isCorrect && (
                    <FontAwesomeIcon icon={faCheck} style={{ color: green[6] }} />
                )}
                {!isCorrect && (
                    <FontAwesomeIcon icon={faTimes} style={{ color: COLOR_SCHEME.error }} />
                )}
            </Space>
        </Tooltip>
    );
};

const replaceBlankWithInputAndHint = (
    questionHTML: string,
    blankList: DragAndDropBlankResult[]
) => {
    const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML);
    let blankCounter = 0;

    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if (
                (node as any).name === "span" &&
                (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
            ) {
                blankCounter++;
                const blankIndex = blankCounter - 1;
                return <BlankDragAndDropWithResult {...blankList[blankIndex]} />;
            }

            return node;
        },
    });
};
const DragAndDropQuestionResultDisplay = (
    props: FillBlankDragAndDropQuestionWithResult
) => {
    // * Question info to display the requirements
    const { description, resultAnswerList } = props;
    if (!description || !resultAnswerList) {
        return null;
    }

    const text = replaceBlankWithInputAndHint(description, resultAnswerList);
    return (
        <>
            <Card>
                <div className={"dnd-question-content-container"}>
                    {
                        JSON.parse(props?.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
                            <AttachmentRender attachment={a} />
                        </Row>)
                    }
                    <Divider />
                    <div className={"answer-list-container"}>
                        <Space>
                            {
                                props.answerList.map((item, index) =>
                                    <Input defaultValue={item.content} disabled={true}
                                        className={'fill-in-blank-drag-and-drop-answer-pool'} />)

                            }
                        </Space>
                    </div>
                    <Divider />
                    <div className={"dnd-question-content-container"}>{text}</div>
                </div>
            </Card>
        </>
    );
};

export default DragAndDropQuestionResultDisplay;
