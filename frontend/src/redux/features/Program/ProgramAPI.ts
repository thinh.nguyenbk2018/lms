import { PaginatedResponse } from "../../interfaces/types";
import { SearchCriteria } from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import { transformSearchQuery } from "../APIUtils/TransformParameter";
import { apiSlice } from "../CentralAPI";
import { ProgramDto, ProgramInList } from "./ProgramType";

export type ProgramListDto = PaginatedResponse<ProgramInList>;

const programApiInvalidatesTags = apiSlice.enhanceEndpoints({addTagTypes: ['Programs']});

const programAPI = programApiInvalidatesTags.injectEndpoints({
    endpoints: (build) => ({
        getProgramListPaginated: build.query<ProgramListDto, SearchCriteria>({
            query: (criteria) => `programs/?${transformSearchQuery(criteria)}`,
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.listData.map(({ id }) => ({ type: 'Programs', id } as const)),
                        { type: 'Programs', id: 'LIST' },
                    ]
                    : // an error occurred, but we still want to refetch this query when `{ type: 'Posts', id: 'LIST' }` is invalidated
                    [{ type: 'Programs', id: 'LIST' }],
        }),
        getProgram: build.query<ProgramDto, number>({
            query: (id) => `programs/${id}`,
            providesTags: (result) =>
                [{type: 'Programs', id: result?.id}]
        }),
        createProgram: build.mutation<ProgramDto, ProgramDto>({
            query (body) {
                return {
                    url: `programs`,
                    method: 'POST',
                    body,
                }
            },
            invalidatesTags: [{ type: 'Programs', id: 'LIST' }],
        }),
        updateProgram: build.mutation<ProgramDto, ProgramDto>({
            query (program) {
                return {
                    url: `programs/${program.id}`,
                    method: 'PUT',
                    body: program,
                }
            },
            invalidatesTags: (_result, _error, args) => [{ type: 'Programs', id: args.id }],
        }),
        deleteProgram: build.mutation<number, number>({
            query (id) {
                return {
                    url: `programs/${id}`,
                    method: 'DELETE',
                }
            },
            invalidatesTags: (_result, _error, args) => [{ type: 'Programs', id: args }],
        })
    }),
    overrideExisting: false
});

export const {useGetProgramListPaginatedQuery,
    useGetProgramQuery,
    useCreateProgramMutation,
    useUpdateProgramMutation,
    useDeleteProgramMutation
} = programAPI;