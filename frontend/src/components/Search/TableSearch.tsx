import {Button, Col, Row} from "antd";
import React from "react";
import {reloadIconSvg, searchIconSvg} from "../Icons/SvgIcons";
import './TableSearch.less';
import {Formik} from "formik";
import {Form, Input} from "formik-antd";
import {useAppDispatch} from "../../hooks/Redux/hooks";
import DEFAULT_PAGE_SIZE, {
    Filter,
    SearchCriteria
} from "../../redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import _ from "lodash";
import useDebounced from "../../hooks/Debounced/useDebounced";
import {ActionCreatorWithPayload} from "@reduxjs/toolkit";

interface SearchItem {
    title: string;
    key: string;
}

export interface TableSearchProps {
    searchFields: SearchItem[];
    defaultFilters: Filter[];
    searchCriteria: Partial<SearchCriteria>;
    changeCriteria: ActionCreatorWithPayload<Partial<SearchCriteria>, string>;
}

const TableSearch = ({searchFields, searchCriteria, changeCriteria, defaultFilters}: TableSearchProps) => {

    const dispatch = useAppDispatch();
    const debouncedCriteriaChange = useDebounced((criteria) => dispatch(changeCriteria(criteria)))

    return (
        <>
            <Formik initialValues={{keyword: ''}}
                    onSubmit={(values, actions) => {
                        // do nothing here because we already handle it in onChange event of input field
                    }}>
                {
                    ({isValid, isSubmitting, dirty, values, setFieldValue, submitForm}) =>
                        <Form>
                            <Row align={"middle"}>
                                {searchFields.map(field => <Col span={12}><Form.Item
                                    name={field.key}
                                    style={{
                                        margin: "0",
                                        padding: "0"
                                    }}
                                >
                                    <Input prefix={searchIconSvg} placeholder={'Nhập để tìm kiếm'} name={'keyword'}
                                           onChange={(e) => {
                                               const keyword = e.target.value;
                                               const criteria = _.cloneDeep(searchCriteria);
                                               criteria.keyword = keyword;
                                               // if user search, reset page to 1
                                               criteria.pagination = {
                                                   page: 1,
                                                   size: DEFAULT_PAGE_SIZE
                                               };
                                               debouncedCriteriaChange(criteria);
                                           }}/>
                                </Form.Item>
                                </Col>)}
                                <Col span={4} offset={1}>
                                    <Button
                                        className={'search-btn'}
                                        type="primary"
                                        icon={reloadIconSvg}
                                        danger
                                        onClick={() => {
                                            setFieldValue('keyword', '');
                                            const criteria = {
                                                keyword: '',
                                                sort: [],
                                                filter: defaultFilters,
                                                pagination: {
                                                    page: 1,
                                                    size: DEFAULT_PAGE_SIZE
                                                }
                                            };
                                            debouncedCriteriaChange(criteria);
                                        }}
                                        // disabled={!dirty || !isValid}
                                        loading={isSubmitting}
                                    >
                                        <span>Xóa bộ lọc</span>
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                }
            </Formik>
        </>
    );
}

export default TableSearch;