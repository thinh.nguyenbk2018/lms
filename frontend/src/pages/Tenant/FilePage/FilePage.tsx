import React from 'react';
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import FileManager from "../../../components/FileManager/FileManager";


const FilePage = () => {
  return <>
    <ContentHeader
      title="Quản lý file"
    />
    <FileManager />
  </>
}


export default FilePage;