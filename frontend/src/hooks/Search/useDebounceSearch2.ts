import AwesomeDebouncePromise from "awesome-debounce-promise";
import {useState} from "react";
import {useAsync} from "react-async-hook";
import useConstant from "use-constant";

const useDebounceSearch2 = <ResultType>(searchFunction: (textToSearch: string)=> {}, interval: number = 650) => {
    // * States to populate an input 
    const [searchText, setSearchText] = useState(''); 
    
    // * Create the debounced search only once
    const debouncedSearchFunction = useConstant(()=> AwesomeDebouncePromise(searchFunction, interval)); 
   
    // The async callback is run each time the text changes,
    // but as the search function is debounced, it does not
    // fire a new request on each keystroke
    const searchResults = useAsync(
        async () => {
        if (searchText.length === 0) {
            return [];
        } else {
            return debouncedSearchFunction(searchText);
        }
        },
        [debouncedSearchFunction, searchText]
    );
    
    return {
        searchText,
        setSearchText,
        searchResults,
    }
}

export default useDebounceSearch2; 