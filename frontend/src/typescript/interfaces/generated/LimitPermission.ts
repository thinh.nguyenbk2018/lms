interface LimitPermission {
  limitByBranch: boolean;
  limitByTeaching: boolean;
  limitByDean: boolean;
  limitByManager: boolean;
  limitByLearn: boolean;
}

export default LimitPermission;

export const DEFAULT_LIMIT_PERMISSION: LimitPermission = {
  limitByBranch: true,
  limitByTeaching: true,
  limitByDean: true,
  limitByManager: true,
  limitByLearn: true,
}