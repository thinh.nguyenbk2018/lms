import React from 'react';
import {Avatar, Col, Row, Space, Typography} from 'antd';
import './ChatItem.less';
import {useAppDispatch} from "../../../hooks/Redux/hooks";
import {showChat} from "../../../redux/features/Chat/ChatSlice";
import {BriefChatInfo} from '../../../redux/features/Chat/ChatAPI';

const ChatItem = ({props, callback, ...rest}: { props: BriefChatInfo, callback: () => void }) => {
    const dispatch = useAppDispatch();
    const onFocusChatItem = () => {
        dispatch(showChat(props.chatRoomId));
        if (callback) callback();
    };
    return (
        <div className={`chat-item ${props.numOfUnseenMessages > 0 ? "unchecked-chat" : "checked-chat"}`}
             onClick={onFocusChatItem}
             {...rest}
        >
            <Row>
                <Col span={4}>
                    <Avatar src={props.image || "https://joeschmoe.io/api/v1/random"} size={"large"}/>
                </Col>
                <Col className={"name-and-last-message"} span={16}>
                    <Space direction={"vertical"} style={{margin: `0 1em`}}>
                        <>
                            <Typography.Text className={"chat-group-name"}>
                                {props.chatRoomName}
                            </Typography.Text>
                            {props.lastMessage}
                        </>
                    </Space>
                </Col>
                <Col span={4}>
                    <div className={"chat-meta-container"}>
                        <Typography.Text>
                            {/*{timeStampHelper.getDateTimeFromTimeStamp(props.lastMessage.time) || "No last"}*/}
                            <>
                                {"No last"}
                            </>
                        </Typography.Text>
                        {props.numOfUnseenMessages > 0 && <Avatar size={"small"}
                                                                  style={{backgroundColor: "red"}}>{props.numOfUnseenMessages}</Avatar>}
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default ChatItem;
