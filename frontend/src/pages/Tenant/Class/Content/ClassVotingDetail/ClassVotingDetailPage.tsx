import {Button, Form, Input as OriginalInput, Modal, Progress, Row, Space, Tooltip, Typography,} from "antd";
import {Formik} from "formik";
import {FormItem, Input} from "formik-antd";
import {useNavigate, useParams} from "react-router-dom";
import "./ClassVotingDetailPage.less";
import * as Yup from "yup";
import React from "react";
import _ from "lodash";
import {blue} from "@ant-design/colors";
import {VotingModal} from "../ClassContentPage";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleXmark, faPenToSquare,} from "@fortawesome/free-solid-svg-icons";
import {
    useAddVotingOptionMutation,
    useChangeChoiceContentMutation,
    useDeleteVotingChoiceMutation,
    useGetClassInformationQuery,
    useGetClassVotingDetailQuery,
    useMakeVotingChoiceMutation,
    Voting,
} from "../../../../../redux/features/Class/ClassAPI";
import EditButton from "../../../../../components/ActionButton/EditButton";
import ContentHeader from "../../../../../components/ContentHeader/ContentHeader";
import {useAppDispatch, useAppSelector,} from "../../../../../hooks/Redux/hooks";
import useClassSidebar from "../../../../../hooks/Routing/useClassSidebar";
import {authSelectors} from "../../../../../redux/features/Auth/AuthSlice";
import {
    classContentActions,
    classContentSelectors,
} from "../../../../../redux/features/Class/ClassContent/ClassContentSlice";
import {DEFAULT_FRONTEND_ID} from "../../../../../redux/interfaces/types";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import {FORM_DEFAULT_MESSAGES} from "../../../../../utils/FormUtils/formUtils";
import LoadingPage from "../../../../UtilPages/LoadingPage";
import ApiErrorRetryPage from "../../../../UtilPages/ApiErrorRetryPage";
import API_ERRORS from "../../../../../redux/features/ApiErrors";
import {errorArrayHandlingHelper} from "../../../../../redux/features/CentralAPI";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import BackButton from "@components/ActionButton/BackButton";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";

export const votingErrorsHandler = (err: any) => {
    const {
        ADD_CHOICE_IS_NOT_ALLOWED,
        VOTING_NOT_FOUND,
        VOTING_CHOICE_NOT_FOUND,
        USER_IS_NOT_ALLOWED_TO_UPDATE_VOTING_CHOICE,
    } = API_ERRORS.VOTING;
    errorArrayHandlingHelper(
        err,
        [ADD_CHOICE_IS_NOT_ALLOWED, "Người dùng không đủ quyền để thêm lựa chọn"],
        [VOTING_NOT_FOUND, "Không tìm thấy cuộc bình chọn"],
        [VOTING_CHOICE_NOT_FOUND, "Không tìm thấy lựa chọn"],
        [
            USER_IS_NOT_ALLOWED_TO_UPDATE_VOTING_CHOICE,
            "Cuộc bình chọn không cho phép người dùng thay đổi các lựa chọn",
        ]
    );
    return;
};

const getMaxVoteCount: (vote: Voting | undefined) => number = (
    vote: Voting | undefined
) => {
    if (!vote) {
        return 0;
    }
    if (vote.choices.length === 0) {
        return 0;
    }
    const voteCounts = vote.choices.map((item) => item.numbersOfChosen);
    return Math.max(...voteCounts);
};

const getNumOfVotes: (vote: Voting | undefined) => number = (
    vote: Voting | undefined
) => {
    if (!vote) {
        return 0;
    }
    if (vote.choices.length === 0) {
        return 0;
    }
    return _.sumBy(vote.choices, (item) => item.numbersOfChosen);
};
const ClassVotingDetailPage = () => {
    const {votingItemId, chapterId, classId} = useParams();
    const [closeSidebar] = useClassSidebar();

    // * Redux query states
    const {
        data: classInfo,
        isSuccess: isSuccessClassInfo,
        isError: isErrorClassInfo,
        isFetching: isFetchingClassInfo,
    } = useGetClassInformationQuery(Number(classId));

    // * Voting detail to display
    const {
        data: votingInfo,
        isSuccess,
        isError,
        error,
        isFetching,
    } = useGetClassVotingDetailQuery(
        {
            classId: Number(classId),
            votingId: Number(votingItemId),
        },
        {
            skip:
                isNaN(Number(votingItemId)) ||
                isNaN(Number(classId)),
        }
    );

    const [makeVotingChoice] = useMakeVotingChoiceMutation();
    const [deleteVotingChoice] = useDeleteVotingChoiceMutation();
    const [updateVotingOptionContent] = useChangeChoiceContentMutation();
    //  * Redux store
    const dispatch = useAppDispatch();
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);
    const votingOptionInEdit = useAppSelector(
        classContentSelectors.selectVotingOptionInEdit
    );

    // * Router
    const navigate = useNavigate();
    const [addVotingOption] = useAddVotingOptionMutation();
    const [changeUserChoice] = useChangeChoiceContentMutation();

    // * Modal state
    const [showRequiredMessage, setShowRequiredMessage] = React.useState(false);
    const [visible, setVisible] = React.useState(false);
    const [newOption, setNewOption] = React.useState("");

    // * handling option content change
    const [optionInEdit, setOptionInEdit] = React.useState<number | undefined>(
        undefined
    );

    const [newOptionForm] = Form.useForm();

    const handleOk = () => {
        if (!newOption) {
            setShowRequiredMessage(true);
            return;
        }
        // TODO: CALL THE API TO ADD THE NEW OPTION
        AntdNotifier.handlePromise(
            addVotingOption({
                classId: Number(classId),
                votingId: Number(votingItemId),
                votingOption: {
                    content: newOption,
                },
            }).unwrap(),
            "Thêm lựa chọn",
            () => {
            },
            votingErrorsHandler
        );
        newOptionForm.setFieldsValue({newOption: ""});
        setVisible(false);
    };
    const handleCancel = () => {
        setVisible(false);
    };

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    if (isSuccessClassInfo && isSuccess) {
        const max = getMaxVoteCount(votingInfo);
        const total = getNumOfVotes(votingInfo);
        console.log("Voting meta: ", max, total);
        const {choices: optionList} = votingInfo;
        return (
            <>
                <ContentHeader
                    title={"Cuộc bình chọn"}
                    action={
                        <Space>
                            <BackButton
                                type="primary"
                                danger
                                onClick={() => {
                                    navigate(
                                        replacePathParams(ROUTING_CONSTANTS.CLASS_CONTENT, [":classId", classId || ""])
                                    );
                                }}
                                className={"back-btn"}
                            >
                                Quay lại
                            </BackButton>
                            <EditButton
                                style={{display: userInfo?.id != votingInfo?.createdBy?.id ? 'none' : ''}}
                                onClick={() => {
                                    dispatch(
                                        classContentActions.updatingActivity({
                                            chapterToEdit: Number(chapterId),
                                            activity: {
                                                ...votingInfo,
                                                type: "vote",
                                                order: -1,
                                                id: votingInfo.id || DEFAULT_FRONTEND_ID,
                                            },
                                        })
                                    );
                                }}
                            />
                        </Space>
                    }
                />
                <Row justify={"center"}>
                    <Space direction={"vertical"} align={"center"}>
                        <h2 style={{color: blue[5]}}>
                            {votingInfo?.title}
                        </h2>
                        <Typography.Text>{votingInfo?.description}</Typography.Text>
                    </Space>
                </Row>
                <Row justify={"center"}>
                    <div className={"voting-option-list-container"}>
                        {votingInfo?.choices.map((item) => {
                            return (
                                <div className={"voting-option-container"}>
                                    <div
                                        className={"voting-option-container__inner"}
                                        key={item.id}
                                        onClick={() => {
                                            if (!item.id) {
                                                return;
                                            }

                                            AntdNotifier.handlePromise(
                                                makeVotingChoice({
                                                    classId: Number(classId),
                                                    votingId: Number(votingItemId),
                                                    choiceId: item.id,
                                                }).unwrap(),
                                                "Tạo lựa chọn",
                                                () => {
                                                },
                                                votingErrorsHandler
                                            );
                                        }}
                                    >
                                        <div className="voting-content-and-bar-container">
                                            <Typography.Link>{item.content}</Typography.Link>{" "}
                                            <Tooltip title={item.numbersOfChosen}>
                                                <Progress
                                                    strokeWidth={20}
                                                    trailColor={blue[2]}
                                                    percent={Number(
                                                        ((item.numbersOfChosen / total) * 100).toFixed(2)
                                                    )}
                                                />
                                            </Tooltip>
                                        </div>

                                        {currentUser?.id === item.createdBy?.id && (
                                            <div className="action-container">
                                                <Tooltip title={"Chỉnh sửa"}>
                                                    <FontAwesomeIcon
                                                        className={"icon edit"}
                                                        icon={faPenToSquare}
                                                        onClick={(e) => {
                                                            e.stopPropagation();
                                                            dispatch(
                                                                classContentActions.setVotingOptionInEdit(item)
                                                            );
                                                        }}
                                                    />
                                                </Tooltip>
                                                <Tooltip title={"Xóa"}>
                                                    <FontAwesomeIcon
                                                        onClick={() => {
                                                            if (!item.id) {
                                                                AntdNotifier.error(
                                                                    "Không thể xóa bớt lựa chọn lúc này"
                                                                );
                                                                return;
                                                            }
                                                            AntdNotifier.handlePromise(
                                                                deleteVotingChoice({
                                                                    classId: Number(classId),
                                                                    votingId: Number(votingItemId),
                                                                    choiceId: item.id,
                                                                }).unwrap(),
                                                                "Xóa bỏ lựa chọn",
                                                                () => {
                                                                },
                                                                votingErrorsHandler
                                                            );
                                                        }}
                                                        className={"icon delete"}
                                                        icon={faCircleXmark}
                                                    />
                                                </Tooltip>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </Row>
                {(userInfo?.id != undefined && userInfo?.id == votingInfo?.createdBy?.id) || votingInfo?.isAllowedToAddChoice ? (
                    <Row justify={"center"} style={{margin: "2em 0"}}>
                        <Button type={"dashed"} onClick={() => setVisible(true)}>
                            {`Thêm lựa chọn mới`}
                        </Button>
                    </Row>
                ) : null}
                <Modal
                    title="Thêm lựa chọn mới"
                    visible={visible}
                    onOk={handleOk}
                    onCancel={handleCancel}
                    okText={'Thêm'}
                    cancelText={'Hủy'}
                >
                    <Form form={newOptionForm}>
                        <Form.Item name={"newOption"}>
                            <OriginalInput
                                allowClear
                                onChange={(event) => setNewOption(event.target.value)}
                            />
                        </Form.Item>
                    </Form>
                </Modal>
                <VotingModal/>
                {votingOptionInEdit && (
                    <Formik
                        enableReinitialize={true}
                        initialValues={votingOptionInEdit || {}}
                        validationSchema={Yup.object({
                            content: Yup.string().required(FORM_DEFAULT_MESSAGES.REQUIRED),
                        })}
                        onSubmit={(values, {resetForm}) => {
                            if (!votingOptionInEdit.id) {
                                AntdNotifier.error("Không thể thay đổi nội dung lụa chọn này");
                                return;
                            }
                            AntdNotifier.handlePromise(
                                updateVotingOptionContent({
                                    classId: Number(classId),
                                    votingId: Number(votingItemId),
                                    choiceId: votingOptionInEdit.id,
                                    votingInfo: {...votingOptionInEdit, ...values},
                                }).unwrap(),
                                "Thay đổi nội dung lựa chọn",
                                () => {
                                    resetForm({})
                                },
                                votingErrorsHandler
                            );
                        }}
                    >
                        {({values, handleSubmit, resetForm, setFieldValue}) => {
                            return (
                                <Modal
                                    visible={!!votingOptionInEdit}
                                    onOk={() => {
                                        handleSubmit();
                                        dispatch(
                                            classContentActions.setVotingOptionInEdit(undefined)
                                        );
                                    }}
                                    onCancel={() =>
                                        dispatch(
                                            classContentActions.setVotingOptionInEdit(undefined)
                                        )
                                    }
                                >
                                    <Form>
                                        <FormItem name={"content"}>
                                            <Input name={"content"}/>
                                        </FormItem>
                                    </Form>
                                </Modal>
                            );
                        }}
                    </Formik>
                )}{" "}
            </>
        );
    } else if (isError && error) {
        votingErrorsHandler(error);
        return <ApiErrorRetryPage/>;
    } else if (isFetchingClassInfo || isFetching) {
        return <LoadingPage/>;
    } else {
        return <></>;
    }
};

export default ClassVotingDetailPage;
