import { Button, Space, Spin } from "antd";
import React from "react";
import { useEffect, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import GradeFormula, { GradeFormulaHandler } from "../../../components/GradeFormula/GradeFormula";
import GradeFormulaGenerator from "../../../components/GradeFormula/GradeFormulaGenerator/GradeFormulaGenerator";
import TagsSelector from "../../../components/GradeFormula/TagsSelector/TagsSelector";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { ScopeTag, useCreateGradeFormulaMutation, useGetAllTagsInScopeQuery } from "../../../redux/features/Grade/GradeApi";
import { gradeFormulaSeletor, resetGradeFormula } from "../../../redux/features/Grade/GradeFormulaSlice";
import { generateGradeManagementUrl } from "../../../redux/utils/createSliceWithSearch/generateGradeManagementUrl";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";



const CreateGradeFormulaPage = ({ scope }: { scope: ScopeTag }) => {
  let navigate = useNavigate();
  let dispatch = useAppDispatch();

  const { classId, courseId } = useParams();

  const [scopeId, setScopeId] = useState(0);

  const gradeFormulaRef = useRef<GradeFormulaHandler>(null);

  useEffect(() => {
    setScopeId(classId ? parseInt(classId) : parseInt(courseId!));
  }, [classId, courseId])

  useEffect(() => {
    return () => {
      dispatch(resetGradeFormula())
    }
  }, [dispatch])

  let tagTitle = useAppSelector(gradeFormulaSeletor.titleSelector);

  let [currentStep, setCurrentStep] = useState<number>(0);

  let [createGradeFormula, { isLoading: isCreating }] = useCreateGradeFormulaMutation();

  let { data: getAllTagsResponse, isLoading: isGettingTags } = useGetAllTagsInScopeQuery({ scope, scopeId })

  const onDone = () => {
    let formulaResult = gradeFormulaRef.current?.getFormula();
    if (formulaResult) {

      createGradeFormula({
        tagTitle: tagTitle,
        formula: formulaResult.formula,
        expression: formulaResult.expression,
        scope,
        scopeId,
        useTags: formulaResult.useTags,
        isPublic: formulaResult.isPublic
      })
        .unwrap()
        .then(response => {
          console.log(response);
          AntdNotifier.success("Tạo cột điểm mới thành công");
          navigate(generateGradeManagementUrl(scope, scopeId));
        })
        .catch(error => {
          AntdNotifier.error(error.data.message);
        })
    }
  }

  const steps = [
    {
      title: 'Chọn các cột điểm',
      content: <TagsSelector allTags={getAllTagsResponse?.tags!} isGettingTags={isGettingTags} />,
    },
    {
      title: "Viết công thức",
      content: <GradeFormulaGenerator />
    }
  ];

  return <>
    <ContentHeader
      title="Tạo công thức tính điểm"
      action={
        <Space>
          {currentStep < steps.length - 1 &&
            <Button
              type="dashed"
              onClick={() => setCurrentStep(currentStep + 1)}
            >
              Viết công thức
            </Button>
          }
          {currentStep > 0 &&
            <Button
              type="dashed"
              onClick={() => setCurrentStep(currentStep - 1)}
            >
              Chọn cột điểm
            </Button>
          }
          <Link to={generateGradeManagementUrl(scope, scopeId)}>
            <Button
              danger
              type="primary"
              color="#73d13d"
            >
              Hủy
            </Button>
          </Link>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onDone}
          >
            Xong
          </Button>
        </Space>
      }
    />
    <Spin spinning={isCreating}>
      <GradeFormula ref={gradeFormulaRef} allTags={getAllTagsResponse?.tags!} isLoading={isGettingTags} currentStep={currentStep} setCurrentStep={setCurrentStep} />
    </Spin>
  </>
}

export default CreateGradeFormulaPage;