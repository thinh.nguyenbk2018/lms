import { Select, Space, Typography } from "antd";
import { FieldProps } from "formik";
import { FormItem } from "formik-antd";
import { CSSProperties } from "react";
import { useGetClassUnitsQuery } from "../../../redux/features/Class/ClassTimeTable/ClassTimeTableAPI";
import { UnitItem } from "../../../typescript/interfaces/courses/CourseTypes";
import { useParams } from "react-router-dom";
import LoadingPage from "../../../pages/UtilPages/LoadingPage";

type SearchSelectFieldProps = FieldProps<any> & {
	label?: string;
	style?: CSSProperties;
	required?: boolean;
	onSelect?: () => void;
	showErrorMessage?: boolean;
};

const UnitSearchSelectField = ({
	field,
	form: {
		touched,
		errors,
		setFieldValue,
		setFieldError,
		handleBlur,
		handleChange,
		setTouched,
		setFieldTouched,
	},
	...otherProps
}: SearchSelectFieldProps) => {
	const { classId } = useParams();
	const {
		data: listUnit,
		isError,
		isSuccess,
		isFetching,
	} = useGetClassUnitsQuery(
		{ classId: Number(classId) },
		{
			skip: isNaN(Number(classId)),
		}
	);
	const { name, value, onChange, onBlur } = field;
	const errorMsg = touched[name] && errors[name];

	if (isFetching) {
		return (
			<>
				<LoadingPage />
			</>
		);
	} else if (isSuccess && listUnit) {
		return (
			// <Space>
				<Select
					{...field}
					onChange={(unitId) => {
						// console.log("SELECTING UNIT: ", unitId);
						setFieldValue(name, unitId);
						setFieldTouched(name, true);
					}}
                    style={{minWidth: "100px"}}
					allowClear={true}
				>
					{listUnit.map((item, index) => {
						return (
							<Select.Option value={item.id} key={item.id}>
								{item.title}
							</Select.Option>
						);
					})}
				</Select>
			// </Space>
		);
	} else {
		return <> </>;
	}
};

export default UnitSearchSelectField;
