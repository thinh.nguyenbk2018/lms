import _ from "lodash";
import {Checkbox, Form, Input, Select} from "formik-antd";
import ContentHeader from "../ContentHeader/ContentHeader";
import {Button, Col, Row, Space, Spin} from "antd";
import {Field, Formik} from "formik";
import React, {useEffect} from "react";
import * as Yup from "yup";
import {useNavigate, useParams} from "react-router-dom";
import TextEditorField from "../CustomFields/TextEditorField/TextEditorField";
import "./CreateAnnouncement.less";
import {useAppDispatch, useAppSelector} from "../../hooks/Redux/hooks";
import {
	changeCriteria as changeClassMemberSearchCriteria,
	classMemberSelector,
} from "../../redux/features/Class/ClassMemberSlice";
import {useGetClassMemberListPaginatedQuery} from "../../redux/features/Class/ClassMemberAPI";
import useDebounced from "../../hooks/Debounced/useDebounced";
import {notificationInitial} from "@redux/features/Announcement/AnnouncementType";
import {authSelectors} from "../../redux/features/Auth/AuthSlice";
import {useSendNotificationMutation} from "@redux/features/Announcement/AnnouncementAPI";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {validateTextEditor} from "../CustomFields/TextEditorField/ValidateTextEditor";
import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";
import {DEFAULT_FRONTEND_ID} from "../../redux/interfaces/types";
import BackButton from "../ActionButton/BackButton";
import DoneButton from "../ActionButton/DoneButton";
import {DEFAULT_TEXT_EDITOR_HTML} from "../Question/DragAndDropQuestion/DragAndDropQuestionDrawer";
import GeneralFileUploadField from "@components/CustomFields/FileUploadField/GeneralFileUploadField";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCirclePlus} from "@fortawesome/free-solid-svg-icons";

const ClassNotificationSchema = Yup.object().shape({
	subject: Yup.string().required("Tiêu đề là trường bắt buộc"),
	content: Yup.string().test(
		"content",
		"Nội dung là trường bắt buộc",
		(text) => {
			return validateTextEditor(text);
		}
	),
	receiversId: Yup.array().test(
		"receiversId",
		"Người nhận là trường bắt buộc",
		(ids) => {
			if (!ids || ids.length === 0) return false;
			return true;
		}
	),
});

const CreateOrUpdateNotification = () => {
	const { id, classId } = useParams();
	const currentUser = useAppSelector(authSelectors.selectCurrentUser);
	const [sendNotification] = useSendNotificationMutation();

	const classMemberSearchCriteria = useAppSelector(
		classMemberSelector.selectSearchCriteria
	);
	const dispatch = useAppDispatch();
	const navigate = useNavigate();

	const isSkip = (idParam: string | number | undefined) => {
		return Number(idParam) === -1 || Number.isNaN(Number(idParam));
	};

	const debouncedClassMemberSearchCriteriaChange = useDebounced((criteria) =>
		dispatch(changeClassMemberSearchCriteria(criteria))
	);

	useEffect(() => {
		const criteria = _.cloneDeep(classMemberSearchCriteria);
		criteria.keyword = "";
		debouncedClassMemberSearchCriteriaChange(criteria);
	}, []);

	const handleMemberSearch = (keyword: any) => {
		const criteria = _.cloneDeep(classMemberSearchCriteria);
		criteria.keyword = keyword;
		debouncedClassMemberSearchCriteriaChange(criteria);
	};

	const {
		data: classMembers,
		isFetching: isClassMembersFetching,
	} = useGetClassMemberListPaginatedQuery(
		{
			...classMemberSearchCriteria,
			classId: Number(classId) || DEFAULT_FRONTEND_ID,
		},
		{ skip: isSkip(classId) }
	);

	const getTitleOfPage = () => {
		return id ? "Chỉnh sửa thông báo" : "Tạo thông báo";
	};

	const getTitleOfButton = () => {
		return id ? "Cập nhật" : "Tạo mới";
	};

	return (
		<>
			<Formik
				enableReinitialize={true}
				initialValues={notificationInitial}
				validateOnChange={false}
				onSubmit={(values, actions) => {
					// console.log("values from formik: ", values);
					const notificationBody = {
						...values,
						senderId: currentUser?.id || 0,
					};
					if (notificationBody.receiversId.includes(-1)) {
						notificationBody.receiversId = [];
					}
					sendNotification({ notificationBody, classId: Number(classId) })
						.unwrap()
						.then(() => {
							antdNotifier.success("Gửi thông báo thành công", "", 1);
							navigate(
								ROUTING_CONSTANTS.CLASS_ANNOUNCEMENT.replace(
									":classId",
									classId?.toString() || ""
								)
							);
							actions.setSubmitting(false);
						})
						.catch((err) => {
							console.log("err", err);
							antdNotifier.error("Gửi thông báo thất bại", "Thông báo", 1);
							actions.setSubmitting(false);
						});
				}}
				validationSchema={ClassNotificationSchema}
			>
				{({
					isValid,
					isSubmitting,
					dirty,
					values,
					setFieldValue,
					handleSubmit,
					setSubmitting
				}) => (
					<>
						<Form layout={"vertical"}>
							<ContentHeader
								title={getTitleOfPage()}
								action={
									<Space>
										<BackButton
											type="primary"
											danger
											onClick={() => {
												navigate(
													ROUTING_CONSTANTS.CLASS_ANNOUNCEMENT.replace(
														":classId",
														classId ? classId : "-1"
													)
												);
											}}
											className={"back-btn"}
										>
											<span>Quay lại</span>
										</BackButton>
										<Button title={"Tạo thông báo"}
												type="primary"
												color="#73d13d"
												onClick={() => {
													handleSubmit();
												}}
												loading={isSubmitting}
												disabled={
													!dirty
													|| isSubmitting
												}
												icon={
													<FontAwesomeIcon style={{ margin: "0 .5em 0 0" }} icon={faCirclePlus} />
												}
										>
											Tạo thông báo
										</Button>
										{/*<DoneButton*/}
										{/*	title={"Tạo thông báo"}*/}
										{/*	type="primary"*/}
										{/*	color="#73d13d"*/}
										{/*	onClick={() => {*/}
										{/*		handleSubmit();*/}
										{/*	}}*/}
										{/*	className={"update-btn"}*/}
										{/*	disabled={*/}
										{/*		isNaN(Number(classId)) ||*/}
										{/*		!currentUser?.id ||*/}
										{/*		!dirty ||*/}
										{/*		!values.content ||*/}
										{/*		values.content === DEFAULT_TEXT_EDITOR_HTML*/}
										{/*		|| isSubmitting*/}
										{/*	}*/}
										{/*	loading={isSubmitting}*/}
										{/*>*/}
										{/*	<span>{getTitleOfButton()}</span>*/}
										{/*</DoneButton>*/}
									</Space>
								}
							/>

							<Row>
								<Col span={14}>
									<Form.Item label="Tiêu đề" name="subject" required={true}>
										<Input name={"subject"} />
									</Form.Item>
								</Col>
								<Col span={9} offset={1}>
									<Form.Item label="Tag (cách nhau bởi dấu phẩy)" name="tags">
										<Input name={"tags"} />
									</Form.Item>
								</Col>
							</Row>
							<Row className={"row-container"}>
								<Field
									name={"content"}
									placeholder={"Nhập nội dung"}
									component={TextEditorField}
									label={"Nội dung"}
									required={true}
								/>
							</Row>
							<Row>
								<Col span={24}>
									<Form.Item
										className={"receiversId"}
										name="receiversId"
										label="Người nhận"
										required={true}
									>
										<Select
											name={"receiversId"}
											mode={"multiple"}
											onSearch={handleMemberSearch}
											showSearch
											filterOption={false}
											notFoundContent={
												isClassMembersFetching ? (
													<Spin size="small" />
												) : (
													"Không tìm thấy người dùng"
												)
											}
											allowClear={true}
										>
											<Select.Option value={-1}>
												Tất cả thành viên
											</Select.Option>
											{isClassMembersFetching
												? []
												: classMembers?.listData.map((user) => {
														return (
															<Select.Option
																value={user.userId}
															>{`${user.lastName} ${user.firstName}`}</Select.Option>
														);
												  })}
										</Select>
									</Form.Item>
								</Col>
							</Row>
							<Row>
								<Col span={24}>
									<Form.Item
										name={"sendMailAsCopy"}
										label={"Gửi kèm mail như một bản sao"}
										className={"send-mail-as-copy-container"}
									>
										<Checkbox name={"sendMailAsCopy"} />
									</Form.Item>
								</Col>
							</Row>
							<Row>
								<Col span={24}>
									{/*<Form.Item label="Đính kèm tệp" name="attachment">*/}
									{/*	<LocalImageChooserWithFirebase*/}
									{/*		callback={(url: string) =>*/}
									{/*			setFieldValue("attachment", url)*/}
									{/*		}*/}
									{/*	/>*/}
									{/*</Form.Item>*/}
									<Form.Item name={"attachment"} label={"Đính kèm tệp"}>
										{/* TODO: add prefix so the files will be saved on firebase as: /textbooks/... */}
										<Field
											name={"attachment"}
											placeholder={"Type article content"}
											component={GeneralFileUploadField}
											showImagePreviewInsteadOfIcon={false}
											showPreview={true}
											displayedLayout={'row'}
										/>
										{/* <Input name={ATTACHMENT_FIELD}/> */}
									</Form.Item>
								</Col>
							</Row>
						</Form>
					</>
				)}
			</Formik>
		</>
	);
};

export default CreateOrUpdateNotification;
