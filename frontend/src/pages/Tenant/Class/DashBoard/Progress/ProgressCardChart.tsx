import React from 'react';
import {Card, Typography} from "antd";

const ProgressCardChart = () => {
    return (
        <div className={"progress-chart-container"}>
            <Card bodyStyle={{minHeight: "300px", display:"flex", alignItems:"center", justifyContent:"center"}}>
                <Typography.Title>
                    CARD CHART
                </Typography.Title>
            </Card>
        </div>
    );
};

export default ProgressCardChart;