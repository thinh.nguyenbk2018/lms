import {TextbookUnit, UnitGroupByTextbook, UnitItem} from "@typescript/interfaces/courses/CourseTypes";
import {useNavigate, useParams} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {Card, Col, Collapse, Row, Space, Switch, Tooltip, Typography} from "antd";
import {UnitIcon} from "@components/TeachingContent/NewActivityMenu";
import COLOR_SCHEME from "@constants/ThemeColor";
import {testSvg} from "@components/Icons/SvgIcons";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {DeleteOutlined} from "@ant-design/icons";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import React, {useEffect, useState} from "react";
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import CustomEmpty from "@components/Util/CustomEmpty";
import LoadingPage from "@pages/UtilPages/LoadingPage";
import ApiErrorWithRetryButton from "@components/Util/ApiErrorWithRetryButton";
import useCourseSidebar from "@hooks/Routing/useCourseSidebar";
import './ClassUnitListPage.less';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faEye, faPenToSquare} from "@fortawesome/free-solid-svg-icons";
import {blue} from "@ant-design/colors";
import {RenderActivityIcon} from "@pages/Tenant/Courses/CourseContent/CourseContentPage";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {courseContentActions} from "@redux/features/Course/CourseContentSlice";
import {
    useDeleteClassUnitMutation,
    useGetClassUnitItemsQuery,
    useLazyGetUnitClassGroupByTextbooksQuery
} from "@redux/features/Class/ClassAPI";
import {classContentActions} from "@redux/features/Class/ClassContent/ClassContentSlice";
import {AddUnitItemModal} from "@pages/Tenant/Class/Content/ClassContentPage";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";

const {Panel} = Collapse;

const UnitItemCard = (props: UnitItem & { accountType: string }) => {
    const {courseId, classId} = useParams();
    const {title, id, textbooks, isFromCourse} = props;

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const [deleteUnit] = useDeleteClassUnitMutation();

    const { classInfo } = useClassCourseInformation(Number(classId));

    const navigateToUnitDetail = () => {
        navigate(
            replacePathParams(
                ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE,
                [":classId", Number(classId)],
                [":unitItemId", id]
            )
        );
    };

    return (
        <Card
            className="class-quiz-card-item"
            title={
                <Space align="center">
                    <UnitIcon/>
                    <p style={{margin: 0, padding: 0}}>
                        {title || "Không có thông tin"}
                    </p>
                </Space>
            }
            extra={
                <Space>
                    <div className="action-container">
                        <Tooltip title={"Chi tiết"}>
                            <Typography.Link
                                className={'flex'}
                                style={{marginRight: '0.25rem', color: COLOR_SCHEME.primary}}
                                onClick={() => {
                                    navigateToUnitDetail();
                                }}
                            >
                                {testSvg}
                            </Typography.Link>
                        </Tooltip>
                        {classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.UPDATE_UNIT_CLASS]}>
                            {!isFromCourse && <Tooltip title={"Chỉnh sửa"}>
                                <FontAwesomeIcon
                                    className={"icon edit action-icon-activity"}
                                    icon={faPenToSquare}
                                    onClick={() => {
                                        dispatch(
                                            classContentActions.updatingActivity(
                                                {
                                                    chapterToEdit: -1,
                                                    activity: {...props, type: "unit"},
                                                }
                                            )
                                        );
                                    }}
                                />
                            </Tooltip>}
                        </PermissionRenderer>}
                        {classInfo?.status != 'ENDED' && !isFromCourse && <PermissionRenderer permissions={[PermissionEnum.DELETE_QUIZ_CLASS]}>
                            <DeleteOutlined
                                className="delete"
                                onClick={() => {
                                    confirmationModal("bài học", title, () => {
                                        AntdNotifier.handlePromise(
                                            deleteUnit({
                                                classId:
                                                    Number(classId),
                                                unitId: id,
                                            }).unwrap(),
                                            "Xóa bài học "
                                        );
                                    });
                                }}
                            />
                        </PermissionRenderer>}
                    </div>
                </Space>
            }
        >
            {(textbooks == undefined || textbooks.length == 0) ? 'Bài học không có giáo trình' : <Row>
                <Col className={'course-unit-page-label course-unit-page-label-left'} span={5}>Giáo trình</Col>
                <Col className={'course-unit-page-label'} span={19}>Ghi chú</Col>
                {textbooks.map((textbook: TextbookUnit) => {
                    return (
                        <>
                            <Col className={'course-unit-page-value-left'} span={5}>
                                <Typography.Link
                                    onClick={() => navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_TEXTBOOK_DETAIL, [":classId", classId || ""], [":textbookId", textbook.textbookId || ""]))}
                                >
                                    {textbook?.name}
                                </Typography.Link>
                            </Col>
                            <Col span={19}>{textbook?.note}</Col>
                        </>
                    )
                })}
            </Row>}
            <AddUnitItemModal/>
        </Card>
    );
};

const ClassUnitListPage = () => {
    const {
        courseId,
        classId
    } = useParams();

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const {} = useCourseSidebar();

    const { classInfo } = useClassCourseInformation(Number(classId));

    const currentUserAccountType = useAppSelector(authSelectors.selectCurrentUserAccountType);

    const [textbookViewMode, setTextbookViewMode] = useState(false);

    const [fetchUnitClassGroupByTextbook, {
        data: unitGroupByTextbook,
        isLoading: unitGroupByTextbookLoading,
        isSuccess: unitGroupByTextbookSuccess,
    }] = useLazyGetUnitClassGroupByTextbooksQuery();

    const {
        data: units,
        isSuccess,
        isFetching,
        isError,
        error,
        refetch: refetchUnitList
    } = useGetClassUnitItemsQuery({classId: Number(classId)}, {skip: isNaN(Number(classId))});
    const [closeSidebar] = useClassSidebar();

    const [deleteUnit] = useDeleteClassUnitMutation();

    useEffect(() => {
        if (textbookViewMode && classId) {
            fetchUnitClassGroupByTextbook({classId: Number(classId)});
        } else if (!textbookViewMode) {
            refetchUnitList();
        }
    }, [textbookViewMode]);

    return (
        <div>
            <ContentHeader
                title={`Bài học`}
                action={<>
                    <Switch
                        onClick={(checked: boolean) => setTextbookViewMode(checked)}
                        style={{marginRight: "1rem"}}
                        checked={textbookViewMode}
                    />
                    <span>Xem theo giáo trình</span>
                </>}
            />
            {
                !textbookViewMode && units?.length === 0 && <CustomEmpty description="Chưa có bài học nào."/>
            }
            {!textbookViewMode && isSuccess && (
                <>
                    {units.map((item: UnitItem, index: number) => {
                        return <UnitItemCard {...item} accountType={currentUserAccountType || ''} key={item.id}/>;
                    })}
                </>
            )}

            {textbookViewMode && unitGroupByTextbookSuccess && <div>
                {unitGroupByTextbook?.map((textbook: UnitGroupByTextbook, index: number) => {
                    return <Collapse className={'textbook-wrapper'}>
                        <Panel
                            header={<>
                                <FontAwesomeIcon
                                    icon={faBook}
                                    style={{margin: "0 .5em 0 0", color: blue.primary}}
                                />
                                <p className={"chapter-title"}>
                                    {textbook.name}
                                </p></>
                            }
                            key={textbook.textbookId}
                        >
                            <div className="activity-list-container">
                                {textbook.units.map((unit: UnitItem, idx: number) => {
                                    return <div
                                        className={"activity-item-container"}
                                    >
                                        <Space>
                                            <RenderActivityIcon
                                                type={"unit"}
                                            />
                                            <p className={"activity-title"}>
                                                {unit.title}
                                            </p>
                                        </Space>
                                        <div className={"action-container"}>
                                            <FontAwesomeIcon
                                                onClick={() => {
                                                    navigate(
                                                        replacePathParams(
                                                            ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE,
                                                            [
                                                                ":classId",
                                                                Number(classId),
                                                            ],
                                                            [
                                                                ":unitItemId",
                                                                unit.id,
                                                            ]
                                                        )
                                                    );
                                                }}
                                                className={"icon detail action-icon-activity"}
                                                icon={faEye}
                                            />
                                            {classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.UPDATE_UNIT_CLASS]}>
                                                {!unit.isFromCourse && <Tooltip
                                                    title={"Chỉnh sửa"}>
                                                    <FontAwesomeIcon
                                                        className={"icon edit action-icon-activity"}
                                                        icon={faPenToSquare}
                                                        onClick={() => {
                                                            dispatch(
                                                                courseContentActions.updatingActivity(
                                                                    {
                                                                        chapterToEdit: -1,
                                                                        activity: {...unit, type: "unit"},
                                                                    }
                                                                )
                                                            );
                                                        }}
                                                    />
                                                </Tooltip>}
                                            </PermissionRenderer>}
                                            {classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.DELETE_UNIT_CLASS]}>
                                                <DeleteOutlined
                                                    onClick={() => {
                                                        confirmationModal("bài học", unit.title, () => {
                                                            AntdNotifier.handlePromise(
                                                                deleteUnit({
                                                                    classId: Number(classId),
                                                                    unitId: unit.id,
                                                                }).unwrap(),
                                                                `Xóa bài học ${unit.title}`,
                                                                () => fetchUnitClassGroupByTextbook({classId: Number(classId)})
                                                            );
                                                        });
                                                    }}
                                                    className={"icon delete action-icon-activity"}
                                                />
                                            </PermissionRenderer>}
                                        </div>
                                    </div>
                                })}
                                {textbook.units.length == 0 ? <div style={{padding: '1rem'}}>Chưa có bài học nào sử dụng giáo trình này</div> : ''}
                            </div>
                        </Panel>
                    </Collapse>
                })}
                <AddUnitItemModal/>
            </div>}


            {(isFetching || unitGroupByTextbookLoading) && <LoadingPage/>}
            {isError && (
                <Row align={"middle"} justify={"center"}>
                    <ApiErrorWithRetryButton onRetry={() => {
                    }}/>;
                </Row>
            )}
        </div>
    );
}

export default ClassUnitListPage;