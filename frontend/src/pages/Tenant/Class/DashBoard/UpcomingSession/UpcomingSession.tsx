import React from "react";
import {Card, Typography} from "antd";

import "./UpcomingSession.less";
import {
    ClassSessionResponse,
    useGetUpComingClassSessionsQuery,
} from "../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableAPI";
import {useNavigate, useParams} from "react-router-dom";
import LoadingPage from "../../../../UtilPages/LoadingPage";
import {formatRelative} from "date-fns";
import {parseDate} from "../../../../../components/Calendar/Utils/CalendarUtils";
import {vi} from "date-fns/locale";
import {blue} from "@ant-design/colors";
import timeStampHelper, {getHourMinute} from "@utils/TimeStampHelper/TimeStampHelper";
import {UnitIcon} from "@components/TeachingContent/NewActivityMenu";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStickyNote} from "@fortawesome/free-solid-svg-icons";

const SessionBriefInfoCard = React.memo((props: Partial<ClassSessionResponse>) => {
    const {id, teacher, unit, startedAt, finishedAt, room, note} = props;

    const {classId} = useParams();

    const navigate = useNavigate();

    if (!startedAt) {
        return <Card>Đã có lỗi xảy ra khi load thông tin buổi học này</Card>;
    }
    return (
        <>
            <Card
                title={
                    timeStampHelper.formatDateTimeByDash(startedAt) + " : " + getHourMinute(startedAt) + " - " + getHourMinute(finishedAt)
                }
                headStyle={{backgroundColor: "#f8f9fa", color: blue[5]}}
                style={{margin: ".5em .3em"}}
            >
                <div className={"up-coming-unit-wrapper"}>
                    <UnitIcon/>
                    {unit?.title
                        ? (<Typography.Link onClick={() => {
                            navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE, [":classId", classId || ""], [":unitItemId", unit?.id || ""]));
                        }}>
                            <p className="session-unit-name">{unit?.title}</p>
                        </Typography.Link>)
                        : (<Typography.Text>Chưa có bài học được thêm.</Typography.Text>)
                    }
                </div>
                <div className={"up-coming-note-wrapper"}>
                    <div className={'icon-wrapper'}>
                        <FontAwesomeIcon icon={faStickyNote} className={"clock-icon"}/>
                    </div>
                    <div className="session-duration-container">
                        <p className="session-note">
                            {note ? note : 'Không có ghi chú.'}
                        </p>
                    </div>
                </div>
            </Card>
        </>
    );
});

const UpcomingSession = () => {
    const {classId} = useParams();

    const shouldSkip = () => {
        return isNaN(Number(classId));
    };

    const {
        data: upComingClassSessions,
        isSuccess: classSessionSuccess,
        isFetching: classSessionFetching,
    } = useGetUpComingClassSessionsQuery(
        {
            classId: Number(classId),
            numsOfPrevious: 0,
            numsOfAfter: 2
        },
        {skip: shouldSkip()}
    );

    const renderFunction = () => {
        if (classSessionFetching) {
            return <LoadingPage/>;
        } else if (classSessionSuccess) {
            // TODO: Might need to wrap the session cards inside container to correctly align them
            return (
                <div style={{display: "flex", gap: "0.8em", flexDirection: "column"}}>
                    {upComingClassSessions.sessions.length > 0 ? (
                        upComingClassSessions.sessions.map((item: ClassSessionResponse, index: number) => {
                            return <SessionBriefInfoCard key={item.id} {...item} />;
                        })
                    ) : (
                        <p style={{color: blue[5]}}>
                            {"Sắp tới không có một buổi học nào cả."}
                        </p>
                    )}
                </div>
            );
        } else return <></>;
    };

    return (
        <>
            <div className={`class-dash-board-upcoming-sessions`}>
                <p className="class-dash-board-upcoming-sessions__title">
                    Các buổi học sắp tới
                </p>

                {renderFunction()}
            </div>
        </>
    );
};

export default UpcomingSession;

