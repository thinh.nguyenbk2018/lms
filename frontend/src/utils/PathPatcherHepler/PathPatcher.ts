/** [param,value]: Replace param with value
 *  example: [":classId", 1]
 * */
export type PathTuple = [string,string | number];

type PathPatcherFunction = (path: string,...pathTuples: Array<PathTuple>) => string;
export const replacePathParams: PathPatcherFunction = (path, ...pathTuples) => {
    let resolutedString = path;
    for (let tuple of pathTuples) {
        resolutedString = resolutedString.replace(tuple[0], tuple[1].toString());
    }
    return resolutedString;
}


// ? Tested at playground
// const testFucntion = () => {
//     const testPath = "/class/:classId/student/:studentId";
//     const res = replacePathParms(testPath, [":classId", 1], [":studentId", "2"]);
//     console.log("REPALED: ", res);
// }
//
// testFucntion();
