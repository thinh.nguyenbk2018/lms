import React from 'react';
import {Button, ButtonProps, Space} from "antd";
import {CloseOutlined} from "@ant-design/icons";

// {onClick,...rest} : {onClick: () => void, ...rest:any}
const CancelButton = (props:ButtonProps) => {
    return (
        <Button type={"primary"} danger {...props}>
            <Space>
                <CloseOutlined />
                Hủy
            </Space>
        </Button>
    );
};

export default CancelButton;