import './RoleAndPermission.less';
import Roles from "./Role/Roles";
import Permissions from './Permission/Permissions';


const RoleAndPermission = () => {
  return <div>
    <Roles />
    <Permissions />
  </div>
}


export default RoleAndPermission;