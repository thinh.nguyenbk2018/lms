import {Table} from "antd";
import {useAppDispatch} from "../../hooks/Redux/hooks";
import CustomPagination from "../CustomPagination/CustomPagination";

import "./CustomTable.less";
import {ActionCreatorWithPayload} from "@reduxjs/toolkit";
import DEFAULT_PAGE_SIZE, {SearchCriteria} from "../../redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";


type CustomTableInterface = {
    columns: any;
    data: any;
    loading?: boolean;
    searchCriteria?: Partial<SearchCriteria>;
    changeSearchActionCreator?: ActionCreatorWithPayload<
        Partial<SearchCriteria>,
        string
    >;
    metaData?: any;
    scroll?: any;
    notUseFilter?: boolean;
};
const CustomTable = ({
    columns,
    data,
    metaData = {},
    loading = false,
    searchCriteria,
    changeSearchActionCreator,
    scroll,
    notUseFilter
}: CustomTableInterface) => {
    console.log("RENDER DATA WITH PROPS");
    const dispatch = useAppDispatch();

    const handleCriteriaChange = (
        pagination: any,
        filters: any,
        sorter: any
    ) => {
        const payload = {
            keyword: searchCriteria?.keyword || "",
            pagination: pagination,
            filter: notUseFilter ? searchCriteria?.filter : filters,
            sort: sorter,
        };
        console.log('criteria to be passed: ', payload);
        if (changeSearchActionCreator) {
            dispatch(changeSearchActionCreator(payload));
        }
    };

    const handlePageChange = (page: any, size: any) => {
        const payload = {
            ...searchCriteria,
            pagination: { page, size },
        };
        if (changeSearchActionCreator) {
            console.log("change page: ", payload);
            dispatch(changeSearchActionCreator(payload));
        }
    };

    const handlePageSizeChange = (current: any, size: any) => {
        const payload = {
            ...searchCriteria,
            pagination: { page: 1, size: size },
        };
        if (changeSearchActionCreator) {
            dispatch(changeSearchActionCreator(payload));
        }
    };


    return (
        <>
            {columns != undefined && (
                <Table
                    loading={loading}
                    columns={columns}
                    dataSource={data}
                    onChange={handleCriteriaChange}
                    rowKey="id"
                    pagination={false}
                    scroll={scroll ? scroll : { x: true }}
                    locale={{emptyText: 'Không có dữ liệu'}}
                />
            )}
            <div className={"pagination-container"}>
                <CustomPagination
                    total={metaData.total}
                    onChange={handlePageChange}
                    current={metaData.page}
                    size={searchCriteria?.pagination?.size || DEFAULT_PAGE_SIZE}
                    onShowSizeChange={handlePageSizeChange}
                />
            </div>
        </>
    );
};

export default CustomTable;
