import { BaseResponse, ID } from "../../../interfaces/types";
import { apiSlice } from "../../CentralAPI";
import { UserDto } from "../../UserSearch/UserType";
import { TeacherRole } from "./ClassTeacherSlice";

const MemberApiWithInvalidateTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["ClassTeacher"],
});

export type Teacher = UserDto & { role: TeacherRole; userId: ID };

export type TeacherAddToClassDto = Pick<Teacher, "id" | "role">;
export type TeacherListResponse = BaseResponse & {
    listData: Partial<Teacher>[];
};
export type TeacherCandidateListResponse = BaseResponse & {
    staffs: Partial<Teacher>[];
};

/**
 * This interface is for the adaptation of the new and the old api
 */
export type TeacherListAdaptedInterface = {
    listData: Teacher[];
};
const classTeacherApi = MemberApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        getTeacherForClass: build.query<
            TeacherListResponse,
            { classId: number }
        >({
            query: ({ classId }) => `classes/${classId}/teachers`,
            providesTags: (_result) => ["ClassTeacher"],
        }),
        getTeacherCandidatesForClass: build.query<
            TeacherCandidateListResponse,
            { classId: number }
        >({
            query: ({ classId }) => `classes/${classId}/teachers/candidates`,
            providesTags: (_result) => ["ClassTeacher"],
        }),
        getTeacherDetail: build.query<
            UserDto,
            { classId: number; teacherId: number }
        >({
            query: ({ classId, teacherId }) => {
                return {
                    url: `classes/${classId}/members/${teacherId}`,
                    method: "GET",
                };
            },
            providesTags: (_res, _err, args) => [
                { type: "ClassTeacher", id: args.teacherId },
            ],
        }),
        addTeachersToClass: build.mutation<
            Teacher[],
            { classId: number; teacherIds: TeacherAddToClassDto[] }
        >({
            query: ({ classId, teacherIds }) => {
                return {
                    url: `classes/${classId}/teachers`,
                    method: "POST",
                    body: {
                        idList: teacherIds,
                    },
                };
            },
            invalidatesTags: (_res, _err, _args) => ["ClassTeacher"],
        }),
        changeRoleOfTeacher: build.mutation<
            void,
            { classId: ID; teacherId: ID; teacherRole: Teacher["role"] }
        >({
            query: ({ classId, teacherId, teacherRole }) => {
                return {
                    url: `classes/${classId}/teachers/${teacherId}`,
                    method: "PUT",
                    body: {
                        id: teacherId,
                        role: teacherRole,
                    },
                };
            },
            invalidatesTags: (_res, _err, _args) => ["ClassTeacher"],
        }),
        deleteTeacherFromClass: build.mutation<
            void,
            { classId: ID; teacherId: ID }
        >({
            query: ({ classId, teacherId }) => {
                return {
                    url: `classes/${classId}/teachers/${teacherId}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: ["ClassTeacher"],
        }),
    }),
    overrideExisting: false,
});

export const {
    useGetTeacherForClassQuery,
    useGetTeacherDetailQuery,
    useGetTeacherCandidatesForClassQuery,
    useDeleteTeacherFromClassMutation,
    useAddTeachersToClassMutation,
    useChangeRoleOfTeacherMutation,
} = classTeacherApi;
