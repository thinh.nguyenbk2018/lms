import {
    Avatar,
    Button,
    Card,
    Col,
    Dropdown,
    Form,
    Input,
    Modal,
    Row,
    Space,
    Table,
    Tag,
    Tooltip,
    Typography,
} from "antd";
import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {deleteIconSvg, testSvg, viewIconSvg,} from "@components/Icons/SvgIcons";
import {useAppDispatch} from "@hooks/Redux/hooks";
import {
    AttendanceState,
    getStudentListFromAttendanceDate,
    SessionOverallAttendanceRateStrategy,
    useChangeMeetingAttendanceBatchMutation,
    useChangeMeetingAttendanceSingleMutation,
    useChangeSessionAttendanceRateCalculationStrategyMutation,
    useDeleteMeetingAttendanceBatchMutation,
    useGetAttendanceByDateQuery,
    usePutNoteToSessionMutation,
} from "../../../../../redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import {
    closeSessionCheckFocus,
    closeStudentSesssionCheckFocus,
    SessionAttendanceCheckID,
    setSessionCheckInFocus,
    setStudentSessionCheckInFocus,
} from "../../../../../redux/features/Class/ClassCheckin/ClassAttendanceSlice";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import antdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorRetryPage from "../../../../UtilPages/ApiErrorRetryPage";
import LoadingPage from "../../../../UtilPages/LoadingPage";
import AttendanceTag from "../AttendanceTag";
import ChangeAttendanceMenu from "../ChangeAttendanceMenu/ChangeAttendanceMenu";
import StrategyChangeMenu from "../StrategyChangeMenu/StrategyChangeMenu";
import {blue, volcano} from "@ant-design/colors";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import './MeetingAttendance.less';
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {ExclamationCircleOutlined} from "@ant-design/icons";
import {format} from "date-fns";
import {vi} from "date-fns/locale";
import {stateMap} from "@pages/Tenant/Class/Attendance/OverallAttendance";

const SingleAttendanceCheckColumnHeader = React.memo(
    ({
         checkName,
         sessionCheckId,
         finishedAt
     }: {
        checkName: string;
        sessionCheckId: SessionAttendanceCheckID;
        finishedAt: Date;
    }) => {
        const {classId, sessionId} = useParams();

        const dispatch = useAppDispatch();
        const [changeBatch] = useChangeMeetingAttendanceBatchMutation();
        const [deleteCheck] = useDeleteMeetingAttendanceBatchMutation();

        if (!classId || !sessionId) {
            AntdNotifier.error(
                "Đã có lỗi xảy ra. Không tìm thấy session và class này. Liên hệ quản trị viên "
            );
        }

        return (
            <>
                {/*{checkName === 'Tổng kết' ? <span>{`${checkName}`}</span> :*/}
                    <div className={'flex align-items-center'}>
                        <Dropdown
                            onVisibleChange={(visible) => {
                                if (visible) {
                                    dispatch(
                                        setSessionCheckInFocus({
                                            sessionCheckId,
                                            sessionId: Number(sessionId),
                                        })
                                    );
                                } else {
                                    dispatch(closeSessionCheckFocus());
                                }
                            }}
                            overlay={
                                <ChangeAttendanceMenu
                                    onSelect={function (key: AttendanceState): void {
                                        if ((new Date(finishedAt)).getTime() < (new Date()).getTime()) {
                                            Modal.confirm({
                                                wrapClassName: 'confirm-delete-modal-wrapper',
                                                title: (
                                                    <span className={'confirm-delete-modal-title-name'}>Thay đổi điểm danh của buổi học đã kết thúc. Bạn có chắc chắn?</span>
                                                ),
                                                content: (
                                                    <span>Bạn đang thay đổi kết quả điểm danh ngày <span
                                                        className={'confirm-delete-modal-subject-name'}>{format(new Date(finishedAt), "PP", {locale: vi})}</span>.</span>
                                                ),
                                                onOk: () => {
                                                    if (!sessionId) {
                                                        AntdNotifier.error("Không tìm thấy ngày để điểm danh");
                                                    }
                                                    changeBatch({
                                                        classId: Number(classId),
                                                        sessionId: Number(sessionId),
                                                        checkTime: sessionCheckId,
                                                        state: key,
                                                    }).unwrap()
                                                        .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                                        .catch(error => {
                                                            console.log('error: ', error);
                                                            if (error?.data) {
                                                                AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                            } else {
                                                                AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                            }
                                                        });
                                                },
                                                icon: <ExclamationCircleOutlined/>,
                                                okText: `Đồng ý`,
                                                cancelText: 'Hủy',
                                                okButtonProps: {className: 'confirm-delete-modal-ok-btn'},
                                                cancelButtonProps: {className: 'confirm-delete-modal-cancel-btn'}
                                            })
                                            return;
                                        } else {
                                            if (!sessionId) {
                                                AntdNotifier.error("Không tìm thấy ngày để điểm danh");
                                            }
                                            changeBatch({
                                                classId: Number(classId),
                                                sessionId: Number(sessionId),
                                                checkTime: sessionCheckId,
                                                state: key,
                                            }).unwrap()
                                                .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                                .catch(error => {
                                                    console.log('error: ', error);
                                                    if (error?.data) {
                                                        AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    } else {
                                                        AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    }
                                                });
                                        }
                                    }}
                                />
                            }
                        >
                            <Row className={'meeting-attendance-check-name'} justify={"space-between"}>
                                <span>{`${checkName}`}</span>
                            </Row>
                        </Dropdown>
                        {checkName !== 'Tổng kết' && <Tooltip title={"Xóa lần điểm danh"}>
                            <Button
                                type={"link"}
                                className={"delete-item-btn"}
                                onClick={() =>
                                    confirmationModal('điểm danh', checkName, () => {
                                        deleteCheck({
                                            classId: Number(classId),
                                            sessionId: Number(sessionId),
                                            checkTime: Number(sessionCheckId),
                                        })
                                            .unwrap()
                                            .then(res => {
                                                antdNotifier.success(`Xóa điểm danh ${checkName} thành công`);
                                            })
                                    })
                                }
                            >
                                {deleteIconSvg}
                            </Button>
                        </Tooltip>}
                    </div>
                {/*}*/}
            </>
        );
    }
);

const buildSingleAttendanceCheckColumn = (
    checkName: string,
    checkId: SessionAttendanceCheckID,
    finishedAt: Date
) => ({
    // * Title is special because this allows us to carry out batch action that effects the whole column data
    title: () => (
        <SingleAttendanceCheckColumnHeader
            checkName={checkName}
            sessionCheckId={checkId}
            finishedAt={finishedAt}
        />
    ),
    dataIndex: checkName,
    width: "15%",
    // TODO: Add sorter + Filter for AttendanceState: --> Create a map to PRESENT > LATE > ABSENT
    // * Render allows to modify only one row of data (EXACTLY ONE SHELL)
    render: (attendanceState: AttendanceState, record: any) => {
        return (
            <AttendanceStateWrapper
                studentId={record.studentId}
                sessionCheckId={checkId}
                state={attendanceState}
                sessionCheckName={checkName}
                finishedAt={finishedAt}
            />
        );
    },
});

// * Path must contain: Class Id and SessionID
const AttendanceStateWrapper = React.memo(
    ({
         studentId,
         sessionCheckId,
         state,
         sessionCheckName,
         finishedAt
     }: {
        studentId: number;
        sessionCheckId: SessionAttendanceCheckID;
        state: AttendanceState;
        sessionCheckName: string;
        finishedAt: Date;
    }) => {
        const dispatch = useAppDispatch();
        const [changeSingleAttendance] = useChangeMeetingAttendanceSingleMutation();
        const {classId, sessionId} = useParams();
        return (
            <>
                {/*{sessionCheckName === 'Tổng kết' ? <span>*/}
				{/*		<AttendanceTag state={state} title={stateMap[state]}/>*/}
				{/*	</span> :*/}
                    <Dropdown
                    onVisibleChange={(visible) => {
                        if (visible) {
                            dispatch(
                                setStudentSessionCheckInFocus({
                                    studentId,
                                    sessionCheckId,
                                    sessionId: Number(sessionId),
                                })
                            );
                        } else {
                            dispatch(closeStudentSesssionCheckFocus());
                        }
                    }}
                    overlay={
                        <ChangeAttendanceMenu
                            onSelect={function (key: AttendanceState): void {
                                if ((new Date(finishedAt)).getTime() < (new Date()).getTime()) {
                                    Modal.confirm({
                                        wrapClassName: 'confirm-delete-modal-wrapper',
                                        title: (
                                            <span className={'confirm-delete-modal-title-name'}>Thay đổi điểm danh của buổi học đã kết thúc. Bạn có chắc chắn?</span>
                                        ),
                                        content: (
                                            <span>Bạn đang thay đổi kết quả điểm danh ngày <span
                                                className={'confirm-delete-modal-subject-name'}>{format(new Date(finishedAt), "PP", {locale: vi})}</span>.</span>
                                        ),
                                        onOk: () => {
                                            if (!sessionId || !studentId) {
                                                AntdNotifier.error(
                                                    "Lỗi hệ thống. tạm thời không tìm thấy ngày hoặc học viên để điểm danh. Hãy thử load lại trang"
                                                );
                                                return;
                                            }

                                            changeSingleAttendance({
                                                classId: Number(classId),
                                                studentId,
                                                sessionId: Number(sessionId),
                                                checkTime: sessionCheckId,
                                                state: key,
                                            }).unwrap()
                                                .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                                .catch(error => {
                                                    console.log('error: ', error);
                                                    if (error?.data) {
                                                        AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    } else {
                                                        AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                                    }
                                                });
                                        },
                                        icon: <ExclamationCircleOutlined/>,
                                        okText: `Đồng ý`,
                                        cancelText: 'Hủy',
                                        okButtonProps: {className: 'confirm-delete-modal-ok-btn'},
                                        cancelButtonProps: {className: 'confirm-delete-modal-cancel-btn'}
                                    })
                                    return;
                                } else {
                                    if (!sessionId || !studentId) {
                                        AntdNotifier.error(
                                            "Lỗi hệ thống. tạm thời không tìm thấy ngày hoặc học viên để điểm danh. Hãy thử load lại trang"
                                        );
                                        return;
                                    }

                                    changeSingleAttendance({
                                        classId: Number(classId),
                                        studentId,
                                        sessionId: Number(sessionId),
                                        checkTime: sessionCheckId,
                                        state: key,
                                    }).unwrap()
                                        .then(res => antdNotifier.success("Thay đổi trạng thái điểm danh thành công"))
                                        .catch(error => {
                                            console.log('error: ', error);
                                            if (error?.data) {
                                                AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                            } else {
                                                AntdNotifier.error("Có lỗi trong quá trình thay đổi điểm danh. Thử lại sau.")
                                            }
                                        });
                                }
                            }}
                        />
                    }
                >
					<span>
						<AttendanceTag state={state} title={stateMap[state]}/>
					</span>
                </Dropdown>
                {/*}*/}
            </>
        );
    }
);

const actionColumn = {
    title: "Action",
    key: "action",
    fixed: "right" as const,
    width: 50,
    render: (record: any) => (
        <Space>
            <Tooltip title={"Xem thông tin điểm danh chi tiết "}>
                <Link to={`/classes/${record.id}/information`}>{viewIconSvg}</Link>
            </Tooltip>
            <Tooltip title={"Xem thông tin chi tiết của học viên"}>
                <Typography.Link>{testSvg}</Typography.Link>
            </Tooltip>
        </Space>
    ),
};

const ATTENDANCE_RATE_STRATEGY_TYPE_COLOR_MAP: {
    [P in SessionOverallAttendanceRateStrategy]: string;
} = {
    ANY: blue[5],
    LAST_TIME: volcano[5],
};
const ATTENDANCE_RATE_STRATEGY_TYPE_INFO_MAP: {
    [P in SessionOverallAttendanceRateStrategy]: string;
} = {
    ANY: "Bất cứ lần điểm danh nào vắng xem như cả buổi là vắng",
    LAST_TIME:
        "Lần cuối của buổi điểm danh sẽ quyết định tình trạng vắng của cả buổi học ",
};

export const keyTitleAttendanceStrategy = {
    "ANY": 'Ưu tiên vắng -> trễ -> có mặt',
    "LAST_TIME": 'Lần cuối cùng'
}

export const StrategyTypeTag = React.memo(
    ({type, title}: { type: SessionOverallAttendanceRateStrategy, title?: string }) => {
        return (
            <Tooltip title={""}>
                <Tag color={ATTENDANCE_RATE_STRATEGY_TYPE_COLOR_MAP[type]}>{title || type}</Tag>
            </Tooltip>
        );
    }
);

const MeetingAttendance = () => {
    const {classId, sessionId} = useParams();

    const [noteValue, setNoteValue] = useState("");
    const [isEditingNote, setIsEditingNote] = useState(false);
    const [pageSize, setPageSize] = useState<number>(DEFAULT_PAGE_SIZE);
    const [page, setPage] = useState<number>(1);

    // * RTK Query hooks
    const {
        data: meetingData,
        isSuccess,
        isFetching,
        isError,
    } = useGetAttendanceByDateQuery(
        {classId: Number(classId), sessionId: Number(sessionId)},
        {
            skip: isNaN(Number(classId)),
        }
    );

    const [changeStrategy, {isLoading}] =
        useChangeSessionAttendanceRateCalculationStrategyMutation();

    const [changeNote, {isLoading: isPuttingNoteLoading}] =
        usePutNoteToSessionMutation();
    // TODO: GET AND CHANGE THE STRATEGY :
    // const changeAttendance

    useEffect(() => {
        if (meetingData && meetingData.metaInfo.note) {
            setNoteValue(meetingData.metaInfo.note);
        }
    }, [isSuccess]);

    const studentInfoColumns = [
        {
            title: "Ảnh đại diện",
            dataIndex: "avatar",
            fixed: "left" as const,
            width: 50,
            render: (avatar: string) => <Avatar src={avatar}/>,
            align: "center" as const,
        },
        {
            title: "Họ và tên",
            dataIndex: "name",
            width: 150,
            fixed: "left" as const,
            // sorter: {
            //     compare: (a: string, b: string) => a.length - b.length,
            //     multiple: 1,
            // },
            // sortDirections: ["descend", "ascend"],
            // sortOrder:
            //  TODO: Route to the student detail page on click using the "studentId" field
            render: (name: string) => <Typography.Link> {name} </Typography.Link>,
        },
    ];

    if (isSuccess && meetingData) {
        const {attendanceInfo, metaInfo} = meetingData;
        const {strategy, note} = metaInfo;

        const checksInfo = Object.values(attendanceInfo);

        const firstDate = checksInfo[0];

        if (!firstDate) {
            return (
                <>
                    <Row justify="center">
                        <Col span={24}>
                            <Card title="Chưa có lần điểm danh nào để hiển thị ">
                                {"Bấm vào nút thêm trên header để thêm một lần điểm danh mới "}
                            </Card>
                        </Col>
                    </Row>
                </>
            );
        }
        const studentList = getStudentListFromAttendanceDate(
            firstDate.studentAttendance
        );
        const isTeacherOfThisClassWithPrivileges = true;
        // const tableData = name, avatar, date1, date2, date3, action;

        const handleChangeStrategy = (
            newStrategy: SessionOverallAttendanceRateStrategy
        ) => {
            if (strategy === newStrategy) {
                return;
            }
            AntdNotifier.handlePromise(
                changeStrategy({
                    classId: Number(classId),
                    sessionId: Number(sessionId),
                    newStrategy,
                }).unwrap(),
                "Thay đổi chiến lược điểm danh"
            );
        };
        const buildColumnsAndData = () => {
            let columns = [];

            const checks = meetingData.attendanceInfo; // Date = sid:{StudentInfo}[]

            const middleColumnsMap: Record<string, AttendanceState[]> = {};
            // alphabet sort
            const checkKeys = Object.keys(checks).sort();
            for (const checkKey of checkKeys) {
                const studentAttendanceOfTheDate = checks[checkKey].studentAttendance;
                const columnData = Object.values(studentAttendanceOfTheDate).map(
                    (item, index) => {
                        return item.state;
                    }
                );
                middleColumnsMap[checkKey] = columnData;
            }

            columns = [
                ...studentInfoColumns,
                ...Object.keys(middleColumnsMap).map((checkKey) => {
                    if (!checkKey) {
                        AntdNotifier.error("Có lỗi trong quá trình hiển thị");
                        return <></>;
                    }
                    const checkId = checks[checkKey].metaInfo.id;
                    // console.log('abc: ', meetingData);
                    return buildSingleAttendanceCheckColumn(checkKey, checkId, meetingData.metaInfo.finishedAt);
                }),
                // actionColumn,
            ];

            // * Aggregate the data in map to list --> So we can display the data in table
            const dataSource = studentList.map((item, index) => {
                let studentchecks: { [K: string]: AttendanceState } = {};
                for (const checkKey in middleColumnsMap) {
                    const date = middleColumnsMap[checkKey];
                    studentchecks[checkKey] = date[index];
                }

                return {
                    name: item.name,
                    avatar: item.avatar,
                    studentId: item.studentId,
                    ...studentchecks,
                };
            });
            return [columns, dataSource];
        };

        const [columnList, data] = buildColumnsAndData();

        return (
            <>
                <Table
                    title={() => (
                        <Row justify="center">
                            {/*<Col span={24} style={{marginBottom: '1rem'}}>*/}
                            {/*    <span className={'class-content-instruction'}>*Chiến lược điểm danh ANY: ưu tiên trạng thái điểm danh lần lượt ABSENT &gt; LATE &gt; PRESENT &gt; NONE</span>*/}
                            {/*</Col>*/}
                            <Col span={24}>
                                <Form.Item label={"Chiến lược điểm danh:"}>
                                    <Dropdown
                                        overlay={
                                            <StrategyChangeMenu
                                                onSelect={(key) => {
                                                    handleChangeStrategy(key);
                                                }}
                                            />
                                        }
                                    >
										<span>
											<StrategyTypeTag type={strategy || "LAST_TIME"}
                                                             title={keyTitleAttendanceStrategy[strategy] || keyTitleAttendanceStrategy["LAST_TIME"]}/>
										</span>
                                    </Dropdown>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label={`Ghi chú cho buổi học`}>
                                    {isEditingNote ? (
                                        <Input.TextArea
                                            value={noteValue}
                                            onChange={(e) => setNoteValue(e.target.value)}
                                            onBlur={(e) => {
                                                if (noteValue !== note) {
                                                    AntdNotifier.handlePromise(
                                                        changeNote({
                                                            classId: Number(classId),
                                                            sessionId: Number(sessionId),
                                                            note: noteValue,
                                                        }).unwrap(),
                                                        "Thay đổi ghi chú cho buổi học"
                                                    );
                                                }
                                                setIsEditingNote(false);
                                            }}
                                        />
                                    ) : (
                                        <Space>
                                            <p style={{padding: 0, margin: 0}}>{noteValue} </p>
                                            <Tooltip title={"Chỉnh sửa ghi chú "}>
												<span
                                                    onClick={() => {
                                                        setIsEditingNote(true);
                                                    }}
                                                    style={{color: blue[5]}}
                                                    className={'edit-not-attendance'}
                                                >
													{viewIconSvg}
												</span>
                                            </Tooltip>
                                        </Space>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <span><span style={{fontWeight: "bold"}}>Thời gian:</span> {format(new Date(meetingData.metaInfo.startedAt), "PPPPpp", {locale: vi})} - {format(new Date(meetingData.metaInfo.finishedAt), "PPPPpp", {locale: vi})}</span>
                            </Col>
                        </Row>
                    )}
                    columns={columnList as any}
                    dataSource={data as any}
                    scroll={{x: 1000}}
                    pagination={{
                        pageSize: pageSize,
                        current: page,
                        showSizeChanger: true, onShowSizeChange: (current: number, size: number) => {
                            setPageSize(size);
                            setPage(page);
                        },
                        onChange: (page: number, size: number) => {
                            setPage(page);
                        }
                    }}
                />
            </>
        );
    } else if (isFetching) {
        return <LoadingPage/>;
    } else {
        return <ApiErrorRetryPage/>;
    }
};

export default MeetingAttendance;
