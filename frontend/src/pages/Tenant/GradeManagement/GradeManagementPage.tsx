import { Button, Space } from "antd"
import React from 'react';
import { useEffect } from "react";
import { Link, useParams } from "react-router-dom"
import ContentHeader from "../../../components/ContentHeader/ContentHeader"
import GradeManagement from "../../../components/GradeManagement/GradeManagement";
import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import { ScopeTag, useLazyGetAllGradeTagsDetailsQuery } from "../../../redux/features/Grade/GradeApi";
import {addIconSvg} from "@components/Icons/SvgIcons";
import useClassSidebar from "@hooks/Routing/useClassSidebar";

const GradeManagementPage = ({ scope }: { scope: ScopeTag }) => {
  const { classId, courseId } = useParams();
  const [getAllGradeTagsDetails, { data: getAllGradeTagsDetailsResponse, isLoading }] = useLazyGetAllGradeTagsDetailsQuery();

  const [closeSidebar] = useClassSidebar();

  useEffect(() => {
    getAllGradeTagsDetails({ scope, scopeId: classId ? parseInt(classId) : parseInt(courseId!) });
  }, [classId, courseId, scope, getAllGradeTagsDetails])

  return <>
    <ContentHeader
      title="Điểm"
      action={
        <Space>
          <Link to={scope === "CLASS" ? ROUTING_CONSTANTS.VIEW_GRADE_IN_CLASS.replace(":classId", classId!) : ROUTING_CONSTANTS.VIEW_GRADE_IN_COURSE.replace(":courseId", courseId!)}>
            <Button
              type="ghost"
            >
              Xem điểm
            </Button>
          </Link>
          <Link to={scope === "CLASS" ? ROUTING_CONSTANTS.CREATE_GRADE_FORMULA_IN_CLASS.replace(":classId", classId!) : ROUTING_CONSTANTS.CREATE_GRADE_FORMULA_IN_COURSE.replace(":courseId", courseId!)}>
            <Button
                className={"button-with-icon"}
                icon={addIconSvg}
                type="primary"
            >
              Thêm
            </Button>
          </Link>
        </Space>
      }
    />
    <GradeManagement scope={scope} scopeId={parseInt(classId || courseId!)} tags={getAllGradeTagsDetailsResponse?.tags || []} isLoading={isLoading} />
  </>
}

export default GradeManagementPage;