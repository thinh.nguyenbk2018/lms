import {Select, Space} from "antd";
import {FieldProps} from "formik";
import {CSSProperties} from "react";
import {useParams} from "react-router-dom";
import LoadingPage from "../../../pages/UtilPages/LoadingPage";
import {useGetTeacherForClassQuery} from "../../../redux/features/Class/TeacherAPI/ClassTeacherAPI";

type SearchSelectFieldProps = FieldProps<any> & {
    label?: string;
    style?: CSSProperties;
    required?: boolean;
    onSelect?: () => void;
    showErrorMessage?: boolean;
};

const TeacherSearchSelectField = ({
                                      field,
                                      form: {
                                          touched,
                                          errors,
                                          setFieldValue,
                                          setFieldTouched,
                                      },
                                      ...otherProps
                                  }: SearchSelectFieldProps) => {
    const {classId} = useParams();
    const {
        data: listTeacher,
        isError,
        isSuccess,
        isFetching,
    } = useGetTeacherForClassQuery(
        {classId: Number(classId)},
        {
            skip: isNaN(Number(classId)),
        }
    );
    const {name, value, onChange, onBlur} = field;
    const errorMsg = touched[name] && errors[name];

    if (isFetching) {
        return (
            <>
                <LoadingPage/>
            </>
        );
    } else if (isSuccess && listTeacher.listData) {
        return (
                <Select
                    {...field}
                    onChange={(teacherId) => {
                        setFieldValue(name, teacherId);
                        setFieldTouched(name, true);
                    }}
                    style={{minWidth: "100px"}}
                    notFoundContent={"Không tìm thấy giáo viên"}
                    allowClear={true}
                >
                    {listTeacher.listData.map((item, index) => {
                        return (
                            <Select.Option value={item.userId} key={item.userId}>
                                {item.lastName} {item.firstName}
                            </Select.Option>
                        );
                    })}
                </Select>
        );
    } else {
        return <> </>;
    }
};

export default TeacherSearchSelectField;
