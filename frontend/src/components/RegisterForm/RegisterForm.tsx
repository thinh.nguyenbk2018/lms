import React from 'react';
import {Button, Form, Image, Input, Space, Typography} from "antd";
import logo from "@assets/logo/logo.png";
import {FileImageTwoTone, MailTwoTone, PhoneTwoTone} from '@ant-design/icons';
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors, registerNewAccount} from "@redux/features/Auth/AuthSlice";
import {LocalImageChooserWithFirebase} from "../ImageChooser/ImageChooserWithFirebase";
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import { TenantCustomInfo } from '@redux/features/Tenant/TenantAPI';


// TODO: Backend please auto generate an calendar id for this user using the calendar service
const RegisterForm = ({customData}:{customData: TenantCustomInfo}) => {
    const [avatar, setAvatar] = React.useState("");
    const dispatch = useAppDispatch();
    const registerSuccess = useAppSelector(authSelectors.selectRegisterSuccess);

    const onFinish = (values: any) => {
        const valuesWithAvatar = {...values, avatar}
        console.log('Success:', valuesWithAvatar);
        dispatch(registerNewAccount(valuesWithAvatar));
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    if (registerSuccess) {
        AntdNotifier.success("Register Successfully");
    }
    return (
        <Space direction={"vertical"} align={"center"}>
            <Image width={200} height={200} src={logo}/>
            <Typography.Title>
                BKLMS
            </Typography.Title>
            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: 'Hãy nhập tài khoản của bạn',
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="First Name"
                    name="firstname"
                    rules={[
                        {
                            required: true,
                            message: "Your First Name",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Last Name"
                    name="lastname"
                    rules={[
                        {
                            required: true,
                            message: "Your Last Name",
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label={
                        <Space>
                            <MailTwoTone/>
                            <Typography.Text>
                                Email
                            </Typography.Text>
                        </Space>
                    }
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Nhập email của bạn',
                        },
                    ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Nhập mật khẩu vào',
                        },
                    ]}
                >
                    <Input.Password/>
                </Form.Item>

                {/*<Space>*/}
                    <Form.Item
                        label="Confirm Password"
                        name="rePassword"
                        rules={[
                            {
                                required: true,
                                message: 'Mật khẩu phải khớp với mật khẩu đã nhập',
                            },
                        ]}
                    >
                        <Input.Password/>
                    </Form.Item>
                {/*</Space>*/}
                <Form.Item
                    label={
                        <Space>
                            <FileImageTwoTone/>
                            <Typography.Text>
                                Avatar
                            </Typography.Text>
                        </Space>
                    }
                >
                    <LocalImageChooserWithFirebase callback={setAvatar}/>
                </Form.Item>
                <Form.Item
                    label={
                        <Space>
                            <PhoneTwoTone/>
                            <Typography.Text>
                                Phone
                            </Typography.Text>
                        </Space>
                    }
                    name="phone"
                >
                    <Input size={"middle"}/>
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                    className={'login-form-login-btn'}
                >
                    <Button type="primary" htmlType="submit">
                        Đăng ký
                    </Button>
                </Form.Item>
            </Form>
        </Space>
    );
};

export default RegisterForm;
