import {notification} from "antd";
import * as React from "react";
import {IconType, NotificationPlacement} from "antd/lib/notification";

export interface ArgsProps {
	message: React.ReactNode;
	description?: React.ReactNode;
	btn?: React.ReactNode;
	key?: string;
	onClose?: () => void;
	duration?: number | null;
	icon?: React.ReactNode;
	placement?: NotificationPlacement;
	style?: React.CSSProperties;
	prefixCls?: string;
	className?: string;
	readonly type?: IconType;
	onClick?: () => void;
	top?: number;
	bottom?: number;
	getContainer?: () => HTMLElement;
	closeIcon?: React.ReactNode;
}
const notiBuilder: (
	message: string | JSX.Element,
	title?: string | JSX.Element,
	duration?: number,
	onClick?: () => void
) => ArgsProps = (message, title, duration, onClick) => {
	if (message && title) {
		return {
			message: title,
			description: message,
			onClick: onClick,
			duration,
		};
	} else {
		return {
			message: message,
			onClick: onClick,
			duration,
		};
	}
};

const AntdNotifier = (() => {
	const warn: (
		message: string | JSX.Element,
		title?: string | JSX.Element,
		duration?: number
	) => void = (message, title, duration) => {
		notification["warn"](notiBuilder(message, title, duration));
	};

	const error: (
		message: string | JSX.Element,
		title?: string | JSX.Element,
		duration?: number
	) => void = (message, title, duration) => {
		notification["error"](notiBuilder(message, title, duration));
	};

	const apiError: (
		message: string | JSX.Element,
		title?: string | JSX.Element,
		duration?: number
	) => void = (message, _title, duration) => {
		notification["error"](
			notiBuilder(
				message,
				undefined,
				duration
			)
		);
	};

	const success: (
		message: string | JSX.Element,
		title?: string | JSX.Element,
		duration?: number,
		onClick?: () => void
	) => void = (message, title, duration, onClick) => {
		notification["success"](notiBuilder(message, title, duration, onClick));
	};

	// * Please unwrap the promise first if said promise is a RTK-Query Mutation
	const handlePromise = (
		promise: Promise<any>,
		actionName: string,
		onSuccess?: (data: any) => void,
		onError?: (err: any) => void
	) => {
		promise
			.then((res) => {
				success(`${actionName} thành công`);
				onSuccess?.(res);
			})
			.catch((err) => {
				if (!onError) {
					error(`${actionName} thất bại`);
				} else {
					onError?.(err);
				}
			});
	};
	// const defaultHandler: ()
	return {
		warn,
		error,
		success,
		apiError,
		handlePromise,
	};
})();

export default AntdNotifier;
