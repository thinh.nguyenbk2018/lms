import { Button } from "antd";
import TodoComponent from "../TodoComponent";
import { useGetTodoListQuery } from "./todoApi";

function gibberish(length: number): string {
    var result = "";
    var characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
        );
    }
    return result;
}

const TodoListWithRTKQuery = () => {
    const {
        data: todos = [],
        isSuccess,
        isError,
        isLoading,
    } = useGetTodoListQuery();

    const numOfTodos = todos.length;
    if (isLoading) {
        return <div>Loading...</div>;
    } else if (isError) {
        return <div>Error...</div>;
    } else if (isSuccess) {
        return (
            <div>
                <div>{`Number of todos are: ${numOfTodos}`}</div>
                {todos.map((item) => (
                    <TodoComponent {...item} key={item.title} />
                ))}
                <Button onClick={() => {}}>add todo</Button>
            </div>
        );
    }
    return <></>;
};

export default TodoListWithRTKQuery;
