import {defineFileAction} from "chonky";

export const PasteFiles = defineFileAction({
  id: 'Paste_files_action',
  button: {
    name: "Paste files",
    icon: "paste",
    group: 'Options',
    contextMenu: true
  },
})

export const MoveFiles = defineFileAction({
  id: 'Move_files_action',
  button: {
    name: "Move files",
    icon: "dndDragging",
    group: 'Options',
    contextMenu: true
  },
  requiresSelection: true
})

export const ShareFiles = defineFileAction({
  id: "Share_files_action",
  button: {
    name: "Share file",
    icon: "symlink",
    group: 'Options',
    contextMenu: true,
  },
  requiresSelection: true
})

export const ReceiveFiles = defineFileAction({
  id: "Receive_files_action",
  button: {
    name: "Receive files",
    icon: "dndCanDrop",
    toolbar: true,
  }
})