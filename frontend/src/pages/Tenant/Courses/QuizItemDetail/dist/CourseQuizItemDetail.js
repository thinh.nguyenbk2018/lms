"use strict";
exports.__esModule = true;
var antd_1 = require("antd");
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var EditButton_1 = require("../../../../components/ActionButton/EditButton");
var ContentHeader_1 = require("../../../../components/ContentHeader/ContentHeader");
var hooks_1 = require("../../../../hooks/Redux/hooks");
var useCourseSidebar_1 = require("../../../../hooks/Routing/useCourseSidebar");
var CourseAPI_1 = require("../../../../redux/features/Course/CourseAPI");
var CourseContentSlice_1 = require("../../../../redux/features/Course/CourseContentSlice");
var LoadingPage_1 = require("../../../UtilPages/LoadingPage");
var CourseContent_1 = require("../CourseContentPage/CourseContentPage");
require("./CourseQuizItemDetail.less");
var CourseQuizItemDetailPage = function () {
    var _a = useCourseSidebar_1["default"](), courseDetail = _a.courseDetail, isCourseDetailFetching = _a.isCourseDetailFetching, isCourseDetailSuccess = _a.isCourseDetailSuccess;
    var _b = react_router_dom_1.useParams(), quizItemId = _b.quizItemId, chapterId = _b.chapterId, courseId = _b.courseId;
    var _c = CourseAPI_1.useGetQuizDetailQuery({
        courseId: Number(courseId),
        chapterId: Number(chapterId),
        quizId: Number(quizItemId)
    }, {
        skip: isNaN(Number(quizItemId)) ||
            isNaN(Number(chapterId)) ||
            isNaN(Number(courseId))
    }), quizData = _c.data, isSuccess = _c.isSuccess, isFetching = _c.isFetching;
    // * Redux store
    var dispatch = hooks_1.useAppDispatch();
    if (isCourseDetailSuccess && isSuccess) {
        return (react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement(ContentHeader_1["default"], { title: "Kh\u00F3a H\u1ECDc: " + (courseDetail === null || courseDetail === void 0 ? void 0 : courseDetail.name) + " - Chi ti\u1EBFt quiz: " + quizData.title, action: react_1["default"].createElement(antd_1.Space, null,
                    react_1["default"].createElement(EditButton_1["default"], { onClick: function () {
                            dispatch(CourseContentSlice_1.courseContentActions.updatingActivity({
                                chapterToEdit: Number(chapterId),
                                activity: quizData
                            }));
                        } })) }),
            react_1["default"].createElement(antd_1.Row, { justify: "center" },
                react_1["default"].createElement("div", { className: "quiz-item-info-card" },
                    react_1["default"].createElement("div", { className: "title" },
                        react_1["default"].createElement("div", { className: "field-value" }, quizData.title)),
                    react_1["default"].createElement("div", { className: "field-container" },
                        react_1["default"].createElement("div", { className: "field-name" }, "M\u00F4 t\u1EA3: "),
                        react_1["default"].createElement("div", { className: "field-value" },
                            quizData.description,
                            " ")),
                    react_1["default"].createElement("div", { className: "field-container" },
                        react_1["default"].createElement("div", { className: "field-name" }, "Tag t\u00EDnh \u0111i\u1EC3m: "),
                        react_1["default"].createElement("div", { className: "field-value" }, quizData.tag)))),
            react_1["default"].createElement(CourseContent_1.AddQuizItemModal, null)));
    }
    else if (isCourseDetailFetching || isFetching) {
        return react_1["default"].createElement(LoadingPage_1["default"], null);
    }
    else {
        return react_1["default"].createElement(react_1["default"].Fragment, null);
    }
};
exports["default"] = CourseQuizItemDetailPage;
