import { CheckCircleTwoTone } from "@ant-design/icons";
import { Row, Typography } from "antd";

const RegisterSuccessPage = () => {
  return <div style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", height: "85vh" }}>
    <CheckCircleTwoTone twoToneColor="#52c41a" style={{ fontSize: '100px', marginBottom: 50 }} />
    <h1>Chào mừng bạn đến với hệ thống của chúng tôi.</h1>
    <h1>Chúng tôi vừa gửi cho bạn một mail xác thực. Làm theo hướng dẫn để hoàn thành đăng ký.</h1>
    <h1 style={{ marginBottom: 350 }}>Kết quả đăng ký sẽ bị hủy sau <Typography.Text type="danger">7 ngày</Typography.Text> nếu bạn không xác thực.</h1>
  </div>
}

export default RegisterSuccessPage;