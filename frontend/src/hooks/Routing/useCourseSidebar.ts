import {useAppDispatch} from "../Redux/hooks";
import {useNavigate, useParams} from "react-router-dom";
import {useEffect} from "react";
import {sideBarActions} from "../../redux/features/SideBar/SidebarSlice";
import { useGetCourseInformationQuery } from "../../redux/features/Course/CourseAPI";


export const isPathRelatedToCourse: (path: string) => boolean = (path) => {
    const pathnames = path.split("/");
    return pathnames.includes("courses");

}
/**
 * This hooks check if the current tab is a child tab of the class
 * If True: => Open the sidebar
 * On leaving : This hooks will try to close the sidebar
 */
const useCourseSidebar = () => {
    const dispatch = useAppDispatch();
    const {courseId} = useParams();

    const {data: courseDetail, isSuccess:isCourseDetailSuccess, isError:isCourseDetailError, isFetching:isCourseDetailFetching} = useGetCourseInformationQuery(Number(courseId), {skip: courseId == undefined});


    const forceCloseSidebar = () => {
        dispatch(sideBarActions.setCourseIdInFocus(Number(undefined)));
    }
    useEffect(() => {
        // console.log("Tring to open the sidebar");
        if (courseId && !isNaN(Number(courseId))) {
            dispatch(sideBarActions.setCourseIdInFocus(Number(courseId)));
        } else {
            dispatch(sideBarActions.setCourseIdInFocus(Number(undefined)));
        }
    }, [])
    return {forceCloseSidebar, courseDetail, isCourseDetailSuccess, isCourseDetailError, isCourseDetailFetching,
        courseId: Number(courseId)};
}

export default useCourseSidebar;