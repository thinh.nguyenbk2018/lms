import React from 'react';
import PropTypes from 'prop-types';
import numericalFieldHandler from '../../../utils/numericalFieldHandler';
import translateLabel from '../../../utils/translateLabel';
import {Col, Input, Row, Select, Space} from "antd";
import {isNaN} from "lodash";

const RepeatMonthlyOn = ({
  id,
  mode,
  on,
  hasMoreModes,
  handleChange,
  translations
}) => {
  const isActive = mode === 'on';

  return (
    <Row className={`form-group row d-flex align-items-sm-center ${!isActive && 'opacity-50'}`}>
      <Col span={24}>
        {hasMoreModes && (
            <Space>
                {translateLabel(translations, 'repeat.monthly.on_day')}
                <Input
                    id={id}
                    type="radio"
                    name="repeat.monthly.mode"
                    aria-label="Repeat monthly on"
                    value="on"
                    checked={isActive}
                    onChange={handleChange}
                />
            </Space>
        )}
      </Col>

      <Col>
        <Select
          id={`${id}-day`}
          name="repeat.monthly.on.day"
          aria-label="Repeat monthly on a day"
          className="form-control"
          value={on.day}
          disabled={!isActive}
          onChange={(value, option) => {
              // Convert input from a string to a number
              const inputNumber = +value;
              // Check if is a number and is less than 1000
              if (isNaN(inputNumber) || inputNumber >= 1000) return;
              const target = {name:"repeat.monthly.on.day",value: inputNumber};
              handleChange({target})
          }}
        >
          {[...new Array(31)].map((day, i) => <option key={i} value={i + 1}>{i + 1}</option>)}
        </Select>
      </Col>
    </Row>
  );
};
RepeatMonthlyOn.propTypes = {
  id: PropTypes.string.isRequired,
  mode: PropTypes.oneOf(['on', 'on the']).isRequired,
  on: PropTypes.shape({
    day: PropTypes.number.isRequired,
  }).isRequired,
  hasMoreModes: PropTypes.bool.isRequired,
  handleChange: PropTypes.func.isRequired,
  translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default RepeatMonthlyOn;
