import { Modal, ModalProps } from "antd";
import classNames from "classnames";
import { isJsxElement } from "@utils/Predicates/Styles/elementPredicates";
import "./ModalWithSections.less";
import { isString } from "lodash";
import Scrollable from "@components/Scrollable/Scrollable";

type ModalSection = {
    title: JSX.Element | string;
    body: JSX.Element | string;
};
type ModalWidth = "small" | "medium" | "large";
/**
 * @widthValue:
 *  - small: the modal is as small as possible and will take up no more than half the screen
 *  - medium: the modal is as big as it should be (content + padding and margin, should take at least half the screen )
 *  - large: the modal is really big (should take almost the whole screen)
 * @return: The value for css width property
 */
const getModalWidth = (widthValue: "small" | "medium" | "large") => {
    if (widthValue == "small") {
        return "min(300px, calc(37% + 50px)";
    } else if (widthValue == "medium") {
        return "min(700px, calc(50% + 100px)";
    } else {
        return "min(1500px, calc(80% + 150px)";
    }
};
/**
 * If the passed in element is an JSX. Then the section will not try interfere with
 * the component styling.
 * IF the passed in element is a string. The the section will try to style it to
 * emphasize its role
 */
const ModalSection = (section: ModalSection) => {
    return (
        <section className="section-modal__section-container">
            <div
                className={classNames({
                    "section-modal__section-header-container": true,
                    "section-modal__section-header-container_text": isString(
                        section.title
                    ),
                })}
            >
                {section.title}
            </div>
            <div
                className={classNames({
                    "section-modal__section-body-container": true,
                    "section-modal__section-body-container_text": isString(
                        section.title
                    ),
                })}
            >
                {section.body}
            </div>
        </section>
    );
};
type ModalWithSectionProps = ModalProps & {
    sections: ModalSection[];
    modalWidth?: ModalWidth;
};
const ModalWithSection = (props: ModalWithSectionProps) => {
    const { sections, modalWidth = "medium", ...modalProps } = props;
    return (
        <Modal {...modalProps} style={{ minWidth: getModalWidth(modalWidth) }} >
            <Scrollable height={"60vh"} direction="Vertical">
                <div className="section-modal-container">
                {sections.map((section, index) => (
                    <ModalSection key={index} {...section} />
                ))}
            </div>
            </Scrollable>
        </Modal>
    );
};

export default ModalWithSection;
