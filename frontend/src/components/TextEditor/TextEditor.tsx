/*
 * Simple editor component that takes placeholder text as a prop
 */
import FileManager from "@components/FileManager/FileManager";
import {Modal} from "antd";
import React, {CSSProperties, FocusEventHandler, useMemo, useRef, useState} from "react";
import ReactDOMServer from "react-dom/server";
import ReactQuill, {Quill} from "react-quill";
import "react-quill/dist/quill.snow.css";
import {useAppSelector} from "../../hooks/Redux/hooks";
import {authSelectors} from "../../redux/features/Auth/AuthSlice";
import antdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import {validateTextEditor} from "../CustomFields/TextEditorField/ValidateTextEditor";
import {fullScreen1IconSvg} from "../Icons/SvgIcons";
// import { ImageResize } from 'quill-image-resize-module';
import "./TextEditor.less";
import BlotFormatter from "quill-blot-formatter";

const Image = Quill.import('formats/image');
// @ts-ignore

// Quill.register('modules/imageResize', ImageResize);

let formats = [
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "color",
    "background",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "height",
    "width",
    "class",
    "style",
    "align",
    "data-align"
];

const ATTRIBUTES = [
	'alt',
	'height',
	'width',
	'class', // this will allow the class attribute on the image tag,
    'data-align',
	'style'
]

class CustomImage extends Image {
	static formats(domNode: any) {
		return ATTRIBUTES.reduce((formats, attribute) => {
			const copy = { ...formats };

			if (domNode.hasAttribute(attribute)) {
				copy[attribute] = domNode.getAttribute(attribute);
			}

			return copy;
		}, {});
	}

	format(name: any, value: any) {
		if (ATTRIBUTES.indexOf(name) > -1) {
			if (value) {
				this.domNode.setAttribute(name, value);
			} else {
				this.domNode.removeAttribute(name);
			}
		} else {
			super.format(name, value);
		}
	}
}

Quill.register("formats/image", CustomImage);
Quill.register("modules/blotFormatter", BlotFormatter);

interface OnChangeHandler {
	(e: string): void;
}

export type TextEditorProps = {
	id?: string;
	onChange?: OnChangeHandler;
	value?: string;
	additionalStyles?: CSSProperties;
	readOnly?: boolean;
	useBubbleTheme?: boolean;
} & {
	onBlur?: FocusEventHandler<HTMLInputElement> | undefined;
};

let state = {
	editorHtml: "",
	theme: "snow",
};

let normalizeValue = (text: string | undefined) => {
	if (text == undefined || !validateTextEditor(text)) {
		return "";
	}
	return text;
};

const TextEditor = ({
	onChange,
	value,
	additionalStyles,
	readOnly,
	useBubbleTheme,
	onBlur,
	id,
}: TextEditorProps) => {
	const userInfo = useAppSelector(authSelectors.selectCurrentUser);
	const isUserLoggedIn = useAppSelector(authSelectors.selectLoginSuccess);
	const editorRef = useRef<ReactQuill>(null);

	const [showFileBrowser, setShowFileBrowser] = useState(false);

	function insertToEditor(url: string) {

		const editor = editorRef.current?.getEditor();
		const position = editor?.getSelection()?.index
			? editor?.getSelection()?.index
			: editor?.getLength();
		editor?.insertEmbed(position ? position : 0, "image", url, "user");
		const newSelection = editor?.getSelection()?.index;
		editor?.setSelection(newSelection ? newSelection + 10 : 10, 0);
	}

	const imageHandler = () => {
		setShowFileBrowser(true);
	};

	const fullScreenRequestHandler = () => {
		if (id) {
			const editor = document.getElementById(id);
			const container = document.querySelector(`#${id} .ql-editor`);
			const body = document.getElementsByTagName("body")[0];
			if (editor?.classList.contains("fullscreen")) {
				editor.setAttribute(
					"class",
					"flex flex-col overscroll-contain relative scroll-bar-thin"
				);
				container?.setAttribute("class", "ql-editor scroll-bar-thin");
				if (!id.includes("modal")) {
					body.setAttribute("class", "");
				}
			} else {
				editor?.setAttribute(
					"class",
					"fullscreen bg-white fixed z-50 top-0 left-0 h-screen w-screen flex flex-col overscroll-contain"
				);
				container?.setAttribute(
					"class",
					"ql-editor ql-editor-full-screen scroll-bar-thin"
				);
				if (!id.includes("modal")) {
					body.setAttribute("class", "overflow-hidden");
				}
			}
		} else {
			antdNotifier.warn("Chế độ toàn màn hình không khả dụng", "Thông báo", 1);
		}
	};

	const modules = useMemo(
		() => ({
			toolbar: {
				container: [
					[
						"bold",
						"italic",
						"underline",
						"strike",
						"color",
						"background",
						"blockquote",
						{ list: "ordered" },
						{ list: "bullet" },
						{ indent: "-1" },
						{ indent: "+1" },
						{ align: 'center' }, { align: 'right' }, { align: 'justify' },
						"link",
						"image",
						"clean"
					],
					// ["fullscreen"],
				],

				handlers: {
					image: imageHandler,
					fullscreen: fullScreenRequestHandler,
				},
			},
			clipboard: {
				matchVisual: false,
			},
			blotFormatter: {}
			// imageResize: {
			// 	parchment: Quill.import('parchment'),
			// 	modules: ['Resize', 'DisplaySize']
			// }
		}),
		[]
	);

	const icons = Quill.import("ui/icons");
	icons["fullscreen"] = ReactDOMServer.renderToString(fullScreen1IconSvg);

	return <>
		<div style={additionalStyles} className={"editor"} onBlur={onBlur}>
			<ReactQuill
				ref={editorRef}
				theme={useBubbleTheme ? "bubble" : state.theme}
				formats={formats}
				value={normalizeValue(value)}
				modules={modules}
				onChange={(text: string) => onChange?.(normalizeValue(text))}
				readOnly={!!readOnly}
				id={id}
			/>

		</div>

		<Modal visible={showFileBrowser} width={"80%"} footer={null} onCancel={() => setShowFileBrowser(false)}>
			<div style={{ padding: "25px 20px 0px 20px" }}>
				<FileManager onSelectFile={({ fileUrl }) => {
					setShowFileBrowser(false);
					insertToEditor(fileUrl);
				}} />
			</div>
		</Modal>
	</>;
};

export default TextEditor;
