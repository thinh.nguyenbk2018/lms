import {Space} from "antd";
import React from "react";
import {useNavigate, useParams} from "react-router-dom";
import AddButton from "../../../../components/ActionButton/AddButton";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import ApiErrorRetryPage from "../../../UtilPages/ApiErrorRetryPage";
import MeetingAttendance from "./MeetingAttendance/MeetingAttendance";
import {
    useAddNewAttendanceCheckMutation,
    useGetClassAttendancesQuery
} from "@redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import BackButton from "@components/ActionButton/BackButton";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";

const ClassAttendanceDetailPage = () => {
    const {classId, sessionId} = useParams();
    const [closeSidebar] = useClassSidebar();

    const navigate = useNavigate();

    const {
        refetch: refetchClassAttendance
    } = useGetClassAttendancesQuery(
        { classId: Number(classId) },
        {
            skip: isNaN(Number(classId)),
        }
    );

    const [addNewAttendanceCheck, {isLoading: isAddingCheckLoading}] =
        useAddNewAttendanceCheckMutation();

    if (
        !classId ||
        !sessionId ||
        isNaN(Number(classId)) ||
        isNaN(Number(sessionId))
    ) {
        return <ApiErrorRetryPage/>;
    }
    return (
        <>
            <ContentHeader
                title={`Điểm danh học viên`}
                action={
                    <Space>
                        <BackButton
                            type="primary"
                            danger
                            onClick={() => {
                                refetchClassAttendance();
                                navigate(
                                    replacePathParams(ROUTING_CONSTANTS.CLASS_CHECK_IN, [":classId", classId || ""])
                                );
                            }}
                            className={"back-btn"}
                        />
                        <AddButton
                            loading={isAddingCheckLoading}
                            onClick={() => {
                                addNewAttendanceCheck({
                                    classId: Number(classId),
                                    sessionId: Number(sessionId),
                                });
                            }}
                        />
                    </Space>
                }
            />
            <MeetingAttendance/>
        </>
    );
};

export default ClassAttendanceDetailPage;
