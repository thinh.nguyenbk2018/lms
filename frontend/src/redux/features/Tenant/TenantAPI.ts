import { BaseResponse } from "@redux/interfaces/types";
import { apiSlice } from "../CentralAPI";


const tenantApiInvalidatesTags = apiSlice.enhanceEndpoints({
  addTagTypes: ["TenantCustomInfo"],
});

export interface TenantCustomInfo {
  tenantId: string,
  name: string,
  logo: string,
  banner: string
}

const tenantAPI = tenantApiInvalidatesTags.injectEndpoints({
  endpoints: (build) => ({
    getTenantCustomInfo: build.query<TenantCustomInfo, string>({
      query: (tenantId)=> `/tenant/customizeInfo?tenantId=${tenantId}`,
      providesTags: ["TenantCustomInfo"]
    }),
    updateCustomInfo: build.mutation<BaseResponse, TenantCustomInfo>({
      query: (body)=>({
        url: "/tenant/customize",
        method: "POST",
        body
      }),
      invalidatesTags: ["TenantCustomInfo"]
    })
  }),
  overrideExisting: false,
});

export const {
  useGetTenantCustomInfoQuery,
  useUpdateCustomInfoMutation
} = tenantAPI;