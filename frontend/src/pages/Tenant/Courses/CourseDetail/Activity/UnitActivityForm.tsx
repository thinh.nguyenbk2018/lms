import { Field, Formik } from 'formik'
import { Form, FormItem, Input } from 'formik-antd'
import React from 'react'
import TextEditorField from '../../../../../components/CustomFields/TextEditorField/TextEditorField'
import { QuizItem } from '../../../../../typescript/interfaces/courses/CourseTypes'


// TODO: Add the array select for the textbook id
type UnitActivityProps = {
  initialValues?: QuizItem,
  onSubmitCallback?: (values:any) => void
}
const UnitActivityForm = ({initialValues, onSubmitCallback} : UnitActivityProps) => {
  return (
    <>
      <Formik initialValues={initialValues || {}} onSubmit={() => {}}>
        {
          ({values }) => {
            return <Form>
              <FormItem name="title">
                <Input name="title" />
              </FormItem>
              <Field name={"content"} placeholder={"Type article content"} component={TextEditorField} label={"ashdiasd"} />
            </Form>
          }
        }
      </Formik>
    </>
  )
}

export default UnitActivityForm