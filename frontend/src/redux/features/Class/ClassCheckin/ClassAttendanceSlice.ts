import { PayloadAction } from "@reduxjs/toolkit";
import { ID } from "../../../interfaces/types";
import { RootState } from "../../../store/store";
import {
	createSliceEnhancedWithSearch,
	EnhancedSliceWithSearch,
	generateInitialSearchSlice,
} from "../../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";

export type SessionAttendanceCheckID = ID;
export interface ClassAttendanceInnerSlice {
	sessionBatchInFocus: number | undefined;
	studentDateInFocus:
		| {
				sessionId: ID | undefined;
				studentId: ID | undefined;
		  }
		| undefined;
	sessionCheckInFocus:
		| {
				sessionId: ID | undefined;
				sessionCheckId: SessionAttendanceCheckID | undefined;
		  }
		| undefined;
	studentSessionCheckInFocus:
		| {
				sessionId: ID | undefined;
				sessionCheckId: SessionAttendanceCheckID | undefined;
				studentId: ID | undefined;
		  }
		| undefined;
}

export const CLASS_ATTENDANCE_SLICE_KEY = "classAttendance";

const initialState: EnhancedSliceWithSearch<ClassAttendanceInnerSlice> = {
	...generateInitialSearchSlice(),
	sessionBatchInFocus: undefined,
	studentDateInFocus: undefined,
	sessionCheckInFocus: undefined,
	studentSessionCheckInFocus: undefined,
};

export const classAttendanceSlice = createSliceEnhancedWithSearch({
	name: CLASS_ATTENDANCE_SLICE_KEY,
	initialState,
	reducers: {
		setSessionBatchInfocus: (state, action: PayloadAction<number>) => {
			state.sessionBatchInFocus = action.payload;
		},
		setStudentSessionInFocus: (
			state,
			action: PayloadAction<{ studentId: ID; sessionId: ID }>
		) => {
			const { studentId, sessionId } = action.payload;
			state.studentDateInFocus = {
				studentId,
				sessionId,
			};
		},
		setSessionCheckInFocus: (
			state,
			action: PayloadAction<{
				sessionId: ID;
				sessionCheckId: SessionAttendanceCheckID;
			}>
		) => {
			const { sessionCheckId, sessionId } = action.payload;
			state.sessionCheckInFocus = { sessionCheckId, sessionId };
		},
		setStudentSessionCheckInFocus: (
			state,
			action: PayloadAction<{
				sessionId: ID;
				sessionCheckId: SessionAttendanceCheckID;
				studentId: ID;
			}>
		) => {
			const { sessionId, sessionCheckId, studentId } = action.payload;
			state.studentSessionCheckInFocus = {
				sessionId,
				sessionCheckId,
				studentId,
			};
		},

		closeBatchSessionFocus: (state) => (state.sessionBatchInFocus = undefined),
		closeStudentSessionFocus: (state) => (state.studentDateInFocus = undefined),
		closeSessionCheckFocus: (state) => (state.sessionCheckInFocus = undefined),
		closeStudentSesssionCheckFocus: (state) =>
			(state.studentSessionCheckInFocus = undefined),
	},
});

export const classAttendanceSelector = {
	selectSearchCriteria: (state: RootState) =>
		state[CLASS_ATTENDANCE_SLICE_KEY].searchCriteria,
	selectSessionBatchInFocus: (state: RootState) =>
		state[CLASS_ATTENDANCE_SLICE_KEY].sessionBatchInFocus,
	selectStudentSessionInFocus: (state: RootState) =>
		state[CLASS_ATTENDANCE_SLICE_KEY].studentDateInFocus,
};

export const {
	changeCriteria,
	setStudentSessionInFocus,
	setSessionBatchInfocus,

	setSessionCheckInFocus,
	setStudentSessionCheckInFocus,

	closeBatchSessionFocus,
	closeStudentSessionFocus,
	closeSessionCheckFocus,
	closeStudentSesssionCheckFocus,
} = classAttendanceSlice.actions;
