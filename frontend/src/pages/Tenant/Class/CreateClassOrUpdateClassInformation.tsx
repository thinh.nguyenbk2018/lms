import GeneralFileUploadField from "@components/CustomFields/FileUploadField/GeneralFileUploadField";
import {Col, Row, Space} from "antd";
import {Field, Formik} from "formik";
import {DatePicker, Form, Input, Select} from "formik-antd";
import {toNumber} from "lodash";
import {useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import * as Yup from "yup";
import BackButton from "../../../components/ActionButton/BackButton";
import DoneButton from "../../../components/ActionButton/DoneButton";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import CourseSearchSelectField from "../../../components/CustomFields/CourseSearchSelectField/CourseSearchSelectField";
import IMAGE_CONSTANTS from "../../../constants/DefaultImage";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";
import API_ERRORS from "../../../redux/features/ApiErrors";
import {
    useCreateClassMutation,
    useGetClassInformationQuery,
    useUpdateClassInformationMutation,
} from "@redux/features/Class/ClassAPI";
import {ClassType, getKeyClassType, initialClassDto,} from "@redux/features/Class/ClassType";
import SectionHeader from "../../../components/SectionHeader/SectionHeader";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import {chapterSchema} from "../Courses/CourseDetail/CreateOrUpdateCoursePage1";
import './CreateClassOrUpdateClassInformation.less'

const ClassSchema = Yup.object().shape({
    name: Yup.string().required("Tên là thông tin bắt buộc"),
    code: Yup.string().required("Mã là thông tin bắt buộc"),
    courseId: Yup.number().required("Khóa học là thông tin bắt buộc"),
    startedAt: Yup.date().test((value) => {
        return value != null;
    }),
    endedAt: Yup.date().nullable()
        .when(
            "startedAt",
            (startedAt, schema) =>
                schema == undefined ||
                (startedAt &&
                    schema.min(
                        startedAt,
                        "Ngày kết thúc phải lớn hơn ngày bắt đầu")))
        .when(
            "status",
            (status, schema) => status == "ENDED" ? schema.required("Ngày kết thúc là thông tin bắt buộc khi kết thúc lớp học") : schema),
    chapters: Yup.array().of(chapterSchema)
});

const CreateClassOrUpdateClassInformation = () => {
    const {id, courseId} = useParams();

    const navigate = useNavigate();

    const [updateClassInformation] = useUpdateClassInformationMutation();
    const [createClass] = useCreateClassMutation();
    const [showFileBrowser, setShowFileBrowser] = useState(false);


    const isSkip = (idParam: string | undefined) => {
        return isNaN(Number(idParam));
    };

    const {data, isError, error} = useGetClassInformationQuery(toNumber(id), {
        skip: isSkip(id),
    });

    const getTitleOfPage = () => {
        return id ? "Thông tin lớp học" : "Tạo lớp học mới";
    };

    const getTitleOfButton = () => {
        return id ? "Cập nhật" : "Tạo mới";
    };

    return (
        <>
            <Formik
                enableReinitialize={true}
                validateOnChange={false}
                initialValues={
                    data
                        ? {
                            ...data,
                            course: data.course?.id,
                            courseId: data.course?.id,
                        }
                        : courseId ? {...initialClassDto, courseId: Number(courseId)} : initialClassDto
                }
                onSubmit={(values, actions) => {
                    const dataToBeSent = {
                        name: values.name,
                        code: values.code,
                        avatar: values.avatar,
                        status: values.status,
                        type: values.type,
                        startedAt: values.startedAt,
                        endedAt: values.endedAt,
                        courseId: values.courseId,
                    };
                    if (id) {
                        const classInformationUpdated = {
                            ...dataToBeSent,
                            id: toNumber(id),
                        };

                        updateClassInformation({
                            classInfo: classInformationUpdated,
                            classId: toNumber(id),
                        })
                            .unwrap()
                            .then((res) => {
                                AntdNotifier.success("Cập nhật thông tin lớp học thành công", "", 1);
                                actions.setSubmitting(false);
                            })
                            .catch((err) => {
                                AntdNotifier.error(err.data.message, "", 1);
                                if (err.data.code === API_ERRORS.CLASS.CLASS_NOT_FOUND) {
                                    navigate(ROUTING_CONSTANTS.CLASS_LIST);
                                }
                                actions.setSubmitting(false);
                            });
                    } else {
                        createClass(dataToBeSent)
                            .unwrap()
                            .then((res) => {
                                AntdNotifier.success("Tạo lớp học thành công", "", 1);
                                navigate(ROUTING_CONSTANTS.CLASS_LIST);
                                actions.setSubmitting(false);
                            })
                            .catch((err) => {
                                AntdNotifier.error(err.data.message, "", 1);
                                actions.setSubmitting(false);
                            })
                    }
                }}
                validationSchema={ClassSchema}
            >
                {({
                      isValid,
                      isSubmitting,
                      dirty,
                      values,
                      setFieldValue,
                      errors,
                      submitForm,
                      handleSubmit,
                  }) => (
                    <>
                        <ContentHeader
                            title={getTitleOfPage()}
                            action={
                                <Space>
                                    <BackButton
                                        type="primary"
                                        danger
                                        onClick={() => {
                                            navigate(-1);
                                        }}
                                        className={"back-btn"}
                                    />
                                    <DoneButton
                                        type="primary"
                                        onClick={() => handleSubmit()}
                                        className={"update-btn"}
                                        disabled={!dirty || isSubmitting}
                                        loading={isSubmitting}
                                        title={getTitleOfButton()}
                                    />
                                </Space>
                            }
                        />

                        <SectionHeader title={"Thông tin lớp  học"}/>

                        <Form layout={"vertical"}>
                            <Row
                                justify="center"
                                className={"new-class-form__banner-row"}
                            >
                                <Col span={24}>
                                    <div className={"class-image-container"}>
                                        <img
                                            className={"class-banner-image"}
                                            src={
                                                values.avatar ||
                                                IMAGE_CONSTANTS.DEFAULT_BACKGROUND_IMAGE
                                            }
                                            alt={"Ảnh nền"}
                                        />
                                    </div>
                                </Col>
                                <Field
                                    name={"avatar"}
                                    component={GeneralFileUploadField}
                                    fast={true}
                                    showPreview={false}
                                    uploadButtonMessage={"Chọn ảnh nền khác"}
                                    allowedType={["jpg", "png", "jpge"]}
                                />
                            </Row>
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Form.Item
                                        label="Tên"
                                        name="name"
                                        required={true}
                                    >
                                        <Input name={"name"}/>
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item
                                        label="Mã (duy nhất)"
                                        name="code"
                                        required={true}
                                    >
                                        <Input
                                            name={"code"}
                                            disabled={!isNaN(Number(id))}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    {/* // TODO: HOW TO CREATE A CLASS */}
                                    <Form.Item
                                        className={"course"}
                                        name="courseId"
                                        label="Khóa học"
                                        required={true}
                                    >
                                        <Field
                                            name={"courseId"}
                                            component={(props: any) => (
                                                <CourseSearchSelectField {...props} disabled={!isNaN(Number(id))}/>)}
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col span={12}>
                                    <Form.Item
                                        className={"startedAt"}
                                        name="startedAt"
                                        label="Ngày bắt đầu"
                                    >
                                        <DatePicker
                                            className={"startedAt-date-picker"}
                                            name="startedAt"
                                            onChange={(value) => {
                                                setFieldValue(
                                                    "startedAt",
                                                    value ? value : undefined,
                                                    true
                                                );
                                            }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        className={`endedAt`}
                                        name="endedAt"
                                        label="Ngày kết thúc"
                                        required={values["status"] == "ENDED"}
                                    >
                                        <DatePicker
                                            className={"startedAt-date-picker"}
                                            name="endedAt"
                                            onChange={(value) => {
                                                setFieldValue(
                                                    "endedAt",
                                                    value ? value : undefined,
                                                    true
                                                );
                                            }}
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col span={16}>
                                    <Form.Item
                                        className={"type"}
                                        name="type"
                                        label="Hình thức"
                                    >
                                        <Select
                                            name="type"
                                            defaultActiveFirstOption
                                        >
                                            <Select.Option
                                                value={getKeyClassType(
                                                    ClassType.OFFLINE
                                                )}
                                            >
                                                {"Offline"}
                                            </Select.Option>
                                            <Select.Option
                                                value={getKeyClassType(
                                                    ClassType.ONLINE
                                                )}
                                            >
                                                {"Online"}
                                            </Select.Option>
                                            <Select.Option
                                                value={getKeyClassType(
                                                    ClassType.HYBRID
                                                )}
                                            >
                                                {"Kết hợp"}
                                            </Select.Option>
                                        </Select>
                                    </Form.Item>
                                </Col>
                                <Col span={8}>
                                    <Form.Item
                                        className={"status"}
                                        name="status"
                                        label="Trạng thái"
                                    >
                                        <Select
                                            name="status"
                                        >
                                            <Select.Option value={"CREATED"}>
                                                {"Mới mở"}
                                            </Select.Option>
                                            <Select.Option value={"ONGOING"}>
                                                {"Đang diễn ra"}
                                            </Select.Option>
                                            {!Number.isNaN(Number(id)) && <Select.Option value={"ENDED"}>
                                                {"Đã kết thúc"}
                                            </Select.Option>}
                                        </Select>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </>
                )}
            </Formik>
        </>
    );
};

export default CreateClassOrUpdateClassInformation;
