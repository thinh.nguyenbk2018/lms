import { Col, Row } from "antd";
import { AttendanceStatistics } from "../../../../../redux/features/Class/ClassCheckin/ClassAttendanceAPI";
import "./AttendanceStatisticsDisplay.less";
const AttendanceStatisticsDisplay = (
	props: AttendanceStatistics & { numberOfStudent: number }
) => {
	const {
		averageAttendance,
		numOfStudentWithAboveAverageAttendance,
		numOfStudentWithBelowAverageAttendance,
		numOfStudentWithPerfectAttendance,
		numberOfStudent = 1,
	} = props;
	const ratePerfect =
		(100 * numOfStudentWithPerfectAttendance) / numberOfStudent;
	const rateBelow =
		(100 * numOfStudentWithBelowAverageAttendance) / numberOfStudent;
	const rateAbove =
		(100 * numOfStudentWithAboveAverageAttendance) / numberOfStudent;
	return (
		<div className="statistics-container">
			<div className="average-attendance-info-container">
				<p className="average-attendance-rate-number">{averageAttendance?.toFixed(2)}</p>
				<p className="average-attendance-rate-explanation">
					{"Tỉ lệ tham gia buổi học"}
				</p>
			</div>
			<div className="info-card">
				<p className={"main-number"}>{numOfStudentWithPerfectAttendance}</p>
				<div className={"info-detail"}>
					<p className={"info-explanation"}>
						{"Học viên tham gia toàn bộ buổi học"}
					</p>
					<p
						className={"info-with-rate"}
					>{`${ratePerfect?.toFixed(2)}% Tổng số học viên`}</p>
				</div>
			</div>
			<div className="info-card">
				<p className={"main-number"}>
					{numOfStudentWithAboveAverageAttendance}
				</p>
				<div className={"info-detail"}>
					<p className={"info-explanation"}>
						{"Học viên tham gia buổi học nhiều hơn/bằng trung bình"}
					</p>
					<p
						className={"info-with-rate"}
					>{`${rateAbove?.toFixed(2)}% Tổng số học viên`}</p>
				</div>
			</div>
			<div className="info-card">
				<p className={"main-number"}>
					{numOfStudentWithBelowAverageAttendance}
				</p>
				<div className={"info-detail"}>
					<p className={"info-explanation"}>
						{"Học viên tham gia ít buổi học hơn trung bình "}
					</p>
					<p
						className={"info-with-rate"}
					>{`${rateBelow?.toFixed(2)}% Tổng số học viên`}</p>
				</div>
			</div>
		</div>
	);
};

export default AttendanceStatisticsDisplay;
