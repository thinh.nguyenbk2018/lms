import {Form, FormItem, Input} from "formik-antd";
import React from "react";
import {QuestionViewAndTakingTestFormProps} from "../interface/question.interface";
import {FillInBlankQuestion} from "../../../redux/features/Quiz/CreateQuizSlice";
import {FormikProps, useFormikContext, withFormik} from "formik";
import {DEFAULT_TEXT_EDITOR_HTML} from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import * as Yup from "yup";
import {DEFAULT_FRONTEND_ID} from "../../../redux/interfaces/types";
import {QuestionAnswer} from "../../../redux/features/Quiz/examAPI";
import {
	DND_BLANK_SPAN_TAG_CLASS,
	replaceBlankWithSpecialSpan,
} from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import ReactHtmlParser from "html-react-parser";
import {Card, Row} from "antd";
import {baseEditQuestionYupSchema} from "../QuestionContainerItem/baseQuestionSchema";
import QuestionSubmissionButton from "../../ActionButton/QuestionSubmissionButton";

const counterFunction = () => {
	let init = 1;
	const increaseCount = () => {
		return init++;
	};
	return { increaseCount };
};

function* newValueGenerator() {
	let i = 1;
	while (true) {
		yield i++;
	}
}

const BlankInput = ({ index }: { index: number }) => {
	const {
		values,
		errors,
		touched,
		isValid,
		setFieldTouched,
		setFieldError,
		setFieldValue,
		handleSubmit

	} = useFormikContext<QuestionAnswer>();

	const handleBlur = (x: any) => {
		handleSubmit();
	}

	return (
		<>
			<FormItem name={`answers[${index}]`}>
				<Input name={`answers[${index}]`} onBlur={handleBlur}/>
			</FormItem>
		</>
	);
};
const renderQuestionAnswerBox = (questionHTML: string) => {
	const [questionReviewHTML, count] = replaceBlankWithSpecialSpan(questionHTML);
	let blankCounter = 0;

	return ReactHtmlParser(questionReviewHTML, {
		replace: (node) => {
			if (
				(node as any).name === "span" &&
				(node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
			) {
				blankCounter++;
				return <BlankInput index={blankCounter - 1} />;
			}
			return node; 
		},
	});
};

// * Check if this persion have got the permission to edit the question / test content
const checkIfCanEdit = () => {
	return true;
};
/** Display the test for review the question content and possibly retrieve the answer */
const WrappedFillInBlankDisplay = (
	props: FillInBlankViewHocProps & FormikProps<QuestionAnswer>
) => {
	// * Question info to display the requirements
	const { description, blankList } = props;

	// * Form State from formik
	const {
		values,
		dirty,
		touched,
		isValid,
		handleSubmit,
		setFieldValue,
		errors,
		handleBlur
	} = props;

	return (
		<>
			<Form>
				<Card>
					{renderQuestionAnswerBox(description)}
					{/*<Row style={{ margin: "1em 0" }} justify="end">*/}
					{/*	<QuestionSubmissionButton*/}
					{/*		onClick={() => handleSubmit()}*/}
					{/*		title={"Gửi đáp án"}*/}
					{/*	/>*/}
					{/*</Row>*/}
				</Card>
			</Form>
		</>
	);
};

// * Question configuration props with Formik HOC
const fillInBlankAnswerSchema = Yup.object({
	answers: Yup.array().of(
		Yup.string().required("Câu trả lời cho một ô trống không được để trống")
	),
}).concat(baseEditQuestionYupSchema);

type FillInBlankViewHocProps =
	QuestionViewAndTakingTestFormProps<FillInBlankQuestion>;
/** A question displayer need the Partial<QuestionDataType> to display the question statement
 *  ? QuestionAnswer to record student choice quen taking the test
 *
 * */
const FillInBlankDisplayWithForm = withFormik<
	FillInBlankViewHocProps,
	QuestionAnswer
>({
	mapPropsToValues: (props) => {
		return {
			id: props.id || DEFAULT_FRONTEND_ID,
			description: props.description || DEFAULT_TEXT_EDITOR_HTML,
			point: props.point || 0,
			blankList: props.blankList || [],
			answers: props.blankList?.map(b => b?.studentAnswer || "") || [],
			optionalStudentNote: "",
		};
	},
	// validationSchema: fillInBlankAnswerSchema,
	handleSubmit: (values, formikBag) => {
		const valuesToSubmit: QuestionAnswer = {
			...values,
		};
		if (values.answers.filter(x => x !== "").length != 0) {
			formikBag.props.onConfirmSubmit?.(valuesToSubmit);
		}
		// console.log("FIB_Question answer values is: ", values);
	}
})(WrappedFillInBlankDisplay);
export default FillInBlankDisplayWithForm;
