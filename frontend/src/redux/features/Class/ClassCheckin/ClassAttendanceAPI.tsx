import {DatePresentation} from "@components/Calendar/Utils/CalendarUtils";
import {ID} from "@redux/interfaces/types";
import {apiSlice} from "../../CentralAPI";
import {SessionAttendanceCheckID} from "./ClassAttendanceSlice";

const classAttendanceApiWithInvalidateTags = apiSlice.enhanceEndpoints({
	addTagTypes: ["ClassAttendance", "ClassMeetingAttendance"],
});

// * NONE --> Not marked yet by the instructor in charge
export type AttendanceState = "PRESENT" | "ABSENT" | "LATE" | "NONE";
export type SessionOverallAttendanceRateStrategy = "ANY" | "LAST_TIME";

export type AttendanceStatistics = {
	averageAttendance: number;
	numOfStudentWithPerfectAttendance: number;
	numOfStudentWithAboveAverageAttendance: number;
	numOfStudentWithBelowAverageAttendance: number;
};

export type StudentAttendanceInfo = {
	name: string;
	avatar: string;
	state: AttendanceState;
	attendanceRate: number;
};

export type StudentAttendanceRecord = {
	[K: ID]: StudentAttendanceInfo;
};

export type AttendanceDateMeta = {
	id?: ID;
	startedAt: Date;
	finishedAt: Date;
	strategy: SessionOverallAttendanceRateStrategy;
	note?: string;
	room?: string;
};
export type AttendanceDate = {
	[SesssionDate: string]: {
		metaInfo: AttendanceDateMeta;
		studentAttendance: StudentAttendanceRecord;
	};
};

export type OverallAttendance = {
	statistics: AttendanceStatistics;
	attendanceInfo: AttendanceDate;
	presentCount: { [key: string]: number };
	happenedSession: number;
};

export type AttendanceCheckMetaData = {
	id: ID;
	createdAt?: DatePresentation;
	updatedAt?: DatePresentation;
};

export type AttendanceCheck = {
	metaInfo: AttendanceCheckMetaData;
	studentAttendance: StudentAttendanceRecord;
};

export type AttendanceChecks = {
	[checkTime: string]: AttendanceCheck;
};

export type MeetingAttendance = {
	metaInfo: AttendanceDateMeta;
	attendanceInfo: AttendanceChecks;
};
// * Each row need student info with his/her checkin record
export type StudentBrieftInfo = Pick<
	StudentAttendanceInfo,
	"name" | "avatar"
> & { studentId: ID };
type StudentListFromDateExtractor = (
	date: StudentAttendanceRecord
) => StudentBrieftInfo[];
export const getStudentListFromAttendanceDate: StudentListFromDateExtractor = (
	data
) => {
	const studentDataList: StudentBrieftInfo[] = [];
	for (const studentID in data) {
		const student = data[studentID];
		studentDataList.push({
			...student,
			studentId: Number(studentID),
		});
	}
	return studentDataList;
};

type AttendanceStateListFromDateExtractor = (
	date: StudentAttendanceRecord
) => AttendanceState[];

export const getAttendanceListFromAttendanceDate: AttendanceStateListFromDateExtractor =
	(date) => {
		const stateList: AttendanceState[] = [];
		for (const studentID in date) {
			const student = date[studentID];
			stateList.push(student.state);
		}
		return stateList;
	};

export type AttendanceOverallViewTableBuilder = (
	overallData: OverallAttendance
) => [];

const classClassAttendanceApi =
	classAttendanceApiWithInvalidateTags.injectEndpoints({
		endpoints: (build) => ({
			// * Overall Stuffs
			getClassAttendances: build.query<OverallAttendance, { classId: number }>({
				query: ({ classId }) => `classes/${classId}/overall_attendance`,
				providesTags: [{ type: "ClassAttendance", id: "LIST" }],
			}),
			changeStudentAttendance: build.mutation<
				void,
				{ classId: ID; studentId: ID; newState: AttendanceState; sessionId: ID }
			>({
				query: ({ classId, studentId, newState, sessionId }) => {
					return {
						method: "POST",
						url: `/classes/${classId}/attendance/session/${sessionId}/students/${studentId}`,
						body: {
							state: newState,
						},
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassAttendance", id: args.sessionId },
					{ type: "ClassAttendance", id: "LIST" },
				],
			}),
			changeStudentAttendanceBatchByDate: build.mutation<
				void,
				{ classId: ID; sessionId: ID; batchState: AttendanceState }
			>({
				query: ({ classId, sessionId, batchState }) => {
					return {
						method: "POST",
						url: `classes/${classId}/attendance/session/${sessionId}/batch`,
						body: {
							state: batchState,
						},
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassAttendance", id: args.sessionId },
					{ type: "ClassAttendance", id: "LIST" },
				],
			}),
			// * Single Session Stuffs: Single Meeting Attendance =  N X Single Attendance Check
			getAttendanceByDate: build.query<
				MeetingAttendance,
				{
					classId: ID;
					sessionId: ID;
				}
			>({
				query: ({ classId, sessionId }) =>
					`classes/${classId}/attendance/session/${sessionId}`,
				providesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),

			addNewAttendanceCheck: build.mutation<
				AttendanceChecks,
				{ classId: ID; sessionId: ID }
			>({
				query: ({ classId, sessionId }) => {
					return {
						url: `classes/${classId}/attendance/${sessionId}`,
						method: "POST",
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),
			changeMeetingAttendanceBatch: build.mutation<
				AttendanceChecks,
				{
					classId: ID;
					sessionId: ID;
					checkTime: SessionAttendanceCheckID;
					state: AttendanceState;
				}
			>({
				query: ({ classId, sessionId, checkTime, state }) => {
					return {
						url: `classes/${classId}/attendance/session/${sessionId}/time/${checkTime}/batch`,
						method: "POST",
						body: {
							state,
						},
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),
			putNoteToSession: build.mutation<
				void,
				{ classId: ID; sessionId: ID; note: string }
			>({
				query: ({ classId, sessionId, note }) => {
					return {
						url: `classes/${classId}/attendance/session/${sessionId}/note`,
						body: {
							note
						},
						method: "POST",
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),
			changeMeetingAttendanceSingle: build.mutation<
				AttendanceChecks,
				{
					classId: ID;
					sessionId: ID;
					checkTime: SessionAttendanceCheckID;
					studentId: ID;
					state: AttendanceState;
				}
			>({
				query: ({ classId, sessionId, checkTime, studentId, state }) => {
					return {
						url: `classes/${classId}/attendance/session/${sessionId}/time/${checkTime}/students/${studentId}`,
						method: "POST",
						body: {
							state,
						},
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),
			changeSessionAttendanceRateCalculationStrategy: build.mutation<
				void,
				{
					classId: ID;
					sessionId: ID;
					newStrategy: SessionOverallAttendanceRateStrategy;
				}
			>({
				query: ({ classId, sessionId, newStrategy }) => {
					return {
						url: `classes/${classId}/attendance/session/${sessionId}/strategy`,
						method: "POST",
						body: {
							strategy: newStrategy,
						},
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),
			deleteMeetingAttendanceBatch: build.mutation<
				AttendanceChecks,
				{ classId: ID; sessionId: ID; checkTime: ID }
			>({
				query: ({ classId, sessionId, checkTime }) => {
					return {
						url: `classes/${classId}/attendance/session/${sessionId}/time/${checkTime}`,
						method: "DELETE",
					};
				},
				invalidatesTags: (_res, _err, args) => [
					{ type: "ClassMeetingAttendance", id: args.sessionId },
				],
			}),
			refetchOverallAttendance: build.mutation<any,  void>({
				queryFn: () => ({ data: null }),
				invalidatesTags: [{type: "ClassAttendance", id: "LIST"}]
			})
		}),
	});

export const {
	// * Class overall attendance management
	useGetClassAttendancesQuery,
	useChangeStudentAttendanceMutation,
	useChangeStudentAttendanceBatchByDateMutation,
	// * Session Detailed attendance management
	useGetAttendanceByDateQuery,
	useAddNewAttendanceCheckMutation,
	useChangeMeetingAttendanceBatchMutation,
	useChangeMeetingAttendanceSingleMutation,
	useDeleteMeetingAttendanceBatchMutation,

	useChangeSessionAttendanceRateCalculationStrategyMutation,
	usePutNoteToSessionMutation,
	useRefetchOverallAttendanceMutation
} = classClassAttendanceApi;
