import { renderWithProviders } from "@mocks/utils/test-utils";
import TodoListWithRedux from "./TodoListWithRedux";
import { screen, fireEvent } from "@testing-library/react";

test("todo with redux behavior", async () => {
    renderWithProviders(<TodoListWithRedux />);

    expect(screen.getByText(/Number of todos are: 0/i)).toBeInTheDocument();
});
 
test("todo with redux behavior: Add new todo should increase count", async () => {
    renderWithProviders(<TodoListWithRedux />);

    expect(screen.getByText(/Number of todos are: 0/i)).toBeInTheDocument(); 
    fireEvent.click(screen.getByRole('button', {name: /add todo/i})) 
    expect(screen.getByText(/Number of todos are: 1/i)).toBeInTheDocument(); 
});

test("todo with redux toolkit query and msw", async () => { 
    
});