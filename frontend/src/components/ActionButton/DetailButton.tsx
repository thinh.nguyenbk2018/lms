import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, ButtonProps } from "antd";
import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";

const DetailButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			type={"primary"}
			icon={
				<FontAwesomeIcon
					icon={faPenToSquare}
					style={{ margin: "0 .5em 0 0" }}
				/>
			}
		>
			{`${props.title || "Chi Tiết "}`}
		</Button>
	);
};

export default DetailButton;
