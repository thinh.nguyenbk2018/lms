import { renderWithProviders } from "@mocks/utils/test-utils"
import TodoListWithRTKQuery from "./TodoListWithRTKQuery"
import { screen } from '@testing-library/react';

test('Initially we render loading', async () => { 
    renderWithProviders(<TodoListWithRTKQuery/>); 

    expect(screen.getByText(/loading.../i)).toBeInTheDocument();  
    
    expect(await screen.findByText(/number of todos are: /i)).toBeInTheDocument(); 
    // * If need verification that the todos are infact loaded
    screen.debug(); 
}); 