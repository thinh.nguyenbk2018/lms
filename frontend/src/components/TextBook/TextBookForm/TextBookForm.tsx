import {Field} from "formik";
import {FormItem, Input, Form} from "formik-antd";
import * as Yup from "yup";
import {TextBook} from "@redux/features/Textbook/TextbookType";
import {
	FORM_DEFAULT_MESSAGES,
	FormNamespace,
	getFieldNameWithNamespace,
	LabelForFormFields,
} from "@utils/FormUtils/formUtils";
import GeneralFileUploadField from "../../CustomFields/FileUploadField/GeneralFileUploadField";

const TEXT_BOOK_FIELD = {
	NAME: "name",
	AUTHOR: "author",
	ATTACHMENT: "attachment",
	DESCRIPTION: "description",
};

const textBookValidationSchema = Yup.object({
	[TEXT_BOOK_FIELD.NAME]: Yup.string().required(FORM_DEFAULT_MESSAGES.REQUIRED),
	[TEXT_BOOK_FIELD.AUTHOR]: Yup.string().required(
		FORM_DEFAULT_MESSAGES.REQUIRED
	),
	[TEXT_BOOK_FIELD.ATTACHMENT]: Yup.string(),
	[TEXT_BOOK_FIELD.DESCRIPTION]: Yup.string(),
});

export const textBookInitialValues: Partial<TextBook> = {
	name: "",
	author: "",
	attachment: "",
	description: "",
};
type TextBookFormProps = {
	namespace: FormNamespace;
	labels: LabelForFormFields<typeof TEXT_BOOK_FIELD>;
};

const TextBookForm = (props: TextBookFormProps) => {
	const { namespace, labels } = props;
	const getFieldName = (name: string) => {
		return getFieldNameWithNamespace(namespace, name);
	};
	const NAME_FIELD = getFieldName(TEXT_BOOK_FIELD.NAME);
	const AUTHOR_FIELD = getFieldName(TEXT_BOOK_FIELD.AUTHOR);
	const ATTACHMENT_FIELD = getFieldName(TEXT_BOOK_FIELD.ATTACHMENT);
	const DESCRIPTION_FIELD = getFieldName(TEXT_BOOK_FIELD.DESCRIPTION);
	return (
		<>
			<Form layout="vertical">
				<FormItem required={true} name={NAME_FIELD} label={labels.NAME}>
					<Input name={NAME_FIELD} />
				</FormItem>
				<FormItem name={AUTHOR_FIELD} label={labels.AUTHOR}>
					<Input name={AUTHOR_FIELD} />
				</FormItem>
				<FormItem name={DESCRIPTION_FIELD} label={labels.DESCRIPTION}>
					<Input name={DESCRIPTION_FIELD} />
				</FormItem>
				<FormItem name={ATTACHMENT_FIELD} label={labels.ATTACHMENT}>
					{/* TODO: add prefix so the files will be saved on firebase as: /textbooks/... */}
					<Field
						name={TEXT_BOOK_FIELD.ATTACHMENT}
						placeholder={""}
						component={GeneralFileUploadField}
						showImagePreviewInsteadOfIcon={false}
						showPreview={true}
					/>
				</FormItem>
			</Form>
		</>
	);
};

export default TextBookForm;
