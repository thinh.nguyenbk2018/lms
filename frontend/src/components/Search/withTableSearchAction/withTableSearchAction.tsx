import { Button, Col, Row, Space } from "antd";
import { Formik } from "formik";
import { Form, Input } from "formik-antd";
import _ from "lodash";
import React from "react";
import useDebounced from "../../../hooks/Debounced/useDebounced";
import { useAppDispatch } from "../../../hooks/Redux/hooks";
import DEFAULT_PAGE_SIZE from "../../../redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import { searchIconSvg, reloadIconSvg } from "../../Icons/SvgIcons";
import { TableSearchProps } from "../TableSearch";

type PropsAreEqual<P> = (
	prevProps: Readonly<P>,
	nextProps: Readonly<P>
) => boolean;

const withTableSearchAction = <P extends TableSearchProps>(
	component: {
		(props: P): Exclude<React.ReactNode, undefined>;
		displayName?: string;
	},
	propsAreEqual?: PropsAreEqual<P> | false,

	componentName = component.displayName ?? component.name
): {
	(props: P): JSX.Element;
	displayName: string;
} => {
	function WithTableSearchHoc(props: P) {
		//Do something special to justify the HoC.

		const { searchCriteria, changeCriteria, searchFields, defaultFilters } =
			props;
		const dispatch = useAppDispatch();
		const debouncedCriteriaChange = useDebounced((criteria) =>
			dispatch(changeCriteria(criteria))
		);

		return (
			<>
				<Formik
					initialValues={{ keyword: "" }}
					onSubmit={(values, actions) => {
						// do nothing here because we already handle it in onChange event of input field
					}}
				>
					{({
						isValid,
						isSubmitting,
						dirty,
						values,
						setFieldValue,
						submitForm,
					}) => (
						<Form>
							<Row>
								{searchFields.map((field) => (
									<Col span={12}>
										<Form.Item name={field.key}>
											<Input
												prefix={searchIconSvg}
												placeholder={"Nhập để tìm kiếm"}
												name={"keyword"}
												onChange={(e) => {
													const keyword = e.target.value;
													const criteria = _.cloneDeep(searchCriteria);
													criteria.keyword = keyword;
													debouncedCriteriaChange(criteria);
												}}
											/>
										</Form.Item>
									</Col>
								))}
								<Col span={4} offset={1}>
									<Button
										className={"search-btn"}
										type="primary"
										icon={reloadIconSvg}
										danger
										onClick={() => {
											setFieldValue("keyword", "");
											const criteria = {
												keyword: "",
												sort: [],
												filter: defaultFilters,
												pagination: {
													page: 1,
													size: DEFAULT_PAGE_SIZE,
												},
											};
											debouncedCriteriaChange(criteria);
										}}
										// disabled={!dirty || !isValid}
										loading={isSubmitting}
									>
										<span>Xóa bộ lọc</span>
									</Button>
								</Col>
								{component(props) as JSX.Element}
							</Row>
						</Form>
					)}
				</Formik>
			</>
		);
	}

	WithTableSearchHoc.displayName = `withTableSearchActionHoC(${componentName})`;

	let wrappedComponent =
		propsAreEqual === false
			? WithTableSearchHoc
			: React.memo(WithTableSearchHoc, propsAreEqual);

	return wrappedComponent as typeof WithTableSearchHoc;
};

export default withTableSearchAction;
