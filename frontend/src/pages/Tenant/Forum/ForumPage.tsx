import React from 'react';
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import {Button, Input, Space} from "antd";
import IconSelector from "../../../components/Icons/IconSelector";
import ForumThreadsList from "../../../components/Forum/ForumThreadList";
import {useNavigate} from "react-router-dom";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";


const ForumPage = () => {
    const navigate = useNavigate();
    const onAddNewThread = () => {
        navigate(ROUTING_CONSTANTS.NEW_FORUM_THREAD);
    }
    return (
        <>
            <ContentHeader
                title={"Diễn đàn môn học"}
                action={
                    <Space>
                        <Input
                            placeholder="Nhập vào để search topic"
                            allowClear={true}
                            prefix={<IconSelector type="SearchOutlined"/>}
                        />
                        <Space onClick={() => console.log("hello")}>
                            <IconSelector type="CaretDownOutlined"/>
                        </Space>
                        <Button type="primary" onClick={onAddNewThread}>Thêm</Button>
                    </Space>
                }
            />
            <ForumThreadsList/>
        </>
    );
};

export default ForumPage;
