"use strict";
exports.__esModule = true;
var react_1 = require("react");
var antd_1 = require("antd");
var DropDownMenu_1 = require("../../DropDownMenu/DropDownMenu");
var IconSelector_1 = require("../../Icons/IconSelector");
var icons_1 = require("@ant-design/icons");
require("./ConversationBox.less");
var hooks_1 = require("../../../hooks/Redux/hooks");
var AuthSlice_1 = require("../../../redux/features/Auth/AuthSlice");
var ChatSlice_1 = require("../../../redux/features/Chat/ChatSlice");
var ChatAPI_1 = require("../../../redux/features/Chat/ChatAPI");
var LoadingPage_1 = require("../../../pages/UtilPages/LoadingPage");
var ThemeColor_1 = require("./../../../constants/ThemeColor");
var DefaultImage_1 = require("./../../../constants/DefaultImage");
var react_infinite_scroll_component_1 = require("react-infinite-scroll-component");
var MessageInputForm = function (_a) {
    var chatRoomId = _a.chatRoomId;
    var currentUser = hooks_1.useAppSelector(AuthSlice_1.authSelectors.selectCurrentUser);
    var _b = react_1.useState(""), message = _b[0], setMessage = _b[1];
    var _c = react_1.useState(0), caretPosition = _c[0], setCaretPostion = _c[1];
    var _d = react_1.useState(false), showPicker = _d[0], setShowPicker = _d[1];
    var onEmojiClick = function (event, emojiObject) {
        console.log(emojiObject);
        insertTextAtCaret(event, emojiObject.emoji);
    };
    var insertTextAtCaret = function (event, text) {
        var cursorPosition = caretPosition;
        var textBeforeCursorPosition = message.substring(0, cursorPosition);
        var textAfterCursorPosition = message.substring(cursorPosition, message.length);
        setMessage(textBeforeCursorPosition + text + textAfterCursorPosition);
    };
    var togglePicker = function () {
        setShowPicker(!showPicker);
    };
    var onSendMessage = function () {
        console.log("Sending message: ", message);
        // * Send the message to server
        // * Receive response
        // * Append to message list if success
        var messageSent = {
            chatRoomId: chatRoomId,
            content: message,
            senderId: currentUser === null || currentUser === void 0 ? void 0 : currentUser.id
        };
        //!! StompUtils.publishToTopic("/chat/send", JSON.stringify(messageSent));
    };
    var handleInputChange = function (e) {
        setMessage(e.target.value);
        setCaretPostion(e.target.selectionStart || e.target.selectionEnd || e.target.value.length);
    };
    var handleInputClick = function (e) {
        setCaretPostion(e.target.selectionStart);
    };
    return (react_1["default"].createElement(react_1["default"].Fragment, null,
        react_1["default"].createElement(antd_1.Row, { justify: "end", align: "middle" },
            react_1["default"].createElement(antd_1.Col, { span: 4, style: { backgroundColor: "white" } },
                react_1["default"].createElement(antd_1.Popconfirm, { placement: "top", title: react_1["default"].createElement(react_1["default"].Fragment, null), onConfirm: function () { return setShowPicker(false); } },
                    react_1["default"].createElement(IconSelector_1["default"], { type: "SmileOutlined", onClick: togglePicker })),
                react_1["default"].createElement(IconSelector_1["default"], { type: "PlusCircleOutlined" })),
            react_1["default"].createElement(antd_1.Col, { span: 18 },
                react_1["default"].createElement(antd_1.Form, { name: "customized_form_controls", layout: "inline", onFinish: function (values) { return onSendMessage(); }, initialValues: {
                        message: ""
                    } },
                    react_1["default"].createElement(antd_1.Space, null,
                        react_1["default"].createElement(antd_1.Input, { name: "message", size: "large", value: message, onChange: handleInputChange, onClick: handleInputClick }),
                        react_1["default"].createElement(antd_1.Form.Item, null,
                            react_1["default"].createElement(antd_1.Button, { type: "primary", htmlType: "submit", size: "large" }, "Submit"))))))));
};
// * Floating conversation
var ConversationBox = function (props) {
    var id = props.id, state = props.state;
    var _a = react_1.useState(true), visible = _a[0], setVisible = _a[1];
    var currentUser = hooks_1.useAppSelector(AuthSlice_1.authSelectors.selectCurrentUser);
    var dispatch = hooks_1.useAppDispatch();
    var _b = react_1.useState(1), page = _b[0], setPage = _b[1];
    var _c = react_1.useState(10), size = _c[0], setSize = _c[1];
    var _d = ChatAPI_1.useGetActiveChatMessagesQuery({ id: id, page: page, size: size }), activeChatRoom = _d.data, isFetching = _d.isFetching, isSuccess = _d.isSuccess, isLoading = _d.isLoading, error = _d.error, isError = _d.isError, refetch = _d.refetch;
    var onClose = function () {
        dispatch(ChatSlice_1.hideChat(id));
    };
    var onMinimize = function () {
        dispatch(ChatSlice_1.minimizeChat(id));
    };
    var fetchOlderMessage = function () {
        setPage(page + 1);
    };
    var renderFunction = function () {
        var _a;
        if (!activeChatRoom) {
            return react_1["default"].createElement(LoadingPage_1["default"], null);
        }
        else {
            return react_1["default"].createElement(antd_1.Card, { title: react_1["default"].createElement(antd_1.Space, null,
                    react_1["default"].createElement(antd_1.Avatar, { src: (activeChatRoom === null || activeChatRoom === void 0 ? void 0 : activeChatRoom.image) || DefaultImage_1["default"].DEFAULT_AVATAR }),
                    react_1["default"].createElement(antd_1.Typography.Text, { strong: true, className: "chat-box-title" },
                        " ",
                        activeChatRoom.chatRoomName),
                    react_1["default"].createElement(DropDownMenu_1["default"], { props: [
                            {
                                itemDisplay: react_1["default"].createElement(antd_1.Typography.Text, null, " T\u1EAFt th\u00F4ng b\u00E1o "),
                                callback: function () { return console.log("MUTE NOTIFICATION FOR CHAT", id); }
                            },
                            {
                                itemDisplay: react_1["default"].createElement(antd_1.Typography.Text, null, " Ch\u1EB7n ng\u01B0\u1EDDi n\u00E0y "),
                                callback: function () { return console.log("MUTE NOTIFICATION FOR CHAT", id); }
                            },
                            {
                                itemDisplay: react_1["default"].createElement(antd_1.Typography.Text, null, " B\u00E1o x\u1EA5u "),
                                callback: function () { return console.log("MUTE NOTIFICATION FOR CHAT", id); }
                            }
                        ] })), extra: react_1["default"].createElement(antd_1.Space, null,
                    react_1["default"].createElement(antd_1.Tooltip, { title: "Minimize" },
                        react_1["default"].createElement(icons_1.MinusCircleTwoTone, { style: { fontSize: "2em" }, onClick: onMinimize })),
                    react_1["default"].createElement(antd_1.Tooltip, { title: "Close" },
                        react_1["default"].createElement(icons_1.CloseCircleTwoTone, { style: { fontSize: "2em" }, onClick: onClose }))), actions: [
                    react_1["default"].createElement(MessageInputForm, { chatRoomId: props.id })
                ], headStyle: { background: ThemeColor_1["default"].primary }, style: {
                    minWidth: "360px",
                    maxWidth: "500px",
                    zIndex: "9999",
                    display: visible ? "static" : "none",
                    maxHeight: "55vh"
                } },
                react_1["default"].createElement(antd_1.Row, { gutter: [0, 10], style: { height: "36vh", overflow: "auto" } },
                    react_1["default"].createElement("div", { id: "chat-scroll-div" },
                        react_1["default"].createElement(react_infinite_scroll_component_1["default"], { next: fetchOlderMessage, hasMore: page * size >= activeChatRoom.messages.total, loader: react_1["default"].createElement("p", null, "...Loading"), dataLength: activeChatRoom.messages.total, scrollableTarget: "chat-scroll-div" }, (_a = activeChatRoom === null || activeChatRoom === void 0 ? void 0 : activeChatRoom.messages) === null || _a === void 0 ? void 0 : _a.listData.map(function (item, index) {
                            return react_1["default"].createElement(antd_1.Col, { span: 24, flex: "space-between", className: 'chat-message' },
                                react_1["default"].createElement(antd_1.Row, { justify: item.senderId === (currentUser === null || currentUser === void 0 ? void 0 : currentUser.id) ? "end" : "start" },
                                    react_1["default"].createElement(antd_1.Tooltip, { title: new Date().toLocaleTimeString() },
                                        react_1["default"].createElement(antd_1.Card, { className: "chat-bubble " + (item.senderId === (currentUser === null || currentUser === void 0 ? void 0 : currentUser.id) ? "my-chat-message" : "other-chat-message"), hoverable: true, style: {
                                                maxWidth: 500,
                                                borderRadius: "1.5em"
                                            }, key: index, bodyStyle: { color: "white", wordWrap: "normal" } },
                                            react_1["default"].createElement(antd_1.Typography.Text, { style: { color: "white" } }, item.content)))));
                        })))));
        }
    };
    return (renderFunction());
};
exports["default"] = ConversationBox;
