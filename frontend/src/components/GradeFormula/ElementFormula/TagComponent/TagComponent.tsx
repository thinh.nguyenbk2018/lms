import { Select } from "antd";
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../../hooks/Redux/hooks";
import { gradeFormulaSeletor, Tag, updateFormula } from "../../../../redux/features/Grade/GradeFormulaSlice";

const TagComponent = ({ value, index }: { value: Tag, index: number }) => {
  let tags = useAppSelector(gradeFormulaSeletor.tagsSelector);
  let formula = useAppSelector(gradeFormulaSeletor.formulaSelector);
  const dispatch = useAppDispatch();

  const onChange = (tagId: number) => {
    let newFormula = [...formula.map(ele => ({ ...ele }))];
    newFormula[index].value = tags.find(tag => tag.id === tagId) as Tag;
    dispatch(updateFormula(newFormula));
  }

  const [open, setOpen] = useState(false);

  return <span onMouseDownCapture={(e) => e.stopPropagation()}>
    <Select
      showSearch
      optionFilterProp="children"
      defaultValue={value.id}
      value={value.id}
      style={{ display: "inline-block", minWidth: 100, textAlign: "center" }}
      filterOption={(input, option) =>
        option!.children!.toString().toLowerCase!().indexOf(input.toLowerCase()) >= 0
      }
      onClick={() => setOpen(!open)}
      open={open} onChange={onChange}>
      {tags.map((tag, index) => (
        <Select.Option key={index} value={tag.id}>
          {tag.title}
        </Select.Option>
      ))}
    </Select>
  </span>
}

export default TagComponent;