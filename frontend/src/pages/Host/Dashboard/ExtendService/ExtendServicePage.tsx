import { Button, Card, Col, Divider, Image, Radio, Row, Space, Typography, Spin } from "antd";
import moment from "moment";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import PackageComponent from "../../../../components/PackageComponent/PackageComponent";
import { useAppDispatch, useAppSelector } from "../../../../hooks/Redux/hooks";
import { useGetExpireTimeQuery, useGetPackagesQuery, useGetPaymentUrlMutation } from "../../../../redux/features/Host/ExtendService/extendServiceApi";
import { changePaymentMethod, changeSelected, extendServiceSeletor } from "../../../../redux/features/Host/ExtendService/extendServiceSlice";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import { format } from "date-fns";
import { parseDate } from "@components/Calendar/Utils/CalendarUtils";
import { vi } from "date-fns/locale";
import { useEffect } from "react";
import { useSearchParams } from 'react-router-dom';

const ExtendService = () => {
  let { data: getPackagesResponse, isLoading: isGettingPackages } = useGetPackagesQuery();
  let selected = useAppSelector(extendServiceSeletor.selected);
  let paymentMethod = useAppSelector(extendServiceSeletor.paymentMethod);

  let [getPaymentURL, { isLoading }] = useGetPaymentUrlMutation();

  let { data: getExpireTimeResponse, isLoading: isLoadingExpireTime } = useGetExpireTimeQuery();

  const dispatch = useAppDispatch();

  const onSubmit = () => {
    getPaymentURL({ gateway: paymentMethod, packageId: selected })
      .unwrap()
      .then(response => {
        window.location.assign(response.paymentUrl);
      })
      .catch(error => {
        AntdNotifier.error(error.data.message);
      })
  }

  const [searchParams] = useSearchParams();

  useEffect(() => {
    if (searchParams.get('vnp_TransactionStatus') !== null) {
      if (searchParams.get('vnp_TransactionStatus') === "00") {
        AntdNotifier.success("Giao dịch thành công. Dịch vụ của bạn sẽ sớm được gia hạn.");
      } else {
        AntdNotifier.error("Giao dịch không thành công");
      }
    }
  }, [searchParams])

  return <>
    <ContentHeader title="Gia hạn dịch vụ" />
    <Spin spinning={isGettingPackages}>
      <Row justify="center" align="middle">
        <div>
          <Row style={{ marginBottom: 20 }}>
            <Typography.Text>
              Ngày hết hạn dịch vụ: {(isLoadingExpireTime || getExpireTimeResponse == undefined) ? <Spin /> : format(parseDate(getExpireTimeResponse.expireTime), "PP, p", { locale: vi })}
            </Typography.Text>
          </Row>
          <Row gutter={[8, 16]}>
            {getPackagesResponse?.packages.map(p => (
              <Col key={p.id}>
                <PackageComponent title={p.numberOfMonths + " tháng"} price={p.price} selected={selected === p.id} onSelect={() => dispatch(changeSelected(p.id))} />
              </Col>
            ))}
          </Row>
          <Divider />
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Card style={{ minHeight: '100%' }} title="Chọn phương thức thanh toán:">
                <Radio.Group style={{ marginBottom: 20 }} onChange={({ target: { value } }) => dispatch(changePaymentMethod(value))} value={paymentMethod} >
                  <Space direction="vertical">
                    <Radio value={"VNPAY"}><Image src="https://soneku.com/wp-content/uploads/2021/11/vi-vnpay.png" preview={false} width={25} /> VNPAY</Radio>
                    <Radio value={"MOMO"}><Image src="http://file.hstatic.net/1000273026/article/momo_logo_ee7f0396e57f4820a504f7ab63e9eade.png" preview={false} width={25} /> MoMo</Radio>
                  </Space>
                </Radio.Group>
              </Card>
            </Col>
          </Row>
          <Row justify="end" style={{ marginTop: 20 }}>
            <Button type="primary" style={{ width: "100%" }} loading={isLoading} onClick={onSubmit}>
              Thanh toán
            </Button>
          </Row>
        </div>
      </Row>
    </Spin>
  </>
}

export default ExtendService;