export const validateTextEditor = (text : string | undefined) : boolean => {
    if (text?.includes("<img")) {
        return true;
    }
    if (text == undefined || text == "") {
        return false;
    }
    if(text?.replace(/<(.|\n)*?>/g, '').trim().length === 0) {
        return false;
    }
    return true;
}