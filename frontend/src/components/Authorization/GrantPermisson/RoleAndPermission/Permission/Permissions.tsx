import { Button, Col, Collapse, DatePicker, Divider, Form, Row, Typography } from "antd";
import moment from "moment";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../../../../hooks/Redux/hooks";
import {
  OriginalPermission,
  PermissionOfUser,
  useGetPermissionsOfUserQuery
} from "../../../../../redux/features/Permission/PermissionAPI";
import {
  addPermission,
  deletePermisson, editPermissionLimit,
  editPermissionTime,
  setPermissions
} from "../../../../../redux/features/Permission/PermissonSlice";
import CustomEmpty from "../../../../Util/CustomEmpty";
import AddPermission from "../../../Permission/AddPermission/AddPermission";
import LimitOfPermission from "../../../Permission/LimitOfPermission/LimitOfPermission";
import LIMIT_VALUE from "../../../../../constants/LimitValue";


const Permissions = () => {
  let currUserId = useAppSelector(state => state.grantPermission.userId);
  let dispatch = useAppDispatch();

  let isEditing = useAppSelector(state => state.grantPermission.isEditing);
  let permissions = useAppSelector(state => state.grantPermission.permissions);

  let { data: getPermissionsOfUserResponse } = useGetPermissionsOfUserQuery(currUserId, { skip: currUserId === 0 });

  const [form] = Form.useForm();

  useEffect(() => {
    dispatch(setPermissions(getPermissionsOfUserResponse?.permissions || []));
  }, [dispatch, getPermissionsOfUserResponse]);


  const generateValidFromFieldName = (permissionId: number) => `permission_${permissionId}.validFrom`;
  const generateExpiresAtFieldName = (permissionId: number) => `permission_${permissionId}.expiresAt`;

  const onChangePermissionValidateFromTime = (index: number, value: moment.Moment, permission: PermissionOfUser) => {
    dispatch(editPermissionTime({ index, value: value?.toDate() || permission.validFrom, key: "validFrom" }))
  }


  const onChangePermissionExpiresAt = (index: number, value: moment.Moment, permission: PermissionOfUser) => {
    dispatch(editPermissionTime({ index, value: value?.toDate() || permission.expiresAt, key: "expiresAt" }))
  }

  const onAddPermission = (permission: OriginalPermission) => {
    dispatch(addPermission({
      id: permission.id,
      title: permission.title,
      description: permission.description,
      isLimitByBranch: permission.hasLimitByBranch ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
      isLimitByDean: permission.hasLimitByDean ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
      isLimitByLearn: permission.hasLimitByLearn ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
      isLimitByManager: permission.hasLimitByManager ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
      isLimitByTeaching: permission.hasLimitByTeaching ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.DONT_CARE,
      validFrom: new Date(),
      expiresAt: new Date()
    }))
  }


  const onEditLimit = (index: number, key: string, value: boolean) => {
    if (!isEditing) return false;
    dispatch(editPermissionLimit({ index, key, value: value ? LIMIT_VALUE.LIMIT : LIMIT_VALUE.UNLIMIT }))
    return; 
  }


  const renderPermissionHeader = (permission: PermissionOfUser, index: number) => (
    <Row style={{ width: '100%' }} align="middle">
      <Col lg={17} md={18}>
        {permission.title}
      </Col>
      <Col lg={7} md={6}>
        <Row justify="end">
          {isEditing && <Button danger onClick={() => dispatch(deletePermisson(index))}>Xóa</Button>}
        </Row>
      </Col>
    </Row >
  )

  return <>
    <Divider orientation="left" orientationMargin={5}><Typography.Title level={5}>Các quyền bổ sung</Typography.Title></Divider>
    <Row justify="center">
      <Form form={form} style={{ width: '90%' }}>
        <Collapse>
          {permissions.map((permission, index) => (
            <Collapse.Panel
              key={index}
              header={renderPermissionHeader(permission, index)}
            >
              <LimitOfPermission permission={permission} onEditLimit={(key, value) => onEditLimit(index, key, value)} />
            </Collapse.Panel>
          ))}
          {permissions.length === 0 && <CustomEmpty description={"Không có dữ liệu"} />}
        </Collapse>
      </Form>
    </Row>
    {currUserId !== 0 && isEditing && <AddPermission hadPermissions={permissions} onAddPermission={onAddPermission} />}
  </>
}

export default Permissions;