import { blue, cyan, green, red, yellow } from "@ant-design/colors";
import { Tag } from "antd";
import React, { memo } from "react";
import COLOR_SCHEME from "@constants/ThemeColor";

export type TagType =
	| "PUBLIC"
	| "PRIVATE"
	| "OFFLINE"
	| "ONLINE"
	| "CREATED"
	| "ONGOING"
	| "ENDED"
	| "HYBRID"
	| "PASSED"
	| "FAILED"
	//  * This is the result states of a student submission
	| "DONE"
	| "WAITING"

	| "UNKNOWN";
type TagTypeProps = {
	type: TagType;
	title?: string;
};

const StageTag = (props: TagTypeProps) => {
	const { type, title } = props;
	switch (type) {
		case "OFFLINE":
			return <Tag color="geekblue">{title}</Tag>;
		case "ONLINE":
			return <Tag color="green">{title}</Tag>;
		case "HYBRID":
			return <Tag color="purple">{title}</Tag>;
		case "PUBLIC":
			return <Tag color="green">{title}</Tag>;
		case "PRIVATE":
			return <Tag color={COLOR_SCHEME.error}>{title} </Tag>;
		case "CREATED":
			return <Tag color={blue[4]}> {title} </Tag>;
		case "ONGOING":
			return <Tag color={green[4]}> {title}</Tag>;
		case "ENDED":
			return <Tag color={red[5]}> {title}</Tag>;
		case "PASSED":
			return <Tag color={green[5]}> {type}</Tag>;
		case "FAILED":
			return <Tag color={red[5]}> {type}</Tag>;
		case "DONE":
			return <Tag color={cyan[5]}> {type}</Tag>;
		case "WAITING":
			return <Tag color={yellow[5]}> {type}</Tag>;
		default:
			return <Tag color={COLOR_SCHEME.error}>UNKNOWN</Tag>;
	}
};

export default memo(
	StageTag,
	(prevProps, nextProps) => prevProps.type === nextProps.type
);
