import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { Button, Card, Form, Image, Input, Row, Space, Typography } from "antd";
import logo from "@assets/logo/logo.png";
import { registerNewTenant, resetRegisterStatus, TenantInfo, tenantSelector } from "../../../redux/features/Tenant/TenantSlice";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import _ from 'lodash';
import { useNavigate } from 'react-router-dom';
import { HOST_ROUTING_CONSTANT } from '../../../navigation/ROUTING_CONSTANTS';
import React from 'react';
import './NewTenantPage.less';

const NewTenantPage = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const onFinish = (values: TenantInfo & { rePassword: string }) => {

        if (values.password !== values.rePassword) {
            AntdNotifier.error("Mật khẩu và nhập lại mật khẩu không khớp");
            return;
        }
        const tenantInfo = _.omit(values, ['rePassword']);
        dispatch(registerNewTenant(tenantInfo));
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const isRegisterSuccess = useAppSelector(tenantSelector.selectTenantRegisterSuccess);
    const isRegisterPending = useAppSelector(tenantSelector.selectTenantRegisterPending);
    const registerStatus = useAppSelector(tenantSelector.selectTenantRegisterStatus);
    const tenantInfo = useAppSelector(tenantSelector.selectTenantInfo);

    useEffect(() => {
        if (isRegisterSuccess) {
            navigate(HOST_ROUTING_CONSTANT.REGISTER_SUCCESS);
            dispatch(resetRegisterStatus());
        }
    }, [isRegisterSuccess, navigate, tenantInfo, dispatch])

    useEffect(() => {
        if (registerStatus.isError) {
            AntdNotifier.error(JSON.stringify(registerStatus.errors))
        }
    }, [registerStatus])
    return (
        <Row justify={"center"} align={"middle"}>
            <Card >
                <Space direction={"vertical"} align={"center"} style={{ width: 500 }}>
                    <Image width={200} height={200} src={logo} preview={false} />
                    <Typography.Title>
                        BKLMS
                    </Typography.Title>
                    <Form
                        name="basic"
                        labelCol={{
                            span: 9,
                        }}
                        wrapperCol={{
                            span: 18,
                        }}

                        initialValues={{
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label={
                                <Space>
                                    <Typography.Text>
                                        Tên của bạn
                                    </Typography.Text>
                                </Space>
                            }
                            name="firstname"
                            rules={[
                                {
                                    required: true,
                                    message: 'Tên của bạn',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={
                                <Space>
                                    <Typography.Text>
                                        Họ của bạn
                                    </Typography.Text>
                                </Space>
                            }
                            name="lastname"
                            rules={[
                                {
                                    required: true,
                                    message: 'Họ của bạn',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Tên đăng nhập"
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Hãy nhập tài khoản của bạn',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: 'Hãy nhập email của bạn',
                                },
                                {
                                    type: 'email',
                                    message: "Email không đúng định dạng",
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Số điện thoại"
                            name="phone"
                            rules={[
                                {
                                    required: true,
                                    message: 'Hãy nhập số điện thoại của bạn',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Workspace"
                            name="domain"
                            rules={[
                                {
                                    required: true,
                                    message: 'Tên miền cho trang web',
                                },
                                {
                                    pattern: /^[a-zA-Z]+$/,
                                    message: "Tên miền chỉ được phép chứa các chữ cái hoa và thường, không dấu"
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Mật khẩu"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Nhập mật khẩu vào',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            label="Nhập lại mật khẩu"
                            name="rePassword"
                            rules={[
                                {
                                    required: true,
                                    message: 'Mật khẩu phải khớp với mật khẩu đã nhập',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            wrapperCol={{
                                offset: 9,
                                span: 18,
                            }}
                            style={{display: "flex", justifyContent: "flex-end"}}
                            className={'submit-tenant-btn'}
                        >
                            <Button type="primary" htmlType="submit" loading={isRegisterPending} >
                                Đăng ký
                            </Button>
                        </Form.Item>
                    </Form>
                </Space>
            </Card>
        </Row>

    );

};

export default NewTenantPage;