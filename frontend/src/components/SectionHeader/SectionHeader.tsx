import { Space} from 'antd'
import React from 'react'
import './SectionHeader.less'
import { CSSProperties } from 'react';

type SectionHeaderProps = { title: JSX.Element | string,
    action?: JSX.Element,
    style?: CSSProperties
}
const SectionHeader = (props: SectionHeaderProps) => {
    const { title, action } = props;
    return (
        <div className={"section-header-container"}>
            <span className='section-header-title'>
                {
                    title
                }
            </span>
            <Space>
                {
                    action
                }
            </Space>
        </div>
    )
}

export default SectionHeader;
