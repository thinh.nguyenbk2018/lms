import pracetice_img from "@assets/images/pencil.png"
import './ProgressCard.less'
const ProgressCard = ({img,number,title} : {img: string | undefined,number: number,title: string}) => {
    return (
        <div className="progress-card-container">
            <div className="progress-card__image_container">
                <img className="progress-card__image" src={img || pracetice_img} alt={"progress image"}/>
            </div>
            <div className="progress-card__elaboration">
                <span className={"progress-card__number_text"}>
                    {
                        number
                    }
                </span>
                <span className={"progress-card__title_text"}>
                    {
                        title
                    }
                </span>
            </div>
        </div>);
};

export default ProgressCard;