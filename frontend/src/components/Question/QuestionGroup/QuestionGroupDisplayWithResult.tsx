import React from "react";
import {
    createTestContentActions,
    QuestionGroupWithResult,
    QuestionType,
} from "../../../redux/features/Quiz/CreateQuizSlice";
import { Card, Row, Typography } from "antd";
import TextEditor from "../../TextEditor/TextEditor";
import { useAppDispatch } from "../../../hooks/Redux/hooks";
import MenuAddNewQuestion from "@pages/Tenant/Courses/CourseQuiz/TestContent/MenuAddNewQuestion";
import './QuestionGroupDisplay.less';
import QuestionResultContainerItem from "@components/Question/QuestionContainerItem/QuestionResultContainerItem";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

/** Dumb component that take in a question GROUP and render it correctly*/
const QuestionGroupDisplayWithResult = (props: QuestionGroupWithResult) => {
    const { id, description, note, answerList } = props;

    const dispatch = useAppDispatch();

    function handleMenuClick(e: any) {
        let choice: QuestionType = e.key;
        dispatch(createTestContentActions.setOpeningDrawerType(choice));
        dispatch(createTestContentActions.setQuestionGroupIdInDraft(id!));
    }

    return (
        <Card
            className={"question-group-content-container"}
        >
            {
                JSON.parse(props?.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
                    <AttachmentRender attachment={a} />
                </Row>)
            }
            <Typography.Text strong={true} type={"secondary"}>
                Mô tả đề:
            </Typography.Text>
            <br />
            <TextEditor
                onChange={() => { }}
                readOnly={true}
                useBubbleTheme={true}
                value={description}
                additionalStyles={{ scrollbarWidth: "none" }}
            />
            <Typography.Text strong={true} type={"secondary"} style={{ display: "block", marginBottom: '0.25rem' }}>
                Trả lời các câu hỏi sau:
            </Typography.Text>
            <br />
            <div className={"question-group__question-list-container"}>
                {answerList.map((item, index) => {
                    return (
                        <QuestionResultContainerItem
                            {...item}
                        />
                    );
                })}
            </div>
        </Card>
    );
};

export default QuestionGroupDisplayWithResult;
