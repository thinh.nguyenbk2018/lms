import React, {useState} from "react";
import {Avatar, Button, Card, Col, Collapse, Row, Space,} from "antd";
import {DownvoteIconWithState, ReplyIconsWithText, UpvoteIconWithState,} from "../Icons/IconWithText";
import {CommentEditor, CommentItem} from "./Comments";
import {
    useDeletePostMutation,
    useGetPostDetailQuery,
    useInteractWithPostMutation,
    useLazyGetChildCommentsOfPostQuery,
} from "@redux/features/Class/ClassDiscussion/ClassDiscussionAPI";
import {ID} from "@redux/interfaces/types";
import {useParams} from "react-router-dom";
import useClassCourseInformation from "../../hooks/Routing/useClassCourseInformation";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {classDiscussionActions,} from "@redux/features/Class/ClassDiscussion/ClassDiscussionSlice";
import LoadingPage from "../../pages/UtilPages/LoadingPage";
import {parseDate} from "../Calendar/Utils/CalendarUtils";
import "./ForumThreadItem.less";
import {formatRelative} from "date-fns";
import {vi} from "date-fns/locale";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleXmark, faPenToSquare,} from "@fortawesome/free-solid-svg-icons";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import TextEditor from "../TextEditor/TextEditor";
import ApiErrorRetryPage from "../../pages/UtilPages/ApiErrorRetryPage";
import {classDiscussionErrorsHanlder} from "./ForumThreadList";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";

const {Panel} = Collapse;

export interface CommentDetail {
    id: string;
    username: string;
    avatar: string;
    time: number;
    content: string;
    upVotesCount: number;
    downVotesCount: number;
    personalPreference: "upvoted" | "downvoted" | "commented";
}

export interface ForumThreadItemType {
    id: string;
    title: string;
    avatar: string;
    username: string;
    time: number;
    content: string;
    upVotesCount: number;
    downVotesCount: number;
    repliesCount: number;
    personalPreference: "upvoted" | "downvoted" | "commented";
    comments: CommentDetail[];
}

const ForumThreadItem = ({id, ...rest}: { id: ID }) => {
    const {classId: classIdFromPath} = useParams();
    // * Redux Toolkit Queries
    const {classId, classInfo, isSuccessClass, isErrorClass, isFetchingClass} =
        useClassCourseInformation(Number(classIdFromPath));

    const [commentPage, setCommentPage] = useState(1);

    const {
        data: postData,
        isSuccess,
        isFetching,
        isError,
        error,
    } = useGetPostDetailQuery(
        {
            classId: Number(classId),
            postId: id,
            commentPagination: {
                page: commentPage,
                size: DEFAULT_PAGE_SIZE,
            },
        },
        {
            skip: isNaN(Number(classIdFromPath)),
        }
    );

    const [fetchCommentsOfPost, {
        data: postComments
    }] = useLazyGetChildCommentsOfPostQuery();

    const [deletePost] = useDeletePostMutation();
    const [interactWithPost] = useInteractWithPostMutation();
    // * Redux Slice
    // * Searching and pagination
    const dispatch = useAppDispatch();
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);

    const [showCollapse, setShowCollapse] = useState(false);
    const onShowComments = () => {
        setShowCollapse(!showCollapse);
    };

    if (isSuccessClass && id && postData && classId) {
        const {
            personalPreference,
            comments: {listData: listComment},
        } = postData;
        return (
            <Card
                key={id}
                title={postData.title}
                className={"thread-item"}
                extra={
                    currentUser?.id === postData.createdBy.id ? (
                        <Space>
                            <FontAwesomeIcon
                                className={'thread-item-edit-icon'}
                                icon={faPenToSquare}
                                onClick={() => {
                                    dispatch(classDiscussionActions.editPost(postData));
                                }}
                            />
                            <FontAwesomeIcon
                                className={'thread-item-delete-icon'}
                                icon={faCircleXmark}
                                onClick={() => {
                                    confirmationModal('bài viết', postData.title, () => {
                                        AntdNotifier.handlePromise(
                                            deletePost({
                                                classId: Number(classId),
                                                postId: postData.id,
                                            }).unwrap(),
                                            "Xóa bài viết"
                                        );
                                    });
                                }}
                            />
                        </Space>
                    ) : (
                        <></>
                    )
                }
            >
                <Row>
                    <Col span={1}>
                        <Avatar
                            src={
                                postData.createdBy.avatar ||
                                "https://joeschmoe.io/api/v1/random"
                            }
                        />
                    </Col>
                    <Col span={23}>
                        <Row>
                            <span
                                style={{fontWeight: 'bold'}}>{postData.createdBy.lastName + " " + postData.createdBy.firstName}</span>
                        </Row>
                        <Row>
                            {formatRelative(
                                parseDate(postData.createdAt),
                                new Date(),
                                {locale: vi}
                            )}
                        </Row>
                        <Row className={'forum-thread-item-content'}>
                            <TextEditor
                                additionalStyles={{display: "inline"}}
                                readOnly={true}
                                useBubbleTheme={true}
                                value={postData.content}
                                onChange={() => {
                                }}
                            />
                        </Row>
                        <Row>
                            <div style={{width: "100%"}}>
                                <Row style={{width: "100%"}}>
                                    {/* {postData.personalPreference && ( */}
                                    {(
                                        <Space size={"large"} style={{margin: ".3em"}}>
                                            <UpvoteIconWithState
                                                upvoted={postData.personalPreference === "UP_VOTE"}
                                                numOfUpvotes={postData.upVoteCount}
                                                onClick={() => {
                                                    interactWithPost({
                                                        classId: classId,
                                                        postId: postData.id,
                                                        postInteraction: "UP_VOTE",
                                                    });
                                                }}
                                            />
                                            <DownvoteIconWithState
                                                downvoted={postData.personalPreference === "DOWN_VOTE"}
                                                numOfDownvotes={postData.downVoteCount}
                                                onClick={() => {
                                                    interactWithPost({
                                                        classId: classId,
                                                        postId: postData.id,
                                                        postInteraction: "DOWN_VOTE",
                                                    });
                                                }}
                                            />
                                            <ReplyIconsWithText
                                                numOfDiscussion={postData.repliesCount || 0}
                                                isOpening={showCollapse}
                                                onClick={() => {
                                                    // fetchCommentsOfPost({
                                                    //     classId: Number(classId),
                                                    //     postId: postData.id,
                                                    //     childCommentPagination: {
                                                    //         page: 1,
                                                    //         size: 5
                                                    //     }
                                                    // })
                                                    onShowComments();
                                                }}
                                            />
                                        </Space>
                                    )}
                                </Row>
                                {/* Comments */}
                                <Row style={{width: "100%"}}>
                                    <Collapse
                                        className={`comment`}
                                        // defaultActiveKey={postData.comments?.listData?.[0].id}
                                        ghost
                                        style={{
                                            display: showCollapse ? "block" : "none",
                                            margin: ".5em",
                                            width: "100%"
                                        }}
                                    >
                                        {listComment &&
                                            listComment.length > 0 &&
                                            listComment.map((comment) => {
                                                return (
                                                    // <Panel
                                                    //     key={comment.id}
                                                    //     header={`Bình luận #${comment.id}`}
                                                    // >
                                                    <CommentItem postId={postData.id} {...comment} />
                                                    // </Panel>
                                                );
                                            })}
                                        {postData.comments.totalPages > commentPage && (
                                            <Row justify="center">
                                                <a>
                                                    <button
                                                        className={'forum-thread-item-load-more-comment'}
                                                        onClick={() => setCommentPage(commentPage + 1)}
                                                    >
                                                        Tải thêm thêm bình luận
                                                    </button>
                                                </a>
                                            </Row>
                                        )}
                                        {/*<div>*/}
                                        {/*    <Pagination*/}
                                        {/*        key={"ADDER"}*/}
                                        {/*        defaultCurrent={1}*/}
                                        {/*        total={postData.comments.total}*/}
                                        {/*        pageSize={5}*/}
                                        {/*        responsive={true}*/}
                                        {/*        onChange={(page, pageSize) => {*/}
                                        {/*            setCommentPage(page);*/}
                                        {/*        }}*/}
                                        {/*    />*/}
                                        {/*</div>*/}
                                        <CommentEditor
                                            key={"EDITOR"}
                                            classId={classId}
                                            postId={Number(postData.id)}
                                            postIdToCommentTo={postData.id}
                                        />
                                    </Collapse>
                                </Row>
                            </div>
                        </Row>
                    </Col>
                </Row>
                {/*<List.Item*/}
                {/*    style={{width: "100%"}}*/}
                {/*    key={postData.id}*/}
                {/*    className={'forum-thread-item-comment-wrapper'}*/}
                {/*    actions={[*/}
                {/*        <div style={{width: "100%"}}>*/}
                {/*            <Row style={{width: "100%"}}>*/}
                {/*                /!* {postData.personalPreference && ( *!/*/}
                {/*                {(*/}
                {/*                    <Space size={"large"} style={{margin: ".3em"}}>*/}
                {/*                        <UpvoteIconWithState*/}
                {/*                            upvoted={postData.personalPreference === "UP_VOTE"}*/}
                {/*                            numOfUpvotes={postData.upVoteCount}*/}
                {/*                            onClick={() => {*/}
                {/*                                interactWithPost({*/}
                {/*                                    classId: classId,*/}
                {/*                                    postId: postData.id,*/}
                {/*                                    postInteraction: "UP_VOTE",*/}
                {/*                                });*/}
                {/*                            }}*/}
                {/*                        />*/}
                {/*                        <DownvoteIconWithState*/}
                {/*                            downvoted={postData.personalPreference === "DOWN_VOTE"}*/}
                {/*                            numOfDownvotes={postData.downVoteCount}*/}
                {/*                            onClick={() => {*/}
                {/*                                interactWithPost({*/}
                {/*                                    classId: classId,*/}
                {/*                                    postId: postData.id,*/}
                {/*                                    postInteraction: "DOWN_VOTE",*/}
                {/*                                });*/}
                {/*                            }}*/}
                {/*                        />*/}
                {/*                        <ReplyIconsWithText*/}
                {/*                            numOfDiscussion={postData.repliesCount || 0}*/}
                {/*                            isOpening={showCollapse}*/}
                {/*                            onClick={() => {*/}
                {/*                                // fetchCommentsOfPost({*/}
                {/*                                //     classId: Number(classId),*/}
                {/*                                //     postId: postData.id,*/}
                {/*                                //     childCommentPagination: {*/}
                {/*                                //         page: 1,*/}
                {/*                                //         size: 5*/}
                {/*                                //     }*/}
                {/*                                // })*/}
                {/*                                onShowComments();*/}
                {/*                            }}*/}
                {/*                        />*/}
                {/*                    </Space>*/}
                {/*                )}*/}
                {/*            </Row>*/}
                {/*            /!* Comments *!/*/}
                {/*            <Row style={{width: "100%"}}>*/}
                {/*                <Collapse*/}
                {/*                    className={`comment`}*/}
                {/*                    // defaultActiveKey={postData.comments?.listData?.[0].id}*/}
                {/*                    ghost*/}
                {/*                    style={{*/}
                {/*                        display: showCollapse ? "block" : "none",*/}
                {/*                        margin: ".5em",*/}
                {/*                        width: "100%"*/}
                {/*                    }}*/}
                {/*                >*/}
                {/*                    {listComment &&*/}
                {/*                        listComment.length > 0 &&*/}
                {/*                        listComment.map((comment) => {*/}
                {/*                            return (*/}
                {/*                                // <Panel*/}
                {/*                                //     key={comment.id}*/}
                {/*                                //     header={`Bình luận #${comment.id}`}*/}
                {/*                                // >*/}
                {/*                                    <CommentItem postId={postData.id} {...comment} />*/}
                {/*                                // </Panel>*/}
                {/*                            );*/}
                {/*                        })}*/}
                {/*                    /!* {postData.comments.totalPages > commentPage && (*/}
                {/*		<Row justify="center">*/}
                {/*			<Button*/}
                {/*				type="primary"*/}
                {/*				onClick={() => setCommentPage(commentPage + 1)}*/}
                {/*			>*/}
                {/*				Load thêm bình luận*/}
                {/*			</Button>*/}
                {/*		</Row>*/}
                {/*	)} *!/*/}
                {/*                    /!*<div>*!/*/}
                {/*                    /!*    <Pagination*!/*/}
                {/*                    /!*        key={"ADDER"}*!/*/}
                {/*                    /!*        defaultCurrent={1}*!/*/}
                {/*                    /!*        total={postData.comments.total}*!/*/}
                {/*                    /!*        pageSize={5}*!/*/}
                {/*                    /!*        responsive={true}*!/*/}
                {/*                    /!*        onChange={(page, pageSize) => {*!/*/}
                {/*                    /!*            setCommentPage(page);*!/*/}
                {/*                    /!*        }}*!/*/}
                {/*                    /!*    />*!/*/}
                {/*                    /!*</div>*!/*/}
                {/*                    <CommentEditor*/}
                {/*                        key={"EDITOR"}*/}
                {/*                        classId={classId}*/}
                {/*                        postId={Number(postData.id)}*/}
                {/*                        postIdToCommentTo={postData.id}*/}
                {/*                    />*/}
                {/*                </Collapse>*/}
                {/*            </Row>*/}
                {/*        </div>,*/}
                {/*    ]}*/}
                {/*>*/}
                {/*    <List.Item.Meta*/}
                {/*        avatar={*/}
                {/*            <Avatar*/}
                {/*                src={*/}
                {/*                    postData.createdBy.avatar ||*/}
                {/*                    "https://joeschmoe.io/api/v1/random"*/}
                {/*                }*/}
                {/*            />*/}
                {/*        }*/}
                {/*        description={formatRelative(*/}
                {/*            parseDate(postData.createdAt),*/}
                {/*            new Date(),*/}
                {/*            {locale: vi}*/}
                {/*        )}*/}
                {/*        title={postData.createdBy.lastName + " " + postData.createdBy.firstName}*/}
                {/*    />*/}
                {/*    <TextEditor*/}
                {/*        additionalStyles={{display: "inline"}}*/}
                {/*        readOnly={true}*/}
                {/*        useBubbleTheme={true}*/}
                {/*        value={postData.content}*/}
                {/*        onChange={() => {*/}
                {/*        }}*/}
                {/*    />{" "}*/}
                {/*    <Divider/>*/}
                {/*</List.Item>*/}
            </Card>
        );
    } else if (isFetchingClass) {
        return (
            <>
                <LoadingPage/>
            </>
        );
    } else if (isError || error) {
        classDiscussionErrorsHanlder(error);
        return <ApiErrorRetryPage/>;
    } else {
        return <></>;
    }
};

export default ForumThreadItem;
