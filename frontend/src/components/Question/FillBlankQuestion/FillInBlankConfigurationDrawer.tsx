import {ExclamationCircleOutlined, SettingOutlined} from '@ant-design/icons';
import {Button, Col, Drawer, Form, Input, Modal, Row, Space, Typography} from 'antd';
import React, {useRef, useState} from 'react';
import CancelButton from "../../ActionButton/CancelButton";
import DoneButton from "../../ActionButton/DoneButton";
import BlanksConfigForms from "./BlanksConfigForms";
import _ from "lodash";
import {FillInBlankQuestion, isFIBquestion} from "../../../redux/features/Quiz/CreateQuizSlice";
import {Field, Formik, useFormikContext} from "formik";
import {DEFAULT_TEXT_EDITOR_HTML} from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import {FormItem} from "formik-antd";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import {DEFAULT_FRONTEND_ID} from "../../../redux/interfaces/types";
import {baseCreateQuestionYupSchema, baseEditQuestionYupSchema} from "../QuestionContainerItem/baseQuestionSchema";
import './FillInBlankConfigurationDrawer.less'
import BackButton from "../../ActionButton/BackButton";
import {useAppDispatch} from "../../../hooks/Redux/hooks";
import useQuestionDrawerType from "../../../hooks/QuizDrawer/useQuestionDrawerType";
import QUESTION_LABEL_CONSTANT from "../constant/QuestionLabelConstant";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import * as Yup from 'yup';
import AttachmentQuestion from '../Attachment/AttachmentQuestion';
import {schema} from "normalizr";

const QUESTION_STAGE = {
    STAGE_ONE: 1,
    STAGE_TWO: 2,
}
// * Find blank regex: /\[.*?\]/g
// * Dealing with nested brackets: (\[(?:\[??[^\[]*?\]))
// * Credits: https://stackoverflow.com/questions/2403122/regular-expression-to-extract-text-between-square-brackets
// * Extracting the whole sentence containing the blank RegEx:  /[^.?!]*(?<=[.?\s!])flung(?=[\s.?!])[^.?!]*[.?!]/g
// * Credits: https://stackoverflow.com/questions/26081820/regular-expression-to-extract-whole-sentences-with-matching-word/48533652

export const findTextsInsideBrackets = (string: string) => {
    return string.match(/\[.*?\]/g);
}

/*
    * Split the question in to string array --> Subtitute the blanks with text inputs
    * How it work? --> run the codes below
    const a = [1, 23, 4, 5, 6];
    const text = "This is a test tring [ass] with brackets ir[here]versab";
    const res = _.split(text, /\[.*?\]/);
    console.log(res);
*/
export const splitQuestionTextByBlanks: (questionText: string) => string[] = (questionText: string) => {
    return _.split(questionText, /\[.*?\]/);
}
export const replaceBlankInQuestionWithInput: (questionText: string) => string = (questionText: string) => {
    return _.replace(questionText, /\[.*?\]/g, "_____");
}
export const replaceBlankWithAntdInput: (questionText: string) => string = (questionText: string) => {
    return _.replace(questionText, /\[.*?\]/g, `
        <span>
            <Button type={"primary"} danger> Test button</Button>
            <Input/>
        </span>
    `);
}

const RenderBlankConfigForms = () => {
    const { values, setFieldValue } = useFormikContext<FillInBlankQuestion>();

    const question = values.description;
    let blanks = findTextsInsideBrackets(question);

    console.log('blanks: ', values['blankList']);

    // * Configuring the default
    if (_.isEmpty(values.blankList)) {
        blanks?.forEach((item, index) => {
            setFieldValue(`blankList[${index}]`, {
                expectedAnswer: item.substring(1, item.length - 1) || "",
                matchStrategy: "EXACT",
                isCaseSensitive: false,
                hint: "",
            })
        })
    }

    return (
        <BlanksConfigForms sentence={question} blanks={blanks || []} />
    );
}


const FillInBlankConfigurationDrawer = () => {
    const [stage, setStage] = useState(QUESTION_STAGE.STAGE_ONE);

    // * The drawer state
    const {
        visible,
        closeDrawer,
        openDrawer,
        isEditing,
        questionDataInDraft,
        doneHandler,
        isFetching,
        isError,
        isSuccess,
        questionIdInDraft,
        testName
    } = useQuestionDrawerType("FILL_IN_BLANK");
    const dispatch = useAppDispatch();

    const drawerRef = useRef<HTMLDivElement | null>(null);


    function closeConfirm(onOk: () => void, onCancel: () => void) {
        Modal.confirm({
            title: 'Xác nhận hủy bỏ câu hỏi',
            icon: <ExclamationCircleOutlined />,
            content: 'Chúng tôi nhận thấy rằng bạn đã soạn phần nào câu hỏi. Chọn xác nhận sẽ xóa hết tất cả.',
            okText: 'Xác nhận',
            cancelText: 'Tiếp tục soạn',
            onOk: onOk,
            onCancel: onCancel,
            getContainer: drawerRef.current!!
        });
    }


    const setStageTwo = () => {
        setStage(QUESTION_STAGE.STAGE_TWO);
    }
    const setStageOne = () => {
        setStage(QUESTION_STAGE.STAGE_ONE);
    }

    const contentByStage = (stage: number,
                            values: any,
                            setFieldValue: (key: string, value: any) => void,
                            validateForm: any) => {
        switch (stage) {
            case QUESTION_STAGE.STAGE_ONE:
                return <>
                    <div ref={drawerRef}>
                    </div>
                    <Row align={"top"} justify={"end"}>
                        <Col>
                            <FormItem name={"point"} label={QUESTION_LABEL_CONSTANT.POINT} initialValue={values.point}>
                                <Input type={"number"} name={"point"} value={values.point}
                                    onChange={({ currentTarget: { value } }) => setFieldValue("point", value)} />
                            </FormItem>
                        </Col>
                    </Row>

                    <Row>
                        <Typography.Paragraph>
                            Để định dạng câu hỏi của bạn, đóng ngoặc vuông cho các ô trống cần điền.
                        </Typography.Paragraph>
                    </Row>
                    <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                        <Col span={24}>
                            <Field
                                name={"attachment"}
                                placeholder={"Type article content"}
                                component={AttachmentQuestion}
                                label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                            />
                        </Col>
                    </Row>
                    <Row justify={"center"}>
                        <div style={{ display: "flex", flexDirection: "column", width: "100%" }}>
                            <Row className={'question-drawer-text-editor-wrapper'}>
                                <Field
                                    name={"description"}
                                    placeholder={""}
                                    component={TextEditorField}
                                    label={"Mô tả đề"}
                                />
                            </Row>
                            <Button onClick={() => {
                                // * Push the blanks to the blank List
                                // setFieldValue(`blankList`,)
                                // console.log("blankList click: ", values["blankList"]);
                                validateForm()
                                    .then((res: any) => {
                                        // console.log('validate: ', res);
                                        if (res["description"] == undefined) {
                                            setStage(QUESTION_STAGE.STAGE_TWO);
                                        }
                                    })
                                }}
                                type={"primary"}
                                style={{
                                    alignSelf: "end",

                                }}
                                icon={<SettingOutlined />}
                            >
                                Cấu hình cho các ô trống
                            </Button>
                        </div>
                    </Row>
                </>
            case QUESTION_STAGE.STAGE_TWO:
                return <>
                    <div className={"fib-main-container"}>
                        <RenderBlankConfigForms />
                        <BackButton onClick={() => {
                            setStageOne();
                            setFieldValue("blankList", []);
                        }}
                        style={{
                            alignSelf: "end",
                            margin: "0.3em 2em"
                        }}
                        />
                    </div>
                </>;
            default:
                return <h1>????</h1>;
        }
    }

    if (questionDataInDraft && !isFIBquestion(questionDataInDraft)) {
        AntdNotifier.error("Lỗi khi load câu hỏi, vui lòng refresh lại hoặc contact chúng tôi")
        return null;
    }

    return (
        <>
            <Formik
                enableReinitialize={true}
                validateOnChange={false}
                initialValues={{
                    id: questionDataInDraft?.id || DEFAULT_FRONTEND_ID,
                    description: questionDataInDraft?.description || DEFAULT_TEXT_EDITOR_HTML,
                    point: questionDataInDraft?.point || 0,
                    blankList: questionDataInDraft?.blankList || [],
                    attachment: questionDataInDraft?.attachment || '[]'
                }}
                validationSchema={Yup.object({
                    description: Yup.string().required("Mô tả câu hỏi là thông tin bắt buộc")
                        .test("Validate description", "Mô tả câu hỏi phải chứa ít nhất 1 cặp [] để biểu thị cho ô trống.", des => {
                            return !(des != undefined && !des.includes("[]"));
                        }),
                    blankList: (Yup.array().min(1, "Phải có ít nhất 1 ô trống").of(blankSchema))
                }).concat(isEditing ? baseEditQuestionYupSchema : baseCreateQuestionYupSchema)}
                onSubmit={
                    (values) => {
                        const valuesToSubmit: FillInBlankQuestion = {
                            type: 'FILL_IN_BLANK',
                            note: '',
                            order: 0,
                            ...values,
                        }
                        doneHandler(valuesToSubmit, setStageOne);
                        console.log("FIB_Question values is: ", values);
                    }

                }
            >
                {
                    ({ dirty, isValid, validateForm, handleSubmit, setFieldValue, values, errors, resetForm }) => {
                        return <Form>
                            <Drawer
                                maskClosable={false}
                                closable={false}
                                title={<Space direction={"vertical"}>
                                    <Typography.Text>
                                        {`${testName || "Không thể hiển thị tên đề kiểm tra"}`}
                                    </Typography.Text>
                                    <Typography.Text strong>
                                        Điền vào chỗ trống
                                    </Typography.Text>
                                </Space>}
                                placement="right"
                                size={"large"}
                                onClose={() => {
                                    // * Check if there was data and add for confirmation
                                    // setVisible(false);
                                    const question = values.description;
                                    if (question && question !== '<p><br></p>' || stage !== QUESTION_STAGE.STAGE_ONE) {
                                        closeConfirm(() => {
                                            closeDrawer()
                                            setStage(QUESTION_STAGE.STAGE_ONE);
                                            setFieldValue("description", "");
                                        }, () => {
                                        });
                                    } else {
                                        closeDrawer();
                                    }
                                }}
                                visible={visible}
                                extra={
                                    <Space>
                                        <CancelButton
                                            onClick={() => {
                                                if (dirty) {
                                                    closeConfirm(
                                                        () => {
                                                            closeDrawer();
                                                            setStage(QUESTION_STAGE.STAGE_ONE);
                                                            setFieldValue("description", "");
                                                        },
                                                        () => {
                                                        }
                                                    );
                                                } else {
                                                    closeDrawer();
                                                }
                                            }
                                            }
                                        />
                                        <DoneButton
                                            disabled={stage === QUESTION_STAGE.STAGE_ONE || !dirty}
                                            onClick={() => handleSubmit()}
                                        />
                                    </Space>
                                }
                            >
                                {contentByStage(stage, values, setFieldValue, validateForm)}
                            </Drawer>
                        </Form>
                    }
                }
            </Formik>
        </>
    );
};

// * Question configuration props with Formik HOC
const blankSchema = Yup.object({
    expectedAnswer: Yup.string().required("Đáp án cho ô trống là thông tin bắt buộc")
})


export default FillInBlankConfigurationDrawer;