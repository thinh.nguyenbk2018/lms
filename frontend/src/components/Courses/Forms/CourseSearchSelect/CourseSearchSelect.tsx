import React from "react";
import {useAppDispatch, useAppSelector} from "../../../../hooks/Redux/hooks";
import {
    changeCriteria as changeCourseSearchCriteria,
    courseSelector,
} from "../../../../redux/features/Course/CourseSlice";
import {Select} from "antd";
import useDebounced from "../../../../hooks/Debounced/useDebounced";
import {ID} from "../../../../redux/interfaces/types";
import {useGetAllCourseListQuery} from "@redux/features/Course/CourseAPI";
import {useParams} from "react-router-dom";

type CourseSearchSelectProps = {
    /** The most important callback */
    onSelect?: (value: ID) => void;
    /* Below callbacks make this compatible with formik as a field */
    onChange?: (value: ID) => void;
    onBlur?: Function;
    value?: ID;
    disabled?: boolean;
};
/** Functionalities tested: Will select and expose the course ID */
const CourseSearchSelect = ({
    onSelect,
    onChange,
    value,
    disabled,
}: CourseSearchSelectProps) => {
    const {courseId} = useParams();
    const courseSearchCriteria = useAppSelector(
        courseSelector.selectSearchCriteria
    );
    const {
        data: paginatedResponse,
        isSuccess,
        isFetching,
    } = useGetAllCourseListQuery(courseSearchCriteria);

    const dispatch = useAppDispatch();

    const debouncedCriteriaChange = useDebounced((criteria) =>
        dispatch(changeCourseSearchCriteria(criteria))
    );

    // @ts-ignore
    return (
        <Select
            value={value}
            showSearch
            allowClear={true}
            disabled={disabled}
            // style={{ width: 200 }}
            placeholder="Chọn khóa học"
            optionFilterProp="children"
            filterOption={(input, option) => {
                const optionContent = (option?.children || "") as string;
                return (
                    optionContent
                        .toLowerCase?.()
                        .indexOf(input.toLowerCase()) >= 0
                );
            }}
            filterSort={(optionA, optionB) => {
                const optionAcontent = (optionA?.children || "") as string;
                const optionBcontent = (optionB?.children || "") as string;
                return optionAcontent
                    .toLowerCase()
                    .localeCompare(optionBcontent.toLowerCase());
            }}
            onChange={(value, option) => {
                console.log("COURSE SEARCH / SELECT", value, option);
                onSelect?.(value);
                onChange?.(value);
                // onBlur?.();
            }}
            onSearch={(value) => {
                const searchCriteria = {
                    ...courseSearchCriteria,
                    keyword: value,
                };
                debouncedCriteriaChange(searchCriteria);
            }}
            onSelect={() => {
                console.log("ON SELECT");
            }}
        >
            {paginatedResponse?.listData.map((item, index) => {
                return (
                    <Select.Option value={item.id} key={item.id}>
                        {item.name}
                    </Select.Option>
                );
            })}
        </Select>
    );
};

export default CourseSearchSelect;
