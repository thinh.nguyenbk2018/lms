import axios from "axios";
import _ from "lodash";
import queryString from "query-string";
import React, { useCallback, useState } from "react";
import { API_CONSTANT } from '../../constants/ApiConfigs';
import { SearchCriteria } from "../../redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";


type PaginationParam = {
    page: number,
    size: number,
    keyword: string,
}

const transformCriteriaToParam : (criteria: SearchCriteria) => string = (criteria) => {
    console.log(`PARAMS IS: `, queryString.stringify(criteria));
    return "";
}
/**
 *
 * @param endpointToSearch - String prefixed with base url
 * @param searchCriteria - filter, sort
 * @param interval - debounce timer
 * @param minLengthToTriggerSearch - request only fire if length bigger than this value
 * @param debug - Log out additional info (default is false)
 * @returns [inputVal,setInputVal, results] - results - List of data found returned from the  server
 * @example
 * const [inputVal,setInputVal, result] = useDebounceSeach<User[]>("/users");
 * * Attach the input
 * <input type="text" value={inputVal} onChange={e => setInputVal(e.target.value)}>
 * * Render the result
 * <List>
 *      {
 *          result.map(item => <UserItem user={item})
 *      }
 * </List>
 */
const useDebounceAndPaginatedSearch = <ResultType>(endpointToSearch: string, searchCriteria: SearchCriteria, callback?: Function, interval: number = 650, minLengthToTriggerSearch: number = 1, debug: boolean = false) => {
    // * States to populate an input
    const [searchText, setSearchText] = useState<string>('');
    const [results, setSearchResult] = useState<ResultType | undefined>();
    // .replace(/([^:]\/)\/+/g, "$1");
    const debounceFn = useCallback(_.debounce((inputValue) => {
        // setVal(inputValue);
        if (inputValue.length > minLengthToTriggerSearch) {
            console.log("Axios Called");
            const endpoint = endpointToSearch[0] === '/' ? endpointToSearch.substring(1) : endpointToSearch;
            const URL = `${API_CONSTANT.BASE_URL}/${endpoint}?${transformCriteriaToParam(searchCriteria)}`;
            if (debug) {
                console.log("Searching the URL:", URL);
            }
            axios.request<ResultType>({url: URL, method: "GET"})
                .then((res) => {
                    if (debug)
                        console.log('Data receive', res.data);
                    setSearchResult(res.data);
                })
                .catch(err => console.log(err));
        }
    }, interval), []);

    const setSearchTextHandler: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        const {target: {value}} = e;
        setSearchText(value);
        debounceFn(value);
        if(callback) {
            callback();
        }
    }

    return [
        searchText,
        setSearchTextHandler,
        results,
    ] as const
}

export default useDebounceAndPaginatedSearch;