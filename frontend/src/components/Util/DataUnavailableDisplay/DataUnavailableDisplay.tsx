import * as React from 'react';
import {Typography} from "antd";

type Props = {
    mode: "text" | "leaflet";
    message: string;
};

export function DataUnavailableDisplay(props: Props) {
    const {mode, message} = props;
    // A simple text displaying the error
    if(mode) {
        return <Typography.Text type={"danger"}>
            {message || ""}
        </Typography.Text>
    }
    return <></>
}