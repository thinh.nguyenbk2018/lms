import React, { useEffect } from "react";
import { Affix, Layout, Menu } from "antd";
import useRoutesTabs from "../../hooks/Routing/useRoutesTabs";
import {
	InAppNavItems,
	isTabNav,
	NavigationItem,
} from "../../navigation/NavigationItem";
import "./SideBar.less";
import { useAppSelector } from "../../hooks/Redux/hooks";
import { authSelectors } from "../../redux/features/Auth/AuthSlice";
import ClassSideBar from "./ClassSidebar/ClassSideBar";
import CourseSidebar from "./CourseSidebar/CourseSidebar";
import { sideBarSelector } from "../../redux/features/SideBar/SidebarSlice";
import classNames from "classnames";
import useActiveKey from "@hooks/Routing/useActiveKey";

const { Sider } = Layout;
// App --> Side Bar --> Class Side -->  Nav
// App --> Nav --> App
/*
    ! Issues: In order to makge the sidebar in sync with the URL in case of changing the URL manually:
    !       + Need an algorithm that parse the URL --> key = Key defined in NavigationItem.tsx (HARD :) )
    !       + Setting key as default selected in the Menu component below
 */
const SideBar = ({ props, ...rest }: { props: NavigationItem[] }) => {
	const collapsed = useAppSelector(sideBarSelector.selectMainSidebarCollapse);
	const show = useAppSelector(sideBarSelector.selectShowSidebar);
	const userInfo = useAppSelector(authSelectors.selectCurrentUser);

	const activeKey = useActiveKey(props);

	const routes = useRoutesTabs(InAppNavItems);

	if (!userInfo?.id) {
		return null;
	}

	return (
		<>
			<Affix>
				<div className={"sidebar-container"}>
					<Sider
						{...rest}
						theme={"light"}
						className={classNames({
							_sidebar: true,
							_sidebar_collapsed: collapsed,
						})}
						collapsible
						collapsed={collapsed}
						style={{
							overflow: "auto",
							// position: 'fixed',
							left: 0,
							top: 0,
							bottom: 0,
							display: show ? "block" : "none",
						}}
						trigger={null}
					>
						<Menu
							theme={`light`}
							defaultSelectedKeys={[isTabNav(props[0]) ? props[0].key : "1"]}
							selectedKeys={[activeKey || "1"]}
							defaultOpenKeys={[activeKey && ["STAFFS","ROLE", "PERMISSION"].includes(activeKey) ? "AUTHORIZATION" : "" ]}
							mode={`inline`}
						>
							{routes}
						</Menu>
					</Sider>
					<ClassSideBar />
					<CourseSidebar />
				</div>
			</Affix>
		</>
	);
};

export default React.memo(SideBar);

// {

//     const collapsed = useAppSelector(sideBarSelector.selectMainSidebarCollapse);
//     const show = useAppSelector(sideBarSelector.selectShowSidebar);
//     const userInfo = useAppSelector(authSelectors.selectCurrentUser);

//     if (!userInfo?.id) {
//         return null;
//     }

//     return (
//         <>
//             <Affix>
//                 <div className={"sidebar-container"}>
//                     <Sider {...rest} theme={"light"} className={"_sidebar"} collapsible collapsed={collapsed}                           style={{
//                                overflow: 'auto',
//                                // position: 'fixed',
//                                left: 0,
//                                top: 0,
//                                bottom: 0,
//                                display: show ? "block" : "none"
//                            }}
//                            trigger={null}
//                     >
//                         <Menu theme={`light`} defaultSelectedKeys={[isTabNav(props[0]) ? props[0].key : "1"]} mode={`inline`}>
//                            <RouteTabs routes={InAppNavItems}/>
//                         </Menu>
//                     </Sider>
//                     <ClassSideBar/>
//                     <CourseSidebar/>
//                 </div>
//             </Affix>
//         </>
//     );
// }
