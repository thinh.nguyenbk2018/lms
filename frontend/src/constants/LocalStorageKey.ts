const LOCAL_STORAGE_KEYS = {
	USER_AUTH: "USER_AUTH",
	TOKEN: "token",
	USER_ID: "userId",
	TENANT: "tenant",
	LOGO: "logo",
	BANNER: "banner",
	TENANT_NAME: "tenantName",
	REFRESH_TOKEN: "refreshToken",
	ACCOUNT_TYPE: "ACCOUNT_TYPE",
};
export default LOCAL_STORAGE_KEYS;
