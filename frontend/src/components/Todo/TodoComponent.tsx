import './Todo.less';
export type Todo = {
    title: string;
    content: string;
};

const TodoComponent = (props: Todo) => {
    const { title, content } = props;
    return (
        <div className="todo-container">
            <h1 className="todo-title"> {title}</h1>
            <div className="todo-content">
                <p className="todo-paragraph">{content}</p>
            </div>
        </div>
    );
};

export default TodoComponent;
