import React, {Ref, useImperativeHandle, useRef, useState} from "react";
import {Modal} from "antd";
import {ExclamationCircleOutlined} from "@ant-design/icons";
import './DeleteConfirmation.less';

type Payload = {
    onOk?(): void;
    onCancel?(): void;
    type: string;
    name: string;
};

export type DeleteConfirmationRefType = {
    show(payload: Payload): void;
};

export const DeleteConfirmation = React.forwardRef((props: any, ref: Ref<DeleteConfirmationRefType>) => {
    const [visible, setVisible] = useState(false);
    const payloadRef = useRef<Payload>({type: "", name: ""});

    useImperativeHandle(
        ref,
        () => ({
            show: payload => {
                payloadRef.current = payload;
                setVisible(true);
            }
        })
    );

    const wrapWithClose = (method?: () => void) => () => {
        setVisible(false);
        method && method();
    };

    return (
        <Modal
            title={
                <>
                    {<ExclamationCircleOutlined/>}
                    <span className={'confirm-delete-modal-title-name'}>Xóa {payloadRef.current?.type}. Bạn có chắc chắn?</span>
                </>}
            visible={visible}
            onOk={wrapWithClose(payloadRef.current.onOk)}
            onCancel={wrapWithClose(payloadRef.current.onCancel)}
        >
        </Modal>
    );
});

export const confirmationModal = (type: string, name: string, ok: () => void, suffix?: string) => {
    Modal.confirm({
        wrapClassName: 'confirm-delete-modal-wrapper',
        title: (
            <span className={'confirm-delete-modal-title-name'}>Xóa {type}{suffix ? ' ' + suffix + '.' : '.'} Bạn có chắc chắn?</span>
        ),
        content: (
            <span>Bạn đang xóa {type} <span
                className={'confirm-delete-modal-subject-name'}>{name}</span> {suffix}, hành động này không thể hoàn tác.</span>
        ),
        onOk: () => {
            ok();
        },
        icon: <ExclamationCircleOutlined/>,
        okText: `Xóa ${type}`,
        cancelText: 'Hủy',
        okButtonProps: {className: 'confirm-delete-modal-ok-btn'},
        cancelButtonProps: {className: 'confirm-delete-modal-cancel-btn'}
    })
}