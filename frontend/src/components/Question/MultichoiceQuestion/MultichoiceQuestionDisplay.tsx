import React from "react";
import { Card, Checkbox, Col, Divider, Radio, Row, Typography, } from "antd";
import { MultichoiceQuestion } from "../../../redux/features/Quiz/CreateQuizSlice";
import TextEditor from "../../TextEditor/TextEditor";
import "./MultichoiceQuestionDisplay.less";
import AttachmentRender from "../Attachment/AttachmentRender";
import { Attachment } from "../Attachment/AttachmentQuestion";

// TODO: Add logic and remove the default test data from the props
const MultichoiceQuestionDisplay = ({
    props,
}: {
    props: MultichoiceQuestion;
}) => {

    // const onChange: ((e: RadioChangeEvent) => void) | undefined = (e) => {
    //     console.log("No thing");
    // };
    //
    // const correctAnswer = props.answerList?.find(
    //     (item) => item.isCorrect
    // )!.content;

    return (
        <>
            <Card className={'multi-choices-question-display-wrapper'}>
                {
                    JSON.parse(props?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                        <AttachmentRender attachment={a} />
                    </Row>)
                }
                <Divider />
                <Row justify="center">
                    <Col span={24} className={"question-box"}>
                        <TextEditor
                            onChange={() => {
                            }}
                            value={props.description}
                            readOnly={true}
                            useBubbleTheme
                        />
                    </Col>
                </Row>
                <Divider />
                <Row>
                    <Col className="answer-box">
                        {props.answerList.map((item: any) => {
                            return props.isMultipleAnswer ? (
                                <Checkbox key={item.content}
                                    checked={item.isCorrect}
                                >
                                    <TextEditor
                                        value={item.content}
                                        onChange={() => {
                                        }}
                                        readOnly={true}
                                        useBubbleTheme={true}
                                    />
                                </Checkbox>
                            ) : (
                                <Radio key={item.content}
                                    checked={item.isCorrect}
                                >
                                    <Typography.Text>
                                        <TextEditor
                                            value={item.content}
                                            onChange={() => {
                                            }}
                                            readOnly={true}
                                            useBubbleTheme={true}
                                        />
                                    </Typography.Text>
                                </Radio>
                            );
                        })}
                    </Col>
                </Row>
            </Card>
        </>
    );
};

export default MultichoiceQuestionDisplay;
