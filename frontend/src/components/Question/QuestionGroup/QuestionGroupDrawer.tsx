import { ExclamationCircleOutlined } from "@ant-design/icons";
import CancelButton from "@components/ActionButton/CancelButton";
import DoneButton from "@components/ActionButton/DoneButton";
import TextEditorField from "@components/CustomFields/TextEditorField/TextEditorField";
import useQuestionDrawerType from "@hooks/QuizDrawer/useQuestionDrawerType";
import { useAppDispatch, useAppSelector } from "@hooks/Redux/hooks";
import {
	createTestContentActions,
	createTestContentSelectors,
	isQuestionGroup,
	QuestionGroup
} from "@redux/features/Quiz/CreateQuizSlice";
import { DEFAULT_FRONTEND_ID } from "@redux/interfaces/types";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { Col, Drawer, Form, Modal, Row, Space, Typography } from "antd";
import { Field, Formik } from "formik";
import { useRef } from "react";
import QUESTION_LABEL_CONSTANT from "../constant/QuestionLabelConstant";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import { baseCreateQuestionYupSchema, baseEditQuestionYupSchema } from "../QuestionContainerItem/baseQuestionSchema";
import * as Yup from "yup";
import './QuestionGroupDrawer.less';
import AttachmentQuestion from "../Attachment/AttachmentQuestion";

let initialValues: Partial<QuestionGroup> = {
	id: DEFAULT_FRONTEND_ID,
	description: DEFAULT_TEXT_EDITOR_HTML,
	point: 0,
	note: "",
};

const QuestionGroupDrawer = () => {
	const {
		visibleGroupQuestion: visible,
		openDrawer,
		isEditing,
		doneHandler,
		isFetching,
		isError,
		isSuccess,
		questionIdInDraft,
		testName,
	} = useQuestionDrawerType("GROUP");

	let questionGroupInDraft = useAppSelector(createTestContentSelectors.selectQuestionGroupInDraft)
	let openingDrawer = useAppSelector(createTestContentSelectors.selectOpeningDrawer)

	const closeDrawer = () => {
		dispatch(createTestContentActions.doneDraftingQuestionGroup())
	}

	const dispatch = useAppDispatch();
	//* The container for the modal to pop up
	const drawerRef = useRef<HTMLDivElement | null>(null);

	function closeConfirm(onOk: () => void, onCancel: () => void) {
		Modal.confirm({
			title: "Xác nhận hủy bỏ câu hỏi",
			icon: <ExclamationCircleOutlined />,
			content:
				"Chúng tôi nhận thấy rằng bạn đã soạn phần nào câu hỏi. Chọn xác nhận sẽ xóa hết tất cả.",
			okText: "Xác nhận",
			cancelText: "Tiếp tục soạn",
			onOk: onOk,
			onCancel: onCancel,
			getContainer: drawerRef.current!!,
		});
	}
	if (questionGroupInDraft && !isQuestionGroup(questionGroupInDraft)) {
		AntdNotifier.error(
			"Lỗi khi load câu hỏi, vui lòng refresh lại hoặc contact chúng tôi"
		);
		return null;
	}

	return (
		<>
			<Formik
				enableReinitialize={true}
				initialValues={
					{
						id: questionGroupInDraft?.id || DEFAULT_FRONTEND_ID,
						description:
							questionGroupInDraft?.description || DEFAULT_TEXT_EDITOR_HTML,
						point: questionGroupInDraft?.point || 0,
						note: questionGroupInDraft?.note || "",
						attachment: questionGroupInDraft.attachment || '[]'
					} ?? initialValues
				}
				validationSchema={Yup.object({
					description: Yup.string().required("Mô tả câu hỏi là thông tin bắt buộc")
						.test("required field", "Mô tả câu hỏi là thông tin bắt buộc", (value: any) => {
							return !(value === '<p><br></p>' || value === '' || value == undefined);

						}),
				}).concat(
					isEditing ? baseEditQuestionYupSchema : baseCreateQuestionYupSchema
				)}
				onSubmit={(values) => {
					const valuesToSubmit: Omit<QuestionGroup, "order"> = {
						type: "GROUP",
						...values,
						questionList: []
					};
					doneHandler(valuesToSubmit);
				}}
			>
				{({
					dirty,
					isValid,
					handleSubmit,
					setFieldValue,
					values,
					errors,
					resetForm,
				}) => {
					return (
						<Form layout={"vertical"}>
							<Drawer
								maskClosable={false}
								closable={false}
								title={
									<Space direction={"vertical"}>
										<Typography.Text>
											{`${testName || "Test Nhẹ kiến thức"}`}
										</Typography.Text>
										<Typography.Text strong>
											Nhóm câu hỏi
										</Typography.Text>
									</Space>
								}
								placement="right"
								size={"large"}
								visible={visible}
								extra={
									<Space>
										<CancelButton
											onClick={() => {
												if (dirty) {
													closeConfirm(
														() => {
															closeDrawer();
															setFieldValue("description", "");
														},
														() => {
														}
													);
												} else {
													closeDrawer();
												}
											}
											}
										/>
										<DoneButton
											disabled={!dirty}
											onClick={() => handleSubmit()}
										/>
									</Space>
								}
								className={"MULTI_CHOICE-drawer"}
							>
								<>
									<div ref={drawerRef}></div>
									<Row className={'question-drawer-instruction'}>
										<Typography.Paragraph className={'question-group-instruction'}>
											Phần mô tả bên dưới được dùng chung cho tất cả các câu hỏi trong nhóm.
										</Typography.Paragraph>
									</Row>
									<Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
										<Col span={24}>
											<Field
												name={"attachment"}
												placeholder={"Type article content"}
												component={AttachmentQuestion}
												label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
											/>
										</Col>
									</Row>
									<Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
										<Field
											name={"description"}
											placeholder={"Type article content"}
											component={TextEditorField}
											label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
										/>
									</Row>
								</>
							</Drawer>
						</Form>
					);
				}}
			</Formik>
		</>
	);
}

export default QuestionGroupDrawer;