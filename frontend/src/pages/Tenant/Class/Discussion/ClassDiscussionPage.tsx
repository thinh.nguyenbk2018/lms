import {Space} from "antd";
import React from "react";
import {useParams} from "react-router-dom";
import AddButton from "../../../../components/ActionButton/AddButton";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import EditCommentModal from "../../../../components/Forum/Form/EditCommentModal";
import NewThreadFormModal from "../../../../components/Forum/Form/NewThreadFormModal";
import ForumThreadList from "../../../../components/Forum/ForumThreadList";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import {
	classDiscussionActions,
	classDiscussionSelectors
} from "@redux/features/Class/ClassDiscussion/ClassDiscussionSlice";
import TableSearch from "@components/Search/TableSearch";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";

const ClassDiscussionPage = () => {
	const { classId } = useParams();
	// const isAddingPost =
	const [closeSidebar] = useClassSidebar();

	const dispatch = useAppDispatch();

	const searchCriteria = useAppSelector(
		classDiscussionSelectors.selectSearchCriteria
	);
	const changeSearchCriteriaAction = classDiscussionActions.changeCriteria;

	const { classInfo } = useClassCourseInformation(Number(classId));

	return (
		<>
			<ContentHeader
				title={"Diễn đàn thảo luận"}
				action={
					<Space>
						<TableSearch
							searchFields={[{ title: 'Từ khóa', key: 'key' }]}
							defaultFilters={[]}
							searchCriteria={searchCriteria}
							changeCriteria={changeSearchCriteriaAction}
						/>
						{classInfo?.status != 'ENDED' && <AddButton
							onClick={() => {
								dispatch(classDiscussionActions.addingNewPost());
							}}
						/>}
					</Space>
				}
			/>
			<ForumThreadList />
			<NewThreadFormModal />
			<EditCommentModal />
		</>
	);
};

export default ClassDiscussionPage;
