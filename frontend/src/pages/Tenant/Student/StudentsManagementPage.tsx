import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import { Button, Dropdown, Menu } from "antd";
import { Link } from "react-router-dom";
import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import StudentsManagement from "../../../components/Students/StudentsManagement";
import TableSearch from "../../../components/Search/TableSearch";
import { addIconSvg } from "@components/Icons/SvgIcons";
import { changeCriteria, studentSelector } from "@redux/features/Student/StudentSlice";
import { useAppSelector } from "@hooks/Redux/hooks";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import IconSelector from "@components/Icons/IconSelector";
import { useRef, useState } from "react";
import { useImportStudentsMutation } from "@redux/features/Student/StudentAPI";
import { API_CONSTANT } from "@constants/ApiConfigs";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

const StudentsManagementPage = () => {
    let importInputRef = useRef<HTMLInputElement>(null);
    let downloadReportRef = useRef<HTMLAnchorElement>(null);

    let searchCriteria = useAppSelector(studentSelector.selectSearchCriteria);
    let [importing, setImporting] = useState(false);

    let [importStudents] = useImportStudentsMutation();

    const uploadFile = (file: File) => {
        if (file.type !== "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            AntdNotifier.error("Loại file vừa chọn không đúng, vui lòng thử lại.");
            return;
        }
        setImporting(true);
        let formData = new FormData();
        formData.append("file", file);
        fetch(API_CONSTANT.BASE_URL + "/students/import", {
            method: "POST",
            headers: {
                authorization: "Bearer " + localStorage.getItem("token")
            },
            body: formData
        }).then(res => {
            if (res.ok) {
                return res.blob()
            } else {
                throw new Error("File không hợp lệ vui lòng kiểm tra lại")
            }
        }).then(blob => {
            let src = URL.createObjectURL(blob)
            downloadReportRef.current!.href = src;
            downloadReportRef.current!.click();
            setImporting(false);
            importStudents()
        }).catch(error => {
            AntdNotifier.error(error.message);
            setImporting(false);
        })
    }

    const menu = (
        <Menu>
            <Menu.Item key={1} onClick={() => console.log("File import mẫu")}>
                <a href={API_CONSTANT.BASE_STATIC_URL + "/api/students.xlsx"} download={"students.xlsx"}>
                    Tải file mẫu
                </a>
            </Menu.Item>
        </Menu>
    );

    return <>
        <ContentHeader
            title="Học viên"
            action={
                <div className={'search-container'}>
                    <TableSearch
                        searchFields={[{ title: 'Từ khóa', key: 'key' }]}
                        defaultFilters={[]}
                        changeCriteria={changeCriteria}
                        searchCriteria={searchCriteria}
                    />
                    <PermissionRenderer permissions={[PermissionEnum.ADD_STUDENT]}>
                        <Dropdown.Button
                            type="primary"
                            style={{ backgroundColor: "#52c41a", borderColor: "#52c41a", marginRight: 10 }}
                            className={'button-with-icon'}
                            loading={importing}
                            onClick={() => {
                                importInputRef.current?.click();
                            }}
                            overlay={menu}
                        >
                            <IconSelector type="ImportOutlined" />
                            <span>Nhập từ files</span>
                            <input
                                placeholder="#"
                                ref={importInputRef}
                                type={"file"}
                                name="file"
                                style={{ visibility: "hidden", width: 1 }}
                                accept={".xlsx,.xlsm,.xls"}
                                onChange={({ target: { files } }) => {
                                    if (files)
                                        uploadFile(files[0]);
                                }}
                            />
                        </Dropdown.Button>
                    </PermissionRenderer>
                    <PermissionRenderer permissions={[PermissionEnum.ADD_STUDENT]}>
                        <Link to={ROUTING_CONSTANTS.CREATE_STUDENT}>
                            <Button type="primary" className={'button-with-icon'}>
                                {addIconSvg}
                                <span>Thêm</span>
                            </Button>
                        </Link>
                    </PermissionRenderer>
                </div>
            }
        />
        <StudentsManagement />
        <a ref={downloadReportRef} download={"import_students_report.xlsx"} href="#" />
    </>
}

export default StudentsManagementPage;