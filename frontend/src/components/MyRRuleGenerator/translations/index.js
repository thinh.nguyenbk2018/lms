import english from './english';
import german from './german';
import vietnamese from './vietnamese';

export default { english, german, vietnamese };
