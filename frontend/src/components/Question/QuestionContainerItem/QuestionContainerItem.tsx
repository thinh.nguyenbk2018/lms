import {Card, Space, Tooltip, Typography} from "antd";
import React from "react";
import {createTestContentActions, QuestionDataType,} from "../../../redux/features/Quiz/CreateQuizSlice";
import {CloseOutlined, EditOutlined} from "@ant-design/icons";
import "./QuestionContainerItem.less";
import MultichoiceQuestionDisplayWithForm from "../MultichoiceQuestion/MultichoiceQuestionDisplayWithForm";
import FillInBlankDisplayWithForm from "../FillBlankQuestion/FillInBlankQuestionDisplayWithForm";
import BlankWithChoicesQuestionDisplayWithForm
    from "../BlankWithChoicesQuestion/BlankWithChoicesQuestionDisplayWithForm";
import WrittenQuestionDisplayWithForm from "../WrittenQuestion/WrittenQuestionDisplayWithForm";
import DragAndDropQuestionDisplayWithForm from "../DragAndDropQuestion/DragAndDropQuestionDisplayWithForm";
import QuestionGroupDisplay from "../QuestionGroup/QuestionGroupDisplay";
import MultichoiceQuestionDisplay from "../MultichoiceQuestion/MultichoiceQuestionDisplay";
import FillInBlankQuestionDisplay from "../FillBlankQuestion/FillInBlankQuestionDisplay";
import BlankWithChoicesQuestionDisplay from "../BlankWithChoicesQuestion/BlankWithChoicesQuestionDisplay";
import WrittenQuestionDisplay from "../WrittenQuestion/WrittenQuestionDisplay";
import {useAppDispatch} from "../../../hooks/Redux/hooks";
import antdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import {ErrorBoundary} from "react-error-boundary";
import {
    QuestionAnswer,
    useFlagQuestionMutation,
    useSubmitStudentAnswerMutation,
    useUnFlagQuestionMutation,
} from "../../../redux/features/Quiz/examAPI";
import useClassCourseInformation from "../../../hooks/Routing/useClassCourseInformation";
import {useParams} from "react-router-dom";
import DragAndDropQuestionDisplay from "@components/Question/DragAndDropQuestion/DragAndDropQuestionDisplay";
import {flagSvgIcon} from "@components/Icons/SvgIcons";

type QuestionContainerProps = {
    questionData: QuestionDataType | undefined;
    canEdit?: boolean;
    onEditCallBack?: () => void;
    onDeleteCallBack?: () => void;
    mode: "view" | "doing";
    order?: number;
    flagged?: boolean;
    inGroup?: boolean;
    groupIndex?: number;
};
const QuestionInDoingMode = ({question, groupIndex}: {question: QuestionDataType, groupIndex: number | undefined}) => {
    const [submitAnswer] = useSubmitStudentAnswerMutation();

    const {
        classId: classIdFromPath,
        quizId,
        testSessionId: sessionId,
    } = useParams();

    const {classId, courseId, isFetchingClass, isSuccessClass} =
        useClassCourseInformation(Number(classIdFromPath));
    const doingProps = {
        mode: "doing" as const,
        onConfirmSubmit: (answers: QuestionAnswer) => {
            if (!sessionId || !classId || !courseId || !question.id) {
                antdNotifier.error("Không đủ thông tin để submit câu trả lời");
                return;
            }
            submitAnswer({
                studentAnswer: answers,
                testSessionId: sessionId,
                classId,
                courseId,
                quizId: Number(quizId),
                questionId: question.id,
            });
        },
    };

    const writingDoingProps = {
        mode: "doing" as const,
        onConfirmSubmit: (answers: QuestionAnswer) => {
            if (!sessionId || !classId || !courseId || !question.id) {
                antdNotifier.error("Không đủ thông tin để submit câu trả lời");
                return;
            }
            AntdNotifier.handlePromise(
                submitAnswer({
                    studentAnswer: answers,
                    testSessionId: sessionId,
                    classId,
                    courseId,
                    quizId: Number(quizId),
                    questionId: question.id,
                }).unwrap(), "Trả lời câu hỏi"
            );
        },
    };

    const renderQuestionAnswerBox: () => JSX.Element = () => {
        // console.log('question and type: ', question, question.type);
        switch (question.type) {
            case "MULTI_CHOICE":
                console.log('questionID: ', question.id);
                return (
                    <MultichoiceQuestionDisplayWithForm {...question} {...doingProps} />
                );
            case "FILL_IN_BLANK":
                return <FillInBlankDisplayWithForm {...question} {...doingProps} />;
            case "FILL_IN_BLANK_WITH_CHOICES":
                return (
                    <BlankWithChoicesQuestionDisplayWithForm
                        {...question}
                        {...doingProps}
                    />
                );
            case "WRITING":
                return <WrittenQuestionDisplayWithForm {...question} {...writingDoingProps} />;
            case "FILL_IN_BLANK_DRAG_AND_DROP":
                return (
                    <DragAndDropQuestionDisplayWithForm {...question} {...doingProps} />
                );
            case "GROUP":
                return <QuestionGroupDisplay groupIndex={groupIndex} {...question} mode={"doing"}/>;
            default:
                return <></>;
        }
    };
    const renderQuestion: () => JSX.Element = () => {
        if (isSuccessClass) {
            return renderQuestionAnswerBox();
        } else return <></>;
    };
    return <div>{renderQuestion()}</div>;
};

const QuestionContainerItem = ({
                                   questionData,
                                   canEdit = false,
                                   onEditCallBack,
                                   onDeleteCallBack,
                                   mode,
                                   order,
                                   flagged,
                                   inGroup,
                                   groupIndex
                               }: QuestionContainerProps) => {
    const questionToRender = questionData;

    const dispatch = useAppDispatch();

    const [flagQuestion] = useFlagQuestionMutation();
    const [unFlagQuestion] = useUnFlagQuestionMutation();

    const {
        classId: classIdFromPath,
        quizId,
        testSessionId: sessionId,
    } = useParams();

    function renderQuestion(
        question: QuestionDataType | undefined,
        mode: "view" | "doing"
    ) {
        if (!question) {
            return <p>Không có câu hỏi</p>;
        }
        if (mode === "doing") {
            return <QuestionInDoingMode question={question} groupIndex={order} />;
        } else if (mode === "view") {
            switch (question.type) {
                case "MULTI_CHOICE":
                    return <MultichoiceQuestionDisplay props={question}/>;
                case "FILL_IN_BLANK":
                    return <FillInBlankQuestionDisplay props={question}/>;
                case "FILL_IN_BLANK_WITH_CHOICES":
                    return <BlankWithChoicesQuestionDisplay props={question}/>;
                case "WRITING":
                    return <WrittenQuestionDisplay mode={"view"} props={question}/>;
                case "FILL_IN_BLANK_DRAG_AND_DROP":
                    return <DragAndDropQuestionDisplay {...question}/>;
                case "GROUP":
                    return <QuestionGroupDisplay groupIndex={order} {...question} mode={mode}/>;
            }
        }
        return;
    }

    return (
        <span className={`question-card`}>
			<Card
                type="inner"
                title={
                    <Typography.Text className={'question-card-title'}>
                        {`Câu hỏi ${inGroup ? groupIndex + '.' + order : order}`}
                    </Typography.Text>
                }
                extra={
                    <Space>
                        <Typography.Text className={'question-card-title'}>
                            {`${questionToRender?.point ?? "0"} điểm`}
                        </Typography.Text>
                        {!inGroup &&
                            mode === "doing" && <span
                                className={"flag-question-taking-quiz"}
                                onClick={() => {
                                    // console.log('flag: ', flagged);
                                    if (!flagged && sessionId && questionToRender?.id) {
                                        flagQuestion({sessionId, questionId: questionToRender?.id})
                                    } else if (flagged && sessionId && questionToRender?.id) {
                                        unFlagQuestion({sessionId, questionId: questionToRender?.id})
                                    }
                                }}
                            >
                            <span
                                className={`flag-question-taking-quiz-icon ${flagged ? 'flag-question-taking-quiz-icon-red' : ''}`}>
                                {flagSvgIcon}
                            </span>
                            <span
                                className={'question-card-title'}>{flagged ? 'Hủy bỏ cờ' : 'Đặt cờ cho câu hỏi'}</span>
                        </span>
                        }
                        {!!canEdit && mode === "view" && (
                            <Tooltip title={"Chỉnh sửa câu hỏi"}>
                                <EditOutlined
                                    style={{color: "#c91c00"}}
                                    onClick={() => {
                                        if (!questionToRender || !questionToRender?.id) {
                                            antdNotifier.error(
                                                "Không tìm thấy câu hỏi để chỉnh sửa"
                                            );
                                        } else {
                                            if (questionToRender.type === "GROUP") {
                                                dispatch(
                                                    createTestContentActions.editQuestionGroup(questionToRender)
                                                );
                                            } else {
                                                // console.log('dispatch question on update: ', questionToRender.id)
                                                dispatch(
                                                    createTestContentActions.editQuestionWithDrawer({
                                                        questionId: questionToRender.id,
                                                        type: questionToRender.type,
                                                    })
                                                );
                                            }
                                            onEditCallBack?.();
                                        }
                                    }}
                                />
                            </Tooltip>
                        )}
                        {mode === "view" && <Tooltip title={"Xóa câu hỏi"}>
                            <CloseOutlined
                                style={{color: "#c91c00"}}
                                onClick={() => {
                                    onDeleteCallBack?.();
                                }}
                            />
                        </Tooltip>}
                    </Space>
                }
                bodyStyle={{display: "none"}}
            />
			<ErrorBoundary
                FallbackComponent={() => {
                    return <h1> Error Loading this question </h1>;
                }}
                onError={(error, info) => {
                    console.log("ERRROR RENDERING QUESTION", error);
                    console.log("ERRROR RENDERING QUESTION INFO:", info);
                }}
            >
				{renderQuestion(questionToRender, mode)}
			</ErrorBoundary>
		</span>
    );
};

export default QuestionContainerItem;
