import { useLocation, useRoutes } from "react-router-dom";
import React, { useEffect } from "react";
import LOCAL_STORAGE_KEYS from "../constants/LocalStorageKey";
import { HomeNavItems, NavigationItem, TenantNavItems } from "./NavigationItem";
import { authActions, authSelectors } from "../redux/features/Auth/AuthSlice";
import { useAppDispatch, useAppSelector } from "../hooks/Redux/hooks";

export const RouterConfig = () => {
	const dispatch = useAppDispatch();
	let permissionsMap = useAppSelector(authSelectors.selectUserPermissions);

	let allRoute = document.location.host.split(".").length === 3
		? TenantNavItems
		: HomeNavItems

	const filterPermission = (routes: NavigationItem[]) => {
		return routes.filter(route => route.required_permission.length === 0 || (permissionsMap && route.required_permission.some(permission => permissionsMap && permissionsMap[permission])))
		.map((route): NavigationItem => {
			if (route.children) {
				return  {
					...route,
					children: filterPermission(route.children)
				}
			} else return route;
		})
	}

	let routesFiltered = filterPermission(allRoute);

	let routes = useRoutes(routesFiltered);
	useEffect(() => {
		const localUser = localStorage.getItem(LOCAL_STORAGE_KEYS.USER_AUTH);
		if (localUser) {
			dispatch(authActions.setUserFromLocalStorage(JSON.parse(localUser)));
		}
		return () => { };
	}, []);
	return routes;
};
