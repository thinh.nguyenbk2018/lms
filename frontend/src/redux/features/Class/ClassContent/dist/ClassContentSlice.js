"use strict";
exports.__esModule = true;
exports.classContentActions = exports.classContentSelectors = exports.classContentSlice = exports.CLASS_CONTENT_SLICE_KEY = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
var initialState = {
    chapterInEdit: undefined,
    activityModalToShow: "NONE",
    activityBeingUpdated: undefined,
    editingChapterTitle: false,
    votingOptionInEdit: undefined
};
exports.CLASS_CONTENT_SLICE_KEY = "classContentSlice";
exports.classContentSlice = toolkit_1.createSlice({
    name: exports.CLASS_CONTENT_SLICE_KEY,
    initialState: initialState,
    reducers: {
        addingUnit: function (state, action) {
            var chapterToEdit = action.payload.chapterToEdit;
            state.chapterInEdit = chapterToEdit;
            state.activityModalToShow = "unit";
        },
        // * Content creation
        addingQuiz: function (state, action) {
            var chapterToEdit = action.payload.chapterToEdit;
            state.chapterInEdit = chapterToEdit;
            state.activityModalToShow = "quiz";
        },
        addingActivity: function (state, action) {
            var _a = action.payload, chapterToEdit = _a.chapterToEdit, type = _a.type;
            state.chapterInEdit = chapterToEdit;
            state.activityModalToShow = type;
        },
        doneAddingActivity: function (state) {
            state.chapterInEdit = undefined;
            state.activityModalToShow = "NONE";
            state.activityBeingUpdated = undefined;
        },
        // * Content updates
        updatingActivity: function (state, action) {
            var _a = action.payload, chapterToEdit = _a.chapterToEdit, activity = _a.activity;
            state.chapterInEdit = chapterToEdit;
            state.activityBeingUpdated = activity;
            state.activityModalToShow = activity.type;
        },
        // * Editing chapter title
        editingChapter: function (state, action) {
            state.chapterInEdit = action.payload;
            state.editingChapterTitle = true;
        },
        doneEditingChapter: function (state) {
            state.chapterInEdit = undefined;
            state.editingChapterTitle = false;
        },
        setVotingOptionInEdit: function (state, action) {
            state.votingOptionInEdit = action.payload;
        }
    }
});
exports.classContentSelectors = {
    selectIsAddingUnit: function (state) {
        var _a = state[exports.CLASS_CONTENT_SLICE_KEY], chapterInEdit = _a.chapterInEdit, activityModalToShow = _a.activityModalToShow;
        return chapterInEdit && activityModalToShow === "unit";
    },
    selectIsAddingQuiz: function (state) {
        var _a = state[exports.CLASS_CONTENT_SLICE_KEY], chapterInEdit = _a.chapterInEdit, activityModalToShow = _a.activityModalToShow;
        return chapterInEdit && activityModalToShow === "quiz";
    },
    selectIsAddingVoting: function (state) {
        var _a = state[exports.CLASS_CONTENT_SLICE_KEY], chapterInEdit = _a.chapterInEdit, activityModalToShow = _a.activityModalToShow;
        return chapterInEdit && activityModalToShow === "vote";
    },
    selectIsUpdatingUnit: function (state) {
        var _a = state[exports.CLASS_CONTENT_SLICE_KEY], chapterInEdit = _a.chapterInEdit, activityBeingUpdated = _a.activityBeingUpdated;
        return chapterInEdit && (activityBeingUpdated === null || activityBeingUpdated === void 0 ? void 0 : activityBeingUpdated.type) === "unit";
    },
    selectIsUpdatingQuiz: function (state) {
        var _a = state[exports.CLASS_CONTENT_SLICE_KEY], chapterInEdit = _a.chapterInEdit, activityBeingUpdated = _a.activityBeingUpdated;
        return chapterInEdit && (activityBeingUpdated === null || activityBeingUpdated === void 0 ? void 0 : activityBeingUpdated.type) === "quiz";
    },
    selectQuizInUpdate: function (state) {
        var _a;
        if (((_a = state[exports.CLASS_CONTENT_SLICE_KEY].activityBeingUpdated) === null || _a === void 0 ? void 0 : _a.type) !== "quiz") {
            return undefined;
        }
        return state[exports.CLASS_CONTENT_SLICE_KEY].activityBeingUpdated;
    },
    selectUnitInUpdate: function (state) {
        var _a;
        if (((_a = state[exports.CLASS_CONTENT_SLICE_KEY].activityBeingUpdated) === null || _a === void 0 ? void 0 : _a.type) !== "unit") {
            return undefined;
        }
        return state[exports.CLASS_CONTENT_SLICE_KEY].activityBeingUpdated;
    },
    selectVotingInUpdate: function (state) {
        var _a;
        if (((_a = state[exports.CLASS_CONTENT_SLICE_KEY].activityBeingUpdated) === null || _a === void 0 ? void 0 : _a.type) !== "vote") {
            return undefined;
        }
        return state[exports.CLASS_CONTENT_SLICE_KEY].activityBeingUpdated;
    },
    selectVotingOptionInEdit: function (state) {
        return state[exports.CLASS_CONTENT_SLICE_KEY].votingOptionInEdit;
    },
    selectChapterInEdit: function (state) {
        return state[exports.CLASS_CONTENT_SLICE_KEY].chapterInEdit;
    },
    selectIsChapterInfoBeingEdited: function (state) {
        return !!(state[exports.CLASS_CONTENT_SLICE_KEY].chapterInEdit &&
            state[exports.CLASS_CONTENT_SLICE_KEY].editingChapterTitle);
    }
};
exports.classContentActions = exports.classContentSlice.actions;
