import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Todo } from "@components/Todo/TodoComponent";
import { RootState } from "redux/store/store";

type TodoSlice = {
    todos: Todo[];
};

const initialState: TodoSlice = {
    todos: [],
};

export const TODO_SLICE_KEY = "todoSlice";
export const todoSlice = createSlice({
    name: TODO_SLICE_KEY,
    initialState,
    reducers: {
        addNewTodo: (state, action: PayloadAction<Todo>) => {
            state.todos.push(action.payload);
        },
        deleteTodo: (state, action: PayloadAction<number>) => {
            const indexToRemove = action.payload;
            if (!(indexToRemove >= 0 && indexToRemove < state.todos.length))
                return;
            state.todos.splice(indexToRemove, 0);
        },
    },
});

export const todoSelectors = {
    selectTodoList: (state: RootState) => state.todoSlice.todos,
    selectNumberOfTodos: (state: RootState) => state.todoSlice.todos.length,
};

export const todoActions = todoSlice.actions;
