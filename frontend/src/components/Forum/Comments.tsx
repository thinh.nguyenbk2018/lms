import React, {useState} from "react";
import {Avatar, Button, Card, Col, Collapse, Comment as CommentComponent, Form, Row, Space, Typography,} from "antd";
import {ReplyIconsWithText,} from "../Icons/IconWithText";
import TextEditor from "../TextEditor/TextEditor";
import {
    Comment,
    useCommentToPostMutation,
    useDeleteCommentMutation,
    useLazyGetChildCommentsOfCommentQuery,
    useReplyToCommentMutation,
    useUpdateCommentMutation,
} from "@redux/features/Class/ClassDiscussion/ClassDiscussionAPI";
import {parseDate} from "../Calendar/Utils/CalendarUtils";
import {formatRelative} from "date-fns";
import {vi} from "date-fns/locale";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {ID} from "@redux/interfaces/types";
import {useParams} from "react-router-dom";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {faCircleXmark, faPenToSquare,} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {classDiscussionActions} from "@redux/features/Class/ClassDiscussion/ClassDiscussionSlice";
import {blue, red} from "@ant-design/colors";
import {classDiscussionErrorsHanlder} from "./ForumThreadList";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import './Comment.less';
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import LoadingPage from "@pages/UtilPages/LoadingPage";

interface EditorProps {
    onChange: (e: any) => void;
    onSubmit: React.MouseEventHandler<HTMLElement> | undefined;
    onReset: () => void;
    submitting: boolean | { delay?: number | undefined } | undefined;
    value: string | undefined;
}

export const Editor = ({
                           onChange,
                           onSubmit,
                           submitting,
                           value,
                           onReset
                       }: EditorProps) => (
    <>
        <Form.Item>
            <TextEditor onChange={onChange} value={value}/>
        </Form.Item>
        <Form.Item style={{justifyItems: "end"}}>
            <Row justify={"end"} gutter={[13, 3]}>
                <Col>
                    <Button
                        htmlType="submit"
                        // loading={submitting}
                        onClick={() => {
                            onReset();
                        }}
                        type="primary"
                        danger
                    >
                        Hủy
                    </Button>
                </Col>
                <Col>
                    <Button
                        htmlType="submit"
                        loading={submitting}
                        onClick={onSubmit}
                        type="primary"
                    >
                        Bình luận
                    </Button>
                </Col>
            </Row>
        </Form.Item>
    </>
);

type CommentEditorProps = {
    classId: number;
    postId: number;
    // ! Please dont pass in both of the ID at the same time
    /** pass this in to comment to post */
    postIdToCommentTo?: ID | undefined;
    /** Pass this in to reply to a comment  */
    commentIdToReply?: ID | undefined;
};

export const CommentEditor = (props: CommentEditorProps) => {
    const {classId, postId, postIdToCommentTo, commentIdToReply} = props;

    const [content, setContent] = useState("");
    const [submit, setSubmitting] = useState(false);
    const [childCommentPage, setChildCommentPage] = useState(1);

    // * Redux
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);
    // * RTK Queries
    const [commentToPost] = useCommentToPostMutation();
    const [replyComment] = useReplyToCommentMutation();

    const onSubmit: React.MouseEventHandler<HTMLElement> | undefined = (e) => {
        if (isNaN(classId) || isNaN(postId)) {
            AntdNotifier.error("Hiện tại bạn không thể bình luận");
        }
        //* Send to backend and append comment if success
        if (postIdToCommentTo) {
            AntdNotifier.handlePromise(
                commentToPost({classId, postId, content}).unwrap(),
                "Bình luận"
            );
        } else if (commentIdToReply) {
            AntdNotifier.handlePromise(
                replyComment({
                    classId,
                    postId,
                    parentCommentId: commentIdToReply,
                    content,
                }).unwrap(),
                "Trả lời bình luận"
            );
        }
        setContent("");
    };
    const onChange = (value: string) => {
        setContent(value);
    };

    return (
        <CommentComponent
            avatar={
                <Avatar
                    src={currentUser?.avatar || "https://joeschmoe.io/api/v1/random"}
                    alt="Han Solo"
                />
            }
            content={
                <Editor
                    onChange={onChange}
                    onSubmit={onSubmit}
                    submitting={submit}
                    onReset={() => setContent("")}
                    value={content}
                />
            }
        />
    );
};

const {Panel} = Collapse;

export const CommentItem = (props: Comment & { postId: number }) => {
    const {classId} = useParams();
    const {postId} = props;
    const comment = props;
    const {createdBy: creator, createdAt, id} = comment;

    const [showChildComments, setShowChildComments] = useState(false);
    const [childCommentPage, setChildCommentPage] = useState(1);
    const [childCommentSize, setChildCommetnSize] = useState(DEFAULT_PAGE_SIZE);

    const dispatch = useAppDispatch();
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);

    // const {
    //     data: childCommentList,
    //     isSuccess,
    //     isFetching,
    //     isError,
    //     error,
    // } = useGetChildCommentsOfCommentQuery(
    //     {
    //         classId: Number(classId),
    //         postId: props.postId,
    //         commentId: id,
    //         childCommentPagination: {
    //             page: childCommentPage,
    //             size: childCommentSize,
    //         },
    //     },
    //     {
    //         skip: isNaN(Number(classId)) || isNaN(id) || isNaN(postId),
    //     }
    // );

    const [fetchComments,
        {
            data: childCommentList,
            isLoading: isFetchCommentsLoading
        }] = useLazyGetChildCommentsOfCommentQuery();

    const [updateComment] = useUpdateCommentMutation();
    const [deleteComment] = useDeleteCommentMutation();

    // if (isError && error) {
    //     classDiscussionErrorsHanlder(error);
    //     return <ApiErrorRetryPage/>;
    // }

    if (isFetchCommentsLoading) {
        return (
            <LoadingPage/>
        );
    }

    return (
        <>
            <Card
                title={
                    <Row justify={"start"} align={"middle"} gutter={[5, 10]}>
                        <Space>
                            <Avatar src={creator.avatar}/>
                            <Typography.Text>{creator.lastName + ' ' + creator.firstName}</Typography.Text>
                        </Space>
                    </Row>
                }
                headStyle={{
                    backgroundColor: blue[1],
                }}
                extra={
                    <Space>
                        <Typography.Text>
                            {formatRelative(parseDate(createdAt), new Date(), {locale: vi})}
                        </Typography.Text>
                        {currentUser?.id === creator.id ? (
                            <Space>
                                <FontAwesomeIcon
                                    className={'comment-edit-icon'}
                                    icon={faPenToSquare}
                                    style={{color: blue[6]}}
                                    onClick={() => {
                                        dispatch(
                                            classDiscussionActions.editComment({
                                                commentToEdit: comment,
                                                postIdOfComment: postId,
                                            })
                                        );
                                    }}
                                />
                                <FontAwesomeIcon
                                    className={'comment-delete-icon'}
                                    icon={faCircleXmark}
                                    style={{color: red[6]}}
                                    onClick={() => {
                                        confirmationModal('bình luận', '', () => {
                                            AntdNotifier.handlePromise(
                                                deleteComment({
                                                    classId: Number(classId),
                                                    postId: postId,
                                                    commentId: id,
                                                }).unwrap(),
                                                "Xóa bình luận",
                                                () => {
                                                },
                                                classDiscussionErrorsHanlder
                                            );
                                        });
                                    }}
                                />
                            </Space>
                        ) : (
                            <></>
                        )}
                    </Space>
                }
                actions={[
                    <Row>
                        <Space style={{margin: `0 1em`}}>
                            {/* <UpvoteIconWithState upvoted={comment.personalPreference === "upvoted"}
			                                     numOfUpvotes={comment.upVotesCount}
			                                     onClick={() => {
			                                     }}/>
			                <DownvoteIconWithState downvoted={comment.personalPreference === "downvoted"}
			                                       numOfDownvotes={comment.upVotesCount}
			                                       onClick={() => {
			                                       }}/> */}

                            <ReplyIconsWithText
                                numOfDiscussion={comment?.numberOfChildrenComments || 0}
                                isOpening={showChildComments}
                                onClick={() => {
                                    fetchComments({
                                        classId: Number(classId),
                                        postId: props.postId,
                                        commentId: id,
                                        childCommentPagination: {
                                            page: childCommentPage,
                                            size: childCommentSize,
                                        },
                                    });
                                    setShowChildComments(!showChildComments);
                                }}
                            />
                        </Space>
                    </Row>,
                ]}
            >
                <TextEditor
                    useBubbleTheme={true}
                    readOnly={true}
                    value={comment.content}
                />
            </Card>
            {showChildComments && (
                <Collapse
                    className={`comment`}
                    // defaultActiveKey={postData.comments?.listData?.[0].id}
                    ghost
                    style={{
                        display: showChildComments ? "block" : "none",
                        margin: "2.5em",
                    }}
                >
                    {childCommentList &&
                        childCommentList.listData.length > 0 &&
                        childCommentList.listData.map((comment) => {
                            return (
                                // <Panel key={comment.id} header={`Bình luận #${comment.id}`}>
                                <CommentItem postId={postId} {...comment} />
                                // </Panel>
                            );
                        })}

                    {childCommentList && childCommentList?.totalPages > childCommentPage && (
                        <Row justify="center">
                            <a>
                                <button
                                    className={'forum-thread-item-load-more-comment'}
                                    onClick={() => {
                                        fetchComments({
                                            classId: Number(classId),
                                            postId: props.postId,
                                            commentId: id,
                                            childCommentPagination: {
                                                page: childCommentPage + 1,
                                                size: childCommentSize,
                                            },
                                        });
                                        setChildCommentPage(childCommentPage + 1);
                                    }}
                                >
                                    Tải thêm thêm bình luận
                                </button>
                            </a>
                        </Row>
                    )}
                    {/*<div className={"pagination-container"}>*/}
                    {/*	<Pagination*/}
                    {/*		key={"ADDER"}*/}
                    {/*		defaultCurrent={1}*/}
                    {/*		total={childCommentList.total}*/}
                    {/*		pageSize={5}*/}
                    {/*		responsive={true}*/}
                    {/*		onChange={(page, pageSize) => {*/}
                    {/*			setChildCommentPage(page);*/}
                    {/*		}}*/}
                    {/*	/>*/}
                    {/*</div>*/}
                    <CommentEditor
                        key={"EDITOR"}
                        classId={Number(classId)}
                        postId={postId}
                        commentIdToReply={id}
                    />
                </Collapse>
            )}
        </>
    );
};

export default CommentItem;

// export const CommentItem = () => {
//     return (
//         <List.Item
//             actions={
//                 [
//                     <UpvoteIconWithState upvoted={comment.personalPreference === "upvoted"}
//                                          numOfUpvotes={comment.upVotesCount}
//                                          onClick={() => {
//                                              console.log("LIKED");
//                                          }}/>,
//                     <DownvoteIconWithState downvoted={comment.personalPreference === "downvoted"}
//                                            numOfDownvotes={comment.downVotesCount}
//                                            onClick={() => {
//                                                console.log("LIKED");
//                                            }}/>,
//                 ]
//             }
//         >
//             <List.Item.Meta
//                 avatar={<Avatar src="https://joeschmoe.io/api/v1/random"/>}
//                 title={comment.username}
//                 description={comment.time}
//             />
//             {comment.content}
//         </List.Item>
//     );
// };
