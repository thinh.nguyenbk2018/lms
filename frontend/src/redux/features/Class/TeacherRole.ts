import { ClassMemberType } from "./ClassType";


export type TeacherRole = Exclude<ClassMemberType, "STUDENT">;
