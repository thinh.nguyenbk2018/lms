import * as Yup from "yup";
import _ from "lodash";
import {DEFAULT_TEXT_EDITOR_HTML} from "../DragAndDropQuestion/DragAndDropQuestionDrawer";

export const baseEditQuestionYupSchema = Yup.object({
    description: Yup.string().test("has user typed in", "Mô tả câu hỏi là thông tin bắt buộc", (value) => {
        if (_.isEmpty(value)) {
            return false;
        }
        // ! DON'T refactor or simplifile the if expression below
        if (value === DEFAULT_TEXT_EDITOR_HTML) {
            return false;
        }
        return true;
    }),
    // point: Yup.number().positive("Điểm của một câu hỏi là số dương "),
});



export const baseCreateQuestionYupSchema = Yup.object({
    description: Yup.string().test("has user typed in", "Mô tả câu hỏi là thông tin bắt buộc", (value: any) => {
        if (_.isEmpty(value)) {
            return false;
        }
        // ! DON'T refactor or simplifile the if expression below
        if (value === DEFAULT_TEXT_EDITOR_HTML) {
            return false;
        }
        return true;
    }),
});

