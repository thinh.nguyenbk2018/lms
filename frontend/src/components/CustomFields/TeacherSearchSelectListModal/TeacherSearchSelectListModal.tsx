import {Modal} from "antd";
import {useState} from "react";
import {Teacher} from "../../../redux/features/Class/TeacherAPI/ClassTeacherAPI";
import {ID} from "../../../redux/interfaces/types";
import TeacherList from "../../Teacher/TeacherList";
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {
    classTeacherSelectors,
    classTeacherSlice,
} from "../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";

type TeacherSearchSelectListProps = {
    onDoneSelectList?: (ids: ID[]) => void;
    onCancel?: () => void;
    preSelectedValues?: ID[];
    data: Partial<Teacher>[];
};

const TeacherSearchSelectListModal = (props: TeacherSearchSelectListProps) => {
    const {onDoneSelectList, onCancel, preSelectedValues = [], data} = props;
    const [selectedValues, setSelectedValues] = useState(preSelectedValues);
    const isInsideClass = (id: ID) => {
        return selectedValues?.includes(id);
    };

    // * Redux slice
    const showModal = useAppSelector(
        classTeacherSelectors.selectIsAssigningTeacher
    );
    const dispatch = useAppDispatch();

    const handleItemClick = (id: ID) => {
        let newList = selectedValues?.slice(0);
        if (isInsideClass(id)) {
            const index = newList.findIndex((item) => item === id);
            if (index) setSelectedValues(newList.splice(index, 1));
        }
        newList.concat(id);
        setSelectedValues(newList);
    };

    return (
        <>
            <Modal
                title={"Cập nhật giáo viên"}
                visible={showModal}
                onOk={() => {
                    if (!selectedValues) {
                        return;
                    }
                    onDoneSelectList?.(selectedValues);
                }}
                onCancel={() => {
                    onCancel?.();
                    dispatch(classTeacherSlice.actions.closeModal());
                }}
                bodyStyle={{ overflowY: 'auto', maxHeight: 'calc(100vh - 200px)' }}
                okText={'Cập nhật'}
                cancelText={'Thoát'}
            >
                <TeacherList listData={data || []} onItemClick={handleItemClick} isInModalBox={true}/>
            </Modal>
        </>
    );
};

export default TeacherSearchSelectListModal;
