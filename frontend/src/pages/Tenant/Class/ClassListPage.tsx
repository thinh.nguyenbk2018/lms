import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { Button, Space, Tooltip, Typography } from "antd";
import { Link, useNavigate } from "react-router-dom";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import { ICourseInformation } from "@components/Courses/CourseInformation/CourseInformation";
import CustomTable from "../../../components/CustomTable/CustomTable";
import { addIconSvg, testSvg, viewIconSvg } from "@components/Icons/SvgIcons";
import TableSearch from "../../../components/Search/TableSearch";
import StageTag, { TagType } from "../../../components/StateTag/StageTag";
import { useAppSelector } from "@hooks/Redux/hooks";
import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import { useDeleteClassMutation, useGetClassListPaginatedQuery } from "@redux/features/Class/ClassAPI";
import { changeCriteria, classSelector } from "@redux/features/Class/ClassSlice";
import {
    changeCriteria as changeCourseClassListCriteria,
    courseClassListSelector
} from "@redux/features/Course/CourseClassListSlice";
import LoadingPage from "../../UtilPages/LoadingPage";
import './ClassListPage.less';
import { DATE_FORMAT } from "@utils/TimeStampHelper/TimeStampHelper";
import moment from "moment";
import { DeleteOutlined } from "@ant-design/icons";
import React, { useEffect, useRef } from "react";
import COLOR_SCHEME from "@constants/ThemeColor";
import { confirmationModal, DeleteConfirmation, DeleteConfirmationRefType } from "@components/Modal/DeleteConfirmation";
import PermissionRenderer, { useHasPermission } from "@components/Util/PermissionRender/PermissionRender";
import { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import { useGetCourseClassListPaginatedQuery } from "@redux/features/Course/CourseAPI";
import { useParams } from "react-router";
import useCourseSidebar from "@hooks/Routing/useCourseSidebar";
import {classStatus, classType} from "@redux/features/Class/ClassType";
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";

// * TODO: Implement this function to check if the current user actually has the access to the class content
// * May need to call backend

const ClassListPage = () => {
    const deleteConfirmation = useRef<DeleteConfirmationRefType>(null);

    const { courseId } = useParams();

    const hasPermissionWithClass = useHasPermission([PermissionEnum.CREATE_CLASS, PermissionEnum.VIEW_CLASS, PermissionEnum.DELETE_CLASS]);

    // const { forceCloseSidebar } = useCourseSidebar();
    const [ closeSidebar ] = useClassSidebar();

    useEffect(() => {
        // do nothing
    }, [])
    const navigate = useNavigate();
    const classListSearchCriteria = useAppSelector(classSelector.selectSearchCriteria);
    const courseClassListSearchCriteria = useAppSelector(courseClassListSelector.selectSearchCriteria);
    const {
        data: paginatedResponse,
        isSuccess,
        isFetching,
    } = useGetClassListPaginatedQuery(classListSearchCriteria, { skip: !isNaN(Number(courseId)) });

    const {
        data: courseClassListPaginatedResponse,
        isSuccess: isCourseClassListSuccess,
        isFetching: isCourseClassListFetching,
    } = useGetCourseClassListPaginatedQuery({ courseId: Number(courseId), criteria: courseClassListSearchCriteria }, { skip: courseId == undefined });

    const [deleteClass] = useDeleteClassMutation();

    let columns = [
        {
            title: "Tên",
            dataIndex: "name",
            key: "title",
            width: "20%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
            },
            sortDirections: ["descend", "ascend"],
            // sortOrder:
            //     searchCriteria.sort.filter((x) => x.field === "name").length > 0 &&
            //     searchCriteria.sort.filter((x) => x.field === "name")[0].order,
        },
        {
            title: "Mã",
            dataIndex: "code",
            key: "code",
            width: "10%",
            sorter: {
                compare: (a: string, b: string) => a.length - b.length,
            },
            sortDirections: ["descend", "ascend"],
            // sortOrder:
            //     searchCriteria.sort.filter((x) => x.field === "code").length > 0 &&
            //     searchCriteria.sort.filter((x) => x.field === "code")[0].order,
        },
        {
            title: "Khóa học",
            dataIndex: "course",
            key: "course",
            width: "25%",
            render: (data: ICourseInformation) => <span>{data?.name}</span>,
        },
        {
            title: "Trạng thái",
            dataIndex: "status",
            key: "status",
            width: "10%",
            render: (data: TagType) => <StageTag type={data} title={classStatus[data]} />,
        },
        {
            title: "Hình thức",
            dataIndex: "type",
            key: "type",
            width: "10%",
            render: (data: TagType) => <StageTag type={data} title={classType[data]} />,
        },
        {
            title: "Thời gian bắt đầu",
            dataIndex: "startedAt",
            key: "startedAt",
            width: "15%",
            sorter: {
                compare: (a: string, b: string) => 0,
            },
            sortDirections: ["descend", "ascend"],
            // sortOrder: searchCriteria.sort.filter((x) => x.field === "startedAt").length > 0 &&
            //     searchCriteria.sort.filter((x) => x.field === "startedAt")[0].order,
            render: (startedAt: Date) => startedAt ? moment(startedAt).format(DATE_FORMAT) : ""
        },
        {
            title: "",
            key: "action",
            render: (record: any) => (
                <Space>
                    <PermissionRenderer permissions={[PermissionEnum.UPDATE_CLASS]}>
                        <Tooltip title={"Chỉnh sửa thông tin chung của lớp"}>
                            <Link className={'flex'}
                                to={
                                    courseId ? ROUTING_CONSTANTS.COURSE_CLASS_EDIT.replace(":courseId", courseId).replace(":id", record.id)
                                        :
                                        ROUTING_CONSTANTS.CLASS_DETAIL_INFORMATION.replace(":id", record.id)}>
                                {viewIconSvg}
                            </Link>
                        </Tooltip>
                    </PermissionRenderer>
                    <PermissionRenderer permissions={[PermissionEnum.VIEW_CLASS]}>
                        <Tooltip title={"Vào lớp học"}>
                            <Typography.Link
                                className={'flex'}
                                style={{ marginRight: '1rem', color: COLOR_SCHEME.primary }}
                                onClick={() => {
                                    courseId ?
                                        navigate(ROUTING_CONSTANTS.COURSE_CLASS_VIEW_DETAILS.replace(":courseId", courseId).replace(":classId", record.id))
                                        :
                                        navigate(ROUTING_CONSTANTS.CLASS_DASHBOARD.replace(":classId", record.id));
                                }}
                            >
                                {testSvg}
                            </Typography.Link>
                        </Tooltip>
                    </PermissionRenderer>
                    <PermissionRenderer permissions={[PermissionEnum.DELETE_CLASS]}>
                        <Tooltip title={"Xóa lớp học"}>
                            <DeleteOutlined
                                className={'delete-item-btn'}
                                onClick={() => {
                                    confirmationModal('lớp', record.name, () => {
                                        deleteClass(record.id)
                                            .unwrap()
                                            .then(res => {
                                                AntdNotifier.success("Xóa lớp học thành công")
                                            })
                                            .catch(error => {
                                                AntdNotifier.error(error)
                                            })
                                    });
                                }}
                            />
                        </Tooltip>
                    </PermissionRenderer>
                </Space>
            ),
        },
    ];

    if (courseId != undefined) {
        columns = [
            {
                title: "Tên",
                dataIndex: "name",
                key: "title",
                width: "30%",
                sorter: {
                    compare: (a: string, b: string) => a.length - b.length,
                },
                sortDirections: ["descend", "ascend"],
                // sortOrder:
                //     searchCriteria.sort.filter((x) => x.field === "name").length > 0 &&
                //     searchCriteria.sort.filter((x) => x.field === "name")[0].order,
            },
            {
                title: "Mã",
                dataIndex: "code",
                key: "code",
                width: "15%",
                sorter: {
                    compare: (a: string, b: string) => a.length - b.length,
                },
                sortDirections: ["descend", "ascend"],
                // sortOrder:
                //     searchCriteria.sort.filter((x) => x.field === "code").length > 0 &&
                //     searchCriteria.sort.filter((x) => x.field === "code")[0].order,
            },
            {
                title: "Trạng thái",
                dataIndex: "status",
                key: "status",
                width: "15%",
                render: (data: TagType) => <StageTag type={data} title={classStatus[data]} />,
            },
            {
                title: "Hình thức",
                dataIndex: "type",
                key: "type",
                width: "15%",
                render: (data: TagType) => <StageTag type={data} title={classType[data]} />,
            },
            {
                title: "Thời gian bắt đầu",
                dataIndex: "startedAt",
                key: "startedAt",
                width: "15%",
                sorter: {
                    compare: (a: string, b: string) => 0,
                },
                sortDirections: ["descend", "ascend"],
                // sortOrder: searchCriteria.sort.filter((x) => x.field === "startedAt").length > 0 &&
                //     searchCriteria.sort.filter((x) => x.field === "startedAt")[0].order,
                render: (startedAt: Date) => startedAt ? moment(startedAt).format(DATE_FORMAT) : ""
            },
            {
                title: "",
                key: "action",
                render: (record: any) => (
                    <Space>
                        <PermissionRenderer permissions={[PermissionEnum.UPDATE_CLASS]}>
                            <Tooltip title={"Chỉnh sửa thông tin chung của lớp"}>
                                <Link className={'flex'}
                                      to={
                                          courseId ? ROUTING_CONSTANTS.COURSE_CLASS_EDIT.replace(":courseId", courseId).replace(":id", record.id)
                                              :
                                              ROUTING_CONSTANTS.CLASS_DETAIL_INFORMATION.replace(":id", record.id)}>
                                    {viewIconSvg}
                                </Link>
                            </Tooltip>
                        </PermissionRenderer>
                        <PermissionRenderer permissions={[PermissionEnum.VIEW_CLASS]}>
                            <Tooltip title={"Vào lớp học"}>
                                <Typography.Link
                                    className={'flex'}
                                    style={{ marginRight: '1rem', color: COLOR_SCHEME.primary }}
                                    onClick={() => {
                                        courseId ?
                                            navigate(ROUTING_CONSTANTS.COURSE_CLASS_VIEW_DETAILS.replace(":courseId", courseId).replace(":classId", record.id))
                                            :
                                            navigate(ROUTING_CONSTANTS.CLASS_DASHBOARD.replace(":classId", record.id));
                                    }}
                                >
                                    {testSvg}
                                </Typography.Link>
                            </Tooltip>
                        </PermissionRenderer>
                        <PermissionRenderer permissions={[PermissionEnum.DELETE_CLASS]}>
                            <Tooltip title={"Xóa lớp học"}>
                                <DeleteOutlined
                                    className={'delete-item-btn'}
                                    onClick={() => {
                                        confirmationModal('lớp', record.name, () => {
                                            deleteClass(record.id)
                                                .unwrap()
                                                .then(res => {
                                                    AntdNotifier.success("Xóa lớp học thành công")
                                                })
                                                .catch(error => {
                                                    AntdNotifier.error(error)
                                                })
                                        });
                                    }}
                                />
                            </Tooltip>
                        </PermissionRenderer>
                    </Space>
                ),
            },
        ];
    }

    const renderFunction = () => {
        let componentToRender: JSX.Element = <></>;
        if (isFetching) {
            componentToRender = <LoadingPage />;
        }
        if (isSuccess || isCourseClassListSuccess) {
            componentToRender = (
                <CustomTable
                    data={paginatedResponse?.listData != undefined ? paginatedResponse.listData : courseId != undefined && courseClassListPaginatedResponse?.listData != undefined ? courseClassListPaginatedResponse.listData : []}
                    columns={hasPermissionWithClass ? columns : columns.filter(col => col.key !== "action")}
                    metaData={courseId != undefined ? courseClassListPaginatedResponse : paginatedResponse}
                    searchCriteria={courseId == undefined ? classListSearchCriteria : courseClassListSearchCriteria}
                    changeSearchActionCreator={courseId == undefined ? changeCriteria : changeCourseClassListCriteria}
                />
            );
        }
        return (
            <>
                <ContentHeader
                    title={"Danh sách lớp học"}
                    action={
                        <>
                            <div className={"search-container"}>
                                <TableSearch
                                    searchFields={[{ title: "Từ khóa", key: "key" }]}
                                    defaultFilters={[]}
                                    changeCriteria={courseId == undefined ? changeCriteria : changeCourseClassListCriteria}
                                    searchCriteria={courseId == undefined ? classListSearchCriteria : courseClassListSearchCriteria}
                                />
                                <PermissionRenderer permissions={[PermissionEnum.CREATE_CLASS]}>
                                    <Link to={courseId != undefined ? replacePathParams(ROUTING_CONSTANTS.COURSE_CLASS_NEW, [":courseId", courseId]) : ROUTING_CONSTANTS.CLASS_NEW}>
                                        <Button type="primary" className={"button-with-icon"}>
                                            {addIconSvg}
                                            <span>Thêm</span>
                                        </Button>
                                    </Link>
                                </PermissionRenderer>
                            </div>
                        </>
                    }
                />
                {componentToRender}
                <DeleteConfirmation ref={deleteConfirmation} />
            </>
        );
    };

    return <>{renderFunction()}</>;
};

export default ClassListPage;
