import React from "react";
import {useGetCalendarInformationQuery} from "../../../redux/features/Calendar/CalendarAPI";
import {skipToken} from "@reduxjs/toolkit/query";
import {Formik} from "formik";
import * as Yup from "yup";
import {Form, Input, SubmitButton} from "formik-antd";
import {Modal, Row, Space} from "antd";
import {ModalProps} from "antd/es";
import CancelButton from "../../ActionButton/CancelButton";

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const CalendarSchema = Yup.object().shape(
    {
        summary: Yup.string().min(4, "Too short!").max(50, "Too Long!").required("Required"),
        location: Yup.string().min(4, "Too short!").max(50, "Too Long!").required("Required"),
        timeZone: Yup.string().min(4, "Too short!").max(50, "Too Long!").required("Required")
    }
)


// location summary timeZone description
// type FormikSubmitHandler = (values: Values, formikHelpers: FormikHelpers<Values>) => (void | Promise<any>);
// TODO: Get calendar ID from the UserSlice and attach it to form values
const CalendarMetaFormModal = ({calendarID, modalProps}: { calendarID?: string, modalProps: ModalProps }) => {
    const {data, isSuccess, isFetching, isError, error} = useGetCalendarInformationQuery(calendarID ?? skipToken);
    return (
        <>
            <Formik initialValues={data || {}} onSubmit={(values,actions) => {
                console.log("Formik Values is: ",values);
                console.log("Formik actions is: ",actions);
                // actions.setSubmitting(false);
            }} validationSchema={CalendarSchema}>
                {
                    ({isValid,isSubmitting, dirty}) =>
                        <Modal {...modalProps} footer={null}>
                            <Form {...formItemLayout} title={"Form Ttle"}>
                                <Form.Item name={"summary"} label={"Summary"}>
                                    <Input name={"summary"}/>
                                </Form.Item>
                                <Form.Item name={"description"} label={"Descriptions"}>
                                    <Input name={"description"}/>
                                </Form.Item>
                                <Form.Item name={"location"} label={"Location"}>
                                    <Input name={"location"}/>
                                </Form.Item>
                                <Form.Item name={"timeZone"} label={"TimeZone"}>
                                    <Input name={"timeZone"}/>
                                </Form.Item>
                                <Row justify={"end"}>
                                    <Space>
                                        <CancelButton onClick={modalProps.onCancel
                                        }/>
                                        <SubmitButton loading={isSubmitting} disabled={!isValid || !dirty}>
                                            Submit
                                        </SubmitButton>
                                    </Space>
                                </Row>
                            </Form>
                        </Modal>
                }
            </Formik>
        </>
    );
}


export default CalendarMetaFormModal;