import React from "react";
import {useNavigate, useParams} from "react-router-dom";

import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";
import antdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";

const CourseTeacher = () => {
	// * Class specif routes
	const { courseId } = useParams();
	const {
		forceCloseSidebar,
		courseDetail,
		isCourseDetailError,
		isCourseDetailFetching,
		isCourseDetailSuccess,
	} = useCourseSidebar();
	const navigate = useNavigate();

	if (!courseId || isNaN(Number(courseId))) {
		antdNotifier.error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
		return (
			<>
				<ApiErrorWithRetryButton onRetry={() => {}} />
			</>
		);
	}

	return <ContentHeader title={`Teacher of ${courseDetail?.name}`} />;
};

export default CourseTeacher;
