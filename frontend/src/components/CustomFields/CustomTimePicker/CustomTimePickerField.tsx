import {Space, Typography} from "antd";
import {FieldProps} from "formik";
import {CSSProperties} from "react";
import CustomTimePicker from "./CustomTimePicker";

type SearchSelectFieldProps = FieldProps<any> & {
    label?: string;
    style?: CSSProperties;
    required?: boolean;
    onSelect?: () => void;
    showErrorMessage?: boolean;
};

const TimePickerField = ({
                             field,
                             form,
                             ...otherProps
                         }: SearchSelectFieldProps) => {
    const {
        touched,
        errors,
        setFieldValue,
        setFieldError,
        handleBlur,
        handleChange,
        setTouched,
        setFieldTouched,
    } = form;
    const {name, value, onChange, onBlur} = field;
    const errorMsg = touched[name] && errors[name];

    return (
        <Space>
            <CustomTimePicker
                onChange={(date) => setFieldValue(name, date)}
                onBlur={() => {
                    setFieldTouched(name, true);
                }}
            />
            <Typography.Text type={"danger"}>
                <>
                    {errorMsg}
                </>
            </Typography.Text>
        </Space>
    );
};

export default TimePickerField;
