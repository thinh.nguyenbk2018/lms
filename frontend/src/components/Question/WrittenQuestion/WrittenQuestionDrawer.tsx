import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Col, Drawer, Form, Modal, Row, Space, Typography, } from "antd";
import React, { useRef } from "react";
import CancelButton from "../../ActionButton/CancelButton";
import DoneButton from "../../ActionButton/DoneButton";
import { WrittenQuestion } from "@redux/features/Quiz/CreateQuizSlice";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { FormItem, Input } from "formik-antd";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import useQuestionDrawerType from "../../../hooks/QuizDrawer/useQuestionDrawerType";
import QUESTION_LABEL_CONSTANT from "../constant/QuestionLabelConstant";
import './WrittenQuestionDrawer.less';
import AttachmentQuestion from "../Attachment/AttachmentQuestion";

const WrittenQuestionDrawer = () => {

    // * The drawer state
    const {
        visible,
        closeDrawer,
        openDrawer,
        isEditing,
        questionDataInDraft,
        doneHandler,
        isFetching,
        isError,
        isSuccess,
        questionIdInDraft,
        testName,
    } = useQuestionDrawerType("WRITING");

    //* The container for the modal to pop up
    const drawerRef = useRef<HTMLDivElement | null>(null);

    const onShowDrawer = () => {
        openDrawer();
    };

    function closeConfirm(onOk: () => void, onCancel: () => void) {
        Modal.confirm({
            title: "Xác nhận hủy bỏ câu hỏi",
            icon: <ExclamationCircleOutlined />,
            content:
                "Chúng tôi nhận thấy rằng bạn đã soạn phần nào câu hỏi. Chọn xác nhận sẽ xóa hết tất cả.",
            okText: "Xác nhận",
            cancelText: "Tiếp tục soạn",
            onOk: onOk,
            onCancel: onCancel,
            getContainer: drawerRef.current!!,
        });
    }

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={{
                    description: questionDataInDraft?.description || "",
                    point: questionDataInDraft?.point || 0,
                    attachment: questionDataInDraft?.attachment || '[]'
                }}
                validationSchema={Yup.object({
                    description: Yup.string().required("Đề bài là trường bắt buộc"),
                })}
                onSubmit={(val) => {
                    const valuesToSubmit: Partial<WrittenQuestion> = {
                        id: questionDataInDraft?.id,
                        note: questionDataInDraft?.note,
                        order: questionDataInDraft?.order,
                        point: val?.point,
                        description: val.description,
                        attachment: val.attachment,
                        type: "WRITING",
                    };
                    doneHandler(valuesToSubmit);
                }}
            >
                {({ handleSubmit, values, isValid, dirty, setFieldValue, touched, resetForm }) => {
                    return (
                        <>
                            <Form>
                                <Drawer
                                    maskClosable={false}
                                    closable={false}
                                    title={
                                        <Space direction={"vertical"}>
                                            <Typography.Text>
                                                {`${testName || "Test Nhẹ kiến thức"}`}
                                            </Typography.Text>
                                            <Typography.Text strong>Câu hỏi tự luận</Typography.Text>
                                        </Space>
                                    }
                                    placement="right"
                                    size={"large"}
                                    onClose={() => {
                                        // * Check if there was data and add for confirmation
                                        if (dirty) {
                                            closeConfirm(
                                                () => {
                                                    closeDrawer();
                                                    setFieldValue("description", "");
                                                },
                                                () => {
                                                }
                                            );
                                        } else {
                                            closeDrawer();
                                        }
                                    }}
                                    visible={visible}
                                    extra={
                                        <Space>
                                            <CancelButton
                                                onClick={() => {
                                                    if (dirty) {
                                                        closeConfirm(
                                                            () => {
                                                                closeDrawer();
                                                                setFieldValue("description", "");
                                                            },
                                                            () => {
                                                            }
                                                        );
                                                    } else {
                                                        closeDrawer();
                                                    }
                                                }
                                                }
                                            />
                                            <DoneButton
                                                disabled={!dirty}
                                                onClick={() => handleSubmit()}
                                            />
                                        </Space>
                                    }
                                >
                                    <>
                                        <div ref={drawerRef} />
                                        <Row align={"top"} justify={"end"}>
                                            <Col>
                                                <Col>
                                                    <FormItem
                                                        name={"point"}
                                                        label={QUESTION_LABEL_CONSTANT.POINT}
                                                        initialValue={values.point}
                                                    >
                                                        <Input
                                                            type={"number"}
                                                            name={"point"}
                                                            onChange={({ currentTarget: { value } }) => {
                                                                setFieldValue("point", value);
                                                            }}
                                                        />
                                                    </FormItem>
                                                </Col>
                                            </Col>
                                        </Row>
                                        <Row className={'question-drawer-instruction'}>
                                            <Typography.Paragraph>
                                                Hãy nhập đề của câu hỏi vào trường bên dưới.
                                            </Typography.Paragraph>
                                            <Typography.Paragraph className={'question-drawer-note'}>
                                                *Lưu ý: Các câu hỏi tự luận sẽ không được áp dụng chấm tự động.
                                            </Typography.Paragraph>
                                        </Row>
                                        <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                                            <Col span={24}>
                                                <Field
                                                    name={"attachment"}
                                                    placeholder={"Type article content"}
                                                    component={AttachmentQuestion}
                                                    label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                                                />
                                            </Col>
                                        </Row>
                                        <Row className={'question-drawer-text-editor-wrapper'} justify={"center"}>
                                            <Field
                                                name={"description"}
                                                placeholder={"Type question content"}
                                                component={TextEditorField}
                                                label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
                                            />
                                        </Row>
                                    </>
                                </Drawer>
                            </Form>
                        </>
                    );
                }}
            </Formik>
        </>
    );
};

export default WrittenQuestionDrawer;
