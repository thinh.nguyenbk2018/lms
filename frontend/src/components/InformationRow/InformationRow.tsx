import {Col, Row} from "antd";
import React from "react";
import './InformationRow.less';

type InformationRowProps = {
    title: string;
    value: any
}

const InformationRow = (props: InformationRowProps) => {
    const {title, value} = {...props};

    return (
        <Row style={{width: '100%'}} className={'course-quiz-item-information-row'}>
            <Col
                sm={{span: 11}}
                md={{span: 11}}
                xxl={{span: 11}}
                className={'information-row-title'}
            >
                {title}
            </Col>
            <Col
                sm={{span: 12}}
                md={{span: 12}}
                xxl={{span: 12}}
            >
                {value || ""}
            </Col>
        </Row>
    );
}

export default InformationRow;