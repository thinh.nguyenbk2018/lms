import React from 'react';
import {Field, FieldArray, useFormikContext} from "formik";
import {MultichoiceQuestion} from "../../../redux/features/Quiz/CreateQuizSlice";
import {Button, Radio as OriginalRadio, Space, Typography} from "antd";
import {Checkbox, Switch} from "formik-antd";
import DraggableChoice from "./DraggableChoice";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import COLOR_SCHEME from "../../../constants/ThemeColor";
import {generateNextKey} from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import BlueBorderedCard from "../../Card/BlueBorderedCard/BlueBorderedCard";
import './OptionsAsRadioOrCheckBox.less';


const counter = generateNextKey(2);

// * Used exclusively for the multiple choice question
// ! Issues: Item is swapped, the state observed in debug mode is swapped correctly but the UI displayed in the wrong order
const OptionsAsRadioOrCheckBox = () => {
    const {values, dirty, touched, setFieldTouched, setFieldValue} = useFormikContext<MultichoiceQuestion>();
    // useEffect(() => {
    //     if (!values.isMultipleAnswer) {
    //         setFieldValue('answerList', values.answerList.map((item, option_index) => {
    //             return {
    //                 ...item
    //             }
    //         }))
    //     }
    // }, [values.isMultipleAnswer])
    return (
        <div className={"option-list-outer-container"}>
            <FieldArray name={"answerList"}
                        render={
                            arrayHelpers => (
                                <div className={"multichoice-option-container"}>
                                    <BlueBorderedCard style={{width: `80%`, borderColor: COLOR_SCHEME.primary}}
                                                      // title={<Typography.Text strong
                                                      //                         style={{color: COLOR_SCHEME.primary}}>
                                                      //   Answer Box
                                                      // </Typography.Text>}
                                                      extra={
                                        <Space>
                                            <Switch defaultChecked name={"isMultipleAnswer"}/>
                                            <Typography.Text>Cho phép nhiều lựa chọn </Typography.Text>
                                            <Button onClick={() => {
                                                arrayHelpers.push({
                                                    content: "",
                                                    isCorrect: false,
                                                    answerKey: counter.next().value,
                                                })
                                            }}
                                                    type={"primary"}
                                            >Thêm câu trả lời mới</Button>
                                        </Space>
                                    }>
                                        {
                                            (values.answerList && values.answerList.length > 0) ? (
                                                    !values.isMultipleAnswer ?
                                                        <div className={"sub-option-list-container-bank"}>
                                                            {
                                                                values.answerList.map((item, index) =>
                                                                    <DraggableChoice index={index} key={item.answerKey}
                                                                                     move={arrayHelpers.move}
                                                                                     answerKey={item.answerKey}>
                                                                        <>
                                                                            <OriginalRadio
                                                                                checked={values.answerList[index].isCorrect}
                                                                                onChange={e => {
                                                                                    const value = e.target.checked;
                                                                                    // console.log(value, index);
                                                                                    setFieldValue('answerList', values.answerList.map((item, option_index) => {
                                                                                        console.log("CHANGING RADIO");
                                                                                        return {
                                                                                            ...item,
                                                                                            isCorrect: option_index === index
                                                                                        }
                                                                                    }))
                                                                                }}>
                                                                    <span onClick={(e) => {
                                                                        e.stopPropagation();
                                                                        e.preventDefault();
                                                                    }}>
                                                                        <Field
                                                                            name={`answerList[${index}].content`}
                                                                            placeholder={"Type article content"}
                                                                            component={TextEditorField}/>
                                                                    </span>
                                                                            </OriginalRadio>
                                                                            <Button danger type='primary'
                                                                                    onClick={() => setFieldValue('answerList', values.answerList.filter((ans, ind) => ind !== index))}>
                                                                                Xóa
                                                                            </Button>
                                                                        </>
                                                                    </DraggableChoice>
                                                                )
                                                            }
                                                        </div>
                                                        : <div className={"sub-option-list-container-bank"}>
                                                            {
                                                                values.answerList.map((item, index) =>
                                                                    <DraggableChoice index={index} key={item.answerKey}
                                                                                     move={arrayHelpers.move}
                                                                                     answerKey={item.answerKey}>
                                                                        <>
                                                                            <Checkbox name={`answerList[${index}].isCorrect`}>
                                                                    <span onClick={(e) => {
                                                                        e.stopPropagation();
                                                                        e.preventDefault();
                                                                    }}>
                                                                        <Field
                                                                            name={`answerList[${index}].content`}
                                                                            placeholder={"Type article content"}
                                                                            component={TextEditorField}/>
                                                                    </span>
                                                                            </Checkbox>
                                                                            <Button danger type='primary'
                                                                                    onClick={() => setFieldValue('answerList', values.answerList.filter((ans, ind) => ind !== index))}>
                                                                                Xóa
                                                                            </Button>
                                                                        </>
                                                                    </DraggableChoice>
                                                                )
                                                            }
                                                        </div>)
                                                : <Typography.Text type={"danger"}>Chưa có lựa chọn nào!</Typography.Text>
                                        }
                                    </BlueBorderedCard>
                                </div>
                            )
                        }
            />
        </div>
    );
};

export default OptionsAsRadioOrCheckBox;


//
// <OriginalRadio
//     checked={values.answerList[index].isCorrect}
//     onChange={e => {
//         const value = e.target.checked;
//         console.log(value, index);
//         setFieldValue('answerList', values.answerList.map((item, option_index) => {
//             console.log("CHANGING RADIO");
//             return {
//                 ...item,
//                 isCorrect: option_index === index
//             }
//         }))
//     }}>
//                                                                                 <span onClick={(e) => {
//                                                                                     e.stopPropagation();
//                                                                                     e.preventDefault();
//                                                                                 }}>
//                                                                                     <Field
//                                                                                         name={`answerList[${index}].content`}
//                                                                                         placeholder={"Type article content"}
//                                                                                         component={TextEditorField}/>
//                                                                                 </span>
// </OriginalRadio>




