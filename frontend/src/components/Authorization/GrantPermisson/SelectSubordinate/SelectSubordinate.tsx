import { Row, Select, Space, Typography } from "antd";
import { useAppDispatch, useAppSelector } from "../../../../hooks/Redux/hooks";
import { changeUser } from "../../../../redux/features/Permission/PermissonSlice";
import { useGetStaffsHaveAccountQuery } from "../../../../redux/features/Staff/StaffAPI";

const { Option } = Select;

const SelectSubordinate = () => {
  let {data: getStaffsHaveAccountResponse, isLoading} = useGetStaffsHaveAccountQuery()

  let dispatch = useAppDispatch();

  let userId = useAppSelector(state => state.grantPermission.userId)

  return <Row justify="end" align="bottom">
    <Space align="baseline" size={10} style={{ marginTop: 10 }}>
      <Typography.Title level={5}>Chọn nhân viên</Typography.Title>
      <Select
        placeholder="Chọn nhân viên..."
        defaultValue={userId || null}
        showSearch
        style={{ width: 200 }}
        filterOption={(input, option) =>
          option?.props?.children?.toLowerCase().indexOf(input.toLowerCase()) >= 0
        } loading={isLoading}
        onChange={(value) => dispatch(changeUser(value))}
      >
        {getStaffsHaveAccountResponse?.staffs.map(staff => <Option value={staff.id}>{`${staff.lastName} ${staff.firstName}`}</Option>)}
      </Select>
    </Space>
  </Row >
}

export default SelectSubordinate;