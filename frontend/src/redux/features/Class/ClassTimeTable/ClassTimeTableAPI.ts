import {DatePresentation, DayOfWeekName} from '@components/Calendar/Utils/CalendarUtils';
import {UnitItem} from '@typescript/interfaces/courses/CourseTypes';
import {ID, PaginatedResponse} from '@redux/interfaces/types';
import {apiSlice} from '../../CentralAPI';
import {UserDto} from "../../UserSearch/UserType";

const ClassScheduleApiWithInvalidateTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["ClassSchedule", "Event", "Calendar", "Classes", "UpComingSessions"],
});

export type ClassSession = {
    id?: number;
    teacherId?: number;
    unitId?: number;
    date: Date;
    startedAt: Date;
    finishedAt: Date;
    room?: string;
    note?: string;
    notifyWithEmail: boolean;
    applyTeacherToAll?: boolean;
    applyRoomToAll?: boolean;
};

export type ClassSessionResponse = {
    id?: number;
    teacher?: UserDto;
    unit?: UnitItem;
    startedAt: Date;
    finishedAt: Date;
    room?: string;
    note?: string;
};

export type ClassSessionResponseWrapper = {
    sessions: ClassSessionResponse[];
    hasAfter: boolean;
    hasPrevious: boolean;
    daysOfWeek: string;
}

//  * Class Schedule =  List [ Class Sessions]  that recurs over an period of time

export type SessionRecurrence = {
    dayOfWeek: DayOfWeekName;
    // * Hour in format: HH:MM
    startedAt: string;
    finishedAt: string;
};
export type ClassSchedule = {
    startedAt: DatePresentation;
    numberOfSession: number | undefined;
    sessionRecurrence: SessionRecurrence[];
};

const CLASS_SCHEDULE_PATH_NAME = "scheduler";

const classStudentApi = ClassScheduleApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        // * Class Schedule
        getClassSchedule: build.query<ClassSchedule, { classId: ID }>({
            query: ({classId}) =>
                `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/teacher/config`,
            providesTags: [{type: "ClassSchedule", id: "LIST"}],
        }),
        getClassScheduleAddedByHand: build.query<ClassSession[], { classId: ID }>({
            query: ({classId}) =>
                `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/teacher/not_configured`,
            providesTags: [{type: "ClassSchedule", id: "LIST"}],
        }),
        getClassSessions: build.query<ClassSessionResponseWrapper,
            { classId: ID, dateTime: string, numsOfPrevious: number, numsOfAfter: number }>({
            query: ({classId, dateTime, numsOfPrevious, numsOfAfter}) =>
                `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/session/?dateTime=${dateTime}&previous=${numsOfPrevious}&after=${numsOfAfter}`,
            providesTags: [{type: "ClassSchedule", id: "LIST"}],
        }),
        getUpComingClassSessions: build.query<ClassSessionResponseWrapper,
            { classId: ID, numsOfPrevious: number, numsOfAfter: number }>({
            query: ({classId, numsOfPrevious, numsOfAfter}) =>
                `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/session/upComing?previous=${numsOfPrevious}&after=${numsOfAfter}`,
            providesTags: [{type: "UpComingSessions", id: "LIST"}],
        }),
        getClassSessionAddedBySchedule: build.query<ClassSessionResponse[],
            { classId: ID }>({
            query: ({classId}) =>
                `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/teacher/configured`,
            providesTags: [{type: "ClassSchedule", id: "LIST"}],
        }),
        createNewClassSchedule: build.mutation<ClassSchedule,
            { classId: number; classSchedule: ClassSchedule }>({
            query: ({classSchedule, classId}) => {
                return {
                    url: `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/config`,
                    method: "POST",
                    body: classSchedule,
                };
            },
            invalidatesTags: (_res, _err, _args) => [
                {type: "ClassSchedule", id: "LIST"}, {type: "Classes", id: _args.classId}
            ],
        }),
        updateClassSchedule: build.mutation<Partial<ClassSession>,
            {
                classId: number;
                classSchedule: Partial<ClassSchedule>;
            }>({
            query: ({classId, classSchedule}) => {
                return {
                    url: `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/config`,
                    method: "POST",
                    body: classSchedule,
                };
            },
            invalidatesTags: (_res, _err, _args) => [
                {type: "ClassSchedule", id: "LIST"}, "Event", "Calendar", "UpComingSessions"
            ],
        }),
        // * Class session
        getFullListSessionForStudent: build.query<ClassSession[], { classId: ID }>({
            query: ({classId}) =>
                `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/student`,
            providesTags: (_res, _err, _args) => [{type: "ClassSchedule", id: "LIST"}],
        }),
        getClassUnits: build.query<UnitItem[], { classId: ID }>({
            query: ({classId}) => `classes/${classId}/learning_content/units`,
            providesTags: (_res, _err, _args) => [
                {type: "ClassSchedule", id: "CLASS_UNIT_LIST"},
            ],
        }),
        getClassTimeTableUnitDetail: build.query<UnitItem, { unitId: ID }>({
            query: ({unitId}) => `units/${unitId}`,
            providesTags: [],
        }),

        createNewSession: build.mutation<PaginatedResponse<ClassSchedule>,
            { classId: number; classSession: Partial<ClassSession> }>({
            query: ({classId, classSession}) => ({
                url: `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/session`,
                method: "POST",
                body: classSession,
            }),
            invalidatesTags: (_result) => [{type: "ClassSchedule", id: "LIST"}, {
                type: "Event",
                id: "LIST_EVENT"
            }, "Calendar", "UpComingSessions"],
        }),
        updateClassSession: build.mutation<ClassSession,
            { classId: ID; classSession: Partial<ClassSession>; sessionId: ID }>({
            query: ({classId, sessionId, classSession}) => {
                return {
                    url: `classes/${classId}/${CLASS_SCHEDULE_PATH_NAME}/session/${sessionId}`,
                    method: "PUT",
                    body: classSession,
                };
            },
            invalidatesTags: (_res, _err, _args) => [
                {type: "ClassSchedule", id: "LIST"}, "UpComingSessions"
            ],
        }),
        deleteClassSession: build.mutation<void, { classId: ID; sessionId: ID }>({
            query: ({classId, sessionId}) => {
                return {
                    url: `classes/${classId}/scheduler/session/${sessionId}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: (_res, _err, _args) => [
                {type: "ClassSchedule", id: "LIST"}, "UpComingSessions"
            ],
        }),
        refetchClassScheduler: build.mutation<any,  void>({
            queryFn: () => ({ data: null }),
            invalidatesTags: (_res, _err, args) => [{type: "ClassSchedule", id: "LIST"}]
        })
    }),
});

export const {
    // * CLASS SCHEDULE
    useGetClassScheduleQuery,
    useGetClassScheduleAddedByHandQuery,
    useGetClassSessionsQuery,
    useGetClassSessionAddedByScheduleQuery,
    useCreateNewClassScheduleMutation,
    useUpdateClassScheduleMutation,

    // * CLASS SESSION
    useGetFullListSessionForStudentQuery,
    useGetClassTimeTableUnitDetailQuery,
    useGetClassUnitsQuery,
    useCreateNewSessionMutation,
    useUpdateClassSessionMutation,
    useDeleteClassSessionMutation,
    useGetUpComingClassSessionsQuery,
    useRefetchClassSchedulerMutation
} = classStudentApi;
