import {apiSlice} from "@redux/features/CentralAPI";


export interface SendEmailDto {
    subject: string;
    content: string;
    attachment?: string;
}

const emailApiInvalidatesTags = apiSlice.enhanceEndpoints({
    addTagTypes: [],
});

const apiEmails = emailApiInvalidatesTags.injectEndpoints({
    endpoints: (build => ({
        sendEmail: build.mutation<void, { emailAddress: string, body: SendEmailDto}>({
            query({emailAddress, body}) {
                return {
                    url: '/email/' + emailAddress,
                    body: body,
                    method: "post"
                }
            }
        })
    }))
})

export const {
    useSendEmailMutation
} = apiEmails;