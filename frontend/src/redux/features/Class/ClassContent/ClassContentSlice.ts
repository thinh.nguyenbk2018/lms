import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
	Activity,
	ActivityType,
	QuizItem,
	UnitItem,
	VotingItem,
} from "../../../../typescript/interfaces/courses/CourseTypes";
import { ID } from "../../../interfaces/types";
import { RootState } from "../../../store/store";
import { VotingOption } from "../ClassAPI";
type ClassContentSlice = {
	chapterInEdit: number | undefined;
	chapterTitle: string;
	activityModalToShow: ActivityType | "NONE";
	activityBeingUpdated: Activity | undefined;
	editingChapterTitle: boolean;
	votingOptionInEdit: VotingOption | undefined;
};

const initialState: ClassContentSlice = {
	chapterInEdit: undefined,
	chapterTitle: '',
	activityModalToShow: "NONE",
	activityBeingUpdated: undefined,
	editingChapterTitle: false,
	votingOptionInEdit: undefined,
};

export const CLASS_CONTENT_SLICE_KEY = "classContentSlice";
export const classContentSlice = createSlice({
	name: CLASS_CONTENT_SLICE_KEY,
	initialState,
	reducers: {
		addingUnit: (state, action: PayloadAction<{ chapterToEdit: ID }>): void => {
			const { chapterToEdit } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityModalToShow = "unit";
		},

		// * Content creation
		addingQuiz: (state, action: PayloadAction<{ chapterToEdit: ID }>) => {
			const { chapterToEdit } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityModalToShow = "quiz";
		},

		addingActivity: (
			state,
			action: PayloadAction<{ chapterToEdit: ID; type: ActivityType }>
		) => {
			const { chapterToEdit, type } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityModalToShow = type;
			state.activityBeingUpdated = undefined;
		},

		doneAddingActivity: (state) => {
			state.chapterInEdit = undefined;
			state.activityModalToShow = "NONE";
			state.activityBeingUpdated = undefined;
		},

		// * Content updates
		updatingActivity: (
			state,
			action: PayloadAction<{ chapterToEdit: ID; activity: Activity }>
		) => {
			const { chapterToEdit, activity } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityBeingUpdated = activity;
			state.activityModalToShow = activity.type;
		},

		// * Editing chapter title
		editingChapter: (state, action: PayloadAction<{id: number, title: string}>) => {
			state.chapterInEdit = action.payload.id;
			state.chapterTitle = action.payload.title;
			state.editingChapterTitle = true;
		},
		doneEditingChapter: (state) => {
			state.chapterInEdit = undefined;
			state.editingChapterTitle = false;
		},

		setVotingOptionInEdit: (
			state,
			action: PayloadAction<VotingOption | undefined>
		) => {
			state.votingOptionInEdit = action.payload;
		},
	},
});

export const classContentSelectors = {
	selectIsAddingUnit: (state: RootState) => {
		const { chapterInEdit, activityModalToShow } =
			state[CLASS_CONTENT_SLICE_KEY];
		return chapterInEdit && activityModalToShow === "unit";
	},
	selectIsAddingQuiz: (state: RootState) => {
		const { chapterInEdit, activityModalToShow } =
			state[CLASS_CONTENT_SLICE_KEY];
		return chapterInEdit && activityModalToShow === "quiz";
	},
	selectIsAddingVoting: (state: RootState) => {
		const { chapterInEdit, activityModalToShow } =
			state[CLASS_CONTENT_SLICE_KEY];
		return chapterInEdit && activityModalToShow === "vote";
	},
	selectIsUpdatingUnit: (state: RootState) => {
		const { chapterInEdit, activityBeingUpdated } =
			state[CLASS_CONTENT_SLICE_KEY];
		return chapterInEdit && activityBeingUpdated?.type === "unit";
	},
	selectIsUpdatingQuiz: (state: RootState) => {
		const { chapterInEdit, activityBeingUpdated } =
			state[CLASS_CONTENT_SLICE_KEY];
		return chapterInEdit && activityBeingUpdated?.type === "quiz";
	},
	selectQuizInUpdate: (state: RootState) => {
		if (state[CLASS_CONTENT_SLICE_KEY].activityBeingUpdated?.type !== "quiz") {
			return undefined;
		}
		return state[CLASS_CONTENT_SLICE_KEY].activityBeingUpdated as QuizItem;
	},
	selectUnitInUpdate: (state: RootState) => {
		if (state[CLASS_CONTENT_SLICE_KEY].activityBeingUpdated?.type !== "unit") {
			return undefined;
		}
		return state[CLASS_CONTENT_SLICE_KEY].activityBeingUpdated as UnitItem;
	},
	selectVotingInUpdate: (state: RootState) => {
		if (state[CLASS_CONTENT_SLICE_KEY].activityBeingUpdated?.type !== "vote") {
			return undefined;
		}
		return state[CLASS_CONTENT_SLICE_KEY].activityBeingUpdated as VotingItem;
	},
	selectVotingOptionInEdit: (state: RootState) => {
		return state[CLASS_CONTENT_SLICE_KEY].votingOptionInEdit;
	},
	selectChapterInEdit: (state: RootState) => {
		return state[CLASS_CONTENT_SLICE_KEY].chapterInEdit;
	},

	selectIsChapterInfoBeingEdited: (state: RootState) => {
		return {
			isChapterInEdit: !!(
				state[CLASS_CONTENT_SLICE_KEY].chapterInEdit &&
				state[CLASS_CONTENT_SLICE_KEY].editingChapterTitle),
			chapterTitle: state[CLASS_CONTENT_SLICE_KEY].chapterTitle
		}
	},
};

export const classContentActions = classContentSlice.actions;
