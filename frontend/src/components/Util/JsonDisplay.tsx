import { Card, CardProps } from 'antd';
import React, { useEffect, useRef } from 'react';
import './JsonDisplay.less';
type Props = {
    cardProps: CardProps,
    obj: any
}
const JsonDisplay = ({ obj, cardProps }: Props) => {
    const preRef = useRef<HTMLPreElement>(null);
    useEffect(() => {
        if (preRef.current) {
        }
    }, [preRef, obj])
    return (
        <Card {...cardProps}>
            <pre id="json" ref={preRef}>
            </pre>
        </Card >
    )
}

export default JsonDisplay