import {Avatar, Col, Row, Space, Spin, Tag, Typography} from "antd";
import {toNumber} from "lodash";
import {useNavigate, useParams} from "react-router-dom";
import {useAppSelector} from "@hooks/Redux/hooks";
import useClassSidebar from "../../hooks/Routing/useClassSidebar";
import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";
import LoadingPage from "../../pages/UtilPages/LoadingPage";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {changeCriteria, announcementSelector,} from "@redux/features/Announcement/AnnouncementSlice";
import "./AnnouncementList.less";
import ApiErrorRetryPage from "../../pages/UtilPages/ApiErrorRetryPage";
import TextEditor from "../TextEditor/TextEditor";
import {
    useDeleteNotificationItemMutation,
    useGetReceivedAnnouncementsQuery,
    useSeenNotificationItemMutation,
    useUnseenNotificationItemMutation,
} from "@redux/features/Announcement/AnnouncementAPI";
import {NotificationItem} from "@redux/features/Announcement/AnnouncementType";
import {ID} from "@redux/interfaces/types";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import CustomTable from "../CustomTable/CustomTable";
import antdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import {format, subDays} from "date-fns";
import {vi} from "date-fns/locale";
import timeStampHelper from "@utils/TimeStampHelper/TimeStampHelper";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";
import FileIconPreviewByFileName from "@components/FilePreview/FileIconPreviewByFileName";
import {getFileNameFromFirebaseUrl} from "@components/FilePreview/Utils/FileUtils";


export interface NotificationListState {
    loading: boolean;
    list: NotificationItem[];
}

/**
 *
 * @param props onEdit: What to do when the user click on the "Mark as Read Button"
 * @returns
 */
const NotificationItemUI = (
    props: NotificationItem & {
        seenOrUnseenAction: (itemId: ID, seen: boolean) => void;
        onDelete: (itemId: ID) => void;
    }
) => {
    const {
        id,
        image,
        senderId,
        senderName,
        sentAt,
        subject,
        content,
        tags,
        seen,
        readOnly,
        seenOrUnseenAction,
        onDelete,
        attachment
    } = props;
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);

    const navigate = useNavigate();
    const navigateToDetailPage = () => {
        const pathToNav = replacePathParams(
            ROUTING_CONSTANTS.CLASS_DETAIL_ANNOUNCEMENT_DETAIL,
            [":notificationId", id]
        );
        navigate(pathToNav);
    };

    return (
        <>
            <Row className={`announcement-wrapper ${!seen ? 'unseen' : ''}`}>
                <Col span={2}>
                    <Avatar
                        className="creator-avatar"
                        src={image}
                        shape="square"
                        size={{xs: 24, sm: 32, md: 40, lg: 64, xl: 64, xxl: 100}}
                    />
                </Col>
                <Col span={22}>
                    <div className={'announcement-header'}>
                        <div className={'announcement-name-time'}>
                            <span className={'announcement-sender'}>{senderName}</span>
                            {subDays(new Date(), 1) < new Date(sentAt) ?
                                <span>
                                        {timeStampHelper.displayRelativeTime(new Date(sentAt))}
                                    </span>
                                :
                                <span className={'announcement-sent-at'}>
                                        {format(new Date(sentAt), 'PP, p', {locale: vi})}
                                    </span>
                            }
                        </div>
                        <div className={'announcement-tags'}>
                            {tags?.map((tag, index) => <Tag className={'announcement-tag'}>#{tag}</Tag>)}
                        </div>
                    </div>
                    <div className={'announcement-content-wrapper'}>
                        <span className={'announcement-subject'}>{subject}</span>
                        <TextEditor
                            additionalStyles={{
                                maxWidth: "85%",
                                maxHeight: "40%",
                                overflow: "hidden",
                            }}
                            readOnly={true}
                            useBubbleTheme={true}
                            onChange={() => {
                            }}
                            value={content}
                        />
                    </div>
                    <div className={'announcement-file-action-btn-wrapper'}>
                        {attachment && <div className={'announcement-attachment-wrapper'}>
                            <a href={attachment || "#"}>
                                <FileIconPreviewByFileName size={25}
                                                           filename={getFileNameFromFirebaseUrl(attachment)}
                                                           showName={true}
                                                           layout={'row'}
                                />
                            </a>
                        </div>}
                        <div className={'announcement-seen-unseen-btn-wrapper'}>
                            <button
                                style={{display: readOnly ? "none" : ""}}
                                className="seen-unseen-btn"
                                onClick={() => seenOrUnseenAction(props.id, seen)}
                            >
                                <Space>
                                    <FontAwesomeIcon icon={faCheck}/>
                                    {seen ? "Đánh dấu chưa đọc" : "Đánh dấu đã đọc"}
                                </Space>
                            </button>
                        </div>
                    </div>
                </Col>
            </Row>
        </>

    );
};

const AnnouncementList = () => {
    const searchCriteria = useAppSelector(
        announcementSelector.selectSearchCriteria
    );
    const {classId} = useParams();
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);

    const [closeSidebar] = useClassSidebar();
    const navigate = useNavigate();

    const [seenNotification] = useSeenNotificationItemMutation();
    const [unseenNotification] = useUnseenNotificationItemMutation();
    const [deleteNotification] = useDeleteNotificationItemMutation();

    const seenOrUnseenAction = (id: ID, seen: boolean) => {
        if (seen) {
            unseenNotification(id);
        }
        seenNotification(id);
    };

    const onDelete = (id: ID) => {
        deleteNotification(id)
            .unwrap()
            .then((res) => {
                antdNotifier.success(
                    "Xóa thông báo thành công",
                    "Thông báo",
                    1
                );
            })
            .catch((err) => {
                antdNotifier.error(
                    "Xóa thông báo thất bại, vui lòng thử lại hoặc tải lại trang",
                    "Thông báo",
                    1
                );
            });
    };

    const {
        data: notificationList,
        isFetching,
        isSuccess,
        isError,
        refetch,
    } = useGetReceivedAnnouncementsQuery(
        {
            ...searchCriteria,
            classId: toNumber(classId),
            receiverId: currentUser?.id,
        },
        {skip: toNumber(classId) === -1 || currentUser == undefined}
    );

    const onRead = () => {
        // TODO: Send backend --> Mark notification as read
    };

    if (isFetching) {
        return (
            <Row justify={"center"}>
                <Space direction={"vertical"} align={"center"}>
                    <Spin/>
                </Space>
            </Row>
        );
    }

    if (isSuccess) {
        return (
            <>
                <Space direction={"vertical"} style={{width: "100%"}}>
                    {notificationList?.listData.map(
                        (item: NotificationItem) => {
                            return (
                                <NotificationItemUI
                                    key={item.id}
                                    {...item}
                                    seenOrUnseenAction={seenOrUnseenAction}
                                    onDelete={onDelete}
                                />
                            );
                        }
                    )}
                </Space>
                {(notificationList != undefined && notificationList.listData.length != 0) && <CustomTable
                    data={[]}
                    columns={null}
                    metaData={notificationList}
                    changeSearchActionCreator={changeCriteria}
                />}
                {notificationList == undefined || notificationList.listData.length == 0 && <span>Chưa có thông báo trong lớp học.</span>}
            </>
        );
    }
    if (isFetching) {
        return <LoadingPage/>;
    }
    if (isError) {
        return (
            <ApiErrorRetryPage
                message={`Đã có lỗi xảy ra trong quá trình load thông báo`}
                onRetry={() => {
                    refetch();
                }}
            />
        );
    }

    return null;
};

export default AnnouncementList;
