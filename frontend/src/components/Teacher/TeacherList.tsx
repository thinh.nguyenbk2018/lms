import {Teacher} from "../../redux/features/Class/TeacherAPI/ClassTeacherAPI";
import {ID} from "../../redux/interfaces/types";
import TeacherCard from "../Card/CustomCard/TeacherCard/TeacherCard";
import "./TeacherList.less";
import _ from "lodash";
import {isJsxElement} from "@utils/Predicates/Styles/elementPredicates";

type ItemRenderer = (props: { itemId: ID, lastName: string, firstName: string }) => JSX.Element;
type TeacherListProps<PropsBag = undefined | number[]> = {
    listData: Partial<Teacher>[];
    onItemEdit?: (itemId: ID) => void;
    onItemDelete?: (itemId: ID) => void;
    onItemClick?: (itemId: ID) => void;
    showItemAction?: boolean;
    // * Is this list being rendered inside a modalbox ?
    isInModalBox?: boolean;
    teacherRoleTag?: (item: Partial<Teacher>) => JSX.Element;
    additionalItemActions?: JSX.Element | ItemRenderer;
};
const TeacherList = (props: TeacherListProps) => {
    const {
        listData,
        onItemDelete,
        onItemEdit,
        onItemClick,
        showItemAction = false,
        isInModalBox = false,
        teacherRoleTag,
        additionalItemActions,
    } = props;
    return (
        <>
            {_.isEmpty(listData) ? (
                <>Không có giáo viên nào</>
            ) : (
                <div className="teacher-grid-container">
                    {listData.map((item, index) => {
                        const { id, userId } = item;
                        const teacherId = id || userId;
                        if (!teacherId) {
                            return <> </>;
                        }
                        return (
                            <TeacherCard
                                {...item}
                                key={teacherId}
                                onDelete={() => {
                                    onItemDelete?.(teacherId);
                                }}
                                onEdit={() => {
                                    onItemEdit?.(teacherId);
                                }}
                                onClick={() => {
                                    onItemClick?.(teacherId);
                                }}
                                showAction={showItemAction}
                                isInModalBox={isInModalBox}
                                teacherRoleTag={
                                    teacherRoleTag && teacherRoleTag(item)
                                }
                                additionalActionsOnHover={
                                    additionalItemActions ? (
                                        isJsxElement(additionalItemActions) ? (
                                            additionalItemActions
                                        ) : (
                                            additionalItemActions({
                                                itemId: teacherId,
                                                lastName: item.lastName || '',
                                                firstName: item.firstName || ''
                                            })
                                        )
                                    ) : (
                                        <></>
                                    )
                                }
                            />
                        );
                    })}
                </div>
            )}
        </>
    );
};

export default TeacherList;
