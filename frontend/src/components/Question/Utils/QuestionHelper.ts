import {QuestionDataType} from "../../../redux/features/Quiz/CreateQuizSlice";
import _ from "lodash";


export const isInEditingMode = (values: QuestionDataType) => {
    return _.isEmpty(values.id);
}