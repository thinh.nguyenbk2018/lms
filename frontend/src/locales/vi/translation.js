import {base_keys} from "../base_keys";

export const TRANSLATIONS_VI = {
    // date
    [base_keys.date_format_one]: "{{-date, DD/MM/YYYY}}",
    // Language Choice
    [base_keys.language_choice.english]: "Tiếng Anh",
    [base_keys.language_choice.vietnamese]: "Tiếng Việt",

    // form
    [base_keys.form.email]: "Email",
    [base_keys.form.password_prompt]: "Nhập email của bạn",
    [base_keys.form.email_valid_prompt]: "Email không hợp lệ. Hãy nhập lại",
    [base_keys.form.email_required_prompt]: "Email là bắt buộc",

    [base_keys.form.password]: "Mật khẩu",
    [base_keys.form.password_prompt]: "Nhập mật khẩu của bạn",
    [base_keys.form.password_required_prompt]: "Mật khẩu là bắt buộc",
    [base_keys.form.password_min_8_requirement]: "Độ dài của mật khẩu phải lớn hơn 8",

    [base_keys.form.username]: "Tên tài khoản",
    [base_keys.form.username_prompt]: "Nhập tài khoản của bạn",
    [base_keys.form.username_required_prompt]: "Tài khoản là bắt buộc",
    [base_keys.form.username_min_8_requirement]: "Độ dài của tài khoản phải lớn hơn 8",

    [base_keys.form.confirm_password]: "Nhập lại mật khẩu",
    [base_keys.form.confirm_password_correct_prompt]: "Mật khẩu nhập lại đang không khớp",
    [base_keys.form.phone_number]: "Số Điện Thoại",
    [base_keys.form.address]: "Địa chỉ của tôi",



    [base_keys.test.test_page1]:"Trang số 1",
    [base_keys.test.test_page2]:"Trang số 2",
    [base_keys.test.navigate_page1]:"Chuyển sang trang 1",
    [base_keys.test.navigate_page2]:"Chuyển sang trang 2",

};
