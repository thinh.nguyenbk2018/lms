import { Button, Input, List, Modal, Row, Typography } from "antd";
import { useState } from "react";
import {
    OriginalPermission,
    PermissionOfUser,
    useGetAllPermissionsQuery,
} from "../../../../redux/features/Permission/PermissionAPI";
import IconSelector from "../../../Icons/IconSelector";
import { PermissionOfRole } from "../../../../redux/features/Permission/RolesAPI";
import Scrollable from "@components/Scrollable/Scrollable";

export interface AddPermissionProps {
    hadPermissions: PermissionOfUser[] | PermissionOfRole[];
    onAddPermission: (permission: OriginalPermission) => void;
}

const AddPermission = ({
    hadPermissions,
    onAddPermission,
}: AddPermissionProps) => {
    let [showModal, setShowModal] = useState(false);

    let { data: response } = useGetAllPermissionsQuery();

    let [searchValue, setSearchValue] = useState("");

    let addablePermissions =
        response?.permissions.filter(
            (permission) =>
                permission.title
                    .toLowerCase()
                    .includes(searchValue.toLowerCase()) &&
                !hadPermissions.map((r) => r.id).includes(permission.id)
        ) || [];

    const addNewPermission = (permission: OriginalPermission) =>
        onAddPermission(permission);

    const onCancel = () => {
        setShowModal(false);
    };

    const onOk = () => {
        setShowModal(false);
    };

    return (
        <Row justify="center" style={{ marginTop: 15 }}>
            <Button
                type="dashed"
                icon={<IconSelector type="PlusOutlined" />}
                size="middle"
                onClick={() => setShowModal(true)}
            >
                Thêm quyền
            </Button>
            <Modal
                title="Thêm quyền"
                visible={showModal}
                onCancel={onCancel}
                onOk={onOk}
            >
                <Input.Search
                    placeholder="Nhập quyền bạn muốn thêm"
                    size="large"
                    style={{ marginBottom: 15 }}
                    onChange={({ currentTarget: { value } }) =>
                        setSearchValue(value)
                    }
                />
                <Scrollable height={550} direction="Vertical">
                    <List
                        header={null}
                        footer={null}
                        bordered
                        dataSource={addablePermissions}
                        renderItem={(item) => (
                            <List.Item>
                                <Typography.Text>{item.title}</Typography.Text>
                                <Button
                                    type="primary"
                                    onClick={() => addNewPermission(item)}
                                >
                                    Thêm
                                </Button>
                            </List.Item>
                        )}
                    />
                </Scrollable>
            </Modal>
        </Row>
    );
};

export default AddPermission;
