// // ! Uncomment below for socket
// import { socketClient } from "./StompJsClient";
// import {IMessage, IPublishParams, messageCallbackType, StompSubscription} from "@stomp/stompjs";

// type MessageStringHandler = (messageBody: string) => void;
// type SubscriptionFunction = (topic: string, listener:MessageStringHandler) => StompSubscription | undefined;

// // TODO: Dealing with teardown, Unsubscription properly --> Won't act weird when user re-login
// // ? Usage: import * from 'pathToThisFile'
// const StompUtils = (() => {
//     /**
//      * @param topic - The topic as defined in backend controller (Without prefix --> Util will prefix it)
//      * @returns topic prefixed so the api is cleaner
//      * @example
//      * With prefix as /api/v1, Return `/api/v1/${topic}``:
//      */
//     const addPrefixToTopic = (topic: string) => {
//         const prefix = '/api/v1'
//         return `${prefix}${topic}`.replace(/([^:]\/)\/+/g, "$1");
//     }
//     /**
//      * Assign listener-handler to a topic
//      * @param topic - The topic as defined in backend controller (Without prefix --> Util will prefix it)
//      * @param listener - Handler dealing with the message body as string
//      * @return - The subscription with the unsubcribe() method to remove the listener
//      */
//     const addListenerToTopic : SubscriptionFunction = (topic, listener) => {
//         if (socketClient.connected) {
//             return socketClient.subscribe(addPrefixToTopic(topic), (msg: IMessage) => {
//                 listener(msg.body);
//             });
//         }
//         console.log("Failed to assign listener to topic:", topic, "-Client not connected");
//         return undefined;
//     }

//     // const removeListenerToTopic = (topic: string) => {
//     //     if (socketClient.connected) {
//     //         socketClient.unsubscibe(addPrefixToTopic(topic));
//     //         return;
//     //     }
//     //     console.log("Failed to assign listener to topic:", topic, "-Client not connected");
//     // }

//     /**
//      * Assign listener-handler to a topic
//      * @param topic - The topic as defined in backend controller
//      * @param msg - Message string to send into the topic
//      *
//      */
//     const test : IPublishParams = {
//         destination: ""
//     }
//     const publishToTopic = (topic: string, msg: string) => {
//         if (socketClient.connected) {
//             console.log("CLIENT SEND SOCK:", msg);
//             socketClient.publish({ destination: addPrefixToTopic(topic), headers:{'X-TENANT-ID':'tenant1'},body: msg })
//             return;
//         }
//         console.log("Failed to assign listener to topic:", topic, "-Client not connected");
//     }



//     // TODO: Backend should implement to match these api
//     const getDefaultTopicForUser : () => string = () => {
//         return "";
//     }
//     /**
//      * @Remark - Each user should have an topic dedicated to them in the server like: /lms/${userID}/general for general use
//      * @Remark - Assign listener-handler to user general-use topic on the backend
//      * @param listener - Handler dealing with the message body as string
//      * @return - The subscription with the unsubcribe() method to remove the listener
//      */
//     const addListenerDefaultTopic = (listener: MessageStringHandler) => {
//         return addListenerToTopic(getDefaultTopicForUser(),listener);
//     }

//     const publishToDefaultTopic = (msg: string) => {
//         publishToTopic(getDefaultTopicForUser(),msg);

//     }
//     return {
//         addPrefixToTopic,publishToDefaultTopic,publishToTopic,getDefaultTopicForUser,
//         addListenerDefaultTopic, addListenerToTopic
//     }
// })();

// export default StompUtils;

// // /**
// //  * @param topic - The topic as defined in backend controller (Without prefix --> Util will prefix it)
// //  * @returns topic prefixed so the api is cleaner
// //  * @example
// //  * With prefix as /api/v1, Return `/api/v1/${topic}``:
// //  */
// // export const addPrefixToTopic = (topic: string) => {
// //     const prefix = 'lms/'
// //     return `${prefix}${topic}`.replace(/([^:]\/)\/+/g, "$1");
// // }
// // /**
// //  * Assign listener-handler to a topic
// //  * @param topic - The topic as defined in backend controller (Without prefix --> Util will prefix it)
// //  * @param listener - Handler dealing with the message body as string
// //  */
// // export const addListenerToTopic = (topic: string, listener: (messageBody: string) => void) => {
// //     if (socketClient.connected) {
// //         socketClient.subscribe(addPrefixToTopic(topic), (msg: IMessage) => {
// //             listener(msg.body);
// //         });
// //         return;
// //     }
// //     console.log("Failed to assign listener to topic:", topic, "-Client not connected");
// // }
// //
// // export const removeListenerToTopic = (topic: string) => {
// //     if (socketClient.connected) {
// //         socketClient.unsubscibe(addPrefixToTopic(topic));
// //         return;
// //     }
// //     console.log("Failed to assign listener to topic:", topic, "-Client not connected");
// // }
// //
// // /**
// //  * Assign listener-handler to a topic
// //  * @param topic - The topic as defined in backend controller
// //  * @param msg - Message string to send into the topic
// //  */
// // export const publishToTopic = (topic: string, msg: string) => {
// //     if (socketClient.connected) {
// //         socketClient.publish({ destination: topic })
// //         return;
// //     }
// //     console.log("Failed to assign listener to topic:", topic, "-Client not connected");
// // }
// //
// //
// //
// // // TODO: Backend should implement to match these api
// //
// // const getDefaultTopicForUser : () => string = () => {
// //     return "";
// // }
// // /**
// //  * @Remark - Each user should have an topic dedicated to them in the server like: /lms/${userID}/general for general use
// //  * @Remark - Assign listener-handler to user general-use topic on the backend
// //  * @param listener - Handler dealing with the message body as string
// //  */
// // export const addListenerDefaultTopic = (listener: (messageBody: string) => void) => {
// //     addListenerToTopic(getDefaultTopicForUser(),listener);
// // }
// //
// // export const publishToDefaultTopic = (msg: string) => {
// //     publishToTopic(getDefaultTopicForUser(),msg);
// //
// // }
// //


export {}