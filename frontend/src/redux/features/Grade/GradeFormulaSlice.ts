import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../store/store";

export interface Tag {
  id: number;
  title: string;
}

export type Operator = "+" | "-" | "*" | "/";

export type Bracket = "(" | ")";

export type Operand = Tag | number;

export type ElementFormula =
  | {
      type: "Operator";
      value: Operator;
    }
  | {
      type: "Tag";
      value: Tag;
    }
  | {
      type: "Bracket";
      value: Bracket;
    }
  | {
      type: "Number";
      value: number;
    };

const initialState: {
  title: string;
  tags: Tag[];
  formula: ElementFormula[];
  isPublic: boolean;
  isDragging: boolean;
} = {
  title: "",
  tags: [],
  formula: [],
  isPublic: false,
  isDragging: false,
};

export const gradeFormulaSlice = createSlice({
  name: "GRADE_FORMULA",
  initialState,
  reducers: {
    updateTagTitle: (state, action: PayloadAction<string>) => {
      state.title = action.payload;
    },
    updateTags: (state, action: PayloadAction<Tag[]>) => {
      state.tags = action.payload;
      state.formula = state.formula.filter(
        (ele) =>
          ele.type !== "Tag" ||
          action.payload.map((t) => t.id).includes(ele.value.id)
      );
    },
    appendFormula: (state, action: PayloadAction<ElementFormula>) => {
      state.formula.push(action.payload);
    },
    updateFormula: (state, action: PayloadAction<ElementFormula[]>) => {
      state.formula = [
        ...action.payload.map((ele) =>
          ele.type === "Tag" ? { ...ele, value: { ...ele.value } } : { ...ele }
        ),
      ];
    },
    setDraggingStatus: (state, action: PayloadAction<boolean>) => {
      state.isDragging = action.payload;
    },
    // @ts-ignore
    resetGradeFormula: (state) => {
      state = {
        title: "",
        tags: [],
        formula: [],
        isPublic: false,
        isDragging: false,
      };
    },
    updateIsPublic: (state, action: PayloadAction<boolean>)=>{
      state.isPublic = action.payload;
    }
  },
});

export const {
  appendFormula,
  updateTags,
  updateFormula,
  setDraggingStatus,
  updateTagTitle,
  resetGradeFormula,
  updateIsPublic
} = gradeFormulaSlice.actions;

export const gradeFormulaSeletor = {
  titleSelector: (state: RootState) => state.gradeFormula.title,
  tagsSelector: (state: RootState) => state.gradeFormula.tags,
  formulaSelector: (state: RootState) => state.gradeFormula.formula,
  isPublicSelector: (state: RootState) => state.gradeFormula.isPublic,
  isDraggingSelector: (state: RootState) => state.gradeFormula.isDragging,
};
