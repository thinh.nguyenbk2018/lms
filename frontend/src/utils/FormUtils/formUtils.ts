import { ReactNode } from "react";

export const getFieldNameWithNamespace = (namespace: string | null | undefined, localName: string) => {
	return namespace ? `${namespace}.${localName}` : `${localName}`;
};

export const FORM_DEFAULT_MESSAGES =  {
	REQUIRED: "Trường này là bắt buộc",
	IS_A_NUMBER: "Trường này phải là một số hợp lệ",
	TOO_SHORT: "Quá ngắn",
	TOO_LONG: "Quá dài",
	INVALID: "Không hợp lệ. kiểm tra lại"
}


export type LabelForFormFields<Type> = {
	[Property in keyof Type]: ReactNode | string;
}

export type FormNamespace = string | null | undefined;