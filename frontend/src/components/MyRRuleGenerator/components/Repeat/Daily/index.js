import React from 'react';
import PropTypes from 'prop-types';
import numericalFieldHandler from '../../../utils/numericalFieldHandler';
import translateLabel from '../../../utils/translateLabel';
import {Input, Row, Space} from "antd";

const RepeatDaily = ({
  id,
  daily: {
    interval,
  },
  handleChange,
  translations
}) => (
  <Row className="form-group row d-flex align-items-sm-center" style={{padding:`8px`}}>
      <Space>
          {translateLabel(translations, 'repeat.daily.every')}
          <Input
              id={`${id}-interval`}
              name="repeat.daily.interval"
              aria-label="Repeat daily interval"
              className="form-control"
              value={interval}
              onChange={numericalFieldHandler(handleChange)}
          />
          {translateLabel(translations, 'repeat.daily.days')}
      </Space>
  </Row>
);
RepeatDaily.propTypes = {
  id: PropTypes.string.isRequired,
  daily: PropTypes.shape({
    interval: PropTypes.number.isRequired,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default RepeatDaily;
