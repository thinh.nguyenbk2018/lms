import {Button, Card, Col, Modal, Row, Space} from "antd";
import {FieldArray, Formik} from "formik";
import {DatePicker, Form, FormItem, Input, InputNumber, Select} from "formik-antd";
import * as Yup from "yup";
import {DAY_OF_WEEK_NAMES, DAY_OF_WEEK_NAMES_MAP,} from "@components/Calendar/Utils/CalendarUtils";
import {useAppDispatch, useAppSelector,} from "@hooks/Redux/hooks";
import {
    ClassSchedule,
    SessionRecurrence,
    useCreateNewClassScheduleMutation,
    useUpdateClassScheduleMutation,
} from "@redux/features/Class/ClassTimeTable/ClassTimeTableAPI";
import {
    classTimeTableActions,
    classTimeTableSelectors,
} from "@redux/features/Class/ClassTimeTable/ClassTimeTableSlice";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import {getFieldNameWithNamespace} from "@utils/FormUtils/formUtils";
import CancelButton from "../../../../../components/ActionButton/CancelButton";
import {HOUR_MINUTE_PATTERN} from "@utils/TimeStampHelper/TimeStampHelper";
import {useParams} from "react-router-dom";
import './ClassSessionFormModal.less';

const FORM_TITLES = {
    STARTED_AT: "startedAt",
    FINISHED_AT: "finishedAt",
    NUM_OF_RECURRENCE: "numberOfSession",
    SESSION_RECURRENCE_TITLE: "sessionRecurrence",
    SESSION_RECURRENCE: {
        DAY_OF_WEEK: "dayOfWeek",
        STARTED_AT: "startedAt",
        FINISHED_AT: "finishedAt",
    },
    // NOTIFY_WITH_EMAIL: "notifyWithEmail",
};

const sessionRecurrenceInitialValues: SessionRecurrence = {
    dayOfWeek: "MONDAY",
    startedAt: "",
    finishedAt: "",
};

type RecurrenceFormProps = {
    namespace?: string;
};
export const SessionRecurrenceForm = (props: RecurrenceFormProps) => {
    const {namespace} = props;

    const getFieldName = (name: string) => {
        return getFieldNameWithNamespace(namespace, name);
    };

    return (
        <>
            <Form>
                <FormItem
                    required={true}
                    name={getFieldName(`${FORM_TITLES.SESSION_RECURRENCE.DAY_OF_WEEK}`)}
                    label={"Thứ trong tuần:"}
                >
                    <Select
                        name={getFieldName(`${FORM_TITLES.SESSION_RECURRENCE.DAY_OF_WEEK}`)}
                    >
                        {DAY_OF_WEEK_NAMES.map((item, index) => {
                            return (
                                <Select.Option value={item} key={index}>
                                    {DAY_OF_WEEK_NAMES_MAP[item]}
                                </Select.Option>
                            );
                        })}
                    </Select>
                </FormItem>

                <Row>
                    <Col span={11}>
                        <FormItem
                            name={getFieldName(
                                `${FORM_TITLES.SESSION_RECURRENCE.STARTED_AT}`
                            )}
                            label={"Từ (HH:MM)"}
                            required={true}
                        >
                            {/*<DatePicker*/}
                            {/*    picker="time"*/}
                            {/*    name={getFieldName(*/}
                            {/*        `${FORM_TITLES.SESSION_RECURRENCE.STARTED_AT}`*/}
                            {/*    )}*/}
                            {/*/>*/}
                            <Input name={getFieldName(
                                `${FORM_TITLES.SESSION_RECURRENCE.STARTED_AT}`
                            )} />
                        </FormItem>
                    </Col>

                    <Col span={11} offset={2}>
                        <FormItem
                            name={getFieldName(
                                `${FORM_TITLES.SESSION_RECURRENCE.FINISHED_AT}`
                            )}
                            required={true}
                            label={"Đến (HH:MM)"}
                        >
                            {/*<DatePicker*/}
                            {/*    picker="time"*/}
                            {/*    name={getFieldName(*/}
                            {/*        `${FORM_TITLES.SESSION_RECURRENCE.FINISHED_AT}`*/}
                            {/*    )}*/}
                            {/*/>*/}
                            <Input name={getFieldName(
                                `${FORM_TITLES.SESSION_RECURRENCE.FINISHED_AT}`
                            )} />
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        </>
    );
};
const classScheduleInitialValues: ClassSchedule = {
    startedAt: new Date(),
    numberOfSession: 1,
    sessionRecurrence: [],
};
const classScheduleValidationSchema = Yup.object().shape({
    "startedAt": Yup.date().nullable().required("Ngày bắt đầu là thông tin bắt buộc"),
    "sessionRecurrence": Yup.array().min(1, 'Thông tin buổi học là trường bắt buộc').of(Yup.object().shape({
        "startedAt": Yup.string().required('Giờ bắt đầu là thông tin bắt buộc').matches(HOUR_MINUTE_PATTERN, "Giờ phải theo định dạng HH:MM"),
        "finishedAt": Yup.string().required('Giờ bắt đầu là thông tin bắt buộc').matches(HOUR_MINUTE_PATTERN, "Giờ phải theo định dạng HH:MM")
    })),
    "numberOfSession": Yup.number().nullable().required('Số buổi học là thông tin bắt buộc')
        .min(1, 'Số buổi học phải lớn hơn 1')
});
const ClassScheduleFormModal = () => {
    const isAddingClassSchedule = useAppSelector(
        classTimeTableSelectors.selectIsAddingClassSchedule
    );
    const classScheduleInEdit = useAppSelector(
        classTimeTableSelectors.selectClassScheduleInEdit
    );

    const {classId} = useParams();

    const dispatch = useAppDispatch();

    const [createNewSchedule] = useCreateNewClassScheduleMutation();
    const [updateSchedule] = useUpdateClassScheduleMutation();
    return (
        <>
            <Formik
                validateOnChange={true}
                enableReinitialize={true}
                initialValues={(classScheduleInEdit != undefined && classScheduleInEdit.numberOfSession != 0)
                    ? (classScheduleInEdit.startedAt > new Date() ? classScheduleInEdit : {...classScheduleInEdit, startedAt: new Date()})
                    : classScheduleInitialValues}
                validationSchema={classScheduleValidationSchema}
                onSubmit={(values, {setSubmitting}) => {
                    const valuesToSubmit = {...values};
                    // const transformedRecurrence = valuesToSubmit.sessionRecurrence.map(
                    //     (item) => ({
                    //         ...item,
                    //         startedAt: getHourAndMinuteFromDate(parseDate(item.startedAt)),
                    //         finishedAt: getHourAndMinuteFromDate(parseDate(item.finishedAt)),
                    //     })
                    // );
                    // valuesToSubmit.sessionRecurrence = transformedRecurrence;
                    // console.log("WILL Submit values: ", valuesToSubmit);
                    if (classScheduleInEdit) {
                        AntdNotifier.handlePromise(
                            updateSchedule({
                                classId: Number(classId),
                                classSchedule: valuesToSubmit,
                            }).unwrap(),
                            "Thiết lập thời khóa biểu",
                            res => {
                                dispatch(classTimeTableActions.resetAfterChangeScheduler());
                                setSubmitting(false);
                            }
                        );
                    } else if (isAddingClassSchedule) {
                        AntdNotifier.handlePromise(
                            createNewSchedule({
                                classId: Number(classId),
                                classSchedule: valuesToSubmit,
                            }).unwrap(),
                            "Tạo lịch học lớp",
                            (res) => {
                                dispatch(classTimeTableActions.closeModal());
                                setSubmitting(false);
                            }
                        );
                    }
                }}
            >
                {({values, handleSubmit, isValid, dirty, isSubmitting}) => {
                    return (
                        <Modal
                        width={900}
                            bodyStyle={{ overflowY: 'auto', maxHeight: 'calc(100vh - 250px)' }}
                            maskClosable={false}
                            title={"Thiết lập thời khóa biểu"}
                            visible={!!classScheduleInEdit || isAddingClassSchedule}
                            onOk={() => {
                                handleSubmit();
                            }}
                            onCancel={() => {
                                dispatch(classTimeTableActions.closeModal());
                            }}
                            okButtonProps={{
                                disabled: !dirty || isSubmitting,
                                loading: isSubmitting
                            }}
                            okText={"Thiết lập"}
                            cancelText={"Hủy"}
                        >
                            <Form layout="vertical" className={'class-scheduler-form'} preserve={false}>
                                <FormItem
                                    name={FORM_TITLES.STARTED_AT}
                                    required={true}
                                    label={"Bắt đầu từ ngày"}
                                    wrapperCol={{span: 16}}
                                >
                                    <DatePicker className={'class-scheduler-form-date-start'} name={FORM_TITLES.STARTED_AT}/>
                                </FormItem>
                                <FormItem
                                    required={true}
                                    name={FORM_TITLES.NUM_OF_RECURRENCE}
                                    label={"Tổng số buổi học"}
                                >
                                    <InputNumber name={FORM_TITLES.NUM_OF_RECURRENCE}/>
                                </FormItem>
                                <FieldArray
                                    name={"sessionRecurrence"}
                                    render={(arrayHelpers: any) => {
                                        return (
                                            <>
                                                {values.sessionRecurrence.map((item: any, index: any) => {
                                                    return (
                                                        <Card
                                                            title={"Buổi học"}
                                                            extra={
                                                                <Space>
                                                                    <CancelButton
                                                                        onClick={() => arrayHelpers.remove(index)}
                                                                    />
                                                                </Space>
                                                            }
                                                            style={{
                                                                margin: "1em",
                                                            }}
                                                            headStyle={{
                                                                backgroundColor: "#f8f9fa",
                                                                color: "#1890ff",
                                                            }}
                                                        >
                                                            <SessionRecurrenceForm
                                                                namespace={`sessionRecurrence[${index}]`}
                                                            />
                                                        </Card>
                                                    );
                                                })}
                                                <Row justify="center">
                                                    <Button
                                                        onClick={() => {
                                                            arrayHelpers.push(sessionRecurrenceInitialValues);
                                                        }}
                                                        type={"primary"}
                                                    >
                                                        Thêm một buổi học
                                                    </Button>
                                                </Row>
                                                {/*<JsonDisplay*/}
                                                {/*    cardProps={{title: "Values"}}*/}
                                                {/*    obj={values}*/}
                                                {/*/>*/}
                                            </>
                                        );
                                    }}
                                />
                            </Form>
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

export default ClassScheduleFormModal;
