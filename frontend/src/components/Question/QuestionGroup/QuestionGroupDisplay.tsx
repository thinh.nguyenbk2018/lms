import React from "react";
import {
    createTestContentActions,
    createTestContentSelectors,
    QuestionGroup,
    QuestionType,
} from "../../../redux/features/Quiz/CreateQuizSlice";
import { Card, Row, Typography } from "antd";
import QuestionContainerItem from "../QuestionContainerItem/QuestionContainerItem";
import TextEditor from "../../TextEditor/TextEditor";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { RootState } from "../../../redux/store/store";
import MenuAddNewQuestion from "@pages/Tenant/Courses/CourseQuiz/TestContent/MenuAddNewQuestion";
import './QuestionGroupDisplay.less';
import { useRemoveQuestionsFromTestMutation } from "@redux/features/Quiz/examAPI";
import { useParams } from "react-router-dom";
import { confirmationModal } from "@components/Modal/DeleteConfirmation";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

/** Dumb component that take in a question GROUP and render it correctly*/
const QuestionGroupDisplay = (props: QuestionGroup & { mode: "view" | "doing", groupIndex: number | undefined}) => {
    const { id, description, questionList, note, mode, groupIndex } = props;
    const dispatch = useAppDispatch();
    const { testId } = useParams();

    const [removeQuestion] = useRemoveQuestionsFromTestMutation();

    const isQuestionGroupBeingEdited = useAppSelector((state: RootState) =>
        createTestContentSelectors.selectIsQuestionGroupBeingEdited(state, id)
    );

    function handleMenuClick(e: any) {
        let choice: QuestionType = e.key;
        dispatch(createTestContentActions.setOpeningDrawerType(choice));
        dispatch(createTestContentActions.setQuestionGroupIdInDraft(id!));
    }

    return (
        <Card
            className={"question-group-content-container"}
            style={{ opacity: isQuestionGroupBeingEdited ? 0.3 : 1 }}
            actions={mode === "view" ? [<MenuAddNewQuestion handleMenuClick={handleMenuClick} />] : []} // view here means update
        >
            {
                JSON.parse(props?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                    <AttachmentRender attachment={a} />
                </Row>)
            }
            <Typography.Text strong={true} type={"secondary"}>
                Đề sau đây áp dụng cho các câu hỏi trong nhóm câu hỏi:
            </Typography.Text>
            <br />
            <TextEditor
                onChange={() => {
                }}
                readOnly={true}
                useBubbleTheme={true}
                value={description}
                additionalStyles={{ scrollbarWidth: "none" }}
            />
            <Typography.Text strong={true} type={"secondary"} style={{ display: "block", marginBottom: '0.25rem' }}>
                Trả lời các câu hỏi sau:
            </Typography.Text>
            <br />
            <div className={"question-group__question-list-container"}>
                {questionList.map((item, index) => {
                    // console.log('group mode: ', mode);
                    return (
                        <QuestionContainerItem
                            questionData={item}
                            key={index}
                            canEdit={true}
                            mode={mode}
                            order={index + 1}
                            inGroup={true}
                            groupIndex={groupIndex}
                            onDeleteCallBack={() => {
                                confirmationModal('câu hỏi', `${index + 1} trong group câu hỏi`, () => {
                                    if (item && item.id) {
                                        removeQuestion({
                                            testId: Number(testId),
                                            questionId: item.id,
                                        })
                                            .unwrap()
                                            .then(result => {
                                                AntdNotifier.success(`Xóa câu hỏi thành công`)
                                            })
                                            .catch(error => {
                                                console.log(error)
                                                if (error?.data) {
                                                    AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá xóa. Thử lại sau.")
                                                } else {
                                                    AntdNotifier.error("Có lỗi trong quá xóa. Thử lại sau.")
                                                }
                                            })
                                    }
                                });
                            }}
                        />
                    );
                })}
            </div>
        </Card>
    );
};

export default QuestionGroupDisplay;
