import {Divider, Typography} from "antd";
import React, {CSSProperties} from "react";
import COLOR_SCHEME from "../../constants/ThemeColor";
import "./ContentHeader.less";

interface ContentHeaderProps {
    title: string;
    action?: React.ReactElement;
    additionalStyle?: CSSProperties;
    actionStyles?: CSSProperties;
}

const ContentHeader = ({
                           title,
                           action,
                           additionalStyle,
                           actionStyles
                       }: ContentHeaderProps) => {
    return (
        <>
            <div
                className={"header-wrapper"}
                style={{
                    ...additionalStyle,
                }}
            >
                <Typography.Title
                    className={"title"}
                    level={3}
                    style={{color: COLOR_SCHEME.primary}}
                >
                    {title}
                </Typography.Title>
                <div className={"action"} style={actionStyles}>{action}</div>
            </div>
            <Divider style={{margin: 4}}/>
        </>
    );
};

export default React.memo(ContentHeader);
