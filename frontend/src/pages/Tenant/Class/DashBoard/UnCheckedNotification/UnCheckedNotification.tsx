import {Card, Typography} from "antd";
const UnCheckedNotification = () => {
    return (
        <div>
            <Card>
                <Typography.Title>
                    Unchecked notification
                </Typography.Title>
            </Card>
        </div>
    );
};

export default UnCheckedNotification;