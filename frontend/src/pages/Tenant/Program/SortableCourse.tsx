import SortableTree, {TreeItem} from 'react-sortable-tree';
import 'react-sortable-tree/style.css';
import './SortableCourseStyle.less';
import {CourseItem, useGetCourseListQuery} from "../../../redux/features/Course/CourseAPI";
import React, {useState} from "react";
import {Button, Form, Modal, Row, Select} from "antd";
import {programSelector, setCoursesInProgram} from "../../../redux/features/Program/ProgramSlice";
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {DeleteOutlined, PlusCircleOutlined} from "@ant-design/icons";
import {courseIconSvg} from "../../../components/Icons/SvgIcons";
import antdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";

interface SortableCourseProps {
    courses?: [{ id: number, code: string, name: string }],
    setField: any,
    data?: any
}

type CourseItemInTree = {
    title: string,
    data: CourseItem
}

const SortableCourse = ({setField}: SortableCourseProps) => {

    const dispatch = useAppDispatch();
    const {data: courseList} = useGetCourseListQuery('');
    const selectCoursesInProgram = useAppSelector(programSelector.selectCoursesInProgram);
    const filteredCourses = courseList?.listData?.filter(o => !selectCoursesInProgram.map(s => s.code).includes(o.code));
    const [form] = Form.useForm();
    const [visibleAddCourseModal, setVisibleAddCourseModal] = useState(false);

    const onDragAndDropCourse = (coursesInProgram: TreeItem<CourseItemInTree>[]) => {
        setField('courses', coursesInProgram);
        dispatch(setCoursesInProgram(coursesInProgram.map(c => c.data)));
    }

    const selectedCourses = (ids: number[]) => {
        if (!courseList) return;
        const courses = courseList.listData.filter(c => ids.includes(c.id))
            .map(c => {return {id: c.id, name: c.name, code: c.code}});
        const coursesToBeUpdated = selectCoursesInProgram.concat(courses);
        setField('courses', coursesToBeUpdated);
        dispatch(setCoursesInProgram(coursesToBeUpdated));
    }

    const onDeselectCourse = (ids: number[]) => {
        if (!courseList) return;
        const deselectedCourses = selectCoursesInProgram.filter(c => !ids.includes(c.id));
        setField('courses', deselectedCourses);
        dispatch(setCoursesInProgram(deselectedCourses));
    }

    return (
        <>
                <div>Khóa học</div>
                <Row hidden={selectCoursesInProgram == undefined || selectCoursesInProgram.length === 0}>
                    <SortableTree<CourseItemInTree>
                        treeData={selectCoursesInProgram?.map(c => {
                            return {title: c.name, data: c};
                        })}
                        style={{width: "100%"}}
                        onChange={onDragAndDropCourse}
                        isVirtualized={false}
                        generateNodeProps={({node}) => ({
                            title: (
                                <>
                                    <div className={'selected-course-container'}>
                                        <div>
                                            {courseIconSvg}
                                            <span>{node.data.name}</span>
                                        </div>
                                        <Button
                                            onClick={(event) => {
                                                event.stopPropagation();
                                                onDeselectCourse([node.data.id])
                                            }}
                                            type="link"
                                            danger
                                        >
                                            <DeleteOutlined/>
                                        </Button>
                                    </div>
                                </>
                            )
                        })}
                    />
                </Row>
                <Row>
                    <Button
                        className={'add-course-btn'}
                        onClick={(event) => {
                            event.stopPropagation();
                            if (filteredCourses && filteredCourses.length > 0) {
                                setVisibleAddCourseModal(true);
                            }
                            else {
                                antdNotifier.error('Đã hết khóa học', 'Thông báo', 1);
                            }
                        }}
                        icon={<PlusCircleOutlined/>}
                    >
                        Thêm khóa học
                    </Button>
                </Row>
            <Modal
                title="Thêm khóa học"
                visible={visibleAddCourseModal}
                onCancel={() => {
                    form.resetFields();
                    setVisibleAddCourseModal(false);
                }}
                maskClosable={false}
                onOk={() => {
                    const selectedCoursesId = form.getFieldsValue().courses;
                    if (selectedCoursesId && selectedCoursesId.length > 0) {
                        selectedCourses(selectedCoursesId);
                    }
                    setVisibleAddCourseModal(false);
                    form.resetFields();
                }}
                width={750}
            >
                <Form form={form}>
                    <Form.Item
                        name={`courses`}
                    >
                        <Select<number, CourseItem>
                            className={'course-program-select'}
                            mode={'multiple'}
                            optionFilterProp="children"
                            showSearch
                            showArrow={true}
                            filterOption={(input, option) =>
                                option!.name.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {filteredCourses?.map(c => <Select.Option
                                    value={c.id}
                                    key={c.id}
                                    id={c.id}
                                    code={c.code}
                                    name={c.name}
                                >
                                    {c.name}
                                </Select.Option>)
                            }
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
}

export default SortableCourse;