import {faCircleXmark} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useAppSelector} from "@hooks/Redux/hooks";
import {
    useDeleteTeacherFromClassMutation,
    useGetTeacherForClassQuery,
} from "@redux_features/Class/TeacherAPI/ClassTeacherAPI";
import {classTeacherSelectors} from "../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";
import TeacherList from "../TeacherList";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {Spin} from "antd";
import ApiErrorWithRetryButton from "@components/Util/ApiErrorWithRetryButton";
import TeacherRoleTagMenuWrapper from "../TeacherRoleTagMenuWrapper";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
// TODO: haveAdmin PERMISSION CHECK
const AlreadyInClassTeacherList = ({
    classId,
    teacherDeleteHandler,
}: {
    classId: number;
    teacherDeleteHandler?: () => void;
}) => {
    const haveAdminPermission = true;
    // Redux state
    const {
        data: inClassTeacherList,
        isSuccess: isInClassTeacherListSuccess,
        isLoading: isInClassTeacherListLoading,
        isFetching: isInClassTeacherListFetching,
        isUninitialized,
        isError: isInClassTeacherListError,
        error,
        refetch,
    } = useGetTeacherForClassQuery(
        { classId: Number(classId) },
        {
            skip: isNaN(Number(classId)),
        }
    );

    const [deleteTeacherHandler] = useDeleteTeacherFromClassMutation();
    const currentlyInClassTeachers = useAppSelector(
        classTeacherSelectors.selectCurrentlyInClassTeachers
    );
    const teachers = Object.values(currentlyInClassTeachers);
    // if (!currentlyInClassTeachers || teachers.length < 1) {
    //     return <span> Không có giáo viên nào </span>;
    // }
    if (isInClassTeacherListSuccess) {
        return (
            <TeacherList
                listData={inClassTeacherList.listData}
                onItemDelete={(teacherId) => {
                    AntdNotifier.handlePromise(
                        deleteTeacherHandler({
                            classId,
                            teacherId,
                        }).unwrap(),
                        "Xóa giáo viên khỏi lớp học",
                        () => {},
                        () => {}
                    );
                }}
                teacherRoleTag={(item) => {
                    const itemId = item.id || item.userId;
                    if (!item.role || !itemId) {
                        return <></>;
                    }
                    return (
                        <TeacherRoleTagMenuWrapper
                            id={itemId}
                            classId={classId}
                            role={item.role}
                        />
                    );
                }}
                additionalItemActions={({ itemId, firstName, lastName }) => (
                    <div
                        className="card-action-item card-delete-action"
                        onClick={() => {
                            confirmationModal('giáo viên', `${lastName} ${firstName}`, () => {
                                AntdNotifier.handlePromise(
                                    deleteTeacherHandler({
                                        classId,
                                        teacherId: itemId,
                                    }).unwrap(),
                                    `Xóa giáo viên ${lastName} ${firstName} khỏi lớp học`,
                                    () => {},
                                    () => {}
                                );
                                teacherDeleteHandler?.();
                            }, 'ra khỏi lớp học');
                        }}
                    >
                        <FontAwesomeIcon icon={faCircleXmark} />
                    </div>
                )}
            />
        );
    } else if (isInClassTeacherListFetching) {
        return <Spin />;
    } else if (isInClassTeacherListError) {
        return (
            <ApiErrorWithRetryButton
                message="Nhấn nút để  tải lại"
                onRetry={refetch}
            />
        );
    } else {
        return <></>;
    }
};

export default AlreadyInClassTeacherList;
