"use strict";
exports.__esModule = true;
var react_1 = require("react");
var ContentHeader_1 = require("../../../components/ContentHeader/ContentHeader");
var FileManager_1 = require("../../../components/FileManager/FileManager");
var FilePage = function () {
    return react_1["default"].createElement(react_1["default"].Fragment, null,
        react_1["default"].createElement(ContentHeader_1["default"], { title: "Qu\u1EA3n l\u00FD file" }),
        react_1["default"].createElement(FileManager_1["default"], null));
};
exports["default"] = FilePage;
