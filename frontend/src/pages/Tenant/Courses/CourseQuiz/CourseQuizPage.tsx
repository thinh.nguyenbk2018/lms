import {QuizItem} from "@typescript/interfaces/courses/CourseTypes";
import {useNavigate, useParams} from "react-router-dom";
import {useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {Card, Col, Row, Space, Tooltip, Typography} from "antd";
import {QuizIcon} from "@components/TeachingContent/NewActivityMenu";
import COLOR_SCHEME from "@constants/ThemeColor";
import {testSvg} from "@components/Icons/SvgIcons";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {DeleteOutlined} from "@ant-design/icons";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {ROUTING_PATH_PARAM_KEY} from "../../../../navigation/ROUTING_PARAM_KEY";
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import CustomEmpty from "@components/Util/CustomEmpty";
import LoadingPage from "@pages/UtilPages/LoadingPage";
import ApiErrorWithRetryButton from "@components/Util/ApiErrorWithRetryButton";
import React from "react";
import {useDeleteQuizMutation, useGetQuizCourseQuery} from "@redux/features/Course/CourseAPI";
import useCourseSidebar from "@hooks/Routing/useCourseSidebar";

const QuizItemCard = (props: QuizItem) => {
    const { courseId } = useParams();
    const { title, description, tag, examId, state, id, isFromCourse } = props;

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const navigate = useNavigate();

    const navigateToQuizDetail = () => {
        navigate(
            replacePathParams(
                ROUTING_CONSTANTS.COURSE_QUIZ_ITEM_DETAIL_PAGE,
                [":courseId", Number(courseId)],
                [":quizItemId", id]
            )
        );
    };

    const [deleteQuiz] = useDeleteQuizMutation();

    const deleteQuizItem = (quizId: number | undefined) => {
        if (courseId == undefined || quizId == undefined) {
            return;
        }
        deleteQuiz({courseId: Number(courseId), quizId: quizId})
            .unwrap()
            .then(res => antdNotifier.success("Xóa bài kiểm tra thành công"))
            .catch(err => antdNotifier.error("Có lỗi trong quá trình xóa bài kiểm tra"));
    };
    return (
        <Card
            className="class-quiz-card-item"
            title={
                <Space align="center">
                    <QuizIcon />
                    <p style={{ margin: 0, padding: 0 }}>
                        {title || "Không có thông tin"}
                    </p>
                </Space>
            }
            extra={
                <Space>
                    <div className="action-container">
                        <Tooltip title={"Chi tiết"}>
                            <Typography.Link
                                className={'flex'}
                                style={{ marginRight: '0.25rem', color: COLOR_SCHEME.primary }}
                                onClick={() => {
                                    navigateToQuizDetail();
                                }}
                            >
                                {testSvg}
                            </Typography.Link>
                        </Tooltip>
                        <PermissionRenderer permissions={[PermissionEnum.DELETE_QUIZ_CLASS]}>
                            {isFromCourse == false && <DeleteOutlined
                                className="delete"
                                onClick={() => {
                                    confirmationModal('bài kiểm tra', title, () => {
                                        deleteQuizItem(id);
                                    });
                                }}
                            />}
                        </PermissionRenderer>
                    </div>
                </Space>
            }
        >
            <Row>
                <Col className={'class-quiz-page-label'} span={2}>Mô tả</Col>
                <Col span={22}>{description || "Không có mô tả cho bài kiểm tra này"}</Col>
            </Row>
        </Card>
    );
};

// * Render list of test with config that was made public and accessible by this user
const CourseQuizPage = () => {
    const {
        courseId,
        [ROUTING_PATH_PARAM_KEY.TEST_WITH_CONFIG]: examWithConfigId,
    } = useParams();

    const {
        forceCloseSidebar
    } = useCourseSidebar();

    const {
        data: quizList,
        isSuccess,
        isFetching,
        isError,
        error,
        refetch,
    } = useGetQuizCourseQuery({ courseId: Number(courseId)}, {skip: isNaN(Number(courseId))});

    return (
        <div>
            <ContentHeader title={`Bài kiểm tra`} action={<></>} />
            {
                quizList?.length === 0 && <CustomEmpty description="Không có bài kiểm tra nào." />
            }
            {isSuccess && (
                <>
                    {quizList.map((item, index) => {
                        return <QuizItemCard {...item} key={item.id} />;
                    })}
                </>
            )}
            {isFetching && <LoadingPage />}
            {isError && (
                <Row align={"middle"} justify={"center"}>
                    <ApiErrorWithRetryButton onRetry={() => { }} />;
                </Row>
            )}
        </div>
    );
};

export default CourseQuizPage;
