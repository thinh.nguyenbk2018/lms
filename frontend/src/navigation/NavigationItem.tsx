import {Navigate} from "react-router-dom";
import viVN from 'antd/es/locale/vi_VN';
import App from "../App";
import CreateOrUpdateNotification from "../components/Notification/CreateAnnouncement";
import NotificationDetail from "../components/Notification/NotificationDetail";
import DashBoardPage from "@pages/DashBoardPage";
import DashboardLayout from "@pages/Host/Dashboard/DashboardLayout/DashboardLayout";
import ExtendService from "@pages/Host/Dashboard/ExtendService/ExtendServicePage";
import HomePage from "@pages/Host/HomePage/HomePage";
import HostLayout from "@pages/Host/Layout/HostLayout";
import LoginPage from "@pages/Host/Login/LoginPage";
import NewTenantPage from "@pages/Host/Tenant/NewTenantPage";
import RegisterSuccessPage from "@pages/Host/Tenant/RegisterSuccessPage";
import LoginRegisterPage from "@pages/LoginRegister/LoginRegisterPage";
import CreateRolePage from "@pages/Tenant/Authorization/CreateRolePage";
import EditRolePage from "@pages/Tenant/Authorization/EditRolePage";
import PermissionPage from "@pages/Tenant/Authorization/GrantPermissionPage";
import RolesManagementPage from "@pages/Tenant/Authorization/RolesManagementPage";
import CreateStaffPage from "@pages/Tenant/Authorization/Staffs/CreateStaffPage";
import EditStaffPage from "@pages/Tenant/Authorization/Staffs/EditStaffPage";
import StaffsManagementPage from "@pages/Tenant/Authorization/Staffs/StaffsManagementPage";
import CalendarPage from "@pages/Tenant/Calendar/CalendarPage";
import ClassAttendanceDetailPage from "@pages/Tenant/Class/Attendance/ClassAttendanceDetailPage";
import ClassAttendancePage from "@pages/Tenant/Class/Attendance/ClassAttendancePage";
import ClassListPage from "@pages/Tenant/Class/ClassListPage";
import ClassContentPage from "@pages/Tenant/Class/Content/ClassContentPage";
import ClassQuizDetailPage from "@pages/Tenant/Class/Content/ClassQuizDetail/ClassQuizDetailPage";
import ClassUnitItemDetailPage from "@pages/Tenant/Class/Content/ClassUnitDetail/ClassUnitItemDetail";
import ClassVotingDetailPage from "@pages/Tenant/Class/Content/ClassVotingDetail/ClassVotingDetailPage";
import CreateClassOrUpdateClassInformation from "@pages/Tenant/Class/CreateClassOrUpdateClassInformation";
import ClassDashBoard from "@pages/Tenant/Class/DashBoard/ClassDashBoard";
import ClassDiscussionPage from "@pages/Tenant/Class/Discussion/ClassDiscussionPage";
import ClassQuizPage from "@pages/Tenant/Class/Quiz/ClassQuizPage";
import ClassStudentPage from "@pages/Tenant/Class/Student/ClassStudentPage";
import ClassTeacherPage from "@pages/Tenant/Class/Teacher/ClassTeacherPage";
import ClassTimeTable from "@pages/Tenant/Class/TimeTable/ClassTimeTable";
import CourseContentPage from "@pages/Tenant/Courses/CourseContent/CourseContentPage";
import CourseDashboard from "@pages/Tenant/Courses/CourseDashboard/CourseDashboard";
import CreateOrUpdateCoursePage1 from "@pages/Tenant/Courses/CourseDetail/CreateOrUpdateCoursePage1";
import CourseQuizBank from "@pages/Tenant/Courses/CourseQuiz/CourseQuizBank";
import CourseQuizItemDetailQuestionsPage
    from "@pages/Tenant/Courses/CourseQuiz/TestContent/CourseQuizItemDetailQuestionsPage";
import CoursesListPage from "@pages/Tenant/Courses/CoursesListPage";
import CourseTeacher from "@pages/Tenant/Courses/CourseTeacher/CourseTeacher";
import CourseQuizItemDetailPage from "@pages/Tenant/Courses/QuizItemDetail/CourseQuizItemDetail";
import CourseUnitItemDetailPage from "@pages/Tenant/Courses/UnitDetail/CourseUnitItemDetail";
import FilePage from "@pages/Tenant/FilePage/FilePage";
import NewForumPage from "@pages/Tenant/Forum/NewForumPage";
import CreateGradeFormulaPage from "@pages/Tenant/GradeFormula/CreateGradeFormulaPage";
import UpdateGradeFormulaPage from "@pages/Tenant/GradeFormula/UpdateGradeFormulaPage";
import GradeManagementPage from "@pages/Tenant/GradeManagement/GradeManagementPage";
import GradeResultPage from "@pages/Tenant/GradeResultPage/GradeResultPage";
import AddStudentPage from "@pages/Tenant/Student/AddStudentPage";
import EditStudentPage from "@pages/Tenant/Student/EditStudentPage";
import StudentsManagementPage from "@pages/Tenant/Student/StudentsManagementPage";
import TextBookPage from "@pages/Tenant/TextBook/TextBookPage";
import NotFoundPage from "@pages/UtilPages/NotFoundPage";
import UnAuthorizedPage from "@pages/UtilPages/UnAuthorizedPage";
// import QuizBankPage from "@pages/Tenant/Quiz/QuizBankPage";
import {HOST_ROUTING_CONSTANT, ROUTING_CONSTANTS} from "./ROUTING_CONSTANTS";
import ClassAnnouncementPage from "@pages/Tenant/Class/ClassAnnouncement/ClassAnnouncementPage";
import PersonalInfoPage from "@pages/Tenant/PersonalInfo/PersonalInfoPage";
import QuizClassTakingSession from "@pages/Tenant/Class/Quiz/QuizSession/QuizClassTakingSession";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import ClassQuizResultManagement from "@pages/Tenant/Class/Quiz/TeacherQuizResult/ClassQuizResultManagement";
import ClassQuizResultStudentSession
    from "@pages/Tenant/Class/Quiz/TeacherQuizResult/ClassQuizResultStudentSession/ClassQuizResultStudentSession";
import CourseUnitListPage from "@pages/Tenant/Courses/Units/CourseUnitListPage";
import TextbookDetail from "@components/TextBook/TextbookDetail";
import Settings from "@pages/Host/Dashboard/Settings/Settings";
import Personalized from "@pages/Host/Dashboard/Settings/Persionalized";
import CourseQuizPage from "@pages/Tenant/Courses/CourseQuiz/CourseQuizPage";
import ClassUnitListPage from "@pages/Tenant/Class/Unit/ClassUnitListPage";
import {ConfigProvider} from "antd";

export type NavigationItem = LinkNav | TabNav | SubMenuNav;

/**
 *  @param - If it is function: => Return true if this path is allowed else return false;
 *    - If it is array: => Specify the role required: User must have all role specified to be able to navigate to the Tab / Link
 */
export type TabNavAuthorizer = PermissionEnum[];

export interface SubMenuNav {
    key: string;
    icon: string;
    tab_name: string;
    required_permission: TabNavAuthorizer;
    type: "submenu";
    children: NavigationItem[];
}

export interface LinkNav {
    path: string;
    element: JSX.Element | undefined | null;
    required_permission: TabNavAuthorizer;
    type: "link";
    children?: NavigationItem[];
    key?: string;
}

export interface TabNav {
    path: string;
    key: string;
    icon: string;
    tab_name: string;
    element: JSX.Element | undefined | null;
    required_permission: TabNavAuthorizer;
    children?: NavigationItem[];
    type: "tab";
}

export function isTabNav(routingItem: NavigationItem): routingItem is TabNav {
    return (routingItem as TabNav).type !== "tab";
}

/*
        ! This route object is used for the single source of routing for entire app
        ! unused property should be set to ""
 */
export const TenantNavItems: NavigationItem[] = [
    {
        path: "/",
        element: <ConfigProvider locale={viVN}><App/></ConfigProvider>,
        required_permission: [],
        children: [
            {
                path: ROUTING_CONSTANTS.ROOT,
                element: <Navigate to={ROUTING_CONSTANTS.DASHBOARD}/>,
                required_permission: [],
                type: "link",
                key: "DASH_BOARD"
            },
            {
                path: ROUTING_CONSTANTS.DASHBOARD,
                key: "DASH_BOARD",
                icon: "HomeOutlined",
                element: <DashBoardPage/>,
                tab_name: "Trang chủ",
                required_permission: [],
                type: "tab",
            },
            // {
            //     path: ROUTING_CONSTANTS.PROGRAM_DETAIL,
            //     element: <CreateOrUpdateProgram />,
            //     required_permission: [],
            //     type: "link",
            //     key: ""
            // },
            // {
            //     path: ROUTING_CONSTANTS.PROGRAM_NEW,
            //     element: <CreateOrUpdateProgram />,
            //     required_permission: [],
            //     type: "link",
            // },
            // * Course
            {
                path: ROUTING_CONSTANTS.COURSE_MANAGEMENT,
                key: "COURSE",
                icon: "CourseOutlined",
                element: <CoursesListPage/>,
                tab_name: "Khóa học",
                required_permission: [PermissionEnum.VIEW_ALL_COURSE],
                type: "tab",
            },
            {
                path: ROUTING_CONSTANTS.NEW_COURSE,
                element: <CreateOrUpdateCoursePage1/>,
                required_permission: [PermissionEnum.CREATE_COURSE],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.EDIT_COURSE,
                element: <CreateOrUpdateCoursePage1/>,
                required_permission: [PermissionEnum.UPDATE_COURSE],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_DASHBOARD,
                element: <CourseDashboard/>,
                required_permission: [PermissionEnum.VIEW_DETAIL_COURSE],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_CLASS_LIST,
                key: "COURSE",
                element: <ClassListPage/>,
                required_permission: [PermissionEnum.VIEW_LIST_CLASS],
                type: "link",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_TEXTBOOK_MANAGEMENT,
                element: <TextBookPage/>,
                required_permission: [],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_CONTENT,
                element: <CourseContentPage/>,
                key: "COURSE",
                required_permission: [PermissionEnum.VIEW_DETAIL_COURSE],
                type: "link",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_UNIT_LIST_PAGE,
                element: <CourseUnitListPage/>,
                key: "COURSE",
                required_permission: [],
                type: "link",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_TEACHER,
                element: <CourseTeacher/>,
                required_permission: [PermissionEnum.VIEW_DETAIL_COURSE],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_EXAM,
                element: <CourseQuizBank/>,
                required_permission: [],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_QUIZ,
                element: <CourseQuizPage/>,
                required_permission: [],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_EXAM_DETAIL,
                element: <CourseQuizItemDetailQuestionsPage/>,
                required_permission: [],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_QUIZ_ITEM_DETAIL_PAGE,
                element: <CourseQuizItemDetailPage/>,
                required_permission: [],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_UNIT_ITEM_DETAIL_PAGE,
                element: <CourseUnitItemDetailPage/>,
                required_permission: [],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_TEXTBOOK_DETAIL,
                element: <TextbookDetail/>,
                required_permission: [],
                type: "link",
                key: "TEXT_BOOK_FOR_COURSE",
            },
            // * classes
            {
                path: ROUTING_CONSTANTS.CLASS_LIST,
                key: "CLASS",
                tab_name: "Lớp học",
                icon: "ClassOutlined",
                element: <ClassListPage/>,
                required_permission: [PermissionEnum.VIEW_LIST_CLASS],
                type: "tab",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_NEW,
                element: <CreateClassOrUpdateClassInformation/>,
                required_permission: [PermissionEnum.CREATE_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_CLASS_NEW,
                element: <CreateClassOrUpdateClassInformation/>,
                required_permission: [PermissionEnum.CREATE_CLASS],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_DETAIL_INFORMATION,
                element: <CreateClassOrUpdateClassInformation/>,
                required_permission: [PermissionEnum.VIEW_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_CLASS_EDIT,
                element: <CreateClassOrUpdateClassInformation/>,
                required_permission: [PermissionEnum.VIEW_CLASS],
                type: "link",
                key: "COURSE",
            },
            // * class : will display subsidebar
            {
                path: ROUTING_CONSTANTS.CLASS_DASHBOARD,
                element: <ClassDashBoard/>,
                required_permission: [PermissionEnum.VIEW_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.COURSE_CLASS_VIEW_DETAILS,
                element: <ClassDashBoard/>,
                required_permission: [PermissionEnum.VIEW_CLASS],
                type: "link",
                key: "COURSE",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_ANNOUNCEMENT,
                element: <ClassAnnouncementPage/>,
                required_permission: [PermissionEnum.VIEW_LIST_ANNOUNCEMENT],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_DETAIL_ANNOUNCEMENT_CREATE,
                element: <CreateOrUpdateNotification/>,
                required_permission: [PermissionEnum.CREATE_ANNOUNCEMENT_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_DETAIL_ANNOUNCEMENT_DETAIL,
                element: <NotificationDetail/>,
                required_permission: [PermissionEnum.CREATE_ANNOUNCEMENT_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_STUDENT,
                element: <ClassStudentPage/>,
                required_permission: [PermissionEnum.VIEW_LIST_STUDENT_IN_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_TEACHER,
                element: <ClassTeacherPage/>,
                required_permission: [PermissionEnum.VIEW_LIST_TEACHER_IN_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_CONTENT,
                element: <ClassContentPage/>,
                required_permission: [PermissionEnum.VIEW_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_CHECK_IN,
                element: <ClassAttendancePage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_CHECK_IN_SESSION_DETAIL,
                element: <ClassAttendanceDetailPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },

            {
                path: ROUTING_CONSTANTS.CLASS_TIME_TABLE,
                element: <ClassTimeTable/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_QUIZ,
                element: <ClassQuizPage/>,
                required_permission: [PermissionEnum.VIEW_LIST_QUIZ_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_TEXTBOOK_MANAGEMENT,
                element: <TextBookPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_QUIZ_SESSION,
                element: <QuizClassTakingSession/>,
                required_permission: [PermissionEnum.DO_QUIZ],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_MANAGEMENT,
                element: <ClassQuizResultManagement/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_STUDENT_SESSION,
                element: <ClassQuizResultStudentSession/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_DISCUSSION,
                element: <ClassDiscussionPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CREATE_STUDENT,
                element: <AddStudentPage/>,
                required_permission: [PermissionEnum.ADD_STUDENT],
                type: "link",
                key: "STUDENTS",
            },
            {
                path: ROUTING_CONSTANTS.EDIT_STUDENT,
                element: <EditStudentPage/>,
                required_permission: [PermissionEnum.UPDATE_STUDENT],
                type: "link",
                key: "STUDENTS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_STUDENT_DETAILS,
                element: <EditStudentPage/>,
                required_permission: [PermissionEnum.VIEW_DETAIL_STUDENT],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_TEXTBOOK_DETAIL,
                element: <TextbookDetail/>,
                required_permission: [],
                type: "link",
                key: "TEXT_BOOK_FOR_COURSE",
            },
            // grade formula
            {
                path: ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_CLASS,
                element: <GradeManagementPage scope="CLASS"/>,
                required_permission: [PermissionEnum.VIEW_LIST_GRADE_TAG_IN_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CREATE_GRADE_FORMULA_IN_CLASS,
                element: <CreateGradeFormulaPage scope="CLASS"/>,
                required_permission: [PermissionEnum.CREATE_GRADE_TAG_IN_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.UPDATE_GRADE_FORMULA_IN_CLASS,
                element: <UpdateGradeFormulaPage scope="CLASS"/>,
                required_permission: [PermissionEnum.UPDATE_GRADE_TAG],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.VIEW_GRADE_IN_CLASS,
                element: <GradeResultPage scope="CLASS"/>,
                required_permission: [PermissionEnum.VIEW_GRADE_RESULT_IN_CLASS],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.STUDENTS_MANAGEMENT,
                key: "STUDENTS",
                icon: "StudentOutlined",
                element: <StudentsManagementPage/>,
                tab_name: "Học viên",
                required_permission: [PermissionEnum.VIEW_LIST_STUDENT],
                type: "tab",
            },
            {
                path: ROUTING_CONSTANTS.CREATE_STUDENT,
                element: <AddStudentPage/>,
                required_permission: [PermissionEnum.ADD_STUDENT],
                type: "link",
                key: "STUDENTS",
            },
            {
                path: ROUTING_CONSTANTS.EDIT_STUDENT,
                element: <EditStudentPage/>,
                required_permission: [PermissionEnum.UPDATE_STUDENT],
                type: "link",
                key: "STUDENTS",
            },
            // {
            //     path: ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_COURSE,
            //     element: <GradeManagementPage scope="COURSE" />,
            //     required_permission: [],
            //     type: "link",
            // },
            // {
            //     path: ROUTING_CONSTANTS.UPDATE_GRADE_FORMULA_IN_COURSE,
            //     element: <UpdateGradeFormulaPage scope="COURSE" />,
            //     required_permission: [],
            //     type: "link",
            // },
            // {
            //     path: ROUTING_CONSTANTS.CREATE_GRADE_FORMULA_IN_COURSE,
            //     element: <CreateGradeFormulaPage scope="COURSE" />,
            //     required_permission: [],
            //     type: "link",
            // },
            // {
            //     path: ROUTING_CONSTANTS.VIEW_GRADE_IN_COURSE,
            //     element: <GradeResultPage scope="COURSE" />,
            //     required_permission: [],
            //     type: "link",
            // },
            {
                path: ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                element: <ClassQuizDetailPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_UNIT_LIST_PAGE,
                element: <ClassUnitListPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE,
                element: <ClassUnitItemDetailPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                path: ROUTING_CONSTANTS.CLASS_VOTING_ITEM_DETAIL_PAGE,
                element: <ClassVotingDetailPage/>,
                required_permission: [],
                type: "link",
                key: "CLASS",
            },
            {
                key: "CLASS",
                required_permission: [],
                type: "link",
                element: <CalendarPage/>,
                path: ROUTING_CONSTANTS.CLASS_CALENDAR,
            },
            {
                key: "CALENDAR",
                icon: "CalendarOutlined",
                tab_name: "Lịch",
                required_permission: [],
                type: "tab",
                element: <CalendarPage/>,
                path: ROUTING_CONSTANTS.PERSONAL_CALENDAR,
            },
            // * File manager
            {
                path: ROUTING_CONSTANTS.FILES,
                key: "FILES",
                icon: "FileManagerOutlined",
                element: <FilePage/>,
                tab_name: "Tệp tin",
                required_permission: [],
                type: "tab",
            },
            // * Certificate
            // {
            //     path: "/files",
            //     key: "CERTIFICATE",
            //     icon: "CertificateOutlined",
            //     element: <h1>Under Construction...</h1>,
            //     tab_name: "Chứng chỉ",
            //     required_permission: [],
            //     type: "tab",
            // },
            {
                key: "AUTHORIZATION",
                icon: "SiteMapOutlined",
                tab_name: "Cài đặt quyền hạn",
                required_permission: [
                    PermissionEnum.CREATE_STAFF,
                    PermissionEnum.DELETE_STAFF,
                    PermissionEnum.UPDATE_STAFF,
                    PermissionEnum.VIEW_ALL_STAFF,
                    PermissionEnum.VIEW_DETAIL_STAFF,
                    PermissionEnum.CREATE_ROLE,
                    PermissionEnum.UPDATE_ROLE,
                    PermissionEnum.DELETE_ROLE,
                    PermissionEnum.VIEW_DETAIL_ROLE,
                    PermissionEnum.VIEW_LIST_ROLE,
                    PermissionEnum.GRANT_PERMISSION
                ],
                type: "submenu",
                children: [
                    {
                        path: ROUTING_CONSTANTS.STAFFS_MANAGEMENT,
                        key: "STAFFS",
                        element: <StaffsManagementPage/>,
                        icon: "UserTieOutlined",
                        tab_name: "Nhân viên",
                        required_permission: [PermissionEnum.VIEW_ALL_STAFF],
                        type: "tab",
                    },
                    {
                        path: "/authorizaton/roles",
                        key: "ROLE",
                        element: <RolesManagementPage/>,
                        icon: "AddressCardOutlined",
                        tab_name: "Vai trò",
                        required_permission: [PermissionEnum.VIEW_LIST_ROLE],
                        type: "tab",
                    },
                    {
                        path: "/authorizaton/grant_permissions",
                        key: "PERMISSION",
                        element: <PermissionPage/>,
                        icon: "ListCheckOutlined",
                        tab_name: "Phân quyền",
                        required_permission: [PermissionEnum.GRANT_PERMISSION],
                        type: "tab",
                    },
                ],
            },
            {
                path: ROUTING_CONSTANTS.EDIT_STAFF,
                element: <EditStaffPage/>,
                required_permission: [PermissionEnum.UPDATE_STAFF],
                type: "link",
                key: "STAFFS",
            },
            {
                path: ROUTING_CONSTANTS.CREATE_STAFF,
                element: <CreateStaffPage/>,
                required_permission: [PermissionEnum.CREATE_STAFF],
                type: "link",
                key: "STAFFS",
            },
            {
                path: ROUTING_CONSTANTS.CREATE_ROLE,
                element: <CreateRolePage/>,
                required_permission: [PermissionEnum.CREATE_ROLE],
                type: "link",
                key: "ROLE",
            },
            {
                path: ROUTING_CONSTANTS.EDIT_ROLE,
                element: <EditRolePage/>,
                required_permission: [PermissionEnum.UPDATE_ROLE],
                type: "link",
                key: "ROLE",
            },
            // {
            //     path: "/settings",
            //     key: "SETTINGS",
            //     icon: "FaSettingsOutlined",
            //     element: <h1>Under Construction...</h1>,
            //     tab_name: "Cài đặt chung",
            //     required_permission: [],
            //     type: "tab",
            // },

            // * Forum
            {
                path: ROUTING_CONSTANTS.NEW_FORUM_THREAD,
                element: <NewForumPage/>,
                required_permission: [],
                type: "link",
            },
            // * Textbook: Manager page
            {
                path: ROUTING_CONSTANTS.TEXTBOOK_MANAGEMENT,
                element: <TextBookPage/>,
                required_permission: [PermissionEnum.VIEW_TEXTBOOK_CENTRAL],
                type: "tab",
                key: "TEXT_BOOK_FOR_MANAGER",
                icon: "TextBookOutlined",
                tab_name: "Giáo trình",
            },
            {
                path: ROUTING_CONSTANTS.CENTRAL_TEXTBOOK_DETAIL,
                element: <TextbookDetail/>,
                required_permission: [],
                type: "link",
                key: "TEXT_BOOK_FOR_MANAGER",
            },
            // personal information
            {
                path: ROUTING_CONSTANTS.PERSONAL_INFORMATION,
                element: <PersonalInfoPage/>,
                required_permission: [],
                type: "link",
                key: "SETTINGS",
            },
            // * Test Page
            // {
            //     path: ROUTING_CONSTANTS.TEST_PAGE,
            //     key: "TEST",
            //     icon: "TestPageOutlined",
            //     element: <TestPage/>,
            //     tab_name: "Test Page",
            //     required_permission: [],
            //     type: "tab",
            // },

        ],
        type: "link",
    },
    // ? Outside App (Before login)
    // * Create an tenant
    {
        path: ROUTING_CONSTANTS.NEW_TENANT,
        element: <NewTenantPage/>,
        required_permission: [],
        type: "link",
    },
    // * Login and Register
    {
        path: ROUTING_CONSTANTS.LOGIN_PAGE,
        element: <LoginRegisterPage/>,
        required_permission: [],
        type: "link",
    },

    // ? Default Routes for Failed Routes
    {
        path: ROUTING_CONSTANTS.NOT_FOUND,
        element: <NotFoundPage/>,
        required_permission: [],
        type: "link",
    },
    {
        path: ROUTING_CONSTANTS.UNAUTHORIZED,
        element: <UnAuthorizedPage/>,
        required_permission: [],
        type: "link",
    },
    {
        path: "*",
        element: <Navigate to={ROUTING_CONSTANTS.NOT_FOUND} replace={true}/>,
        required_permission: [],
        type: "link",
    },
];

export const HomeNavItems: NavigationItem[] = [
    {
        path: HOST_ROUTING_CONSTANT.ROOT,
        element: <HostLayout/>,
        required_permission: [],
        type: "link",
        children: [
            {
                path: HOST_ROUTING_CONSTANT.HOME_PAGE,
                element: <HomePage/>,
                required_permission: [],
                type: "link",
            },
            {
                path: HOST_ROUTING_CONSTANT.REGISTER,
                element: <NewTenantPage/>,
                required_permission: [],
                type: "link",
            },
            {
                path: HOST_ROUTING_CONSTANT.REGISTER_SUCCESS,
                element: <RegisterSuccessPage/>,
                required_permission: [],
                type: "link",
            },
            {
                path: HOST_ROUTING_CONSTANT.LOGIN,
                element: <LoginPage/>,
                required_permission: [],
                type: "link",
            },
        ],
    },
    {
        path: HOST_ROUTING_CONSTANT.ADMIN,
        element: <DashboardLayout/>,
        required_permission: [],
        type: "link",
        children: [
            // {
            //     path: HOST_ROUTING_CONSTANT.DASHBOARD,
            //     key: "DASHBOARD",
            //     icon: "HomeOutlined",
            //     element: <MainDashboard/>,
            //     tab_name: "Tổng quan",
            //     required_permission: [],
            //     type: "tab",
            // },
            {
                path: HOST_ROUTING_CONSTANT.EXTEND,
                key: "EXTEND",
                icon: "MoneyCollectOutlined",
                element: <ExtendService/>,
                tab_name: "Gia hạn dịch vụ",
                required_permission: [],
                type: "tab",
            },
            {
                path: HOST_ROUTING_CONSTANT.SETTINGS,
                key: "SETTINGS",
                icon: "SettingOutlined",
                element: <Settings/>,
                tab_name: "Cài đặt",
                required_permission: [],
                type: "tab",
            },
            {
                path: HOST_ROUTING_CONSTANT.PERSONALIZED,
                element: <Personalized />,
                required_permission: [],
                type: "link",
            },
        ],
    },
];

export let InAppNavItems: NavigationItem[] = TenantNavItems[0].children!!;
export let DashboardTenantAppNavItems: NavigationItem[] =
    HomeNavItems[1].children!!;
