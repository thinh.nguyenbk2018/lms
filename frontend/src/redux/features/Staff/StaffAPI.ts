import {apiSlice} from "../CentralAPI";
import {BaseResponse, PaginatedResponse} from "../../interfaces/types";
import {SearchCriteria} from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {transformSearchQuery} from "@redux/features/APIUtils/TransformParameter";

const staffApiInvalidatesTags = apiSlice.enhanceEndpoints({
  addTagTypes: ["Staffs"],
});

export interface Staff {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  avatar: string;
  description: string;
}


type StaffsResponse = PaginatedResponse<Staff>;

export interface GetStaffDetailsResponse extends BaseResponse {
  staff: Staff;
}

export type UpdateStaffRequest = Pick<
  Staff,
  "id" | "firstName" | "lastName" | "email" | "phone"
>;

export interface UpdateStaffResponse extends BaseResponse {
  staffId: number;
}

export type CreateStaffRequest = Omit<UpdateStaffRequest, "id">;

export type CreateStaffResponse = UpdateStaffResponse;

export interface AllocateAccountResquest {
  staffId: number;
  username: string;
  password: string;
}

export interface AllocateAccountResponse {
  staffId: number;
}

export type DeleteAccountResponse = AllocateAccountResponse;

export interface GetStaffsHaveAccountResponse {
  staffs: Pick<Staff, "id" | "firstName" | "lastName">[];
}

export interface DeleteStaffResponse extends BaseResponse {
  staffId: number
}

const staffAPI = staffApiInvalidatesTags.injectEndpoints({
  endpoints: (build) => ({
    getAllStaffs: build.query<StaffsResponse, SearchCriteria>({
      query: (args) => `/staffs/?${transformSearchQuery(args)}`,
      providesTags: ["Staffs"],
    }),
    getStaffDetails: build.query<GetStaffDetailsResponse, number>({
      query: (staffId) => `/staffs/${staffId}`,
      providesTags: (_, __, staffId) => [{ type: "Staffs", id: staffId }],
    }),
    updateStaff: build.mutation<UpdateStaffResponse, UpdateStaffRequest>({
      query: (staffInfo) => ({
        url: "/staffs",
        method: "PUT",
        body: staffInfo,
      }),
      invalidatesTags: (result) =>
        result?.staffId
          ? ["Staffs", { type: "Staffs", id: result.staffId }]
          : [],
    }),
    createStaff: build.mutation<CreateStaffResponse, CreateStaffRequest>({
      query: (staffInfo) => ({
        url: "/staffs",
        method: "POST",
        body: staffInfo,
      }),
      invalidatesTags: ["Staffs"],
    }),
    allocateAccount: build.mutation<
      AllocateAccountResponse,
      AllocateAccountResquest
    >({
      query: (body) => ({
        url: "/staffs/allocateAccount",
        method: "POST",
        body,
      }),
      invalidatesTags: (result) =>
        result?.staffId
          ? ["Staffs", { type: "Staffs", id: result.staffId }]
          : [],
    }),
    deleteAccount: build.mutation<DeleteAccountResponse, number>({
      query: (staffId) => ({
        url: `/staffs/${staffId}/deleteAccount`,
        method: "POST",
      }),
      invalidatesTags: (result) =>
        result?.staffId
          ? ["Staffs", { type: "Staffs", id: result.staffId }]
          : [],
    }),
    getStaffsHaveAccount: build.query<GetStaffsHaveAccountResponse, void>({
      query: () => "/staffsHaveAccount",
      providesTags: ["Staffs"],
    }),
    deleteStaff: build.mutation<DeleteStaffResponse, number>({
      query: (staffId) => ({
        method: "DELETE",
        url: `/staffs/${staffId}`
      }),
      invalidatesTags: ["Staffs"]
    }),
    refetchStaffs: build.mutation<any,  void>({
			queryFn: () => ({ data: null }),
			invalidatesTags: ["Staffs"]
		})
  }),
  overrideExisting: false,
});

export const {
  useGetAllStaffsQuery,
  useGetStaffDetailsQuery,
  useUpdateStaffMutation,
  useCreateStaffMutation,
  useAllocateAccountMutation,
  useDeleteAccountMutation,
  useGetStaffsHaveAccountQuery,
  useDeleteStaffMutation,
  useRefetchStaffsMutation
} = staffAPI;
