import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { authService } from "./AuthService";
import {
    error,
    generateStatus,
    loading,
    success,
} from "../../../utils/ReduxHelper/Generators";
import { AsyncStatus, ID } from "../../interfaces/types";
import { RootState } from "../../store/store";
import LOCAL_STORAGE_KEYS from "../../../constants/LocalStorageKey";
import antdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
// import * as Sentry from "@sentry/browser";
import * as Sentry from "@sentry/react";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import { isApiError } from "../CentralAPI";
import API_ERRORS from "../ApiErrors";
import ERROR_MESSAGES from "../ERROR_MESSAGES";
import PermissionMap, { PermissionEnum } from "@typescript/interfaces/generated/PermissionMap";
import { DEFAULT_LIMIT_PERMISSION } from "@typescript/interfaces/generated/LimitPermission";

export interface UserAuth {
    accountID: string;
    username: string;
    token: string;
    status: "LOGGED_IN" | "LOADING" | "FAILED";
}

export type AccountType = "STAFF" | "STUDENT";
export interface User {
    id: ID;
    username: string;
    firstName: string;
    lastName: string;
    avatar: string;
    email?: string;
    phone?: string;
    // * Add field này vào
    calendarId?: string;
    accountType: AccountType;
}
export type UserInfoForClickNavigation = Pick<
    User,
    "id" | "avatar" | "firstName" | "username" | "lastName"
>;

export interface UserSlice {
    data: LoginPayload;
    token: string; // * Token is only available when the login status is success
    loginStatus: AsyncStatus;
    registerStatus: AsyncStatus;
}

const samplePayload = {
    code: 1,
    message: "Success",
    payload: null,
    token: "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1NjQ5ZGFhMy05OTA0LTRiZTYtOGJhMy1lMzA2OTUwMjQ3NjEiLCJzdWIiOiJhc2RmYXNkZmFzc3MiLCJ1c2VybmFtZSI6ImFzZGZhc2RmYXNzcyIsInRlbmFudElkIjoidGVuYW50MSIsImlhdCI6MTY0NTM2OTA1NCwiZXhwIjoxNjQ1MzcwODU0fQ.lAz6E6oVTYdDMbowsnLAdZFqO0btgiCmWYvQfFcu_5lFQV4Vg0QJYNWb1r-3z9d8o1k-BtdvNzVg3dOICyxdUVM_XSwQdDhSt2uMGltXowmi7XTL1s_Hmo8zd1Sm5YrO-JOiE8ZjaGlGIhZraIQSHyeNhNEbIygjpWSssPH_ZcQ8e0jcfdabqf2nDD88KlLtS-wmqxiWxOXW8wPEwt6-WTm5AChOEjYIZYe6RbcOBNi3YyK0P7cSUk03djUqy2113qYZoKzwdWVfMKCmq_lajV9jZiSkTm7YusvLD-DE5LfQpnmXVMnn77NK3t9hgyHSg5GLx2D32bD84iGgZbStaA",
    refreshToken:
        "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIzOThlMWU0Ni1hYjk5LTRkN2EtODAwNy04NWM4OTE2YzBhNTkiLCJzdWIiOiJhc2RmYXNkZmFzc3MiLCJ1c2VybmFtZSI6ImFzZGZhc2RmYXNzcyIsImlhdCI6MTY0NTM2OTA1NCwiZXhwIjoxNjQ1OTczODU0fQ.IARDdaVP_tj-NlOnPnhQ06x8sMI8aDct55ko7HE6yIpzxLzsBANOTmpw_W0j7vSSB0JguueAfE83OG2hy-FNN1nO3-9mealiTK1Lk_J6pFsyht7AOEi4w_y7QE6SkTcqyGuvtNUW7i89I5VAfVTSmGpdT7ojmmQtvCa6XwCpqjVUByK2smJuzWbGIhe7GvS4N_KBUkkbfhlAaEXV_KiZJL4rwTZMZ1lOGJPABsrbx_dFXokiuosgBSQlMeuBHA61z9O0imTyQ3E-lOXoH6BIlR6g0QIavRVhrC0a4AOwPc4pSJuH5jZWILZuyzVu4KzPma7QOC1xmTNNiq-bOztefg",
    tenant: "",
    user: {
        id: 2,
        username: "asdfasdfasss",
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        avatar: "",
        calendarId: "",
        accountType: "STUDENT",
    },
    permissions: Object.fromEntries(Object.keys(PermissionEnum).map(key => [key, DEFAULT_LIMIT_PERMISSION])),
};

// * Usage enter frontend without running the backend for development
const mockUserPayload = {
    code: 1,
    message: "Success",
    payload: null,
    token: "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1NjQ5ZGFhMy05OTA0LTRiZTYtOGJhMy1lMzA2OTUwMjQ3NjEiLCJzdWIiOiJhc2RmYXNkZmFzc3MiLCJ1c2VybmFtZSI6ImFzZGZhc2RmYXNzcyIsInRlbmFudElkIjoidGVuYW50MSIsImlhdCI6MTY0NTM2OTA1NCwiZXhwIjoxNjQ1MzcwODU0fQ.lAz6E6oVTYdDMbowsnLAdZFqO0btgiCmWYvQfFcu_5lFQV4Vg0QJYNWb1r-3z9d8o1k-BtdvNzVg3dOICyxdUVM_XSwQdDhSt2uMGltXowmi7XTL1s_Hmo8zd1Sm5YrO-JOiE8ZjaGlGIhZraIQSHyeNhNEbIygjpWSssPH_ZcQ8e0jcfdabqf2nDD88KlLtS-wmqxiWxOXW8wPEwt6-WTm5AChOEjYIZYe6RbcOBNi3YyK0P7cSUk03djUqy2113qYZoKzwdWVfMKCmq_lajV9jZiSkTm7YusvLD-DE5LfQpnmXVMnn77NK3t9hgyHSg5GLx2D32bD84iGgZbStaA",
    refreshToken:
        "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIzOThlMWU0Ni1hYjk5LTRkN2EtODAwNy04NWM4OTE2YzBhNTkiLCJzdWIiOiJhc2RmYXNkZmFzc3MiLCJ1c2VybmFtZSI6ImFzZGZhc2RmYXNzcyIsImlhdCI6MTY0NTM2OTA1NCwiZXhwIjoxNjQ1OTczODU0fQ.IARDdaVP_tj-NlOnPnhQ06x8sMI8aDct55ko7HE6yIpzxLzsBANOTmpw_W0j7vSSB0JguueAfE83OG2hy-FNN1nO3-9mealiTK1Lk_J6pFsyht7AOEi4w_y7QE6SkTcqyGuvtNUW7i89I5VAfVTSmGpdT7ojmmQtvCa6XwCpqjVUByK2smJuzWbGIhe7GvS4N_KBUkkbfhlAaEXV_KiZJL4rwTZMZ1lOGJPABsrbx_dFXokiuosgBSQlMeuBHA61z9O0imTyQ3E-lOXoH6BIlR6g0QIavRVhrC0a4AOwPc4pSJuH5jZWILZuyzVu4KzPma7QOC1xmTNNiq-bOztefg",
    user: {
        id: 2,
        username: "ThinhTestAccount",
        firstName: "",
        lastName: "",
        email: "thinh@gmail.com",
        phone: "0886673468",
        avatar: "https://thumbs.dreamstime.com/b/cartoon-laughing-funny-zombie-face-vector-monster-square-avatar-101864273.jpg",
        calendarId: "someweirdsring",
        tenant: "",
        accountType: "STUDENT",
        permissions: {},
    },
};

const initialState: UserSlice = {
    data: samplePayload,
    token: "",
    loginStatus: generateStatus(),
    registerStatus: generateStatus(),
};

export interface RegisterInfo {
    username: string;
    email: string;
    password: string;
    rePassword: string;
    avatar: string;
    phone: string;
}

// export type UserRoleOption = "INSIDER" | "STUDENT";
export type LoginInfo = Pick<RegisterInfo, "username" | "password"> & {
    // role: UserRoleOption;
};

export type LoginPayload = Partial<typeof samplePayload>;
export type LoginResponse = {
    id: number;
    username: string | null | undefined;
    firstName: string | null | undefined;
    lastName: string | null | undefined;
    email: string | null | undefined;
    phone: string | null | undefined;
    avatar: string | null | undefined;
    calendarId: string | null | undefined;
    permissions?: {};
    // role: UserRoleOption;
};

export const registerNewAccount = createAsyncThunk<RegisterInfo, RegisterInfo>(
    "auth/register",
    async (registerInfo, thunkAPI) => {
        try {
            const response = await authService.register(registerInfo);
            return response;
        } catch (err) {
            return thunkAPI.rejectWithValue(err);
        }
    }
);

export const loginUser = createAsyncThunk<LoginPayload, LoginInfo>(
    "auth/login",
    async (loginInfo, thunkAPI) => {
        try {
            const response = await authService.login(loginInfo);
            return response;
        } catch (err) {
            console.log("Error from login thunk", err);
            return thunkAPI.rejectWithValue(err);
        }
    }
);
// * Special handling for Network error of redux using thunk
const NETWORK_ERROR_MESSAGE = "Network Error";
function isNetworkErrorPayload(err: any): boolean {
    if (!err) return false;
    return err.message === NETWORK_ERROR_MESSAGE;
}

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        // Reducer comes here
        setUserFromLocalStorage(state, action: PayloadAction<LoginPayload>) {
            state.data = action.payload;
            state.loginStatus = success();
        },
        cleanUpOnSignOut(state) {
            state.data = samplePayload;
            Sentry.configureScope((scope) => scope.setUser(null));
            state.loginStatus = loading();
        },
        loginMock(state) {
            state.data = mockUserPayload;
            localStorage.setItem(
                LOCAL_STORAGE_KEYS.TOKEN,
                mockUserPayload.token!
            );
            localStorage.setItem(
                LOCAL_STORAGE_KEYS.USER_AUTH,
                JSON.stringify(mockUserPayload)
            );
            state.loginStatus = success();
        },
        tokenUpdated(
            _state,
            action: PayloadAction<{ token: string; refreshToken: string }>
        ) {
            const { payload } = action;
            localStorage.setItem(LOCAL_STORAGE_KEYS.TOKEN, payload.token);
            localStorage.setItem(
                LOCAL_STORAGE_KEYS.REFRESH_TOKEN,
                payload.refreshToken
            );
        },
    },
    extraReducers: (builder) => {
        builder.addCase(registerNewAccount.fulfilled, (state) => {
            state.registerStatus = success();
        });
        builder.addCase(registerNewAccount.pending, (state) => {
            state.registerStatus = loading();
        });
        builder.addCase(registerNewAccount.rejected, (state, action) => {
            const { payload } = action;
            let messageToAnnounce = "";
            if (isApiError(payload)) {
                const { code } = payload.response.data;
                switch (code) {
                    case API_ERRORS.LOGIN_REGISTER.USERNAME_EXISTED:
                        messageToAnnounce = "Tên người dùng đã tồn tại";
                        break;
                    case API_ERRORS.LOGIN_REGISTER.USER_NOT_FOUND:
                        messageToAnnounce = "Không tìm thấy người dùng";
												break;
										case API_ERRORS.LOGIN_REGISTER.USERNAME_OR_PASSWORD_INCORRECT:
												messageToAnnounce = "Tên tài khoản hoặc mật khẩu không chính xác";
												break;
                    default:
                        messageToAnnounce = "Đã có lỗi khi đăng ký người dùng";
                        break;
                }
                AntdNotifier.apiError(messageToAnnounce);
            }
            state.registerStatus = error();
        });
        builder.addCase(loginUser.fulfilled, (state, { payload }) => {
            state.data = payload;
            localStorage.setItem(LOCAL_STORAGE_KEYS.TOKEN, payload.token!);
            localStorage.setItem(
                LOCAL_STORAGE_KEYS.USER_ID,
                `${payload.user?.id}`
            );
            localStorage.setItem(
                LOCAL_STORAGE_KEYS.USER_AUTH,
                JSON.stringify(payload)
            );
            localStorage.setItem(LOCAL_STORAGE_KEYS.TENANT, payload.tenant!);

            if (!payload.user?.accountType) {
                console.warn(
                    "BRO, PLEASE MAKE SURE ACCOUNT TYPE IS RETURNED INSIDE USER OF LOGIN PAYLOAD __ THINH"
                );
            }
            localStorage.setItem(
                LOCAL_STORAGE_KEYS.ACCOUNT_TYPE,
                payload.user?.accountType!
            );

            // * Auto refressh token
            if (!payload.refreshToken) {
                antdNotifier.error("Missing refresh token");
                state.loginStatus = error();
            } else {
                localStorage.setItem(
                    LOCAL_STORAGE_KEYS.REFRESH_TOKEN,
                    payload.refreshToken
                );
                const { user } = payload;
                // * Set the user info for sentry debugging:
                Sentry.setUser({
                    email: user?.email || "NO_EMAIL_SET",
                    username: user?.username,
                    id: `${user?.id}`,
                });

                state.loginStatus = success();
            }
        });
        builder.addCase(loginUser.pending, (state) => {
            state.loginStatus = loading();
        });
        builder.addCase(loginUser.rejected, (state, action) => {
            const { payload } = action;
            let messageToAnnounce = "";
            console.log("INSIDE LOGIN_ERROR:", payload);
            if (isNetworkErrorPayload(payload)) {
                AntdNotifier.apiError(ERROR_MESSAGES.API.GENERAL.DEFAULT);
            }
            if (isApiError(payload)) {
                const { code } = payload.response.data;
                switch (code) {
                    case API_ERRORS.LOGIN_REGISTER.PASSWORD_INCORRECT:
                        messageToAnnounce = "Mật khẩu không đúng";
                        break;
                    case API_ERRORS.LOGIN_REGISTER.USERNAME_EMPTY:
                        messageToAnnounce = "Không được để trống người dùng";
                        break;
                    case API_ERRORS.LOGIN_REGISTER.USER_NOT_FOUND:
												messageToAnnounce = "Không tìm thấy người dùng";
                        break;
										case API_ERRORS.LOGIN_REGISTER.USERNAME_OR_PASSWORD_INCORRECT:
												messageToAnnounce = "Tên tài khoản hoặc mật khẩu không chính xác";
                        break;
                    default:
                        messageToAnnounce =
                            "Đã có lỗi khi đăng nhập người dùng";
                        break;
                }
                AntdNotifier.apiError(messageToAnnounce);
            }
            state.loginStatus = error(action.error);
        });
    },
});

export const authSelectors = {
    selectCurrentUser: (state: RootState) => state.auth.data.user,
    selectCurrentUserAccountType: (state: RootState) => state.auth.data.user?.accountType,
    selectUserToken: (state: RootState) => state.auth.data.token,
    selectUserCalendarId: (state: RootState) =>
        state.auth.data.user?.calendarId,

    selectLoginStatus: (state: RootState) => state.auth.loginStatus,
    selectLoginSuccess: (state: RootState) => state.auth.loginStatus.isSuccess,
    selectLoginError: (state: RootState) => state.auth.loginStatus.isError,
    selectLoginPending: (state: RootState) => state.auth.loginStatus.isLoading,

    selectRegisterStatus: (state: RootState) => state.auth.registerStatus,
    selectRegisterSuccess: (state: RootState) =>
        state.auth.registerStatus.isSuccess,
    selectRegisterError: (state: RootState) =>
        state.auth.registerStatus.isError,
    selectRegisterPending: (state: RootState) =>
        state.auth.registerStatus.isLoading,
    selectUserPermissions: (state: RootState) => state.auth.data.permissions,
};

export const authActions = authSlice.actions;
