"use strict";
exports.__esModule = true;
var axios_1 = require("axios");
var lodash_1 = require("lodash");
var query_string_1 = require("query-string");
var react_1 = require("react");
var ApiConfigs_1 = require("../../constants/ApiConfigs");
var transformCriteriaToParam = function (criteria) {
    console.log("PARAMS IS: ", query_string_1["default"].stringify(criteria));
    return "";
};
/**
 *
 * @param endpointToSearch - String prefixed with base url
 * @param searchCriteria - filter, sort
 * @param interval - debounce timer
 * @param minLengthToTriggerSearch - request only fire if length bigger than this value
 * @param debug - Log out additional info (default is false)
 * @returns [inputVal,setInputVal, results] - results - List of data found returned from the  server
 * @example
 * const [inputVal,setInputVal, result] = useDebounceSeach<User[]>("/users");
 * * Attach the input
 * <input type="text" value={inputVal} onChange={e => setInputVal(e.target.value)}>
 * * Render the result
 * <List>
 *      {
 *          result.map(item => <UserItem user={item})
 *      }
 * </List>
 */
var useDebounceAndPaginatedSearch = function (endpointToSearch, searchCriteria, callback, interval, minLengthToTriggerSearch, debug) {
    if (interval === void 0) { interval = 650; }
    if (minLengthToTriggerSearch === void 0) { minLengthToTriggerSearch = 1; }
    if (debug === void 0) { debug = false; }
    // * States to populate an input
    var _a = react_1.useState(''), searchText = _a[0], setSearchText = _a[1];
    var _b = react_1.useState(), results = _b[0], setSearchResult = _b[1];
    // .replace(/([^:]\/)\/+/g, "$1");
    var debounceFn = react_1.useCallback(lodash_1["default"].debounce(function (inputValue) {
        // setVal(inputValue);
        if (inputValue.length > minLengthToTriggerSearch) {
            console.log("Axios Called");
            var endpoint = endpointToSearch[0] === '/' ? endpointToSearch.substring(1) : endpointToSearch;
            var URL = ApiConfigs_1.API_CONSTANT.BASE_URL + "/" + endpoint + "?" + transformCriteriaToParam(searchCriteria);
            if (debug) {
                console.log("Searching the URL:", URL);
            }
            axios_1["default"].request({ url: URL, method: "GET" })
                .then(function (res) {
                if (debug)
                    console.log('Data receive', res.data);
                setSearchResult(res.data);
            })["catch"](function (err) { return console.log(err); });
        }
    }, interval), []);
    var setSearchTextHandler = function (e) {
        var value = e.target.value;
        setSearchText(value);
        debounceFn(value);
        if (callback) {
            callback();
        }
    };
    return [
        searchText,
        setSearchTextHandler,
        results,
    ];
};
exports["default"] = useDebounceAndPaginatedSearch;
