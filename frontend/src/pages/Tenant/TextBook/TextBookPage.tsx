import {Button, Input, List, Modal, Skeleton, Space, Typography} from "antd";
import React, {useEffect, useState} from "react";
import AddButton from "../../../components/ActionButton/AddButton";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import TextBookList from "../../../components/TextBook/TextBookList";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {errorArrayHandlingHelper} from "@redux/features/CentralAPI";
import {changeCriteria, createTextBook, textbookSelector} from "@redux/features/Textbook/TextbookSlice";
import API_ERRORS from '../../../redux/features/ApiErrors';
import {TextBook} from "@redux/features/Textbook/TextbookType";
import {useParams} from "react-router-dom";
import {useAddTextbooksToCourseMutation, useLazyGetTextbooksNotInCourseQuery} from "@redux/features/Course/CourseAPI";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import useCourseSidebar from "@hooks/Routing/useCourseSidebar";
import TableSearch from "@components/Search/TableSearch";
import {faBook} from "@fortawesome/free-solid-svg-icons";
import {blue} from "@ant-design/colors";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


export const textBookErrorsHandler = (error: any) => {
    const {TEXTBOOK_NOT_FOUND} = API_ERRORS.TEXT_BOOK;
    errorArrayHandlingHelper(error, [TEXTBOOK_NOT_FOUND, "Không tìm thấy giáo trình"])
    return;
}
const TextbookPage = () => {

    const [closeSidebar] = useClassSidebar();

    const {
        forceCloseSidebar
    } = useCourseSidebar();

    const dispatch = useAppDispatch();
    const [showModal, setShowModal] = useState<boolean>(false);
    const [selectedTextbooks, setSelectedTextbooks] = useState<TextBook[]>([]);
    const {classId, courseId} = useParams();

    const searchCriteria = useAppSelector(textbookSelector.selectSearchCriteria);

    const [
        getTextbooksNotInCourse,
        {data: textbooksNotInCourse, isLoading: isTextbooksNotInCourseLoading},
    ] = useLazyGetTextbooksNotInCourseQuery();

    const [addTextbooksToCourse] = useAddTextbooksToCourseMutation();

    let timeout: any;
    const searchTextbooks = (keyword: string) => {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            if (keyword && courseId) {
                getTextbooksNotInCourse({keyword: keyword, courseId: Number(courseId)});
            }
        }, 300);
    };

    useEffect(() => {
        if (showModal) {
            getTextbooksNotInCourse({keyword: "", courseId: Number(courseId)});
        }
    }, [showModal]);


    const addTextbooks = () => {
        if (courseId != undefined && selectedTextbooks) {
            addTextbooksToCourse({courseId: Number(courseId), idList: selectedTextbooks.map(s => s.id)})
                .unwrap()
                .then(res => {
                    antdNotifier.success("Thêm giáo trình vào khóa học thành công");
                    setShowModal(false);
                })
                .catch(err => antdNotifier.error("Thêm giáo trình vào khóa học thất bại"));
        }
    }

    return (
        <>
            <ContentHeader
                title="Giáo trình"
                action={
                    <Space>
                        {courseId == undefined && classId == undefined && <TableSearch
                            searchFields={[{ title: 'Từ khóa', key: 'key' }]}
                            defaultFilters={[]}
                            searchCriteria={searchCriteria}
                            changeCriteria={changeCriteria}
                        />}
                        <AddButton
                            style={{display: classId != undefined ? 'none' : ''}}
                            type="primary"
                            onClick={() => {
                                if (courseId) {
                                    setShowModal(true);
                                } else {
                                    dispatch(createTextBook());
                                }
                            }}
                        />
                    </Space>
                }
            />
            <div>
                <TextBookList/>
            </div>

            <Modal
                maskClosable={false}
                title="Thêm giáo trình vào khóa học"
                visible={showModal}
                onCancel={() => {
                    setShowModal(false);
                    setSelectedTextbooks([]);
                }}
                onOk={addTextbooks}
                okText="Thêm tài liệu"
                cancelText="Hủy"
                width={800}
                destroyOnClose={true}
                closable
                okButtonProps={{disabled: selectedTextbooks.length == 0}}
            >
                <Input.Search
                    placeholder="Tìm giáo trình"
                    size="large"
                    style={{marginBottom: 15}}
                    onChange={({currentTarget: {value}}) =>
                        searchTextbooks(value)
                    }
                />
                <div
                    style={{
                        overflowY: "auto",
                        height: 225,
                        padding: 2,
                        border: "1px solid #d9d9d9",
                    }}
                >
                    <List
                        locale={{emptyText: 'Không có dữ liệu'}}
                        header={null}
                        footer={null}
                        dataSource={textbooksNotInCourse?.filter(
                            (s: TextBook) => !selectedTextbooks.map((st: TextBook) => st.id).includes(s.id)
                        )}
                        bordered={
                            !!textbooksNotInCourse?.filter(
                                (s: TextBook) =>
                                    !selectedTextbooks.map((st) => st.id).includes(s.id)
                            ).length
                        }
                        renderItem={(textbook: TextBook) =>
                            selectedTextbooks
                                .map((s) => s.id)
                                .includes(textbook?.id) || (
                                <Skeleton
                                    avatar
                                    title={false}
                                    loading={isTextbooksNotInCourseLoading}
                                    active
                                >
                                    <List.Item
                                        extra={
                                            <Button
                                                type="primary"
                                                onClick={() =>
                                                    setSelectedTextbooks((prev) => [
                                                        ...prev,
                                                        textbook,
                                                    ])
                                                }
                                                className={'button-with-icon'}
                                            >
                                                Thêm
                                            </Button>
                                        }
                                    >
                                        <List.Item.Meta
                                            // avatar={<Avatar src={textbook.avatar}/>}
                                            title={<><FontAwesomeIcon
                                                icon={faBook}
                                                style={{margin: "0 .5em 0 0", color: blue.primary}}
                                            />{textbook.name}</>}
                                            // description={`${student.email} - ${student.phone}`}
                                        />
                                    </List.Item>
                                </Skeleton>
                            )
                        }
                    />
                </div>
                <Typography.Title level={5} style={{marginTop: 10}}>
                    Danh sách thêm
                </Typography.Title>
                <div
                    style={{
                        overflowY: "auto",
                        height: 225,
                        padding: 2,
                        border: "1px solid #d9d9d9",
                    }}
                >
                    <List
                        locale={{emptyText: 'Không có dữ liệu'}}
                        header={null}
                        footer={null}
                        dataSource={selectedTextbooks}
                        bordered={selectedTextbooks.length != 0}
                        renderItem={(textbook: TextBook) => (
                            <Skeleton avatar title={false} loading={isTextbooksNotInCourseLoading} active>
                                <List.Item
                                    extra={
                                        <Button
                                            type="primary"
                                            danger
                                            onClick={() =>
                                                setSelectedTextbooks((prev) =>
                                                    prev.filter((s) => s.id != textbook.id)
                                                )
                                            }
                                        >
                                            Xóa
                                        </Button>
                                    }
                                >
                                    <List.Item.Meta
                                        // avatar={<Avatar src={student.avatar}/>}
                                        title={<><FontAwesomeIcon
                                            icon={faBook}
                                            style={{margin: "0 .5em 0 0", color: blue.primary}}
                                        />{textbook.name}</>}
                                        // description={`${student.email} - ${student.phone}`}
                                    />
                                </List.Item>
                            </Skeleton>
                        )}
                    />
                </div>
            </Modal>
        </>
    );
};

export default React.memo(TextbookPage);
