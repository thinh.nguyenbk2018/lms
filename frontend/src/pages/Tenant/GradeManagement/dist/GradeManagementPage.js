"use strict";
exports.__esModule = true;
var antd_1 = require("antd");
var react_1 = require("react");
var react_2 = require("react");
var react_router_dom_1 = require("react-router-dom");
var ContentHeader_1 = require("../../../components/ContentHeader/ContentHeader");
var GradeManagement_1 = require("../../../components/GradeManagement/GradeManagement");
var ROUTING_CONSTANTS_1 = require("../../../navigation/ROUTING_CONSTANTS");
var GradeApi_1 = require("../../../redux/features/Grade/GradeApi");
var GradeManagementPage = function (_a) {
    var scope = _a.scope;
    var _b = react_router_dom_1.useParams(), classId = _b.classId, courseId = _b.courseId;
    var _c = GradeApi_1.useLazyGetAllGradeTagsDetailsQuery(), getAllGradeTagsDetails = _c[0], _d = _c[1], getAllGradeTagsDetailsResponse = _d.data, isLoading = _d.isLoading;
    react_2.useEffect(function () {
        getAllGradeTagsDetails({ scope: scope, scopeId: classId ? parseInt(classId) : parseInt(courseId) });
    }, [classId, courseId, scope, getAllGradeTagsDetails]);
    return react_1["default"].createElement(react_1["default"].Fragment, null,
        react_1["default"].createElement(ContentHeader_1["default"], { title: "Qu\u1EA3n l\u00FD \u0111i\u1EC3m", action: react_1["default"].createElement(antd_1.Space, null,
                react_1["default"].createElement(react_router_dom_1.Link, { to: scope === "CLASS" ? ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.VIEW_GRADE_IN_CLASS.replace(":classId", classId) : ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.VIEW_GRADE_IN_COURSE.replace(":courseId", courseId) },
                    react_1["default"].createElement(antd_1.Button, { type: "ghost" }, "Xem \u0111i\u1EC3m")),
                react_1["default"].createElement(react_router_dom_1.Link, { to: scope === "CLASS" ? ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.CREATE_GRADE_FORMULA_IN_CLASS.replace(":classId", classId) : ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.CREATE_GRADE_FORMULA_IN_COURSE.replace(":courseId", courseId) },
                    react_1["default"].createElement(antd_1.Button, { type: "primary" }, "Th\u00EAm"))) }),
        react_1["default"].createElement(GradeManagement_1["default"], { scope: scope, scopeId: parseInt(classId || courseId), tags: (getAllGradeTagsDetailsResponse === null || getAllGradeTagsDetailsResponse === void 0 ? void 0 : getAllGradeTagsDetailsResponse.tags) || [], isLoading: isLoading }));
};
exports["default"] = GradeManagementPage;
