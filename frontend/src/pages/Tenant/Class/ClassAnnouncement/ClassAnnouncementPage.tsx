import {toNumber} from "lodash";
import React from "react";
import {useNavigate, useParams} from "react-router-dom";
import AddButton from "../../../../components/ActionButton/AddButton";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import AnnouncementList from "../../../../components/Notification/AnnouncementList";
import {useAppDispatch, useAppSelector} from "../../../../hooks/Redux/hooks";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {authSelectors} from "../../../../redux/features/Auth/AuthSlice";
import {announcementSelector, changeCriteria,} from "@redux/features/Announcement/AnnouncementSlice";
import {SearchCriteria,} from "../../../../redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import "./ClassAnnouncementPage.less";
import useDebounced from "../../../../hooks/Debounced/useDebounced";
import {useGetReceivedAnnouncementsQuery} from "@redux/features/Announcement/AnnouncementAPI";
import TableSearch from "../../../../components/Search/TableSearch";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";

const ClassAnnouncementPage = () => {
    const searchCriteria = useAppSelector(
        announcementSelector.selectSearchCriteria
    );
    const {classId} = useParams();
    const currentUser = useAppSelector(authSelectors.selectCurrentUser);

    const [closeSidebar] = useClassSidebar();
    const { classInfo } = useClassCourseInformation(Number(classId));
    const navigate = useNavigate();

    const [word, setWord] = React.useState("");
    const dispatch = useAppDispatch();
    const debouncedCriteriaChange = useDebounced((criteria: SearchCriteria) =>
        dispatch(changeCriteria(criteria))
    );
    const {
        data: notificationList,
        isFetching,
        isSuccess,
        isError,
        refetch,
    } = useGetReceivedAnnouncementsQuery(
        {
            ...searchCriteria,
            classId: toNumber(classId),
            receiverId: currentUser?.id,
        },
        {skip: toNumber(classId) === -1 || currentUser == undefined}
    );

    const onRead = () => {
        // TODO: Send backend --> Mark notification as read
        const a = 2;
    };

    return (
        <>
            <ContentHeader
                title={"Thông báo"}
                action={
                    <>
                        <div className={"search-container"}>
                            <TableSearch
                                searchFields={[{title: "Từ khóa", key: "key"}]}
                                defaultFilters={[]}
                                changeCriteria={changeCriteria}
                                searchCriteria={searchCriteria}
                            />
                            {classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.CREATE_ANNOUNCEMENT_CLASS]}>
                                <AddButton
                                    title="Tạo thông báo"
                                    onClick={() =>
                                        navigate(
                                            ROUTING_CONSTANTS.CLASS_DETAIL_ANNOUNCEMENT_CREATE.replace(
                                                ":classId",
                                                classId ? classId : "-1"
                                            )
                                        )
                                    }
                                />
                            </PermissionRenderer>}
                        </div>
                    </>
                }
            />
            <AnnouncementList/>
        </>
    );
};

export default ClassAnnouncementPage;
