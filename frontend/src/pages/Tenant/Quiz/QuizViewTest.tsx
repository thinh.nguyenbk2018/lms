// import {
// 	BlankWithChoicesQuestion,
// 	FillInBlankQuestion,
// 	MultichoiceQuestion,
// 	WrittenQuestion,
// 	FillBlankDragAndDropQuestion,
// } from "../../../redux/features/Quiz/CreateQuizSlice";
// import { User } from "../../../redux/features/Auth/AuthSlice";
// import { DEFAULT_FRONTEND_ID } from "../../../redux/interfaces/types";
// import { Test } from "../../../redux/features/Quiz/examAPI";
//
// export const singleFillInBlankQuestionTest: FillInBlankQuestion = {
// 	id: DEFAULT_FRONTEND_ID,
// 	note: `string`,
// 	point: 4,
// 	order: 2,
// 	type: "FILL_IN_BLANK",
// 	description:
// 		"<p>đây là một câu hỏi điền khuyết:</p><p>Hãy cho biết tên chó của Lan [Luke] và mèo của lan tên là [Garfield]</p>",
// 	blankList: [
// 		{
// 			expectedAnswer: "Luke",
// 			matchStrategy: "EXACT",
// 			isCaseSensitive: true,
// 		},
// 		{
// 			expectedAnswer: "Garfield",
// 			matchStrategy: "CONTAIN",
// 			isCaseSensitive: true,
// 		},
// 	],
// };
//
// export const singleBlankWithChoicesQuestionTest: BlankWithChoicesQuestion = {
// 	blankList: [
// 		{
// 			hint: "no hint",
// 			answerList: [],
// 			correctAnswerKey: 1,
// 		},
// 		{
// 			hint: "no hint",
// 			answerList: [],
// 			correctAnswerKey: 2,
// 		},
// 	],
// 	id: 21,
// 	note: `string`,
// 	point: 4,
// 	order: 2,
// 	type: "FILL_IN_BLANK_WITH_CHOICES",
// 	description:
// 		"<p><strong>Who is the <em>biggest guy</em> [on;in;at] the planet ? [No;Multiple] answer  </strong></p>",
// };
// export const singleWrittenQuestionTest: WrittenQuestion = {
// 	order: 0,
// 	id: 45,
// 	note: `string`,
// 	point: 4,
// 	type: "WRITING",
// 	description:
// 		"<p><strong> Write a paragraph about your last holiday  </strong></p>",
// };
//
// export const singleMultichoiceQuestionTest: MultichoiceQuestion = {
// 	id: 1,
// 	note: `string`,
// 	point: 3,
// 	order: 1,
// 	type: "MULTI_CHOICE",
// 	description:
// 		"<p><strong>Who is the <em>biggest guy</em> on the planet ? Choose the correct answer </strong></p>",
// 	answerList: [
// 		{
// 			content: "option 1",
// 			isCorrect: true,
// 			answerKey: 1,
// 		},
// 		{
// 			content: "option 2",
// 			isCorrect: false,
// 			answerKey: 2,
// 		},
// 	],
// 	isMultipleAnswer: false,
// };
//
// export const singleDragAndDropQuestionTest: FillBlankDragAndDropQuestion = {
// 	id: DEFAULT_FRONTEND_ID,
// 	note: "Keo cac lua chon vao vi tri dung ",
// 	point: 2,
// 	order: 4,
// 	type: "FILL_IN_BLANK_DRAG_AND_DROP",
// 	description: "Drag and drop [] question testing []",
// 	answerList: [
// 		{
// 			content: "option 1",
// 			key: 1,
// 			order: 1,
// 		},
// 		{
// 			content: "option 2",
// 			key: 2,
// 			order: 2,
// 		},
// 		{
// 			content: "option 3",
// 			key: 3,
// 			order: 3,
// 		},
// 		{
// 			content: "option 4",
// 			key: 4,
// 			order: 4,
// 		},
// 		{
// 			content: "option 5",
// 			key: 5,
// 			order: 5,
// 		},
// 	],
// 	blankList: [
// 		{
// 			hint: "guess 1",
// 			answerKey: 1,
// 		},
// 		{
// 			hint: "guess 2",
// 			answerKey: 2,
// 		},
// 	],
// };
//
// export const mockUser: User = {
// 	username: "Thinh",
// 	firstName: "",
// 	lastName: "",
// 	id: 123,
// 	avatar: "http://placehold.it/120x120&text=image4",
// 	accountType: "STUDENT",
// };
//
// export const mockTestData: Test = {
// 	courseId: DEFAULT_FRONTEND_ID,
// 	title: "Mock Test Title",
// 	description: "Mock test description",
// 	questionList: {
// 		page: 1,
// 		perPage: 5,
// 		total: 20,
// 		totalPages: 3,
// 		listData: [
// 			singleFillInBlankQuestionTest,
// 			singleWrittenQuestionTest,
// 			singleMultichoiceQuestionTest,
// 			singleBlankWithChoicesQuestionTest,
// 		],
// 	},
// 	id: "",
// 	createdAt: 0,
// 	createdBy: mockUser,
// 	updatedAt: 0,
// 	updatedBy: mockUser,
// 	state: "PUBLIC",
// };

const a = () => {

}
export default a;