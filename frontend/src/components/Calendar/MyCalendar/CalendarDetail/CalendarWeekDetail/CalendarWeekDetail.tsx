import {Button, Space, Typography} from "antd";
import React, {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../../../../hooks/Redux/hooks";
import {
    calendarActions,
    calendarSelector,
    EventHashMap,
    getFromMap,
    mapHasKey,
    prepareKey,
} from "../../../../../redux/features/Calendar/CalendarSlice";
import {getDaysName, getEventStartTime, getMonthName,} from "../../../Utils/CalendarUtils";
import {
    useGetClassCalendarEventQuery,
    useGetPersonalCalendarQuery,
    useRefetchEventsMutation,
} from "../../../../../redux/features/Calendar/CalendarAPI";
import CustomEvent from "../../../Event/CustomEvent";
import {authSelectors} from "../../../../../redux/features/Auth/AuthSlice";
import "./CalendarWeekDetail.less";
import {LeftOutlined, RightOutlined} from "@ant-design/icons";
import {
    classTimeTableActions,
    classTimeTableSlice,
} from "../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableSlice";
import {useParams} from "react-router-dom";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {dateHours} from "@components/Calendar/MyCalendar/CalendarDetail/CalendarDetail";
import ClassSessionFormModal from "@pages/Tenant/Class/TimeTable/Form/ClassSessionFormModal";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";

const CalendarWeekDetail = () => {
    const selectedDate = useAppSelector(calendarSelector.selectCurrentDate);
    const weekToFocus = useAppSelector(calendarSelector.selectCurrentWeek);
    const dispatch = useAppDispatch();

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);
    const userID = userInfo?.id;

    const {classId} = useParams();

    const {
        classInfo
    } = useClassCourseInformation(Number(classId));

    const {
    	data: eventMap = {},
    	isSuccess,
    	isError,
    	isFetching,
    	error,
    } = useGetPersonalCalendarQuery(userID || -1, {skip: classId != undefined || userID == undefined});

    const [refetchEvents] = useRefetchEventsMutation();

    useEffect(() => {
        refetchEvents();
    }, []);

    const {
        data: classCalendarEventMap = {},

    } = useGetClassCalendarEventQuery(Number(classId), {skip: classId == undefined})
    // const [eventMap, setEventMap] = useState(calendarHashMapTest);

    const getCalendarTitleText: () => string = () => {
        if (!weekToFocus) {
            return "";
        }
        const firstDateOfWeek = weekToFocus[0];
        const lastDateOfWeek = weekToFocus[6];
        if (firstDateOfWeek.getMonth() === lastDateOfWeek.getMonth()) {
            return `${getMonthName(
                firstDateOfWeek
            )}, ngày ${firstDateOfWeek.getDate()} đến ${lastDateOfWeek.getDate()}`;
        } else {
            return `${getMonthName(
                firstDateOfWeek
            )}, ngày ${firstDateOfWeek.getDate()} - ${getMonthName(
                lastDateOfWeek
            )}, ngày ${lastDateOfWeek.getDate()}`;
        }
    };
    // * On add event handlers:
    const onAddClassSchedule = () => {
        dispatch(classTimeTableActions.addNewClassSchedule());
    };
    const onAddClassSession = () => {
        dispatch(classTimeTableActions.addNewClassSession());
    };

    const onAddPersonalEvent = () => {
        dispatch(classTimeTableActions.addNewPersonalEvetn());
    };

    const getSelectedDateClass = (date: Date) => {
        return date.getDate() === selectedDate?.getDate() &&
        date.getMonth() === selectedDate.getMonth()
            ? "selected-date-title"
            : "";
    };

    // * Event [] =>
    const pullEventsAndPaint = (
        dateToRender: Date,
        hourTitle: string,
        eventMap: EventHashMap
    ) => {
        // console.log('event map: ', eventMap);
        // console.log('hour title: ', hourTitle);
        // * Check if the cell has event attached to it
        const key = prepareKey(dateToRender, hourTitle);
        // console.log('key: ', key);
        if (!mapHasKey(eventMap, key)) {
            return null;
        }
        const eventList = getFromMap(eventMap, key)!!;
        // console.log('event list: ', eventList);
        // * Loop through list event and paints them on the calendar cell
        if (eventList.length !== 0) {
            return (
                <>
                    {eventList.map((item, index) => {
                        return (
                            <CustomEvent
                                shareNumber={eventList.length}
                                index={index}
                                eventInfo={item}
                                dateToRender={dateToRender}
                                key={index.toString().concat(getEventStartTime(item).toString())}
                            />
                        );
                    })}
                </>
            );
        }
        return null;
    };

    return (
        <>
            <div className={"calendar-detail-container"}>
                <div className={"calendar-control-header"}>
                    <div className={"calendar-date-name"}>
                        <Typography.Title level={1}>
                            {getCalendarTitleText()}
                        </Typography.Title>
                        <div className={"button-nav-container"}>
                            <Space>
                                <Button
                                    icon={<LeftOutlined/>}
                                    onClick={() => dispatch(calendarActions.decreaseDate())}
                                >
                                    {/*Backward*/}
                                </Button>
                                <Button
                                    icon={<RightOutlined/>}
                                    onClick={() => dispatch(calendarActions.increaseDate())}
                                >
                                    {/*<Space>*/}
                                    {/*    /!*Forward*!/*/}
                                    {/*    <RightOutlined/>*/}
                                    {/*</Space>*/}
                                </Button>
                            </Space>
                        </div>
                    </div>
                    {/*<div className={"calendar-control"}>*/}
                    {/*    <div className={"calendar-mode-container"}>*/}
                    {/*        <Select defaultValue={"week"}>*/}
                    {/*            <Option value={"week"}>Week</Option>*/}
                    {/*            <Option value={"day"}>Day</Option>*/}
                    {/*        </Select>*/}
                    {/*    </div>*/}
                    {/*    <CalendarNewEventFormModal modalProps={{}}/>*/}
                    {/*    <div className={"calendar-add-button"} onClick={() => {*/}
                    {/*    }}>*/}
                    {/*        <Dropdown*/}
                    {/*            overlay={*/}
                    {/*                <AddNewEventMenu*/}
                    {/*                    onSelect={(key) => {*/}
                    {/*                        switch (key) {*/}
                    {/*                            case "CLASS_SESSION":*/}
                    {/*                                onAddClassSession();*/}
                    {/*                                return;*/}
                    {/*                            case "PERSONAL":*/}
                    {/*                                onAddPersonalEvent();*/}
                    {/*                                return;*/}
                    {/*                            case "CLASS_SCHEDULE":*/}
                    {/*                                onAddClassSchedule();*/}
                    {/*                                return;*/}
                    {/*                            default:*/}
                    {/*                                return;*/}
                    {/*                        }*/}
                    {/*                    }}*/}
                    {/*                />*/}
                    {/*            }*/}
                    {/*        >*/}
                    {/*            <Button type={"primary"}>Thêm một sự kiện</Button>*/}
                    {/*        </Dropdown>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {classInfo?.status != 'ENDED' && classId != undefined && <PermissionRenderer permissions={[PermissionEnum.CREATE_CLASS_SESSION]} >
                        <Button
                            style={{marginLeft: "1rem"}}
                            type={"primary"}
                            onClick={() => {
                                dispatch(classTimeTableSlice.actions.addNewClassSession());
                                refetchEvents();
                            }}
                        >
                            Thêm buổi học
                        </Button>
                    </PermissionRenderer>}
                </div>
                <div className={"calendar-detail-date-title-header"}>
                    {weekToFocus.map((date) => (
                        <div
                            className={`calendar-detail-date-info-box ${getSelectedDateClass(
                                date
                            )}`}
                        >
                            <div className={"calendar-detail-date-of-week-number"}>
                                {date.getDate()}
                            </div>
                            <div className={"calendar-detail-date-of-week-name"}>
                                {getDaysName(date)}
                            </div>
                        </div>
                    ))}
                </div>
                <div className={"calendar-detail-main"}>
                    <div className={"date-hour-column"}>
                        {dateHours.map((hourTitle) => {
                            return (
                                <div className={"hour-title"}>
                                    <Typography.Text strong>{hourTitle}</Typography.Text>
                                </div>
                            );
                        })}
                    </div>
                    <div
                        className={"calendar-detail-with-events"}
                        onScroll={(e) => {
                            // console.log("Scroll: ", e);
                        }}
                    >
                        {dateHours.map((hourTitle: any) => {
                            return (
                                <>
                                    {new Array(7).fill(0).map((item, index) => {
                                        return (
                                            <div className={"date-hour-detail"}>
                                                {pullEventsAndPaint(
                                                    weekToFocus[index],
                                                    hourTitle,
                                                    classId == undefined ? eventMap : classCalendarEventMap
                                                )}
                                            </div>
                                        );
                                    })}
                                </>
                            );
                        })}
                    </div>
                </div>
            </div>
            <ClassSessionFormModal />
        </>
    );
};
export default CalendarWeekDetail;
