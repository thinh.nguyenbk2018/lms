import { Form, FormikProps, withFormik } from "formik";
import { FormItem, Input } from "formik-antd";
import { Button } from "antd";
import * as Yup from 'yup';
import React from "react";
interface FormValues {

    email: string;

    password: string;

}



interface OtherProps {

    message: string;

}

const validationSchema = Yup.object().shape(
    {
        email: Yup.string().min(4, "len > 5")
    }
)



// Aside: You may see InjectedFormikProps<OtherProps, FormValues> instead of what comes below in older code.. InjectedFormikProps was artifact of when Formik only exported a HoC. It is also less flexible as it MUST wrap all props (it passes them through).

const InnerForm = (props: OtherProps & FormikProps<FormValues>) => {

    const { message, handleSubmit, isValid, dirty } = props;

    return (

        <Form>

            <h1>{message}</h1>
            <FormItem name={'email'}>
                <Input name={'email'} />
            </FormItem>
            <Button type="primary" danger={true} onClick={() => handleSubmit()} disabled={!dirty || !isValid}>
                Submit
            </Button>
        </Form>

    );

};



// The type of props MyForm receives

interface MyFormProps {

    initialEmail?: string;

    message: string; // if this passed all the way through you might do this or make a union type

}



// Wrap our form with the withFormik HoC

const MyForm = withFormik<MyFormProps, FormValues>({

    // Transform outer props into form values

    mapPropsToValues: props => {

        return {

            email: props.initialEmail || '',

            password: '',

        };

    },

    validationSchema: validationSchema,
    handleSubmit: values => {

        // do submitting things
        // console.log("values: ", values)

    },

})(InnerForm);

export {MyForm}