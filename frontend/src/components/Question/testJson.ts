import { QuestionDataType } from "../../redux/features/Quiz/CreateQuizSlice";

const questionResultTestData = {
	sessionId: "4a34d011-081a-479e-81ec-7efb3ecd16d7",
	user: {
		id: 2,
		username: "lmslms2",
		firstName: "Thịnh",
		lastName: "Nguyễn Hoàng",
		avatar:
			"https://robohash.org/afef74884da855b084ce6e8551cfc245?set=set4&bgset=&size=400x400",
	},
	finalVerdict: null,
	totalScore: null,
	score: 2,
	startedAt: "2022-05-01T10:38:17.706998",
	submittedAt: "2022-05-01T10:42:07.54872",
	timeTaken: 229,
	attempt: null,
	gradedState: null,
	questions: {
		page: 1,
		perPage: 5,
		total: 6,
		totalPages: 2,
		listData: [
			//    ! 0 Fill In BLnka
			{
				id: 2,
				description:
					"He [] a good father. He [] tries his best to earn enough money to support his family. The [] was built with most of its budget was paid by []",
				note: "question_notes",
				point: 2,
				level: "MEDIUM",
				order: 1,
				type: "FILL_IN_BLANK",
				textbookId: 1,
				resultAnswerList: [
					{
						id: 2,
						hint: "this is hint",
						expectedAnswer: "is",
						matchStrategy: "EXACT",
						studentAnswer: null,
						isCorrect: false,
					},
					{
						id: 1,
						hint: "this is hint",
						expectedAnswer: "always",
						matchStrategy: "CONTAIN",
						studentAnswer: null,
						isCorrect: false,
					},
					{
						id: 3,
						hint: "this is hint",
						expectedAnswer: "/([A-Z])\\w+",
						matchStrategy: "REGEX",
						studentAnswer: null,
						isCorrect: false,
					},
				],
				earnedPoint: 0,
			},
			// !  1 Fill Blank with choices
			{
				id: 3,
				description:
					"He [chool;pen;cat;dog] a good father. He [chool;pen;cat;dog] tries his best to earn enough money to support his family. The [chool;pen;cat;dog] was built with most of its budget was paid by [chool;pen;cat;dog]",
				note: "question_notes",
				point: 2,
				level: "MEDIUM",
				order: 2,
				type: "FILL_IN_BLANK_WITH_CHOICES",
				textbookId: 1,
				resultAnswerList: [
					{
						id: 1,
						hint: "this is hint",
						correctAnswerKey: 1,
						answerList: [
							{
								id: 1,
								answerKey: 1,
								content: "school",
								order: 1,
							},
							{
								id: 2,
								answerKey: 2,
								content: "pen",
								order: 2,
							},
							{
								id: 4,
								answerKey: 4,
								content: "dog",
								order: 4,
							},
							{
								id: 3,
								answerKey: 3,
								content: "cat",
								order: 3,
							},
						],
						studentAnswerKey: 0,
						isCorrect: false,
					},
					{
						id: 2,
						hint: "this is hint",
						correctAnswerKey: 2,
						answerList: [
							{
								id: 5,
								answerKey: 1,
								content: "school",
								order: 1,
							},
							{
								id: 6,
								answerKey: 2,
								content: "pen",
								order: 2,
							},
							{
								id: 7,
								answerKey: 3,
								content: "cat",
								order: 3,
							},
							{
								id: 8,
								answerKey: 4,
								content: "dog",
								order: 4,
							},
						],
						studentAnswerKey: 1,
						isCorrect: false,
					},
					{
						id: 3,
						hint: "this is hint",
						correctAnswerKey: 3,
						answerList: [
							{
								id: 12,
								answerKey: 4,
								content: "dog",
								order: 4,
							},
							{
								id: 9,
								answerKey: 1,
								content: "school",
								order: 1,
							},
							{
								id: 10,
								answerKey: 2,
								content: "pen",
								order: 2,
							},
							{
								id: 11,
								answerKey: 3,
								content: "cat",
								order: 3,
							},
						],
						studentAnswerKey: 1,
						isCorrect: false,
					},
					{
						id: 4,
						hint: "this is hint",
						correctAnswerKey: 4,
						answerList: [
							{
								id: 16,
								answerKey: 4,
								content: "dog",
								order: 4,
							},
							{
								id: 13,
								answerKey: 1,
								content: "school",
								order: 1,
							},
							{
								id: 15,
								answerKey: 3,
								content: "cat",
								order: 3,
							},
							{
								id: 14,
								answerKey: 2,
								content: "pen",
								order: 2,
							},
						],
						studentAnswerKey: 1,
						isCorrect: false,
					},
				],
				earnedPoint: 0,
			},
			// ! 2 WRITTING
			{
				id: 5,
				description: "question 1 description",
				note: "question_notes",
				point: 2,
				level: "MEDIUM",
				order: 2,
				type: "WRITING",
				textbookId: 1,
				earnedPoint: null,
				answer: null,
			},
			// !3 Multichoice
			{
				id: 1,
				description: "[Minh;Lan;Can] was the house's breadwinner",
				note: "question_notes",
				point: 2,
				level: "MEDIUM",
				order: 3,
				type: "MULTI_CHOICE",
				textbookId: 1,
				isMultipleAnswer: false,
				resultAnswerList: [
					{
						id: 1,
						content: "Minh",
						answerKey: 1,
						isCorrect: false,
						order: 1,
						isChosenByStudent: false,
					},
					{
						id: 2,
						content: "Lan",
						answerKey: 2,
						isCorrect: true,
						order: 2,
						isChosenByStudent: false,
					},
					{
						id: 3,
						content: "Can",
						answerKey: 3,
						isCorrect: false,
						order: 3,
						isChosenByStudent: false,
					},
				],
				earnedPoint: 2,
			},
			// ! 4 Multichoice
			{
				id: 6,
				description: "[Minh;Lan;Can] was the house's breadwinner",
				note: "question_notes",
				point: 2,
				level: "MEDIUM",
				order: 3,
				type: "MULTI_CHOICE",
				textbookId: 1,
				isMultipleAnswer: false,
				resultAnswerList: [
					{
						id: 6,
						content: "Can",
						answerKey: 3,
						isCorrect: false,
						order: 3,
						isChosenByStudent: false,
					},
					{
						id: 4,
						content: "Minh",
						answerKey: 1,
						isCorrect: false,
						order: 1,
						isChosenByStudent: false,
					},
					{
						id: 5,
						content: "Lan",
						answerKey: 2,
						isCorrect: true,
						order: 2,
						isChosenByStudent: false,
					},
				],
				earnedPoint: 0,
			},

			// !  Drag And Drop
			{
				id: 4,
				description:
					"Human [] contains lots of calcium in compare with human []",
				note: "question_notes",
				point: 2,
				level: "MEDIUM",
				order: 4,
				type: "FILL_IN_BLANK_DRAG_AND_DROP",
				textbookId: 1,
				earnedPoint: 0,

				resultAnswerList:  {
					
				}
			},
		],
	},
};

export default questionResultTestData;
