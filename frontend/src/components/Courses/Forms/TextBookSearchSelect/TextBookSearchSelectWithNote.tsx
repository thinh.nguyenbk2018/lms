import React from "react";
import { Select } from "antd";
import useDebounced from "../../../../hooks/Debounced/useDebounced";
import { ID } from "../../../../redux/interfaces/types";
import { useAppSelector, useAppDispatch } from "../../../../hooks/Redux/hooks";
import { textbookSelector } from "../../../../redux/features/Textbook/TextbookSlice";
import { useGetTextbooksQuery } from "../../../../redux/features/Textbook/TextbookAPI";
import { changeCriteria } from "../../../../redux/features/Textbook/TextbookSlice";

type TextbookSeachSelectProps = {
	onSelect?: (value: ID) => void;
	onChange?: (value: ID) => void;
	onBlur?: Function;
};
const TextbookSearchSelect = ({
	onSelect,
	onChange,
	onBlur,
}: TextbookSeachSelectProps) => {
	// const searchQuery = jk
	const textbookSearchCriteria = useAppSelector(
		textbookSelector.selectSearchCriteria
	);
	const {
		data: paginatedResponse,
		isSuccess,
		isFetching,
	} = useGetTextbooksQuery(textbookSearchCriteria);

	const dispatch = useAppDispatch();

	const debouncedCriteriaChange = useDebounced((criteria) =>
		dispatch(changeCriteria(criteria))
	);

	// @ts-ignore
	return (
		<Select
			showSearch
			style={{ width: 200 }}
			placeholder="Search to Select"
			optionFilterProp="children"
			filterOption={(input, option) => {
				const optionContent = (option?.children || "") as string;
				return optionContent.toLowerCase?.().indexOf(input.toLowerCase()) >= 0;
			}}
			filterSort={(optionA, optionB) => {
				const optionAcontent = (optionA?.children || "") as string;
				const optionBcontent = (optionB?.children || "") as string;
				return optionAcontent
					.toLowerCase()
					.localeCompare(optionBcontent.toLowerCase());
			}}
			onChange={(value, option) => {
				console.log("textbook SEARCH / SELECT", value, option);
				onSelect?.(value);
				onChange?.(value);
				onBlur?.();
			}}
			onSearch={(value) => {
				const searchCriteria = { ...textbookSearchCriteria, keyword: value };
				debouncedCriteriaChange(searchCriteria);
			}}
		>
			{paginatedResponse?.listData.map((item, index) => {
				return (
					<Select.Option value={item.id} key={item.id}>
						{item.name}
					</Select.Option>
				);
			})}
		</Select>
	);
};

export default TextbookSearchSelect;
