import { Col, Row, Space } from "antd";
import { Field, FieldArrayRenderProps, Formik } from "formik";
import { Form, FormItem, Input } from "formik-antd";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { PartialDeep } from "type-fest";
import * as Yup from "yup";
import GeneralFileUploadField from "../../../../components/CustomFields/FileUploadField/GeneralFileUploadField";
import TextEditorField from "../../../../components/CustomFields/TextEditorField/TextEditorField";
import {
    useAddCourseMutation,
    useGetCourseInformationQuery,
    useUpdateCourseMutation,
} from "../../../../redux/features/Course/CourseAPI";
import {
    ActivityType,
    Chapter,
    Course,
    QuizItem,
    TextbookUnit,
    UnitItem,
} from "../../../../typescript/interfaces/courses/CourseTypes";
import { PartialExcept } from "../../../../typescript/typehelper/RecursivePartialExcept";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import LoadingPage from "../../../UtilPages/LoadingPage";
import { ROUTING_CONSTANTS } from "../../../../navigation/ROUTING_CONSTANTS";
import { DEFAULT_FRONTEND_ID } from "../../../../redux/interfaces/types";
import DoneButton from "../../../../components/ActionButton/DoneButton";
import "./index.less";
import SectionHeader from "../../../../components/SectionHeader/SectionHeader";
import "./CourseStyles.less";
import IMAGE_CONSTANTS from "../../../../constants/DefaultImage";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import { FORM_DEFAULT_MESSAGES } from "../../../../utils/FormUtils/formUtils";
import React from "react";
import BackButton from "../../../../components/ActionButton/BackButton";

type PartialQuiz = Omit<PartialExcept<QuizItem, "title">, "type">;

const baseAcitivySchema = Yup.object({
    id: Yup.number().required(),
    type: Yup.string().required(),
});

export const quizCourseSchema: Yup.SchemaOf<PartialQuiz> = Yup.object({
    id: Yup.number().required(),
    title: Yup.string().required(
        "Tên bài kiểm tra là thông tin bắt buộc"
    ),
    tag: Yup.string().required("Tên cột điểm là thông tin bắt buộc"),
})
    .concat(baseAcitivySchema)
    .required();

export const quizSchema: Yup.SchemaOf<PartialQuiz> = Yup.object({
    id: Yup.number().required(),
    title: Yup.string().required(
        "Tên bài kiểm tra là thông tin bắt buộc"
    ),
    // examId: Yup.number().positive("Đề kiểm tra là thông tin bắt buộc").required(FORM_DEFAULT_MESSAGES.REQUIRED),
    tag: Yup.string().required("Tên cột điểm là thông tin bắt buộc"),
})
    .concat(baseAcitivySchema)
    .required();

export const textBookSchema: Yup.SchemaOf<TextbookUnit> = Yup.object({
    textbookId: Yup.number()
        .positive("Hãy chọn một giáo trình")
        .required("Hãy chọn một giáo trình"),
    note: Yup.string().notRequired(),
    name: Yup.string().notRequired()
}).required();

export const unitSchema: Yup.SchemaOf<Omit<UnitItem, "type">> = Yup.object({
    id: Yup.number().required("Unit id required"),
    title: Yup.string().required("Tên của bài học là thông tin bắt buộc"),
    textbooks: Yup.array()
        .of(textBookSchema)
        .test(
            "Không được chọn một giáo trình nhiều lần",
            "Giáo trình đã có vui lòng chọn giáo trình khác",
            (textbooks) => {
                if (!textbooks) return true;
                if (textbooks?.length >= 2) {
                    // console.log("validate text books: ", textbooks);
                    const newTextBooks = textbooks?.slice(0);
                    const textBookSet = new Set(
                        newTextBooks.map((item) => item.textbookId)
                    );
                    // console.log("SET", textBookSet, "LIST", newTextBooks);
                    return textBookSet.size === newTextBooks?.length;
                }
                return true;
            }
        ),
    content: Yup.string().required("Nội dung của bài học là trường bắt buộc"),
})
    .concat(baseAcitivySchema)
    .required();

type PartialChapter = PartialDeep<Omit<Chapter, "actions">>;
//
export const chapterSchema = Yup.object({
    units: Yup.array().of(unitSchema),
    quizzes: Yup.array().of(quizSchema),
    // TODO: Fix to force order required
    order: Yup.number().notRequired(),
    id: Yup.number().required(),
    title: Yup.string().required("Tên chương là bắt  buộc"),
}).required();

const courseSchema = Yup.object({
    name: Yup.string().required("Tên khóa học là thông tin bắt buộc"),
    code: Yup.string().required("Mã khóa học là thông tin bắt buộc"),
    level: Yup.string().required("Trình độ học là thông tin bắt buộc"),
    price: Yup.number().positive("Giá của một khóa phải có giá trị hợp lệ"),
    background: Yup.string(),
    description: Yup.string(),
    createdAt: Yup.date(),
    programs: Yup.string(),
    chapters: Yup.array().of(chapterSchema),
}).required();

type ChapterWithEditting = Chapter & { isEditing: boolean };

const initialValues: Course = {
    chapters: [],
    id: 0,
    name: "",
    code: "",
    level: "",
};

const CreateOrUpdateCoursePage1 = () => {
    const navigate = useNavigate();
    const { courseId } = useParams();
    const location = useLocation();
    // * Course base  query
    const isSkip = (idParam: string | undefined) => {
        return isNaN(Number(idParam));
    };
    const { data, isSuccess, isFetching, isLoading, isError, error } =
        useGetCourseInformationQuery(Number(courseId), { skip: isSkip(courseId) });
    // * Course mutations
    const [addCourse] = useAddCourseMutation();
    const [updateCourse] = useUpdateCourseMutation();
    const isEditing = () => {
        return !isNaN(Number(courseId));

    };
    const isCreating = () => {
        console.log(
            "Course Is Creating:",
            location.pathname,
            ROUTING_CONSTANTS.NEW_COURSE
        );

        return location.pathname === ROUTING_CONSTANTS.NEW_COURSE;
    };

    if (isError) {
        AntdNotifier.error("Failed to load course");
        return <></>;
    } else if (isFetching) {
        return <LoadingPage />;
    } else if (isCreating() || (isEditing() && isSuccess && data)) {
        const addingActivity = (
            key: ActivityType,
            helper: FieldArrayRenderProps
        ) => {
            switch (key) {
                case "quiz":
                    helper.push({
                        type: "quiz",
                        title: "",
                        description: "",
                        tag: "",
                        testId: DEFAULT_FRONTEND_ID,
                    });
                    return;
                case "unit":
                    AntdNotifier.success("Added unit");

                    helper.push({
                        type: "unit",
                        title: "",
                        textbooks: [],
                        content: "",
                    });
                    return;
                default:
                    return;
            }
        };
        return (
            <>
                <Formik
                    validationSchema={courseSchema}
                    enableReinitialize={true}
                    validateOnChange={false}
                    initialValues={data || initialValues}
                    onSubmit={(formValues) => {
                        const values = formValues;
                        console.log("Submit COURSE: ", values);
                        if (!isEditing()) {
                            addCourse(values)
                                .unwrap()
                                .then((res) => {
                                    AntdNotifier.success(
                                        res.message
                                    );
                                    navigate(ROUTING_CONSTANTS.COURSE_MANAGEMENT);
                                })
                                .catch((err) => {
                                    AntdNotifier.error(
                                        err.data.message
                                    );
                                });
                        } else {
                            updateCourse(values)
                                .unwrap()
                                .then((res) => AntdNotifier.success("Cập nhật khóa học thành công"))
                                .catch((err) => {
                                    AntdNotifier.error(err.data.message);
                                });
                        }
                    }}
                >
                    {({
                        values,
                        errors,
                        setFieldError,
                        setFieldTouched,
                        dirty,
                        touched,
                        handleSubmit,
                        isValid,
                        setFieldValue,
                    }) => {
                        return (
                            <div className={"page-main-container"}>
                                <ContentHeader
                                    title={values.name || "Tạo khóa học"}
                                    action={
                                        <Space>
                                            <BackButton
                                                type="primary"
                                                danger
                                                onClick={() => {
                                                    navigate(
                                                        ROUTING_CONSTANTS.COURSE_MANAGEMENT
                                                    );
                                                }}
                                                className={"back-btn"}
                                            >
                                                <span>Quay lại</span>
                                            </BackButton>
                                            <DoneButton
                                                onClick={() => handleSubmit()}
                                                disabled={!dirty}
                                            >
                                                Xong
                                            </DoneButton>
                                        </Space>
                                    }
                                />
                                <div className={"course-information-container"}>
                                    <SectionHeader title={"Thông tin khóa học"} />
                                    <Row justify="center">
                                        <Col span={24}>
                                            <div className={"course-image-container"}>
                                                <img
                                                    className={"course-banner-image"}
                                                    src={
                                                        values.background ||
                                                        IMAGE_CONSTANTS.DEFAULT_BACKGROUND_IMAGE
                                                    }
                                                    alt="Ảnh nền"
                                                />
                                            </div>
                                        </Col>
                                        <Field
                                            name={"background"}
                                            component={GeneralFileUploadField}
                                            fast={true}
                                            showPreview={false}
                                            uploadButtonMessage={"Chọn ảnh nền khác"}
                                            allowedType={["jpg", "png", "jpge"]}
                                        />
                                    </Row>
                                    <Form layout="vertical">
                                        <Row gutter={16}>
                                            <Col span={16}>
                                                <FormItem
                                                    label="Tên"
                                                    name="name"
                                                    required
                                                    rules={[
                                                        {
                                                            required: true,
                                                            message: "Tên là phần bắt buộc!",
                                                        },
                                                    ]}
                                                    showValidateSuccess={true}
                                                >
                                                    <Input name="name" fast={true} />
                                                </FormItem>
                                            </Col>
                                            <Col span={8}>
                                                <FormItem
                                                    label="Mã khóa học"
                                                    name="code"
                                                    required
                                                    rules={[
                                                        {
                                                            required: true,
                                                            message: "Mã khóa học là phần bắt buộc!",
                                                        },
                                                    ]}
                                                    showValidateSuccess={true}
                                                >
                                                    <Input
                                                        name="code"
                                                        disabled={values?.id != null && values.id !== 0}
                                                        fast={true}
                                                    />
                                                </FormItem>
                                            </Col>
                                        </Row>
                                        <Row gutter={16}>
                                            <Col span={12}>
                                                <FormItem
                                                    label="Trình độ"
                                                    name="level"
                                                    required={true}
                                                    showValidateSuccess={true}
                                                >
                                                    <Input name="level" fast={true} />
                                                </FormItem>
                                            </Col>
                                            <Col span={12}>
                                                <FormItem
                                                    label="Giá (VND)"
                                                    name="price"
                                                    showValidateSuccess={true}
                                                >
                                                    <Input name="price" type={"number"} fast={true} />
                                                </FormItem>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col span={24}>
                                                <FormItem
                                                    label="Mô tả khóa học"
                                                    name="description"
                                                // initialValue={initialData?.content}
                                                >
                                                    <Field
                                                        name={"description"}
                                                        component={TextEditorField}
                                                        fast={true}
                                                    />
                                                </FormItem>
                                            </Col>
                                        </Row>
                                    </Form>
                                </div>
                                {/* // TODO: Below for debug */}
                                {/* {
                                    <>
                                        <JsonDisplay obj={values} cardProps={
                                            { title: "Values" }
                                        } />
                                        <Typography.Paragraph style={{ color: "green" }}>
                                            {
                                                JSON.stringify(touched)
                                            }
                                        </Typography.Paragraph>
                                        <Typography.Paragraph style={{ color: "red" }}>
                                            {
                                                JSON.stringify(errors)
                                            }
                                        </Typography.Paragraph>
                                    </>
                                } */}
                            </div>
                        );
                    }}
                </Formik>
            </>
        );
    } else return <>{JSON.stringify(data)}</>;
};

export default CreateOrUpdateCoursePage1;
