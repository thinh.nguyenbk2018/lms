import React from 'react';
import {Button, Result} from "antd";
import {useNavigate} from "react-router-dom";
import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";

const NotFoundPage = () => {
    const navigate = useNavigate();
    return (
        <Result
            status="404"
            title="404"
            subTitle="Xin lỗi. Đường dẫn bạn đi vào là không hợp lệ. Nếu bạn nghĩ đây là lỗi, vui lòng liên hệ với bộ phận kỹ thuật để được giải quyết"
            extra={<Button type="primary" onClick={() => navigate(ROUTING_CONSTANTS.DASHBOARD)}>Back Home</Button>}
        />
    );
};

export default NotFoundPage;