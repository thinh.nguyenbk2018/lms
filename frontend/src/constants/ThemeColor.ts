import { blue, green } from "@ant-design/colors";

const COLOR_SCHEME = {
    primary: blue.primary,
    success: green[6],
    error: "#c91c00"
}

export default COLOR_SCHEME;