import isNumeric from 'antd/es/_util/isNumeric';

import {ID, PaginatedResponse} from '../../interfaces/types';
import {SearchCriteria} from '../../utils/createSliceWithSearch/createSliceEnhancedWithSearch';
import {transformSearchQuery} from '../APIUtils/TransformParameter';
import {User, UserInfoForClickNavigation} from '../Auth/AuthSlice';
import {apiSlice} from '../CentralAPI';
import {
    QuestionAnswerPagedResponseDto,
    QuestionDataType,
    QuestionType,
    QuizSessionResponseDto
} from './CreateQuizSlice';
import {SessionID} from './TestSessionSlice';
import {TextbookRef} from "@typescript/interfaces/courses/CourseTypes";

export type Answer = number | string;
/**
 * POST to /exam_session/1/question/{questionId}
 * ? Do backend need question type information  ???
 *
 * @param answers Semantic:
 + MultipleChoice: [Choice Key 1, Choice Key 2]
 + FillInBlank Written: ["Blank 1 answer","Blank 2 answer"]
 + FillInBlank DragAndDrop / FillInBlank Multichoice: [Blank Answer Key 1, Blank Answer Key 2]
 + Written Test: ["What the student submitted in the test editor"]
 *
 * @param optionalStudentNote - This act as an fallback to store student additional notes in case the student cannot
 * interact with the question / error, ...
 */
export type QuestionAnswer = {
    answers: Answer[];
    optionalStudentNote?: string;
};

const quizAPIInvalidateTags = apiSlice.enhanceEndpoints({
    addTagTypes: [
        "Exam",
        "ExamContent",
        "ExamInstance",
        "Question",
        "ExamSession",
        "ExamResult",
        "QuizManagement",
        "ClassContent",
        "AnsweredAndFlag"
    ],
});


export const TEST_CONTENT_PATH_ITEM = "exam";
export const QUIZ_ITEM_PATH_ITEM = "quiz";

type DatePresentation = number | string | Date;
export type TestConfig = {
    haveTimeLimit: boolean;
    timeLimit: number;
    canNavigateBackward: boolean;
    allowDisconnect: boolean;
    state: "PUBLIC" | "PRIVATE";
    startedAt: DatePresentation | undefined;
    validBefore: DatePresentation | undefined;
    passScore: number | undefined;
    viewPreviousSessions?: boolean;
    viewPreviousSessionsTime?: Date;
    showResultImmediately: boolean;
    showResultTime?: Date;
    classId: number;
    maxAttempt: number;
    calculateFinalResultWay: "ARITHMETIC_MEAN" | "LAST_TIME" | "HIGHEST_SCORE";
};

export interface QuestionIdAndType {
    id: number;
    type: string;
    order: number;
    status: "ANSWERED" | "PARTIALLY_ANSWERED" | "NONE";
    flagged: boolean;
}

export interface Test {
    id: number;
    title: string;
    description: string;
    textbooks: TextbookRef[];
    questionList: PaginatedResponse<QuestionDataType>;
    courseId: ID;
    courseName?: string;
    totalGrade: number;

    state: "PUBLIC" | "PRIVATE";

    createdAt?: DatePresentation;
    createdBy?: UserInfoForClickNavigation;
    updatedAt?: DatePresentation;
    updatedBy?: UserInfoForClickNavigation;
}

export type BriefTestItem = Pick<Test,
    "id" | "title" | "description" | "courseId" | "updatedAt">;

export type TestWithConfig = Test & {
    config: TestConfig;
};

export function isPaginatedResponseQuestionList(
    data: any
): data is PaginatedResponse<QuestionDataType> {
    if (!data) {
        return false;
    }
    return isNumeric((data as PaginatedResponse<QuestionDataType>).page);
}

export type FinalVerdict = "PASSED" | "FAILED";
export type TestGradingState = "DONE" | "WAITING";

type TestSession = {
    // * Configuration from instance
    instanceTitle: string;
    instanceDescription: string;

    config: TestConfig;

    questionList: QuestionDataType[] | PaginatedResponse<QuestionDataType>;
    classId: number;
    // * Session private states
    id: SessionID;
    startedAt?: number;
    submittedAt?: number;
};

export type QuestionQuizStatistic = {
     courseName: string;
     courseCode: string;
     description: string;
     type: string;
     averageScore: number;
     standardDeviation: number;
     gradedAttempts : number;
     notAnswered : number;
     needGrading: boolean;
}


export type FinalResult = "PASSED" | "FAILED";
export type StudentSessionResult = {
    sessionId: SessionID;
    user: User;
    totalScore: number;
    score: number;
    startedAt: DatePresentation;
    submittedAt: DatePresentation;
    timeTaken: number;
    attempt: number;
    gradedState: TestGradingState;
    finalVerdict: FinalVerdict;
    questions: QuestionAnswerPagedResponseDto;
    isLastSession?: boolean;
};
// * Review the result of the test
export type TestStudentResultList = {
    listData: StudentSessionResult[];
};
export type TeacherQuizResultData = {
    totalParticipation: number;
    averageScore: number;
    totalNumberOfPassedStudent: number;
    sessionsResult: PaginatedResponse<StudentSessionResult>;
};
export const prepareRemovePathFromIds = (idList: ID[]) => {
    return idList
        .reduce((prev, curr, _idx, _arr) => {
            return prev + `id=${curr}&`;
        }, "")
        .slice(0, -1);
};

const examAPI = quizAPIInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        // ? QUESTION_CONTENT API
        // * GET_TEST_CONTENT_LIST: Expect returning list of test meta: {id,title,description,courseId,courseName}

        // * courseName: display on table | courseId: Click on course name --> navigate to the clicked course page
        getTestContentList: build.query<PaginatedResponse<BriefTestItem & { courseName: string }>,
            { courseId: number; searchCriteria: SearchCriteria }>({
            query: ({courseId, searchCriteria}) =>
                `/course/${courseId}/${TEST_CONTENT_PATH_ITEM}?${transformSearchQuery(
                    searchCriteria
                )}`,
            providesTags: (_result) => {
                return [{type: "Exam", id: "LIST"}];
            },
        }),

        searchExams: build.query<PaginatedResponse<BriefTestItem & { courseName: string }>,
            { courseId: number; searchCriteria: SearchCriteria }>({
            query: ({courseId, searchCriteria}) =>
                `/course/${courseId}/${TEST_CONTENT_PATH_ITEM}/search?${transformSearchQuery(
                    searchCriteria
                )}`,
            providesTags: (_result) => {
                return [{type: "Exam", id: "LIST"}];
            },
        }),

        // * GET TEST DETAIL
        getTest: build.query<Test,
            SearchCriteria & { courseId: number; testId: number }>({
            query: ({courseId, testId, ...searchCriteria}) =>
                `course/${courseId}/${TEST_CONTENT_PATH_ITEM}/${testId}?${transformSearchQuery(
                    searchCriteria
                )}`,
            providesTags: (_res, _error, args) => [
                // {type: 'ExamContent', id: args.testId}
                {type: "ExamContent", id: args.testId},
            ],
        }),

        // * CREATE_TEST_CONTENT
        createTestContent: build.mutation<Partial<Test>,
            { testData: Partial<Test>; courseId: number }>({
            query({courseId, testData}) {
                return {
                    url: `course/${courseId}/${TEST_CONTENT_PATH_ITEM}`,
                    method: "POST",
                    body: testData,
                };
            },
            invalidatesTags: [{type: "Exam", id: "LIST"}],
        }),

        // * UPDATE_TEST_CONTENT: PATCHING THE TEST METAINFO
        updateTest: build.mutation<Test,
            Partial<Test> & { courseId: number; id: number }>({
            query: ({id, courseId, ...others}) => ({
                url: `course/${courseId}/${TEST_CONTENT_PATH_ITEM}/${id}`,
                method: "PATCH",
                body: {
                    ...others,
                },
            }),
            invalidatesTags: (result) => [{type: "ExamContent", id: result?.id}],
        }),

        // * DELETE_TEST_CONTENT
        deleteTest: build.mutation<number, { testIdToDelete: number }>({
            query({testIdToDelete}) {
                return {
                    url: `${TEST_CONTENT_PATH_ITEM}/${testIdToDelete}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: [{type: "Exam", id: "LIST"}],
        }),
        // * IMPORT_QUESTION
        importQuestion: build.mutation<Test, { testId: ID; questionIds: ID[] }>({
            query(args) {
                return {
                    url: `${TEST_CONTENT_PATH_ITEM}/${args.testId}`,
                    method: "POST",
                    body: args.questionIds,
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: "ExamContent", id: args.testId},
            ],
        }),
        // * Question related apis
        /** Question here can also be a GROUP of question */
        // * ADD_QUESTION
        addQuestionToTest: build.mutation<Partial<QuestionDataType> & { testId?: ID },
            Omit<Partial<QuestionDataType>, "id"> & { courseId: number; testId: ID }>({
            query({courseId, testId, ...others}) {
                return {
                    url: `course/${courseId}/${TEST_CONTENT_PATH_ITEM}/${testId}/question`,
                    method: `POST`,
                    body: {
                        ...others,
                    },
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `ExamContent`, id: args.testId},
            ],
        }),
        // * Question related apis
        // * ADD_QUESTION
        updateQuestionInTest: build.mutation<number,
            Partial<QuestionDataType> & { testId: ID }>({
            query({testId, id: questionId, ...others}) {
                return {
                    url: `${TEST_CONTENT_PATH_ITEM}/${testId}/question/${questionId}`,
                    method: `PUT`,
                    body: {
                        ...others,
                        id: questionId
                    },
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `ExamContent`, questionId: args.id},
            ],
        }),
        // ** List of questionList to remove
        // * REMOVE_QUESTION
        removeQuestionsFromTest: build.mutation<void,
            { testId: number; questionId: ID }>({
            query({testId, questionId}) {
                return {
                    url: `${TEST_CONTENT_PATH_ITEM}/${testId}/question/${questionId}`,
                    method: "DELETE",
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `ExamContent`, id: args.testId},
            ],
        }),
        // * GET_QUESTION
        getQuestion: build.query<QuestionDataType,
            { testContentId: number; questionId: number }>({
            query: ({testContentId, questionId}) =>
                `${TEST_CONTENT_PATH_ITEM}/${testContentId}/question/${questionId}`,
        }),

        // * QUESTION GROUP RELATED APIs:
        createNewQuestionGroup: build.mutation<Partial<QuestionDataType> & { testId?: ID },
            Omit<Partial<QuestionDataType>, "id"> & { courseId: number; testId: ID }>({
            query({courseId, testId, ...others}) {
                return {
                    url: `course/${courseId}/${TEST_CONTENT_PATH_ITEM}/${testId}/group_question`,
                    method: `POST`,
                    body: {
                        ...others,
                    },
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `ExamContent`, id: args.testId},
            ],
        }),

        addQuestionToQuestionGroup: build.mutation<Partial<QuestionDataType> & { testId?: ID },
            Omit<Partial<QuestionDataType>, "id"> & {
            courseId: number;
            testId: ID;
            groupId: ID;
        }>({
            query({courseId, testId, groupId, ...others}) {
                return {
                    url: `course/${courseId}/${TEST_CONTENT_PATH_ITEM}/${testId}/group_question/${groupId}`,
                    method: `POST`,
                    body: {
                        ...others,
                    },
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `ExamContent`, id: args.testId},
            ],
        }),

        // ? QUESTION_INSTANCE APIs
        // * CREATE_TEST_INSTANCE
        // ? TEST INSTANCE INSIDE CLASS - WITH CONFIG
        // * UPDATE_TEST_INSTANCE:
        // * CREAT AND UPDATE TEST WITH CONFIG
        // getTestWithConfigList
        // Quiz entry: In course - Only create a item
        // In class - Add test to item.
        createUpdateTestConfig: build.mutation<TestWithConfig,
            Partial<TestConfig> & {
            examId: number;
            classId: number;
            courseId: number;
        }>({
            query: ({examId, classId, courseId, ...other}) => ({
                url: `/course/${courseId}/class/${classId}/${QUIZ_ITEM_PATH_ITEM}/${examId}`,
                body: {
                    ...other,
                    examId,
                    classId,
                    courseId,
                },
                method: "POST",
            }),
            invalidatesTags: (_res, _error, args) => [
                {type: "ExamInstance", id: `${args.examId}`},
            ],
        }),
        // * GET_TEST_INSTANCE INSIDE CLASS = CONFIG + QUESTIONS WITH POINT
        getTestInstanceWithConfig: build.query<TestWithConfig,
            {
                searchCriteria: SearchCriteria;
                testWithConfigId: number;
                classId: number;
                courseId: number;
            }>({
            query: ({courseId, classId, searchCriteria, testWithConfigId}) =>
                `course/${courseId}/class/${classId}/${QUIZ_ITEM_PATH_ITEM}/${testWithConfigId}?${transformSearchQuery(
                    searchCriteria
                )}`,
            providesTags: (_result, _error, args) => {
                return [{type: "ExamInstance", id: args.testWithConfigId}];
            },
        }),

        getQuizOverviewStudent: build.query<TestWithConfig,
            { testWithConfigId: number; classId: number; }>({
            query: ({classId, testWithConfigId}) =>
                `/class/${classId}/${QUIZ_ITEM_PATH_ITEM}/${testWithConfigId}`,
            providesTags: (_result, _error, args) => {
                return [{type: "ExamInstance", id: args.testWithConfigId}];
            },
        }),

        // * REMOVE_TEST_INSTANCE
        removeTestInstanceWithConfig: build.mutation<void,
            { testInstanceId: number }>({
            query: ({testInstanceId}) => ({
                url: `${TEST_CONTENT_PATH_ITEM}/${testInstanceId}`,
            }),
            invalidatesTags: ["ExamInstance"],
        }),

        // ? TEST SESSION APIs
        // * GET FULL TEST SESSION
        getFullTestSession: build.query<QuizSessionResponseDto,
            {
                testWithConfigId: number;
                classId: number;
                sessionId: SessionID;
                searchCriteria: SearchCriteria;
                questionId: number | undefined
            }>({
            query: ({
                        classId,
                        sessionId,
                        searchCriteria,
                        testWithConfigId,
                        questionId
                    }) => ({
                url: `class/${classId}/${QUIZ_ITEM_PATH_ITEM}/${testWithConfigId}/session/${sessionId}?${questionId ? 'question=' + questionId + '&' : ''}${transformSearchQuery(searchCriteria)}`,
                method: "GET",
            }),
            providesTags: (_res, _err, args) => {
                return [{type: "ExamSession", id: args.sessionId}];
            },
        }),
        getQuestionsIdAndType: build.query<QuestionIdAndType[],
            {
                examId: number,
                sessionId: string
            }>({
            query: ({
                        examId,
                        sessionId
                    }) => ({
                url: `/exam/${examId}/session/${sessionId}`,
                method: "GET",
            }),
            providesTags: (_res, _err, args) => {
                return [{type: "AnsweredAndFlag", id: "LIST"}];
            },
        }),
        flagQuestion: build.mutation<void, { sessionId: string, questionId: number }>({
            query({sessionId, questionId}) {
                return {
                    url: `/session/${sessionId}/question/${questionId}/flag`,
                    method: 'post'
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `AnsweredAndFlag`, id: "LIST"},
            ]
        }),
        unFlagQuestion: build.mutation<void, { sessionId: string, questionId: number }>({
            query({sessionId, questionId}) {
                return {
                    url: `/session/${sessionId}/question/${questionId}/unFlag`,
                    method: 'post'
                };
            },
            invalidatesTags: (_result, _error, args) => [
                {type: `AnsweredAndFlag`, id: "LIST"},
            ]
        }),
        getExamIdOfQuiz: build.query<number,
            {
                quizId: number
            }>({
            query: ({
                        quizId
                    }) => ({
                url: `/quiz/${quizId}/exam`,
                method: "GET",
            })
        }),
        // * INIT_TEST_SESSION
        initTestSession: build.mutation<TestSession,
            { testWithConfigId: number; classId: number }>({
            query: ({testWithConfigId, classId}) => ({
                url: `class/${classId}/${QUIZ_ITEM_PATH_ITEM}/${testWithConfigId}/session/init`,
                method: "POST",
            }),
            invalidatesTags: (_res, _err, args) => [
                {type: "ExamSession", id: args.testWithConfigId},
            ],
        }),
        // * END_TEST_SESSION
        endTestSession: build.mutation<void,
            { classId: ID; quizId: ID; testSessionId: SessionID }>({
            query: ({testSessionId, classId, quizId}) => ({
                url: `/class/${classId}/quiz/${quizId}/session/${testSessionId}/end`,
                method: "POST",
            }),
            invalidatesTags: (_res, _err, args) => [
                {type: "ExamSession", id: args.testSessionId},
                {type: "ExamResult", id: "CLASS_LIST"},
                {type: "ClassContent", id: "QUIZ_DETAIL"},
            ],
        }),

        // * ANSWER_QUESTION
        submitStudentAnswer: build.mutation<void,
            {
                studentAnswer: QuestionAnswer;
                testSessionId: SessionID;
                courseId: number;
                classId: number;
                quizId: number;
                questionId: number;
            }>({
            query({
                      courseId,
                      classId,
                      questionId,
                      quizId,
                      testSessionId,
                      ...answerBody
                  }) {
                return {
                    url: `course/${courseId}/class/${classId}/quiz/${quizId}/session/${testSessionId}/question/${questionId}`,
                    method: "POST",
                    body: answerBody.studentAnswer,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "ExamSession", id: args.testSessionId},
                {type: "AnsweredAndFlag", id: "LIST"}
            ],
        }),

        // * Viewing RESULT
        getTeacherQuizResult: build.query<TeacherQuizResultData,
            { quizId: number; searchCriteria: SearchCriteria; }>({
            query: ({quizId, searchCriteria}) => `quiz/${quizId}/session_result/management?${transformSearchQuery(searchCriteria)}`,
            providesTags: ["QuizManagement"],
        }),

        getQuizResultOfStudent: build.query<TestStudentResultList,
            { classId: number; quizId: number; studentId: number }>({
            query: ({classId, quizId, studentId}) =>
                `class/${classId}/quiz/${quizId}/session/?user=${studentId}`,
            providesTags: (_res, _err, args) => [
                {type: "ExamResult", id: args.studentId},
            ],
        }),

        getSessionsResultOfStudent: build.query<TestStudentResultList,
            { classId: number; quizId: number }>({
            query: ({classId, quizId}) =>
                `class/${classId}/quiz/${quizId}/session/student`,
            providesTags: (_res, _err, args) => [
                {type: "ExamResult", id: "CLASS_LIST"},
            ],
        }),

        // * Endpoint for grading
        getSessionResult: build.query<StudentSessionResult,
            {
                classId: ID;
                quizId: ID;
                sessionId: SessionID;
                searchCriteria: SearchCriteria;
                question: number | undefined;
            }>({
            query: ({classId, quizId, sessionId, searchCriteria, question}) =>
                `/class/${classId}/quiz/${quizId}/session/${sessionId}/result?${question ? 'question=' + question + '&' : ''}${transformSearchQuery(
                    searchCriteria
                )}`,
            providesTags: (_res, _err, args) => [
                {type: "ExamResult", id: args.sessionId},
            ],
        }),
        updatePointForWritingQuestion: build.mutation<void,
            {
                courseId: ID;
                classId: ID;
                quizId: ID;
                sessionId: SessionID;
                questionId: ID;
                gradingDto: {
                    point: number;
                    type: QuestionType;
                    resultState?: any;
                };
            }>({
            query: ({
                        courseId,
                        classId,
                        quizId,
                        sessionId,
                        questionId,
                        gradingDto,
                    }) => {
                return {
                    url: `course/${courseId}/class/${classId}/quiz/${quizId}/session/${sessionId}/question/${questionId}/point`,
                    method: "POST",
                    body: gradingDto,
                };
            },
            invalidatesTags: (_res, _err, args) => [
                {type: "ExamResult", id: args.sessionId},
                "QuizManagement",
            ],
        }),
    }),
    overrideExisting: true,
});

export const {
    // * Test content
    useGetTestContentListQuery,
    useGetTestQuery,
    useCreateTestContentMutation,
    useUpdateTestMutation,
    useDeleteTestMutation,
    // * Question in test
    useRemoveQuestionsFromTestMutation,
    useUpdateQuestionInTestMutation,
    useAddQuestionToTestMutation,
    useGetQuestionQuery,
    // Lazy ones
    useLazyGetTestContentListQuery,
    useLazyGetTestQuery,
    useLazyGetQuestionQuery,
    //  ? Question group
    useCreateNewQuestionGroupMutation,
    useAddQuestionToQuestionGroupMutation,

    // * Test with config: Inside class
    useRemoveTestInstanceWithConfigMutation,
    useCreateUpdateTestConfigMutation,
    useGetTestInstanceWithConfigQuery,

    useLazyGetTestInstanceWithConfigQuery,
    // * Test session
    useGetFullTestSessionQuery,
    useSubmitStudentAnswerMutation,
    useInitTestSessionMutation,
    useEndTestSessionMutation,
    useLazyGetFullTestSessionQuery,
    // * Quiz Overview
    // ***** Teacher  ****** //
    useGetTeacherQuizResultQuery,
    useGetQuizOverviewStudentQuery,
    useLazyGetSessionResultQuery,
    useUpdatePointForWritingQuestionMutation,
    // * Test result
    useGetQuizResultOfStudentQuery,
    useGetSessionsResultOfStudentQuery,
    useGetQuestionsIdAndTypeQuery,
    useGetExamIdOfQuizQuery,
    useFlagQuestionMutation,
    useUnFlagQuestionMutation,
    useSearchExamsQuery,
} = examAPI;
