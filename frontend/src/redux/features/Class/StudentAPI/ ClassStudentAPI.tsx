import { apiSlice } from "../../CentralAPI";
import { BaseResponse, PaginatedResponse } from "../../../interfaces/types";
import { SearchCriteria } from "../../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import { transformSearchQuery } from "../../APIUtils/TransformParameter";
import { Student as StudentNotInClass } from "@redux/features/Student/StudentAPI";

const MemberApiWithInvalidateTags = apiSlice.enhanceEndpoints({
	addTagTypes: ["ClassStudent", "StudentsNotInClass"],
});

type Student = {
	userId: number;
	firstName: string;
	lastName: string;
	username: string;
	avatar: string;
	roles?: string[];
	email: string;
	phone: string;
	address: string;
};

interface AddStudentToClassResponse extends BaseResponse {

}

interface GetStudentsNotInClass extends BaseResponse {
	studentInfos: Omit<StudentNotInClass, "username" | "address">[]
}

const classStudentApi = MemberApiWithInvalidateTags.injectEndpoints({
	endpoints: (build) => ({
		getStudents: build.query<
			PaginatedResponse<Student>,
			{ classId: number; searchCriteria: SearchCriteria }
		>({
			query: ({ classId, searchCriteria }) =>
				`classes/${classId}/students?${transformSearchQuery(searchCriteria)}`,
			providesTags: (result) =>
				result
					? [
						...result.listData.map(
							({ userId }) => ({ type: "ClassStudent", userId } as const)
						),
						{ type: "ClassStudent", id: "LIST" },
					]
					: [{ type: "ClassStudent", id: "LIST" }],
		}),
		getStudentDetail: build.query<
			Student,
			{ classId: number; votingId: number }
		>({
			query: ({ classId, votingId }) => {
				return {
					url: `classes/${classId}/members/${votingId}`,
					method: "GET",
				};
			},
			providesTags: (_res, _err, args) => [
				{ type: "ClassStudent", id: args.votingId },
			],
		}),
		addStudentsToClass: build.mutation<AddStudentToClassResponse, { classId: number, studentIds: number[] }>({
			query: ({ classId, studentIds }) => ({
				url: `/classes/${classId}/students`,
				method: "POST",
				body: { studentIds }
			}),
			invalidatesTags: [{ type: "ClassStudent", id: "LIST" }, { type: "StudentsNotInClass", id: "LIST" }]
		}),
		getStudentsNotInClass: build.query<GetStudentsNotInClass, { keyword: string, classId: number }>({
			query: ({ keyword, classId }) => `/students/getStudentsNotInClass?q=${keyword}&classId=${classId}`,
			providesTags: [{ type: "ClassStudent", id: "LIST" }, { type: "StudentsNotInClass", id: "LIST" }]
		}),
	}),
	overrideExisting: false,
});

export const {
	useGetStudentsQuery,
	useGetStudentDetailQuery,
	useAddStudentsToClassMutation,
	useLazyGetStudentsNotInClassQuery
} = classStudentApi;
