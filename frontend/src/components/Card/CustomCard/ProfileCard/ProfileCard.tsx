import React from "react";
import { User } from "../../../../redux/features/Auth/AuthSlice";
import BaseCard from "../BaseCard";
import "./ProfileCard.less";

type UserDescriptionProps = {
	phone?: string | number | undefined;
	email?: string | undefined;
	username?: string;
	description?: string | undefined;
};
export const UserDescription = React.memo(
	({ phone, email, username, description }: UserDescriptionProps) => {
		return (
			<div className="description-container">
				<div style={{display: username == undefined ? 'none' : ''}} className="description-field">
					<span className="field">Username: </span>
					<span className="value">{username || ""}</span>
				</div>
				<div className="description-field">
					<span className="field">Email: </span>
					<span className="value">
						<a href={`mailto:${email}`}>{email || "Không"}</a>
					</span>
				</div>
				<div className="description-field">
					<span className="field">Số điện thoại: </span>
					<span className="value">{phone || "Không"}</span>
				</div>
				{description && (
					<div className="description-field">
						<span className="value">{description}</span>
					</div>
				)}
			</div>
		);
	}
);

const ProfileCard = (props: Partial<User>) => {
	const { username, firstName, lastName, avatar, phone, email } = props;

	const fullName = `${firstName}, ${lastName}`;
	return (
		<>
			<BaseCard
				title={fullName}
				withMiddleImage={true}
				middleImage={avatar}
				description={
					<UserDescription
						phone={phone || ""}
						email={email || ""}
						username={username || ""}
					/>
				}
			/>
		</>
	);
};

export default ProfileCard;
