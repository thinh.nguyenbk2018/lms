import defaultAva from '@assets/images/default_avatar.jpg';
import flagged from '@assets/images/flagged.svg';
import unflagged from '@assets/images/unflagged.svg';
import defaultCourseBackground from '@assets/images/default_course_back_ground.png';

const IMAGE_CONSTANTS = {
    DEFAULT_AVATAR: defaultAva,
    FLAGGED: flagged,
    UNFLAGGED: unflagged,
    DEFAULT_BACKGROUND_IMAGE: defaultCourseBackground
}

export default IMAGE_CONSTANTS;