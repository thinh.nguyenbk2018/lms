import { DeleteOutlined } from "@ant-design/icons";
import { Button, Col, Row, Typography } from "antd";
import { DropTargetMonitor } from "react-dnd";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { gradeFormulaSeletor, appendFormula, ElementFormula, updateFormula } from "../../../redux/features/Grade/GradeFormulaSlice";
import DropZone from "../../DropZone/DropZone";
import ElementFormulaComponent from "../ElementFormula/ElementFormulaComponent";
import "./GradeFormulaGenerator.less"

const GradeFormulaGenerator = () => {
  let tags = useAppSelector(gradeFormulaSeletor.tagsSelector);
  let formula = useAppSelector(gradeFormulaSeletor.formulaSelector);
  const dispatch = useAppDispatch();

  const handleAppendFormula = (element: ElementFormula) => {
    dispatch(appendFormula(element));
  }

  const onDropElement = (item: { element: ElementFormula, sortIndex: number }, monitor: DropTargetMonitor<unknown, unknown>, dropZoneIndex: number) => {
    let newFormula = [...formula.map(ele => ({ ...ele }))];
    let dragData = newFormula.splice(item.sortIndex, 1)[0];
    if (item.sortIndex < dropZoneIndex) dropZoneIndex--;
    newFormula.splice(dropZoneIndex, 0, dragData);
    dispatch(updateFormula(newFormula));
  }

  const DropZoneElementFormula = ({ dropZoneIndex }: { dropZoneIndex: number }) => (
    <DropZone
      accept={"ElementFormula"}
      onDrop={(item: { element: ElementFormula, sortIndex: number }, monitor) =>
        onDropElement(item, monitor, dropZoneIndex)
      }
      checkCanDrop={(item: { sortIndex: number }, monitor) => {
        return (
          item.sortIndex !== dropZoneIndex &&
          item.sortIndex + 1 !== dropZoneIndex
        );
      }}
      isResize={true}
      style={{ height: 30, width: 0, border: "1px dashed rgba(0, 0, 0, 0)" }}
      canDropStyle={{ width: 30, height: 30, border: "1px dashed grey" }}
      cannotDropStyle={{ backgroundColor: "yellow" }}
      direction="Horizontal"
    />
  );

  const onDeleteElement = (item: { element: ElementFormula, sortIndex: number }) => {
    let newFormula = [...formula.map(ele => ({ ...ele }))];
    newFormula.splice(item.sortIndex, 1);
    dispatch(updateFormula(newFormula));
  }

  const DropZoneDelete = () => (
    <DropZone
      accept={"ElementFormula"}
      onDrop={(item: { element: ElementFormula, sortIndex: number }, monitor) =>
        onDeleteElement(item)
      }
      checkCanDrop={(item, monitor) => true}
      isResize={true}
      style={{ height: 40, width: 40, border: "1px solid rgba(0, 0, 0, 0)", borderRadius: "50%", opacity: 0.8 }}
      canDropStyle={{ width: 50, height: 50, border: "1px solid rgba(0, 0, 0, 0)", borderRadius: "50%", opacity: 1, boxShadow: "0px 0px 5px red" }}
      cannotDropStyle={{ backgroundColor: "yellow" }}
      direction="Horizontal"
    >
      <Button shape="circle" icon={<DeleteOutlined />} style={{ width: "100%", height: "100%" }} danger />
    </DropZone>
  );

  return <>
    <Row>

    </Row>
    <Row gutter={[8, 8]}>
      <Col span={20}>
        <div className="formulaWrapper">
          {formula.length > 0 && <>
            <DropZoneElementFormula dropZoneIndex={0} />
            {formula
              .map<React.ReactNode>((element, index) => (
                <ElementFormulaComponent element={element} index={index} key={index} />
              ))
              .reduce((prev, curr, currIndex) => [
                prev,
                <DropZoneElementFormula dropZoneIndex={currIndex} />,
                curr,
              ])}
            <DropZoneElementFormula dropZoneIndex={formula.length} />
          </>
          }
          {
            <div className="deleteZone">
              <DropZoneDelete />
            </div>
          }
        </div>
        <Typography.Text style={{ fontStyle: "italic", fontSize: 12 }}><span style={{ color: "red" }}>* </span>Kéo thả để chỉnh sửa công thức, kéo vào vùng xóa để xóa phần tử</Typography.Text>
      </Col>
      <Col span={4}>
        <Row gutter={[8, 8]}>
          <Col span={12}>
            <Button style={{ width: "100%" }}
              onClick={() => handleAppendFormula({ type: "Operator", value: "+" })}
            >+</Button>
          </Col>
          <Col span={12}>
            <Button style={{ width: "100%" }}
              onClick={() => handleAppendFormula({ type: "Operator", value: "-" })}
            >-</Button>
          </Col>
          <Col span={12}>
            <Button style={{ width: "100%" }}
              onClick={() => handleAppendFormula({ type: "Operator", value: "*" })}
            >*</Button>
          </Col>
          <Col span={12}>
            <Button style={{ width: "100%" }}
              onClick={() => handleAppendFormula({ type: "Operator", value: "/" })}
            >/</Button>
          </Col>
          <Col span={12}>
            <Button style={{ width: "100%" }}
              onClick={() => handleAppendFormula({ type: "Bracket", value: "(" })}
              danger
            >(</Button>
          </Col>
          <Col span={12}>
            <Button style={{ width: "100%" }}
              onClick={() => handleAppendFormula({ type: "Bracket", value: ")" })}
              danger
            >)</Button>
          </Col>
          <Col span={24}>
            <Button style={{ width: "100%" }} type="primary"
              onClick={() => handleAppendFormula({ type: "Number", value: 0 })}
            >Số</Button>
          </Col>
          <Col span={24}>
            <Button style={{ width: "100%" }} type="primary" disabled={tags.length === 0}
              onClick={() => handleAppendFormula({ type: "Tag", value: tags[0] })}
            >Cột điểm</Button>
          </Col>
          <Col span={24}>
            <Button danger style={{ width: "100%" }} type="primary"
              onClick={() => dispatch(updateFormula([]))}
            >Xóa hết</Button>
          </Col>
        </Row>
      </Col>
    </Row>
  </>
}

export default GradeFormulaGenerator;