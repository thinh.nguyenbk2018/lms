import './PackageComponent.less';

const PackageComponent = ({ selected, price, title, onSelect }: { selected: boolean, title: string, price: number, onSelect?: () => void }) => {
  return <div className={`package-wrapper ${selected ? 'selected' : ''}`} onClick={onSelect}>
    <h3>
      {title}
    </h3>
    <h2 className='price' >
      {price.toLocaleString()}đ
    </h2>
    <hr style={{ width: '100%', marginBottom: 15, marginTop: -2 }} />
    <div className='features'>
      <ul>
        <li>
          Chức năng 1
        </li>
        <li>
          Chức năng 2
        </li>
        <li>
          Chức năng 3
        </li>
      </ul>
    </div>
  </div>
}

export default PackageComponent;