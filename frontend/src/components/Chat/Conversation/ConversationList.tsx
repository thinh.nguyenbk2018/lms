import React from 'react';
import {Avatar, Space, Tooltip} from "antd";
import ConversationBox from "./ConversationBox";
import {MessageTwoTone} from "@ant-design/icons";
import NewMessagePromptBox from "./NewMessagePromptBox";
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {chatSliceSelector, showChat, showChatSearchBox} from "../../../redux/features/Chat/ChatSlice";
import {BriefChatInfo} from '../../../redux/features/Chat/ChatAPI';

const ConversationList = () => {
    const conv = useAppSelector(chatSliceSelector.selectActiveChats);
    const dispatch = useAppDispatch();


    const minimizedChats = useAppSelector(chatSliceSelector.selectMinimizedChats);
    const isChatSearchBoxVisible = useAppSelector(chatSliceSelector.selectShowChatSearchBox);


    const onActivate = (itemClicked: BriefChatInfo) => {
        dispatch(showChat(itemClicked.chatRoomId));
    }
    const onNewMessage = () => {
        dispatch(showChatSearchBox());
    }
    return (
        <Space style={{zIndex: "12", position: "fixed", bottom: "10px", right: "10px"}} align={"end"}>

            {
                isChatSearchBoxVisible && <NewMessagePromptBox/>
            }
            {
                conv.map((item, index) => {
                    return <ConversationBox {...item} key={index}/>
                })
            }
            <Space direction={"vertical"} align={"end"} style={{margin: "1em"}}>
                {
                    minimizedChats.map((item) => {
                        // console.log("Minized item is: ", item);
                        return <span onClick={() => onActivate(item)}>
                            <Avatar src={item.image} size={"large"}/>
                        </span>

                    })
                }
                {
                    <Tooltip title={"New Message"}>
                        <span onClick={() => onNewMessage()}>
                            <Avatar size={"large"} icon={<MessageTwoTone/>} style={{
                                backgroundColor: "white",
                                boxShadow: `0 3px 6px -4px #0000001f,0 6px 16px #00000014,0 9px 28px 8px #0000000d`
                            }}/>
                        </span>
                    </Tooltip>
                }
            </Space>
        </Space>
    );
};

export default ConversationList;
