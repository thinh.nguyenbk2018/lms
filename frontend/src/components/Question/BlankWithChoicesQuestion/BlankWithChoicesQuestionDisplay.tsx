import React, { useState } from 'react';
import { Card, Col, Divider, Row, Select, Space, Tooltip } from "antd";
import { BlankWithChoicesQuestion, BlankWithOption, Choice, } from "../../../redux/features/Quiz/CreateQuizSlice";
import { DND_BLANK_SPAN_TAG_CLASS, } from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import ReactHtmlParser from "html-react-parser";
import { blankRegex } from "../FillBlankQuestion/FillInBlankQuestionDisplay";
import { CheckOutlined, InfoCircleOutlined } from '@ant-design/icons';
import COLOR_SCHEME from "../../../constants/ThemeColor";
import './BlankWithChoicesQuestionDisplay.less';
import { Attachment } from '../Attachment/AttachmentQuestion';
import AttachmentRender from '../Attachment/AttachmentRender';

const { Option } = Select;


// const text = "This is a test tring [ass] with brackets ir[here]versable";

// const res = _.split(text, /\[.*?\]/);
export const BlankWithChoices = (props: BlankWithOption & { index: number; mode: "create" | "doing" }) => {
    const { index, hint = "", answerList, correctAnswerKey } = props;
    const [selectedChoice, setSelectedChoice] = useState<number>();
    return (
        <Space>
            {(selectedChoice === correctAnswerKey) && <CheckOutlined style={{ color: COLOR_SCHEME.success }} />}
            <Select placeholder={"Chọn đáp án"}
                defaultValue={Number(correctAnswerKey)}
                onChange={(value, option) => {
                    console.log("SELECT: ", value);
                    setSelectedChoice(value);
                }}
                disabled={true}
                className={'fill-in-blank-with-choices-input'}
            >
                {
                    answerList.map((item, index) => <Option key={item.answerKey} value={item.answerKey}>
                        {
                            item.content
                        }
                    </Option>)
                }
            </Select>
            {
                hint && <Tooltip title={`Gợi ý: ${hint}`}>
                    <InfoCircleOutlined style={{ color: COLOR_SCHEME.primary }} />
                </Tooltip>
            }
        </Space>
    );
}

/**
 * Enclosing blanks with special span
 * @param questionText
 */
export const replaceBlankWithSpecialSpanButKeepContent: (questionText: string) => [string, number] = (questionText: string) => {
    const count = questionText.match(blankRegex)?.length || 0;
    return [questionText.replace(blankRegex, (item) => {
        return `<span class="${DND_BLANK_SPAN_TAG_CLASS}">${item}</span>`
    }), count];
}

export const getContentInsideSpecialSpanNode = (node: any) => {
    return node.children[0].data;
}

const replaceBlankNotationByBlankWithChoices = (questionHTML: string, callbakOnAnswerList?: Function, blankList?: BlankWithOption[]) => {
    const [questionReviewHTML, count] = replaceBlankWithSpecialSpanButKeepContent(questionHTML)
    let blankCounter = 0;

    const parseAnswersInBlank = (question: string) => {
        return question.substring(1, question.length - 1).split(";");
    }
    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if ((node as any).name === "span" && (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS) {
                // const answers = parseAnswersInBlank();
                // console.log("Span node sample: ", node);
                blankCounter++;
                const blankIndex = blankCounter - 1;
                const answerTexts = parseAnswersInBlank(getContentInsideSpecialSpanNode(node));
                const answerList: Choice[] = answerTexts.map((item, index) => {
                    return {
                        content: item,
                        answerKey: index,
                        isCorrect: false,
                        order: index
                    }
                })
                callbakOnAnswerList?.(blankIndex, answerList);
                return <BlankWithChoices hint={""} answerList={answerList} correctAnswerKey={null} index={blankIndex}
                    mode={"create"} />
            }
            return node;
        }
    })
}


const renderChoiceAnswerBox = (questionHTML: string, callbakOnAnswerList?: Function, blankList?: BlankWithOption[]) => {
    const [questionReviewHTML, count] = replaceBlankWithSpecialSpanButKeepContent(questionHTML)
    let blankCounter = 0;

    const parseAnswersInBlank = (question: string) => {
        return question.substring(1, question.length - 1).split(";");
    }
    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if ((node as any).name === "span" && (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS) {
                // const answers = parseAnswersInBlank();
                // console.log("Span node sample: ", node);
                blankCounter++;
                const blankIndex = blankCounter - 1;
                const answerTexts = parseAnswersInBlank(getContentInsideSpecialSpanNode(node));
                const answerList: Choice[] = answerTexts.map((item, index) => {
                    return {
                        content: item,
                        answerKey: index,
                        isCorrect: false
                    }
                })
                console.log("Blank index is : ", blankIndex);
                return <BlankWithChoices hint={""} answerList={answerList}
                    correctAnswerKey={blankList?.[blankIndex].correctAnswerKey}
                    index={blankIndex}
                    mode={'create'} />
            }
            return node;
        }
    })
}


const BlankWithChoicesQuestionDisplay = ({ props }: { props: BlankWithChoicesQuestion }) => {
    // * The splitted string with Input to be inserted in between the elements
    return (
        <>
            <Card>
                {
                    JSON.parse(props?.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                        <AttachmentRender attachment={a} />
                    </Row>)
                }
                <Divider />
                <Row>
                    <Col span={24}>
                        {renderChoiceAnswerBox(props.description, () => {
                        }, props.blankList)}
                    </Col>
                </Row>


                {/*{*/}
                {/*    blanks?.map((blank, index) => {*/}
                {/*        const answers = parseAnswersInBlank(blank);*/}
                {/*        // BeautifulLogger.logFocus(answers);*/}
                {/*        return <Form key={index}>*/}
                {/*            <Form.Item label={*/}
                {/*                <Typography.Text strong>*/}
                {/*                    {`BLANK-${index}`}*/}
                {/*                </Typography.Text>*/}
                {/*            }>*/}
                {/*                <OriginalSelect*/}
                {/*                    placeholder="Hãy chọn đáp án đúng cho ô trống này"*/}
                {/*                    onChange={() => {*/}
                {/*                    }}*/}
                {/*                    allowClear>*/}
                {/*                    {*/}
                {/*                        answers.map(item => {*/}
                {/*                            return <OriginalSelect.Option value={item} key={item}>*/}
                {/*                                {item}*/}
                {/*                            </OriginalSelect.Option>*/}
                {/*                        })*/}
                {/*                    }*/}
                {/*                </OriginalSelect>*/}
                {/*            </Form.Item>*/}
                {/*        </Form>*/}
                {/*    })*/}
                {/*}*/}
            </Card>
        </>
    );
};

export default BlankWithChoicesQuestionDisplay;