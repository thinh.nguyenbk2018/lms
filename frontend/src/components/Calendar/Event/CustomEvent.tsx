import React, {CSSProperties} from "react";
import {CalendarEvent, EventType,} from "../../../redux/features/Calendar/CalendarAPI";
import {Popover, Typography} from "antd";
import EventDetail from "./EventDetail";
import "./CustomEvent.less";
import {adjustBrightness,} from "@utils/Color/ColorUtils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faCircleCheck, faGraduationCap,} from "@fortawesome/free-solid-svg-icons";
import {blue, green, red} from "@ant-design/colors";
import {calculateHourDiff, getEventEndTime, getEventStartTime} from "../Utils/CalendarUtils";

type EventProps = {
    // * The lenth of the hashmap bucked containing the event passed in
    shareNumber: number;
    index: number;
    eventInfo: CalendarEvent;
    dateToRender: Date;
};

const CalendarEventIconByType = React.memo(
    ({eventType}: { eventType: EventType }) => {
        switch (eventType) {
            case "CLASS_SESSION":
                return <FontAwesomeIcon color={"#fff"} icon={faGraduationCap}/>;
            case "QUIZ_DEADLINE":
                return <FontAwesomeIcon color={"#fff"} icon={faCircleCheck}/>;
            case "PERSONAL":
                return <FontAwesomeIcon color={green[4]} icon={faCheck}/>;
            default:
                return <FontAwesomeIcon color={"#fff"} icon={faCircleCheck}/>;
        }
    }
);

const color_boldness = 4;

const EVENT_TYPE_COLOR_MAP: { [P in EventType]: string } = {
    CLASS_SESSION: "#177B63",
    QUIZ_DEADLINE: red[color_boldness],
    PERSONAL: green[color_boldness],
    QUIZ_OPEN: blue[color_boldness]
};

//  * Return the color of the event to display on the calendar
const getEventColorByType: (eventType: EventType) => string = (eventType) => {
    return "";
};

const EventBriefInfo = React.memo((event: CalendarEvent) => {
    return (
        <>
            <div className="event-brief-info__container">
                <div className="event-brief-info__icon">
                    <CalendarEventIconByType eventType={event.eventType}/>
                </div>
                <div className="event-brieft-info__summary">{event.summary}</div>
            </div>
        </>
    );
});
// start: 1:30  + end: 3:00 --> length : 1.5
export const calculateEventLengthToPaint = (event: CalendarEvent): number => {
    const start = getEventStartTime(event);
    const end = getEventEndTime(event);
    return calculateHourDiff(start, end);
};

const CustomEvent = React.memo((props: EventProps) => {
    const sharedColumn = props.shareNumber > 1;
    const [showDetail, setShowDetail] = React.useState(false);
    // ! Red 6 indicating lack of event type
    const colorToPaint =
        EVENT_TYPE_COLOR_MAP[props.eventInfo.eventType] || red[6];
    const {eventInfo: event} = props;

    /*
     *   Render: 7:45
     *   7:00 ---------------
     *           | Offset
     *           | Offset
     *       ||||||||||||||||
     *   8:00 ----------------
     *
     */
    function calculateY_Offset(event: CalendarEvent): CSSProperties {
        const start = getEventStartTime(event);
        // * Offset =  start.getMinutes() / 60 * 100 %
        // * Ex: 7:45 --> 45/60 = 3/4 * 100  = 75 %
        return {
            transform: `translateY(${(start.getMinutes() / 60) * 100})`,
        };
    }

    function calculateX_Offset(event: CalendarEvent): CSSProperties {
        const offset = props.index * (1 / props.shareNumber) * 100;
        return {
            transform: `translateX(${offset}%)`,
        };
    }

    function calculateCssLength(event: CalendarEvent): CSSProperties {
        const lenghtToPaint = calculateEventLengthToPaint(event);
        return {
            height: `${lenghtToPaint * 100}%`,
        };
    }

    function calculateCssWidth(event: CalendarEvent): CSSProperties {
        return {
            maxWidth: sharedColumn ? `${100 / props.shareNumber}%` : "100%",
        };
    }

    function getLeftBorder(): CSSProperties {
        return {
            borderLeft: `8px solid ${adjustBrightness(colorToPaint, -40)}`,
        };
    }

    function calculateCss(event: CalendarEvent): CSSProperties {
        return {
            // overflow: "scroll",
            // ...calculateCssLength(event),
            // ...calculateCssWidth(event),
            // ...calculateY_Offset(event),
            // ...calculateX_Offset(event),
            backgroundColor: colorToPaint,
            ...getLeftBorder(),
        };
    }

    const getTextColor: () => CSSProperties = () => {
        return {
            color: adjustBrightness(colorToPaint, -125),
        };
    };
    const eventContent: () => JSX.Element = () => {
        return (
            <>
                {/*<div className="event-brief-info__container">*/}
                <CalendarEventIconByType eventType={event.eventType}/>
                <Typography.Text
                    strong={true}
                    style={{color: "#fff", width: 150}}
                    ellipsis={true}
                >
                    {/*{`${format(getEventStartTime(event), "HH:mm")} - ${event.summary}`}*/}
                    {`${event.summary}`}
                </Typography.Text>
                {/*</div>*/}
                {/*<br/>*/}
            </>
        );
    };
    // * Wraps with tooltip if shared columns
    const renderEventContent = (content: JSX.Element) => {
        // if (sharedColumn) {
        // 	console.log('shared column: ', sharedColumn);
        // 	return <p>{props.shareNumber}</p>;
        // }
        return (
            <Popover
                content={<EventDetail {...props.eventInfo} />}
                visible={showDetail}
                onVisibleChange={setShowDetail}
            >
                <div className={"event-place-holder"}>{content}</div>
            </Popover>
        );
    };
    return (
        <div style={calculateCss(event)} className={"event-container"}>
            {renderEventContent(eventContent())}
        </div>
    );
});

export default CustomEvent;
