import {green} from "@ant-design/colors";
import {faCircleCheck, faCircleXmark, faPenToSquare,} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Teacher} from "@redux_features/Class/TeacherAPI/ClassTeacherAPI";
import {Space} from "antd";
import {useState} from "react";
import {UserDescription} from "../ProfileCard/ProfileCard";
import "./TeacherCard.less";

const TeacherCard = (
    props: Partial<Teacher> & {
        onEdit?: () => void;
        onDelete?: () => void;
        onClick?: () => void;
        showAction?: boolean;
        // * Indicating wether this card is being displayed inside a modal for class session assignment
        isInModalBox?: boolean;
        // * Additional action when hovering
        additionalActionsOnHover?: JSX.Element;
        teacherRoleTag?: JSX.Element;
    }
) => {
    const [showAdditionalActions, setShowAdditionalActions] = useState(false);
    const handleMouseOver = () => {
        setShowAdditionalActions(true);
    };
    const handleMouseOut = () => {
        setShowAdditionalActions(false);
    };
    // * Teacher data
    const {
        avatar,
        firstName,
        lastName,
        username,
        phone,
        email,
        role,
        additionalActionsOnHover,
        teacherRoleTag,
    } = props;

    // * Handler
    const {
        onEdit,
        onDelete,
        onClick,
        showAction = true,
        isInModalBox = false,
    } = props;
    return (
        <div
            className="teacher-card-container"
            onMouseOver={handleMouseOver}
            onMouseOut={handleMouseOut}
        >
            <div className="card-background">
                {isInModalBox && (
                    <div className="tick-box">
                        <FontAwesomeIcon
                            icon={faCircleCheck}
                            color={green[6]}
                        />
                    </div>
                )}
                {role && teacherRoleTag && <>{teacherRoleTag}</>}
                {additionalActionsOnHover && showAdditionalActions && (
                    <div className="additional-actions-container">
                        {additionalActionsOnHover}
                    </div>
                )}
            </div>
            <img
                onClick={() => {
                    onClick?.();
                }}
                src={avatar}
                className="profile-image"
                alt="Profile image"
            />
            <div className="card-meta">
                <p className="name">{`${lastName} ${firstName}`}</p>
                <p className="description">
                    <UserDescription
                        phone={phone}
                        email={email}
                        description={""}
                    />
                </p>
            </div>
            {showAction && (
                <div className="card-action">
                    <div
                        className="card-action-item card-edit-action"
                        onClick={() => onEdit?.()}
                    >
                        <Space>
                            <FontAwesomeIcon icon={faPenToSquare} />
                            Chỉnh sửa
                        </Space>
                    </div>
                    <div
                        className="card-action-item card-delete-action"
                        onClick={() => onDelete?.()}
                    >
                        <Space>
                            <FontAwesomeIcon icon={faCircleXmark} />
                            Xóa
                        </Space>
                    </div>
                </div>
            )}
        </div>
    );
};

export default TeacherCard;
