import {blue} from "@ant-design/colors";
import {BookTwoTone} from "@ant-design/icons";
import {faCircleCheck, faSquarePollVertical,} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Menu} from "antd";
import React from "react";
import {ActivityType} from "../../typescript/interfaces/courses/CourseTypes";

export const QuizIcon = React.memo(() => (
    <FontAwesomeIcon
        className="anticon"
        style={{
            color: blue.primary,
        }}
        icon={faCircleCheck}
    />
));
export const UnitIcon = React.memo(() => <BookTwoTone/>);

export const VotingIcon = React.memo(() => (
    <FontAwesomeIcon
        className="anticon"
        style={{
            color: blue.primary,
        }}
        icon={faSquarePollVertical}
    />
));
type NewActivityMenuProps = {
    onChoose: (key: ActivityType) => void;
    variation?: "CLASS" | "COURSE";
};

const NewActivityMenu = (props: NewActivityMenuProps) => {
    const {onChoose, variation} = props;
    return (
        <Menu
            className="activity-menu"
            onClick={({key}) => {
                onChoose(key as ActivityType);
            }}
        >
            {/*<PermissionRenderer permissions={[PermissionEnum.CREATE_QUIZ_CLASS]}>*/}
                <Menu.Item
                    className={"activity-menu-item button-with-icon"}
                    key={"quiz"}
                    icon={<QuizIcon/>}
                >
                    Bài kiểm tra
                </Menu.Item>
            {/*</PermissionRenderer>*/}
            {/*<PermissionRenderer permissions={[PermissionEnum.CREATE_UNIT_CLASS]}>*/}
                <Menu.Item
                    className={"activity-menu-item button-with-icon"}
                    key={"unit"}
                    icon={<UnitIcon/>}
                >
                    Bài học
                </Menu.Item>
            {/*</PermissionRenderer>*/}
            {variation && variation === "CLASS" && (
                <>
                    {/*<PermissionRenderer permissions={[PermissionEnum.CREATE_VOTING]}>*/}
                        <Menu.Item
                            className={"activity-menu-item button-with-icon"}
                            key={"vote"}
                            icon={<VotingIcon/>}
                        >
                            Bình chọn
                        </Menu.Item>
                    {/*</PermissionRenderer>*/}
                </>
            )}
        </Menu>
    );
};

export default React.memo(NewActivityMenu);
