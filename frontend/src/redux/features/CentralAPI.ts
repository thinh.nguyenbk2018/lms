import {BaseQueryFn, createApi, FetchArgs, fetchBaseQuery,} from "@reduxjs/toolkit/query/react";
import {API_CONSTANT} from "../../constants/ApiConfigs";
import {tenantUtils} from "../../utils/TenantHelper/TenantUtils";
import LOCAL_STORAGE_KEYS from "../../constants/LocalStorageKey";
import {Mutex} from "async-mutex";
import {authActions} from "./Auth/AuthSlice";
import {FetchBaseQueryError} from "@reduxjs/toolkit/dist/query";
import {SerializedError} from "@reduxjs/toolkit";
import API_ERRORS from "./ApiErrors";
import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";
import ERROR_MESSAGES, {DEFAULT_API_ERROR_MESSAGE} from "./ERROR_MESSAGES";

const mutex = new Mutex();

const getToken = () => {
    return localStorage.getItem(LOCAL_STORAGE_KEYS.TOKEN);
};
const getRefreshToken = () => {
    return localStorage.getItem(LOCAL_STORAGE_KEYS.REFRESH_TOKEN);
};

const baseQuery = fetchBaseQuery({
    baseUrl: API_CONSTANT.BASE_URL,
    // * Return headres
    prepareHeaders(headers) {
        // By default, if we have a token in the store, let's use that for authenticated requests
        // * Use below code to attach the token to the request so the backend can validate the user
        const token = localStorage.getItem(LOCAL_STORAGE_KEYS.TOKEN);
        if (token) {
            headers.set("Authorization", `Bearer ${token}`);
        }
        // set tenantId
        const tenantId = tenantUtils.getTenantIdFromHostname();
        // console.log(`X-TENANT-ID`, tenantId);
        if (tenantId) {
            headers.set(`X-TENANT-ID`, tenantId);
        }
        return headers;
    },
});

function isString(data: any): data is string {
    return typeof data === "string";
}

// * ======= Backend Error Types + Handler + Type Predicates  ===============
export function isApiError(err: any): err is ApiError {
    return (
        isString((err as ApiError).response.data.code) &&
        isString((err as ApiError).response.data.message)
    );
}

/**
 *
 * @param err The error that we received from the backend
 * @param code The Code of the error defined as status code on the backend
 * @param _message The error message that will be toasted on to the UI
 */
export const errorHandlingHelper = (
    err: any,
    code: string,
    _message?: string | undefined | null
) => {
    if (!isApiError(err)) {
        return;
    } else {
        // * Decompose the error to check if the code matches to emit the error message
        const {
            response: {
                data: { code: errorCode, message },
            },
        } = err;
        if (errorCode === code) {
            AntdNotifier.apiError(message);
            return;
        }
        // * Toasting the default error message
        AntdNotifier.apiError(DEFAULT_API_ERROR_MESSAGE);
    }
};

export type ErrorCodeMessageTuple = [string, string];
export const errorArrayHandlingHelper = (
    err: any,
    ...errorCodeArray: Array<ErrorCodeMessageTuple>
) => {
    if (!isApiError(err)) {
        return;
    } else {
        // * Decompose the error to check if the code matches to emit the error message
        const {
            response: {
                data: { code: errorCode },
            },
        } = err;
        for (const codeMessageTuple of errorCodeArray) {
            const [code, messageToAnnounce] = codeMessageTuple;
            if (errorCode === code) {
                AntdNotifier.apiError(messageToAnnounce);
                return;
            }
        }
        // * Toasting the default error message
        AntdNotifier.apiError(DEFAULT_API_ERROR_MESSAGE);
    }
};

export type ApiError = {
    response: {
        data: BackendErrorData;
    };
};

export type BackendErrorData = {
    code: string;
    message: string;
};

function isBackendErrorData(err: any): err is BackendErrorData {
    if (!err) return false;
    return (
        (err as BackendErrorData).code !== undefined &&
        (err as BackendErrorData).message !== undefined
    );
}

// * ========================== End of Backend Error Related stuff ==========

// * ========================== FRONT_END_ERRORS - No information from backend: CORS Error, Server Down, Timeout Request (due to not reaching the server), ..... =============
export interface FrontEndError {
    type: string;
    message: string;
}

export function isFrontEndError(maybeError: any): maybeError is FrontEndError {
    if (!maybeError) return false;
    return (
        isString((maybeError as FrontEndError).message) &&
        isString((maybeError as FrontEndError).type)
    );
}

// * ==== Check if the error due to unfortunate and uncontrollable events:  Server is down, User network is down
// ? Why Check for error like this: https://www.notion.so/Front-End-Notes-8c4cc4779f8d4f9ba95617044866b91f (Section 1.1)
const RTK_NETWORK_ERROR_STATUS = "FETCH_ERROR";
const RTK_NETWORK_ERROR_MESSAGE =
    "TypeError: NetworkError when attempting to fetch resource.";
const RKT_NETWORK_TOAST_MESSAGE =
    "Không thể kết nối serer, hãy thử kiểm tra kết nối của bạn hoặc liên hệ với bộ phận kỹ thuật để được giải quyết";

export function isRtkQueryNetworkError(err: any): boolean {
    if (!err || !isString(err.status) || !isString(err.error)) {
        console.log("rtk error: false");
        return false;
    }
    console.log("rtk error: true");
    const { status, error } = err;
    return (
        status === RTK_NETWORK_ERROR_STATUS && error === RTK_NETWORK_ERROR_MESSAGE
    );
}

type RtkErrorResult = {
    error: {
        error: string;
        status?: string;
    };
    meta: any;
};

export function isRtkErrorResult(err: any): err is RtkErrorResult {
    if (!err) return false;
    return (err as RtkErrorResult).error && (err as RtkErrorResult).meta;
}

// * ========================== END FRONTEND ERRORS =========================

// * ======= TIMEOUT ERROR ================

/**
 * This is the type definition for basequery customization
 * @timeout the duration that we will treat the api request as error if no result or answer found from the backend side
 */
export type BaseQueryExtraOptions = {
    timeout?: number;
};

const DEFAULT_REQUEST_TIMEOUT = 10000;

// * ======= END TIMEOUT ERROR ============
// @ts-ignore
const baseQueryWithReauth: BaseQueryFn<string | FetchArgs,
    unknown,
    FetchBaseQueryError | SerializedError | FrontEndError,
    BaseQueryExtraOptions> = async (args, api, extraOptions) => {
        await mutex.waitForUnlock();
        // * Check for timeout before handling reauth and other stuffs
        let timeout = DEFAULT_REQUEST_TIMEOUT;
        if (extraOptions) {
            timeout = extraOptions.timeout || DEFAULT_REQUEST_TIMEOUT;
        }

        let result = await Promise.race([
            baseQuery(args, api, extraOptions),
            new Promise<FrontEndError>((resolve, _reject) => {
                const timeoutErrorReponse: FrontEndError = {
                    type: API_ERRORS.NETWORK_ERRORS.TIME_OUT,
                    message: ERROR_MESSAGES.API.GENERAL.TIME_OUT,
                };
                setTimeout(
                    () => resolve(timeoutErrorReponse),
                    timeout ?? DEFAULT_REQUEST_TIMEOUT
                );
            }),
        ]);

        // console.log("ERROR IS: ", result);
        // * Network error:
        if (isRtkErrorResult(result)) {
            const { error } = result;
            if (isRtkQueryNetworkError(error)) {
                AntdNotifier.error(RKT_NETWORK_TOAST_MESSAGE);
                return result;
            }
        }

        // * Time out error:
        if (isFrontEndError(result)) {
            const { type } = result;
            switch (type) {
                case API_ERRORS.NETWORK_ERRORS.TIME_OUT:
                    AntdNotifier.apiError(ERROR_MESSAGES.API.GENERAL.TIME_OUT);
                    return result;
                default:
                    return result;
            }
        }
        // * Other general error:  Token Expired , Un Authorized
        // @ts-ignore
        if (result.error && "data" in result.error) {
            // @ts-ignore
            const { data: errData } = result.error;
            if (isBackendErrorData(errData)) {
                // * Stuff inside error data body - provided by backend
                const { code } = errData;
                switch (code) {
                    case API_ERRORS.JWT_EXPIRED:
                        if (!mutex.isLocked()) {
                            const release = await mutex.acquire();

                            try {
                                const refreshResult = await baseQuery(
                                    {
                                        url: "auth/refresh/",
                                        method: "POST",
                                        body: { token: getToken(), refreshToken: getRefreshToken() },
                                    },
                                    api,
                                    extraOptions
                                );
                                if (refreshResult.data) {
                                    // @ts-ignore
                                    api.dispatch(authActions.tokenUpdated(refreshResult.data));
                                    result = await baseQuery(args, api, extraOptions);
                                } else {
                                    // api.dispatch(authActions.cleanUpOnSignOut());
                                }
                            } finally {
                                release();
                            }
                        } else {
                            await mutex.waitForUnlock();
                            result = await baseQuery(args, api, extraOptions);
                        }
                        break;
                    case API_ERRORS.UNAUTHORIZED:
                        window.location.replace(ROUTING_CONSTANTS.UNAUTHORIZED);
                        break;
                    default:
                        break;
                }
            }
        }

        return result;
    };

export interface ListResponse<T> {
    data: T[];
}

export const apiSlice = createApi({
    reducerPath: "api",
    baseQuery: baseQueryWithReauth,
    endpoints: () => ({}),
});

// * Force refetch with dispatch (Not recommended)
// * https://stackoverflow.com/questions/69402541/how-update-state-in-rtk-query-by-useeffect
//
//
// * refetch
// * https://redux-toolkit.js.org/rtk-query/usage/cache-behavior#manipulating-cache-behavior
//
// * Manual Cache update - Pessimistic vs Optimistic
// * https://redux-toolkit.js.org/rtk-query/usage/manual-cache-updates
//
// * RTK Query is for calling api and data caching only --> Cannot dispatch action to change the data locally
// * https://stackoverflow.com/questions/68753347/process-fetched-data-rtk-query-redux-toolkit-react
