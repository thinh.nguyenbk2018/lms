"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.changeCriteria = exports.classStudentSelector = exports.classStudentSlice = exports.CLASS_STUDENT_SLICE_KEY = void 0;
var createSliceEnhancedWithSearch_1 = require("../../../utils/createSliceWithSearch/createSliceEnhancedWithSearch");
exports.CLASS_STUDENT_SLICE_KEY = "classStudent";
var initialState = __assign({}, createSliceEnhancedWithSearch_1.generateInitialSearchSlice());
exports.classStudentSlice = createSliceEnhancedWithSearch_1.createSliceEnhancedWithSearch({
    name: exports.CLASS_STUDENT_SLICE_KEY,
    initialState: initialState,
    reducers: {}
});
exports.classStudentSelector = {
    // * Select the criteria to pass into the query
    selectSearchCriteria: function (state) {
        return state[exports.CLASS_STUDENT_SLICE_KEY].searchCriteria;
    }
};
exports.changeCriteria = exports.classStudentSlice.actions.changeCriteria;
