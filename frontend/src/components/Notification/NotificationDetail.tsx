import { Avatar, Button, Card, Col, Row, Space, Tag, Typography } from "antd";
import { formatDistanceToNow } from "date-fns";
import viLocale from "date-fns/locale/vi";
import { PresetColorTypes } from "antd/lib/_util/colors";
import { Link, useNavigate, useParams } from "react-router-dom";
import LoadingPage from "../../pages/UtilPages/LoadingPage";
import { addIconSvg, backIconSvg, updateIconSvg } from "../Icons/SvgIcons";
import ContentHeader from "../ContentHeader/ContentHeader";
import { ROUTING_CONSTANTS } from "../../navigation/ROUTING_CONSTANTS";
import useClassSidebar from "../../hooks/Routing/useClassSidebar";
import { zonedTimeToUtc } from "date-fns-tz";
import { useGetNotificationDetailQuery } from "@redux/features/Announcement/AnnouncementAPI";
import BackButton from "../ActionButton/BackButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import "./AnnouncementList.less";

const NotificationDetail = () => {
	const [closeSidebar] = useClassSidebar();
	const navigate = useNavigate();
	const { notificationId, classId } = useParams();

	const isSkip = (idParam: string | number | undefined) => {
		return Number(idParam) === -1 || Number.isNaN(Number(idParam));
	};

	const { data: notification, isFetching } = useGetNotificationDetailQuery(
		Number(notificationId),
		{ skip: isSkip(notificationId) }
	);

	return (
		<>
			<ContentHeader
				additionalStyle={{ alignItems: "center" }}
				title={"Chi tiết thông báo"}
				action={
					<Space>
						<BackButton
							onClick={() => {
								navigate(-1);
							}}
						/>
					</Space>
				}
			/>
			{isFetching ? (
				<LoadingPage />
			) : (
				<Card className={"gray-background notification-detail-container"}>
					<Row>
						<Row className="subject-and-action-container">
							<Space align="center">
								<p className="subject-title"> {notification?.subject} </p>
								<Space>
									{notification?.tags.map((item) => (
										<Tag
											color={
												PresetColorTypes[
													Math.floor(
														Math.random() * (PresetColorTypes.length - 1)
													)
												]
											}
										>
											{item}
										</Tag>
									))}
								</Space>
							</Space>
							<Space>
								<div
									className="card-action-item card-edit-action"
									onClick={() => {}}
								>
									<Space>
										<FontAwesomeIcon icon={faCheck} />
										Đã đọc
									</Space>
								</div>
								<div
									className="card-action-item card-delete-action"
									onClick={() => {}}
								>
									<Space>
										<FontAwesomeIcon icon={faCircleXmark} />
										Xóa
									</Space>
								</div>
							</Space>
						</Row>
						<Row className="sender-container">
							<Space>
								<p className="sender-label"> From: </p>
								<div className="sender-container-card">
									<Avatar src={notification?.image} size={"large"} />
									<p className="sender-name">{notification?.senderName} </p>
								</div>
							</Space>

							<Space>
								<Typography.Text italic={true}>
									{formatDistanceToNow(
										zonedTimeToUtc(
											notification?.sentAt || new Date(),
											"Asia/Ho_Chi_Minh"
										),
										{
											addSuffix: true,
											locale: viLocale,
										}
									)}
								</Typography.Text>
							</Space>
						</Row>
						<Col span={22}>
							<Row>
								<div
									dangerouslySetInnerHTML={{
										__html: notification?.content || "",
									}}
								/>
							</Row>
							<Row>
								<Typography.Paragraph>
									{notification?.attachment}
								</Typography.Paragraph>
							</Row>
						</Col>
					</Row>
				</Card>
			)}
		</>
	);
};

export default NotificationDetail;
