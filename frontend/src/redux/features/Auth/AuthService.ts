import request from "../../../utils/RequestHelper";
import {LoginInfo, RegisterInfo} from "./AuthSlice";

const API_ENDPOINTS = {
    LOGIN: "/login",
    REGISTER: "/register",
}

export const authService = (() => {

    const login = async (payload:LoginInfo) => {
        return request.postAsync(API_ENDPOINTS.LOGIN, payload);
    }

    const register = async (payload:RegisterInfo) => {
        return request.postAsync(API_ENDPOINTS.REGISTER,payload);
    }

    const resetPassword = async () => {
        return request.getAsync(API_ENDPOINTS.REGISTER);
    }

    const changePassword = async () => {
        return request.getAsync(API_ENDPOINTS.REGISTER);
    }

    return {login, register,resetPassword, changePassword};
})();


