import {PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../store/store";
import {CourseItem} from "../Course/CourseAPI";
import {
    createSliceEnhancedWithSearch,
    EnhancedSliceWithSearch,
    generateInitialSearchSlice,
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";


export interface CourseInProgram {
    courses: CourseItem[],
}



const PROGRAM_SLICE_KEY = "program"

// * When create initial state: Put your state type in the EnhancedSliceWithSearch<YOUR_STATE_TYPE_HERE>
// *        --> searchCriteriaWillBe attached.
const initialState : EnhancedSliceWithSearch<CourseInProgram> = {
    courses : [],
    ...generateInitialSearchSlice()
}

/*
    ! IMPORTTANT: Please read :)
    * createSlice --> createSliceEnhancedWithSearch
    * This slice is enhanced with search:
    *   - searchCriteria is the state stored (Create the selector to select this)
    *   - changeCriteria is the action to change the criteria
 */
export const programSlice = createSliceEnhancedWithSearch({
    name: PROGRAM_SLICE_KEY,
    initialState,
    reducers: {
        setCoursesInProgram: (state, action: PayloadAction<CourseItem[]>) => {
            state.courses = action.payload;
        },
    },
});

export const programSelector = {
    selectCoursesInProgram: (state: RootState) => state.program.courses,
    // * Select the criteria to pass into the query
    selectSearchCriteria: (state: RootState) => state.program.searchCriteria
}

export const {setCoursesInProgram, changeCriteria} = programSlice.actions;
