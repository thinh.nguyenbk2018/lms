import {apiSlice} from "../CentralAPI";
import {UserDto} from "./UserType";

const userSearchApiWithInvalidateTags = apiSlice.enhanceEndpoints({addTagTypes: ['User']});
const userSearchAPI = userSearchApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        getUsers: build.query<UserDto[], void>({
            query: () => 'users',
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.map(({id}) => ({type: 'User', id} as const)),
                        {type: 'User', id: 'LIST'},
                    ]
                    :
                    [{type: 'User', id: 'LIST'}],
        })
    }),
    overrideExisting: false
})


export const {
    useGetUsersQuery
} = userSearchAPI;


// * Provide tags for cache invalidation when code splitting
// * https://github.com/reduxjs/redux-toolkit/issues/1510

