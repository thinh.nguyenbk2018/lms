enum FormType {
    UNIT = "unit",
    QUIZ = "quiz",
}

export default FormType;