import { MenuOutlined } from "@ant-design/icons";
import { Button, Drawer, Image, Layout, Menu, Space, Typography } from "antd";
import { Content, Header } from "antd/lib/layout/layout";
import { useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import logo from "@assets/logo/logo.png";
import LOCAL_STORAGE_KEYS from "../../../constants/LocalStorageKey";
import { HOST_ROUTING_CONSTANT } from "../../../navigation/ROUTING_CONSTANTS";
import './HostLayout.less';


const HostLayout = () => {
  const [visibleDrawMenu, setVisibleDrawMenu] = useState<boolean>(false);
  const navigate = useNavigate();

  const logout = ()=>{
    localStorage.removeItem(LOCAL_STORAGE_KEYS.TOKEN);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.USER_AUTH);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.TENANT);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.REFRESH_TOKEN);
    navigate(HOST_ROUTING_CONSTANT.ROOT)
  }

  const menuItems = () => (<>
    <Menu.Item key="1">Bảng giá</Menu.Item>
    <Menu.Item key="2">Tài liệu</Menu.Item>
    <Menu.Item key="3">Liên hệ</Menu.Item>
    {
      !localStorage.getItem(LOCAL_STORAGE_KEYS.TOKEN) ? <>
        <Menu.Item key="4">
          <Link to={HOST_ROUTING_CONSTANT.LOGIN}>
            Đăng nhập
          </Link>
        </Menu.Item>
        <Menu.Item key="5">
          <Link to={HOST_ROUTING_CONSTANT.REGISTER}>
            <Button type="primary">Đăng ký</Button>
          </Link>
        </Menu.Item>
      </> : <>
        <Menu.Item key="4">
          <Link to={HOST_ROUTING_CONSTANT.DASHBOARD}>
            <Button>
              Dashboad
            </Button>
          </Link>
        </Menu.Item>
        <Menu.Item key="5">
          <Button type="primary" onClick={logout}>Đăng xuất</Button>
        </Menu.Item>
      </>
    }
  </>);

  return <Layout>
    <Header className="header">
      <Link to={HOST_ROUTING_CONSTANT.ROOT}>
        <Space align="end" style={{ margin: "0px 20px" }}>
          <Image src={logo} preview={false} height={50} />
          <Typography.Title level={3} style={{ marginLeft: 5 }}>BKLMS</Typography.Title>
        </Space>
      </Link>
      <Menu theme="light" mode="horizontal" className="menu" >
        {menuItems()}
      </Menu>
      <Space className="menu-btn">
        <Button type="text" style={{ marginTop: 18 }} onClick={() => setVisibleDrawMenu(true)}>
          <MenuOutlined style={{ fontSize: 25, color: "#1890ff" }} />
        </Button>
      </Space>
    </Header>
    <Content className="site-layout" style={{ padding: '0 50px', marginTop: 10 }}>
      <Outlet />
    </Content>
    {/* <Footer style={{ textAlign: 'center', marginTop: 10 }}>Đồ án nhóm @GR22</Footer> */}
    <Drawer
      title="Menu"
      placement="right"
      onClose={() => setVisibleDrawMenu(false)}
      visible={visibleDrawMenu}
    >
      <Menu theme="light" mode="vertical" style={{ border: "none" }}>
        {menuItems()}
      </Menu>
    </Drawer>
  </Layout >
}

export default HostLayout;