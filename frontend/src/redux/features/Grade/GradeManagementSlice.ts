import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "../../store/store";

const GRADE_MANAGEMENT_SLICE_KEY = "gradeResult";

const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice()
}

export const gradeManagementSlice = createSliceEnhancedWithSearch({
    name: GRADE_MANAGEMENT_SLICE_KEY,
    initialState,
    reducers: {

    },
});

export const gradeManagementSelector = {
    selectSearchCriteria: (state: RootState) => state.gradeManagement.searchCriteria
}

export const {changeCriteria} = gradeManagementSlice.actions;