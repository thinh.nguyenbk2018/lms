import {Button, Select, Space, Typography} from "antd";
import React, {useState} from "react";
import {skipToken} from "@reduxjs/toolkit/query";
import "./CalendarDayDetail.less";
import {LeftOutlined, RightOutlined} from "@ant-design/icons";
import {Dropdown} from "antd/es";
import {useGetPersonalCalendarQuery} from "../../../../../redux/features/Calendar/CalendarAPI";
import {useAppDispatch, useAppSelector,} from "../../../../../hooks/Redux/hooks";
import {
    calendarActions,
    calendarSelector,
    EventHashMap,
    getFromMap,
    mapHasKey,
    prepareKey,
} from "../../../../../redux/features/Calendar/CalendarSlice";
import {authSelectors} from "../../../../../redux/features/Auth/AuthSlice";
import {getDaysName, getEventStartTime, getMonthName,} from "../../../Utils/CalendarUtils";
import {classTimeTableActions} from "../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableSlice";
import CalendarNewEventFormModal from "../../../Forms/CalendarNewEventFormModal";
import AddNewEventMenu from "../NewEventMenu/NewEventMenu";
import CustomEvent from "../../../Event/CustomEvent";
import {dateHours} from "../CalendarDetail";

const {Option} = Select;
const CalendarDayDetail = () => {
    const selectedDate = useAppSelector(calendarSelector.selectCurrentDate);
    const weekToFocus = useAppSelector(calendarSelector.selectCurrentWeek);
    const dispatch = useAppDispatch();

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);
    const userID = userInfo?.id;

    const [newEventModalVisible, setNewEventModalVisible] = useState(false);

    const {
        data: eventMap = {},
        isSuccess,
        isError,
        isFetching,
        error,
    } = useGetPersonalCalendarQuery(userID ?? skipToken);
    const getCalendarTitleText: () => string = () => {
        if (!weekToFocus) {
            return "";
        }
        const firstDateOfWeek = weekToFocus[0];
        const lastDateOfWeek = weekToFocus[6];
        if (firstDateOfWeek.getMonth() === lastDateOfWeek.getMonth()) {
            return `${getMonthName(
                firstDateOfWeek
            )}, ${firstDateOfWeek.getDate()} - ${lastDateOfWeek.getDate()}`;
        } else {
            return `${getMonthName(
                firstDateOfWeek
            )}, ${firstDateOfWeek.getDate()} - ${getMonthName(
                lastDateOfWeek
            )}, ${lastDateOfWeek.getDate()}`;
        }
    };
    // * On add event handlers:
    const onAddClassSchedule = () => {
        dispatch(classTimeTableActions.addNewClassSchedule());
    };
    const onAddClassSession = () => {
        dispatch(classTimeTableActions.addNewClassSession());
    };

    const onAddPersonalEvent = () => {
        dispatch(classTimeTableActions.addNewPersonalEvetn());
    };

    const getSelectedDateClass = (date: Date) => {
        return date.getDate() === selectedDate?.getDate() &&
        date.getMonth() === selectedDate.getMonth()
            ? "selected-date-title"
            : "";
    };

    // * Event [] =>
    const pullEventsAndPaint = (
        dateToRender: Date,
        hourTitle: string,
        eventMap: EventHashMap
    ) => {
        const key = prepareKey(dateToRender, hourTitle);
        if (!mapHasKey(eventMap, key)) {
            return null;
        }
        const eventList = getFromMap(eventMap, key)!!;
        if (eventList.length !== 0) {
            return (
                <>
                    {eventList.map((item, index) => {
                        return (
                            <CustomEvent
                                shareNumber={eventList.length}
                                index={index}
                                eventInfo={item}
                                dateToRender={dateToRender}
                                key={item.id.concat(getEventStartTime(item).toString())}
                            />
                        );
                    })}
                </>
            );
        }
        return null;
    };

    return (
        <>
            <div className={"calendar-detail-container"}>
                <div className={"calendar-control-header"}>
                    <div className={"calendar-date-name"}>
                        <Typography.Title level={1}>
                            {getCalendarTitleText()}
                        </Typography.Title>
                        <div className={"button-nav-container"}>
                            <Space>
                                <Button
                                    icon={<LeftOutlined/>}
                                    onClick={() => dispatch(calendarActions.decreaseDate())}
                                >
                                    Backward
                                </Button>
                                <Button
                                    onClick={() => dispatch(calendarActions.increaseDate())}
                                >
                                    <Space>
                                        Forward
                                        <RightOutlined/>
                                    </Space>
                                </Button>
                            </Space>
                        </div>
                        1
                    </div>
                    <div className={"calendar-control"}>
                        <div className={"calendar-mode-container"}>
                            <Select defaultValue={"week"}>
                                <Option value={"week"}>Week</Option>
                                <Option value={"day"}>Day</Option>
                            </Select>
                        </div>
                        <CalendarNewEventFormModal/>
                        <div className={"calendar-add-button"} onClick={() => {
                        }}>
                            <Dropdown
                                overlay={
                                    <AddNewEventMenu
                                        onSelect={(key) => {
                                            switch (key) {
                                                case "CLASS_SESSION":
                                                    onAddClassSession();
                                                    return;
                                                case "PERSONAL":
                                                    onAddPersonalEvent();
                                                    return;
                                                case "CLASS_SCHEDULE":
                                                    onAddClassSchedule();
                                                    return;
                                                default:
                                                    return;
                                            }
                                        }}
                                    />
                                }
                            >
                                <Button type={"primary"}>Thêm một sự kiện</Button>
                            </Dropdown>
                        </div>
                    </div>
                </div>
                <div className={"calendar-detail-date-title-header"}>
                    {weekToFocus.map((date) => (
                        <div
                            className={`calendar-detail-date-info-box ${getSelectedDateClass(
                                date
                            )}`}
                        >
                            <div className={"calendar-detail-date-of-week-number"}>
                                {date.getDate()}
                            </div>
                            <div className={"calendar-detail-date-of-week-name"}>
                                {getDaysName(date)}
                            </div>
                        </div>
                    ))}
                </div>
                <div className={"calendar-detail-main"}>
                    <div className={"date-hour-column"}>
                        {dateHours.map((hourTitle) => {
                            return (
                                <div className={"hour-title"}>
                                    <Typography.Text strong>{hourTitle}</Typography.Text>
                                </div>
                            );
                        })}
                    </div>
                    <div
                        className={"calendar-detail-with-events"}
                        onScroll={(e) => {
                            console.log("Scroll: ", e);
                        }}
                    >
                        {dateHours.map((hourTitle) => {
                            return (
                                <>
                                    {new Array(7).fill(0).map((item, index) => {
                                        return (
                                            <div className={"date-hour-detail"}>
                                                {pullEventsAndPaint(
                                                    weekToFocus[index],
                                                    hourTitle,
                                                    eventMap
                                                )}
                                            </div>
                                        );
                                    })}
                                </>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
};
export default CalendarDayDetail;
