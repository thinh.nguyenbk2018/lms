import { Button, Space } from "antd";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import { useNavigate } from "react-router-dom";
import { useRef } from "react";
import RoleForm, { CreateRoleFormHandle } from "../../../components/Authorization/Role/RoleForm";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import { CreateRoleResponse } from "../../../redux/features/Permission/RolesAPI";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";

const CreateRolePage = () => {

    let navigate = useNavigate();

    let createRoleFormRef = useRef<CreateRoleFormHandle>(null)

    const onCancel = () => {
        navigate(ROUTING_CONSTANTS.ROLES_MANAGEMENT);
    }

    const onDone = () => {
        createRoleFormRef.current?.submitCreateForm()
            .then((result: any) => {
                if ("data" in result) {
                    AntdNotifier.success("Thêm mới vai trò thành công");
                    navigate(ROUTING_CONSTANTS.ROLES_MANAGEMENT)
                } else {
                    AntdNotifier.error("Có lỗi xảy ra trong quá trình lưu, thử lại sau");
                    console.log(result);
                }
            })
            .catch((error: any) => {

            })
    }

    return <>
        <ContentHeader
            title="Tạo vai trò mới"
            action={
                <Space>
                    <Button
                        danger
                        type="primary"
                        color="#73d13d"
                        onClick={onCancel}
                    >
                        Hủy
                    </Button>
                    <Button
                        type="primary"
                        color="#73d13d"
                        onClick={onDone}
                    >
                        Xong
                    </Button>
                </Space>
            }
        />
        <RoleForm ref={createRoleFormRef} />
    </>
}


export default CreateRolePage;