import "./TestPage.less";
import { useEffect, useState } from "react";
import TodoComponent, { Todo } from "../../../components/Todo/TodoComponent";
import TodoList from "@components/Todo/TodoList";
import ModalWithSection from "@components/GenericComponents/Modals/ModalWithSections";
import UserInfoNavigateOnClick from "../../../components/UserInfoNavigateOnClick/UserInfoNavigateOnClick";
import { Button } from "antd";
import { useAppDispatch } from "../../../hooks/Redux/hooks";
import { useAppSelector } from "@hooks/Redux/hooks";
import {
    classTeacherActions,
    classTeacherSelectors,
} from "../../../redux/features/Class/TeacherAPI/ClassTeacherSlice";
import NewTeacherFromStaffModal from "@components/Teacher/NewTeacherFromStaffModal/NewTeacherFromStaffModal";
import TeacherRoleMenu from "@components/Teacher/TeacherRoleMenu";
import UploadForm from "@components/UploadForm/UploadForm";
import UploadFileIconTestSuite from "@pages/Tenant/PlayGround/UploadFileIconTestSuite";

const todo: Todo = {
    title: "Task 1",
    content: "Please do task 1 carefully before coding real hard",
};

const todoList: Todo[] = [
    {
        title: "Task 1",
        content: "Task 1 detail ssaf  asdfkdjfi as ",
    },
    {
        title: "task 2 ",
        content: "Please finish task 1 before doing task 2",
    },
];
const TestPage = () => {
    const dispatch = useAppDispatch();
    return (
        <div>
            <UploadFileIconTestSuite />
        </div>
    );
};

export default TestPage;
