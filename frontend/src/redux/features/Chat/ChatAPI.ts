import { ID, PaginatedRequest, PaginatedResponse } from '../../interfaces/types';
import { User } from '../Auth/AuthSlice';
import { apiSlice } from '../CentralAPI';

export interface Message {
    chatRoomId: ID,
    senderId: ID,
    receiverId?: ID,
    content: string,
    createdAt?: number,
}

// * Used in ConversationPopOver Component ==> Represent 1 chat room
export interface BriefChatInfo {
    chatRoomId: ID,
    chatRoomName: string, // * User: FirstName + LastName if type === 'USER'
    image: string,
    lastMessage: Message,
    numOfUnseenMessages: number,
    type: ChatGroupType,
}

// * Each active chat is a bubble / conversation box at the bottom right
// * Correspond to the Gruop entity in the back end
export interface ActiveChat {
    readonly chatRoomId?: ID,
    chatRoomName?: string,
    image?: string,
    readonly messages: PaginatedResponse<Message>,
    readonly members: User[],
    readonly adminID?: ID, // *
    numberOfUnseenMessage: number,
}

type ChatGroupType = "USER" | "GROUP";
export interface CreateChatRoomRequest {
    usersId: number[],
    name: string,
    type: ChatGroupType
}

export interface BriefChatListResponse {
    listData: BriefChatInfo[];
}

export type SendingMessageDto = Message;

const chatApiWithInvalidateTags = apiSlice.enhanceEndpoints({ addTagTypes: ['Chat'] });



const chatAPI = chatApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => {
        return ({
            // * Get list of brief information of the chat rooms
            getChatsBriefInfo: build.query<BriefChatListResponse, void>({
                query: () => 'chat',
                providesTags: [{ type: 'Chat', id: 'LIST' }]
            }),
            // * Get all the message for the specific chat gruop
            // * A chat GROUP should contain at least 2 member (2 is still OK)
            getActiveChatMessages: build.query<ActiveChat, PaginatedRequest>({
                query: (args) => `chat/${args.id}?page=${args.page}&size=${args.size}`,
                async onCacheEntryAdded(
                    _arg,
                    { cacheDataLoaded, cacheEntryRemoved
                    }
                ) {
                    // * In our case, the socket connection is persisted throughout each session
                    // TODO: Change the line below to get the persisted connection instead of initiating
                    try {
                        // wait for the initial query to resolve before proceeding
                        await cacheDataLoaded
                        // when data is received from the socket connection to the server,
                        // if it is a message and for the appropriate channel,
                        // update our query result with the received message
                        // * We should receive the subscription for ease of subscription
                        //!! subscription = StompUtils.addListenerToTopic(`chat/${arg.id}`, listener);
                    } catch {
                        // no-op in case `cacheEntryRemoved` resolves before `cacheDataLoaded`,
                        // in which case `cacheDataLoaded` will throw
                    }
                    // cacheEntryRemoved will resolve when the cache subscription is no longer active
                    await cacheEntryRemoved
                    // perform cleanup steps once the `cacheEntryRemoved` promise resolves
                    // * In our case, the socket connection is persisted throughout each session
                    // * So, we only need to remove the listener
                    //!! subscription?.unsubscribe();
                }
            }),

            createChatRoom: build.mutation<string, CreateChatRoomRequest>({
                invalidatesTags: (_result, _err) => [{ type: 'Chat', id: 'LIST' }],
                query(body) {
                    return {
                        url: `chat/room`,
                        method: 'POST',
                        body
                    }
                },
            }),

            checkRoomAsSeen: build.mutation<string, number>(
                {
                    invalidatesTags: (_result, _err) => [{ type: 'Chat', id: 'LIST' }],
                    query(chatRoomId) {
                        return {
                            url: `chat/seen/${chatRoomId}`,
                            method: 'POST'
                        }
                    },
                }
            ),

            // * id: Chat room id, message :
            addNewMessageToChatRoom: build.mutation<string, SendingMessageDto>({
                invalidatesTags: (_result, _err, args) => [{ type: 'Chat', id: 'LIST' }, {
                    type: 'Chat',
                    id: args.chatRoomId
                }],
                query(body) {
                    return {
                        url: `chat/send`,
                        method: 'POST',
                        body
                    }
                },
            }),

            testChatRoomExist: build.mutation<ActiveChat, { users: ID[] }>(
                {
                    invalidatesTags: (_result, _err, _arg) => [{ type: 'Chat', id: 'LIST' }],
                    query(body) {
                        return {
                            url: `/chat/test`,
                            method: `POST`,
                            body
                        }
                    }
                }
            ),


            // ! Below methods are not implemented --> Don't test these yet
            updateChatRoom: build.mutation<string, { id: ID, name: string }>({
                invalidatesTags: (_result, _err, arg) => [{ type: 'Chat', id: arg.id }, { type: 'Chat', id: 'LIST' }],
                query(body) {
                    return {
                        url: `chat/room`,
                        method: 'PATCH',
                        body
                    }
                },
            }),
            deleteChatRoom: build.mutation<string, ID>({
                invalidatesTags: (_result, _err, id) => [{ type: 'Chat', id }, { type: 'Chat', id: 'LIST' }],
                query(body) {
                    return {
                        url: `chat/room`,
                        method: 'DELETE',
                        body
                    }
                }
            })
        });
    },
    overrideExisting: false
});

export const {
    useGetChatsBriefInfoQuery, useLazyGetChatsBriefInfoQuery, useAddNewMessageToChatRoomMutation, useCreateChatRoomMutation
    , useCheckRoomAsSeenMutation, useUpdateChatRoomMutation, useDeleteChatRoomMutation, useGetActiveChatMessagesQuery
    , useTestChatRoomExistMutation
} = chatAPI;
// * Provide tags for cache invalidation when code splitting
// * https://github.com/reduxjs/redux-toolkit/issues/1510

