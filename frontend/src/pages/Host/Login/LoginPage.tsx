import {Button, Card, Form, Image, Input, Row, Space, Typography} from "antd";
import {useNavigate} from "react-router-dom";
import logo from "@assets/logo/logo.png";
import LOCAL_STORAGE_KEYS from "../../../constants/LocalStorageKey";
import {HOST_ROUTING_CONSTANT} from "../../../navigation/ROUTING_CONSTANTS";
import {useLoginHostMutation} from "../../../redux/features/Host/Auth/AuthAPI";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
import './LoginPage.less';

const LoginPage = () => {
  const [login, { isLoading }] = useLoginHostMutation();
  const navigate = useNavigate();

  const onFinish = (values: { username: string, password: string }) => {
    login(values).unwrap()
      .then(response => {
        console.log(response);
        localStorage.setItem(LOCAL_STORAGE_KEYS.TOKEN, response.token!);
        localStorage.setItem(LOCAL_STORAGE_KEYS.USER_AUTH, JSON.stringify(response));
        localStorage.setItem(LOCAL_STORAGE_KEYS.TENANT, response.tenant!);
        localStorage.setItem(LOCAL_STORAGE_KEYS.REFRESH_TOKEN, response.refreshToken);
        navigate(HOST_ROUTING_CONSTANT.SETTINGS)
      })
      .catch(error => {
        console.log(error);
        AntdNotifier.error(error.data.message)
      })
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return <Row justify={"center"} align={"middle"}>
    <Card >
      <Space direction={"vertical"} align={"center"} style={{ width: 500 }}>
        <Image width={200} height={200} src={logo} preview={false} />
        <Typography.Title>
          BKLMS
        </Typography.Title>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}

          initialValues={{
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >

          <Form.Item
            label="Tên đăng nhập"
            name="username"
            rules={[
              {
                required: true,
                message: 'Tên đăng nhập là thông tin bắt buộc',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[
              {
                required: true,
                message: 'Mật khẩu là thông tin bắt buộc',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
            style={{display: "flex", justifyContent: "flex-end"}}
            className={'submit-tenant-btn'}
          >
            <Button type="primary" htmlType="submit" loading={isLoading} >
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Card>
  </Row>
}

export default LoginPage;