import React, { CSSProperties, useState } from "react";
import { FieldProps } from "formik";
import { FormItem } from "formik-antd";
import { Typography } from "antd";
import useClassCourseInformation from "../../../hooks/Routing/useClassCourseInformation";
import { useParams } from "react-router-dom";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import TeacherSearchSelectListModal from "../TeacherSearchSelectListModal/TeacherSearchSelectListModal";
import { useGetTeacherForClassQuery } from "../../../redux/features/Class/TeacherAPI/ClassTeacherAPI";

type SearchSelectFieldProps = FieldProps<any> & {
    label?: string;
    style?: CSSProperties;
    required?: boolean;
    onSelect?: () => void;
    showErrorMessage?: boolean;
    trigger?: JSX.Element;
};

const TeacherSearchSelectListField = ({
    field,
    form: {
        touched,
        errors,
        setFieldValue,
        setFieldError,
        handleBlur,
        handleChange,
        setTouched,
        setFieldTouched,
    },
    ...otherProps
}: SearchSelectFieldProps) => {
    const { name, value, onChange, onBlur } = field;
    const errorMsg = touched[name] && errors[name];

    const { classId } = useParams();

    const [visible, setVisible] = useState(false);

    const {
        classId: classIdAsNumber,
        courseId,
        classInfo,
        isFetchingClass,
        isSuccessClass,
        isErrorClass
    } = useClassCourseInformation(Number(classId));
    const {
        data: teacherList,
        isSuccess,
        isLoading,
        isError,
        isUninitialized,
        isFetching,
    } = useGetTeacherForClassQuery(
        { classId: Number(classId) },
        {
            skip: isNaN(Number(classId)),
        }
    );
    if (isNaN(Number(classId)) || !isSuccessClass) {
        return null;
    } else if (isSuccessClass && isSuccess) {
        return (
            <>
                <FormItem
                    required={otherProps?.required}
                    label={otherProps?.label || ""}
                    name={name}
                >
                    <span
                        onClick={() => {
                            setVisible(true);
                        }}
                    >
                        {otherProps.trigger}
                    </span>
                    <TeacherSearchSelectListModal
                        data={teacherList.listData || []}
                        onDoneSelectList={(ids) => {
                            setFieldValue(name, ids);
                            setVisible(false);
                        }}
                    />
                    <Typography.Text type={"danger"}>
                        <>
                            {errorMsg}
                        </>
                    </Typography.Text>
                </FormItem>
            </>
        );
    }
    return <></>;
};

export default TeacherSearchSelectListField;
