import { Button, Col, Divider, Input, Row, Spin } from "antd";
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks/Redux/hooks";
import { gradeFormulaSeletor, Tag, updateTags } from "../../../redux/features/Grade/GradeFormulaSlice";

const { Search } = Input;

const TagsSelector = ({ allTags, isGettingTags }: { allTags: Tag[], isGettingTags: boolean }) => {
  const [keyword, setKeyword] = useState<string>("");
  const tags = useAppSelector(gradeFormulaSeletor.tagsSelector);

  const dispatch = useAppDispatch();

  const addTag = (tag: Tag) => dispatch(updateTags([...tags, tag]));
  const moveOutTag = (tag: Tag) => dispatch(updateTags(tags.filter(t => t.id !== tag.id)));

  return <>
    <Row justify="end" >
      <Search placeholder="Tìm bài kiểm tra" style={{ width: 200, marginRight: 25, marginBottom: 10 }} onChange={({ currentTarget: { value } }) => setKeyword(value)} />
    </Row>
    <Spin spinning={isGettingTags}>
      <Row justify="center" style={{ border: "1px solid rgb(207, 207, 207)" }}>
        <Col span={11} >
          <Row justify="center" style={{ padding: 5 }}>
            {
              allTags && allTags.filter(tag => tag.title.toLowerCase().includes(keyword.toLowerCase()) && !tags.map(t => t.id).includes(tag.id)).map((tag, index) => (
                <Button onClick={() => addTag(tag)} style={{ margin: 2 }} key={index}>
                  {tag.title}
                </Button>
              ))
            }
          </Row>
        </Col>
        <Divider style={{ minHeight: 350, width: 1, backgroundColor: "rgb(207, 207, 207)" }} type="vertical" />
        <Col span={11}>
          <Row justify="center" style={{ padding: 5 }}>
            {
              tags.filter(tag => tag.title.toLowerCase().includes(keyword.toLowerCase())).map(tag => (
                <Button type="primary" onClick={() => moveOutTag(tag)} style={{ margin: 2 }}>
                  {tag.title}
                </Button>
              ))
            }
          </Row>
        </Col>
      </Row>
    </Spin>
  </>
}

export default TagsSelector;