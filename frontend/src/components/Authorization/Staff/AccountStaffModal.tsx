import {Card, Col, Form, Input, Modal, Row} from "antd"
import {Staff, useAllocateAccountMutation} from "../../../redux/features/Staff/StaffAPI";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

export interface AccountStaffModalProps {
    staff: Staff,
    visible: boolean,
    setVisible: (visible: boolean) => void
}

const AccountStaffModal = ({staff, visible, setVisible}: AccountStaffModalProps) => {
    let [form] = Form.useForm();

    let [allocateAccount] = useAllocateAccountMutation();

    const onCancel = () => setVisible(false)

    const onSubmit = () => {
        form
            .validateFields()
            .then(values => {
                form.resetFields();
                allocateAccount({
                    staffId: staff.id,
                    username: values['username'],
                    password: values['password']
                })
                    .unwrap()
                    .then(result => {
                        AntdNotifier.success("Cập nhật tài khoản thành công")
                        setVisible(false);
                    })
                    .catch(error => {
                        console.log(error)
                        if (error?.data) {
                            AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình cập nhật tài khoản")
                        } else {
                            AntdNotifier.error("Có lỗi trong quá trình cập nhật tài khoản")
                        }
                    })
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
    }

    const validateMessages = {
        required: '${label} là trường bắt buộc'
    };


    return <Modal
        title={`Tài khoản của ${staff.lastName} ${staff.firstName}`}
        onCancel={onCancel}
        onOk={onSubmit}
        visible={visible}
        destroyOnClose
        okText={staff?.username ? 'Cập nhật' : 'Cấp tài khoản'}
        cancelText={'Hủy'}
        maskClosable={false}
    >
        <Card>
            <Row justify="center">
                <Col lg={12} sm={24}>
                    <Form layout="vertical"
                          labelAlign="left"
                          form={form}
                          preserve={false}
                          validateMessages={validateMessages}
                    >
                        <Form.Item
                            label="Tên đăng nhập"
                            name={"username"}
                            initialValue={staff?.username}
                            rules={[{required: true}]}
                        >
                            <Input placeholder="Nhập tên đăng nhập"/>
                        </Form.Item>
                        <Form.Item
                            label="Mật khẩu"
                            name={"password"}
                            rules={[{required: true}]}
                        >
                            <Input.Password placeholder="Nhập mật khẩu"/>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Card>
    </Modal>
}

export default AccountStaffModal;