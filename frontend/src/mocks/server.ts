/**
 * This file setup the msw server for mocking api and unit testing (in node without a webdriver)  - use screen.debug and find methods
 */
import { store } from "@redux_store";
import { setupServer } from "msw/node";
import handlers from "./handlers/handlers";
import ReactFirebaseFileUpload from '../utils/UploadFile/FileUploader';

const server = setupServer(...handlers);

const beforeAllTestUtil = () => {
    console.log("Before All Test is Run: ");
    server.listen();
};

const beforeEachTestHook = () => {
}

const afterAllTestUtil = () => {
    console.log("After all test have run!");
    server.close();
};
beforeAll(beforeAllTestUtil);
// beforeEach()
afterEach(() => server.resetHandlers());
afterAll(afterAllTestUtil);
export default server;
