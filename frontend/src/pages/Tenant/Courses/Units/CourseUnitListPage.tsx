import {TextbookUnit, UnitGroupByTextbook, UnitItem} from "@typescript/interfaces/courses/CourseTypes";
import {useNavigate, useParams} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {Card, Col, Collapse, Row, Space, Switch, Tooltip, Typography} from "antd";
import {UnitIcon} from "@components/TeachingContent/NewActivityMenu";
import COLOR_SCHEME from "@constants/ThemeColor";
import {testSvg} from "@components/Icons/SvgIcons";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {DeleteOutlined} from "@ant-design/icons";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import React, {useEffect, useState} from "react";
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import CustomEmpty from "@components/Util/CustomEmpty";
import LoadingPage from "@pages/UtilPages/LoadingPage";
import ApiErrorWithRetryButton from "@components/Util/ApiErrorWithRetryButton";
import {
    useDeleteUnitMutation,
    useGetUnitCourseQuery,
    useLazyGetUnitCourseGroupByTextbooksQuery
} from "@redux/features/Course/CourseAPI";
import useCourseSidebar from "@hooks/Routing/useCourseSidebar";
import './CourseUnitListPage.less';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faEye, faPenToSquare} from "@fortawesome/free-solid-svg-icons";
import {blue} from "@ant-design/colors";
import {AddUnitItemModal, RenderActivityIcon} from "@pages/Tenant/Courses/CourseContent/CourseContentPage";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {courseContentActions} from "@redux/features/Course/CourseContentSlice";

const {Panel} = Collapse;

const UnitItemCard = (props: UnitItem & { accountType: string }) => {
    const {courseId} = useParams();
    const {title, id, textbooks} = props;

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const [deleteUnit] = useDeleteUnitMutation();

    const navigateToUnitDetail = () => {
        navigate(
            replacePathParams(
                ROUTING_CONSTANTS.COURSE_UNIT_ITEM_DETAIL_PAGE,
                [":courseId", Number(courseId)],
                [":unitItemId", id]
            )
        );
    };

    return (
        <Card
            className="class-quiz-card-item"
            title={
                <Space align="center">
                    <UnitIcon/>
                    <p style={{margin: 0, padding: 0}}>
                        {title || "Không có thông tin"}
                    </p>
                </Space>
            }
            extra={
                <Space>
                    <div className="action-container">
                        <Tooltip title={"Chi tiết"}>
                            <Typography.Link
                                className={'flex'}
                                style={{marginRight: '0.25rem', color: COLOR_SCHEME.primary}}
                                onClick={() => {
                                    navigateToUnitDetail();
                                }}
                            >
                                {testSvg}
                            </Typography.Link>
                        </Tooltip>
                        <Tooltip title={"Chỉnh sửa"}>
                            <FontAwesomeIcon
                                className={"icon edit action-icon-activity"}
                                icon={faPenToSquare}
                                onClick={() => {
                                    dispatch(
                                        courseContentActions.updatingActivity(
                                            {
                                                chapterToEdit: -1,
                                                activity: {...props, type: "unit"},
                                            }
                                        )
                                    );
                                }}
                            />
                        </Tooltip>
                        <PermissionRenderer permissions={[PermissionEnum.DELETE_QUIZ_CLASS]}>
                            <DeleteOutlined
                                className="delete"
                                onClick={() => {
                                    confirmationModal("bài học", title, () => {
                                        AntdNotifier.handlePromise(
                                            deleteUnit({
                                                courseId:
                                                    Number(courseId),
                                                unitId: id,
                                            }).unwrap(),
                                            "Xóa bài học "
                                        );
                                    });
                                }}
                            />
                        </PermissionRenderer>
                    </div>
                </Space>
            }
        >
            {(textbooks == undefined || textbooks.length == 0) ? 'Bài học không có giáo trình' : <Row>
                <Col className={'course-unit-page-label course-unit-page-label-left'} span={5}>Giáo trình</Col>
                <Col className={'course-unit-page-label'} span={19}>Ghi chú</Col>
                {textbooks.map((textbook: TextbookUnit) => {
                    return (
                        <>
                            <Col className={'course-unit-page-value-left'} span={5}>
                                <Typography.Link
                                    onClick={() => navigate(replacePathParams(ROUTING_CONSTANTS.COURSE_TEXTBOOK_DETAIL, [":courseId", courseId || ""], [":textbookId", textbook.textbookId || ""]))}
                                >
                                    {textbook?.name}
                                </Typography.Link>
                            </Col>
                            <Col span={19}>{textbook?.note}</Col>
                        </>
                    )
                })}
            </Row>}
            <AddUnitItemModal/>
        </Card>
    );
};

const CourseUnitListPage = () => {
    const {
        courseId
    } = useParams();

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const {} = useCourseSidebar();

    const currentUserAccountType = useAppSelector(authSelectors.selectCurrentUserAccountType);

    const [textbookViewMode, setTextbookViewMode] = useState(false);

    const [fetchUnitCourseGroupByTextbook, {
        data: unitGroupByTextbook,
        isLoading: unitGroupByTextbookLoading,
        isSuccess: unitGroupByTextbookSuccess,
    }] = useLazyGetUnitCourseGroupByTextbooksQuery();

    const {
        data: units,
        isSuccess,
        isFetching,
        isError,
        error,
        refetch: refetchUnitList
    } = useGetUnitCourseQuery({courseId: Number(courseId)});
    const [closeSidebar] = useClassSidebar();

    const [deleteUnit] = useDeleteUnitMutation();

    useEffect(() => {
        if (textbookViewMode && courseId) {
            fetchUnitCourseGroupByTextbook({courseId: Number(courseId)});
        } else if (!textbookViewMode) {
            refetchUnitList();
        }
    }, [textbookViewMode]);

    return (
        <div>
            <ContentHeader
                title={`Bài học`}
                action={<>
                    <Switch
                        onClick={(checked: boolean) => setTextbookViewMode(checked)}
                        style={{marginRight: "1rem"}}
                        checked={textbookViewMode}
                    />
                    <span>Xem theo giáo trình</span>
                </>}
            />
            {
                !textbookViewMode && units?.length === 0 && <CustomEmpty description="Chưa có bài học nào."/>
            }
            {!textbookViewMode && isSuccess && (
                <>
                    {units.map((item: UnitItem, index: number) => {
                        return <UnitItemCard {...item} accountType={currentUserAccountType || ''} key={item.id}/>;
                    })}
                </>
            )}

            {textbookViewMode && unitGroupByTextbookSuccess && <div>
                {unitGroupByTextbook?.map((textbook: UnitGroupByTextbook, index: number) => {
                    return <Collapse className={'textbook-wrapper'}>
                        <Panel
                            header={<>
                                <FontAwesomeIcon
                                    icon={faBook}
                                    style={{margin: "0 .5em 0 0", color: blue.primary}}
                                />
                                <p className={"chapter-title"}>
                                    {textbook.name}
                                </p></>
                            }
                            key={textbook.textbookId}
                        >
                            <div className="activity-list-container">
                                {textbook.units.map((unit: UnitItem, idx: number) => {
                                        return <div
                                            className={"activity-item-container"}
                                        >
                                            <Space>
                                                <RenderActivityIcon
                                                    type={"unit"}
                                                />
                                                <p className={"activity-title"}>
                                                    {unit.title}
                                                </p>
                                            </Space>
                                            <div className={"action-container"}>
                                                <FontAwesomeIcon
                                                    onClick={() => {
                                                        navigate(
                                                            replacePathParams(
                                                                ROUTING_CONSTANTS.COURSE_UNIT_ITEM_DETAIL_PAGE,
                                                                [
                                                                    ":courseId",
                                                                    Number(courseId),
                                                                ],
                                                                [
                                                                    ":unitItemId",
                                                                    unit.id,
                                                                ]
                                                            )
                                                        );
                                                    }}
                                                    className={"icon detail action-icon-activity"}
                                                    icon={faEye}
                                                />
                                                <Tooltip
                                                    title={"Chỉnh sửa"}>
                                                    <FontAwesomeIcon
                                                        className={"icon edit action-icon-activity"}
                                                        icon={faPenToSquare}
                                                        onClick={() => {
                                                            dispatch(
                                                                courseContentActions.updatingActivity(
                                                                    {
                                                                        chapterToEdit: -1,
                                                                        activity: {...unit, type: "unit"},
                                                                    }
                                                                )
                                                            );
                                                        }}
                                                    />
                                                </Tooltip>
                                                <DeleteOutlined
                                                    onClick={() => {
                                                        confirmationModal("bài học", unit.title, () => {
                                                            AntdNotifier.handlePromise(
                                                                deleteUnit({
                                                                    courseId:
                                                                        Number(courseId),
                                                                    unitId: unit.id,
                                                                }).unwrap(),
                                                                `Xóa bài học ${unit.title}`,
                                                                () => fetchUnitCourseGroupByTextbook({courseId: Number(courseId)})
                                                            );
                                                        });
                                                    }}
                                                    className={"icon delete action-icon-activity"}
                                                />
                                            </div>
                                        </div>
                                    })}
                                {textbook.units.length == 0 ? <div style={{padding: '1rem'}}>Chưa có bài học nào sử dụng giáo trình này</div> : ''}
                            </div>
                        </Panel>
                    </Collapse>
                })}
                <AddUnitItemModal/>
            </div>}


            {(isFetching || unitGroupByTextbookLoading) && <LoadingPage/>}
            {isError && (
                <Row align={"middle"} justify={"center"}>
                    <ApiErrorWithRetryButton onRetry={() => {
                    }}/>;
                </Row>
            )}
        </div>
    );
}

export default CourseUnitListPage;