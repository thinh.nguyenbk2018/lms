import {Button, ButtonProps} from "antd";
import {HideIcon,} from "@pages/Tenant/Courses/CourseContent/CourseContentPage";

const HideButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			type={"primary"}
			danger
			icon={<HideIcon style={{ margin: "0 .5em 0 0" }} />}
		>
			{`${props.title || "Ẩn bài kiểm tra"}`}
		</Button>
	);
};

export default HideButton;
