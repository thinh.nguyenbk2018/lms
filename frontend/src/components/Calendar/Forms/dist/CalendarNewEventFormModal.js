"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var antd_1 = require("antd");
var date_fns_1 = require("date-fns");
var formik_1 = require("formik");
var formik_antd_1 = require("formik-antd");
var formik_devtools_extension_1 = require("formik-devtools-extension");
var moment_1 = require("moment");
var react_1 = require("react");
var Yup = require("yup");
var hooks_1 = require("../../../hooks/Redux/hooks");
var AuthSlice_1 = require("../../../redux/features/Auth/AuthSlice");
var CalendarAPI_1 = require("../../../redux/features/Calendar/CalendarAPI");
var formUtils_1 = require("../../../utils/FormUtils/formUtils");
var CancelButton_1 = require("../../ActionButton/CancelButton");
var MyRRuleGenerator_1 = require("../../MyRRuleGenerator");
var CalendarUtils_1 = require("../Utils/CalendarUtils");
var formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
    }
};
var tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};
function disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment_1["default"]().endOf("day");
}
var EventSchema = Yup.object().shape({
    summary: Yup.string()
        .min(4, formUtils_1.FORM_DEFAULT_MESSAGES.TOO_SHORT)
        .max(50, formUtils_1.FORM_DEFAULT_MESSAGES.TOO_LONG)
        .required(formUtils_1.FORM_DEFAULT_MESSAGES.REQUIRED),
    location: Yup.string()
        .min(4, formUtils_1.FORM_DEFAULT_MESSAGES.TOO_SHORT)
        .max(50, formUtils_1.FORM_DEFAULT_MESSAGES.TOO_LONG)
        .required(formUtils_1.FORM_DEFAULT_MESSAGES.REQUIRED),
    description: Yup.string()
        .min(1, formUtils_1.FORM_DEFAULT_MESSAGES.TOO_SHORT)
        .required(formUtils_1.FORM_DEFAULT_MESSAGES.REQUIRED),
    timeZone: Yup.string()
        .required("Please choos a timezone")
        .min(2, "Please input valid timezone")
        .max(50, formUtils_1.FORM_DEFAULT_MESSAGES.INVALID),
    time: Yup.array()
        .of(Yup.date())
        .length(2, "Range should only contains exactly 2 timestamp")
        .test("valid start time", "Enter valid start time please", function (range) {
        if (!(range === null || range === void 0 ? void 0 : range[0]) || !moment_1["default"](range === null || range === void 0 ? void 0 : range[0]).isValid()) {
            return false;
        }
        return true;
    })
        .test("valid end time", "Enter valid end time please", function (range) {
        if (!(range === null || range === void 0 ? void 0 : range[1]) || !moment_1["default"](range === null || range === void 0 ? void 0 : range[1]).isValid()) {
            return false;
        }
        return true;
    })
        .test("start_is_earlier_to_end", "Start time should be earlier than End Time!!!", function (range) {
        if (!range || !range[0] || !range[1]) {
            return false;
        }
        return range[0] < range[1];
    })
});
var getNextDate = function () {
    var day = new Date();
    var nextDay = new Date(day);
    return nextDay.setDate(day.getDate() + 1);
};
var constInitialValues = {
    summary: "",
    description: "",
    location: "",
    timeZone: "",
    time: [moment_1["default"](new Date()), moment_1["default"](getNextDate())]
};
var mapDataToFormVal = function (eventInfo) {
    if (!eventInfo || !eventInfo.start || !eventInfo.end) {
        return constInitialValues;
    }
    var time = [];
    time[0] = moment_1["default"](CalendarUtils_1.getEventStartTime({ start: eventInfo.start }));
    time[1] = moment_1["default"](CalendarUtils_1.getEventEndTime({ end: eventInfo.end }));
    var timeZone = eventInfo.start.timeZone;
    var res = {
        summary: eventInfo.summary || "",
        description: eventInfo.description || "",
        location: eventInfo.location || "",
        timeZone: timeZone,
        time: time
    };
    return res;
};
// TODO: Remove the default event values  - This is only for faster testing only
// TODO: Remove the calendarDefaultId on submitting the form. Someone's merge on the backend erased the calendarId
// TODO: from the user object --> Calendar now will render empty
// * Calendar props is passed in only when testing without backed and need a default calendar to retrieve events
var CalendarNewEventFormModal = function (_a) {
    var calendarID = _a.calendarID, modalProps = _a.modalProps, initialValues = _a.initialValues, isUpdateForm = _a.isUpdateForm;
    var currentUserCalendarId = hooks_1.useAppSelector(AuthSlice_1.authSelectors.selectUserCalendarId);
    var calendarIdToUse = currentUserCalendarId || calendarID;
    // console.log("RERENDER FORM");
    var _b = react_1.useState(false), allowRecurence = _b[0], setAllowRecurrence = _b[1];
    var _c = react_1.useState(""), recurrenceSettings = _c[0], setRecurrenceSettings = _c[1];
    var _d = CalendarAPI_1.useAddEventMutation(), addEvent = _d[0], _e = _d[1], data = _e.data, isSuccess = _e.isSuccess, isError = _e.isError;
    var _f = CalendarAPI_1.useUpdateEventMutation(), updateEvent = _f[0], _g = _f[1];
    function onRecurrenceChange(e) {
        setAllowRecurrence(e.target.checked);
    }
    var onRRuleChange = react_1.useCallback(function (rrule) {
        setRecurrenceSettings(rrule);
    }, []);
    return (react_1["default"].createElement(react_1["default"].Fragment, null,
        react_1["default"].createElement(formik_1.Formik, { initialValues: mapDataToFormVal(initialValues), onSubmit: function (values, actions) {
                console.log("Formik Values is: ", values);
                // * Add calendar Id for service to call backend appropriately
                var time = values.time, timeZone = values.timeZone;
                var start = time[0].toDate();
                var recurrence = [];
                if (allowRecurence) {
                    var recurrenceRule = CalendarUtils_1.addHourAndMinuteToRRuleString(recurrenceSettings, start);
                    recurrence[0] = recurrenceRule;
                }
                // if(!calendarIdToUse) return;
                var valuesToSubmit = __assign(__assign({}, values), { calendarId: calendarIdToUse !== null && calendarIdToUse !== void 0 ? calendarIdToUse : "0v2mjauc84vkna6gqb07lel2ik@group.calendar.google.com", start: {
                        dateTime: time[0].format(),
                        timeZone: "Asia/Ho_Chi_Minh"
                    }, end: {
                        dateTime: time[1].format(),
                        timeZone: "Asia/Ho_Chi_Minh"
                    }, recurrence: recurrence });
                console.log("Submitted values is: ", valuesToSubmit);
                if (isUpdateForm) {
                    var id = initialValues === null || initialValues === void 0 ? void 0 : initialValues.id;
                    if (!id) {
                        console.log("No id  update");
                    }
                    var eventUpdateDto = __assign(__assign({}, valuesToSubmit), { id: id });
                    updateEvent(eventUpdateDto);
                }
                else {
                    addEvent(valuesToSubmit);
                }
                actions.setSubmitting(false);
            }, validationSchema: EventSchema }, function (formikProps) {
            formik_devtools_extension_1.withFormikDevtools(formikProps);
            return (react_1["default"].createElement(antd_1.Modal, __assign({}, modalProps, { footer: null }),
                react_1["default"].createElement(formik_antd_1.Form, __assign({}, formItemLayout),
                    react_1["default"].createElement(formik_antd_1.Form.Item, { name: "summary", label: "Summary" },
                        react_1["default"].createElement(formik_antd_1.Input, { name: "summary" })),
                    react_1["default"].createElement(formik_antd_1.Form.Item, { name: "description", label: "Descriptions" },
                        react_1["default"].createElement(formik_antd_1.Input, { name: "description" })),
                    react_1["default"].createElement(formik_antd_1.Form.Item, { name: "location", label: "Location" },
                        react_1["default"].createElement(formik_antd_1.Input, { name: "location" })),
                    react_1["default"].createElement(formik_antd_1.Form.Item, { name: "timeZone", label: "TimeZone" },
                        react_1["default"].createElement(formik_antd_1.Input, { name: "timeZone" })),
                    react_1["default"].createElement(formik_antd_1.Form.Item, { name: "time", label: "Time" },
                        react_1["default"].createElement(formik_antd_1.DatePicker.RangePicker, { disabledDate: disabledDate, showTime: {
                                hideDisabledOptions: false,
                                defaultValue: [
                                    moment_1["default"](new Date()),
                                    moment_1["default"](date_fns_1.addHours(new Date(), 2)),
                                ]
                            }, format: "YYYY-MM-DD HH:mm:ss", name: "time" }),
                        react_1["default"].createElement(antd_1.Typography.Text, { type: "danger" },
                            react_1["default"].createElement(react_1["default"].Fragment, null, formikProps.errors.time))),
                    react_1["default"].createElement(formik_antd_1.Form.Item, { name: "recurrence", label: "Recurrence" },
                        react_1["default"].createElement(antd_1.Checkbox, { onChange: onRecurrenceChange }, "Checkbox")),
                    react_1["default"].createElement(antd_1.Divider, null, " Recurrence Settings"),
                    react_1["default"].createElement(MyRRuleGenerator_1["default"], { onChange: onRRuleChange, translations: MyRRuleGenerator_1.translations.vietnamese, hideStart: false }),
                    react_1["default"].createElement(antd_1.Divider, null),
                    react_1["default"].createElement(antd_1.Row, { justify: "end" },
                        react_1["default"].createElement(antd_1.Space, null,
                            react_1["default"].createElement(CancelButton_1["default"], { onClick: modalProps === null || modalProps === void 0 ? void 0 : modalProps.onCancel }),
                            react_1["default"].createElement(formik_antd_1.SubmitButton, { loading: formikProps.isSubmitting, disabled: !formikProps.isValid }, "Submit"))))));
        })));
};
exports["default"] = CalendarNewEventFormModal;
