import {Card, Col, Modal, Pagination, Row, Space, Typography} from "antd";
import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import QuestionContainerItem from "../../../../../components/Question/QuestionContainerItem/QuestionContainerItem";
import {useAppDispatch, useAppSelector,} from "@hooks/Redux/hooks";
import useClassSidebar from "../../../../../hooks/Routing/useClassSidebar";
import SubmitButton from "../../../../../components/ActionButton/TestSubmissionButton";
import {
    isPaginatedResponseQuestionList,
    useEndTestSessionMutation,
    useGetExamIdOfQuizQuery,
    useGetQuestionsIdAndTypeQuery,
    useGetSessionsResultOfStudentQuery,
    useLazyGetFullTestSessionQuery,
} from "@redux/features/Quiz/examAPI";
import {testSessionActions, testSessionSelectors,} from "@redux/features/Quiz/TestSessionSlice";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import antdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import {examErrorsHandler} from "../../Content/ClassQuizDetail/ClassQuizDetailPage";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import './QuizClassTakingSession.less';
import BackButton from "@components/ActionButton/BackButton";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import COLOR_SCHEME from "@constants/ThemeColor";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {useGetClassQuizDetailQuery} from "@redux/features/Class/ClassAPI";
import QuizClassTakingSessionCountdown from "@pages/Tenant/Class/Quiz/QuizSession/QuizClassTakingSessionCountdown";
import DEFAULT_PAGE_SIZE from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import LoadingPage from "@pages/UtilPages/LoadingPage";
import {ExclamationCircleOutlined} from "@ant-design/icons";
import ApiErrorWithRetryButton from "@components/Util/ApiErrorWithRetryButton";

// TODO: display test config and the questions form for taking the student answers
const QuizClassTakingSession = () => {
    const [closeClassSibar, classId, courseInfo] = useClassSidebar();

    const [examId, setExamId] = useState(0);

    // * Router States
    const navigate = useNavigate();
    const params = useParams();
    const {testSessionId: sessionId, quizId: quizItemId} = useParams();
    // * Redux States
    const dispatch = useAppDispatch();

    const searchCriteria = useAppSelector(
        testSessionSelectors.selectSearchCriteria
    );

    const changeSearchCriteria = testSessionActions.changeCriteria;


    const {
        data: fetchedExamId,
        isSuccess: fetchExamIdSuccess
    } = useGetExamIdOfQuizQuery({quizId: Number(quizItemId)}, {skip: Number.isNaN(Number(quizItemId))})

    const {
        data: questionsIdAndType,
        isSuccess: fetchQuestionsIdAndTypeSuccess
    } = useGetQuestionsIdAndTypeQuery({examId: examId, sessionId: sessionId || ""}, {skip: examId === 0 && !sessionId});

    const [
        trigger,
        {data: testData, isSuccess, isLoading, isFetching, isError, error},
    ] = useLazyGetFullTestSessionQuery();

    useEffect(() => {
        if (!sessionId || !courseInfo?.id || !quizItemId) {
            return;
        }
        // console.log("FOUND SESSION ID: ", sessionId);
        dispatch(testSessionActions.setCurrentTestSessionId(sessionId));
        trigger({
            classId,
            testWithConfigId: Number(params.quizId),
            searchCriteria,
            sessionId: sessionId,
            questionId: undefined
        });
        // return () => {
        //     dispatch(testSessionActions.endTestSession());
        // };
    }, [sessionId, courseInfo?.id, searchCriteria]);

    useEffect(() => {
        setExamId(fetchedExamId || 0);
    }, [fetchedExamId]);

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const shouldSkip = () => {
        return (
            isNaN(Number(quizItemId)) ||
            isNaN(Number(classId))
        );
    };

    const {
        refetch: refetchClassQuizDetail
    } = useGetClassQuizDetailQuery(
        {
            classId: Number(classId),
            quizId: Number(quizItemId),
        },
        {
            skip: shouldSkip(),
        }
    );

    const {
        refetch: refetchSessionsResultOfStudent
    } = useGetSessionsResultOfStudentQuery({
        classId: Number(classId),
        quizId: Number(quizItemId),
    }, {
        skip: userInfo?.accountType === 'STAFF' || shouldSkip()
    })

    const [endTestSession] = useEndTestSessionMutation();

    const submitSession = React.useCallback(() => {
        if (quizItemId != undefined && classId != undefined && sessionId != undefined) {
            endTestSession({classId: classId, quizId: Number(quizItemId), testSessionId: sessionId})
            .unwrap()
            .then(res => {
                antdNotifier.warn("Bài quiz đã được tự động nộp khi hết giờ làm bài", "", 2);
                refetchClassQuizDetail();
                refetchSessionsResultOfStudent();
                navigate(
                    replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                        [":classId", classId || ""],
                        [":quizItemId", quizItemId || ""])
                )
            })
            .catch(err => {
                console.log('err: ', err);
                antdNotifier.error("Đã có lỗi khi tự động nộp bài", "", 2);
            })
        }
    }, []);

    if (isSuccess && courseInfo?.id) {
        return (
            <>
                <ContentHeader
                    title={'Làm bài kiểm tra'}
                    action={
                        <Space>
                            <BackButton
                                type="primary"
                                danger
                                onClick={() => {
                                    refetchClassQuizDetail();
                                    refetchSessionsResultOfStudent();
                                    navigate(
                                        replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                            [":classId", classId || ""],
                                            [":quizItemId", quizItemId || ""])
                                    )
                                }}
                                className={"back-btn"}
                            />
                        </Space>
                    }
                />
                <Row>
                    <Col sm={18} md={18} lg={18} xl={18}>
                        <Row gutter={[12, 23]}>
                            <Col sm={24} md={24} lg={24} xl={24} xxl={{span: 18, offset: 2}}>
                                <Row gutter={[0, 20]} align={"middle"} justify={"center"}>
                                    {isPaginatedResponseQuestionList(testData)
                                        ? testData?.listData.map((item, index) => (
                                            <Col span={24} key={item.id}>
                                                <span id={`question-${item.id}`}/>
                                                <QuestionContainerItem
                                                    questionData={item}
                                                    canEdit={true}
                                                    mode={"doing"}
                                                    order={(testData.page - 1) * searchCriteria.pagination.size + index + 1}
                                                    flagged={questionsIdAndType && questionsIdAndType.find(x => x.id === item.id)?.flagged}
                                                />
                                            </Col>
                                        ))
                                        : "Không thể hiển thị câu hỏi, vui lòng liên hệ với quản trị viên hệ thống."}
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={6} md={6} lg={6} xl={6}>
                        <Card title={<Typography.Title
                            className={"title"}
                            level={5}
                            style={{color: COLOR_SCHEME.primary}}
                        >
                            {'Điều hướng câu hỏi'}
                        </Typography.Title>}>
                            <div className={'question-navigate-wrapper'}>
                                {questionsIdAndType?.map((question, index) => {
                                    return (
                                        <a className={'question-navigate-link'}
                                            onClick={() => {
                                                if (sessionId != undefined) {
                                                    trigger({
                                                        classId,
                                                        testWithConfigId: Number(params.quizId),
                                                        searchCriteria,
                                                        sessionId: sessionId,
                                                        questionId: question.id
                                                    })
                                                        .unwrap()
                                                        .then(res => {
                                                            const violation = document.getElementById(`question-${question.id}`);
                                                            window.scrollTo({
                                                                top:violation?.getBoundingClientRect().top,
                                                                behavior:"smooth"
                                                            });
                                                        });
                                                }
                                            }}
                                        >
                                            <span
                                                className={`thispageholder ${questionsIdAndType[index].flagged ? 'flagged-question' : ''}`}/>
                                            <span
                                                className={`trafficlight ${questionsIdAndType[index].status}`}/>
                                            <span className="accesshide">Câu hỏi </span>{index + 1}
                                            <span className="accesshide"> Trang này <span className="flagstate"/>
                                        </span>
                                        </a>
                                    )
                                })}
                            </div>
                        </Card>
                        <div className={'countdown-wrapper'}>
                            <QuizClassTakingSessionCountdown
                                remainingTime={testData?.remainingTime}
                                submitSession={submitSession}
                                haveTimeLimit={testData?.haveTimeLimit}
                            />
                        </div>
                        <div className={'quiz-session-taking-submit-btn flex justify-content-center'}>
                            <SubmitButton
                                onClick={() => {
                                    Modal.confirm({
                                        wrapClassName: 'confirm-continue-modal-wrapper',
                                        title: (
                                            <span className={'confirm-delete-modal-title-name'}>Xác nhận nộp bài</span>
                                        ),
                                        content: (
                                            <span>Bạn có chắc chắn muốn nộp bài ngay bây giờ?</span>
                                        ),
                                        onOk: () => {
                                            if (
                                                !sessionId ||
                                                isNaN(classId) ||
                                                isNaN(Number(params.quizId))
                                            ) {
                                                AntdNotifier.error(
                                                    "Không thể nộp bài của bạn. Hãy liên hệ với giáo viên để được giải quyết"
                                                );
                                                return;
                                            }
                                            AntdNotifier.handlePromise(
                                                endTestSession({
                                                    testSessionId: sessionId,
                                                    classId: classId,
                                                    quizId: Number(params.quizId),
                                                }).unwrap(),
                                                "Nộp bài", () => {
                                                    navigate(replacePathParams(
                                                        ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                                        [
                                                            ":classId",
                                                            Number(classId),
                                                        ],
                                                        [
                                                            ":quizItemId",
                                                            Number(quizItemId),
                                                        ]
                                                    ))
                                                },
                                                examErrorsHandler
                                            );
                                        },
                                        icon: <ExclamationCircleOutlined/>,
                                        okText: `Nộp bài`,
                                        cancelText: 'Hủy',
                                        okButtonProps: {className: 'confirm-continue-modal-ok-btn'},
                                        cancelButtonProps: {className: 'confirm-continue-modal-cancel-btn'}
                                    })
                                }}
                            />
                        </div>
                    </Col>
                </Row>

                <Row justify={"end"} style={{marginTop: 12}}>
                    {isPaginatedResponseQuestionList(testData) && (
                        <Pagination
                            showSizeChanger
                            pageSize={searchCriteria?.pagination?.size || DEFAULT_PAGE_SIZE}
                            onChange={(page: any, size: any) => {
                                const payload = {
                                    ...searchCriteria,
                                    pagination: {page, size},
                                };
                                dispatch(changeSearchCriteria(payload));
                            }}
                            onShowSizeChange={(current, newSize) => {
                                let newSearchCriteria = {
                                    ...searchCriteria,
                                    pagination: {
                                        size: newSize,
                                        page: 1,
                                    },
                                };
                                dispatch(
                                    changeSearchCriteria(newSearchCriteria)
                                );
                                window.scrollTo(0, 0);
                            }}
                            current={testData?.page}
                            total={testData?.total}
                        />
                    )}
                </Row>
            </>
        );
    } else if (isLoading) {
        return <LoadingPage/>;
    } else if (isError) {
        AntdNotifier.error(`Session failed with ${JSON.stringify(error)}`);
        return <ApiErrorWithRetryButton onRetry={() => {
        }}/>;
    } else return <></>;
};

export default QuizClassTakingSession;
