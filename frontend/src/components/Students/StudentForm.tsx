import { Avatar, Card, Col, Divider, Form, Input, Row, Space, Typography, Image, Spin } from "antd";
import { forwardRef, Ref, useEffect, useImperativeHandle } from "react";
import { Student, useCreateStudentMutation, useUpdateStudentMutation } from "../../redux/features/Student/StudentAPI";

export interface StudentFormHandle {
  updateStudent: () => any,
  createStudent: () => any,
}

const StudentForm = ({ student }: { student?: Student }, ref: Ref<StudentFormHandle>) => {

  const [createStudent, { isLoading: isCreating }] = useCreateStudentMutation();
  const [updateStudent, { isLoading: isUpdating }] = useUpdateStudentMutation();

  let [form] = Form.useForm();

  useImperativeHandle(ref, () => ({
    createStudent: async () => {
      try {
        let values = await form.validateFields();
        return createStudent(values).unwrap();
      } catch (error) {
        console.log(error)
        return null;
      }
    } ,
    updateStudent: async () => {
      try {
        let values = await form.validateFields();
        return updateStudent({
          id: student?.id,
          ...values
        }).unwrap();
      } catch (error) {
        console.log(error)
        return null;
      }
    } 
  }))

  useEffect(() => form.resetFields(), [student, form]);

  return <Spin spinning={isCreating || isUpdating}>
    {student &&
      <Row justify="center">
        <Space direction="vertical" align="center">
          <Avatar size={150} src={<Image src={student?.avatar} />} />
          <Typography.Title level={3}>{student?.lastName} {student?.firstName}</Typography.Title>
          <Typography.Text>NV_{student?.id}</Typography.Text>
        </Space>
      </Row>
    }
    <Divider orientation="left" orientationMargin={5}><Typography.Title level={5}>Thông tin chung</Typography.Title></Divider>
    <Card>
      <Row justify="center">
        <Col lg={12} sm={24}>
          <Form layout="vertical"
            labelAlign="left"
            form={form}
          >
            <Row gutter={[16, 16]}>
              <Col span={16}>
                <Form.Item
                  label="Họ và tên đệm"
                  name={"lastName"}
                  initialValue={student?.lastName}
                  required
                  rules={[{required: true, message: "Vui lòng điền Họ và tên đệm"}]}
                >
                  <Input placeholder="Nhập họ và tên đệm" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  label="Tên"
                  name={"firstName"}
                  initialValue={student?.firstName}
                  required
                  rules={[{required: true, message: "Vui lòng điền Tên"}]}
                >
                  <Input placeholder="Nhập tên" />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item
              label="Email"
              name={"email"}
              initialValue={student?.email}
              required
              rules={[{required: true, message: "Vui lòng điền Email"}]}
            >
              <Input placeholder="Nhập email" />
            </Form.Item>
            <Form.Item
              label="Số điện thoại"
              name={"phone"}
              initialValue={student?.phone}
              required
              rules={[{required: true, message: "Vui lòng điền Số điện thoại"}]}
            >
              <Input placeholder="Nhập số điện thoại" />
            </Form.Item>
            <Form.Item
              label="Địa chỉ"
              name={"address"}
              initialValue={student?.address}
              required
              rules={[{required: true, message: "Vui lòng điền địa chỉ"}]}
            >
              <Input placeholder="Nhập địa chỉ" />
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Card>
  </Spin>
}

export default forwardRef(StudentForm);