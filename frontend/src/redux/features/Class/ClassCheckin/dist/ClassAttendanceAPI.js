"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.usePutNoteToSesssionMutation = exports.useChangeSessionAttendanceRateCalculationStrategyMutation = exports.useDeleteMeetingAttendanceBatchMutation = exports.useChangeMeetingAttendanceSingleMutation = exports.useChangeMeetingAttendanceBatchMutation = exports.useAddNewAttendanceCheckMutation = exports.useGetAttendanceByDateQuery = exports.useChangeStudentAttendanceBatchByDateMutation = exports.useChangeStudentAttendanceMutation = exports.useGetClassAttendancesQuery = exports.getAttendanceListFromAttendanceDate = exports.getStudentListFromAttendanceDate = void 0;
var CentralAPI_1 = require("../../CentralAPI");
var classAttendanceApiWithInvalidateTags = CentralAPI_1.apiSlice.enhanceEndpoints({
    addTagTypes: ["ClassAttendance", "ClassMeetingAttendance"]
});
exports.getStudentListFromAttendanceDate = function (data) {
    var studentDataList = [];
    for (var studentID in data) {
        var student = data[studentID];
        studentDataList.push(__assign(__assign({}, student), { studentId: Number(studentID) }));
    }
    return studentDataList;
};
exports.getAttendanceListFromAttendanceDate = function (date) {
    var stateList = [];
    for (var studentID in date) {
        var student = date[studentID];
        stateList.push(student.state);
    }
    return stateList;
};
var classClassAttendanceApi = classAttendanceApiWithInvalidateTags.injectEndpoints({
    endpoints: function (build) { return ({
        // * Overall Stuffs
        getClassAttendances: build.query({
            query: function (_a) {
                var classId = _a.classId;
                return "classes/" + classId + "/overall_attendance";
            },
            providesTags: [{ type: "ClassAttendance", id: "LIST" }]
        }),
        changeStudentAttendance: build.mutation({
            query: function (_a) {
                var classId = _a.classId, studentId = _a.studentId, newState = _a.newState, sessionId = _a.sessionId;
                return {
                    method: "POST",
                    url: "/classes/" + classId + "/attendance/session/" + sessionId + "/students/" + studentId,
                    body: {
                        state: newState
                    }
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassAttendance", id: args.sessionId },
                { type: "ClassAttendance", id: "LIST" },
            ]; }
        }),
        changeStudentAttendanceBatchByDate: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId, batchState = _a.batchState;
                return {
                    method: "POST",
                    url: "classes/" + classId + "/attendance/session/" + sessionId + "/batch",
                    body: {
                        state: batchState
                    }
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassAttendance", id: args.sessionId },
                { type: "ClassAttendance", id: "LIST" },
            ]; }
        }),
        // * Single Session Stuffs: Single Meeting Attendance =  N X Single Attendance Check
        getAttendanceByDate: build.query({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId;
                return "classes/" + classId + "/attendance/session/" + sessionId;
            },
            providesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        }),
        addNewAttendanceCheck: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId;
                return {
                    url: "classes/" + classId + "/attendance/" + sessionId,
                    method: "POST"
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        }),
        changeMeetingAttendanceBatch: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId, checkTime = _a.checkTime, state = _a.state;
                return {
                    url: "classes/" + classId + "/attendance/session/" + sessionId + "/time/" + checkTime + "/batch",
                    method: "POST",
                    body: {
                        state: state
                    }
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        }),
        putNoteToSesssion: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId, note = _a.note;
                return {
                    url: "classes/" + classId + "/attendance/session/" + sessionId + "/note",
                    body: {
                        note: note
                    },
                    method: "POST"
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        }),
        changeMeetingAttendanceSingle: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId, checkTime = _a.checkTime, studentId = _a.studentId, state = _a.state;
                return {
                    url: "classes/" + classId + "/attendance/session/" + sessionId + "/time/" + checkTime + "/students/" + studentId,
                    method: "POST",
                    body: {
                        state: state
                    }
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        }),
        changeSessionAttendanceRateCalculationStrategy: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId, newStrategy = _a.newStrategy;
                return {
                    url: "classes/" + classId + "/attendance/session/" + sessionId + "/strategy",
                    method: "POST",
                    body: {
                        strategy: newStrategy
                    }
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        }),
        deleteMeetingAttendanceBatch: build.mutation({
            query: function (_a) {
                var classId = _a.classId, sessionId = _a.sessionId, checkTime = _a.checkTime;
                return {
                    url: "classes/" + classId + "/attendance/session/" + sessionId + "/time/" + checkTime,
                    method: "DELETE"
                };
            },
            invalidatesTags: function (_res, _err, args) { return [
                { type: "ClassMeetingAttendance", id: args.sessionId },
            ]; }
        })
    }); }
});
// * Class overall attendance management
exports.useGetClassAttendancesQuery = classClassAttendanceApi.useGetClassAttendancesQuery, exports.useChangeStudentAttendanceMutation = classClassAttendanceApi.useChangeStudentAttendanceMutation, exports.useChangeStudentAttendanceBatchByDateMutation = classClassAttendanceApi.useChangeStudentAttendanceBatchByDateMutation, 
// * Session Detailed attendance management
exports.useGetAttendanceByDateQuery = classClassAttendanceApi.useGetAttendanceByDateQuery, exports.useAddNewAttendanceCheckMutation = classClassAttendanceApi.useAddNewAttendanceCheckMutation, exports.useChangeMeetingAttendanceBatchMutation = classClassAttendanceApi.useChangeMeetingAttendanceBatchMutation, exports.useChangeMeetingAttendanceSingleMutation = classClassAttendanceApi.useChangeMeetingAttendanceSingleMutation, exports.useDeleteMeetingAttendanceBatchMutation = classClassAttendanceApi.useDeleteMeetingAttendanceBatchMutation, exports.useChangeSessionAttendanceRateCalculationStrategyMutation = classClassAttendanceApi.useChangeSessionAttendanceRateCalculationStrategyMutation, exports.usePutNoteToSesssionMutation = classClassAttendanceApi.usePutNoteToSesssionMutation;
