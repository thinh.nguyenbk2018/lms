"use strict";
exports.__esModule = true;
var ContentHeader_1 = require("../../../components/ContentHeader/ContentHeader");
var antd_1 = require("antd");
var PermissionContent_1 = require("../../../components/Authorization/GrantPermisson/PermissonContent/PermissionContent");
var hooks_1 = require("../../../hooks/Redux/hooks");
var PermissonSlice_1 = require("../../../redux/features/Permission/PermissonSlice");
var PermissionAPI_1 = require("../../../redux/features/Permission/PermissionAPI");
var AntdNotifier_1 = require("../../../utils/AntdAnnouncer/AntdNotifier");
var PermissionPage = function () {
    var grantPermissionState = hooks_1.useAppSelector(function (state) { return state.grantPermission; });
    var grantPermission = PermissionAPI_1.useGrantPermissionMutation()[0];
    var dispatch = hooks_1.useAppDispatch();
    var onEdit = function () {
        dispatch(PermissonSlice_1.backupRolesAndPermissions());
        dispatch(PermissonSlice_1.toggleEidtState());
    };
    var onCancel = function () {
        dispatch(PermissonSlice_1.cancelEdit());
    };
    var onDone = function () {
        grantPermission(grantPermissionState)
            .then(function (response) {
            if ('data' in response) {
                AntdNotifier_1["default"].success("Cấp quyền thành công");
                dispatch(PermissonSlice_1.toggleEidtState());
            }
            else {
                AntdNotifier_1["default"].error("Có lỗi hệ thống");
                console.log(response);
            }
        })["catch"](function (reason) {
            AntdNotifier_1["default"].error("Có lỗi hệ thống");
            console.log(reason);
        });
    };
    var renderActions = function () {
        if (grantPermissionState.userId !== 0)
            return !grantPermissionState.isEditing ?
                React.createElement(antd_1.Space, null,
                    React.createElement(antd_1.Button, { type: "primary", color: "#73d13d", onClick: onEdit }, "Ch\u1EC9nh s\u1EEDa"))
                :
                    React.createElement(antd_1.Space, null,
                        React.createElement(antd_1.Button, { danger: true, type: "primary", color: "#73d13d", onClick: onCancel }, "H\u1EE7y"),
                        React.createElement(antd_1.Button, { type: "primary", color: "#73d13d", onClick: onDone }, "Xong"));
        return React.createElement(React.Fragment, null, " ");
    };
    return React.createElement(React.Fragment, null,
        React.createElement(ContentHeader_1["default"], { title: "Ph\u00E2n quy\u1EC1n", action: renderActions() }),
        React.createElement(PermissionContent_1["default"], null));
};
exports["default"] = PermissionPage;
