import React from 'react';
import {Button} from "antd";
import {SettingOutlined} from "@ant-design/icons";

const SettingButton = ({onClick}: {onClick:  React.MouseEventHandler<HTMLElement>}) => {
    return (
        <Button type={"primary"} onClick={onClick}>
            <SettingOutlined />
            Thiết lập
        </Button>
    );
};

export default SettingButton;