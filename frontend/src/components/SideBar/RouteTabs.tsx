import { useNavigate } from "react-router-dom";
import React from "react";
import { Menu } from "antd";
import IconSelector from "../../components/Icons/IconSelector";
import { NavigationItem, TabNav } from "../../navigation/NavigationItem";

const { SubMenu } = Menu;

const RoutesTabs = ({ routes }: { routes: NavigationItem[] }) => {
    const navigate = useNavigate();

    // TODO: Implement the logic to check for required permission using the required_permission array
    function checkPermisison(route: NavigationItem) {
        return true;
    }

    function displayRouteMenu(routes: NavigationItem[]) {

        function singleRoute(route: TabNav) {
            return (
                <Menu.Item key={route.key} onClick={() => {
                    navigate(route.path)
                }} icon={<IconSelector type={route.icon ? route.icon : ""} />}>
                    {route.tab_name}
                </Menu.Item>
            );
        }

        return (
            routes.filter(item => item.type !== "link").map(route => {
                if (checkPermisison(route)) {
                    if (route.type === "submenu") {
                        return (
                            <SubMenu key={route.key} title={route.tab_name}
                                icon={<IconSelector type={route.icon ? route.icon : ""} />}>
                                {displayRouteMenu(route.children)}
                            </SubMenu>
                        );
                    }
                    return singleRoute(route as TabNav);
                }
                return null;
            })
        );
    }

    return (
        <>
            {displayRouteMenu(routes)}
        </>
    );
}

export default React.memo(RoutesTabs);
