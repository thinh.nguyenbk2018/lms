import React from 'react';
import {Image, Space, Tooltip, Typography} from "antd";
import FileIconWithColor from "./FileIcon/FileIconWithColor";
import {getFileExtension, getGeneralFileType, isImageFile} from "./Utils/FileUtils";


export const MAX_NAME_LENGTH = 100;

type FilePreviewProps = {
    file: File | undefined,
    size?: number,
    showName: boolean,
    showImageInsteadOfIcon?: boolean,
}
const FilePreview = ({file, size, showName = true, showImageInsteadOfIcon = true}: FilePreviewProps) => {
    const previewSize = size || 50;
    const nameLen = file?.name.length || 0;
    const renderPreview = (file: File | undefined) => {
        if (!file) {
            return "";
            // return <Typography.Text type={"danger"}>
            //     Default icon here
            // </Typography.Text>
        }
        if (isImageFile(file) && showImageInsteadOfIcon) {
            return <Image width={previewSize} src={URL.createObjectURL(file)}/>;
        } else {
            const ext = getFileExtension(file);
            const extBacked = ext || getGeneralFileType(file);
            return <>
                {/*<Tooltip title={file.name}>*/}
                <div style={{display: 'flex', alignItems: 'center'}}>
                    <span style={{marginRight: '0.5rem'}}>
                        <FileIconWithColor width={previewSize} extension={extBacked} height={undefined}/>
                    </span>
                    <span>{file.name}</span>
                </div>
                {/*</Tooltip>*/}
            </>;
        }
    }
    return (
        <Space direction={"vertical"}>
            {renderPreview(file)}
            {
                (showName && nameLen <= MAX_NAME_LENGTH && file?.name) && <Typography.Text>
                    {file.name}
                </Typography.Text>
            }
        </Space>
    );
};

export default FilePreview;