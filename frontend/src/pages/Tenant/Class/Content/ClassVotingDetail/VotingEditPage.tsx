const EditVotingPage = () => {
	return (
		<>
			{/* <Formik
	initialValues={
		votingInfo || {
			title: "",
			description: "",
			isAllowedToAddChoice: false,
			choices: [],
		}
	}
	validationSchema={Yup.object({
		title: Yup.string().required(FORM_DEFAULT_MESSAGES.REQUIRED),
		description: Yup.string().required(FORM_DEFAULT_MESSAGES.REQUIRED),
	})}
	onSubmit={(values) => {
		console.log("submiting voting info: ", values);
	}}
>
	{({ values, handleSubmit, dirty, isValid, errors }) => {
		return (
			<Form
				labelCol={{ span: 8 }}
				wrapperCol={{ span: 24 }}
				layout="horizontal"
			>
				<FormItem
					label={"Tên cuộc bâu chọn"}
					name={ACTIVITY_FIELD_NAMES.VOTING.TITLE}
				>
					<Input name={ACTIVITY_FIELD_NAMES.VOTING.TITLE} />
				</FormItem>
				<FormItem
					label={"Mô tả thêm"}
					name={ACTIVITY_FIELD_NAMES.VOTING.DESCRIPTION}
				>
					<Input
						name={ACTIVITY_FIELD_NAMES.VOTING.DESCRIPTION}
						placeholder={"Mô tả thêm"}
					/>
				</FormItem>
				<FormItem
					label={"Cho phép thêm lựa chọn mới"}
					name={ACTIVITY_FIELD_NAMES.VOTING.ALLOW_ADDING_CHOICES}
				>
					<Space>
						<Switch
							name={ACTIVITY_FIELD_NAMES.VOTING.ALLOW_ADDING_CHOICES}
						/>
						<Tooltip title="Lựa chọn này cho phép người tham gia bỏ phiếu thêm lựa chọn mới">
							<InfoCircleOutlined
								style={{ color: "rgba(0,0,0,.45)" }}
							/>
						</Tooltip>
					</Space>
				</FormItem>
				<FormItem
					label={"Các lựa chọn"}
					wrapperCol={{ span: 4 }}
					name={ACTIVITY_FIELD_NAMES.VOTING.CHOICES}
				>
					{optionList.map((item, index) => {
						return (
							<span key={index}>
								<Typography.Link>{item}</Typography.Link>
								<Progress
									strokeWidth={20}
									trailColor={"grey"}
									percent={0}
								/>
							</span>
						);
					})}
					<Button
						type="dashed"
						onClick={() => setVisible(true)}
						style={{
							marginTop: `${optionList.length > 0 ? "1em" : "0"}`,
						}}
					>
						Thêm lựa chọn
					</Button>
				</FormItem>
				<OriginalForm.Item wrapperCol={{ offset: 8, span: 16 }}>
					<Button type="primary" htmlType="submit">
						Xong
					</Button>
				</OriginalForm.Item>
			</Form>
		);
	}}
	<Modal
		title="Nhập lựa chọn mới"
		visible={visible}
		onOk={handleOk}
		onCancel={handleCancel}
	>
		<OriginalInput
			allowClear
			onChange={(event) => setNewOption(event.target.value)}
		/>
		{showRequiredMessage && (
			<Typography.Text type={"danger"}>
				Trường này là bắt buộc
			</Typography.Text>
		)}
	</Modal>
</Formik> */}
		</>
	);
};

export default EditVotingPage;
