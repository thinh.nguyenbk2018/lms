import React from "react";
import * as Yup from "yup";
import { Field, Formik } from "formik";
import { Form } from "formik-antd";
import { Button } from "antd";
import TestSearchSelectField from "../../../components/CustomFields/TestSearchSelectField/TestSearchSelectField";
import CourseSearchSelect from "../../../components/Courses/Forms/CourseSearchSelect/CourseSearchSelect";
import TestSearchSelect from "../../../components/Courses/Forms/TestSeachSelect/TestSearchSelect";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";
const schema = Yup.object({
	testId: Yup.number().required("REQ").positive("ID Must be posi"),
});

const SearchSelectTestSuite = () => {
	return (
		<>
			<Formik
				initialValues={{ testId: -1 }}
				validationSchema={schema}
				onSubmit={(values) => {
					AntdNotifier.success(`Submitted: ${JSON.stringify(values)}`);
				}}
			>
				{({ values, errors, handleSubmit }) => {
					return (
						<Form>
							<Field
								name={"testId"}
								placeholder={"Type article content"}
								component={TestSearchSelectField}
								label={"Test"}
								courseIdToSearch={1}
							/>
							<Button onClick={() => handleSubmit()}>Submit</Button>
							{JSON.stringify(values)}
							{JSON.stringify(errors)}
						</Form>
					);
				}}
			</Formik>
			<CourseSearchSelect />
			<TestSearchSelect courseIdToSearch={1} />
		</>
	);
};

export default SearchSelectTestSuite;
