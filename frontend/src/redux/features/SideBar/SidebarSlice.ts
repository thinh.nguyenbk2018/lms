import {RootState} from "../../store/store";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {ID} from "../../interfaces/types";

const KEY = "class_sidebar";

type SidebarSlice = {
    classIdInFocus: ID | undefined,
    courseIdInFocus: ID | undefined,
    mainSidebarCollapse: boolean,
    showSidebar: boolean
}
const initialState : SidebarSlice = {
    classIdInFocus: undefined,
    courseIdInFocus:  undefined,
    mainSidebarCollapse: false,
    showSidebar: true
}

export const sideBarSlice = createSlice({
    name: KEY,
    initialState,
    reducers: {
        setShowSidebar: (state, action: PayloadAction<boolean>) => {
            const {payload} = action;
            state.showSidebar = payload;
        },
        setMainSidebarCollapse: (state,action:PayloadAction<boolean>) => {
            const {payload} = action;
            state.mainSidebarCollapse = payload;
        },
        setClassIdInFocus: (state,action:PayloadAction<number>) => {
            const {payload} = action;
            state.classIdInFocus = payload;
            state.mainSidebarCollapse = true;
        },
        setCourseIdInFocus: (state,action:PayloadAction<number>) => {
            const {payload} = action;
            state.courseIdInFocus = payload;
            state.mainSidebarCollapse = true;
        },
        closeClassSidebar: (state) => {
            state.classIdInFocus = undefined;
            if (!state.courseIdInFocus) {
                state.mainSidebarCollapse = false;
            }
        },
        closeCourseSidebar: (state) => {
            state.courseIdInFocus = undefined;
            if (!state.classIdInFocus) {
                state.mainSidebarCollapse = false;
            }
        }
    },
});
// collapses = inClass || in Course
export const sideBarSelector = {
    selectClassIdInFocus: (state: RootState) => state.sidebar.classIdInFocus,
    selectCourseIdInFocus: (state: RootState) => state.sidebar.courseIdInFocus,
    selectMainSidebarCollapse: (state: RootState) => state.sidebar.mainSidebarCollapse,
    selectShowSidebar: (state: RootState) => state.sidebar.showSidebar,
}

export const sideBarActions = sideBarSlice.actions;

export default sideBarSlice;