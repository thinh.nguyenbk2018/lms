import { Todo } from "./TodoComponent";
import TodoList, { todoUrl } from "./TodoList";
import { render, screen, waitFor } from "@testing-library/react";
import server from "@mocks/server";
import { rest } from "msw";

describe("Todo list behavior", () => {
    it("should fetch the list properly", async () => {
        const { findByText } = render(<TodoList />);

        // * Uncomment and check command terminal to understand how things are mocked
        // const todoListTitle = await findByText(/List of Todo/i);
        // screen.debug();
        // expect(todoListTitle).toBeInTheDocument();

        // * Uncomment and check command terminal to understand how things are mocked  v1.
        // await waitFor(() => screen.getByRole('heading'))
        // screen.debug();
        const todoTask1Title = await findByText(/Task1/i);
        // screen.debug();
        expect(todoTask1Title).toBeInTheDocument();
    });

    it("should handle error gracefully", async () => {
        // * This will override the handlers added inside the testServer.ts
        server.use(
            rest.get(todoUrl, (req, res, ctx) => {
                return res(ctx.status(404));
            })
        );
        const { findByText } = render(<TodoList />);
        const todoErrorMessage = await findByText(
            /Request failed with status code 404/i
        );
        // screen.debug();
        expect(todoErrorMessage).toBeInTheDocument();
    });
});
