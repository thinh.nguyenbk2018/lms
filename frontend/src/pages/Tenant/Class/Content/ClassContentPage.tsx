import './ClassContentPage.less';

import {DeleteOutlined} from '@ant-design/icons';
import {faBook, faEye, faPenToSquare} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Button, Collapse, Dropdown, Form, Modal, Row, Space, Switch, Tooltip} from 'antd';
import {Formik} from 'formik';
import {FormItem, Input as InputFormik} from 'formik-antd';
import {useNavigate, useParams} from 'react-router-dom';
import * as Yup from 'yup';

import ContentHeader from '../../../../components/ContentHeader/ContentHeader';
import {addIconSvg} from '@components/Icons/SvgIcons';
import NewActivityMenu from '../../../../components/TeachingContent/NewActivityMenu';
import ApiErrorWithRetryButton from '../../../../components/Util/ApiErrorWithRetryButton';
import {useAppDispatch, useAppSelector} from '@hooks/Redux/hooks';
import useClassCourseInformation from '../../../../hooks/Routing/useClassCourseInformation';
import useClassSidebar from '../../../../hooks/Routing/useClassSidebar';
import {ROUTING_CONSTANTS} from '../../../../navigation/ROUTING_CONSTANTS';
import API_ERRORS from '../../../../redux/features/ApiErrors';
import {errorArrayHandlingHelper} from '@redux/features/CentralAPI';
import {
    useChangeClassItemPlacementMutation,
    useClassChapterPlacementMutation,
    useCreateClassQuizMutation,
    useCreateClassUnitMutation,
    useCreateClassVotingMutation,
    useDeleteClassChapterMutation,
    useDeleteClassQuizMutation,
    useDeleteClassUnitMutation,
    useDeleteClassVotingMutation,
    useGetClassContentQuery,
    useGetClassQuizDetailQuery,
    useGetClassUnitDetailQuery,
    useGetClassVotingDetailQuery,
    useHideQuizMutation,
    useHideUnitMutation,
    useLazyGetClassContentGroupByTextbooksQuery,
    usePublishQuizMutation,
    usePublishUnitMutation,
    useUpdateClassChapterMutation,
    useUpdateClassQuizMutation,
    useUpdateClassUnitMutation,
    useUpdateClassVotingMutation,
} from '../../../../redux/features/Class/ClassAPI';
import {
    classContentActions,
    classContentSelectors
} from '../../../../redux/features/Class/ClassContent/ClassContentSlice';
import {DEFAULT_FRONTEND_ID, ID} from '../../../../redux/interfaces/types';
import {
    Activity,
    ActivityType,
    ChapterGroupByTextbook,
    isPublishableActivity,
    PublishableAcitivty,
    TextbookUnit,
} from '../../../../typescript/interfaces/courses/CourseTypes';
import AntdNotifier from '../../../../utils/AntdAnnouncer/AntdNotifier';
import antdNotifier from '../../../../utils/AntdAnnouncer/AntdNotifier';
import {FORM_DEFAULT_MESSAGES} from '../../../../utils/FormUtils/formUtils';
import {replacePathParams} from '../../../../utils/PathPatcherHepler/PathPatcher';
import LoadingPage from '../../../UtilPages/LoadingPage';
import {HideIcon, PublishIcon, RenderActivityIcon} from '../../Courses/CourseContent/CourseContentPage';
import {QuizItemForm, UnitItemForm, VotingItemForm} from '../../Courses/CourseDetail/Activity/ActivityWithForm';
import {ChapterPlacementInfo,} from '../../Courses/CourseDetail/Chapter/DraggableChapter';
import {quizSchema, unitSchema} from '../../Courses/CourseDetail/CreateOrUpdateCoursePage1';
import {votingErrorsHandler} from './ClassVotingDetail/ClassVotingDetailPage';
import React, {useEffect, useState} from 'react';
import {mapType} from "@pages/UtilPages/MappingUtils";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {blue} from "@ant-design/colors";

const {Panel} = Collapse;

const getDeletePermission = (type: string): PermissionEnum => {
    if (type == 'quiz') {
        return PermissionEnum.DELETE_QUIZ_CLASS;
    }
    if (type == 'unit') {
        return PermissionEnum.DELETE_UNIT_CLASS;
    }
    return PermissionEnum.DELETE_VOTING;
}

const getUpdatePermission = (type: string): PermissionEnum => {
    if (type == 'quiz') {
        return PermissionEnum.UPDATE_QUIZ_CLASS;
    }
    if (type == 'unit') {
        return PermissionEnum.UPDATE_UNIT_CLASS;
    }
    return PermissionEnum.UPDATE_VOTING;
}

export const AddQuizItemModal = ({
                                     courseIdToSearch,
                                 }: {
    courseIdToSearch: number;
}) => {
    const {classId} = useParams();
    const isAddingQuiz = useAppSelector(classContentSelectors.selectIsAddingQuiz);

    const dispatch = useAppDispatch();

    const quizItemInEdit = useAppSelector(
        classContentSelectors.selectQuizInUpdate
    );
    const chapterToUpdate = useAppSelector(
        classContentSelectors.selectChapterInEdit
    );
    const [createQuiz] = useCreateClassQuizMutation();
    const [updateQuiz] = useUpdateClassQuizMutation();

    const {
        data: quizData,
        isSuccess,
        isError,
        isFetching,
    } = useGetClassQuizDetailQuery(
        {
            classId: Number(classId),
            quizId: Number(quizItemInEdit?.id),
        },
        {
            skip:
                isNaN(Number(quizItemInEdit?.id)) ||
                isNaN(Number(chapterToUpdate)) ||
                isNaN(Number(classId)),
        }
    );

    const defaultQuiz = {
        id: -1,
        title: "",
        description: "",
        tag: "",
        type: "quiz" as const,
        order: -1,
    };

    const [internalQuiz, setInternalQuiz] = useState(defaultQuiz);

    useEffect(() => {
        if (isAddingQuiz || !!quizItemInEdit) {
            if (quizItemInEdit != undefined && quizData != undefined) {
                setInternalQuiz(quizData)
            } else {
                setInternalQuiz(defaultQuiz);
            }
        }
    }, [quizData, isAddingQuiz]);

    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={
                    internalQuiz
                }
                validateOnChange={false}
                validationSchema={quizSchema}
                onSubmit={(values, {resetForm, setSubmitting}) => {
                    const classToUpdate = Number(classId);
                    if (isNaN(classToUpdate) || !chapterToUpdate) {
                        AntdNotifier.error(
                            "Tạm thời không thêm chỉnh sửa content được. Vui lòng thử lại sau hoặc liên hệ bộ phận bảo trì"
                        );
                        return;
                    }
                    if (quizItemInEdit) {
                        updateQuiz({
                            classId: classToUpdate,
                            quizItem: values,
                        }).unwrap()
                            .then(res => {
                                antdNotifier.success("Cập nhật bài kiểm tra thành công");
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultQuiz});
                                setSubmitting(false);
                            })
                            .catch(err => {
                                if (err?.data) {
                                    AntdNotifier.error(err?.data.message ? err?.data?.message : "Có lỗi trong quá trình cập nhật bài kiểm tra.")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình cập nhật bài kiểm tra.")
                                }
                                setSubmitting(false);
                            });
                    } else {
                        createQuiz({
                            classId: classToUpdate,
                            chapterId: chapterToUpdate,
                            quizItem: values,
                        }).unwrap()
                            .then(res => {
                                antdNotifier.success("Thêm bài kiểm tra thành công");
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultQuiz});
                                setSubmitting(false);
                            })
                            .catch(err => {
                                if (err?.data) {
                                    AntdNotifier.error(err?.data.message ? err?.data?.message : "Có lỗi trong quá trình thêm bài kiểm tra.")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình thêm bài kiểm tra.")
                                }
                                setSubmitting(false);
                            });
                    }
                }}
            >
                {({values, errors, handleSubmit, isValid, dirty, resetForm, isSubmitting}) => {
                    return (
                        <Modal
                            maskClosable={false}
                            title={!!quizItemInEdit ? "Cập nhật bài kiểm tra" : "Thêm bài kiểm tra"}
                            visible={isAddingQuiz || !!quizItemInEdit}
                            onOk={() => {
                                handleSubmit();
                                // resetForm({values: defaultQuiz});
                                // dispatch()
                            }}
                            okButtonProps={{
                                disabled: !dirty || isSubmitting,
                                loading: isSubmitting
                            }}
                            onCancel={() => {
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultQuiz});
                                setInternalQuiz(defaultQuiz);
                            }}
                            okText={!!quizItemInEdit ? "Cập nhật" : "Thêm"}
                            cancelText={"Hủy"}
                        >
                            {<QuizItemForm type={"quiz"} courseIdToSearch={courseIdToSearch}
                                                                                 isUpdating={!!quizItemInEdit} isFromCourse={values["isFromCourse"]}/>}

                            {/*<JsonDisplay cardProps={{title: "Values"}} obj={values}/>*/}
                            {/*<JsonDisplay cardProps={{title: "Errors"}} obj={errors}/>*/}
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

export const AddUnitItemModal = () => {
    const {classId} = useParams();
    const isAddingUnit = useAppSelector(classContentSelectors.selectIsAddingUnit);

    const unitItemInEdit = useAppSelector(
        classContentSelectors.selectUnitInUpdate
    );

    const chapterToUpdate = useAppSelector(
        classContentSelectors.selectChapterInEdit
    );
    const [createUnit] = useCreateClassUnitMutation();
    const [updateUnit] = useUpdateClassUnitMutation();

    const {
        data: unitData,
        isSuccess,
        isFetching,
    } = useGetClassUnitDetailQuery(
        {
            classId: Number(classId),
            unitId: Number(unitItemInEdit?.id)
        },
        {
            skip:
                isNaN(Number(unitItemInEdit?.id)) ||
                isNaN(Number(classId)),
        }
    );

    const defaultUnit = {
        type: "unit" as const,
        title: "",
        textbooks: [] as TextbookUnit[],
        content: "",
        id: DEFAULT_FRONTEND_ID,
        order: -1,
    };

    const [internalUnit, setInternalUnit] = useState(defaultUnit);

    useEffect(() => {
        if (isAddingUnit || !!unitItemInEdit) {
            if (unitItemInEdit != undefined && unitData != undefined) {
                setInternalUnit(unitData)
            } else {
                setInternalUnit(defaultUnit);
            }
        }
    }, [unitData, isAddingUnit]);


    const dispatch = useAppDispatch();
    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={
                    internalUnit
                }
                validateOnChange={false}
                validationSchema={unitSchema}
                onSubmit={(values, {resetForm}) => {
                    // console.log("Unit: ", values);
                    const classToUpdate = Number(classId);
                    if (isNaN(classToUpdate)) {
                        AntdNotifier.error(
                            "Tạm thời không thêm unit được. Vui lòng thử lại sau hoặc liên hệ bộ phận bảo trì"
                        );
                        return;
                    }
                    if (unitItemInEdit) {
                        AntdNotifier.handlePromise(
                            updateUnit({
                                classId: classToUpdate,
                                unitItem: values,
                            }).unwrap(),
                            "Cập nhật bài học",
                            () => {
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultUnit});
                            },
                            () => {
                                dispatch(classContentActions.doneAddingActivity());
                            }
                        );
                    } else {
                        AntdNotifier.handlePromise(
                            createUnit({
                                classId: classToUpdate,
                                chapterId: chapterToUpdate || -1,
                                unitItem: values,
                            }).unwrap(),
                            "Thêm bài học",
                            () => {
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultUnit});
                            },
                            () => {
                                dispatch(classContentActions.doneAddingActivity());
                            }
                        );
                    }
                }}
            >
                {({values, errors, handleSubmit, resetForm, isValid, dirty}) => {
                    return (
                        <Modal
                            title={!!unitItemInEdit ? 'Cập nhật bài học' : 'Thêm bài học'}
                            visible={isAddingUnit || !!unitItemInEdit}
                            onOk={() => {
                                handleSubmit();
                            }}
                            onCancel={() => {
                                // resetForm({values: defaultUnit})
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultUnit});
                                setInternalUnit(defaultUnit);
                            }}
                            wrapClassName={'content-page-modal-wrapper'}
                            maskClosable={false}
                            okButtonProps={{disabled: !dirty}}
                            okText={!!unitItemInEdit ? 'Cập nhật' : 'Thêm bài học'}
                            cancelText={'Hủy'}
                            bodyStyle={{overflowY: 'auto', maxHeight: 'calc(100vh - 250px)'}}
                        >
                            {<UnitItemForm type={"unit"}/>}
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

const votingCreationSchema = Yup.object({
    title: Yup.string().required(FORM_DEFAULT_MESSAGES.REQUIRED),
    isAllowedToAddChoice: Yup.boolean().required(FORM_DEFAULT_MESSAGES.REQUIRED),
});
export const VotingModal = () => {
    const {classId} = useParams();
    const isAddingVoting = useAppSelector(
        classContentSelectors.selectIsAddingVoting
    );

    const votingItemInEdit = useAppSelector(
        classContentSelectors.selectVotingInUpdate
    );

    const chapterToUpdate = useAppSelector(
        classContentSelectors.selectChapterInEdit
    );
    const [createVoting] = useCreateClassVotingMutation();
    const [updateVoting] = useUpdateClassVotingMutation();


    const {
        data: votingData,
        isSuccess,
        isFetching,
    } = useGetClassVotingDetailQuery(
        {
            classId: Number(classId),
            votingId: Number(votingItemInEdit?.id)
        },
        {
            skip:
                isNaN(Number(votingItemInEdit?.id)) ||
                isNaN(Number(classId)),
        }
    );

    const defaultVoting = {
        id: DEFAULT_FRONTEND_ID,
        type: "vote" as const,
        title: "",
        description: "",
        isAllowedToAddChoice: false,
    };

    const [internalVoting, setInternalVoting] = useState(defaultVoting);

    useEffect(() => {
        if (isAddingVoting || !!votingItemInEdit) {
            if (votingItemInEdit != undefined && votingData != undefined) {
                setInternalVoting(votingData)
            } else {
                setInternalVoting(defaultVoting);
            }
        }
    }, [votingData, isAddingVoting]);

    const dispatch = useAppDispatch();
    return (
        <>
            <Formik
                enableReinitialize={true}
                initialValues={
                    internalVoting
                }
                validateOnChange={false}
                validationSchema={votingCreationSchema}
                onSubmit={(values, {resetForm}) => {
                    const classToUpdate = Number(classId);
                    if (isNaN(classToUpdate) || (!chapterToUpdate && !votingItemInEdit)) {
                        AntdNotifier.error(
                            "Tạm thời không thể thêm / cập nhật cuộc bình chọn. Vui lòng thử lại sau hoặc liên hệ bộ phận bảo trì"
                        );
                        return;
                    }
                    if (votingItemInEdit) {
                        AntdNotifier.handlePromise(
                            updateVoting({
                                classId: classToUpdate,
                                votingId: Number(values.id),
                                votingInfo: values,
                            }).unwrap(),
                            "Cập nhật cuộc bình chọn",
                            () => {
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultVoting});
                            },
                            (err) => {
                                votingErrorsHandler(err);
                                dispatch(classContentActions.doneAddingActivity());
                            }
                        );
                    } else {
                        AntdNotifier.handlePromise(
                            createVoting({
                                classId: classToUpdate,
                                chapterId: chapterToUpdate || DEFAULT_FRONTEND_ID,
                                votingInfo: values,
                            }).unwrap(),
                            "Thêm cuộc bình chọn",
                            () => {
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultVoting});
                            },
                            (err) => {
                                votingErrorsHandler(err);
                                dispatch(classContentActions.doneAddingActivity());
                            }
                        );
                    }
                }}
            >
                {({values, errors, handleSubmit, resetForm}) => {
                    return (
                        <Modal
                            title={votingItemInEdit ? 'Cập nhật bình chọn' : 'Tạo bình chọn'}
                            visible={isAddingVoting || !!votingItemInEdit}
                            onOk={() => handleSubmit()}
                            onCancel={() => {
                                dispatch(classContentActions.doneAddingActivity());
                                resetForm({values: defaultVoting});
                                setInternalVoting(defaultVoting);
                            }}
                            okText={votingItemInEdit ? 'Cập nhật' : 'Tạo bình chọn'}
                            cancelText={'Hủy'}
                        >
                            <VotingItemForm type={"vote"}/>
                            {/*<JsonDisplay cardProps={{title: "values"}} obj={values}/>*/}
                            {/*<JsonDisplay cardProps={{title: "values"}} obj={errors}/>*/}
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

export const EditChapterTitleModal = () => {
    const [updateChapter] = useUpdateClassChapterMutation();
    const {classId} = useParams();
    const dispatch = useAppDispatch();
    const {isChapterInEdit, chapterTitle} = useAppSelector(
        classContentSelectors.selectIsChapterInfoBeingEdited
    );
    const chapterIdInEdit = useAppSelector(
        classContentSelectors.selectChapterInEdit
    );

    return (
        <Formik
            enableReinitialize={true}
            initialValues={{'title': chapterTitle}}
            validationSchema={Yup.object({
                title: Yup.string().required("Tên chương không được để trống"),
            })}
            onSubmit={(values) => {
                if (!chapterIdInEdit || isNaN(Number(classId))) {
                    AntdNotifier.error(
                        "Tạm thời không update thông tin chapter được. Liên hệ bộ phận bảo trì"
                    );
                    return;
                }
                AntdNotifier.handlePromise(
                    updateChapter({
                        classId: Number(classId),
                        chapterId: chapterIdInEdit,
                        chapterInfo: {id: chapterIdInEdit, title: values.title},
                    }).unwrap(),
                    "Cập nhật chương",

                    () => {
                        dispatch(classContentActions.doneEditingChapter());
                    },
                    () => {
                        dispatch(classContentActions.doneEditingChapter());
                    }
                );
            }}
        >
            {({handleSubmit}) => {
                return (
                    <Modal
                        title={"Tên chương"}
                        visible={isChapterInEdit}
                        onOk={() => {
                            handleSubmit();
                        }}
                        onCancel={() => {
                            dispatch(classContentActions.doneEditingChapter());
                        }}
                        cancelText={'Hủy'}
                        okText={'Cập nhật'}
                    >
                        <Form preserve={false}>
                            <FormItem required={true} name="title" showValidateSuccess label={"Tiêu đề"}>
                                <InputFormik name="title"/>
                            </FormItem>
                        </Form>
                    </Modal>
                );
            }}
        </Formik>
    );
};

export const teachingContentErrorHandler = (err: any) => {
    const {
        CHAPTER_NOT_FOUND,
        DRAG_AND_DROP_IS_NOT_ALLOWED,
        UNSUPPORTED_CONTENT_TYPE,
    } = API_ERRORS.TEACHING_CONTENT;
    errorArrayHandlingHelper(
        err,
        [
            CHAPTER_NOT_FOUND,
            "Không tìm thấy chương, hãy thử load lại trang để đảm bảo dữ liệu mới nhất ",
        ],
        [DRAG_AND_DROP_IS_NOT_ALLOWED, "DRAG_AND_DROP_IS_NOT_ALLOWED"],
        [
            UNSUPPORTED_CONTENT_TYPE,
            "Chưa hỗ trợ / không tìm thấy loại hoạt động này",
        ]
    );
};

const ClassContentPage = () => {
    const {classId} = useParams();
    const [] = useClassSidebar();

    const [textbookViewMode, setTextbookViewMode] = useState(false);

    const {
        classInfo,
        isFetchingClass,
        isSuccessClass,
    } = useClassCourseInformation(Number(classId));
    const {
        data: classContent,
        isSuccess,
    } = useGetClassContentQuery(Number(classId), {
        skip: isNaN(Number(classId)),
    });

    // * Chapter
    const [changeChapterPlacement] = useClassChapterPlacementMutation();

    const [changeItemPlacement] = useChangeClassItemPlacementMutation();

    const onExchangePlace = (
        from: ChapterPlacementInfo,
        to: ChapterPlacementInfo
    ) => {
        AntdNotifier.handlePromise(
            changeChapterPlacement({classId: Number(classId), from, to}).unwrap(),
            "Thay dổi vị trí chương",
            () => {
            },
            (err) => {
                teachingContentErrorHandler(err);
            }
        );
    };

    const navigate = useNavigate();

    // * Redux store state
    const dispatch = useAppDispatch();

    const [deleteChapter] = useDeleteClassChapterMutation();


    // * Quiz
    const [deleteQuiz] = useDeleteClassQuizMutation();
    const [publishQuiz] = usePublishQuizMutation();
    const [hideQuiz] = useHideQuizMutation();
    // * Chatper
    const [deleteUnit] = useDeleteClassUnitMutation();
    const [publishUnit] = usePublishUnitMutation();
    const [hideUnit] = useHideUnitMutation();
    const [deleteVoting] = useDeleteClassVotingMutation();

    const [fetchClassContentGroupByTextbook, {
        data: classContentGroupByTextbook,
        isLoading: classContentGroupByTextbookLoading,
        isFetching: classContentGroupByTextbookLoadingFetching,
        isSuccess: classContentGroupByTextbookSuccess
    }] = useLazyGetClassContentGroupByTextbooksQuery();

    useEffect(() => {
        if (textbookViewMode && classId) {
            fetchClassContentGroupByTextbook(Number(classId));
        }
    }, [textbookViewMode]);

    const handleAccessibilityChange = (item: PublishableAcitivty) => {
        switch (item.type) {
            case "quiz":
                switch (item.state) {
                    case "PRIVATE":
                        // * Thing is private --> We publish it
                        publishQuiz({
                            classId: Number(classId),
                            quizId: item.id,
                        })
                            .unwrap()
                            .then(res => antdNotifier.success("Hiện bài kiểm tra thành công."))
                            .catch(error => {
                                if (error?.data) {
                                    AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình hiện bài kiểm tra")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình hiện bài kiểm tra")
                                }
                            })
                        return;
                    case "PUBLIC":
                        hideQuiz({classId: Number(classId), quizId: item.id})
                            .unwrap()
                            .then(res => antdNotifier.success("Ẩn bài kiểm tra thành công."))
                            .catch(error => {
                                if (error?.data) {
                                    AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình ẩn bài kiểm tra")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình ẩn bài kiểm tra")
                                }
                            })
                        return;
                    default:
                        return;
                }
            case "unit":
                switch (item.state) {
                    case "PRIVATE":
                        publishUnit({
                            classId: Number(classId),
                            unitId: item.id,
                        })
                            .unwrap()
                            .then(res => antdNotifier.success("Hiện bài học thành công."))
                            .catch(error => {
                                if (error?.data) {
                                    AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình hiện bài học")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình hiện bài học")
                                }
                            })
                        return;
                    case "PUBLIC":
                        hideUnit({classId: Number(classId), unitId: item.id})
                            .unwrap()
                            .then(res => antdNotifier.success("Ẩn bài học thành công."))
                            .catch(error => {
                                if (error?.data) {
                                    AntdNotifier.error(error?.data.message ? error?.data?.message : "Có lỗi trong quá trình ẩn bài học")
                                } else {
                                    AntdNotifier.error("Có lỗi trong quá trình ẩn bài học")
                                }
                            })
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    };
    // * Add activity immediately, user can edit the tree later on
    const handleAddingActivity = (type: ActivityType, chapterId: ID) => {
        dispatch(
            classContentActions.addingActivity({chapterToEdit: chapterId, type})
        );
    };

    if (!classId || isNaN(Number(classId))) {
        AntdNotifier.error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
        return (
            <>
                <ApiErrorWithRetryButton onRetry={() => {
                }}/>
            </>
        );
    }

    const isAbleEdit = (item: Activity) : boolean => {
        return !(item.isFromCourse && item.type == 'unit');

    }

    if (isSuccessClass && ((!textbookViewMode && isSuccess) || (textbookViewMode && classContentGroupByTextbookSuccess)) && classInfo?.course?.id) {
        return (
            <>
                <ContentHeader
                    title={
                        "Nội dung giảng dạy"
                    }
                    action={<>
                        <Switch
                            onClick={(checked: boolean) => setTextbookViewMode(checked)}
                            style={{marginRight: "1rem"}}
                            checked={textbookViewMode}
                        />
                        <span>Xem theo giáo trình</span>
                    </>}
                />
                {/*{userInfo?.accountType === 'STAFF' &&*/}
                {/*    <div className={'class-content-instruction'}>*Chỉ được phép chỉnh sửa những nội dung được tạo trong lớp học.</div>}*/}
                {!textbookViewMode && <div className={`class-content-container`}>
                    <>
                        {classContent?.map((chapterItem, chapterIdx) => {
                            return (
                                <>
                                    <Collapse className="chapter-wrapper">
                                        <Panel
                                            className="chapter-panel"
                                            showArrow={false}
                                            key={chapterIdx}
                                            header={
                                                <>
                                                    <p className={"chapter-title"}>
                                                        {chapterItem.title}
                                                    </p>
                                                </>
                                            }
                                        >
                                            <>
                                                <div className="activity-list-container">
                                                    {chapterItem.activities.map((item) => {
                                                        return (
                                                            <>
                                                                <div
                                                                    className={"activity-item-container"}
                                                                >
                                                                    <Space>
                                                                        <RenderActivityIcon
                                                                            type={item.type}
                                                                        />
                                                                        <p className={"activity-title"}>
                                                                            {item.title}
                                                                        </p>
                                                                    </Space>
                                                                    <div className={"action-container"}>
                                                                        {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                                            permissions={[getUpdatePermission(item.type)]}>
                                                                            {isPublishableActivity(item) &&
                                                                                (item.state === "PRIVATE" ? (
                                                                                    <Tooltip
                                                                                        title="Hiện cho học viên">
                                                                                        <PublishIcon
                                                                                            className="icon publish action-icon-activity"
                                                                                            onClick={() => {
                                                                                                handleAccessibilityChange(item);
                                                                                            }}
                                                                                        />
                                                                                    </Tooltip>
                                                                                ) : (
                                                                                    <Tooltip
                                                                                        title="Nhấp vào để ẩn đi">
                                                                                        <HideIcon
                                                                                            className="icon hide action-icon-activity"
                                                                                            onClick={() => {
                                                                                                handleAccessibilityChange(item);
                                                                                            }}
                                                                                        />
                                                                                    </Tooltip>
                                                                                ))}
                                                                        </PermissionRenderer>}
                                                                        <PermissionRenderer
                                                                            permissions={[PermissionEnum.VIEW_QUIZ_CLASS]}>
                                                                            <Tooltip title={"Chi tiết"}>
                                                                                <FontAwesomeIcon
                                                                                    onClick={() => {
                                                                                        switch (item.type) {
                                                                                            case "quiz":
                                                                                                navigate(
                                                                                                    replacePathParams(
                                                                                                        ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                                                                                        [
                                                                                                            ":classId",
                                                                                                            Number(classId),
                                                                                                        ],
                                                                                                        [
                                                                                                            ":quizItemId",
                                                                                                            item.id,
                                                                                                        ]
                                                                                                    )
                                                                                                );
                                                                                                return;
                                                                                            case "unit":
                                                                                                navigate(
                                                                                                    replacePathParams(
                                                                                                        ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE,
                                                                                                        [
                                                                                                            ":classId",
                                                                                                            Number(classId),
                                                                                                        ],
                                                                                                        [
                                                                                                            ":unitItemId",
                                                                                                            item.id,
                                                                                                        ]
                                                                                                    )
                                                                                                );
                                                                                                return;
                                                                                            case "vote":
                                                                                                navigate(
                                                                                                    replacePathParams(
                                                                                                        ROUTING_CONSTANTS.CLASS_VOTING_ITEM_DETAIL_PAGE,
                                                                                                        [
                                                                                                            ":classId",
                                                                                                            Number(classId),
                                                                                                        ],
                                                                                                        [
                                                                                                            ":votingItemId",
                                                                                                            item.id,
                                                                                                        ]
                                                                                                    )
                                                                                                );
                                                                                                return;
                                                                                            default:
                                                                                                return;
                                                                                        }
                                                                                    }}
                                                                                    className={"icon detail action-icon-activity"}
                                                                                    icon={faEye}
                                                                                />
                                                                            </Tooltip>
                                                                        </PermissionRenderer>

                                                                        {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                                            permissions={[getUpdatePermission(item.type)]}>
                                                                            {isAbleEdit(item) && <Tooltip
                                                                                title={"Chỉnh sửa"}>
                                                                                <FontAwesomeIcon
                                                                                    className={"icon edit action-icon-activity"}
                                                                                    icon={faPenToSquare}
                                                                                    onClick={() => {
                                                                                        dispatch(
                                                                                            classContentActions.updatingActivity(
                                                                                                {
                                                                                                    chapterToEdit:
                                                                                                    chapterItem.id,
                                                                                                    activity: item,
                                                                                                }
                                                                                            )
                                                                                        );
                                                                                    }}
                                                                                />
                                                                            </Tooltip>}
                                                                        </PermissionRenderer>}


                                                                        {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                                            permissions={[getDeletePermission(item.type)]}>
                                                                            {!item.isFromCourse &&
                                                                                <Tooltip title={"Xóa"}>
                                                                                    <DeleteOutlined
                                                                                        onClick={() => {
                                                                                            confirmationModal(mapType(item.type), item.title, () => {
                                                                                                switch (item.type) {
                                                                                                    case "quiz":
                                                                                                        AntdNotifier.handlePromise(
                                                                                                            deleteQuiz({
                                                                                                                classId:
                                                                                                                    Number(classId),
                                                                                                                quizId: item.id,
                                                                                                            }).unwrap(),
                                                                                                            "Xóa bài kiểm tra"
                                                                                                        );
                                                                                                        return;
                                                                                                    case "unit":
                                                                                                        AntdNotifier.handlePromise(
                                                                                                            deleteUnit({
                                                                                                                classId: Number(classId),
                                                                                                                unitId: item.id,
                                                                                                            }).unwrap(),
                                                                                                            "Xóa bài học"
                                                                                                        );
                                                                                                        return;
                                                                                                    case "vote":
                                                                                                        deleteVoting({
                                                                                                            classId: Number(classId),
                                                                                                            votingId: item.id,
                                                                                                        })
                                                                                                            .unwrap()
                                                                                                            .then(res => AntdNotifier.success("Xóa bình chọn thành công"))
                                                                                                        return;
                                                                                                    default:
                                                                                                        return;
                                                                                                }
                                                                                            });
                                                                                        }}
                                                                                        className={"icon delete action-icon-activity"}
                                                                                    />
                                                                                </Tooltip>}
                                                                        </PermissionRenderer>}

                                                                    </div>
                                                                </div>
                                                            </>
                                                        );
                                                    })}
                                                </div>
                                                <Row
                                                    justify="center"
                                                    style={{margin: "1.3em 2em"}}
                                                >
                                                    {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                        permissions={[PermissionEnum.CREATE_QUIZ_CLASS, PermissionEnum.CREATE_UNIT_CLASS, PermissionEnum.CREATE_VOTING]}>
                                                        <Dropdown
                                                            overlay={
                                                                <NewActivityMenu
                                                                    onChoose={(key) => {
                                                                        handleAddingActivity(
                                                                            key,
                                                                            chapterItem.id
                                                                        );
                                                                    }}
                                                                    variation={"CLASS"}
                                                                />
                                                            }
                                                        >
                                                            <Button
                                                                className={"add-button"}
                                                                onClick={(e) => {
                                                                    e.preventDefault();
                                                                }}
                                                                icon={addIconSvg}
                                                                type="default"
                                                            >
                                                                Thêm hoạt động
                                                            </Button>
                                                        </Dropdown>
                                                    </PermissionRenderer>}
                                                </Row>
                                            </>
                                        </Panel>
                                    </Collapse>
                                </>
                            );
                        })}
                        <AddQuizItemModal courseIdToSearch={classInfo.course.id || 0}/>
                        <AddUnitItemModal/>
                        <VotingModal/>
                        <EditChapterTitleModal/>
                    </>
                </div>}
                {textbookViewMode && <>
                    {classContentGroupByTextbook?.map((textbook: ChapterGroupByTextbook, index: number) => {
                        return <Collapse
                            className={'textbook-wrapper course-content-group-by-textbook-wrapper course-content-container'}>
                            <Panel
                                header={<>
                                    <FontAwesomeIcon
                                        icon={faBook}
                                        style={{margin: "0 .5em 0 0", color: blue.primary}}
                                    />
                                    <p className={"chapter-title"}>
                                        {textbook.textbookName}
                                    </p></>
                                }
                                key={textbook.textbookId}
                            >
                                {textbook.chapters?.map((chapterItem, chapterIdx) => {
                                    return (
                                        <>
                                            <Collapse className="chapter-wrapper">
                                                <Panel
                                                    className="chapter-panel-group-by-textbook"
                                                    showArrow={false}
                                                    key={chapterIdx}
                                                    header={
                                                        <>
                                                            <p className={"chapter-title"}>
                                                                {chapterItem.title}
                                                            </p>
                                                        </>
                                                    }
                                                >
                                                    <>
                                                        <div className="activity-list-container">
                                                            {chapterItem.activities.map((item, index) => {
                                                                return (
                                                                    <>

                                                                        <div
                                                                            className={"activity-item-container"}
                                                                        >
                                                                            <Space>
                                                                                <RenderActivityIcon
                                                                                    type={item.type}
                                                                                />
                                                                                <p className={"activity-title"}>
                                                                                    {item.title}
                                                                                </p>
                                                                            </Space>
                                                                            <div
                                                                                className={"action-container"}>
                                                                                {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                                                    permissions={[getUpdatePermission(item.type)]}>
                                                                                    {isPublishableActivity(item) &&
                                                                                        (item.state === "PRIVATE" ? (
                                                                                            <Tooltip
                                                                                                title="Hiện cho học viên">
                                                                                                <PublishIcon
                                                                                                    className="icon publish action-icon-activity"
                                                                                                    onClick={() => {
                                                                                                        handleAccessibilityChange(item);
                                                                                                    }}
                                                                                                />
                                                                                            </Tooltip>
                                                                                        ) : (
                                                                                            <Tooltip
                                                                                                title="Nhấp vào để ẩn đi">
                                                                                                <HideIcon
                                                                                                    className="icon hide action-icon-activity"
                                                                                                    onClick={() => {
                                                                                                        handleAccessibilityChange(item);
                                                                                                    }}
                                                                                                />
                                                                                            </Tooltip>
                                                                                        ))}
                                                                                </PermissionRenderer>}
                                                                                <Tooltip title={"Chi tiết"}>
                                                                                    <FontAwesomeIcon
                                                                                        onClick={() => {
                                                                                            switch (item.type) {
                                                                                                case "quiz":
                                                                                                    navigate(
                                                                                                        replacePathParams(
                                                                                                            ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
                                                                                                            [
                                                                                                                ":classId",
                                                                                                                Number(classInfo.id || ""),
                                                                                                            ],
                                                                                                            [
                                                                                                                ":quizItemId",
                                                                                                                item.id,
                                                                                                            ]
                                                                                                        )
                                                                                                    );
                                                                                                    return;
                                                                                                case "unit":
                                                                                                    navigate(
                                                                                                        replacePathParams(
                                                                                                            ROUTING_CONSTANTS.CLASS_UNIT_ITEM_DETAIL_PAGE,
                                                                                                            [
                                                                                                                ":classId",
                                                                                                                Number(classId),
                                                                                                            ],
                                                                                                            [
                                                                                                                ":unitItemId",
                                                                                                                item.id,
                                                                                                            ]
                                                                                                        )
                                                                                                    );
                                                                                                    return;
                                                                                                default:
                                                                                                    return;
                                                                                            }
                                                                                        }}
                                                                                        className={"icon detail action-icon-activity"}
                                                                                        icon={faEye}
                                                                                    />
                                                                                </Tooltip>
                                                                                {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                                                    permissions={[getUpdatePermission(item.type)]}>
                                                                                    {isAbleEdit(item) && <Tooltip
                                                                                        title={"Chỉnh sửa"}>
                                                                                        <FontAwesomeIcon
                                                                                            className={"icon edit action-icon-activity"}
                                                                                            icon={faPenToSquare}
                                                                                            onClick={() => {
                                                                                                dispatch(
                                                                                                    classContentActions.updatingActivity(
                                                                                                        {
                                                                                                            chapterToEdit:
                                                                                                            chapterItem.id,
                                                                                                            activity: item,
                                                                                                        }
                                                                                                    )
                                                                                                );
                                                                                            }}
                                                                                        />
                                                                                    </Tooltip>}
                                                                                </PermissionRenderer>}
                                                                                {classInfo?.status != 'ENDED' && <PermissionRenderer
                                                                                    permissions={[getDeletePermission(item.type)]}>
                                                                                    {!item.isFromCourse &&
                                                                                        <Tooltip title={"Xóa"}>
                                                                                            <DeleteOutlined
                                                                                                onClick={() => {
                                                                                                    confirmationModal(mapType(item.type), item.title, () => {
                                                                                                        switch (item.type) {
                                                                                                            case "quiz":
                                                                                                                AntdNotifier.handlePromise(
                                                                                                                    deleteQuiz({
                                                                                                                        classId: Number(classId),
                                                                                                                        quizId: item.id,
                                                                                                                    }).unwrap(),
                                                                                                                    "Xóa bài kiểm tra",
                                                                                                                    () => {
                                                                                                                    },
                                                                                                                    (err) => {
                                                                                                                        teachingContentErrorHandler(
                                                                                                                            err
                                                                                                                        );
                                                                                                                    }
                                                                                                                );
                                                                                                                return;
                                                                                                            case "unit":
                                                                                                                AntdNotifier.handlePromise(
                                                                                                                    deleteUnit({
                                                                                                                        classId: Number(classId),
                                                                                                                        unitId: item.id,
                                                                                                                    }).unwrap(),
                                                                                                                    "Xóa bài học",
                                                                                                                    () => {
                                                                                                                    },
                                                                                                                    (err) => {
                                                                                                                        teachingContentErrorHandler(
                                                                                                                            err
                                                                                                                        );
                                                                                                                    }
                                                                                                                );
                                                                                                                return;
                                                                                                            default:
                                                                                                                return;
                                                                                                        }
                                                                                                    });
                                                                                                }}
                                                                                                className={"icon delete action-icon-activity"}
                                                                                            />
                                                                                        </Tooltip>}
                                                                                </PermissionRenderer>}
                                                                            </div>
                                                                        </div>
                                                                    </>
                                                                );
                                                            })}
                                                        </div>
                                                    </>
                                                </Panel>
                                            </Collapse>
                                        </>
                                    );
                                })}
                                {textbook.chapters.length == 0 ?
                                    <div style={{padding: '1rem'}}>Chưa có bài học hay bài kiểm tra nào sử dụng giáo
                                        trình này</div> : ''}
                            </Panel>
                        </Collapse>
                    })}
                    <EditChapterTitleModal/>
                    <AddQuizItemModal courseIdToSearch={classInfo.course.id || 0}/>
                    <AddUnitItemModal/>
                </>}
            </>
        );
    } else if (isFetchingClass || classContentGroupByTextbookLoading || classContentGroupByTextbookLoadingFetching) {
        return <LoadingPage/>;
    } else return <></>;
};

export default ClassContentPage;