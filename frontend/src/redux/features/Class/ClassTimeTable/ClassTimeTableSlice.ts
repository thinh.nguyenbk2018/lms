import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../store/store";
import { EventType } from "../../Calendar/CalendarAPI";
import {ClassSession, ClassSchedule, ClassSessionResponse} from "./ClassTimeTableAPI";
import {DEFAULT_LOAD_MORE} from "@redux/utils/createSliceWithSearch/createSliceEnhancedWithSearch";

export type ClassTimeTableEventType = EventType | "CLASS_SCHEDULE" | "NONE";

type ClassTimeTableSlice = {
	openingModal: ClassTimeTableEventType;
	classTimeTableEventTypeInEdit: ClassSchedule | ClassSession | undefined;
	dateTime: string;
	numsOfPrevious: number;
	numsOfAfter: number;
	sessions: ClassSessionResponse[];
};

const initialState: ClassTimeTableSlice = {
	openingModal: "NONE",
	classTimeTableEventTypeInEdit: undefined,
	dateTime: new Date().toISOString(),
	numsOfPrevious: 0,
	numsOfAfter: DEFAULT_LOAD_MORE,
	sessions: []
};

export const CLASS_TIME_TABLE_SLICE_KEY = "classTimeTableSlice";
export const classTimeTableSlice = createSlice({
	name: CLASS_TIME_TABLE_SLICE_KEY,
	initialState,
	reducers: {
		addNewClassSchedule: (state) => {
			state.openingModal = "CLASS_SCHEDULE";
		},
		addNewClassSession: (state) => {
			state.openingModal = "CLASS_SESSION";
		},
		addNewPersonalEvetn: (state) => {
			state.openingModal = "PERSONAL";
		},

		editClassSchedule: (state, action: PayloadAction<ClassSchedule>) => {
			state.openingModal = "CLASS_SCHEDULE";
			const { payload: classSchedule } = action;

			state.classTimeTableEventTypeInEdit = classSchedule;
		},
		editClassSession: (state, action: PayloadAction<ClassSession>) => {
			state.openingModal = "CLASS_SESSION";
			state.classTimeTableEventTypeInEdit = action.payload;
		},
		closeModal: (state) => {
			state.openingModal = "NONE";
			state.classTimeTableEventTypeInEdit = undefined;
		},
		resetAfterChangeScheduler: (state) => {
			state.openingModal = "NONE";
			state.classTimeTableEventTypeInEdit = undefined;
			state.numsOfPrevious= 0;
			state.numsOfAfter = DEFAULT_LOAD_MORE;
		},
		changeDateTime: (state, action) => {
			state.dateTime = action.payload
		},
		changeNumsOfPrevious: (state, action) => {
			state.numsOfPrevious += action.payload;
		},
		changeNumsOfAfter: (state, action) => {
			state.numsOfAfter += action.payload;
		}
	},
});

export const classTimeTableSelectors = {
	selectIsAddingClassSession: (state: RootState) => {
		return state[CLASS_TIME_TABLE_SLICE_KEY].openingModal === "CLASS_SESSION";
	},
	selectIsAddingPersonalEvent: (state: RootState) => {
		return state[CLASS_TIME_TABLE_SLICE_KEY].openingModal === "PERSONAL";
	},
	selectIsAddingClassSchedule: (state: RootState) => {
		return state[CLASS_TIME_TABLE_SLICE_KEY].openingModal === "CLASS_SCHEDULE";
	},
	selectClassScheduleInEdit: (state: RootState) => {
		if (state[CLASS_TIME_TABLE_SLICE_KEY].openingModal === "CLASS_SCHEDULE")
			return state[CLASS_TIME_TABLE_SLICE_KEY]
				.classTimeTableEventTypeInEdit as ClassSchedule;
		else return undefined;
	},
	selectClassSessionInEdit: (state: RootState) => {
		if (state[CLASS_TIME_TABLE_SLICE_KEY].openingModal === "CLASS_SESSION")
			return state[CLASS_TIME_TABLE_SLICE_KEY]
				.classTimeTableEventTypeInEdit as ClassSession;
		else return undefined;
	},
	selectDateTime: (state: RootState) => {
		return state[CLASS_TIME_TABLE_SLICE_KEY].dateTime;
	},
	selectNumsOfPrevious: (state: RootState) => {
		return state[CLASS_TIME_TABLE_SLICE_KEY].numsOfPrevious;
	},
	selectNumsOfAfter: (state: RootState) => {
		return state[CLASS_TIME_TABLE_SLICE_KEY].numsOfAfter;
	}
};

export const classTimeTableActions = classTimeTableSlice.actions;
