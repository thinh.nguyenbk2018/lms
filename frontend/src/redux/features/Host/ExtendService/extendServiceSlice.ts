import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../store/store";

const initialState: {
  selected: number;
  paymentMethod: "VNPAY" | "MOMO";
} = {
  selected: 1,
  paymentMethod: "VNPAY",
};

export const extendServiceSlice = createSlice({
  name: "GRADE_FORMULA",
  initialState,
  reducers: {
    changeSelected: (state, action: PayloadAction<number>) => {
      state.selected = action.payload;
    },
    changePaymentMethod: (state, action: PayloadAction<"VNPAY" | "MOMO">) => {
      state.paymentMethod = action.payload;
    },
  },
});

export const { changeSelected, changePaymentMethod } =
  extendServiceSlice.actions;

export const extendServiceSeletor = {
  selected: (state: RootState) => state.extendService.selected,
  paymentMethod: (state: RootState) => state.extendService.paymentMethod,
};
