import {ID} from "../../interfaces/types";
import {transformSearchQuery} from "../APIUtils/TransformParameter";
import {apiSlice} from "../CentralAPI";
import {
    NotificationClassPaged,
    NotificationClassSearchCriteria,
    NotificationDto,
    NotificationItem,
    SendNotificationRequest
} from "./AnnouncementType";

const notificationApiWithInvalidateTags = apiSlice.enhanceEndpoints({
    addTagTypes: ["NotificationReceived", "NotificationSent"],
});
const announcementAPI = notificationApiWithInvalidateTags.injectEndpoints({
    endpoints: (build) => ({
        getReceivedAnnouncements: build.query<NotificationClassPaged,
            NotificationClassSearchCriteria>({
            query: (criteria) =>
                `classes/${
                    criteria.classId
                }/announcement/received?${transformSearchQuery(criteria)}`,
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.listData.map(
                            ({id}) => ({type: "NotificationReceived", id} as const)
                        ),
                        {type: "NotificationReceived", id: "LIST"},
                    ]
                    : [{type: "NotificationReceived", id: "LIST"}],
        }),
        getSentNotificationWithPaging: build.query<NotificationClassPaged,
            NotificationClassSearchCriteria>({
            query: (criteria) =>
                `classes/${criteria.classId}/announcement/sent?${transformSearchQuery(
                    criteria
                )}`,
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.listData.map(
                            ({id}) => ({type: "NotificationSent", id} as const)
                        ),
                        {type: "NotificationSent", id: "LIST"},
                    ]
                    : [{type: "NotificationSent", id: "LIST"}],
        }),

        getNotificationDetail: build.query<NotificationItem, number>({
            query: (notificationId) => `notification/${notificationId}`,
            providesTags: (_res, _err, args) => [
                {type: "NotificationReceived", id: args},
            ],
        }),

        sendNotification: build.mutation<Partial<NotificationItem>,
            { notificationBody: Partial<SendNotificationRequest>; classId: ID }>({
            query: ({notificationBody, classId}) => ({
                url: `classes/${classId}/announcement`,
                method: "post",
                body: notificationBody,
            }),
            invalidatesTags: [{type: "NotificationReceived", id: "LIST"}],
        }),
        deleteNotificationItem: build.mutation<void, number>({
            query: (id) => ({
                url: `announcement/received/${id}`,
                method: "delete"
            }),
            invalidatesTags: [{type: "NotificationReceived", id: "LIST"}],
        }),
        clearReceivedNotificationItem: build.mutation<void, number>({
            query: (id) => `announcement/received/${id}`,
            invalidatesTags: [{type: "NotificationReceived", id: "LIST"}],
        }),

        seenNotificationItem: build.mutation<NotificationDto, number>({
            query: (id) => ({
                url: `/announcement/seen/${id}`,
                method: "post"
            }),
            invalidatesTags: (res, err) => [{type: "NotificationReceived", id: res?.id}]
        }),
        unseenNotificationItem: build.mutation<NotificationDto, number>({
            query: (id) => ({
                url: `/announcement/unseen/${id}`,
                method: "post"
            }),
            invalidatesTags: (res, err) => [{type: "NotificationReceived", id: res?.id}]
        })
    }),
    overrideExisting: false,
});

export const {
    // * Received Notification
    useGetReceivedAnnouncementsQuery,
    useClearReceivedNotificationItemMutation,

    // ? Get notification detail
    useGetNotificationDetailQuery,

    // * Sent Notification
    useSendNotificationMutation,

    useSeenNotificationItemMutation,
    useUnseenNotificationItemMutation,

    useGetSentNotificationWithPagingQuery,
    useDeleteNotificationItemMutation,
} = announcementAPI;

// * Provide tags for cache invalidation when code splitting
// * https://github.com/reduxjs/redux-toolkit/issues/1510
