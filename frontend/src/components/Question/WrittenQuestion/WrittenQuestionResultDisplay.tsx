import {Card, Col, Divider, Row} from "antd";
import {WrittenQuestionWithResult} from "../../../redux/features/Quiz/CreateQuizSlice";
import TextEditor from "../../TextEditor/TextEditor";
import React from "react";

const WrittenQuestionResultDisplay = (props: WrittenQuestionWithResult) => {
    return (
        // <Card>
        // 	<TextEditor
        // 		readOnly={true}
        // 		useBubbleTheme={true}
        // 		onChange={() => {}}
        // 		value={props.answer}
        // 	/>
        // </Card>
        <Card>
            <Row>
                <Col span={24} className={"question-box"}>
                    <TextEditor onChange={() => {
                    }} value={props.description} readOnly={true} useBubbleTheme/>
                </Col>
                <Divider/>
                <Col span={24} className={"question-box"}>
                    <TextEditor
                        readOnly={true}
                        useBubbleTheme={true}
                        onChange={() => {
                        }}
                        value={props.answer ? props.answer : 'Không có câu trả lời từ học viên'}
                    />
                </Col>
            </Row>
        </Card>
    );
};

export default WrittenQuestionResultDisplay;
