import React, {useState} from 'react';

// * The states provided by the HOC
interface WithDrawerStateAndQuestionProps {
    point: number;
    visible: boolean;
    question: string;
    drawerRef:  React.MutableRefObject<HTMLDivElement | null>
}

// * The Component that is passed in the HOC itself
interface Props extends WithDrawerStateAndQuestionProps {
    children: React.ReactNode;
}

export function withDrawerStateAndQuestionProps<T extends WithDrawerStateAndQuestionProps = WithDrawerStateAndQuestionProps>(
    WrappedComponent: React.ComponentType<T>
) {
    // Try to create a nice displayName for React Dev Tools.
    const displayName =
        WrappedComponent.displayName || WrappedComponent.name || "Component";

    // Creating the inner component. The calculated Props type here is the where the magic happens.
    const ComponentWithDrawerStateAndQuestion = (props: Omit<T, keyof WithDrawerStateAndQuestionProps>) => {
        // Fetch the props you want to inject. This could be done with context instead.
        const [point, setPoint] = useState(0);
        // * The drawer state
        const [visible, setVisible] = useState(false);
        const [question, setQuestion] = useState<string | null>("");
        const drawerStateAndQuestionProps = {
            point,
            setPoint,
            visible,
            setVisible,
            question,
            setQuestion
        }
        // props comes afterwards so the can override the default ones.
        return <WrappedComponent {...drawerStateAndQuestionProps} {...(props as T)} />;
    };

    ComponentWithDrawerStateAndQuestion.displayName = `withDrawerStateAndQuestionProps(${displayName})`;

    return ComponentWithDrawerStateAndQuestion;
};
