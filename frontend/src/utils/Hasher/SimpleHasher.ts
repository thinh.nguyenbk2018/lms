const SimpleHasher = (msg: string | number) => {
    let str = String(msg);
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        const char = str.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash &= hash; // Convert to 32bit integer
    }
    return new Uint32Array([hash])[0].toString(36);
};
console.log("TEST HASHER:", SimpleHasher(4));
export default SimpleHasher;