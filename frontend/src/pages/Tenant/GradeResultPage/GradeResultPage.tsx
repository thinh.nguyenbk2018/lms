import {Button, Divider, Modal, Space} from "antd";
import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import GradeResult from "../../../components/GradeResult/GradeResult";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";
import {
    ExportGradeTags,
    GradeTagDto,
    ScopeTag,
    useLazyGetGradeResultsQuery,
    useLazyGetGradeTagsByClassQuery
} from "../../../redux/features/Grade/GradeApi";
import './GradeResultPage.less';
import useClassSidebar from "@hooks/Routing/useClassSidebar";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faDownload} from "@fortawesome/free-solid-svg-icons";
import {Formik} from "formik";
import {Checkbox, Form, FormItem} from "formik-antd";
import * as Yup from "yup";
import axios from "axios";
import {API_CONSTANT} from "@constants/ApiConfigs";
import LOCAL_STORAGE_KEYS from "@constants/LocalStorageKey";
import download from "downloadjs";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";

const GradeResultPage = ({scope}: { scope: ScopeTag }) => {
    const {classId, courseId} = useParams();
    const [getGradeResults, {data: getGradeResultsResponse, isLoading}] = useLazyGetGradeResultsQuery();

    const [closeSidebar] = useClassSidebar();

    const [isDownloadingExport, setIsDownloadingExport] = useState(false);
    const [showChooseGradeTagModal, setShowChooseGradeTagModal] = useState(false);

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const [getGradeTags, {
        data: gradeTags,
        isSuccess: getGradeTagsSuccess
    }] = useLazyGetGradeTagsByClassQuery();

    useEffect(() => {
        getGradeResults({scope, scopeId: parseInt(classId || courseId!)});
    }, [classId, courseId, getGradeResults, scope]);

    useEffect(() => {
        if (showChooseGradeTagModal && classId) {
            getGradeTags(Number(classId));
        }
    }, [showChooseGradeTagModal]);

    const initialValues = {
        gradeTagsId: [] as number[],
        all: 0
    }

    const validationSchema = Yup.object({
        gradeTagsId: Yup.array().min(1, "Phải chọn ít nhất một cột điểm để xuất báo cáo"),
    });

    return <>
        <ContentHeader
            title="Điểm"
            action={
                <Space>
                    <PermissionRenderer permissions={[PermissionEnum.VIEW_LIST_GRADE_TAG_IN_CLASS]}>
                        <Link
                            to={scope === "CLASS" ? ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_CLASS.replace(":classId", classId!) : ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_COURSE.replace(":courseId", courseId!)}>
                            <Button
                                type="ghost"
                            >
                                Các cột điểm
                            </Button>
                        </Link>
                    </PermissionRenderer>
                    {userInfo?.accountType === 'STAFF' && <Button
                        type="primary"
                        onClick={() => {
                            setShowChooseGradeTagModal(true);
                        }}
                        className={"back-btn"}
                    >
                        <FontAwesomeIcon icon={faDownload}/>
                        Xuất điểm
                    </Button>}
                </Space>
            }
        />
        <GradeResult gradeResults={getGradeResultsResponse?.gradeResults || []}
                     tags={getGradeResultsResponse?.tags || []} isLoading={isLoading}/>

        <Formik
            initialValues={initialValues}
                onSubmit={(values, {resetForm}) => {
                    // console.log('values: ', values);
                    setIsDownloadingExport(true);
                    axios.post(`${API_CONSTANT.BASE_URL}/quizzes/grades`,
                        {
                            gradeTagsId: values['gradeTagsId']
                        },
                        {
                            responseType: 'arraybuffer',
                            headers: {
                                'Authorization': `Bearer ${localStorage.getItem(LOCAL_STORAGE_KEYS.TOKEN)}`
                            }
                        })
                        .then(res => {
                            setIsDownloadingExport(false);
                            setShowChooseGradeTagModal(false);
                            resetForm({values: initialValues});
                            download(res.data, `grades.xlsx`, res.headers['content-type']);
                        })
                        .catch(err => {
                            antdNotifier.error("Có lỗi trong quá trình xuất báo cáo");
                            setIsDownloadingExport(false);
                        })
                }}
            validationSchema={validationSchema}>
            {
                ({
                     values,isValid, dirty, handleSubmit, setFieldValue, resetForm
                 }) => {
                    return <Modal
                        title={'Xuất báo cáo điểm'}
                        maskClosable={false}
                        visible={showChooseGradeTagModal}
                        onCancel={() => {
                            setShowChooseGradeTagModal(false);
                            resetForm({values: initialValues});
                        }}
                        onOk={() => handleSubmit()}
                        okText={'Xuất báo cáo'}
                        cancelText={'Hủy'}
                        okButtonProps={{disabled: !dirty || isDownloadingExport, loading: isDownloadingExport}}
                    >
                        <Form>
                            <div style={{marginBottom: '1.5rem', fontWeight: 'bold'}}>Chọn cột điểm để xuất báo cáo:</div>
                            <FormItem name={'gradeTagsId'}>
                                <Checkbox name={'all'}
                                          value={1}
                                          onChange={(value) => {
                                              if (values['all'] == 1) {
                                                  console.log('turn off');
                                                  setFieldValue('gradeTagsId', []);
                                                  setFieldValue('all', 0)
                                              } else {
                                                  setFieldValue('all', 1)
                                                  setFieldValue('gradeTagsId', gradeTags?.map(t => t.id) || [])
                                              }
                                          }}
                                >
                                    Chọn tất cả cột điểm
                                </Checkbox>
                                <Divider />
                                <Checkbox.Group name={'gradeTagsId'}>
                                    {getGradeTagsSuccess && gradeTags?.map((tag: GradeTagDto, index: number) => <div style={{display: 'block', marginBottom: '1rem'}}>
                                        <Checkbox
                                            name={'gradeTagsId'}
                                            value={tag.id}
                                            onChange={(valueEvent: any) => {
                                                const tagsId = gradeTags?.map(t => t.id);
                                                const value = valueEvent.target.value;
                                                const substract = tagsId.filter(x => !values['gradeTagsId'].includes(x));
                                                if (tagsId.length == values['gradeTagsId'].length) {
                                                    setFieldValue('all', 0);
                                                    return;
                                                }
                                                if (substract.length == 1 && substract[0] == value) {
                                                    setFieldValue('all', 1);
                                                }
                                            }}
                                        >{tag.title}
                                        </Checkbox></div>)
                                    }
                                </Checkbox.Group>
                            </FormItem>
                        </Form>
                    </Modal>
                }
            }
        </Formik>
    </>
}

export default GradeResultPage;