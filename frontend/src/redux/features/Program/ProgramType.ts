import {CourseItem} from "../Course/CourseAPI";

export interface ProgramDto {
    id?: number,
    name: string,
    code: string,
    description: string,
    isStrict?: boolean,
    isPublished: boolean,
    courses: CourseItem[]
}

export interface ProgramInList {
    id: number;
    name: string;
    code: string;
    isPublished: boolean;
    createdAt: Date;
    courses: CourseItem[]
}