import {css} from "@emotion/react";
import React from "react";
import {Button, Space} from "antd";
import CalendarMetaFormModal from "./Forms/CalendarMetaFormModal";
// @ts-ignore
import Calendar from "@ericz1803/react-google-calendar";
import CalendarNewEventFormModal from "./Forms/CalendarNewEventFormModal";

const API_KEY = process.env.REACT_APP_GCLOUD_API_KEY;
let calendars = [
    {calendarId: "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com", color: "#fefefe"}, //add a color field to specify the color of a calendar
    {calendarId: "hkr1dj9k6v6pa79gvpv03eapeg@GROUP.calendar.google.com"}, //without a specified color, it defaults to blue (#4786ff)
    {calendarId: "rg4m0k607609r2jmdr97sjvjus@GROUP.calendar.google.com", color: "rgb(63, 191, 63)"} //accepts hex and rgb strings (doesn't work with color names)
];

let styles = {
    //you can use object styles (no import required)
    calendar: {
        borderWidth: "3px", //make outer edge of calendar thicker
    },

    //you can also use emotion's string styles
    today: css`
      /* highlight today by making the text red and giving it a red border */
      color: red;
      border: 1px solid red;
    `
}
const language = 'de';


type CalendarActionType = "SHOW_META_MODAL" | "SHOW_NEW_EVENT_MODAL" | "CLOSE_META_MODAL" | "CLOSE_NEW_EVENT_MODAL";
type ModalState = {
    SHOW_META_MODAL: boolean,
    SHOW_NEW_EVENT_MODAL: boolean,
}
type ModalAction = { type: CalendarActionType };

const initState: ModalState = {
    SHOW_META_MODAL: false,
    SHOW_NEW_EVENT_MODAL: false,
}
const reducer = (state: ModalState, action: ModalAction) => {
    switch (action.type) {
        case "SHOW_META_MODAL":
            return {
                ...state,
                SHOW_META_MODAL: true
            }
        case "SHOW_NEW_EVENT_MODAL":
            return {
                ...state,
                SHOW_NEW_EVENT_MODAL: true
            };
        case "CLOSE_META_MODAL":
            return {
                ...state,
                SHOW_META_MODAL: false,
            }
        case "CLOSE_NEW_EVENT_MODAL":
            return {
                ...state,
                SHOW_NEW_EVENT_MODAL: false
            }
        default:
            throw new Error("Invalid action, Calendar::ModalReducer");
    }
}

type ModalOkHandler = ((e: React.MouseEvent<HTMLElement, MouseEvent>) => void) | undefined;
const CalendarView = () => {
    const [modalState, dispatch] = React.useReducer(reducer, initState);

    const handleMetaModelOk: ModalOkHandler = () => {

    }
    return (
			<div>
				<Space>
					<Button onClick={() => dispatch({ type: "SHOW_META_MODAL" })}>
						Update calendar metadata
					</Button>
					<Button onClick={() => dispatch({ type: "SHOW_NEW_EVENT_MODAL" })}>
						{" "}
						Add New Events
					</Button>
					<Button> Clear calendar</Button>
				</Space>
				<CalendarMetaFormModal
					modalProps={{
						visible: modalState.SHOW_META_MODAL,
						onCancel: () => dispatch({ type: "CLOSE_META_MODAL" }),
					}}
				/>
				{/* <CalendarNewEventFormModal modalProps={{
                visible: modalState.SHOW_NEW_EVENT_MODAL,
                onCancel: () => dispatch({type: "CLOSE_NEW_EVENT_MODAL"})
            }}/> */}
				<Calendar
					apiKey={API_KEY}
					calendars={calendars}
					styles={styles}
					language={language}
				/>
			</div>
		);
}
export default CalendarView;