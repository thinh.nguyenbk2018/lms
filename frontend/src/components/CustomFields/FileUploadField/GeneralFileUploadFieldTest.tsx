import {Field, Formik, FormikConfig} from 'formik';
import React, {useState} from 'react';
import {schema} from "normalizr";
import {Button, Form, Typography} from "antd";
import {Input} from 'formik-antd';
import * as Yup from 'yup';
import {GENERAL_FILE_TYPES, getFileExtensionFromFileName, isPlayableMedia} from "../../FilePreview/Utils/FileUtils";
import GeneralFileUploadField from "./GeneralFileUploadField";

// * Notes: Initial values of formik fields should never be undefined --> use Empty string "" or null instead
// *        Reason: undefined values is labeled as uncontrolled by Formik --> Formik will do nothing
const initialValues = {
    file: "",
}

/*
    * Demonstration of complicated validation with yup
    * we can use .test(testName,testErrorMessage,testFunction) to implement some really complicated validation
    * (Examples can be found at CalendarNewEventFormModal.tsx - validation of time Range)
    *   - testName: is to identify the test. Later tests with same name will override the former
    *   - testErrorMessage: the error message to display - kept inside the errors object of formik -> errors[fieldName]
    *   - testFunction: (value) => { validateThisValueWithSomeHardCoreLogic}
    *       Return: false if this value fails the test, true otherwise
 */
const validationSchema = Yup.object().shape(
    {
        file: Yup.object().nullable()
            .test("Test file is playable", "File should be playable (Audio / Video)", (file) => {
                if (typeof file === "string") {
                    const ext = getFileExtensionFromFileName(file);
                    if (!(ext?.includes(GENERAL_FILE_TYPES.AUDIO) || ext?.includes(GENERAL_FILE_TYPES.VIDEO))) {
                        return false;
                    }
                } else if (file instanceof File) {
                    if (!isPlayableMedia(file)) {
                        return false;
                    }
                }
                return true;
            })
        ,
    }
)


const GeneralFileUploadFieldTest = () => {
    // * Display result for testing
    const [result, setResult] = useState("");

    const handleSubmit = (values: any) => {
        console.log("Subited values", values);
        setResult(values);
    }

    return (
        <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
            {
                ({
                     values,isValid, dirty, isSubmitting, isValidating, setSubmitting, submitForm,
                     submitCount, errors, touched
                 }) => {
                    return <Form>
                        <Field name={"file"} placeholder={"Type article content"} component={GeneralFileUploadField}
                               label={"File Upload test"}/>
                        <Button htmlType={"submit"} onClick={submitForm} disabled={!isValid || !dirty}>
                            Submit
                        </Button>
                        <Typography.Paragraph>
                            {
                                JSON.stringify(values)
                            }
                        </Typography.Paragraph>
                        <Typography.Paragraph style={{color: "green"}}>
                            {
                                JSON.stringify(touched)
                            }
                        </Typography.Paragraph>
                        <Typography.Paragraph style={{color: "red"}}>
                            {
                                JSON.stringify(errors)
                            }
                        </Typography.Paragraph>
                    </Form>
                }
            }
        </Formik>
    );
};

export default GeneralFileUploadFieldTest;