import {CheckOutlined} from "@ant-design/icons";
import {faBusinessTime, faCalendarCheck,} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Menu} from "antd";
import React, {useMemo} from "react";
import {useParams} from "react-router-dom";
import {EventType} from "../../../../../redux/features/Calendar/CalendarAPI";
import {ClassTimeTableEventType} from "../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableSlice";

export type EventMenuItem = {
    label: JSX.Element | string;
    key: ClassTimeTableEventType;
    icon: JSX.Element | string;
};

export type AddNewEventMenuProps = {
    onSelect: (key: ClassTimeTableEventType) => void;
};
const AddNewEventMenu = React.memo((props: AddNewEventMenuProps) => {
    const {classId} = useParams();
    const isInsideClass = !isNaN(Number(classId));
    const {onSelect} = props;
    // TODO: Add authorization to allow teacher of the class to add new session
    const hasTeacherPrivileges = true;
    const menuItems: EventMenuItem[] = useMemo(() => {
        const personalMenuItems: EventMenuItem[] = [
            {
                label: "Việc phải làm",
                key: "PERSONAL",
                icon: <CheckOutlined/>,
            },
        ];
        const insideClassWithPrivileges: EventMenuItem[] = [
            {
                label: "Buổi học",
                key: "CLASS_SESSION",
                icon: <FontAwesomeIcon icon={faBusinessTime}/>,
            },
            {
                label: "Lịch học",
                key: "CLASS_SCHEDULE",
                icon: <FontAwesomeIcon icon={faCalendarCheck}/>,
            },
            {
                label: "Việc phải làm",
                key: "PERSONAL",
                icon: <CheckOutlined/>,
            },
        ];
        return isInsideClass && hasTeacherPrivileges
            ? insideClassWithPrivileges
            : personalMenuItems;
    }, [isInsideClass, hasTeacherPrivileges]);

    return (
        <>
            <Menu
                items={menuItems}
                onClick={({key}) => {
                    onSelect(key as EventType);
                }}
            />
        </>
    );
});

export default AddNewEventMenu;
