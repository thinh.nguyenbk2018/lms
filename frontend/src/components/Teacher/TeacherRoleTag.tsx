import {TeacherRole} from "@redux_features/Class/TeacherAPI/ClassTeacherSlice";
import {Tag} from 'antd';


type Props = {
    role: TeacherRole;
}
const teacherRoleToColorMap : Record<TeacherRole, string> =  {
    TEACHER: "blue",
    TEACHER_ASSISTANT: "orange",
} 

const teacherRoleToTitleMap : Record<TeacherRole, string> =  {
    TEACHER: "Giáo viên",
    TEACHER_ASSISTANT: "Trợ giảng",
}
const TeacherRoleTag = (props: Props) => {
    const color = teacherRoleToColorMap[props.role]; 
    return (
        <Tag color={color}>
            {
                teacherRoleToTitleMap[props.role]
            }
        </Tag>
    );
} 

export default TeacherRoleTag; 
