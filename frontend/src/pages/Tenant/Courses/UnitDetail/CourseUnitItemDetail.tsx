import {Col, Row, Space, Tooltip, Typography} from "antd";
import React from 'react';
import { useNavigate, useParams } from "react-router-dom";
import EditButton from "../../../../components/ActionButton/EditButton";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import TextEditor from "../../../../components/TextEditor/TextEditor";
import { useAppDispatch } from "@hooks/Redux/hooks";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";
import { ROUTING_CONSTANTS } from "../../../../navigation/ROUTING_CONSTANTS";
import { useGetUnitDetailQuery } from "@redux/features/Course/CourseAPI";
import { courseContentActions } from "@redux/features/Course/CourseContentSlice";
import SectionHeader from "../../../../components/SectionHeader/SectionHeader";
import LoadingPage from "../../../UtilPages/LoadingPage";
import { AddUnitItemModal } from "../CourseContent/CourseContentPage";
import "./CourseUnitItemDetail.less";
import BackButton from "@components/ActionButton/BackButton";
import { Attachment } from "@components/Question/Attachment/AttachmentQuestion";
import AttachmentRender from "@components/Question/Attachment/AttachmentRender";
import InformationRow from "@components/InformationRow/InformationRow";
import {TextbookUnit} from "@typescript/interfaces/courses/CourseTypes";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";

const CourseUnitItemDetailPage = () => {
	const {
		courseDetail,
		isCourseDetailFetching,
		isCourseDetailSuccess,
	} = useCourseSidebar();

	const { unitItemId, courseId } = useParams();

	const {
		data: unitData,
		isSuccess,
		isFetching,
	} = useGetUnitDetailQuery(
		{
			courseId: Number(courseId),
			unitId: Number(unitItemId),
		},
		{
			skip:
				isNaN(Number(unitItemId)) ||
				isNaN(Number(courseId)),
		}
	);

	//  * Redux store
	const dispatch = useAppDispatch();


	// * Router
	const navigate = useNavigate();

	if (isCourseDetailSuccess && isSuccess) {
		return (
			<>
				<ContentHeader
					title={`${unitData.title}`}
					action={
						<Space>
							<BackButton
								type="primary"
								danger
								onClick={() => {
									navigate(
										ROUTING_CONSTANTS.COURSE_CONTENT.replace(
											":courseId",
											courseId || ""
										)
									);
								}}
								className={"back-btn"}
							>
								<span>Quay lại</span>
							</BackButton>
							<EditButton
								onClick={() => {
									dispatch(
										courseContentActions.updatingActivity({
											chapterToEdit: -1,
											activity: unitData,
										})
									);
								}}
							/>
						</Space>
					}
				/>
				<SectionHeader title={"Thông tin bài học"} />
				<Row justify="center">
					<div className="unit-item-info-card">
						<div className="title">
							<div className="field-value">{unitData.title}</div>
						</div>
						<div>
							{(unitData.textbooks == undefined || unitData.textbooks.length == 0) ? 'Bài học không có giáo trình' : <Row>
								<Col className={'course-unit-page-label course-unit-page-label-left'} span={12}>Giáo trình</Col>
								<Col className={'course-unit-page-label'} span={12}>Ghi chú</Col>
								{unitData.textbooks.map((textbook: TextbookUnit) => {
									return (
										<>
											<Col className={'course-unit-page-value-left'} span={12}>
												<Typography.Link
													onClick={() => navigate(replacePathParams(ROUTING_CONSTANTS.COURSE_TEXTBOOK_DETAIL, [":courseId", courseId || ""], [":textbookId", textbook.textbookId || ""]))}
												>
													{textbook?.name}
												</Typography.Link>
											</Col>
											<Col span={12}>{textbook?.note}</Col>
										</>
									)
								})}
							</Row>}
						</div>
					</div>
				</Row>
				<SectionHeader title={"Nội dung bài học"} />
				<div className="unit-content-container">
					<TextEditor
						value={unitData.content}
						useBubbleTheme={true}
						readOnly={true}
						onChange={() => { }}
						additionalStyles={{
							overflowY: "hidden",
						}}
					/>
				</div>
				{
					JSON.parse(unitData.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
						<AttachmentRender attachment={a} />
					</Row>)
				}
				<AddUnitItemModal />
			</>
		);
	} else if (isCourseDetailFetching || isFetching) {
		return <LoadingPage />;
	} else {
		return <></>;
	}
};

export default CourseUnitItemDetailPage;
