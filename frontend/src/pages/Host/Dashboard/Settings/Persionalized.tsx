import ContentHeader from "@components/ContentHeader/ContentHeader";
import defaultLogo from "@assets/logo/logo.png";
import defaultBanner from '@assets/images/anhtrang.jpg'
import { Button, Form, Input, Row, Space, Typography, Image, Card, Col } from "antd";
import { useEffect, useState } from "react";
import EditableImage from "@components/EditableImage/EditableImage";
import BackButton from "@components/ActionButton/BackButton";
import { HOST_ROUTING_CONSTANT } from "../../../../navigation/ROUTING_CONSTANTS";
import { useNavigate } from "react-router";
import DoneButton from "@components/ActionButton/DoneButton";
import { useGetTenantCustomInfoQuery, useUpdateCustomInfoMutation } from "@redux/features/Tenant/TenantAPI";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

const Tabs = [
  {
    key: "login",
    tab: "Đăng nhập"
  },
  {
    key: "register",
    tab: "Đăng ký"
  }
]

const Personalized = () => {
  const navigate = useNavigate();
  const tenantId = localStorage.getItem("tenant");

  let [name, setName] = useState("BKLMS")
  let [banner, setBanner] = useState(defaultBanner);
  let [logo, setLogo] = useState(defaultLogo);

  let { data } = useGetTenantCustomInfoQuery(tenantId || "");

  let [updateCustomInfo] = useUpdateCustomInfoMutation();

  useEffect(() => {
    if (data) {
      setName(data.name);
      setLogo(data.logo);
      setBanner(data.banner);
    }
  }, [data])

  const submitForm = () => {
    updateCustomInfo({
      tenantId: tenantId!,
      name,
      logo,
      banner
    })
      .then(value => {
        AntdNotifier.success("Đổi thông tin thành công")
      })
      .catch(error => {
        AntdNotifier.error(error.data.message);
      })
  }

  return <>
    <ContentHeader title="Cá nhân hóa" action={<Space>
      <Space>
        <BackButton
          type="primary"
          danger
          onClick={() => {
            navigate(HOST_ROUTING_CONSTANT.SETTINGS);
          }}
          className={"back-btn"}
        >
          <span>Quay lại</span>
        </BackButton>
        <DoneButton
          type="primary"
          color="#73d13d"
          onClick={() => submitForm()}
          className={"update-btn"}
        >
          <span>Xong</span>
        </DoneButton>
      </Space>
    </Space>} />
    <Row justify="center" align="middle" style={{ height: "75vh" }}>
      <Space align="center" direction="horizontal" style={{ border: "1px solid #f0f0f0" }}>
        <Card
          headStyle={{ display: 'flex', justifyContent: `center` }}
          tabList={Tabs}
        >
          <Space direction={"vertical"} align={"center"}>
            <EditableImage style={{ height: 200, maxWidth: 200 }} src={logo} onChangeImage={(url) => setLogo(url)} />
            <Typography.Title level={2} editable={{ tooltip: false, onChange: (value) => setName(value) }}>{name}</Typography.Title>
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              autoComplete="off"
            >
              <Form.Item
                label="Username"
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Hãy nhập tài khoản của bạn",
                  },
                ]}
              >
                <Input disabled />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Nhập mật khẩu vào",
                  },
                ]}
              >
                <Input.Password disabled />
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
                className={'login-form-login-btn'}
              >
                <Button type="primary" htmlType="button">
                  Đăng nhập
                </Button>
              </Form.Item>
            </Form>
          </Space>
        </Card>
        <EditableImage style={{ height: 560, maxWidth: 390 }} src={banner} onChangeImage={(url) => setBanner(url)} />
      </Space>
    </Row>
  </>
}


export default Personalized;