import React from "react";
import {useParams} from "react-router-dom";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import OverallAttendance from "./OverallAttendance";

const ClassAttendancePage = () => {
	const { classId } = useParams();
	const [closeSidebar] = useClassSidebar();

	return (
		<>
			<ContentHeader title="Điểm danh" />
			<OverallAttendance />
		</>
	);
};

export default ClassAttendancePage;
