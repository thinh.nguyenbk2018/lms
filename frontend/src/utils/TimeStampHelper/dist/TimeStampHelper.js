"use strict";
exports.__esModule = true;
var date_fns_tz_1 = require("date-fns-tz");
var date_fns_1 = require("date-fns");
var CalendarUtils_1 = require("../../components/Calendar/Utils/CalendarUtils");
var formatRelative_1 = require("date-fns/formatRelative");
var vi_1 = require("date-fns/locale/vi");
var VN_TIME_ZONE = "Asia/Ho_Chi_Minh";
var formatDateTimeInTable = function (date) {
    if (!date)
        return "";
    return date_fns_1.format(new Date(date), "dd-MM-yyyy");
};
var timeStampHelper = {
    getCurrentTimestampVNTimezone: function () {
        return date_fns_tz_1.utcToZonedTime(new Date(), "Europe/Berlin");
    },
    formatDateTimeInTable: formatDateTimeInTable,
    fromUtcDateToHoChiMinhTimeZone: function (date) {
        return date_fns_tz_1.utcToZonedTime(date, "Europe/Berlin");
    },
    convertToMatchTimeStampBackend: function (date) {
        var timeZone = VN_TIME_ZONE;
        var zonedDate = date_fns_tz_1.utcToZonedTime(CalendarUtils_1.parseDate(date), timeZone);
        return zonedDate;
    },
    displayRelativeTime: function (date) {
        return formatRelative_1["default"](CalendarUtils_1.parseDate(date), new Date(), { locale: vi_1["default"] });
    }
};
exports["default"] = timeStampHelper;
