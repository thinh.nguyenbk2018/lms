import React from "react";
import { Button, Space } from "antd";
import CalendarMetaFormModal from "../Forms/CalendarMetaFormModal";
import CalendarNewEventFormModal from "../Forms/CalendarNewEventFormModal";
import AddButton from "../../ActionButton/AddButton";

type CalendarActionType =
	| "SHOW_META_MODAL"
	| "SHOW_NEW_EVENT_MODAL"
	| "CLOSE_META_MODAL"
	| "CLOSE_NEW_EVENT_MODAL";
type ModalState = {
	SHOW_META_MODAL: boolean;
	SHOW_NEW_EVENT_MODAL: boolean;
};
type ModalAction = { type: CalendarActionType };

const initState: ModalState = {
	SHOW_META_MODAL: false,
	SHOW_NEW_EVENT_MODAL: false,
};
const reducer = (state: ModalState, action: ModalAction) => {
	switch (action.type) {
		case "SHOW_META_MODAL":
			return {
				...state,
				SHOW_META_MODAL: true,
			};
		case "SHOW_NEW_EVENT_MODAL":
			return {
				...state,
				SHOW_NEW_EVENT_MODAL: true,
			};
		case "CLOSE_META_MODAL":
			return {
				...state,
				SHOW_META_MODAL: false,
			};
		case "CLOSE_NEW_EVENT_MODAL":
			return {
				...state,
				SHOW_NEW_EVENT_MODAL: false,
			};
		default:
			throw new Error("Invalid action, Calendar::ModalReducer");
	}
};

type ModalOkHandler =
	| ((e: React.MouseEvent<HTMLElement, MouseEvent>) => void)
	| undefined;

const CalendarControlActions = () => {
	const [modalState, dispatch] = React.useReducer(reducer, initState);

	const handleMetaModelOk: ModalOkHandler = () => {};

	return (
		<div>
			<Space>
				<AddButton onClick={() => dispatch({ type: "SHOW_NEW_EVENT_MODAL" })}>
					{" "}
					Thêm một sự kiện{" "}
				</AddButton>
				<Button onClick={() => dispatch({ type: "SHOW_NEW_EVENT_MODAL" })}>
					{" "}
					Add New Events
				</Button>
			</Space>
			<CalendarMetaFormModal
				modalProps={{
					visible: modalState.SHOW_META_MODAL,
					onCancel: () => dispatch({ type: "CLOSE_META_MODAL" }),
				}}
			/>
			<CalendarNewEventFormModal />
		</div>
	);
};

export default CalendarControlActions;
