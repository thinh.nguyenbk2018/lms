import React from 'react';
import {useAppDispatch, useAppSelector} from "../../../../hooks/Redux/hooks";
import {Select} from "antd";
import useDebounced from "../../../../hooks/Debounced/useDebounced";
import {ID} from "../../../../redux/interfaces/types";
import {createTestContentActions, createTestContentSelectors} from "../../../../redux/features/Quiz/CreateQuizSlice";
import {useSearchExamsQuery} from "@redux/features/Quiz/examAPI";


export type TestSeachSelectProps = {
    /** The most important callback */
    courseIdToSearch: number,
    onSelect?: (value: ID) => void,
    /* Below callbacks make this compatible with formik as a field */
    onChange?: (value: ID) => void,
    onBlur?: Function,
    value?: number;
    width?: string;
    isUpdating?: boolean
}
/** Functionalities tested: Will select and expose the course ID */
const TestSearchSelect = ({onSelect, onChange, onBlur, courseIdToSearch, value, width, isUpdating}: TestSeachSelectProps) => {
    const testSearchCriteria= useAppSelector(createTestContentSelectors.selectSearchCriteria);
    const {data: paginatedResponse, isSuccess, isFetching} = useSearchExamsQuery({searchCriteria: testSearchCriteria, courseId:courseIdToSearch});

    const dispatch = useAppDispatch();

    const debouncedCriteriaChange=  useDebounced((criteria) => dispatch(createTestContentActions.changeCriteria(criteria)))

    // @ts-ignore
    return (
        <Select
            // disabled={isUpdating}
            value={value}
            allowClear={true}
            showSearch
            style={{width: width ? width : 200}}
            placeholder="Chọn đề kiểm tra"
            notFoundContent={'Không có đề kiểm tra'}
            optionFilterProp="children"
            filterOption={(input, option) => {
                const optionContent = (option?.children || "") as string;
                return optionContent.toLowerCase?.().indexOf(input.toLowerCase()) >= 0
            }
            }
            filterSort={(optionA, optionB) => {
                const optionAcontent = (optionA?.children || "") as string;
                const optionBcontent = (optionB?.children || "") as string;
                return optionAcontent.toLowerCase().localeCompare(optionBcontent.toLowerCase());
            }
            }
            onChange={(value,option) => {
                onSelect?.(value);
                onChange?.(value);
                // onBlur?.();
            }}
            onSearch={value => {
                const searchCriteria = {...testSearchCriteria, keyword: value};
                debouncedCriteriaChange(searchCriteria);
            }}
        >
            {
                paginatedResponse?.listData.map((item, index) => {
                    return <Select.Option value={item.id} key={item.id}>
                        {`${item.title}`}
                    </Select.Option>;
                })
            }
        </Select>);
};

export default TestSearchSelect;