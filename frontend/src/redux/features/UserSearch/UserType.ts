export interface UserDto {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    avatar: string;
}