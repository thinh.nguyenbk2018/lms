FROM node:lts-alpine3.15 as build-stage
WORKDIR /app
COPY frontend/ .
RUN npm install --force
RUN npm run build

# production stage
FROM node:lts-alpine3.15 as production-stage
COPY --from=build-stage /app/build ./build/
RUN npm install -g serve
ENTRYPOINT [ "serve", "-s", "build", "-l", "80" ]