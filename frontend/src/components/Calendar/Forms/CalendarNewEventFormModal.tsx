import { Checkbox, Divider, Modal, Row, Space, Typography } from "antd";
import { ModalProps } from "antd/es";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { addHours } from "date-fns";
import { Formik } from "formik";
import { DatePicker, Form, Input, SubmitButton } from "formik-antd";
import { withFormikDevtools } from "formik-devtools-extension";
import moment from "moment";
import React, { useCallback, useState } from "react";
import * as Yup from "yup";
import { useAppSelector } from "../../../hooks/Redux/hooks";
import { authSelectors } from "../../../redux/features/Auth/AuthSlice";
import {
    CalendarEvent,
    useAddEventMutation,
    useUpdateEventMutation
} from "../../../redux/features/Calendar/CalendarAPI";
import { FORM_DEFAULT_MESSAGES } from "@utils/FormUtils/formUtils";
import CancelButton from "../../ActionButton/CancelButton";
import MyRRuleGenerator, { translations } from "../../MyRRuleGenerator";
import {
    addHourAndMinuteToRRuleString,
    getEventEndTime,
    getEventStartTime
} from "../Utils/CalendarUtils";

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

function disabledDate(current: any) {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
}

const EventSchema = Yup.object().shape({
    summary: Yup.string()
        .min(4, FORM_DEFAULT_MESSAGES.TOO_SHORT)
        .max(50, FORM_DEFAULT_MESSAGES.TOO_LONG)
        .required(FORM_DEFAULT_MESSAGES.REQUIRED),
    location: Yup.string()
        .min(4, FORM_DEFAULT_MESSAGES.TOO_SHORT)
        .max(50, FORM_DEFAULT_MESSAGES.TOO_LONG)
        .required(FORM_DEFAULT_MESSAGES.REQUIRED),
    description: Yup.string()
        .min(1, FORM_DEFAULT_MESSAGES.TOO_SHORT)
        .required(FORM_DEFAULT_MESSAGES.REQUIRED),
    timeZone: Yup.string()
        .required("Please choos a timezone")
        .min(2, "Please input valid timezone")
        .max(50, FORM_DEFAULT_MESSAGES.INVALID),
    time: Yup.array()
        .of(Yup.date())
        .length(2, "Range should only contains exactly 2 timestamp")
        .test("valid start time", "Enter valid start time please", (range) => {
            if (!range?.[0] || !moment(range?.[0]).isValid()) {
                return false;
            }
            return true;
        })
        .test("valid end time", "Enter valid end time please", (range) => {
            if (!range?.[1] || !moment(range?.[1]).isValid()) {
                return false;
            }
            return true;
        })
        .test(
            "start_is_earlier_to_end",
            "Start time should be earlier than End Time!!!",
            (range) => {
                if (!range || !range[0] || !range[1]) {
                    return false;
                }
                return range[0] < range[1];
            }
        ),
});

const getNextDate = () => {
    let day = new Date();
    let nextDay = new Date(day);
    return nextDay.setDate(day.getDate() + 1);
};
const constInitialValues = {
    summary: "",
    description: "",
    location: "",
    timeZone: "",
    time: [moment(new Date()), moment(getNextDate())],
};

type EventFormShape = typeof constInitialValues;
const mapDataToFormVal: (
    eventInfo: Partial<CalendarEvent> | undefined
) => EventFormShape = (eventInfo) => {
    if (!eventInfo || !eventInfo.start || !eventInfo.end) {
        return constInitialValues;
    }
    const time = [];
    time[0] = moment(getEventStartTime({ start: eventInfo.start }));
    time[1] = moment(getEventEndTime({ end: eventInfo.end }));
    const timeZone = eventInfo.start.timeZone;
    const res: EventFormShape = {
        summary: eventInfo.summary || "",
        description: eventInfo.description || "",
        location: eventInfo.location || "",
        timeZone,
        time,
    };
    return res;
};
// TODO: Remove the default event values  - This is only for faster testing only
// TODO: Remove the calendarDefaultId on submitting the form. Someone's merge on the backend erased the calendarId
// TODO: from the user object --> Calendar now will render empty
// * Calendar props is passed in only when testing without backed and need a default calendar to retrieve events
const CalendarNewEventFormModal = ({
    calendarID,
    modalProps,
    initialValues,
    isUpdateForm,
}: {
    calendarID?: string;
    modalProps?: ModalProps;
    initialValues?: Partial<CalendarEvent>;
    isUpdateForm?: boolean;
}) => {
    const currentUserCalendarId = useAppSelector(
        authSelectors.selectUserCalendarId
    );
    const calendarIdToUse = currentUserCalendarId || calendarID;
    // console.log("RERENDER FORM");
    const [allowRecurence, setAllowRecurrence] = useState(false);
    const [recurrenceSettings, setRecurrenceSettings] = useState("");

    const [addEvent, { data, isSuccess, isError }] = useAddEventMutation();
    const [updateEvent, { }] = useUpdateEventMutation();

    function onRecurrenceChange(e: CheckboxChangeEvent) {
        setAllowRecurrence(e.target.checked);
    }

    const onRRuleChange = useCallback((rrule: any) => {
        setRecurrenceSettings(rrule);
    }, []);
    return (
        <>
            <Formik
                initialValues={mapDataToFormVal(initialValues)}
                onSubmit={(values, actions) => {
                    console.log("Formik Values is: ", values);
                    // * Add calendar Id for service to call backend appropriately
                    const { time, timeZone } = values;
                    const start = time[0].toDate();
                    let recurrence = [];
                    if (allowRecurence) {
                        const recurrenceRule = addHourAndMinuteToRRuleString(
                            recurrenceSettings,
                            start
                        );
                        recurrence[0] = recurrenceRule;
                    }
                    // if(!calendarIdToUse) return;
                    let valuesToSubmit = {
                        ...values,
                        calendarId:
                            calendarIdToUse ??
                            "0v2mjauc84vkna6gqb07lel2ik@group.calendar.google.com",
                        start: {
                            dateTime: time[0].format(),
                            timeZone: "Asia/Ho_Chi_Minh",
                        },
                        end: {
                            dateTime: time[1].format(),
                            timeZone: "Asia/Ho_Chi_Minh",
                        },
                        recurrence,
                    };
                    console.log("Submitted values is: ", valuesToSubmit);
                    if (isUpdateForm) {
                        const id = initialValues?.id;
                        if (!id) {
                            console.log("No id  update");
                        }
                        const eventUpdateDto = { ...valuesToSubmit, id };
                        updateEvent(eventUpdateDto);
                    } else {
                        addEvent(valuesToSubmit);
                    }
                    actions.setSubmitting(false);
                }}
                validationSchema={EventSchema}
            >
                {(formikProps) => {
                    withFormikDevtools(formikProps);
                    return (
                        <Modal {...modalProps} footer={null}>
                            <Form {...formItemLayout}>
                                <Form.Item name={"summary"} label={"Summary"}>
                                    <Input name={"summary"} />
                                </Form.Item>
                                <Form.Item name={"description"} label={"Descriptions"}>
                                    <Input name={"description"} />
                                </Form.Item>
                                <Form.Item name={"location"} label={"Location"}>
                                    <Input name={"location"} />
                                </Form.Item>
                                <Form.Item name={"timeZone"} label={"TimeZone"}>
                                    <Input name={"timeZone"} />
                                </Form.Item>
                                <Form.Item name={"time"} label={"Time"}>
                                    <DatePicker.RangePicker
                                        disabledDate={disabledDate}
                                        showTime={{
                                            hideDisabledOptions: false,
                                            defaultValue: [
                                                moment(new Date()),
                                                moment(addHours(new Date(), 2)),
                                            ],
                                        }}
                                        format="YYYY-MM-DD HH:mm:ss"
                                        name={"time"}
                                    />
                                    <Typography.Text type={"danger"}>
                                        <>
                                            {formikProps.errors.time}
                                        </>
                                    </Typography.Text>
                                </Form.Item>
                                <Form.Item name={"recurrence"} label={"Recurrence"}>
                                    <Checkbox onChange={onRecurrenceChange}>Checkbox</Checkbox>
                                </Form.Item>
                                <Divider> Recurrence Settings</Divider>
                                <MyRRuleGenerator
                                    onChange={onRRuleChange}
                                    translations={translations.vietnamese}
                                    hideStart={false}
                                />
                                <Divider />
                                <Row justify={"end"}>
                                    <Space>
                                        <CancelButton onClick={modalProps?.onCancel} />
                                        <SubmitButton
                                            loading={formikProps.isSubmitting}
                                            disabled={!formikProps.isValid}
                                        >
                                            Submit
                                        </SubmitButton>
                                    </Space>
                                </Row>
                            </Form>
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

export default CalendarNewEventFormModal;
