export const mapType = (type: string | undefined) => {
    if (type === 'unit') {
        return 'bài học';
    }
    if (type === 'quiz') {
        return 'bài kiểm tra';
    }
    if (type === 'vote') {
        return 'cuộc bình chọn';
    }
    return "";
}