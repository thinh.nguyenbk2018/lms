import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {isNaN, range} from 'lodash';

import numericalFieldHandler from '../../../utils/numericalFieldHandler';
import {MONTHS} from '../../../constants/index';
import translateLabel from '../../../utils/translateLabel';
import {Col, Input, Row, Select, Form, Space, Typography} from "antd";

const RepeatYearlyOn = ({
                            id,
                            mode,
                            on,
                            hasMoreModes,
                            handleChange,
                            translations
                        }) => {
    const daysInMonth = moment(on.month, 'MMM').daysInMonth();
    const isActive = mode === 'on';

    return (
        <Row gutter={[5,10]} className={`form-group row d-flex align-items-sm-center ${!isActive && 'opacity-50'}`}>
            <Col span={24}>
                {hasMoreModes && (
                    <>
                        <Space>
                            <Typography.Text>
                                {translateLabel(translations, 'repeat.yearly.on')}
                            </Typography.Text>
                            <Input
                                id={id}
                                type="radio"
                                name="repeat.yearly.mode"
                                aria-label="Repeat yearly on"
                                value="on"
                                checked={isActive}
                                onChange={handleChange}
                            />
                        </Space>
                    </>
                )}
            </Col>
            <Col>
                <Select
                    id={`${id}-month`}
                    name="repeat.yearly.on.month"
                    aria-label="Repeat yearly on month"
                    className="form-control"
                    value={on.month}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        const target = {name: "repeat.yearly.on.month", value};
                        handleChange({target})
                    }}
                >
                    {MONTHS.map(month => <option key={month}
                                                 value={month}>{translateLabel(translations, `months.${month.toLowerCase()}`)}</option>)}
                </Select>
            </Col>

            <Col>
                <Select
                    id={`${id}-day`}
                    name="repeat.yearly.on.day"
                    aria-label="Repeat yearly on a day"
                    className="form-control"
                    value={on.day}
                    disabled={!isActive}
                    onChange={(value, option) => {
                        // Convert input from a string to a number
                        const inputNumber = +value;
                        // Check if is a number and is less than 1000
                        if (isNaN(inputNumber) || inputNumber >= 1000) return;
                        const target = {name: "repeat.yearly.on.day", value: inputNumber};
                        handleChange({target})
                    }}
                >
                    {range(0, daysInMonth).map((i) => (
                        <Select.Option key={i} value={i + 1}>{i + 1}</Select.Option>
                    ))}
                </Select>
            </Col>
        </Row>
    );
};
RepeatYearlyOn.propTypes = {
    id: PropTypes.string.isRequired,
    mode: PropTypes.oneOf(['on', 'on the']).isRequired,
    on: PropTypes.shape({
        month: PropTypes.oneOf(MONTHS).isRequired,
        day: PropTypes.number.isRequired,
    }).isRequired,
    hasMoreModes: PropTypes.bool.isRequired,
    handleChange: PropTypes.func.isRequired,
    translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default RepeatYearlyOn;
