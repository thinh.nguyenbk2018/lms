import {Typography} from 'antd';
import React from 'react';
import FileIconWithColor from '../../../components/FilePreview/FileIcon/FileIconWithColor';
import FileMediaUploadWithFirebase from "../../../components/ImageChooser/FileMediaUploadWithFirebase";

const UploadFileIconTestSuite = () => {
    return (
        <>
            <FileIconWithColor height={undefined} extension={"mp3"}/>
            <Typography.Text type={"success"} strong={true}>
                Upload other media type to firebase and show preview
            </Typography.Text>
            <br/>
            <Typography.Text type={"success"} strong={true}>
                {"<FileMediaUploadWithFirebase showImagePreviewInsteadOfIcon={false}/>"}
            </Typography.Text>
            <FileMediaUploadWithFirebase showImagePreviewInsteadOfIcon={false}/>
            <Typography.Text type={"success"} strong={true}>
                Upload other media type to firebase and show preview
            </Typography.Text>
            <br/>
            <Typography.Text type={"success"} strong={true}>
                {"<FileMediaUploadWithFirebase/>"}
            </Typography.Text>
            <FileMediaUploadWithFirebase/>
        </>
    );
};

export default UploadFileIconTestSuite;