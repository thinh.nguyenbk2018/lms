import React, {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {Field, Formik} from "formik";
import {Form, FormItem, Input} from "formik-antd";
import {Button, Modal, Row, Space} from "antd";
import _ from "lodash";
import * as Yup from "yup";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import CustomTable from "../../../../components/CustomTable/CustomTable";
import {extractPaginationInformation} from "../../../../components/CustomTable/TableUtils";
import {addIconSvg, viewIconSvg,} from "../../../../components/Icons/SvgIcons";
import TableSearch from "../../../../components/Search/TableSearch";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import {useAppSelector} from "../../../../hooks/Redux/hooks";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {
    useCreateTestContentMutation,
    useDeleteTestMutation,
    useGetTestContentListQuery,
} from "../../../../redux/features/Quiz/examAPI";
import AntdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import {replacePathParams} from "../../../../utils/PathPatcherHepler/PathPatcher";
import {changeCriteria, courseQuizBankSelector} from "@redux/features/Quiz/CourseQuizBankSlice";
import {DATE_TIME_FORMAT} from "@utils/TimeStampHelper/TimeStampHelper";
import moment from "moment";
import {DatePresentation} from "@components/Calendar/Utils/CalendarUtils";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import {DeleteOutlined} from "@ant-design/icons";
import TextBookSearchArrayFieldWithNote
    from "@components/CustomFields/TextBookSearchSelectField/TextBookSearchArrayFieldWithNote/TextBookSearchArrayFieldWithNote";
import {TextbookRef} from "@typescript/interfaces/courses/CourseTypes";
import {DEFAULT_FRONTEND_ID} from "@redux/interfaces/types";

export const testContentMetaSchema = Yup.object({
    title: Yup.string().required(
        "Tên đề kiểm tra là thông tin bắt buộc"
    ),
    description: Yup.string().required("Mô tả đề kiểm tra là thông tin bắt buộc"),
});

const MODEL_KEY = {
    CREATE_TEST: "CREATE_TEST" as const,
    CONFIRM_DELETE_TEST: "DELETE_TEST" as const,
};

function LoadingPage() {
    return null;
}

const CourseQuizBank = () => {
    const {
        forceCloseSidebar,
        courseDetail,
        isCourseDetailError,
        isCourseDetailFetching,
        isCourseDetailSuccess,
        courseId,
    } = useCourseSidebar();

    // * States
    const [modals, setModals] = useState<{
        [MODEL_KEY.CREATE_TEST]: boolean;
        [MODEL_KEY.CONFIRM_DELETE_TEST]: boolean;
    }>({
        [MODEL_KEY.CREATE_TEST]: false,
        [MODEL_KEY.CONFIRM_DELETE_TEST]: false,
    });
    const searchCriteria = useAppSelector(
        courseQuizBankSelector.selectSearchCriteria
    );
    // * Queries
    // const [
    // 	triggerFecthList,
    // 	{
    // 		data: testContentList,
    // 		isSuccess: isContentListFetched,
    // 		isFetching: isContentListFetching,
    // 		isError: isContentFetchError,
    // 	},
    // ] = useGetTestContentListQuery(courseId, searchCriteria);

    const [deleteTest] = useDeleteTestMutation();

    const {
        data: testContentList,
        isSuccess: isContentListFetched,
        isFetching: isContentListFetching,
        isError: isContentFetchError,
    } = useGetTestContentListQuery({courseId: Number(courseId), searchCriteria: searchCriteria}, {skip: isNaN(Number(courseId))});

    const [deleteTestContent] = useDeleteTestMutation();
    const [
        createNewTestContent,
        {data: testContentCreated, isSuccess, isError, isLoading},
    ] = useCreateTestContentMutation();

    const changModalVisibility = (
        key: keyof typeof MODEL_KEY,
        value: boolean
    ) => {
        const clonedState = _.cloneDeep(modals);
        clonedState[MODEL_KEY[key]] = value;
        setModals(clonedState);
    };
    // * Hooks
    const navigate = useNavigate();

    // * Quiz List Table State
    const columns = [
        {
            title: "Tên",
            key: "title",
            dataIndex: "title",
            width: "20%",
        },
        {
            title: "Mô tả",
            key: "description",
            dataIndex: "description",
            width: "50%",
        },
        {
            title: "Cập nhật",
            dataIndex: "updatedAt",
            key: "updatedAt",
            width: "15%",
            sorter: {
                compare: (a: string, b: string) => 0,
                multiple: 3,
            },
            sortDirections: ["descend", "ascend"],
            sortOrder:
                searchCriteria.sort.filter((x) => x.field === "updatedAt").length > 0 &&
                searchCriteria.sort.filter((x) => x.field === "updatedAt")[0].order,
            render: (updatedAt: DatePresentation) => {
                console.log('date: ', updatedAt)
                return updatedAt ? moment(updatedAt).format(DATE_TIME_FORMAT) : "";
            }
        },
        {
            title: "",
            key: "action",
            width: "15%",
            render: (record: any) => (
                <>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <Link
                            className={'flex'}
                            style={{marginRight: "1rem"}}
                            to={`${replacePathParams(
                                ROUTING_CONSTANTS.COURSE_EXAM_DETAIL,
                                [":courseId", Number(courseId)],
                                [":testId", record.id]
                            )}`}
                        >
                            {viewIconSvg}
                        </Link>
                        <DeleteOutlined
                            className={'delete-item-btn'}
                            onClick={() => {
                                confirmationModal('đề kiểm tra', record.title, () => {
                                    console.log('data: ', record.id);
                                    deleteTest({testIdToDelete: record.id})
                                        .unwrap()
                                        .then((_res: any) => {
                                            AntdNotifier.success('Xóa đề kiểm tra thành công', '', 1);
                                        })
                                        .catch((err: any) => {
                                            if ("message" in err.data) {
                                                return AntdNotifier.error(err.data.message);
                                            }
                                            AntdNotifier.error('Xóa đề kiểm tra thất bại', '', 1);
                                        })
                                });
                            }}
                        />
                    </div>
                </>
            ),
        },
    ];

    const defaultExam = {
        title: "",
        textbooks: [] as TextbookRef[],
        description: "",
        id: DEFAULT_FRONTEND_ID,
    };

    return (
        <>
            <ContentHeader
                title={"Đề kiểm tra"}
                action={
                    <Space>
                        <div className={"search-container"}>
                            <TableSearch
                                searchFields={[{title: "Từ khóa", key: "key"}]}
                                defaultFilters={[]}
                                searchCriteria={searchCriteria}
                                changeCriteria={changeCriteria}
                            />
                            <Button
                                type="primary"
                                className={'button-with-icon'}
                                onClick={() => {
                                    changModalVisibility("CREATE_TEST", true);
                                }}
                            >
                                {addIconSvg}
                                <span>Thêm</span>
                            </Button>
                        </div>
                    </Space>
                }
            />

            <Formik
                initialValues={defaultExam}
                validationSchema={testContentMetaSchema}
                validateOnChange={false}
                onSubmit={(values, {resetForm}) => {
                    // console.log("Tried creating test: ", values);
                    const valuesToSubmit = {
                        testData: values,
                        courseId: Number(courseId),
                    };
                    AntdNotifier.handlePromise(
                        createNewTestContent(valuesToSubmit).unwrap(),
                        "Tạo đề kiểm tra mới",
                        () => {
                            changModalVisibility("CREATE_TEST", false);
                            resetForm({values: defaultExam});
                        },
                        () => changModalVisibility("CREATE_TEST", false)
                    );
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleSubmit,
                      dirty,
                      isValid,
                      resetForm,
                  }) => {
                    return (

                        <Modal
                            maskClosable={false}
                            title="Đề kiểm tra"
                            visible={modals.CREATE_TEST}
                            onOk={() => {
                                handleSubmit();
                            }}
                            onCancel={() => {
                                resetForm();
                                changModalVisibility("CREATE_TEST", false);
                            }}
                            okButtonProps={{disabled: !dirty}}
                            okText={"Thêm"}
                            cancelText={"Hủy"}
                        >
                            <Form layout="vertical">
                                <FormItem name={"title"} label={"Tiêu đề"} required>
                                    <Input name={"title"}/>
                                </FormItem>
                                <FormItem name={"description"} label={"Mô tả"} required>
                                    <Input name={"description"}/>
                                </FormItem>
                                <Field
                                    name={`textbooks`}
                                    component={TextBookSearchArrayFieldWithNote}
                                    label="giáo trình"
                                    scopeIdToSearch={courseId}
                                    scopeToSearch={"COURSE"}
                                />
                            </Form>
                        </Modal>
                    );
                }}
            </Formik>
            {isContentListFetched && (
                <CustomTable
                    columns={columns}
                    data={testContentList.listData || []}
                    searchCriteria={searchCriteria}
                    metaData={extractPaginationInformation(testContentList)}
                    changeSearchActionCreator={changeCriteria}
                />
            )}
            {isContentListFetching && <LoadingPage/>}
            {isContentFetchError && (
                <Row align={"middle"} justify={"center"}>
                    <ApiErrorWithRetryButton onRetry={() => {
                    }}/>;
                </Row>
            )}
        </>
    );
};

export default CourseQuizBank;

// *
