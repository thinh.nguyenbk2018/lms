import '../SubSidebar.less';

import classNames from 'classnames/bind';
import React, { useEffect, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { useAppSelector } from '../../../hooks/Redux/hooks';
import { ROUTING_CONSTANTS } from '../../../navigation/ROUTING_CONSTANTS';
import { sideBarSelector } from '../../../redux/features/SideBar/SidebarSlice';
import { SubSidebarTab, useActiveSubSideBar } from '../SubSidebar';
import { PermissionEnum } from '@typescript/interfaces/generated/PermissionMap';
import { authSelectors } from '@redux/features/Auth/AuthSlice';


const classSidebarTabs: SubSidebarTab[] = [
    {
        displayName: "Thông tin chung ",
        navigationRoute: ROUTING_CONSTANTS.CLASS_DASHBOARD,
        require_permissions: [PermissionEnum.VIEW_CLASS],
        key: "dashboard"
    },
    {
        displayName: "Thời khóa biểu",
        navigationRoute: ROUTING_CONSTANTS.CLASS_TIME_TABLE,
        require_permissions: [PermissionEnum.VIEW_CLASS_SCHEDULER],
        key: "timeTable"
    },
    {
        displayName: "Lịch",
        navigationRoute: ROUTING_CONSTANTS.CLASS_CALENDAR,
        require_permissions: [],
        key: "calendar"
    },
    {
        displayName: "Thông báo",
        navigationRoute: ROUTING_CONSTANTS.CLASS_ANNOUNCEMENT,
        require_permissions: [PermissionEnum.VIEW_LIST_ANNOUNCEMENT],
        key: "notification"
    },
    {
        displayName: "Học viên",
        navigationRoute: ROUTING_CONSTANTS.CLASS_STUDENT,
        require_permissions: [PermissionEnum.VIEW_LIST_STUDENT_IN_CLASS],
        key: "student"
    },
    {
        displayName: "Giáo viên",
        navigationRoute: ROUTING_CONSTANTS.CLASS_TEACHER,
        require_permissions: [PermissionEnum.VIEW_LIST_TEACHER_IN_CLASS],
        key: "teacher"
    },
    {
        displayName: "Nội dung giảng dạy",
        navigationRoute: ROUTING_CONSTANTS.CLASS_CONTENT,
        require_permissions: [PermissionEnum.VIEW_LEARNING_CONTENT],
        key: "teachingContent"
    },
    {
        displayName: "Bài học",
        navigationRoute: ROUTING_CONSTANTS.CLASS_UNIT_LIST_PAGE,
        require_permissions: [PermissionEnum.VIEW_LIST_UNIT_CLASS],
        key: "units"
    },
    {
        displayName: "Bài kiểm tra",
        navigationRoute: ROUTING_CONSTANTS.CLASS_QUIZ,
        require_permissions: [PermissionEnum.VIEW_LIST_QUIZ_CLASS],
        key: "quizzes"
    },
    {
        displayName: "Điểm danh",
        navigationRoute: ROUTING_CONSTANTS.CLASS_CHECK_IN,
        require_permissions: [PermissionEnum.VIEW_CLASS_ATTENDANCE],
        key: "checkIn"
    },
    {
        displayName: "Diễn đàn",
        navigationRoute: ROUTING_CONSTANTS.CLASS_DISCUSSION,
        require_permissions: [],
        key: "discussion"
    }, 
    {
        displayName: "Điểm",
        navigationRoute: ROUTING_CONSTANTS.VIEW_GRADE_IN_CLASS,
        require_permissions: [PermissionEnum.VIEW_GRADE_RESULT_IN_CLASS],
        key: "grades"
    },
    {
        displayName: "Giáo trình",
        navigationRoute: ROUTING_CONSTANTS.CLASS_TEXTBOOK_MANAGEMENT,
        require_permissions: [],
        key: "textbooks"
    },
    
    // chỉ để map cho đúng
    {
        navigationRoute: ROUTING_CONSTANTS.GRADE_MANAGEMENT_IN_CLASS,
        key: "grades"
    },
]
const tabKeys = Object.keys(classSidebarTabs);

const ClassSidebar = () => {
    const navigate = useNavigate();
    const classIdInFocus = useAppSelector(sideBarSelector.selectClassIdInFocus);
    const permissionsMap = useAppSelector(authSelectors.selectUserPermissions);
    // * Selected tab: default to first tab in the list
    const [selectedTab, setSelectedTab] = useState(classSidebarTabs[0].key)

	const currentNavigationRoute = useActiveSubSideBar(classSidebarTabs);

    useEffect(()=>{
		setSelectedTab((classSidebarTabs.find(tab => tab.navigationRoute === currentNavigationRoute) || classSidebarTabs[0]).key)
	}, [setSelectedTab, currentNavigationRoute]);

	const show = useAppSelector(sideBarSelector.selectShowSidebar);
    if (!classIdInFocus) {
        return <></>;
    }
    return (
        <div className={"sub-sidebar-tab-container"} style={{display: show ? `block` : `none`}}>
            {
                classSidebarTabs.filter(tab => tab.displayName).filter(tab =>  tab.require_permissions?.length === 0 || (permissionsMap && tab.require_permissions?.some(permission=>permissionsMap[permission]))).map((tab, index) => {
                    let clsNames: string = classNames({
                        'sub-sidebar-tab': true,
                        'sub-sidebar-tab-selected': tab.key === selectedTab
                    });

                    return (
                        <div key={tab.key}>
                            <a className={clsNames} onClick={() => {
                                setSelectedTab(tab.key)
                                const pathReplacedWithClassId = tab.navigationRoute.replace(':classId', classIdInFocus.toString());
                                navigate(pathReplacedWithClassId);
                            }} tabIndex={index}>
                                {
                                    tab.displayName
                                }
                            </a>
                        </div>
                    );
                })
            }
        </div>
    );
}

export default ClassSidebar;