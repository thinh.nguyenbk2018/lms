import { TeacherRole } from "@redux_features/Class/TeacherRole.1";
import { Menu, Tag } from "antd";
import TeacherRoleTag from "./TeacherRoleTag";

type TeacherRoleMenuTagItem = {
    label: JSX.Element;
    key: TeacherRole;
};

type RoleChangeMenuProps = {
    onSelect: (role: TeacherRole) => void;
};

const TeacherRoleMenu = (props: RoleChangeMenuProps) => {
    const { onSelect } = props;
    const menuItems: TeacherRoleMenuTagItem[] = [
        {
            label: <TeacherRoleTag role={"TEACHER"}></TeacherRoleTag>,
            key: "TEACHER" as const,
        },
        {
            label: <TeacherRoleTag role={"TEACHER_ASSISTANT"}></TeacherRoleTag>,
            key: "TEACHER_ASSISTANT" as const,
        },
    ];
    return (
        <Menu
            items={menuItems}
            onClick={({ key }) => {
                onSelect(key as TeacherRole);
            }}
        />
    );
};

export default TeacherRoleMenu;
