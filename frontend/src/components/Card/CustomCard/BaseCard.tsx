import {
	faCircleXmark,
	faPenToSquare,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Space, Typography } from "antd";
import "./BaseCard.less";
// * To display in the list of Textbooks of this user
type BaseCardProps = {
    title?: JSX.Element | string,
    description?: JSX.Element | string,

    // * Middle image config
	withMiddleImage?: boolean;
	middleImage?: string | undefined | null;

    // * Shoud we display the title on the background
	titleOnBackGround?: boolean;
	backgroundImage?: string | undefined | null;

    // * Action config
    showAction?: boolean;
	onEdit?: () => void;
	onDelete?: () => void;
};


const BaseCard = (props: BaseCardProps) => {
	const {title,description, withMiddleImage, titleOnBackGround,backgroundImage, middleImage,showAction,onEdit,onDelete } = props;
	return (
		<div className="card-container">
			<div className="card-background">
				{
					titleOnBackGround && <p className="name">
						{title}
					</p>
				}
			</div>
			{withMiddleImage && (
				<img
					src={middleImage || "https://images.unsplash.com/photo-1444011283387-7b0f76371f12?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjE0NTg5fQ" }
					className="profile-image"
					alt="Profile image"
				/>
			)}
			<div className="card-meta">
				{!titleOnBackGround && <p className="name">
					{title}
				</p> }
				<p className="description">
					{description}
				</p>
			</div>
			<div className="card-action">
				<div className="card-action-item card-edit-action" onClick={() => onEdit?.()}>
					<Space>
						<FontAwesomeIcon icon={faPenToSquare} />
						Chỉnh sửa
					</Space>
				</div>
				<div className="card-action-item card-delete-action" onClick={() => onDelete?.()}>
					<Space>
						<FontAwesomeIcon icon={faCircleXmark} />
						Xóa
					</Space>
				</div>
			</div>
		</div>
	);
};
export default BaseCard;
