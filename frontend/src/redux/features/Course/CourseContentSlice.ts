import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Activity, ActivityType, QuizItem, UnitItem } from '../../../typescript/interfaces/courses/CourseTypes';
import { ID } from '../../interfaces/types';
import { RootState } from '../../store/store';

type ClassTeacherSlice = {
	chapterInEdit: number | undefined;
	chapterTitle: string;
	activityModalToShow: ActivityType | "NONE";
	activityBeingUpdated: Activity | undefined;
	editingChapterTitle: boolean;
};

const initialState: ClassTeacherSlice = {
	chapterInEdit: undefined,
	chapterTitle: '',
	activityModalToShow: "NONE",
	activityBeingUpdated: undefined,
	editingChapterTitle: false
};

export const COURSE_CONTENT_SLICE_KEY = "courseContentSlice";
export const courseContentSlice = createSlice({
	name: COURSE_CONTENT_SLICE_KEY,
	initialState,
	reducers: {
		addingUnit: (state, action: PayloadAction<{ chapterToEdit: ID }>) => {
			const { chapterToEdit } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityModalToShow = "unit";
		},

		// * Content creation
		addingQuiz: (state, action: PayloadAction<{ chapterToEdit: ID }>) => {
			const { chapterToEdit } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityModalToShow = "quiz";
		},

		addingActivity: (
			state,
			action: PayloadAction<{ chapterToEdit: ID; type: ActivityType }>
		) => {
			const { chapterToEdit, type } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityModalToShow = type;
			state.activityBeingUpdated = undefined;
		},

		doneAddingActivity: (state) => {
			state.chapterInEdit = undefined;
			state.activityModalToShow = "NONE";
			state.activityBeingUpdated = undefined;
		},

		// * Content updates
		updatingActivity: (
			state,
			action: PayloadAction<{ chapterToEdit: ID; activity: Activity }>
		) => {
			const { chapterToEdit, activity } = action.payload;
			state.chapterInEdit = chapterToEdit;
			state.activityBeingUpdated = activity;
			state.activityModalToShow = activity.type;
		},

		// * Editing chapter title
		editingChapter: (state, action: PayloadAction<{id: number, title: string}>) => {
			state.chapterInEdit = action.payload.id;
			state.chapterTitle = action.payload.title;
			state.editingChapterTitle = true;
		},
		doneEditingChapter: (state) => {
			state.chapterInEdit = undefined;
			state.editingChapterTitle = false;
		}
	},
});

export const courseContentSelectors = {
	selectIsAddingUnit: (state: RootState) => {
		const { chapterInEdit, activityModalToShow } =
			state[COURSE_CONTENT_SLICE_KEY];
		return chapterInEdit && activityModalToShow === "unit";
	},
	selectIsAddingQuiz: (state: RootState) => {
		const { chapterInEdit, activityModalToShow } =
			state[COURSE_CONTENT_SLICE_KEY];
		return chapterInEdit && activityModalToShow === "quiz";
	},
	selectIsUpdatingUnit: (state: RootState) => {
		const { chapterInEdit, activityBeingUpdated } =
			state[COURSE_CONTENT_SLICE_KEY];
		return chapterInEdit && activityBeingUpdated?.type === "unit";
	},
	selectIsUpdatingQuiz: (state: RootState) => {
		const { chapterInEdit, activityBeingUpdated } =
			state[COURSE_CONTENT_SLICE_KEY];
		return chapterInEdit && activityBeingUpdated?.type === "quiz";
	},
	selectQuizInUpdate: (state: RootState) => {
		if (state[COURSE_CONTENT_SLICE_KEY].activityBeingUpdated?.type !== "quiz") {
			return undefined;
		}
		return state[COURSE_CONTENT_SLICE_KEY].activityBeingUpdated as QuizItem;
	},
	selectUnitInUpdate: (state: RootState) => {
		if (state[COURSE_CONTENT_SLICE_KEY].activityBeingUpdated?.type !== "unit") {
			return undefined;
		}
		return state[COURSE_CONTENT_SLICE_KEY].activityBeingUpdated as UnitItem;
	},
	selectChapterInEdit: (state: RootState) => {
		return state[COURSE_CONTENT_SLICE_KEY].chapterInEdit;
	},
	selectIsChapterInfoBeingEdited: (state: RootState) => {
		return {
			isChapterInEdit: !!(state[COURSE_CONTENT_SLICE_KEY].chapterInEdit && state[COURSE_CONTENT_SLICE_KEY].editingChapterTitle),
			chapterTitle: state[COURSE_CONTENT_SLICE_KEY].chapterTitle
		};
	}
};

export const courseContentActions = courseContentSlice.actions;
