interface ScrollableProps {
  height: number | string,
  width?: number | string,
  direction: "Vertical" | "Horizontal" | "Both"
}

const Scrollable = ({ height, width, direction, children }: React.PropsWithChildren<ScrollableProps>) => {
  return <div style={{
    overflowX: direction != "Vertical" ? undefined : "auto",
    overflowY: direction != "Horizontal" ? undefined : "auto",
    height: height,
    width: width 
  }}>
    {children}
  </div>
}

export default Scrollable;