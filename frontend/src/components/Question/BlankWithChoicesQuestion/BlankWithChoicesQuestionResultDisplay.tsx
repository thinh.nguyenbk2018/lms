import { CheckOutlined, InfoCircleOutlined } from "@ant-design/icons";
import { Card, Col, Divider, Row, Select, Space, Tooltip } from "antd";
import ReactHtmlParser from "html-react-parser";

import COLOR_SCHEME from "../../../constants/ThemeColor";
import {
    BlankWitchChoicesQuestionWithResult,
    BlankWithOption,
    BlankWithOptionWithResult,
    Choice,
} from "../../../redux/features/Quiz/CreateQuizSlice";
import { DND_BLANK_SPAN_TAG_CLASS } from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import { blankRegex } from "../FillBlankQuestion/FillInBlankQuestionDisplay";
import './BlankWithChoicesQuestionResultDisplay.less';
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Attachment } from "../Attachment/AttachmentQuestion";
import AttachmentRender from "../Attachment/AttachmentRender";

const { Option } = Select;

const BlankWithChoices = (
    props: BlankWithOption & {
        index: number;
        selectedChoice: string | number | null | undefined;
        isCorrect: boolean;
    }
) => {
    const {
        index,
        hint = "",
        answerList,
        correctAnswerKey,
        selectedChoice,
        isCorrect,
    } = props;
    return (
        <Space>
            <Select
                placeholder={"Chọn đáp án"}
                disabled={true}
                defaultValue={selectedChoice}
                className={'fill-in-blank-with-choices-input'}
            >
                {answerList.map((item, index) => (
                    <Option key={item.answerKey} value={item.answerKey}>
                        {item.content}
                    </Option>
                ))}
            </Select>
            {isCorrect && <CheckOutlined style={{ color: COLOR_SCHEME.success }} />}
            {!isCorrect && <FontAwesomeIcon icon={faTimes} style={{ color: COLOR_SCHEME.error }} />}

            {hint && (
                <Tooltip title={`Gợi ý: ${hint}`}>
                    <InfoCircleOutlined style={{ color: COLOR_SCHEME.primary }} />
                </Tooltip>
            )}
        </Space>
    );
};

/**
 * Enclosing blanks with special span
 * @param questionText
 */
const replaceBlankWithSpecialSpanButKeepContent: (
    questionText: string
) => [string, number] = (questionText: string) => {
    const count = questionText.match(blankRegex)?.length || 0;
    return [
        questionText.replace(blankRegex, (item) => {
            return `<span class="${DND_BLANK_SPAN_TAG_CLASS}">${item}</span>`;
        }),
        count,
    ];
};

const getContentInsideSpecialSpanNode = (node: any) => {
    return node.children[0].data;
};

export const renderChoiceAnswerBoxWithResult = (
    questionHTML: string,
    callbakOnAnswerList?: Function,
    blankList?: BlankWithOptionWithResult[]
) => {
    const [questionReviewHTML, count] =
        replaceBlankWithSpecialSpanButKeepContent(questionHTML);
    let blankCounter = 0;

    const parseAnswersInBlank = (question: string) => {
        return question.substring(1, question.length - 1).split(";");
    };
    return ReactHtmlParser(questionReviewHTML, {
        replace: (node) => {
            if (
                (node as any).name === "span" &&
                (node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
            ) {
                // const answers = parseAnswersInBlank();
                // console.log("Span node sample: ", node);
                blankCounter++;
                const blankIndex = blankCounter - 1;
                const answerTexts = parseAnswersInBlank(
                    getContentInsideSpecialSpanNode(node)
                );
                const answerList: Choice[] = answerTexts.map((item, index) => {
                    return {
                        content: item,
                        answerKey: index,
                        isCorrect: false,
                        isChosenByStudent: false
                    };
                });
                console.log("Blank index is : ", blankIndex);
                return (
                    <BlankWithChoices
                        hint={""}
                        answerList={answerList}
                        isCorrect={blankList?.[blankIndex].isCorrect || false}
                        index={blankIndex}
                        selectedChoice={blankList?.[blankIndex].studentAnswerKey}
                        studentAnswer={blankList?.[blankIndex].studentAnswerKey} />
                );
            }
            return node;
        },
    });
};

const BlankWithChoicesQuestionResultDisplay = (
    props: BlankWitchChoicesQuestionWithResult
) => {
    return (
        <>
            <Card>
                <Col span={24}>
                    {
                        JSON.parse(props?.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
                            <AttachmentRender attachment={a} />
                        </Row>)
                    }
                    <Divider />
                    {renderChoiceAnswerBoxWithResult(
                        props.description || "",
                        () => {
                        },
                        props.resultAnswerList
                    )}
                </Col>
            </Card>
        </>
    );
};

export default BlankWithChoicesQuestionResultDisplay;
