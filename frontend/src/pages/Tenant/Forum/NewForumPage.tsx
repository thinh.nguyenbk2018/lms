import React from 'react';
import {Button, Space} from "antd";
import {useNavigate} from "react-router-dom";
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import { ROUTING_CONSTANTS } from '../../../navigation/ROUTING_CONSTANTS';
import AntdNotifier from '../../../utils/AntdAnnouncer/AntdNotifier';

const NewForumPage = () => {
    const navigate = useNavigate();
    const onCancel = () => {
        console.log("change danger new forum");
        navigate(ROUTING_CONSTANTS.FORUM);
        AntdNotifier.warn("Test message","Test message",123);
    }
    const onSubmit = () => {
        console.log("done fill info new forum");
        navigate(ROUTING_CONSTANTS.FORUM);
    }
    return (
        <>
            <ContentHeader
                title={"Tạo topic thảo luận"}
                action={
                    <Space>
                        <Button type="primary" danger onClick={onCancel}>
                            Hủy
                        </Button>
                        <Button
                            type="primary"
                            color="#73d13d"
                            onClick={onSubmit}
                        >
                            Xong
                        </Button>
                    </Space>
                }
            />
        </>
    );
};

export default NewForumPage;
