import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit"
import { generateStatus, loading, success} from "../../../utils/ReduxHelper/Generators";
import {AsyncStatus} from "../../interfaces/types";
import {RootState} from "../../store/store";
import {tenantService} from "./TenantService";


export interface TenantSlice{
    data: {
        firstname: string,
        lastname: string,
        username: string,
        password: string,
        domain: string,
    }
    // * This is only available on register success
    // * Usage: Put in headers and
    tenantID: string,
    status: AsyncStatus
}

const initialState: TenantSlice = {
    data: {
        firstname: "",
        lastname: "",
        username: "",
        password: "",
        domain: "",
    },
    tenantID: "",
    status: generateStatus()
}

export type TenantInfo = typeof initialState.data;

export const registerNewTenant = createAsyncThunk<string,TenantInfo>(
    'tenant/register',
    async (registerInfo, thunkAPI) => {
        try {
            const response = await tenantService.registerTenant(registerInfo);
            return response.domain;
        }
        catch(err) {
            return thunkAPI.rejectWithValue(err);
        }
    }
)

export const tenantSlice = createSlice({
    name: "tenant",
    initialState,
    reducers: {
        // Reducer comes here
        setTenantID: (state, action: PayloadAction<string>) => {
            state.tenantID = action.payload;
        }, 
        resetRegisterStatus: (state)=>{
            state.status = generateStatus();
        }
    },
    extraReducers: (builder) => {
        builder.addCase(registerNewTenant.fulfilled, (state, {payload}) => {
            state.data.domain = payload;
            state.status = success()
        })
        builder.addCase(registerNewTenant.pending, (state) => {
            state.status = loading();
        })
        builder.addCase(registerNewTenant.rejected, (state,action) => {
            state.status = {
                isSuccess: false,
                isError: true,
                isLoading: false,
                errors: (action as any)?.payload?.response?.data.message ?? "Error"
            };;
        })
    },
})

export const tenantSelector = {
    selectTenantInfo: (state: RootState) => state.tenant.data,

    selectTenantRegisterStatus: (state: RootState) => state.tenant.status,
    selectTenantRegisterSuccess: (state: RootState) => state.tenant.status.isSuccess,
    selectTenantRegisterError: (state: RootState) => state.tenant.status.isError,
    selectTenantRegisterPending: (state: RootState) => state.tenant.status.isLoading,
};


export const {setTenantID, resetRegisterStatus} = tenantSlice.actions;