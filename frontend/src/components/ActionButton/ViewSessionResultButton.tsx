import {
	faBan,
	faCircleRadiation,
	faStar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ButtonProps, Button } from "antd";
import {
	HideIcon,
	PublishIcon,
} from "@pages/Tenant/Courses/CourseContent/CourseContentPage";

const ViewSessionResultButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			type={"primary"}
			icon={
				<FontAwesomeIcon
					icon={faCircleRadiation}
					style={{ margin: "0 .5em 0 0" }}
				/>
			}
		>
			{`${props.title || "Xem bài làm học viên"}`}
		</Button>
	);
};

export default ViewSessionResultButton;
