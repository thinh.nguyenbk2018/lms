import React from 'react';
import PropTypes from 'prop-types';
import numericalFieldHandler from '../../utils/numericalFieldHandler';
import translateLabel from '../../utils/translateLabel';
import {Col, Input, Row, Space} from "antd";

const EndAfter = ({
                      id,
                      after,
                      handleChange,
                      translations
                  }) => (
    <Col>
        <Row gutter={[5, 10]} className="form-group m-0 row d-flex align-items-center">
            <Space>
                <Input
                    id={id}
                    name="end.after"
                    aria-label="End after"
                    className="form-control"
                    value={after}
                    onChange={numericalFieldHandler(handleChange)}
                />
                {translateLabel(translations, 'end.executions')}
            </Space>
        </Row>
    </Col>
);

EndAfter.propTypes = {
    id: PropTypes.string.isRequired,
    after: PropTypes.number.isRequired,
    handleChange: PropTypes.func.isRequired,
    translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default EndAfter;
