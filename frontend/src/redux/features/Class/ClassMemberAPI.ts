import {apiSlice} from "../CentralAPI";
import {
    AddMemberClassRequest,
    ClassMemberDto,
    ClassMemberListDto,
    ClassMemberQuery,
    ClassMemberSearchCriteria
} from "./ClassType";
import {transformSearchQuery} from "../APIUtils/TransformParameter";
import { ID } from "../../interfaces/types";



const classApiInvalidatesTags = apiSlice.enhanceEndpoints({addTagTypes: ['ClassMember']});

const classMemberAPI = classApiInvalidatesTags.injectEndpoints({
    endpoints: (build) => ({
        getClassMemberListPaginated: build.query<ClassMemberListDto, ClassMemberSearchCriteria>({
            query: (criteria) => `classes/${criteria.classId}/members?${transformSearchQuery(criteria)}`,
            providesTags: (result) =>
                // is result available?
                result
                    ? // successful query
                    [
                        ...result.listData.map(({ username }) => ({ type: 'ClassMember', username } as const)),
                        { type: 'ClassMember', username: 'LIST' },
                    ] : [{ type: 'ClassMember', username: 'LIST' }],
        }),
        addMember: build.mutation<ClassMemberDto, AddMemberClassRequest>({
            query (body) {
                return {
                    url: `classes/${body.classId}/members`,
                    method: 'POST',
                    body,
                }
            },
            invalidatesTags: (res, _err) => [{ type: 'ClassMember', username: res?.username }],
        }),
        updateMember: build.mutation<ClassMemberDto, AddMemberClassRequest>({
            query (body) {
                return {
                    url: `classes/${body.classId}/members/${body.memberId}`,
                    method: 'PUT',
                    body,
                }
            },
            invalidatesTags: (res, _err) => [{ type: 'ClassMember', username: res?.username }],
        }),
        getMember: build.query<ClassMemberDto, ClassMemberQuery>({
            query: (args) => `classes/${args.classId}/members/${args.memberId}`
        }),
        // * No need for pagination
        getTeacherOfClass: build.query<ID[],{classId: number} >({
            query: () => "classes"
        }),
    }),
    overrideExisting: false
});

export const {
    useGetClassMemberListPaginatedQuery,
    useAddMemberMutation,
    useGetMemberQuery,
    useUpdateMemberMutation
} = classMemberAPI;