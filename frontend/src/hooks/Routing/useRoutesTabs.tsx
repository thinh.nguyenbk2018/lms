import { useNavigate } from "react-router-dom";
import React from "react";
import { Menu } from "antd";
import IconSelector from "../../components/Icons/IconSelector";
import { NavigationItem, TabNav } from "../../navigation/NavigationItem";
import { authSelectors } from "../../redux/features/Auth/AuthSlice";
import { useAppSelector } from "@hooks/Redux/hooks";

const { SubMenu } = Menu;

const useRoutesTabs = (routes: NavigationItem[]) => {
	const navigate = useNavigate();
	const permissionsMap = useAppSelector(authSelectors.selectUserPermissions);
	const routesComputed = React.useMemo(
		() => displayRouteMenu(routes),
		[routes]
	);

	function displayRouteMenu(routes: NavigationItem[]) {
		const renderRoute = (route: NavigationItem) => {
			return route.type === "submenu" ?
				<SubMenu
					key={route.key}
					title={route.tab_name}
					icon={<IconSelector type={route.icon ? route.icon : ""} />}
				>
					{displayRouteMenu(route.children)}
				</SubMenu>
				: <Menu.Item
					key={(route as TabNav).key}
					onClick={() => {
						navigate(route.path);
					}}
					icon={<IconSelector type={(route as TabNav).icon ? (route as TabNav).icon : ""} />}
				>
					{(route as TabNav).tab_name}
				</Menu.Item>
		}

		return routes
			.filter(item => (item.required_permission.length === 0 || item.required_permission.some(permission => permissionsMap && permissionsMap[permission])))
			.filter((item) => item.type !== "link")
			.map(renderRoute);
	}

	return routesComputed;
};

export default useRoutesTabs;
