const QUESTION_LABEL_CONSTANT = {
    DESCRIPTION: "Mô tả đề",
    NOTE: "Ghi chú thêm",
    POINT: "Điểm"
}

export default QUESTION_LABEL_CONSTANT;