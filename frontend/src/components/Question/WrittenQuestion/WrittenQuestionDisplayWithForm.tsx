import { Form } from "formik-antd";
import React from "react";
import { QuestionViewAndTakingTestFormProps } from "../interface/question.interface";
import { WrittenQuestion } from "../../../redux/features/Quiz/CreateQuizSlice";
import { Field, FormikProps, withFormik } from "formik";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import * as Yup from "yup";
import { DEFAULT_FRONTEND_ID } from "../../../redux/interfaces/types";
import { QuestionAnswer } from "../../../redux/features/Quiz/examAPI";
import TextEditor from "../../TextEditor/TextEditor";
import TextEditorField from "../../CustomFields/TextEditorField/TextEditorField";
import { Card, Col, Divider, Row } from "antd";
import QuestionSubmissionButton from "../../ActionButton/QuestionSubmissionButton";
import './WrittenQuestionDisplayWithForm.less';
import AttachmentRender from "../Attachment/AttachmentRender";
import { Attachment } from "../Attachment/AttachmentQuestion";

/** Display the test for review the question content and possibly retrieve the answer */
const WrappedWrittentQuestionDisplay = (
	props: WrittenViewHocProps & FormikProps<QuestionAnswer>
) => {
	// * Question info to display the requirements
	const { description, studentAnswer } = props;

	// * Form State from formik
	const {
		errors,
		values,
		dirty,
		touched,
		isValid,
		handleSubmit,
		setFieldValue,
	} = props;

	return (
		<>
			<Form>
				<Card>
					<Col span={24} className={"written-question-box-wrapper"}>
						{
							JSON.parse(props.attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
								<AttachmentRender attachment={a} />
							</Row>)
						}
						<Divider />
						<TextEditor onChange={() => {
						}} value={props.description} readOnly={true} useBubbleTheme />
					</Col>
					<Field
						name={"answers[0]"}
						placeholder={"Nhập câu trả lời cho câu hỏi"}
						label={"Bài làm "}
						component={TextEditorField}
					/>
					<Row style={{ margin: "1em 0" }} justify="end">
						<QuestionSubmissionButton
							onClick={() => handleSubmit()}
							title={"Gửi đáp án"}
						/>
					</Row>
				</Card>
			</Form>
		</>
	);
};

type WrittenViewHocProps = QuestionViewAndTakingTestFormProps<WrittenQuestion>;
/** A question displayer need the Partial<QuestionDataType> to display the question statement
 *  ? QuestionAnswer to record student choice quen taking the test
 *
 * */
const WrittentQuestionDisplayWithForm = withFormik<
	WrittenViewHocProps,
	QuestionAnswer
>({
	mapPropsToValues: (props) => {
		return {
			id: props.id || DEFAULT_FRONTEND_ID,
			description: props.description || DEFAULT_TEXT_EDITOR_HTML,
			point: props.point || 0,
			// TODO [QUIZ]: pass answered to this field
			answers: [props.studentAnswer || ""],
			optionalStudentNote: "",
		};
	},
	validationSchema: Yup.object(),
	handleSubmit: (values, formikBag) => {
		const valuesToSubmit: QuestionAnswer = {
			...values,
		};
		formikBag.props.onConfirmSubmit?.(valuesToSubmit);
	},
})(WrappedWrittentQuestionDisplay);
export default WrittentQuestionDisplayWithForm;
