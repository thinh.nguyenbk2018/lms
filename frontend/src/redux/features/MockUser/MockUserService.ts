import {MockUserCreateDto} from "./MockUserSlice";
import request from "../../../utils/RequestHelper";
import {ID} from "../../interfaces/types";

const API_ENDPOINTS = {
    USER_MOCK: "/user_mock",
}

export const mockUserService = (() => {

    const createMockUser = async (payload : MockUserCreateDto) => {
        return request.postAsync(API_ENDPOINTS.USER_MOCK, payload);
    }

    const getMockUsers = async () => {
        return request.getAsync(API_ENDPOINTS.USER_MOCK);
    }

    const getMockUsersDetail = async (id: ID) => {
        return request.getAsync(`${API_ENDPOINTS.USER_MOCK}/${id}`);
    }

    return {createMockUser,getMockUsers,getMockUsersDetail};
})();

