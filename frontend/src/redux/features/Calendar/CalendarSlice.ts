import {addDays, formatISO, subDays} from "date-fns";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../store/store";
import {
    getDateFromString,
    getEventEndTime,
    getEventStartTime,
    getPaddedDateAndHourFromTimeStamp,
    takeWeek,
} from "../../../components/Calendar/Utils/CalendarUtils";
import {CalendarEvent} from "./CalendarAPI";
import {HashWithStringKey} from "../../interfaces/types";
/*
 * Event semantic: [where][type][name]
 * Checkin: Check with SQL database instead of embedding the data in Google Calendar
 * Class - Employee Assignment: Enforce with SQL
 */

export type EventHashMap = HashWithStringKey<CalendarEvent[]>;

export type CalendarViewType = "DAY" | "WEEK" | "MONTH" | "LIST";
export type CalendarOpeningModalType =
    | "CREATE_NEW_EVENT"
    | "UPDATE_EVENT"
    | "NONE";
type CalendarSlice = {
    selectedDate: string;
    calendarId: string | null;
    // * Each entry is a bucket of calendar event
    eventHashMap: EventHashMap;
    calendarViewType: CalendarViewType;
    openingModal: CalendarOpeningModalType;
};

const initialState: CalendarSlice = {
    selectedDate: new Date().toString(),
    calendarId: null,
    eventHashMap: {},
    calendarViewType: "WEEK",
    openingModal: "NONE",
};

/*
 * Hash the event with start hours --> Reduce search complexity when renddering the event on the calendar
    // * Return HH:MM padded --> Rounded down : 13:45:00 --> 13:45 --> 13:00
 * TODO: Can be more robust if we check for other presentation of event times
 */
export const calculateKey: (event: CalendarEvent) => string = (
    event: CalendarEvent
) => {
    if (event.eventType == "QUIZ_DEADLINE") {
        return getPaddedDateAndHourFromTimeStamp(getEventEndTime(event).setSeconds(0));
    }
    return getPaddedDateAndHourFromTimeStamp(getEventStartTime(event).setSeconds(0));
};

// * (Timestamp, "16:00") => ISOString with hour set to HH:00
export const prepareKey: (date: Date, hour: string) => string = (
    date,
    hour
) => {
    const d = new Date(date);
    const HH = hour.substring(0, 2);
    d.setHours(Number(HH), 0);
    return formatISO(d);
};
export const mapHasKey = (map: EventHashMap, key: string) => !!map[key];

export const getFromMap = (map: EventHashMap, key: string) => {
    if (mapHasKey(map, key)) return map[key];
    return undefined;
};

export const putToMap = (map: EventHashMap, event: CalendarEvent) => {
    const key = calculateKey(event);
    if (!mapHasKey(map, key)) {
        const bucket: CalendarEvent[] = [];
        bucket.push(event);
        map[key] = bucket;
        return;
    }
    map[key].push(event);
};

export const removeFromMap = (map: EventHashMap, event: CalendarEvent) => {
    const key = calculateKey(event);
    if (map[key]) {
        const bucket = map[key];
        const index = bucket.findIndex((item) => item.id === event.id);
        if (index) {
            bucket.splice(index, 1);
        }
    }
};

export const clearMap = (map: EventHashMap) => {
    map = {};
};
// ? Redux reduces can modify state directly (No need to spread object / array if we know key / index)
export const calendarSlice = createSlice({
    name: "calendar",
    initialState,
    reducers: {
        setCalendarId: (state, action: PayloadAction<string | null>) => {
            const {payload} = action;
            if (payload) {
                state.calendarId = payload;
            }
        },
        setSelectedDate: (state, action: PayloadAction<string | Date>) => {
            const {payload} = action;
            state.selectedDate = payload.toString();
        },
        increaseDate: (state) => {
            const date = getDateFromString(state.selectedDate);
            state.selectedDate = addDays(date, 1).toString();
        },
        decreaseDate: (state) => {
            const date = getDateFromString(state.selectedDate);
            state.selectedDate = subDays(date, 1).toString();
        },
        insertEventList: (state, action: PayloadAction<CalendarEvent[]>) => {
            const {payload} = action;
            payload.forEach((item) => {
                putToMap(state.eventHashMap, item);
            });
        },
        setEventList: (state, action: PayloadAction<CalendarEvent[]>) => {
            clearMap(state.eventHashMap);
            const {payload} = action;
            const newMap: EventHashMap = {};
            payload.forEach((item) => {
                putToMap(newMap, item);
            });
            state.eventHashMap = {...newMap};
            console.log("New calendar events is: ", state.eventHashMap);
        },
        // * Change view type:
        viewWeek: (state: CalendarSlice) => {
            state.calendarViewType = "WEEK";
        },
        viewDate: (state: CalendarSlice) => {
            state.calendarViewType = "DAY";
        },
        viewMonth: (state) => {
            state.calendarViewType = "MONTH";
        },
        viewList: (state) => {
            state.calendarViewType = "LIST";
        },
        // * New Event Modal State;
        setOpeningModal: (
            state: CalendarSlice,
            action: PayloadAction<CalendarOpeningModalType>
        ) => {
            state.openingModal = action.payload;
        },
        closeModal: (state: CalendarSlice) => {
            state.openingModal = "NONE";
        },
    },
});

export const calendarSelector = {
    selectCurrentDate: (state: RootState) =>
        getDateFromString(state.calendar.selectedDate),

    selectCurrentWeek: (state: RootState) => {
        const date = getDateFromString(state.calendar.selectedDate);
        const weekGetter = takeWeek(date);
        return weekGetter();
    },

    selectEventList: (state: RootState) => state.calendar.eventHashMap,

    // * Pure UI Component state
    currentCalendarView: (state: RootState) => state.calendar.calendarViewType,

    isViewingWeek: (state: RootState) =>
        state.calendar.calendarViewType === "WEEK",
    isViewingDay: (state: RootState) => state.calendar.calendarViewType === "DAY",

    isCreatingNewEvent: (state: RootState) =>
        state.calendar.openingModal === "CREATE_NEW_EVENT",
    isUpdatingEvent: (state: RootState) =>
        state.calendar.openingModal === "UPDATE_EVENT",
};

export const calendarActions = calendarSlice.actions;
export default calendarSlice.reducer;
