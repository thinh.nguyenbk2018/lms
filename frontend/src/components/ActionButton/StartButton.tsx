import { faBan, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ButtonProps, Button } from "antd";
import {
	HideIcon,
	PublishIcon,
} from "@pages/Tenant/Courses/CourseContent/CourseContentPage";

const HideButton = (props: ButtonProps) => {
	return (
		<Button
			{...props}
			type={"primary"}
			icon={<FontAwesomeIcon icon={faStar} style={{ margin: "0 .5em 0 0" }} />}
		>
			{`${props.title || "Bắt đầu"}`}
		</Button>
	);
};

export default HideButton;
