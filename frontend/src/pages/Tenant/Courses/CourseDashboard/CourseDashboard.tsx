import React from 'react';
import {useNavigate, useParams} from "react-router-dom";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import antdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import LoadingPage from "../../../UtilPages/LoadingPage";
import useCourseSidebar from "../../../../hooks/Routing/useCourseSidebar";
import {Avatar, Button, Card, Col, Form, Row} from "antd";
import {blue} from "@ant-design/colors";
import IMAGE_CONSTANTS from "@constants/DefaultImage";
import {useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {useGetCourseInformationQuery} from "@redux/features/Course/CourseAPI";
import {format} from "date-fns";
import {vi} from "date-fns/locale";
import './CourseDashBoard.less';
import InformationRow from "@components/InformationRow/InformationRow";
import TextEditor from "@components/TextEditor/TextEditor";

const CourseDashBoard = () => {
    // * Class specif routes
    const {courseId} = useParams();
    const {forceCloseSidebar} = useCourseSidebar();
    const navigate = useNavigate();
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    if (!courseId || isNaN(Number(courseId))) {
        antdNotifier.error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
        return <>
            <ApiErrorWithRetryButton onRetry={() => {
            }}/>
        </>;
    }

    const {
        data: courseDetail,
        isSuccess: isCourseDetailSuccess,
        isFetching: isCourseDetailFetching
    } = useGetCourseInformationQuery(Number(courseId), {skip: courseId == undefined});

    const renderFunction = () => {
        if (courseDetail) {
            return (
                <>
                    <ContentHeader title={courseDetail?.name || "Dashboard lớp học"}/>
                    <div className="class-dash-board-container">
                        <div className="class-dash-board-greeting">
                            <p className="greeting-text">
                                {`Chào mừng bạn quay trở lại, ${
                                    userInfo?.firstName || "học viên thân yêu"
                                }`}
                            </p>
                        </div>
                        <div className="course-dash-board-information class-dash-board__class-information">
                            <Row>
                                <Col span={24}>
                                    <Card
                                        title={"Thông tin khóa học"}
                                        headStyle={{
                                            backgroundColor: blue[5],
                                            color: "white",
                                            textAlign: "center",
                                        }}
                                    >
                                        <InformationRow title={'Mã'} value={courseDetail.code}/>
                                        <InformationRow title={'Trình độ'} value={courseDetail.level}/>
                                        <InformationRow title={'Cập nhật vào lúc'} value={format(courseDetail.updatedAt
                                            ? new Date(courseDetail.updatedAt)
                                            : new Date(), 'PP, p', {locale: vi})}/>
                                        <InformationRow title={'Người cập nhật'} value={<><Avatar src={courseDetail?.updatedBy?.avatar}
                                                                                                alt={courseDetail?.updatedBy?.firstName}/>
                                            <span
                                            style={{padding: "0 0.5rem"}}>{courseDetail?.updatedBy?.lastName} {courseDetail?.updatedBy?.firstName}</span></>} />
                                        <InformationRow title={'Mô tả'} value={<TextEditor
                                            onChange={() => {
                                            }}
                                            value={courseDetail.description}
                                            readOnly={true}
                                            useBubbleTheme
                                            />
                                        }/>
                                    </Card>
                                </Col>
                            </Row>
                        </div>
                        <Row className="class-background-container" justify="center">
                            <Col span={24}>
                                <img
                                    className="class-background-image"
                                    src={
                                        courseDetail.background || IMAGE_CONSTANTS.DEFAULT_BACKGROUND_IMAGE
                                    }
                                    alt="Ảnh nền"
                                />
                            </Col>
                        </Row>
                        <div className={'short-cut'}>
                            <div className={`class-dash-board-upcoming-sessions`}>
                                <p className="class-dash-board-upcoming-sessions__title">
                                    Lối tắt
                                </p>
                                <div className={'flex short-cut-btn-wrapper'}>
                                    <Button type={"primary"} ghost>Thêm lớp học</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        } else if (isCourseDetailFetching) {
            return <LoadingPage/>;
        }
        return <></>;
    };
    return renderFunction();

};

export default CourseDashBoard;