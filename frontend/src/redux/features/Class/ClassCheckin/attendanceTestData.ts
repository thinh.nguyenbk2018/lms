import {MeetingAttendance, OverallAttendance} from "./ClassAttendanceAPI";
// * Detailed for one session
export const meetingAttendance: MeetingAttendance = {
    metaInfo: {
        id: 1,
        startedAt: new Date(),
        finishedAt: new Date(),
        note: "notejasdfl; ",
        room: "asdjfaksl;fd",
        strategy: "ANY",
    },
    attendanceInfo: {
        "Lần 1 nè HIHI ": {
            metaInfo: {
                id: 1,
                createdAt: "12/21/2222",
                updatedAt: "12/21/2222",
            },
            studentAttendance: {
                1: {
                    name: "Nguyễn Hoàng Thịnh",
                    avatar:
                        "https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp&f=y",
                    state: "PRESENT",
                    attendanceRate: 33,
                },
                2: {
                    name: "Nguyễn Hoàng Thịnh",
                    avatar:
                        "https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp&f=y",
                    state: "PRESENT",
                    attendanceRate: 33,
                },
            },
        },
        "Lần 2 nè HIHI ": {
            metaInfo: {
                id: 1,
                createdAt: "12/21/2222",
                updatedAt: "12/21/2222",
            },
            studentAttendance: {
                1: {
                    name: "Nguyễn Hoàng Thịnh",
                    avatar:
                        "https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp&f=y",
                    state: "PRESENT",
                    attendanceRate: 33,
                },
                2: {
                    name: "Nguyễn Hoàng Thịnh",
                    avatar:
                        "https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp&f=y",
                    state: "PRESENT",
                    attendanceRate: 33,
                },
            },
        },
    },
};

const attendanceData: OverallAttendance = {
    statistics: {
        averageAttendance: 3.5,
        numOfStudentWithAboveAverageAttendance: 9,
        numOfStudentWithBelowAverageAttendance: 1,
        numOfStudentWithPerfectAttendance: 1,
    },
    attendanceInfo: {
        "12/29/2022": {
            metaInfo: {
                id: 1,
                startedAt: new Date(),
                finishedAt: new Date(),
                note: "asda",
                room: "12",
                strategy: "ANY",
            },
            studentAttendance: {
                1: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "LATE",
                    attendanceRate: 1.2,
                },
                2: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "ABSENT",
                    attendanceRate: 1.2,
                },
                3: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
                4: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
            },
        },
        "12/22/2022": {
            metaInfo: {
                id: 1,
                startedAt: new Date(),
                finishedAt: new Date(),
                note: "asda",
                room: "12",
                strategy: "LAST_TIME",
            },
            studentAttendance: {
                1: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "ABSENT",
                    attendanceRate: 1.2,
                },
                2: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
                3: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
                4: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "NONE",
                    attendanceRate: 1.2,
                },
            },
        },
        "12/23/2022": {
            metaInfo: {
                id: 1,
                startedAt: new Date(),
                finishedAt: new Date(),
                note: "asda",
                room: "12",
                strategy: "ANY",
            },
            studentAttendance: {
                1: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
                2: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
                3: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
                4: {
                    name: "THINH",
                    avatar: "asjdkl;fa",
                    state: "PRESENT",
                    attendanceRate: 1.2,
                },
            },
        },
    },
    happenedSession: 1,
    presentCount: {
        "1" : 2,
    }
};

export default attendanceData;
