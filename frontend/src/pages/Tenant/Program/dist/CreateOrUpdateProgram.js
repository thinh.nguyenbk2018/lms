"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
require("./CreateOrUdpateProgram.less");
var antd_1 = require("antd");
var formik_1 = require("formik");
var formik_antd_1 = require("formik-antd");
var lodash_1 = require("lodash");
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var Yup = require("yup");
var ContentHeader_1 = require("../../../components/ContentHeader/ContentHeader");
var SvgIcons_1 = require("../../../components/Icons/SvgIcons");
var ImageChooserWithFirebase_1 = require("../../../components/ImageChooser/ImageChooserWithFirebase");
var TextEditor_1 = require("../../../components/TextEditor/TextEditor");
var hooks_1 = require("../../../hooks/Redux/hooks");
var ROUTING_CONSTANTS_1 = require("../../../navigation/ROUTING_CONSTANTS");
var ApiErrors_1 = require("../../../redux/features/ApiErrors");
var CentralAPI_1 = require("../../../redux/features/CentralAPI");
var ProgramAPI_1 = require("../../../redux/features/Program/ProgramAPI");
var ProgramSlice_1 = require("../../../redux/features/Program/ProgramSlice");
var AntdNotifier_1 = require("../../../utils/AntdAnnouncer/AntdNotifier");
var RequestHelper_1 = require("../../../utils/RequestHelper");
var ApiErrorRetryPage_1 = require("../../UtilPages/ApiErrorRetryPage");
var LoadingPage_1 = require("../../UtilPages/LoadingPage");
var SortableCourse_1 = require("./SortableCourse");
var ProgramSchema = Yup.object().shape({
    name: Yup.string().required("Tên là trường bắt buộc"),
    code: Yup.string().required("Mã là trường bắt buộc")
});
var CreateOrUpdateProgram = function () {
    var id = react_router_dom_1.useParams().id;
    var navigate = react_router_dom_1.useNavigate();
    var dispatch = hooks_1.useAppDispatch();
    var createProgram = ProgramAPI_1.useCreateProgramMutation()[0];
    var updateProgram = ProgramAPI_1.useUpdateProgramMutation()[0];
    var selectCoursesInProgram = hooks_1.useAppSelector(function (state) {
        return ProgramSlice_1.programSelector.selectCoursesInProgram(state);
    });
    var formikRef = react_1.useRef(null);
    var getTitleOfPage = function () {
        return id ? "Thông tin chương trinh" : "Tạo chương trình mới";
    };
    var getTitleOfButton = function () {
        return id ? "Cập nhật" : "Tạo mới";
    };
    var isSkip = function (idParam) {
        return isNaN(Number(idParam));
    };
    var _a = ProgramAPI_1.useGetProgramQuery(lodash_1.toNumber(id), { skip: isSkip(id) }), data = _a.data, isError = _a.isError, isFetching = _a.isFetching, isSuccess = _a.isSuccess, error = _a.error;
    react_1.useEffect(function () {
        dispatch(ProgramSlice_1.setCoursesInProgram((data === null || data === void 0 ? void 0 : data.courses) || []));
    }, [data]);
    if (isSuccess && data) {
        return (react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement(formik_1.Formik, { innerRef: formikRef, enableReinitialize: true, initialValues: data || {}, onSubmit: function (values, actions) {
                    var dataToBeSent = {
                        name: values.name,
                        code: values.code,
                        description: values.description,
                        isStrict: !!values.isStrict,
                        isPublished: !!values.isPublished,
                        courses: selectCoursesInProgram
                    };
                    console.log("data to be sent: ", dataToBeSent);
                    if (id) {
                        var programUpdated = __assign(__assign({}, dataToBeSent), { id: lodash_1.toNumber(id) });
                        updateProgram(programUpdated)
                            .unwrap()
                            .then(function (_result) {
                            AntdNotifier_1["default"].success("Cập nhật chương trình thành công", "Thông báo", 1);
                            setTimeout(function () { return dispatch(ProgramSlice_1.setCoursesInProgram([])); }, 0);
                            navigate(ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.PROGRAM_LIST);
                        })["catch"](function (_err) { });
                    }
                    else {
                        createProgram(dataToBeSent)
                            .unwrap()
                            .then(function (_result) {
                            AntdNotifier_1["default"].success("Tạo chương trình thành công", "Thông báo", 1);
                            setTimeout(function () { return dispatch(ProgramSlice_1.setCoursesInProgram([])); }, 0);
                            navigate(ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.PROGRAM_LIST);
                        })["catch"](function (err) {
                            if (err.data.error ===
                                ApiErrors_1["default"].PROGRAM.PROGRAM_CODE_ALREADY_EXISTS) {
                                AntdNotifier_1["default"].error("Mã của chương trình đã tồn tại", "Thông báo", 1);
                            }
                        });
                    }
                    actions.setSubmitting(false);
                }, validationSchema: ProgramSchema }, function (_a) {
                var isValid = _a.isValid, isSubmitting = _a.isSubmitting, dirty = _a.dirty, values = _a.values, setFieldValue = _a.setFieldValue, submitForm = _a.submitForm;
                return (react_1["default"].createElement(react_1["default"].Fragment, null,
                    react_1["default"].createElement(formik_antd_1.Form, { layout: "vertical", initialValues: data },
                        react_1["default"].createElement(ContentHeader_1["default"], { title: getTitleOfPage(), action: react_1["default"].createElement(antd_1.Space, null,
                                react_1["default"].createElement(antd_1.Button, { type: "primary", danger: true, onClick: function () {
                                        setTimeout(function () { return dispatch(ProgramSlice_1.setCoursesInProgram([])); }, 0);
                                        navigate("" + ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.PROGRAM_LIST);
                                    }, className: "back-btn" },
                                    SvgIcons_1.backIconSvg,
                                    react_1["default"].createElement("span", null, "Quay l\u1EA1i")),
                                react_1["default"].createElement(antd_1.Button, { type: "primary", color: "#73d13d", onClick: function () { return submitForm(); }, className: "update-btn", disabled: !dirty || !isValid, loading: isSubmitting },
                                    SvgIcons_1.okeSvgIcon,
                                    react_1["default"].createElement("span", null, getTitleOfButton()))) }),
                        react_1["default"].createElement(antd_1.Row, null,
                            react_1["default"].createElement(antd_1.Col, { span: 12 },
                                react_1["default"].createElement(formik_antd_1.FormItem, { label: "T\u00EAn", name: "name", required: true },
                                    react_1["default"].createElement(formik_antd_1.Input, { name: "name" }))),
                            react_1["default"].createElement(antd_1.Col, { span: 6, offset: 1 },
                                react_1["default"].createElement(formik_antd_1.FormItem, { label: "M\u00E3 ch\u01B0\u01A1ng tr\u00ECnh (duy nh\u1EA5t)", name: "code", required: true },
                                    react_1["default"].createElement(formik_antd_1.Input, { name: "code", disabled: !isNaN(Number(id)) })))),
                        react_1["default"].createElement(antd_1.Row, null,
                            react_1["default"].createElement(antd_1.Col, { span: 24 },
                                react_1["default"].createElement(formik_antd_1.FormItem, { label: "M\u00F4 t\u1EA3 kh\u00F3a h\u1ECDc", name: "description" },
                                    react_1["default"].createElement(TextEditor_1["default"], { id: "program-information", onChange: function (d) {
                                            return setFieldValue("description", d);
                                        }, value: values.description })))),
                        react_1["default"].createElement(antd_1.Row, null,
                            react_1["default"].createElement(antd_1.Col, { span: 10 },
                                react_1["default"].createElement(formik_antd_1.FormItem, { className: "strict-checkbox", name: "isStrict", label: "\u00C1p d\u1EE5ng h\u1ECDc theo l\u1ED9 tr\u00ECnh" },
                                    react_1["default"].createElement(formik_antd_1.Checkbox, { name: "isStrict" }))),
                            react_1["default"].createElement(antd_1.Col, { span: 10, offset: 4 },
                                react_1["default"].createElement(formik_antd_1.FormItem, { className: "published-checkbox", name: "isPublished", label: "C\u00F4ng khai" },
                                    react_1["default"].createElement(formik_antd_1.Checkbox, { name: "isPublished" })))),
                        react_1["default"].createElement(antd_1.Row, null,
                            react_1["default"].createElement(formik_antd_1.FormItem, { label: "\u1EA2nh n\u1EC1n", name: "background" },
                                react_1["default"].createElement(ImageChooserWithFirebase_1.LocalImageChooserWithFirebase, { callback: function (url) {
                                        return setFieldValue("background", url);
                                    } })))),
                    react_1["default"].createElement(SortableCourse_1["default"], { setField: setFieldValue, data: values.courses })));
            }))));
    }
    else if (isFetching) {
        return react_1["default"].createElement(LoadingPage_1["default"], null);
    }
    else if (isError) {
        if (CentralAPI_1.isApiError(error)) {
            var errorMessage = RequestHelper_1.DEFAULT_ERROR_MESSAGE;
            var code = error.response.data.code;
            if (code === ApiErrors_1["default"].PROGRAM.PROGRAM_NOT_FOUND) {
                errorMessage = "Không tìm thấy program";
            }
            AntdNotifier_1["default"].apiError(errorMessage);
        }
        return react_1["default"].createElement(ApiErrorRetryPage_1["default"], null);
    }
    else
        return react_1["default"].createElement(react_1["default"].Fragment, null);
};
exports["default"] = CreateOrUpdateProgram;
