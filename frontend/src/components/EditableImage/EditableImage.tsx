import IconSelector from "@components/Icons/IconSelector"
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier"
import { storage } from "@utils/FirebaseSDK/firebase";
import { Button, Spin, Upload } from "antd"
import { UploadChangeParam, UploadFile } from "antd/lib/upload"
import { getDownloadURL, ref as fbRef, StorageReference, uploadBytes } from "firebase/storage";
import { CSSProperties, useState } from "react"

import './EditableImage.less'

interface EditableImageProps {
  src: string
  style?: CSSProperties
  onChangeImage: (url: string) => void
}

const EditableImage = ({ src, style, onChangeImage }: EditableImageProps) => {

  const [uploading, setUploading] = useState(false);

  const props = {
    name: 'file',
    multiple: true,
    async onChange(info: UploadChangeParam<UploadFile<any>>) {
      setUploading(true);
      let fileArrayBuffer = await info.file.originFileObj?.arrayBuffer();
      let systemStorageRef = fbRef(storage, "system");
      uploadBytes(fbRef(systemStorageRef, info.file.name), fileArrayBuffer!)
        .then((value) => {
          getDownloadURL(fbRef(systemStorageRef, value.metadata.name))
            .then(url => {
              onChangeImage(url);
              setUploading(false)
            })
        })
        .catch((reason: any) => {
          console.log(reason);
          AntdNotifier.error(`Xảy ra lỗi trong quá trình upload file ${info.file.name}`, "Lỗi Upload")
        })
        ;
    },
  };

  return <Spin spinning={uploading}>
    <div style={style} className="editable-image-wrapper">
      <Upload {...props} className="edit-button" itemRender={()=>null}>
        <Button shape="circle">
          <IconSelector type="EditOutlined" />
        </Button>
      </Upload>
      <img src={src} alt="anhr" style={{ objectFit: "cover", width: "100%", height: "100%" }} />
    </div>
  </Spin>
}


export default EditableImage;