import React, {useState} from 'react';
import {Avatar, Button, Card, Input, Popconfirm, Space, Tooltip, Typography} from "antd";
import {CloseCircleTwoTone} from "@ant-design/icons";
import Search from "antd/es/input/Search";
import {useAppDispatch, useAppSelector} from "../../../hooks/Redux/hooks";
import {chatSliceSelector, hideChatSearchBox} from "../../../redux/features/Chat/ChatSlice";
import {CreateChatRoomRequest, useCreateChatRoomMutation} from "../../../redux/features/Chat/ChatAPI";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {authSelectors} from "../../../redux/features/Auth/AuthSlice";
import {useGetUserSearchWithKeyWordQuery} from '../../../redux/features/UserSearch/UserSearchAPI';

interface User {
    id: string,
    image: string,
    name: string
}

const initUsers: User[] = []

const NewUser1vs1ChatPrompt = () => {
    const [newChatPartnerName, setNewChatPartnerName] = useState<string | undefined>();

    const userInfo = useAppSelector(authSelectors.selectCurrentUser);
    const [
        createChatRoom, // This is the mutation trigger
        {isLoading: isUpdating, isSuccess, isError}, // This is the destructured mutation result
    ] = useCreateChatRoomMutation();

    if (isError) {
        antdNotifier.error("Failed to create chat");
    }
    if (isSuccess) {
        antdNotifier.success("Created chat room successfully");
    }
    const handleInitChat1vs1: PopConfirmHandler = (e) => {
        if (!newChatPartnerName) {
            return;
        }
        const createChatRoomDto: CreateChatRoomRequest = {
            usersId: [],
            name: "",
            type: "USER",
        };
        console.log("Creating chat room");
        createChatRoom(createChatRoomDto);
    };
    return (
        <Popconfirm
            title={
                <Space>
                    <Typography.Text>
                        Enter user name
                    </Typography.Text>
                    <Input value={newChatPartnerName} onChange={e => setNewChatPartnerName(e.target.value)}>
                    </Input>
                </Space>
            }
            onConfirm={handleInitChat1vs1}
            onVisibleChange={() => console.log('visible change')}
        >
            <Button danger>
                Init Chat 1-1
            </Button>
        </Popconfirm>
    );
}
const NewUserGroupChatPrompt = () => {
    const [newChatPartnersName, setNewChatPartnersName] = useState<string | undefined>();
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    const [
        createChatRoom, // This is the mutation trigger
        {isLoading: isUpdating, isSuccess, isError}, // This is the destructured mutation result
    ] = useCreateChatRoomMutation();

    if (isError) {
        antdNotifier.error("Failed to create chat");
    }
    if (isSuccess) {
        antdNotifier.success("Created chat room successfully");
    }
    const handleChatGroupInit: PopConfirmHandler = (e) => {
        if (!newChatPartnersName) {
            return;
        }
        const userList = newChatPartnersName.split(";");
        const createChatRoomDto: CreateChatRoomRequest = {
            // usersId: userList.concat(userInfo?.username || "lmslms"),
            usersId: [],
            name: "",
            type: "USER",
        };
        createChatRoom(createChatRoomDto);
    };
    return (
        <Popconfirm
        title={
            <Space>
                <Typography.Text>
                    UserNames (';' seperated)
                </Typography.Text>
                <Input value={newChatPartnersName} onChange={e => setNewChatPartnersName(e.target.value)}>
                </Input>
            </Space>
        }
        onConfirm={handleChatGroupInit}
        onVisibleChange={() => console.log('visible change')}
    >
        <Button danger>
            Init Chat Group
        </Button>
    </Popconfirm>);
}


type PopConfirmHandler = ((e?: (React.MouseEvent<HTMLElement, MouseEvent> | undefined)) => void) | undefined;
type InputChangeHandler = React.ChangeEventHandler<HTMLInputElement> | undefined
// * Floating conversation
const NewMessagePromptBox = () => {
    const dispatch = useAppDispatch();


    const showChatSearchBox = useAppSelector(chatSliceSelector.selectShowChatSearchBox);
    const [searchData, setSearchData] = useState(""); 
    
    // const debounceSearch = useCallback(debounce((value) => useGetUserSearchWithKeyWordQuery(value),1000),[]); 

    // const {data,isError,isFetching,isSuccess} = useGetUserSearchWithKeyWordQuery(searchData);
    
    const onClose = () => {
        dispatch(hideChatSearchBox());
    }

    const handleSearchChange :InputChangeHandler = (e) => {
        setSearchData(e.target.value)
        
    }
    const handleSearch = (value:string) => {
        console.log(value);
    }
    
    const handleInitChatGroup: PopConfirmHandler = (e) => {
        const createChatRoomDto: CreateChatRoomRequest = {
            usersId: [],
            name: "",
            type: "GROUP",
        }
    }
    return (
        <Card title={
            <Space>
                <Typography.Text> Tin nhắn mới </Typography.Text>
                <Search placeholder={"Nhập tên người nhận"} onSearch={handleSearch} allowClear onChange={handleSearchChange}/>
            </Space>
        } extra={
            <Space style={{margin: "0 0 0 1em"}}>
                <Tooltip title={"Close"}>
                    <CloseCircleTwoTone style={{fontSize: "2em"}} onClick={onClose}/>
                </Tooltip>
            </Space>
        }
              style={{
                  width: 360,
                  zIndex: `9999`,
                  display: showChatSearchBox ? "static" : "none",
                  minHeight: `55vh`,
                  overflow: `auto`
              }}>
            <Space direction={"vertical"}>
                {
                    initUsers.map((item, index) => {
                        return <Space key={index} direction={"horizontal"} onClick={() => {
                            console.log("ITEM MESSAGE: ", item);
                        }
                        }>
                            <Avatar src={item.image}>
                            </Avatar>
                            <Typography.Text>{item.name}</Typography.Text>
                        </Space>
                    })
                }
            </Space>
            <Space>
                <NewUser1vs1ChatPrompt></NewUser1vs1ChatPrompt>
                <NewUserGroupChatPrompt></NewUserGroupChatPrompt>
            </Space>
        </Card>
    );
};

export default NewMessagePromptBox;
