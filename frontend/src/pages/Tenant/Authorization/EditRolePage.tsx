import { Button, Space, Spin } from "antd";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import RoleForm, { CreateRoleFormHandle } from "../../../components/Authorization/Role/RoleForm";
import { useNavigate, useParams } from "react-router-dom";
import { useRef } from "react";
import { useGetRoleQuery } from "../../../redux/features/Permission/RolesAPI";
import { Navigate } from 'react-router-dom';
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";

const EditRolePage = () => {
    let navigate = useNavigate();
    let { roleId } = useParams();
    let { data: getRoleResponse, isLoading, isError } = useGetRoleQuery({ roleId: parseInt(roleId!) });

    let createRoleFormRef = useRef<CreateRoleFormHandle>(null)

    const onCancel = () => {
        navigate(ROUTING_CONSTANTS.ROLES_MANAGEMENT);
    }

    const onDone = () => {
        createRoleFormRef.current?.submitEditForm()
            .then((result: any) => {
                if ("data" in result) {
                    AntdNotifier.success("Cập nhật vai trò thành công");
                    navigate(ROUTING_CONSTANTS.ROLES_MANAGEMENT)
                } else {
                    AntdNotifier.error("Có lỗi xảy ra trong quá trình lưu, thử lại sau");
                    console.log(result);
                }
            })
            .catch((error: any) => {

            })
    }

    if (isError) {
        return <Navigate to={ROUTING_CONSTANTS.UNAUTHORIZED}/>
    } else {
        return isLoading ? <Spin /> : <>
            <ContentHeader
                title="Sửa vai trò"
                action={
                    <Space>
                        <Button
                            danger
                            type="primary"
                            color="#73d13d"
                            onClick={onCancel}
                        >
                            Hủy
                        </Button>
                        <Button
                            type="primary"
                            color="#73d13d"
                            onClick={onDone}
                        >
                            Xong
                        </Button>
                    </Space>
                }
            />
            <RoleForm ref={createRoleFormRef} role={getRoleResponse?.role} />
        </>
    }
}


export default EditRolePage;