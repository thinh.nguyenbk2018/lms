import {useNavigate, useParams} from "react-router-dom";
import {
    useGetTextbookDetailQuery,
    useGetTextbooksQuery,
    useUpdateTextbookMutation
} from "@redux/features/Textbook/TextbookAPI";
import LoadingPage from "@pages/UtilPages/LoadingPage";
import {Card, Col, Modal, Row, Space} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook} from "@fortawesome/free-solid-svg-icons";
import {blue} from "@ant-design/colors";
import React from "react";
import ContentHeader from "@components/ContentHeader/ContentHeader";
import EditButton from "@components/ActionButton/EditButton";
import BackButton from "@components/ActionButton/BackButton";
import {setTextBookInEdit, textbookSelector, textbookSlice} from "@redux/features/Textbook/TextbookSlice";
import {useDispatch} from "react-redux";
import TextBookForm from "@components/TextBook/TextBookForm/TextBookForm";
import AntdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {textBookErrorsHandler} from "@pages/Tenant/TextBook/TextBookPage";
import {DEFAULT_FRONTEND_ID} from "@redux/interfaces/types";
import {Formik} from "formik";
import {useAppSelector} from "@hooks/Redux/hooks";
import {TextbookSchema} from "@components/TextBook/TextBookList";
import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";
import FileIconPreviewByFileName from "@components/FilePreview/FileIconPreviewByFileName";
import {getFileNameFromFirebaseUrl} from "@components/FilePreview/Utils/FileUtils";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";

const TextbookDetail = () => {

    const {textbookId, classId, courseId} = useParams();

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const searchCriteria = useAppSelector(textbookSelector.selectSearchCriteria);

    const {
        refetch: refetchCentralTextbooks
    } = useGetTextbooksQuery(searchCriteria);

    const textBookInEdit = useAppSelector(textbookSelector.selectTextBookInEdit);
    const [updateTextbook] = useUpdateTextbookMutation();

    const {
        data: textbook,
        isLoading,
        isSuccess
    } = useGetTextbookDetailQuery(Number(textbookId), {skip: isNaN(Number(textbookId))});

    if (isLoading) {
        return <LoadingPage/>;
    }
    if (isSuccess) {
        return (
            <>
                <ContentHeader
                    title={textbook.name}
                    action={
                        <>
                            <Space>
                                {/*<div className={"search-container"}>*/}
                                <BackButton
                                    type="primary"
                                    danger
                                    onClick={() => {
                                        if (classId == undefined && courseId == undefined) {
                                            refetchCentralTextbooks();
                                            navigate(
                                                ROUTING_CONSTANTS.TEXTBOOK_MANAGEMENT);
                                        } else if (classId != undefined) {
                                            navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_TEXTBOOK_MANAGEMENT, [":classId", classId]));
                                        } else {
                                            navigate(replacePathParams(ROUTING_CONSTANTS.COURSE_TEXTBOOK_MANAGEMENT, [":courseId", courseId || ""]));
                                        }
                                    }}
                                    className={"back-btn"}
                                >
                                    <span>Quay lại</span>
                                </BackButton>
                                {classId == undefined && courseId == undefined && <EditButton
                                    title={"Cập nhật"}
                                    onClick={() => {
                                        dispatch(setTextBookInEdit(textbook.id));
                                    }}
                                />}

                                {/*</div>*/}
                            </Space>
                        </>
                    }
                />
                {/*<Card*/}
                {/*    className="class-quiz-card-item"*/}
                {/*    title={*/}
                {/*        <Space align="center">*/}
                {/*            <FontAwesomeIcon*/}
                {/*                icon={faBook}*/}
                {/*                style={{margin: "0 .5em 0 0", color: blue.primary}}*/}
                {/*            />*/}
                {/*            <p style={{margin: 0, padding: 0}}>*/}
                {/*                {textbook?.name || "Không có thông tin"}*/}
                {/*            </p>*/}
                {/*        </Space>*/}
                {/*    }*/}
                {/*>*/}
                    <Row style={{marginTop: "1rem"}}>
                        <Col className={'course-unit-page-label course-unit-page-label-left'} span={3}>Tác giả</Col>
                        <Col span={19}>{textbook?.author}</Col>
                    </Row>
                    <Row>
                        <Col className={'course-unit-page-label course-unit-page-label-left'} span={3}>Mô tả</Col>
                        <Col span={19}>{textbook?.description || "Chưa có mô tả"}</Col>
                    </Row>
                    <Row>
                        <Col className={'course-unit-page-label course-unit-page-label-left'} span={3}>Tệp đính
                            kèm</Col>
                        <Col span={19}>
                            {textbook?.attachment ? <div className={'announcement-attachment-wrapper'}>
                                <a href={textbook?.attachment || "#"}>
                                    <FileIconPreviewByFileName size={25}
                                                               filename={getFileNameFromFirebaseUrl(textbook?.attachment)}
                                                               showName={true}
                                                               layout={'row'}
                                    />
                                </a>
                            </div> : <span>Chưa có tệp nào được tải lên</span>}
                        </Col>
                    </Row>
                {/*</Card>*/}
                <Formik
                    enableReinitialize={true}
                    validationSchema={TextbookSchema}
                    initialValues={textbook}
                    onSubmit={(values) => {
                        if (textBookInEdit) {
                            updateTextbook({...values, id: textBookInEdit})
                                .unwrap()
                                .then((res) => {
                                    dispatch(setTextBookInEdit(undefined));
                                    AntdNotifier.success("Cập nhật giáo trình thành công");
                                })
                                .catch((err) => {
                                    textBookErrorsHandler(err);
                                });
                        }
                    }}
                >
                    {({handleSubmit, resetForm, isValid, dirty}) => (
                        <Modal
                            title={"Cập nhật giáo trình"}
                            visible={!!textBookInEdit}
                            okButtonProps={{
                                disabled: !dirty,
                            }}
                            onOk={() => {
                                handleSubmit();
                            }}
                            afterClose={() => {
                                resetForm();
                                dispatch(textbookSlice.actions.setTextBookInEdit(undefined));
                            }}
                            onCancel={() => {
                                dispatch(setTextBookInEdit(undefined));
                            }}
                            destroyOnClose={true}
                            maskClosable={false}
                            okText={(textBookInEdit && textBookInEdit != DEFAULT_FRONTEND_ID) ? "Cập nhật" : "Thêm"}
                            cancelText={"Hủy"}
                        >
                            <TextBookForm
                                namespace={undefined}
                                labels={{
                                    NAME: "Tên giáo trình",
                                    AUTHOR: "Tên tác giả",
                                    DESCRIPTION: "Ghi chú thêm",
                                    ATTACHMENT: "Tệp đính kèm",
                                }}
                            />
                        </Modal>
                    )}
                </Formik>
            </>
        )
    }
    return <span>"Đã có lỗi xảy ra không quá trình hiển thị giáo trình"</span>;
}

export default TextbookDetail;