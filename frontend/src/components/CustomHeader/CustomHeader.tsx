import "./CustomHeader.less";

import {blue} from "@ant-design/colors";
import {EllipsisOutlined, MenuFoldOutlined, MenuUnfoldOutlined,} from "@ant-design/icons";
import {faSnapchat} from "@fortawesome/free-brands-svg-icons";
import {faGhost} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Avatar, Button, Dropdown, Menu, PageHeader, Space, Tooltip, Typography,} from "antd";
import classnames from "classnames";
import React, {useEffect} from "react";
import {Link, useNavigate} from "react-router-dom";

import logo from "@assets/logo/logo.png";
import {useAppDispatch, useAppSelector} from "../../hooks/Redux/hooks";
import {ROUTING_CONSTANTS} from "../../navigation/ROUTING_CONSTANTS";
import {authSelectors,} from "../../redux/features/Auth/AuthSlice";
import {sideBarActions, sideBarSelector,} from "../../redux/features/SideBar/SidebarSlice";
import {setTenantID} from "../../redux/features/Tenant/TenantSlice";
import {tenantUtils} from "@utils/TenantHelper/TenantUtils";
import ChatDrawer from "../Chat/ChatDrawer/ChatDrawer";
import IMAGE_CONSTANTS from "./../../constants/DefaultImage";
import LOCAL_STORAGE_KEYS from "@constants/LocalStorageKey";

const DropdownMenu = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    useEffect(() => {
        const tenantID = tenantUtils.getTenantIdFromHostname();
        if (tenantID) {
            dispatch(setTenantID(tenantID));
        } else console.log("No tenant id found");
    }, []);
    const onSignOut = () => {
        localStorage.clear();
        // dispatch(authActions.cleanUpOnSignOut());
        location.reload();
        // navigate(ROUTING_CONSTANTS.LOGIN_PAGE);
    };
    const navigateToUpdateUser = () => {
        // naviagate(ROUTING_CONSTANTS.)
    };

    const menu = (
        <Menu>
            <Menu.Item>
                <Link to={ROUTING_CONSTANTS.PERSONAL_INFORMATION}>
                    Thông tin cá nhân
                </Link>
            </Menu.Item>
            <Menu.Item onClick={onSignOut}>
                <Typography.Text>Đăng xuất</Typography.Text>
            </Menu.Item>
        </Menu>
    );
    return (
        <Dropdown key="more" overlay={menu}>
            <Button
                style={{
                    border: "none",
                    padding: 0,
                }}
            >
                <EllipsisOutlined
                    style={{
                        fontSize: 20,
                        verticalAlign: "top",
                    }}
                />
            </Button>
        </Dropdown>
    );
};

const CustomHeader = () => {
    const navigate = useNavigate();
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    // * Sidebar state
    const collapsed = useAppSelector(sideBarSelector.selectMainSidebarCollapse);
    const showSidebar = useAppSelector(sideBarSelector.selectShowSidebar);
    const sidebarCollapse = useAppSelector(
        sideBarSelector.selectMainSidebarCollapse
    );
    const dispatch = useAppDispatch();

    const onToggleVisibility = () => {
        dispatch(sideBarActions.setShowSidebar(!showSidebar));
    };
    const toggleCollapse = () => {
        dispatch(sideBarActions.setMainSidebarCollapse(!sidebarCollapse));
    };
    useEffect(() => {
        if (!userInfo?.id) {
            navigate(ROUTING_CONSTANTS.LOGIN_PAGE);
        }
        return () => {
        };
    }, []);

    return (
        <>
            <PageHeader
                title={
                    <Space>
                        <Typography.Text className={"brand-name"}>
                            {localStorage.getItem(LOCAL_STORAGE_KEYS.TENANT_NAME)}
                        </Typography.Text>
                        <Button
                            className={classnames({
                                "sidebar-visibility-toggle": true,
                                "sidebar-shower": !showSidebar,
                            })}
                            onClick={onToggleVisibility}
                        >
                            {showSidebar ? (
                                <FontAwesomeIcon icon={faSnapchat}/>
                            ) : (
                                <FontAwesomeIcon icon={faGhost}/>
                            )}
                        </Button>
                        <Tooltip title={"Đóng mở sidebar"}>
                            <div onClick={() => toggleCollapse()} style={{color: blue[6]}}>
                                {collapsed ? <MenuUnfoldOutlined/> : <MenuFoldOutlined/>}
                            </div>
                        </Tooltip>
                    </Space>
                }
                className="site-page-header custom-header"
                extra={
                    <Space>
                        <Avatar src={userInfo?.avatar || IMAGE_CONSTANTS.DEFAULT_AVATAR}/>
                        <Typography.Text>
                            {(userInfo?.firstName && `${userInfo?.lastName} ${userInfo?.firstName}`) ||
                                userInfo?.username}
                        </Typography.Text>
                        <DropdownMenu key="more"/>
                    </Space>
                }
                avatar={{src: localStorage.getItem(LOCAL_STORAGE_KEYS.LOGO) || logo, alt: `https://joeschmoe.io/api/v1/random`}}
            />
        </>
    );
};

export default CustomHeader;
