"use strict";
var _a;
exports.__esModule = true;
exports.removeTemporaryChatWithUserId = exports.addTemporaryChatWithUserId = exports.setChatList = exports.hideChatSearchBox = exports.showChatSearchBox = exports.setChatState = exports.showChat = exports.hideChat = exports.minimizeChat = exports.chatSliceSelector = exports.chatSlice = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
var initialState = {
    showChatSearchBox: false,
    entities: {}
};
/*
    * CLick on user found
    *   - Create new transient chat box
    *   - If user conv existed --> Receive Real Active Chat --> Add to chat slice in active state
    *   - If user conv not existed --> Receive Fake Active Chat --> With no Chat Room Id and only user id and empty list message
    *
 */
// ? Redux reduces can modify state directly (No need to spread object / array if we know key / index)
exports.chatSlice = toolkit_1.createSlice({
    name: 'chat',
    initialState: initialState,
    reducers: {
        showChatSearchBox: function (state) {
            state.showChatSearchBox = true;
        },
        hideChatSearchBox: function (state) {
            state.showChatSearchBox = false;
        },
        minimizeChat: function (state, action) {
            state.entities[action.payload].state = "minimized";
        },
        hideChat: function (state, action) {
            state.entities[action.payload].state = "hidden";
        },
        showChat: function (state, action) {
            state.entities[action.payload].state = "active";
        },
        setChatState: function (state, action) {
            var _a = action.payload, id = _a.id, newChatState = _a.state;
            state.entities[id].state = newChatState;
        },
        setChatList: function (state, action) {
            var payload = action.payload;
            var newEntities = {};
            if (!payload) {
                return;
            }
            payload.forEach(function (item) {
                // @ts-ignore
                newEntities[item] = {
                    id: item,
                    state: "hidden"
                };
            });
            state.entities = newEntities;
        },
        addTemporaryChatWithUserId: function (state, action) {
            var userID = action.payload;
            state.entities[userID] = {
                id: userID,
                state: "active"
            };
        },
        removeTemporaryChatWithUserId: function (state, action) {
            var userID = action.payload;
            delete state.entities[userID];
        }
    }
});
exports.chatSliceSelector = {
    selectActiveChats: function (state) { return Object.values(state.chat.entities).filter(function (item) { return item.state === "active"; }); },
    selectMinimizedChats: function (state) { return Object.values(state.chat.entities).filter(function (item) { return item.state === "minimized"; }); },
    selectHiddenChats: function (state) { return Object.values(state.chat.entities).filter(function (item) { return item.state === "hidden"; }); },
    selectShowChatSearchBox: function (state) { return state.chat.showChatSearchBox; }
};
exports.minimizeChat = (_a = exports.chatSlice.actions, _a.minimizeChat), exports.hideChat = _a.hideChat, exports.showChat = _a.showChat, exports.setChatState = _a.setChatState, exports.showChatSearchBox = _a.showChatSearchBox, exports.hideChatSearchBox = _a.hideChatSearchBox, exports.setChatList = _a.setChatList, exports.addTemporaryChatWithUserId = _a.addTemporaryChatWithUserId, exports.removeTemporaryChatWithUserId = _a.removeTemporaryChatWithUserId;
exports["default"] = exports.chatSlice.reducer;
