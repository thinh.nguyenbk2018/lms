import {Button, Modal, Row, Space, Table, Tag, Typography,} from "antd";
import {Link, useNavigate, useParams} from "react-router-dom";
import PublishButton from "../../../../../components/ActionButton/PublishButton";
import ContentHeader from "../../../../../components/ContentHeader/ContentHeader";
import {useAppSelector,} from "@hooks/Redux/hooks";
import useClassSidebar from "../../../../../hooks/Routing/useClassSidebar";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import {
    useGetClassInformationQuery,
    useGetClassQuizDetailQuery,
    useHideQuizMutation,
    usePublishQuizMutation,
} from "@redux/features/Class/ClassAPI";
import LoadingPage from "../../../../UtilPages/LoadingPage";
import {AddQuizItemModal} from "../ClassContentPage";
import HideButton from "../../../../../components/ActionButton/HideButton";
import QuizConfigurationDrawer from "../../Quiz/QuizInstanceWithConfig/QuizConfigurationDrawer";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import {
    StudentSessionResult,
    useGetSessionsResultOfStudentQuery,
    useGetTestQuery,
    useInitTestSessionMutation,
} from "@redux/features/Quiz/examAPI";
import {testWithConfigInfoSelectors,} from "@redux/features/Quiz/TestWithConfigInfoSlice";
import "./ClassQuizDetailPage.less";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import ViewSessionResultButton from "../../../../../components/ActionButton/ViewSessionResultButton";
import API_ERRORS from "../../../../../redux/features/ApiErrors";
import {errorArrayHandlingHelper,} from "@redux/features/CentralAPI";
import ApiErrorRetryPage from "../../../../UtilPages/ApiErrorRetryPage";
import timeStampHelper from "@utils/TimeStampHelper/TimeStampHelper";
import React from "react";
import {ExclamationCircleOutlined} from "@ant-design/icons";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import InformationRow from "@components/InformationRow/InformationRow";
import './ClassQuizDetailPage.less';

export const examErrorsHandler = (err: any) => {
    const {
        EXAM_NOT_FOUND,
        EXAM_INSTANCE_HAS_ALREADY_CLOSED,
        EXAM_INSTANCE_IS_NOT_FULLY_CONFIGURED,
        FINISHED_TIME_NOT_VALID,
        EXAM_SESSION_EXPIRED,
        EXAM_HAS_NOT_SCORED_FOR_STUDENT,
        EXAM_INSTANCE_HAS_NOT_CONFIGURED_YET,
        QUIZ_IS_NOT_ALLOWED_TO_PUBLISH,
        SESSION_HAS_NOT_ENDED,
    } = API_ERRORS.EXAM;
    errorArrayHandlingHelper(
        err,
        [EXAM_SESSION_EXPIRED, "Đã kết thúc thời gian làm bài"],
        [EXAM_NOT_FOUND, "Không tìm thấy bài kiểm tra"],
        [EXAM_INSTANCE_HAS_ALREADY_CLOSED, "Bài kiểm tra đã kết thúc"],
        [
            EXAM_INSTANCE_IS_NOT_FULLY_CONFIGURED,
            "Bài kiểm tra chưa được cấu hình hoàn toàn",
        ],
        [EXAM_INSTANCE_HAS_NOT_CONFIGURED_YET, "Bài kiểm tra chưa được cấu hình"],
        [FINISHED_TIME_NOT_VALID, "Thời gian kết thúc không hợp lệ"],
        [EXAM_HAS_NOT_SCORED_FOR_STUDENT, "Bài kiểm tra chưa được chấm điểm"],
        [
            QUIZ_IS_NOT_ALLOWED_TO_PUBLISH,
            "Không được phép publish bài kiểm tra này",
        ],
        [SESSION_HAS_NOT_ENDED, "Phiên làm bài vẫn chưa kết thúc "]
    );
};

const ClassQuizDetailPage = () => {
    const {quizItemId, classId} = useParams();

    const [closeSidebar] = useClassSidebar();
    const navigate = useNavigate();

    // * Redux query states
    const {
        data: classInfo,
        isSuccess: isSuccessClassInfo,
        isError: isErrorClassInfo,
        isFetching: isFetchingClassInfo,
    } = useGetClassInformationQuery(Number(classId));
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    // * Test questions search criteria
    const searchCriteria = useAppSelector(
        testWithConfigInfoSelectors.selectSearchCriteria
    );

    const shouldSkip = () => {
        return (
            isNaN(Number(quizItemId)) ||
            isNaN(Number(classId))
        );
    };
    // * Quiz Item Data
    const {
        data: quizItemData,
        isSuccess,
        isError: isFetchQuizError,
        isFetching,
        error,
        isLoading: isLoadingQuiz
    } = useGetClassQuizDetailQuery(
        {
            classId: Number(classId),
            quizId: Number(quizItemId),
        },
        {
            skip: shouldSkip(),
        }
    );

    // STUDENT ROLE
    const {
        data: previousSession,
        isSuccess: getPreviousSessionForStudentSuccess,
        refetch: refetchSessionsResultOfStudent
    } = useGetSessionsResultOfStudentQuery({
        classId: Number(classId),
        quizId: Number(quizItemId),
    }, {
        skip: userInfo?.accountType === 'STAFF' || shouldSkip()
    })

    // TODO: update auth
    const isTeacherOfThisClassWithViewingQuestionPermission = userInfo?.accountType === 'STAFF';
    // * Get the exam questions
    const {
        data: testQuestion,
        isSuccess: isQuestionListSuccess,
        isError: isQuestionListError,
        isFetching: isQuestionListFetching,
        isUninitialized: isQuestionListUninitialized,
        error: questionListError,
    } = useGetTestQuery(
        {
            courseId: Number(classInfo?.course?.id),
            testId: Number(quizItemData?.examId),
            ...searchCriteria,
        },
        {
            skip: !classInfo?.course?.id || !quizItemData?.examId || userInfo?.accountType === 'STUDENT' || shouldSkip()
        }
    );
    // * Teacher or privileged user
    const [publishQuiz] = usePublishQuizMutation();
    const [hideQuiz] = useHideQuizMutation();

    // * Student specific
    const [initSession] = useInitTestSessionMutation();

    const columns = [
        {
            title: "Tình trạng",
            width: "10%",
            align: "center" as const,
            render: (session: StudentSessionResult) => {
                if (session.submittedAt) {
                    return <Tag color={'teal'}>Đã hoàn thành</Tag>
                }
                return <Tag color={'#c91c00'}>Chưa hoàn thành</Tag>
            }
        },
        {
            title: "Thời gian kết thúc",
            dataIndex: "submittedAt",
            key: "submittedAt",
            width: "20%",
            align: "center" as const,
            render: (submittedAt: Date | undefined) => {
                if (!submittedAt) {
                    return <></>
                }
                return timeStampHelper.formatDateTime(submittedAt);
            }
        },
        {
            title: "Thời gian làm bài",
            dataIndex: "timeTaken",
            key: "timeTaken",
            width: "20%",
            align: "center" as const,
            render: (timeTaken: number) => `${Math.floor(timeTaken / 60)} phút ${timeTaken - Math.floor(timeTaken / 60) * 60} giây`
        },
        {
            title: `Điểm / ${quizItemData?.totalGrade}`,
            dataIndex: "score",
            key: "score",
            width: "10%",
            align: "center" as const,
            render: (s: number) => s?.toFixed(2)
        },
        {
            title: "Xem lại",
            width: "20%",
            align: "center" as const,
            render: (session: StudentSessionResult) => {
                if (session.submittedAt == undefined) {
                    return <></>
                }
                if (quizItemData?.config?.viewPreviousSessions
                    && (quizItemData?.config?.viewPreviousSessionsTime != undefined && new Date(quizItemData?.config.viewPreviousSessionsTime) < new Date())) {
                    return <Link to={replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_STUDENT_SESSION,
                        [":classId", classId || ""],
                        [":quizItemId", quizItemId || ""],
                        [":testSessionId", session.sessionId])}>Xem lại</Link>
                }
                return <span>Không được xem lại</span>
            }
        }
    ];

    const handleAccessibilityChange = () => {
        if (!quizItemData || isNaN(Number(classId))) {
            AntdNotifier.error("Không tìm thấy quiz để thay đổi trạng thái");
            return;
        }
        switch (quizItemData.state) {
            case "PRIVATE":
                // * Thing is private --> We publish it
                publishQuiz({
                    classId: Number(classId),
                    quizId: quizItemData.id,
                }).unwrap()
                    .then(res => {
                        AntdNotifier.success("Hiển thị bài kiểm tra cho học viên thành công")
                    })
                    .catch(err => {
                        if (err.data) {
                            AntdNotifier.error(err.data.message ? err.data?.message : "Có lỗi xảy ra hiển thị bài kiểm tra cho học viên.")
                        } else {
                            AntdNotifier.error("Có lỗi xảy ra hiển thị bài kiểm tra cho học viên.")
                        }
                    });
                return;
            case "PUBLIC":
                AntdNotifier.handlePromise(
                    hideQuiz({
                        classId: Number(classId),
                        quizId: quizItemData.id,
                    }).unwrap(),
                    "Ẩn bài kiểm tra",
                    () => {
                    },
                    examErrorsHandler
                );
                return;
            default:
                return;
        }
    };

    if (isSuccessClassInfo && isSuccess && classInfo.id && classInfo.course?.id) {
        return (
            <div className="main-container">
                <ContentHeader
                    title={`${quizItemData.title}`}
                    action={
                        <Space>
                            <PermissionRenderer permissions={[PermissionEnum.VIEW_QUIZ_RESULT_MANAGEMENT]}>
                                <ViewSessionResultButton
                                    onClick={() => {
                                        let pathToNavTo = "";
                                        if (isTeacherOfThisClassWithViewingQuestionPermission) {
                                            pathToNavTo = replacePathParams(
                                                ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_MANAGEMENT,
                                                [":classId", Number(classId)],
                                                [":quizItemId", Number(quizItemId)]
                                            );
                                            navigate(pathToNavTo);
                                        } else if (
                                            isTeacherOfThisClassWithViewingQuestionPermission
                                        ) {
                                            pathToNavTo = replacePathParams(
                                                ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_MANAGEMENT,
                                                [":classId", Number(classId)],
                                                [":quizItemId", Number(quizItemId)]
                                            );
                                            navigate(pathToNavTo);
                                        } else {
                                            AntdNotifier.error(
                                                "Bạn không  có đủ quyền để xem thông tin này "
                                            );
                                        }
                                    }}
                                />
                            </PermissionRenderer>
                            <PermissionRenderer permissions={[PermissionEnum.UPDATE_QUIZ_CLASS]}>
                                <QuizConfigurationDrawer
                                    config={quizItemData?.config}
                                    classId={classInfo.id}
                                    courseId={classInfo.course.id}
                                />
                                {quizItemData.state && quizItemData.state === "PRIVATE" ? (
                                    <PublishButton style={{marginLeft: '0.25rem'}} onClick={() => handleAccessibilityChange()}/>
                                ) : (
                                    <HideButton style={{marginLeft: '0.25rem'}} onClick={() => handleAccessibilityChange()}/>
                                )}
                            </PermissionRenderer>

                        </Space>
                    }
                />
                <Row justify="center">
                    <div className="quiz-item-info-card quiz-item-info-card-class">
                        <InformationRow title={'Mô tả'} value={quizItemData.description}/>
                        <InformationRow title={'Tên cột điểm'} value={quizItemData.tag}/>
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Mô tả:</div>*/}
                        {/*    <div className="field-value">{quizItemData.description} </div>*/}
                        {/*</div>*/}
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-container">*/}
                        {/*        <div className="field-name">Tag tính điểm:</div>*/}
                        {/*        <div className="field-value">{quizItemData.tag}</div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {isTeacherOfThisClassWithViewingQuestionPermission && (
                            // <div className="field-container">
                            //     <div className="field-name"> Đề:</div>
                            //     <div
                            //         className="field-value"
                            //         onClick={() => {
                            //             if (!classInfo?.course?.id || !quizItemData?.id) {
                            //                 return;
                            //             }
                            //             navigate(
                            //                 replacePathParams(
                            //                     ROUTING_CONSTANTS.COURSE_QUIZ_DETAIL,
                            //                     [":courseId", classInfo?.course?.id],
                            //                     [":testId", quizItemData?.examId]
                            //                 )
                            //             );
                            //         }}
                            //     >
                            //         <Typography.Link>{testQuestion?.title}</Typography.Link>
                            //     </div>
                            // </div>
                            <InformationRow title={'Đề'} value={<div
                                className="field-value"
                                onClick={() => {
                                    if (!classInfo?.course?.id || !quizItemData?.id) {
                                        return;
                                    }
                                    navigate(
                                        replacePathParams(
                                            ROUTING_CONSTANTS.COURSE_EXAM_DETAIL,
                                            [":courseId", classInfo?.course?.id],
                                            [":testId", quizItemData?.examId]
                                        )
                                    );
                                }}
                            ><Typography.Link>{testQuestion?.title}</Typography.Link></div>}/>
                        )}

                        <InformationRow title={'Thời gian làm bài'} value={quizItemData.config?.haveTimeLimit ? `${quizItemData.config.timeLimit} phút` : 'Không giới hạn thời gian'}/>
                        <InformationRow title={'Cách thức chấm điểm'} value={'Lần làm bài cuối cùng'}/>
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Thời gian làm bài:</div>*/}
                        {/*    <div*/}
                        {/*        className="field-value">{quizItemData.config?.haveTimeLimit ? quizItemData.config.timeLimit : 'Không giới hạn thời gian'}</div>*/}
                        {/*</div>*/}
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Cách thức chấm điểm:</div>*/}
                        {/*    <div className="field-value">Lần làm bài cuối cùng</div>*/}
                        {/*</div>*/}
                        <InformationRow title={'Thời gian có thể bắt đầu làm bài'} value={timeStampHelper.formatDateTime(quizItemData.config?.startedAt)}/>
                        <InformationRow title={'Thời gian kết thúc làm bài'} value={timeStampHelper.formatDateTime(quizItemData.config?.validBefore)}/>
                        <InformationRow title={'Số lần làm bài cho phép'} value={(quizItemData.config?.maxAttempt == undefined || quizItemData.config?.maxAttempt <= 0) ? 'Không giới hạn' : quizItemData.config?.maxAttempt}/>
                        <InformationRow title={'Điểm đạt'} value={quizItemData.config?.passScore ? quizItemData.config?.passScore : '0'}/>
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Thời gian có thể bắt đầu làm bài:</div>*/}
                        {/*    <div*/}
                        {/*        className="field-value">{timeStampHelper.formatDateTime(quizItemData.config?.startedAt)}</div>*/}
                        {/*</div>*/}
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Thời gian kết thúc làm bài:</div>*/}
                        {/*    <div*/}
                        {/*        className="field-value">{timeStampHelper.formatDateTime(quizItemData.config?.validBefore)}</div>*/}
                        {/*</div>*/}
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Số lần làm bài cho phép:</div>*/}
                        {/*    <div*/}
                        {/*        className="field-value">{(quizItemData.config?.maxAttempt == undefined || quizItemData.config?.maxAttempt <= 0) ? 'Không giới hạn' : quizItemData.config?.maxAttempt}</div>*/}
                        {/*</div>*/}
                        {/*<div className="field-container">*/}
                        {/*    <div className="field-name">Điểm đạt:</div>*/}
                        {/*    <div*/}
                        {/*        className="field-value">{quizItemData.config?.passScore ? quizItemData.config?.passScore : '0'}</div>*/}
                        {/*</div>*/}
                    </div>
                </Row>
                {/*{getPreviousSessionForStudentSuccess && previousSession.listData.map((item) => (*/}
                {/*    <SessionReviewCard {...item} key={item.sessionId}/>*/}
                {/*))}*/}
                <AddQuizItemModal courseIdToSearch={classInfo.courseId || 0}/>
                <div className={'flex justify-content-center'}
                     style={{
                         display: (userInfo?.accountType === 'STAFF'
                             || (!quizItemData.isAllowedToInitSession && !quizItemData.isAllowedToContinueLastSession)) ? 'none' : ''
                     }}>
                    <Button
                        type="primary" color="#73d13d"
                        style={{borderRadius: '0.25rem', marginBottom: '1rem'}}
                        onClick={() => {
                            if (!classId) {
                                return;
                            }
                            if (quizItemData.isAllowedToInitSession) {
                                Modal.confirm({
                                    wrapClassName: 'confirm-continue-modal-wrapper',
                                    title: (
                                        <span
                                            className={'confirm-delete-modal-title-name'}>Bắt đầu làm bài kiểm tra <b>{quizItemData.title}</b></span>
                                    ),
                                    content: (
                                        <span>Đồng hồ sẽ bắt đầu đếm ngược, bạn đã sẵn sàng làm bài?</span>
                                    ),
                                    onOk: () => {
                                        initSession({
                                            testWithConfigId: Number(quizItemId),
                                            classId: Number(classId),
                                        })
                                            .unwrap()
                                            .then((res) => {
                                                // AntdNotifier.success("Session created: " + res.id);
                                                navigate(
                                                    replacePathParams(
                                                        ROUTING_CONSTANTS.CLASS_QUIZ_SESSION,
                                                        [":quizId", Number(quizItemId)],
                                                        [":classId", Number(classId)],
                                                        [":testSessionId", res.id]
                                                    )
                                                );
                                            })
                                            .catch((err: any) => {
                                                if (err.data) {
                                                    AntdNotifier.error(err.data.message ? err.data?.message : "Có lỗi xảy ra khi bắt đầu làm bài, vui lòng liên hệ với giáo viên.")
                                                } else {
                                                    AntdNotifier.error("Có lỗi xảy ra khi bắt đầu làm bài, vui lòng liên hệ với giáo viên.")
                                                }
                                            });
                                    },
                                    icon: <ExclamationCircleOutlined/>,
                                    okText: `Làm bài`,
                                    cancelText: 'Hủy',
                                    okButtonProps: {className: 'confirm-continue-modal-ok-btn'},
                                    cancelButtonProps: {className: 'confirm-continue-modal-cancel-btn'}
                                })
                            } else if (quizItemData.isAllowedToContinueLastSession && quizItemData.unEndedSessionId) {
                                navigate(replacePathParams(
                                    ROUTING_CONSTANTS.CLASS_QUIZ_SESSION,
                                    [":quizId", Number(quizItemId)],
                                    [":classId", Number(classId)],
                                    [":testSessionId", quizItemData.unEndedSessionId]
                                ))
                            }
                        }}
                    >
                        {quizItemData?.isAllowedToInitSession
                            ? 'Bắt đầu làm bài'
                            : quizItemData?.isAllowedToContinueLastSession ? 'Tiếp tục lần làm bài cuối cùng' : ''}
                    </Button>
                </div>
                {
                    userInfo?.accountType === 'STUDENT' && quizItemData?.config?.startedAt != undefined && new Date(quizItemData?.config?.startedAt) > new Date() ? <div style={{textAlign: 'center', color: '#c91c00'}}>Chưa tới thời gian làm bài, vui lòng quay lại sau.</div> : ''
                }

                {
                    userInfo?.accountType === 'STUDENT' && quizItemData?.config?.validBefore != undefined && new Date(quizItemData?.config?.validBefore) < new Date() ? <div style={{textAlign: 'center', color: '#c91c00'}}>Bài kiểm tra đã đã quá hạn làm bài.</div> : ''
                }

                {
                    previousSession?.listData && previousSession?.listData.length > 0 && <Table
                        loading={isLoadingQuiz}
                        columns={columns || []}
                        dataSource={previousSession?.listData || []}
                        rowKey="sessionId"
                    />
                }
            </div>
        );
    } else if (isFetchingClassInfo || isFetching) {
        return <LoadingPage/>;
    } else if (isFetchQuizError || questionListError) {
        examErrorsHandler(isFetchQuizError);
        examErrorsHandler(questionListError);
        return <ApiErrorRetryPage/>;
    } else {
        return <></>;
    }
};

export default ClassQuizDetailPage;
