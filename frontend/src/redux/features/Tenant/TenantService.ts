import request from "../../../utils/RequestHelper";
import {TenantInfo} from "./TenantSlice";

const API_ENDPOINTS = {
    TENANT_REGISTER: "/tenant/register",
}

export const tenantService = (() => {

    const registerTenant = async (payload:TenantInfo) => {
        return request.postAsync(API_ENDPOINTS.TENANT_REGISTER,payload);
    }

    return {registerTenant};
})();
