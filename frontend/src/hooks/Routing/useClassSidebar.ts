import {useAppDispatch} from "../Redux/hooks";
import {useNavigate, useParams} from "react-router-dom";
import {useEffect} from "react";
import {sideBarActions} from "../../redux/features/SideBar/SidebarSlice";
import { useGetClassInformationQuery} from "../../redux/features/Class/ClassAPI";
import React from "react";
import AntdNotifier from "../../utils/AntdAnnouncer/AntdNotifier";


export const isPathRelatedToClass : (path: string) => boolean = (path) => {
    const pathnames = path.split("/");
    return pathnames.includes("classes");

}
// TODO: Can be customized to receive the class Id and do something specific for the class
/**
 * This hooks check if the current tab is a child tab of the class
 * If True: => Open the sidebar
 * On leaving : This hooks will try to close the sidebar
 */
const useClassSidebar = () => {
    const dispatch = useAppDispatch();
    const {classId} = useParams();

    const classIdInNumber: number = Number(classId);

    const {data: classInformation, isSuccess,isError,isFetching,isUninitialized} = useGetClassInformationQuery(Number(classId),{skip: isNaN(Number(classId))});

    const courseInformation = classInformation?.course;
    const forceCloseSidebar = () => {
        dispatch(sideBarActions.setClassIdInFocus(Number(undefined)));
    }

    useEffect(() => {
        // console.log("Tring to open the sidebar");
        if (classId && !isNaN(Number(classId))) {
            dispatch(sideBarActions.setClassIdInFocus(Number(classId)));
        } else {
            dispatch(sideBarActions.setClassIdInFocus(Number(undefined)));
        }
    },[])


    React.useEffect(
        () => {
            if(isSuccess && !courseInformation?.id){
                AntdNotifier.warn("Không tìm thấy thông tin khoá mà lớp này thuộc vào ");
            }
        },
        []
    );
    return [forceCloseSidebar, classIdInNumber, courseInformation] as const;
}

export default useClassSidebar;