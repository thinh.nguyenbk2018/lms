import { Field } from "formik";
import { ActivityType } from "@typescript/interfaces/courses/CourseTypes";
import { Checkbox, Form, FormItem, Input } from "formik-antd";
import TextEditorField from "../../../../../components/CustomFields/TextEditorField/TextEditorField";
import { getFieldNameWithNamespace } from "@utils/FormUtils/formUtils";
import TextBookSearchArrayFieldWithNote
	from "../../../../../components/CustomFields/TextBookSearchSelectField/TextBookSearchArrayFieldWithNote/TextBookSearchArrayFieldWithNote";
import TestSearchSelectField from "../../../../../components/CustomFields/TestSearchSelectField/TestSearchSelectField";
import './ActivityWithForm.less';
import { useParams } from "react-router-dom";
import AttachmentQuestion from "@components/Question/Attachment/AttachmentQuestion";
import QUESTION_LABEL_CONSTANT from "@components/Question/constant/QuestionLabelConstant";

type ActivityFormProps = {
	type: ActivityType;
	namespace?: string;
};

export const ACTIVITY_FIELD_NAMES = {
	QUIZ: {
		TITLE: "title",
		DESCRIPTION: "description",
		TAG: "tag",
		EXAM: "examId",
	},
	UNIT: {
		TITLE: "title",
		CONTENT: "content",
		TEXTBOOKS: "textbooks",
	},
	VOTING: {
		TITLE: "title",
		DESCRIPTION: "description",
		ALLOW_ADDING_CHOICES: "isAllowedToAddChoice",
		CHOICES: "choices",
	},
};



export const UnitItemForm = ({ type, namespace }: ActivityFormProps) => {
	const getFieldName = (name: string) => {
		return getFieldNameWithNamespace(namespace, name);
	};
	const { classId, courseId } = useParams();

	return (
		<Form layout="vertical">
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.UNIT.TITLE)}`}
				showValidateSuccess={true}
				label={"Tiêu đề"}
				required={true}
			>
				<Input
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.UNIT.TITLE)}`}
					fast={true}
				/>
			</FormItem>
			<Field
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.UNIT.CONTENT)}`}
				placeholder={"Nhập nội dung bài học"}
				component={TextEditorField}
				label={"Nội dung"}
				showValidateSuccess={true}
				required={true}
			/>
			<Field
				name={"attachment"}
				placeholder={"Type article content"}
				component={AttachmentQuestion}
				label={QUESTION_LABEL_CONSTANT.DESCRIPTION}
			/>
			<Field
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.UNIT.TEXTBOOKS)}`}
				component={TextBookSearchArrayFieldWithNote}
				label="Giáo trình"
				scopeIdToSearch={classId != undefined ? classId : courseId}
				scopeToSearch={classId != undefined ? "CLASS" : "COURSE"}
			/>
		</Form>
	);
};
export const QuizItemForm = ({
	type,
	namespace,
	courseIdToSearch,
	isUpdating,
	isFromCourse
}: ActivityFormProps & { courseIdToSearch: number, isUpdating: boolean, isFromCourse?: boolean }) => {
	const getFieldName = (name: string) => {
		return getFieldNameWithNamespace(namespace, name);
	};
	return (
		<Form layout="vertical" preserve={false}>
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.TITLE)}`}
				showValidateSuccess={true}
				required={true}
				label={"Tiêu đề"}
			>
				<Input
					disabled={isFromCourse}
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.TITLE)}`}
					fast={true}
				/>
			</FormItem>
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.EXAM)}`}
				showValidateSuccess={true}
				label={"Đề kiểm tra"}
			>
				<Field
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.EXAM)}`}
					component={TestSearchSelectField}
					courseIdToSearch={courseIdToSearch}
					isUpdating={isUpdating}
				/>
			</FormItem>
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.TAG)}`}
				showValidateSuccess={true}
				label={"Tên cột điểm"}
				required={true}
			>
				<Input
					disabled={isFromCourse}
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.TAG)}`}
					fast={true}
				/>
			</FormItem>
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.DESCRIPTION)}`}
				// showValidateSuccess={true}
				label={"Mô tả"}
			>
				<Input
					disabled={isFromCourse}
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.QUIZ.DESCRIPTION)}`}
					fast={true}
				/>
			</FormItem>
		</Form>
	);
};

export const VotingItemForm = ({ type, namespace }: ActivityFormProps) => {
	const getFieldName = (name: string) => {
		return getFieldNameWithNamespace(namespace, name);
	};
	return (
		<Form layout="vertical">
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.VOTING.TITLE)}`}
				showValidateSuccess={true}
				required={true}
				label={"Tiêu đề"}
			>
				<Input
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.VOTING.TITLE)}`}
					fast={true}
				/>
			</FormItem>
			<FormItem
				name={`${getFieldName(ACTIVITY_FIELD_NAMES.VOTING.DESCRIPTION)}`}
				showValidateSuccess={true}
				label={"Mô tả thêm"}
			>
				<Input
					name={`${getFieldName(ACTIVITY_FIELD_NAMES.VOTING.DESCRIPTION)}`}
					fast={true}
				/>
			</FormItem>
			<FormItem
				name={`${getFieldName(
					ACTIVITY_FIELD_NAMES.VOTING.ALLOW_ADDING_CHOICES
				)}`}
				showValidateSuccess={true}
				label={"Cho phép học viên thêm lựa chọn mới"}
				className={'allow-add-choice-wrapper'}
			>
				<Checkbox
					name={`${getFieldName(
						ACTIVITY_FIELD_NAMES.VOTING.ALLOW_ADDING_CHOICES
					)}`}
					fast={true}
				/>
			</FormItem>
		</Form>
	);
};
