import React from 'react';
import PropTypes from 'prop-types';
import EndAfter from './After';
import EndOnDate from './OnDate';

import translateLabel from '../../utils/translateLabel';
import {Col, Row, Select} from "antd";

const End = ({
  id,
  end: {
    mode,
    after,
    onDate,
    options,
  },
  handleChange,
  translations
}) => {
  const isOptionAvailable = option => !options.modes || options.modes.indexOf(option) !== -1;
  const isOptionSelected = option => mode === option;

  return (
    <div className="px-3">
      <Row gutter={[5,10]}>
        <Col span={24}>
          <label
            htmlFor={id}
            className="col-form-label"
          >
            <strong>
              {translateLabel(translations, 'end.label')}
            </strong>
          </label>
        </Col>
        <Col>
          <Select
            name="end.mode"
            id={id}
            className="form-control"
            value={mode}
            onChange={(value) => {
              const target = {name: "end.mode", value}
              handleChange({target});
            }}
          >
            {isOptionAvailable('Never') && <Select.Option value="Never">{translateLabel(translations, 'end.never')}</Select.Option>}
            {isOptionAvailable('After') && <Select.Option value="After">{translateLabel(translations, 'end.after')}</Select.Option>}
            {isOptionAvailable('On date') && <Select.Option value="On date">{translateLabel(translations, 'end.on_date')}</Select.Option>}
          </Select>
        </Col>

        {
          isOptionSelected('After') &&
          <EndAfter
            id={`${id}-after`}
            after={after}
            handleChange={handleChange}
            translations={translations}
          />
        }
        {
          isOptionSelected('On date') &&
          <EndOnDate
            id={`${id}-onDate`}
            onDate={onDate}
            handleChange={handleChange}
            translations={translations}
          />
        }
      </Row>
    </div>
  );
};

End.propTypes = {
  id: PropTypes.string.isRequired,
  end: PropTypes.shape({
    mode: PropTypes.string.isRequired,
    after: PropTypes.number.isRequired,
    onDate: PropTypes.object.isRequired,
    options: PropTypes.shape({
      modes: PropTypes.arrayOf(PropTypes.oneOf(['Never', 'After', 'On date'])),
      weekStartsOnSunday: PropTypes.bool,
    }).isRequired,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  translations: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default End;
