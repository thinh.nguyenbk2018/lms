import {Col, Modal, Row} from 'antd';
import {Field, Formik} from 'formik';
import {DatePicker, Form, FormItem, Input, Checkbox} from 'formik-antd';
import {useParams} from 'react-router-dom';
import * as Yup from 'yup';
import './ClassSessionFormModal.less';

import TeacherSearchSelectField from '../../../../../components/CustomFields/TeacherSelectField/TeacherSelectFields';
import UnitSearchSelectField from '../../../../../components/CustomFields/UnitSearchSelect/UnitSearchSelectField';
import {useAppDispatch, useAppSelector} from '../../../../../hooks/Redux/hooks';
import {
    ClassSession,
    useCreateNewSessionMutation,
    useUpdateClassSessionMutation,
} from '../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableAPI';
import {
    classTimeTableActions,
    classTimeTableSelectors,
} from '../../../../../redux/features/Class/ClassTimeTable/ClassTimeTableSlice';
import AntdNotifier from '../../../../../utils/AntdAnnouncer/AntdNotifier';
import {getFieldNameWithNamespace} from '../../../../../utils/FormUtils/formUtils';
import timeStampHelper, {
    getHourMinute,
    HOUR_MINUTE_PATTERN
} from "../../../../../utils/TimeStampHelper/TimeStampHelper";
import React from "react";

const FORM_TITLES = {
    STARTED_AT: "startedAt",
    FINISHED_AT: "finishedAt",
    UNIT: "unitId",
    TEACHER: "teacherId",
    ROOM: "room",
    NOTE: "note",
    NOTIFY_WITH_EMAIL: "notifyWithEmail"
};

const classSessionSchema = Yup.object({
    date: Yup.date().required("Ngày là thông tin bắt buộc"),
    startedAt: Yup.string().required("Giờ bắt đầu là thông tin bắt buộc").matches(HOUR_MINUTE_PATTERN, "Giờ phải theo định dạng HH:MM"),
    finishedAt: Yup.string().required("Giờ kết thúc là thông tin bắt buộc").matches(HOUR_MINUTE_PATTERN, "Giờ phải theo định dạng HH:MM"),
});

export type classSessionFormType = {
    teacherId: number | undefined;
    unitId: number | undefined;
    date: Date;
    startedAt: string;
    finishedAt: string;
    room: string;
    note: string;
    notifyWithEmail: boolean;
}

const classSessionInitialValues: classSessionFormType = {
    teacherId: undefined,
    unitId: undefined,
    date: new Date(),
    startedAt: "",
    finishedAt: "",
    room: "",
    note: "",
    notifyWithEmail: false
};

type ClassSessionFormProps = {
    namespace?: string;
};
export const ClassSessionFormModal = (props: ClassSessionFormProps) => {
    const {namespace} = props;

    const {classId} = useParams();
    const getFieldName = (name: string) => {
        return getFieldNameWithNamespace(namespace, name);
    };
    const isAddingClassSession = useAppSelector(
        classTimeTableSelectors.selectIsAddingClassSession
    );
    const classSessionInEdit = useAppSelector(
        classTimeTableSelectors.selectClassSessionInEdit
    );

    const dispatch = useAppDispatch();

    const [createNewSession, {isLoading: isCreateLoading}] =
        useCreateNewSessionMutation();
    const [updateSession, {isLoading: isUpdateLoading}] =
        useUpdateClassSessionMutation();

    const closeModal = () => {
        dispatch(classTimeTableActions.closeModal());
    };

    return (
        <>
            <Formik
                enableReinitialize={true}
                onSubmit={(values, {resetForm}) => {
                    const transformValues: ClassSession = {
                        ...values,
                        startedAt: timeStampHelper.createDateTime(values.date, values.startedAt),
                        finishedAt: timeStampHelper.createDateTime(values.date, values.finishedAt)
                    };
                    // console.log('data: ', transformValues);
                    if (isNaN(Number(classId))) {
                        AntdNotifier.error(
                            "Hành động không được phép. Liên hệ quản trị viên để biết thêm chi tiết"
                        );
                        return;
                    }
                    if (classSessionInEdit) {
                        if (!classSessionInEdit.id) {
                            AntdNotifier.error(
                                "Đã có lỗi xảy ra. Hiện tại bạn không thể update session này. Hãy thử reload lại trang"
                            );
                            return;
                        }
                        AntdNotifier.handlePromise(
                            updateSession({
                                classId: Number(classId),
                                classSession: transformValues,
                                sessionId: classSessionInEdit.id,
                            }).unwrap(),
                            "Cập nhật buổi học",
                            closeModal
                        );
                    } else if (isAddingClassSession) {
                        AntdNotifier.handlePromise(
                            createNewSession({
                                classId: Number(classId),
                                classSession: transformValues,
                            }).unwrap(),
                            "Thêm một buổi học",
                            () => {
                                closeModal();
                                resetForm({values: classSessionInitialValues});
                            }
                        );
                    }
                }}
                initialValues={classSessionInEdit
                    ? {
                        ...classSessionInEdit,
                        startedAt: getHourMinute(classSessionInEdit?.startedAt),
                        finishedAt: getHourMinute(classSessionInEdit?.finishedAt)
                    }
                    : classSessionInitialValues}
                validationSchema={classSessionSchema}
                validateOnChange={false}
            >
                {({values, handleSubmit, isValid, dirty, setFieldValue, resetForm}) => {
                    return (
                        <Modal
                            width={900}
                            bodyStyle={{overflowY: 'auto', maxHeight: 'calc(100vh - 250px)'}}
                            maskClosable={false}
                            title={classSessionInEdit ? "Cập nhật buổi học" : "Thêm buổi học"}
                            visible={!!classSessionInEdit || isAddingClassSession}
                            onOk={() => {
                                handleSubmit();
                            }}
                            onCancel={() => {
                                resetForm({values: classSessionInitialValues});
                                dispatch(classTimeTableActions.closeModal());
                            }}
                            okButtonProps={{
                                disabled: !dirty,
                                loading: isCreateLoading || isUpdateLoading,
                            }}
                            okText={classSessionInEdit ? "Cập nhật": "Thêm buổi học"}
                            cancelText={"Hủy"}
                        >
                            <>
                                <Form layout={"vertical"}>
                                    <Row>
                                        <Col span={11}>
                                            <FormItem
                                                name={'date'}
                                                required={true}
                                                label={"Ngày"}
                                            >
                                                <DatePicker style={{width: '100%'}} name={'date'}/>
                                            </FormItem>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={11}>
                                            <FormItem
                                                name={getFieldName(`${FORM_TITLES.STARTED_AT}`)}
                                                label={"Từ (HH:MM)"}
                                                required={true}
                                            >
                                                <Input name={getFieldName(
                                                    `${FORM_TITLES.STARTED_AT}`
                                                )}/>
                                            </FormItem>
                                        </Col>

                                        <Col span={11} offset={2}>
                                            <FormItem
                                                name={getFieldName(`${FORM_TITLES.FINISHED_AT}`)}
                                                required={true}
                                                label={"Đến (HH:MM)"}
                                            >
                                                <Input name={getFieldName(
                                                    `${FORM_TITLES.FINISHED_AT}`
                                                )}/>
                                            </FormItem>
                                        </Col>
                                    </Row>

                                    <div style={{
                                        border: "1px solid #1890ff",
                                        padding: "1rem 1rem 1rem 0.5rem",
                                        marginBottom: "1rem"
                                    }}>
                                        <FormItem
                                            name={getFieldName(`${FORM_TITLES.TEACHER}`)}
                                            label={"Giáo viên"}
                                            labelCol={{span: 4}}
                                            wrapperCol={{span: 24}}
                                            style={{marginBottom: "1rem"}}
                                        >
                                            <Field
                                                name={getFieldName(`${FORM_TITLES.TEACHER}`)}
                                                component={TeacherSearchSelectField}
                                            />
                                        </FormItem>

                                        <FormItem
                                            name={"applyTeacherToAll"}
                                            label={"Áp dụng cho tất cả buổi học hiện tại"}
                                            className={'checkbox-container-row-layout'}
                                        >
                                            <Checkbox name={"applyTeacherToAll"}/>
                                        </FormItem>
                                    </div>

                                    <div style={{
                                        border: "1px solid #1890ff",
                                        padding: "1rem 1rem 1rem 0.5rem",
                                        marginBottom: "1rem"
                                    }}>
                                        <FormItem
                                            name={getFieldName(`${FORM_TITLES.ROOM}`)}
                                            label={"Phòng học"}
                                            labelCol={{span: 4}}
                                            wrapperCol={{span: 24}}
                                        >
                                            <Input name={getFieldName(`${FORM_TITLES.ROOM}`)}/>
                                        </FormItem>
                                        <FormItem
                                            name={"applyRoomToAll"}
                                            label={"Áp dụng cho tất cả buổi học hiện tại"}
                                            className={'checkbox-container-row-layout'}
                                        >
                                            <Checkbox name={"applyRoomToAll"}/>
                                        </FormItem>
                                    </div>

                                    <FormItem
                                        name={getFieldName(`${FORM_TITLES.UNIT}`)}
                                        label={"Bài học"}
                                        labelCol={{span: 4}}
                                        wrapperCol={{span: 24}}
                                    >
                                        <Field
                                            name={getFieldName(`${FORM_TITLES.UNIT}`)}
                                            component={UnitSearchSelectField}
                                        />
                                    </FormItem>

                                    <FormItem
                                        name={getFieldName(`${FORM_TITLES.NOTE}`)}
                                        label={"Ghi chú"}
                                        labelCol={{span: 4}}
                                        wrapperCol={{span: 24}}
                                    >
                                        <Input name={getFieldName(`${FORM_TITLES.NOTE}`)}/>
                                    </FormItem>
                                    <FormItem
                                        name={getFieldName(`${FORM_TITLES.NOTIFY_WITH_EMAIL}`)}
                                        label={"Gửi thông báo và email đến thành viên lớp học"}
                                        labelCol={{span: 9}}
                                        wrapperCol={{span: 2}}
                                        style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}
                                        className={'notify-with-email'}
                                    >
                                        <Checkbox
                                            name={getFieldName(`${FORM_TITLES.NOTIFY_WITH_EMAIL}`)}
                                            // onChange={(event) => setFieldValue(getFieldName(`${FORM_TITLES.NOTIFY_WITH_EMAIL}`), event.target.checked)}
                                        />
                                    </FormItem>
                                </Form>
                            </>
                        </Modal>
                    );
                }}
            </Formik>
        </>
    );
};

export default ClassSessionFormModal;
