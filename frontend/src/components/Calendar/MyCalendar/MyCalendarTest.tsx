import React, {useState} from 'react';
import {Button, Card, Col, Row, Space, Typography} from "antd";
import {DateArray, DateMatrix, takeMonth, takeWeek} from "../Utils/CalendarUtils";


const colStyles = {
    flexBasis: "14%",
    width: "14%"
};

const weekGetter = takeWeek();
const monthGetter = takeMonth();
const MyCalendarTest = () => {
    const [month, setMonth] = useState<DateMatrix>([]);
    const [week, setWeek] = useState<DateArray>([]);
    const getLastMonthDate = () => {
        const lastMonthTs = new Date().setDate(0);
        const firstDate = new Date(lastMonthTs).setDate(1);
        console.log("LASTMONT_DATE: ",firstDate);
        return new Date(firstDate)
    }
    return (
        <div>
            <Card>
                <Typography.Title>
                    Thinh's Test calendar is here
                </Typography.Title>
                <Card title={"week test"}>
                    <Space>
                        {
                            week.map((item, index) => <Button ghost danger key={index}> {item.getDate()}</Button>)
                        }
                    </Space>
                </Card>
                <Card title={"month test"}>
                    <Row gutter={[5, 10]}>
                        {
                            month.flat(1).map((item, index) => <Col key={index} style={{ ...colStyles }}>
                                <Button ghost danger> {item.getDate()}</Button>
                            </Col>)
                        }
                    </Row>
                </Card>
                <Button onClick={() => setWeek(weekGetter())}>
                    Fetch new Week
                </Button>
                <Button onClick={() => setMonth(monthGetter())}>
                    Fetch new Month
                </Button>
                <Button onClick={() => {
                    const prevMonthGetter = takeMonth(getLastMonthDate());
                    setMonth(prevMonthGetter());
                }}>
                    Fetch prev Month
                </Button>
            </Card>
        </div>
    );
};

export default MyCalendarTest;