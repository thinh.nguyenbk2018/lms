import { CSSProperties } from "react";
import { DropTargetMonitor, useDrop } from "react-dnd";
import { TargetType } from "dnd-core";
import styles from "./DropZone.module.scss";

interface DropZoneProps {
    accept: TargetType;
    onDrop: (item: any, monitor: DropTargetMonitor<unknown, unknown>) => void;
    onHover?: (item: any, monitor: DropTargetMonitor<unknown, unknown>) => void;
    checkCanDrop: (
        item: any,
        monitor: DropTargetMonitor<unknown, unknown>
    ) => boolean;
    style?: CSSProperties;
    canDropStyle?: CSSProperties;
    cannotDropStyle?: CSSProperties;
    isResize?: boolean;
    direction?: "Vertical" | "Horizontal",
    children?: React.ReactNode;
}

const DropZone = ({
    accept,
    onDrop,
    onHover,
    checkCanDrop,
    style = {},
    canDropStyle,
    cannotDropStyle,
    direction = "Vertical",
    children
}: DropZoneProps) => {
    let [{ isOver, canDrop }, drop] = useDrop(() => ({
        accept,
        drop: (item, monitor) => onDrop(item, monitor),
        hover: (item, monitor) => {
            if (onHover) onHover(item, monitor);
        },
        canDrop: (item, monitor) => {
            return checkCanDrop(item, monitor);
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
            item: monitor.getItem(),
        }),
    }));

    return (
        <div ref={drop} className={direction === "Vertical" ? styles.wrapperVertical : styles.wrapperHorizontal} style={{ display: direction === "Vertical" ? "block" : "inline-block" }}>
            <div
                className={styles.smooth}
                style={
                    !isOver ? style : canDrop ? canDropStyle : cannotDropStyle // NOSONAR
                }
            >
                {children}
            </div>
        </div>
    );
};

export default DropZone;
