import { Button, Space, Spin } from "antd";
import React from "react";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import StudentForm, { StudentFormHandle } from "../../../components/Students/StudentForm";
import { ROUTING_CONSTANTS } from "../../../navigation/ROUTING_CONSTANTS";
import { useGetStudentDetailsQuery } from "../../../redux/features/Student/StudentAPI";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";

const EditStudentPage = () => {
  const { studentId } = useParams();
  const navigate = useNavigate();
  const studentFormRef = useRef<StudentFormHandle>(null);
  const { data: getStudentDetailsResponse, isLoading } = useGetStudentDetailsQuery(parseInt(studentId!));
  const onDone = () => {
    studentFormRef.current?.updateStudent()
      .then((result: any) => {
        if (!result) return;
        if ("code" in result && result.code === "SUCCESS") {
          navigate(ROUTING_CONSTANTS.STUDENTS_MANAGEMENT)
        }
      })
      .catch((error: any) => {
        if (error?.data?.message) {
          AntdNotifier.error(error.data.message);
          return;
        }
        AntdNotifier.error("Có lỗi xảy ra trong quá trình cập nhật dữ liệu, thử lại sau");
      })
  }

  const onCancel = () => {
    navigate(-1)
  }
  return <>
    <ContentHeader
      title="Chi tiết học viên"
      action={
        <Space>
          <Button
            danger
            type="primary"
            color="#73d13d"
            onClick={onCancel}
          >
            Hủy
          </Button>
          <Button
            type="primary"
            color="#73d13d"
            onClick={onDone}
          >
            Xong
          </Button>
        </Space>
      }
    />
    <Spin spinning={isLoading}>
      <StudentForm ref={studentFormRef} student={getStudentDetailsResponse?.student} />
    </Spin>
  </>
}

export default EditStudentPage;