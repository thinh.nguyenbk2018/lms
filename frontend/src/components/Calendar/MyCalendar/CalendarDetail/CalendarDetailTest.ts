// import {CalendarEvent, OptionalEvent} from "../../../../redux/features/Calendar/CalendarAPI";
// import {EventHashMap} from "../../../../redux/features/Calendar/CalendarSlice";

// export const calendarEventTestList : CalendarEvent[] = [
//     {
//         "kind": "calendar#event",
//         "etag": "\"3283227079338000\"",
//         "id": "32jj6n7a7vmut2t3o6f53ovksssd1",
//         "status": "confirmed",
//         "htmlLink": "https://www.google.com/calendar/event?eid=MzJqajZuN2E3dm11dDJ0M282ZjUzb3ZrZDFfMjAyMjAxMDNUMDYwMDAwWiBob2FuZ3RoaW5oa2czMzNAbQ",
//         "created": "2022-03-08T03:45:39.000Z",
//         "updated": "2022-03-08T03:45:39.669Z",
//         "summary": "Learning time - Algorithms, Part ZZ",
//         "description": "Course: Algorithms, Part I\nhttps://www.coursera.org/learn/algorithms-part1/home/welcome",
//         "creator": {
//             "email": "hoangthinhkg333@gmail.com",
//             "self": true
//         },
//         "organizer": {
//             "email": "hoangthinhkg333@gmail.com",
//             "self": true
//         },
//         "start": {
//             "dateTime": "2022-03-15T13:20:00+07:00",
//             "timeZone": "Asia/Ho_Chi_Minh"
//         },
//         "end": {
//             "dateTime": "2022-03-15T14:58:00+07:00",
//             "timeZone": "Asia/Ho_Chi_Minh"
//         },
//         "location": "Some where weird",
//     },
//     {
//         "kind": "calendar#event",
//         "etag": "\"3283227079338000\"",
//         "id": "32jj6n7a7vmut2t3o6f53ovkasdd1",
//         "status": "confirmed",
//         "htmlLink": "https://www.google.com/calendar/event?eid=MzJqajZuN2E3dm11dDJ0M282ZjUzb3ZrZDFfMjAyMjAxMDNUMDYwMDAwWiBob2FuZ3RoaW5oa2czMzNAbQ",
//         "created": "2022-01-17T03:45:39.000Z",
//         "updated": "2022-01-17T03:50:39.669Z",
//         "summary": "Learning time - Algorithms, Part YY",
//         "description": "Course: Algorithms, Part I\nhttps://www.coursera.org/learn/algorithms-part1/home/welcome",
//         "creator": {
//             "email": "hoangthinhkg333@gmail.com",
//             "self": true
//         },
//         "organizer": {
//             "email": "hoangthinhkg333@gmail.com",
//             "self": true
//         },
//         "start": {
//             "dateTime": "2022-01-16T13:45:00+07:00",
//             "timeZone": "Asia/Ho_Chi_Minh"
//         },
//         "end": {
//             "dateTime": "2022-01-16T13:55:00+07:00",
//             "timeZone": "Asia/Ho_Chi_Minh"
//         },
//         "location": "Some where weird",
//     },
//     {
//         "kind": "calendar#event",
//         "etag": "\"3283227079338000\"",
//         "id": "32jj6n7a7vmut2t3o6f53ovkd1",
//         "status": "confirmed",
//         "htmlLink": "https://www.google.com/calendar/event?eid=MzJqajZuN2E3dm11dDJ0M282ZjUzb3ZrZDFfMjAyMjAxMDNUMDYwMDAwWiBob2FuZ3RoaW5oa2czMzNAbQ",
//         "created": "2022-03-08T03:45:39.000Z",
//         "updated": "2022-03-08T03:45:39.669Z",
//         "summary": "Learning time - Algorithms, Part XX",
//         "description": "Course: Algorithms, Part I\nhttps://www.coursera.org/learn/algorithms-part1/home/welcome",
//         "creator": {
//             "email": "hoangthinhkg333@gmail.com",
//             "self": true
//         },
//         "organizer": {
//             "email": "hoangthinhkg333@gmail.com",
//             "self": true
//         },
//         "start": {
//             "dateTime": "2022-03-17T11:25:00+07:00",
//             "timeZone": "Asia/Ho_Chi_Minh"
//         },
//         "end": {
//             "dateTime": "2022-03-17T15:00:00+07:00",
//             "timeZone": "Asia/Ho_Chi_Minh"
//         },
//         "location": "Some where weird",
//     },
// ]


// export const sampleEventRequestDto = {"summary":"Thinh Event","description":"Test Event","location":"Ho Chi MInh City ","timeZone":"Asia, Ho Chi Minh","time":["2022-03-14T04:57:35.172Z","2022-03-15T04:57:35.172Z"],"calendarId":"nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com","start":{"dateTime":"2022-03-14T04:57:35.172Z","timeZone":"Asia, Ho Chi Minh"},"end":{"dateTime":"2022-03-15T04:57:35.172Z","timeZone":"Asia, Ho Chi Minh"},"recurrence":["RRULE:FREQ=WEEKLY;INTERVAL=1;BYDAY=TU,WE"]};


import {EventHashMap} from "../../../../redux/features/Calendar/CalendarSlice";

export const calendarHashMapTest: EventHashMap = {
    "2022-03-21T17:00": [
        {
            "created": {
                "value": 1647244010000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "jkljaslkjdklasjdkl",
            "end": {
                "dateTime": {
                    "value": 1647882367000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294494022262000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=bzgzMDFzaGFudTFsdG4ybXRzYTlzdGlkMDAgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "o8301shanu1ltn2mtsa9stid00@google.com",
            "id": "o8301shanu1ltn2mtsa9stid00",
            "kind": "calendar#event",
            "location": "asdfsadasdasdjalkd",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 1,
            "start": {
                "dateTime": {
                    "value": 1647882367000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "HAHAHA",
            "updated": {
                "value": 1647247011131,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        }
    ],
    "2022-03-31T10:00": [
        {
            "created": {
                "value": 1647239087000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "Toi test ti evetn thoi",
            "end": {
                "dateTime": {
                    "value": 1648720806000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294494693602000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=NzU4b2RtN2JwZ2FlOXU0NTA0a29xbG4zMGsgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "758odm7bpgae9u4504koqln30k@google.com",
            "id": "758odm7bpgae9u4504koqln30k",
            "kind": "calendar#event",
            "location": "Asia",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 2,
            "start": {
                "dateTime": {
                    "value": 1648720806000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "Thinh Event 2.1.11. ",
            "updated": {
                "value": 1647247346801,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "PERSONAL"
        }
    ],
    "2022-03-17T17:00": [
        {
            "created": {
                "value": 1647245067000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "test la test 11",
            "end": {
                "dateTime": {
                    "value": 1647536400000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294496340204000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=MmJsbDZqMWh1N3JlMmpqb2xtbWNmampvYnMgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "2bll6j1hu7re2jjolmmcfjjobs@google.com",
            "id": "2bll6j1hu7re2jjolmmcfjjobs",
            "kind": "calendar#event",
            "location": "test la test ",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 0,
            "start": {
                "dateTime": {
                    "value": 1647536400000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "Thinh TESTTest TEST",
            "updated": {
                "value": 1647248170102,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        }
    ],
    "2022-03-15T21:00": [
        {
            "created": {
                "value": 1647235263000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "Thinh Test Event",
            "end": {
                "dateTime": {
                    "value": 1647403200000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294496859946000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=MG1mZ2oyNDR0am4zZG0xYmpmNHJqZm4yYmcgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "0mfgj244tjn3dm1bjf4rjfn2bg@google.com",
            "id": "0mfgj244tjn3dm1bjf4rjfn2bg",
            "kind": "calendar#event",
            "location": "Localtion Test",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 4,
            "start": {
                "dateTime": {
                    "value": 1647378300000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "JKSJKLJKL Thinh Event",
            "updated": {
                "value": 1647248429973,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        }
    ],
    "2022-03-14T17:00": [
        {
            "created": {
                "value": 1647245467000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "ENGRISKENGRISK",
            "end": {
                "dateTime": {
                    "value": 1647363599000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294496994510000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=Z21wMG5wc2luZWRzcWtscmRvdHY3MnVoa28gbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "gmp0npsinedsqklrdotv72uhko@google.com",
            "id": "gmp0npsinedsqklrdotv72uhko",
            "kind": "calendar#event",
            "location": "ENGRISKENGRISK",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 3,
            "start": {
                "dateTime": {
                    "value": 1647277200000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "JJJJJ",
            "updated": {
                "value": 1647248497255,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        }
    ],
    "2022-03-15T18:00": [
        {
            "created": {
                "value": 1647253728000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "Test ",
            "end": {
                "dateTime": {
                    "value": 1647388800000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294507456960000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=ZnExYzhmbDJkbjJhaWxvZmR2MjBkM21vaWcgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "fq1c8fl2dn2ailofdv20d3moig@google.com",
            "id": "fq1c8fl2dn2ailofdv20d3moig",
            "kind": "calendar#event",
            "location": "Test",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 0,
            "start": {
                "dateTime": {
                    "value": 1647367500000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "Lớp học tiếng anh ",
            "updated": {
                "value": 1647253728480,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        }
    ],
    "2022-03-16T21:00": [
        {
            "created": {
                "value": 1647354683000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "TestTestTest",
            "end": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294709367820000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=cGc1dDRwbzFib3Q1b2h2NXFvbDE1N2hxcnMgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "pg5t4po1bot5ohv5qol157hqrs@google.com",
            "id": "pg5t4po1bot5ohv5qol157hqrs",
            "kind": "calendar#event",
            "location": "TestTestTestTest",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 0,
            "start": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "TestTestTest",
            "updated": {
                "value": 1647354683910,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        },
        {
            "created": {
                "value": 1647354683000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "TestTestTest",
            "end": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294709367820000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=cGc1dDRwbzFib3Q1b2h2NXFvbDE1N2hxcnMgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "pg5t4po1bot5ohv5qol157hqrs@google.com",
            "id": "pg5t4po1bot5ohv5qol157hqrs",
            "kind": "calendar#event",
            "location": "TestTestTestTest",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 0,
            "start": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "TestTestTest",
            "updated": {
                "value": 1647354683910,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        },
        {
            "created": {
                "value": 1647354683000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "TestTestTest",
            "end": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294709367820000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=cGc1dDRwbzFib3Q1b2h2NXFvbDE1N2hxcnMgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "pg5t4po1bot5ohv5qol157hqrs@google.com",
            "id": "pg5t4po1bot5ohv5qol157hqrs",
            "kind": "calendar#event",
            "location": "TestTestTestTest",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 0,
            "start": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "TestTestTest",
            "updated": {
                "value": 1647354683910,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        },
        {
            "created": {
                "value": 1647354683000,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "creator": {
                "email": "lms-spring-server@bklms-47f9b.iam.gserviceaccount.com"
            },
            "description": "TestTestTest",
            "end": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "etag": "\"3294709367820000\"",
            "htmlLink": "https://www.google.com/calendar/event?eid=cGc1dDRwbzFib3Q1b2h2NXFvbDE1N2hxcnMgbm9nNTU5bzRhdWhvdTdqZTFxczZqdWY4ajBAZw",
            "iCalUID": "pg5t4po1bot5ohv5qol157hqrs@google.com",
            "id": "pg5t4po1bot5ohv5qol157hqrs",
            "kind": "calendar#event",
            "location": "TestTestTestTest",
            "organizer": {
                "displayName": "Thinh Calendaer",
                "email": "nog559o4auhou7je1qs6juf8j0@GROUP.calendar.google.com",
                "self": true
            },
            "reminders": {
                "useDefault": true
            },
            "sequence": 0,
            "start": {
                "dateTime": {
                    "value": 1647464406000,
                    "dateOnly": false,
                    "timeZoneShift": 0
                },
                "timeZone": "Asia/Ho_Chi_Minh"
            },
            "status": "confirmed",
            "summary": "TestTestTest",
            "updated": {
                "value": 1647354683910,
                "dateOnly": false,
                "timeZoneShift": 0
            },
            "eventType": "CLASS_SESSION"
        },
    ]
}


export {};