import { useAppSelector } from "@hooks/Redux/hooks";
import { Button } from "antd";
import TodoComponent from "../TodoComponent";
import { todoActions, todoSelectors } from "./todoSlice";
import { useAppDispatch } from '../../../hooks/Redux/hooks';

function gibberish(length : number) : string {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

const TodoListWithRedux = () => {
    const todos = useAppSelector(todoSelectors.selectTodoList);
    const numOfTodos = useAppSelector(todoSelectors.selectNumberOfTodos);

    const dispatch = useAppDispatch(); 
    return (
        <div>
            <div>{`Number of todos are: ${numOfTodos}`}</div>
            {todos.map((item) => (
                <TodoComponent {...item} key={item.title} />
            ))}
            <Button onClick={() => {
               dispatch(todoActions.addNewTodo({
                    content: gibberish(300),
                    title: gibberish(12)
               })) 
            }}>add todo</Button>
        </div>
    );
};

export default TodoListWithRedux;
