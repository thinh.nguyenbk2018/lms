import {Divider, DrawerProps, Typography} from "antd";
import React from "react";
import {Formik} from "formik";
import {Checkbox, DatePicker, Form, FormItem, Input, Switch} from "formik-antd";
import {TestConfig,} from "@redux/features/Quiz/examAPI";
import AntdNotifier from "../../../../../utils/AntdAnnouncer/AntdNotifier";
import DrawerWithDoneCancel from "../../../../../components/DrawerHOC/DrawerWithDoneCancel";
import SettingButton from "../../../../../components/ActionButton/SettingButton";
import * as Yup from "yup";
import {newTestConfigSchema} from "../ClassQuizPage";
import {useParams} from "react-router-dom";
import {useConfigQuizMutation} from "@redux/features/Class/ClassAPI";
import {useAppSelector} from "@hooks/Redux/hooks";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import locale from 'antd/es/date-picker/locale/vi_VN';
import './QuizConfigurationDrawer.less';
import type {Moment} from 'moment';
import moment from "moment";

const layout = {
    labelCol: {span: 10},
    wrapperCol: {span: 16},
};

type ConfigurationDrawerFormProps = {
    config: Partial<TestConfig> | undefined;
    classId: number;
    courseId: number;
} & DrawerProps;
const QuizConfigurationDrawer = (props: ConfigurationDrawerFormProps) => {
    const {config, classId, courseId} = props;

    const currentUserAccountType = useAppSelector(authSelectors.selectCurrentUserAccountType);

    const {quizItemId} = useParams();

    // const [submiHandler] = useCreateUpdateTestConfigMutation();

    const [configQuiz] = useConfigQuizMutation();
    const [configDrawerVisible, setConfigDrawerVisible] = React.useState(false);

    const onChange = (value: Moment | null, field: string, setFieldValue: any) => {
        if (value == null) {
            return;
        }
        setFieldValue(field, moment.utc(value.add(7, "hour")).local());
    }

    const checkUTCTime = (value: any) => {
        if (value != undefined && value.includes("Z")) {
            return moment(value).add(7, "hour");
        }
        return value;
    }

    return (
        <Formik
            enableReinitialize
            initialValues={{
                haveTimeLimit: config?.haveTimeLimit || false,
                timeLimit: config?.timeLimit || 30,
                canNavigateBackward: config?.canNavigateBackward || true,
                allowDisconnect: config?.canNavigateBackward || true,
                state: config?.state || ("PRIVATE" as const),
                startedAt: config?.startedAt,
                validBefore: config?.validBefore,
                passScore: config?.passScore || 0,
                maxAttempt: config?.maxAttempt || 1,
                viewPreviousSessions: config?.viewPreviousSessions || false,
                viewPreviousSessionsTime: config?.viewPreviousSessionsTime || undefined,
                showResultImmediately: config?.showResultImmediately || false,
                showResultTime: config?.showResultTime || undefined,
                examId: 1,
                calculateFinalResultWay:
                    ("LAST_TIME" as const) || config?.calculateFinalResultWay,
            }}
            validationSchema={newTestConfigSchema.concat(
                Yup.object({
                    title: Yup.string().optional(),
                    description: Yup.string().optional(),
                })
            )}
            onSubmit={(values, hepler) => {
                const valuesToSubmit = {
                    ...values,
                    classId,
                    courseId,
                    startedAt: checkUTCTime(values["startedAt"]),
                    validBefore: checkUTCTime(values["validBefore"]),
                    viewPreviousSessionsTime: checkUTCTime(values["viewPreviousSessionsTime"])
                };
                // console.log('values to submit: ', valuesToSubmit);

                configQuiz({
                    classId,
                    quizId: Number(quizItemId),
                    config: valuesToSubmit
                }).unwrap()
                    .then(res => {
                        AntdNotifier.success("Thiết lập bài kiểm tra thành công.")
                    })
                    .catch(err => {
                        if (err.data) {
                            AntdNotifier.error(err.data.message ? err.data?.message : "Có lỗi xảy ra khi thiết lập quiz cho học viên.")
                        } else {
                            AntdNotifier.error("Có lỗi xảy ra khi thiết lập bài quiz cho học viên.")
                        }
                    })
            }}
        >
            {({
                  values,
                  errors,
                  touched,
                  handleSubmit,
                  dirty,
                  isValid,
                  resetForm,
                setFieldValue
              }) => {
                return (
                    <DrawerWithDoneCancel
                        maskClosable={false}
                        closable={false}
                        title="Thiết lập bài kiểm tra"
                        visible={configDrawerVisible}
                        trigger={
                            <SettingButton onClick={() => setConfigDrawerVisible(true)}/>
                        }
                        content={
                            <>
                                {/*<Typography.Paragraph className={'quiz-settings-note'}>*/}
                                {/*    *Lưu ý: Các trường <span className={'quiz-settings-note-fields'}>điểm đạt, số lần làm bài tối đa, thời gian bắt đầu, thời gian kết thúc</span> phải*/}
                                {/*    có giá trị hợp lệ để có thể hiển thị bài kiểm tra cho học viên.*/}
                                {/*</Typography.Paragraph>*/}
                                <Typography.Paragraph className={'quiz-settings-note'}>
                                    *Bài kiểm tra sẽ không giới hạn số lần làm bài nếu giá trị của trường <span
                                    className={'quiz-settings-note-fields'}>số lần làm bài tối đa</span> bằng 0.
                                </Typography.Paragraph>
                                <Divider/>
                                <Form {...layout}>
                                    <FormItem label={"Giới hạn thời gian"} name={"haveTimeLimit"}>
                                        <Switch defaultChecked name={"haveTimeLimit"}/>
                                    </FormItem>
                                    {values.haveTimeLimit && (
                                        <FormItem name={"timeLimit"} label={"Thời gian làm bài (phút)"}>
                                            <Input name={"timeLimit"} type={"number"}/>
                                        </FormItem>
                                    )}
                                    <FormItem label={"Điểm đạt"} name={`passScore`}>
                                        <Input name={`passScore`} type={"number"}/>
                                    </FormItem>
                                    <FormItem name={`maxAttempt`} label={"Số lần làm tối đa"}>
                                        <Input name={`maxAttempt`} type={"number"}/>
                                    </FormItem>
                                    <FormItem name={"startedAt"} label={"Thời gian bắt đầu"}>
                                        <DatePicker
                                            locale={{
                                                ...locale,
                                                lang: {
                                                    ...locale.lang,
                                                    now: "Bây giờ",
                                                    ok: "Chọn",
                                                }
                                            }}
                                            showTime
                                            name={"startedAt"}

                                            // onChange={(value: Moment | null) => {
                                            //     onChange(value, "startedAt", setFieldValue);
                                            // }}
                                        />
                                    </FormItem>
                                    <FormItem name={"validBefore"} label={"Thời gian kết thúc"}>
                                        <DatePicker
                                            locale={{
                                                ...locale,
                                                lang: {
                                                    ...locale.lang,
                                                    now: "Bây giờ",
                                                    ok: "Chọn",
                                                }
                                            }}
                                            showTime
                                            name={"validBefore"}
                                            // onChange={(value: Moment | null) => {
                                            //     onChange(value, "validBefore", setFieldValue);
                                            // }}
                                        />
                                    </FormItem>
                                    <FormItem name={"viewPreviousSessions"} label={"Cho phép học viên xem lại bài làm và kết quả"}>
                                        <Checkbox name={"viewPreviousSessions"}/>
                                    </FormItem>
                                    <FormItem required={values["viewPreviousSessions"] == true} name={"viewPreviousSessionsTime"} label={"Thời gian cho phép học viên xem lại bài làm và kết quả"}>
                                        <DatePicker
                                            disabled={!values['viewPreviousSessions']}
                                            locale={{
                                                ...locale,
                                                lang: {
                                                    ...locale.lang,
                                                    now: "Bây giờ",
                                                    ok: "Chọn",
                                                }
                                            }}
                                            showTime name={"viewPreviousSessionsTime"}
                                            // onChange={(value: Moment | null) => {
                                            //     onChange(value, "viewPreviousSessionsTime", setFieldValue);
                                            // }}
                                        />
                                    </FormItem>
                                    {/*<FormItem name={"showResultImmediately"} label={"Hiển thị kết quả ngay lập tức"}>*/}
                                    {/*    <Checkbox name={"showResultImmediately"}/>*/}
                                    {/*</FormItem>*/}
                                    {/*<FormItem name={"showResultTime"} label={"Thời gian hiển thị kết quả"}>*/}
                                    {/*    <DatePicker disabled={values['showResultImmediately']} locale={locale} showTime*/}
                                    {/*                name={"showResultTime"}/>*/}
                                    {/*</FormItem>*/}
                                    {/*<Typography.Text type="success">*/}
                                    {/*    {JSON.stringify(values)}*/}
                                    {/*</Typography.Text>*/}
                                    {/*<Typography.Text type="danger">*/}
                                    {/*    {JSON.stringify(errors)}*/}
                                    {/*</Typography.Text>*/}
                                </Form>
                            </>
                        }
                        onDone={() => {
                            handleSubmit();
                        }}
                        doneButtonProps={{disabled: !dirty}}
                    />
                );
            }}
        </Formik>
    );
};

export default QuizConfigurationDrawer;
