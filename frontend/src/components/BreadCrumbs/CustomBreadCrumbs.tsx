import React from 'react';
import {Link, useLocation, useNavigate, useParams} from "react-router-dom";
import { Breadcrumb, Typography } from "antd";
import { ROUTING_CONSTANTS } from "../../navigation/ROUTING_CONSTANTS";
import { useGetBreadcrumbTitleQuery } from '@redux/features/Breadcrumb/BreadcrumbAPI';

const BreadcrummbMap = {
    dashboard: "Thông tin chung",
    courses: "Khóa học",
    class: "Lớp học",
    classes: "Lớp học",
    voting: "Biểu quyết",
    roles: "Vai trò",
    quiz_content: "Đề kiểm tra",
    textbooks: "Giáo trình",
    content: "Nội dung giảng dạy",
    units: "Bài học",
    scheduler: "Thời khóa biểu",
    calendar: "Lịch",
    announcement: "Thông báo",
    student: "Học viên",
    students: "Học viên",
    teacher: "Giáo viên",
    attendances: "Điểm danh",
    quizzes: "Bài kiểm tra",
    discussion: "Diễn đàn",
    grades: "Điểm",
    result: "Kết quả",
    files: "Tệp tin",
    staffs: "Nhân viên",
    authorizaton: "Phân quyền",
    grant_permissions: "Gán quyền",
    edit: "Chỉnh sửa",
    new: "Thêm mới",
    information: "Thông tin chung",
    exams: "Đề kiểm tra",
    session: "Phiên làm bài",
    personalized: "Cá nhân hóa",
    settings: "Thiết lập",
    admin: "Quản lý",
    extend: "Gia hạn",
    quiz_result: "Kết quả bài kiểm tra"
}

const CustomBreadCrumbs = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const pathnames = location.pathname.split("/").filter(x => x);
    const {classId, courseId} = useParams();

    const CustomBreadcrumbItem = ({ id, routeTo, type, scope }: { id: number, routeTo: string, type: string, scope: string }) => {
        let {data: getBreadcrumbTitleRes} = useGetBreadcrumbTitleQuery({
            id, type, scope
        }, {skip: type === "session"})

        return <Breadcrumb.Item>
            <Typography.Link onClick={() => {
                console.log('route to: ', routeTo);
                navigate(routeTo);
            }}>
                {type === "session" ? "" : getBreadcrumbTitleRes?.title}
            </Typography.Link>
        </Breadcrumb.Item>;
    }

    return (
        <Breadcrumb separator={`/`}>
            {
                pathnames.length > 0 ? <Breadcrumb.Item>
                    <Link to={"/"}>Trang chủ</Link>
                </Breadcrumb.Item>
                    : <Breadcrumb.Item>
                        <Typography.Link onClick={() => navigate(ROUTING_CONSTANTS.ROOT)}>
                            Trang chủ
                        </Typography.Link>
                    </Breadcrumb.Item>
            }
            {
                pathnames.map((name, index) => {
                    const routeTo = `/${pathnames.slice(0, index + 1).join("/")}`
                    const isLast = index === pathnames.length - 1;
                    const showedName = BreadcrummbMap[name] || name;
                    // console.log('parse name: ', name, parseInt(name));
                    if (!parseInt(name) || name.includes("-")) {
                        return isLast ? <Breadcrumb.Item key={name}>
                            <Typography.Text>
                                {showedName}
                            </Typography.Text>
                        </Breadcrumb.Item> : <Breadcrumb.Item key={name}>
                            <Typography.Link key={name} onClick={() => navigate(routeTo)}>
                                {showedName}
                            </Typography.Link>
                        </Breadcrumb.Item>;
                    } else return <CustomBreadcrumbItem
                        id={parseInt(name)}
                        routeTo={isLast ? routeTo : routeTo+"/"+pathnames[index+1]}
                        type={["edit"].includes(pathnames[index-1]) ? pathnames[index-2] : pathnames[index-1]}
                        scope={classId != undefined ? "CLASS" : courseId != undefined ? "COURSE" : ""}
                    />;
                })
            }
        </Breadcrumb>
    );
};

export default CustomBreadCrumbs;

// * Resole breadcrumb name from routes config object. null if not found
// const findNameInRoutes: (name: string, routes: NavigationItem[]) => string | null = (name: string, routes: NavigationItem[]) => {
//
//     function checkPermisison(route: NavigationItem) {
//         return true;
//     }
//
//     const findNameInNav = (name: string, navItem: NavigationItem) => {
//         if (navItem.type === "tab") {
//             if (navItem.path === name) {
//             }
//         }
//         if (navItem.type !== "submenu") {
//             if(navItem.children)
//         }
//         return null;
//     }
//     const listNav = routes.filter(item => item.type === "tab");
//     let tabName : string | null = "";
//     for(let route of listNav) {
//         if (checkPermisison(route)) {
//             if (route.children && route.children?.length > 0) {
//                 const routeTabs = routes.filter(item => item.type === "tab") as TabNav[];
//                 // Find in side tab list before going down one depth
//                 const navTabFound = routeTabs.find(item => item.path === name);
//                 tabName = navTabFound ? navTabFound.tab_name : null;
//                 return tabName;
//             }
//             return findNameInNav(name, route as TabNav);
//         }
//         return null;
//     }
//     return name;
// }