import React from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import {Button, Card, Col, Form, Row, Typography} from "antd";
import {useGetClassInformationQuery} from "../../../../redux/features/Class/ClassAPI";
import antdNotifier from "../../../../utils/AntdAnnouncer/AntdNotifier";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import LoadingPage from "../../../UtilPages/LoadingPage";
import {useAppSelector} from "../../../../hooks/Redux/hooks";
import {authSelectors} from "../../../../redux/features/Auth/AuthSlice";
import pracetice_img from "@assets/images/pencil.png";
import tick_img from "@assets/images/work-in-progress.png";

import ProgressCard from "./Progress/ProgressCard";
import "./ClassDashBoard.less";
import ProgressCardChart from "./Progress/ProgressCardChart";
import UpcomingSession from "./UpcomingSession/UpcomingSession";
import UnCheckedNotification from "./UnCheckedNotification/UnCheckedNotification";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {replacePathParams} from "../../../../utils/PathPatcherHepler/PathPatcher";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import IMAGE_CONSTANTS from "../../../../constants/DefaultImage";
import StageTag from "../../../../components/StateTag/StageTag";
import {TagType} from "antd/es";
import {blue} from "@ant-design/colors";
import {classStatus} from "@redux/features/Class/ClassType";
import {useGetClassSessionsQuery} from "@redux/features/Class/ClassTimeTable/ClassTimeTableAPI";

/**
 * DashBoard Design (With some section replaced)
 * https://dribbble.com/shots/15509475-Lingu-platform-with-online-language-courses/attachments/7286635?mode=media
 * */
const ClassDashBoard = () => {
    // * Class specif routes
    const {classId} = useParams();
    const [closeSidebar] = useClassSidebar();
    const navigate = useNavigate();

    // * Redux query states
    const {
        data: classInfo,
        isSuccess,
        isError,
        isFetching,
    } = useGetClassInformationQuery(Number(classId));
    const userInfo = useAppSelector(authSelectors.selectCurrentUser);

    if (!classId || isNaN(Number(classId))) {
        antdNotifier.error("Không tìm thấy / Bạn không có quyền truy cập lớp này");
        return (
            <>
                <ApiErrorWithRetryButton onRetry={() => {
                }}/>
            </>
        );
    }

    const renderFunction = () => {
        if (isSuccess && classInfo) {
            return (
                <>
                    <ContentHeader title={classInfo?.name || "Dashboard lớp học"}/>
                    <div className="class-dash-board-container">
                        <div className="class-dash-board-greeting">
                            <p className="greeting-text">
                                {`Chào mừng bạn quay trở lại, ${
                                    userInfo?.firstName || "học viên thân yêu"
                                }`}
                            </p>
                            {userInfo?.accountType === 'STUDENT' && <p className="greeting-message">{`Hãy cố chăm chỉ nhé. Tiến độ của bạn đang rất tốt.`}</p>}
                        </div>
                        <div className="class-dash-board__class-information">
                            <Row>
                                <Col span={24}>
                                    <Card
                                        title={"Thông tin lớp học"}
                                        headStyle={{
                                            backgroundColor: blue[5],
                                            color: "white",
                                            textAlign: "center",
                                        }}
                                    >
                                        <Row>
                                            <Col
                                                sm={24}
                                                md={{span: 14, offset: 8}}
                                                xxl={{span: 14, offset: 8}}
                                            >
                                                <Form.Item label={"Khóa học"}>
                                                    <Typography.Link
                                                        onClick={() => {
                                                            if (!classInfo.course?.name) {
                                                                return;
                                                            }
                                                            navigate(
                                                                replacePathParams(
                                                                    ROUTING_CONSTANTS.COURSE_DETAIL,
                                                                    [":id", classInfo.course?.name]
                                                                )
                                                            );
                                                        }}
                                                    >
                                                        {classInfo.course?.name || "No course found"}
                                                    </Typography.Link>
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                sm={24}
                                                md={{span: 14, offset: 8}}
                                                xxl={{span: 14, offset: 8}}
                                            >
                                                <Form.Item label={"Trạng thái"}>
                                                    <StageTag type={classInfo.status}
                                                              title={classStatus[classInfo.status]}/>
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                sm={24}
                                                md={{span: 14, offset: 8}}
                                                xxl={{span: 14, offset: 8}}
                                            >
                                                <Form.Item label={"Thời gian bắt đầu:"}>
                                                    <>
                                                        {classInfo.startedAt || ""}
                                                    </>
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                        {classInfo.status == 'ENDED' ? <Row>
                                            <Col
                                                sm={24}
                                                md={{span: 14, offset: 8}}
                                                xxl={{span: 14, offset: 8}}
                                            >
                                                <Form.Item label={"Thời gian kết thúc: "}>
                                                    <>
                                                        {classInfo.endedAt || ""}
                                                    </>
                                                </Form.Item>
                                            </Col>
                                        </Row> : classInfo.endedAt ? <Row>
                                            <Col
                                                sm={24}
                                                md={{span: 14, offset: 8}}
                                                xxl={{span: 14, offset: 8}}
                                            >
                                                <Form.Item label={"Thời gian kết thúc dự kiến: "}>
                                                    <>
                                                        {classInfo.endedAt || ""}
                                                    </>
                                                </Form.Item>
                                            </Col>
                                        </Row> : <></>}
                                    </Card>
                                </Col>
                            </Row>
                        </div>
                        <Row className="class-background-container" justify="center">
                            <Col span={24}>
                                <img
                                    className="class-background-image"
                                    src={
                                        classInfo.avatar || IMAGE_CONSTANTS.DEFAULT_BACKGROUND_IMAGE
                                    }
                                    alt="Ảnh nền"
                                />
                            </Col>
                        </Row>
                        <div className="class-dash-board-upcoming-sessions">
                            <UpcomingSession/>
                        </div>
                    </div>
                </>
            );
        } else if (isFetching) {
            return <LoadingPage/>;
        }
        return <></>;
    };
    return renderFunction();
};

export default ClassDashBoard;
