import React, {useRef} from 'react'
import {DragAndDropAnswer} from './../../../redux/features/Quiz/CreateQuizSlice';
import {useDrag, useDrop} from 'react-dnd';
import {QuizDragTypes} from '../../../constants/DragTypes';
import type {Identifier} from 'dnd-core'

export type DragAndDropAnswerProps = {
    children: JSX.Element;
    index: number;
    move: (from: number, to: number) => void,
    answerKey: DragAndDropAnswer["key"]
}

export interface DragAnswer {
    index: number;
    answerKey: number | string;
    type: string;
}


// * A draggable answer that does not allow shifting position of modifying the answer content
export const DraggableAnswerInViewMode = (props: Pick<DragAndDropAnswerProps, "children" | "answerKey" | "index">) => {
    const ref = useRef<HTMLDivElement>(null);
    const [{isDragging}, drag] = useDrag({
        type: QuizDragTypes.FillInBlankDnD.DnDAnswer,
        item: () => {
            return {answerKey: props.answerKey, index: props.index}
            // return {answerKey: props.answerKey, index: props.index}
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        })
    })
    const opacity = isDragging ? 0.5 : 1;
    drag(ref);
    return (
        <span ref={ref} className={"draggable-answer"} style={{opacity}}>
            {
                props.children
            }
        </span>
    )
}


const DraggableAnswer = (props: DragAndDropAnswerProps) => {
    const ref = useRef<HTMLDivElement>(null);

    const [{handlerId}, drop] = useDrop<DragAnswer, void, { handlerId: Identifier | null }>({
        accept: QuizDragTypes.FillInBlankDnD.DnDAnswer,
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId(),
            }
        },
        hover(item: DragAnswer, monitor) {
            if (!ref.current) {
                return;
            }
            const dragIdx = item.index;
            const hoverIdx = props.index;
            if (dragIdx == hoverIdx) {
                return;
            }
            props.move(dragIdx, hoverIdx);

            item.index = hoverIdx;
        },
    })


    const [{isDragging}, drag] = useDrag({
        type: QuizDragTypes.FillInBlankDnD.DnDAnswer,
        item: () => {
            return {answerKey: props.answerKey, index: props.index}
            // return {answerKey: props.answerKey, index: props.index}
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        })
    })
    const opacity = isDragging ? 0.5 : 1;
    drag(drop(ref))
    return (
        <div ref={ref} className={"draggable-answer"} data-hanlder-id={handlerId} style={{opacity}}>
            {
                props.children
            }
        </div>
    )
}

export default DraggableAnswer


