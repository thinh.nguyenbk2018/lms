import ContentHeader from "../../../components/ContentHeader/ContentHeader";
import {Button, Input, Space} from "antd";
import IconSelector from "../../../components/Icons/IconSelector";
import {Link} from "react-router-dom";
import RolesManagement from "../../../components/Authorization/Role/RolesManagement";
import {useState} from "react";
import {ROUTING_CONSTANTS} from "../../../navigation/ROUTING_CONSTANTS";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";

const RolesManagementPage = () => {
    let [searchKeyword, setSearchKeyword] = useState("");

    return <>
        <ContentHeader
            title="Vai trò"
            action={
                <Space>
                    <Input
                        placeholder="Enter to search ..."
                        allowClear={true}
                        prefix={<IconSelector type="SearchOutlined" />}
                        onChange={({ target: { value } }) => setSearchKeyword(value)}
                    />
                    <Space onClick={() => console.log("hello")}>
                        <IconSelector type="CaretDownOutlined" />
                    </Space>
                    <PermissionRenderer permissions={[PermissionEnum.CREATE_ROLE]}>
                        <Link to={ROUTING_CONSTANTS.CREATE_ROLE}>
                            <Button type="primary">Thêm</Button>
                        </Link>
                    </PermissionRenderer>
                </Space>
            }
        />
        <RolesManagement searchKeyword={searchKeyword} />
    </>
}

export default RolesManagementPage;