import React from "react";
import {useNavigate, useParams} from "react-router-dom";
import {Card, Col, Row, Space, Tooltip, Typography,} from "antd";
import * as Yup from "yup";
import ContentHeader from "../../../../components/ContentHeader/ContentHeader";
import ApiErrorWithRetryButton from "../../../../components/Util/ApiErrorWithRetryButton";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import useClassSidebar from "../../../../hooks/Routing/useClassSidebar";
import {ROUTING_CONSTANTS} from "../../../../navigation/ROUTING_CONSTANTS";
import {ROUTING_PATH_PARAM_KEY} from "../../../../navigation/ROUTING_PARAM_KEY";
import {createTestContentSelectors} from "@redux/features/Quiz/CreateQuizSlice";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import LoadingPage from "../../../UtilPages/LoadingPage";
import {QuizItem} from "@typescript/interfaces/courses/CourseTypes";
import StageTag from "../../../../components/StateTag/StageTag";
import {useDeleteClassQuizMutation, useGetClassQuizItemsQuery} from "@redux/features/Class/ClassAPI";
import {QuizIcon} from "@components/TeachingContent/NewActivityMenu";
import {DeleteOutlined} from "@ant-design/icons";
import "./ClassQuizPage.less";
import {authSelectors} from "@redux/features/Auth/AuthSlice";
import CustomEmpty from "@components/Util/CustomEmpty";
import {testSvg} from "@components/Icons/SvgIcons";
import COLOR_SCHEME from "@constants/ThemeColor";
import {confirmationModal} from "@components/Modal/DeleteConfirmation";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import antdNotifier from "@utils/AntdAnnouncer/AntdNotifier";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPenToSquare} from "@fortawesome/free-solid-svg-icons";
import {classContentActions} from "@redux/features/Class/ClassContent/ClassContentSlice";
import {AddQuizItemModal} from "@pages/Tenant/Class/Content/ClassContentPage";
import useClassCourseInformation from "@hooks/Routing/useClassCourseInformation";

export const newTestConfigSchema = Yup.object({
	haveTimeLimit: Yup.boolean(),
	timeLimit: Yup.number().when("haveTimeLimit", {
		is: true,
		then: (timeLimit) =>
			timeLimit.required(
				"Phải mô tả thời gian làm bài tối đa khi lựa chọn có giới hạn"
			),
		otherwise: (timeLimit) => timeLimit.notRequired(),
	}),
	state: Yup.string()
		.required("No empty")
		.oneOf(["PUBLIC" as const, "PRIVATE" as const]),
	canNavigateBackward: Yup.boolean().required("No empty"),
	viewPreviousSessionsTime: Yup.date().nullable()
		.when(
		"viewPreviousSessions",
		(viewPreviousSessions, schema) => viewPreviousSessions == true ? schema.required("Thời gian cho phép xem lại bài làm là thông tin bắt buộc") : schema),
	allowDisconnect: Yup.boolean().required("No empty"),
	// startedAt: Yup.string().nullable().test("test date", "Thời gian bắt đầu phải ở tương lai", (value: any) => {
	// 	return value == undefined || new Date() <= new Date(value);
	// }),
	validBefore: Yup.date().nullable()
		.test("Thời gian kết thúc phải lớn hơn hiện tại", "Thời gian kết thúc phải lớn hơn hiện tại", (value: any) => {
			return value == undefined || new Date(value) > new Date()
		})
		.when("startedAt",
		(startedAt, schema) =>
			schema == undefined ||
			(startedAt &&
				schema.min(
					startedAt,
					"Ngày kết thúc phải lớn hơn ngày bắt đầu"
				))
	),
	maxAttempt: Yup.number()
		.required("Tổng số lần làm tối đa không được để trống")
		.positive("Phải cho phép ít nhất 1 lần làm bài"),
	examId: Yup.number().required("Phải chọn một đề kiểm tra "),
	passScore: Yup.number().nullable()
		.min(0, 'Điểm đạt phải có giá trị lớn hơn 0')
});

const QuizItemCard = (props: QuizItem & { accountType: string, courseId: number | undefined }) => {
	const { classId } = useParams();
	const { title, description, tag, examId, state, id, accountType, isFromCourse, courseId } = props;

	const userInfo = useAppSelector(authSelectors.selectCurrentUser);

	const { classInfo } = useClassCourseInformation(Number(classId));

	const navigate = useNavigate();

	const navigateToQuizDetail = () => {
		navigate(
			replacePathParams(
				ROUTING_CONSTANTS.CLASS_QUIZ_ITEM_DETAIL_PAGE,
				[":classId", Number(classId)],
				[":quizItemId", id]
			)
		);
	};

	const dispatch = useAppDispatch();

	const [deleteQuiz] = useDeleteClassQuizMutation();

	const deleteQuizItem = (quizId: number | undefined) => {
		if (classId == undefined || quizId == undefined) {
			return;
		}
		deleteQuiz({classId: Number(classId), quizId: quizId})
			.unwrap()
			.then(res => antdNotifier.success("Xóa bài kiểm tra thành công"))
			.catch(err => antdNotifier.error("Có lỗi trong quá trình xóa bài kiểm tra"));
	};
	return (
		<Card
			className="class-quiz-card-item"
			title={
				<Space align="center">
					<QuizIcon />
					<p style={{ margin: 0, padding: 0 }}>
						{title || "Không có thông tin"}
					</p>
				</Space>
			}
			extra={
				<Space>
					<div className="action-container">
						{classInfo?.status != 'ENDED' && <PermissionRenderer
							permissions={[PermissionEnum.UPDATE_QUIZ_CLASS]}>
							{<Tooltip
								title={"Chỉnh sửa"}>
								<FontAwesomeIcon
									className={"icon edit action-icon-activity"}
									icon={faPenToSquare}
									onClick={() => {
										dispatch(
											classContentActions.updatingActivity(
												{
													chapterToEdit: -1,
													activity: {...props, type: "quiz"},
												}
											)
										);
									}}
								/>
							</Tooltip>}
						</PermissionRenderer>}
						<Tooltip title={"Chi tiết"}>
							<Typography.Link
								className={'flex'}
								style={{ marginRight: '0.25rem', color: COLOR_SCHEME.primary }}
								onClick={() => {
									navigateToQuizDetail();
								}}
							>
								{testSvg}
							</Typography.Link>
						</Tooltip>
						{classInfo?.status != 'ENDED' && <PermissionRenderer permissions={[PermissionEnum.DELETE_QUIZ_CLASS]}>
							{isFromCourse == false && <DeleteOutlined
								className="delete"
								onClick={() => {
									confirmationModal('bài kiểm tra', title, () => {
										deleteQuizItem(id);
									});
								}}
							/>}
						</PermissionRenderer>}
					</div>
				</Space>
			}
		>
			<Row>
				<Col className={'class-quiz-page-label'} span={2}>Mô tả</Col>
				<Col span={22}>{description || "Không có mô tả cho bài kiểm tra này"}</Col>
			</Row>
			{userInfo?.accountType === 'STAFF' && <Row>
				<Col className={'class-quiz-page-label'} span={2}>Trạng thái</Col>
				<Col span={22}>{<StageTag type={state || "UNKNOWN"} title={state == 'PUBLIC' ? 'Hiện' : 'Ẩn'} />}</Col>
			</Row>}
		</Card>
	);
};

// * Render list of test with config that was made public and accessible by this user
const ClassQuizPage = () => {
	const {
		classId,
		[ROUTING_PATH_PARAM_KEY.TEST_WITH_CONFIG]: examWithConfigId,
	} = useParams();
	const searchCriteria = useAppSelector(
		createTestContentSelectors.selectSearchCriteria
	);
	// * The list of test that is published and accessible in this class

	const currentUserAccountType = useAppSelector(authSelectors.selectCurrentUserAccountType);

	const {
		data: quizList,
		isSuccess,
		isFetching,
		isError,
		error,
		refetch,
	} = useGetClassQuizItemsQuery({ classId: Number(classId) });
	const [closeSidebar, classIdInNumber, courseInformation ] = useClassSidebar();
	// console.log('course: ', courseInformation, classIdInNumber);

	// console.log('abc: ', courseInformation);

	return (
		<div>
			<ContentHeader title={`Bài kiểm tra`} action={<></>} />
			{
				quizList?.length === 0 && <CustomEmpty description="Không có bài kiểm tra nào." />
			}
			{isSuccess && (
				<>
					{quizList.map((item, index) => {
						return <QuizItemCard {...item} accountType={currentUserAccountType || ''} key={item.id} courseId={courseInformation?.id || 0} />;
					})}
				</>
			)}
			{isFetching && <LoadingPage />}
			{isError && (
				<Row align={"middle"} justify={"center"}>
					<ApiErrorWithRetryButton onRetry={() => { }} />;
				</Row>
			)}
			<AddQuizItemModal courseIdToSearch={courseInformation?.id || 0}/>
		</div>
	);
};

export default ClassQuizPage;
