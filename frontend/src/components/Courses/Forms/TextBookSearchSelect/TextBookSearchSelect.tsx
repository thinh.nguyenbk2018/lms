import React from "react";
import {Select} from "antd";
import useDebounced from "../../../../hooks/Debounced/useDebounced";
import {ID} from "@redux/interfaces/types";
import {useAppDispatch, useAppSelector} from "@hooks/Redux/hooks";
import {changeCriteria, textbookSelector} from "@redux/features/Textbook/TextbookSlice";
import {TextBook} from "@redux/features/Textbook/TextbookType";


type TextbookSeachSelectProps = {
    onSelect?: (value: ID) => void;
    onChange?: (value: ID) => void;
    onBlur?: Function;
    value?: number;
    width?: string;
    textbooks: TextBook[];
};
const TextbookSearchSelect = ({
                                  onSelect,
                                  onChange,
                                  onBlur,
                                  value,
                                  width,
                                  textbooks
                              }: TextbookSeachSelectProps) => {
    // const searchQuery = jk
    const textbookSearchCriteria = useAppSelector(
        textbookSelector.selectSearchCriteria
    );

    // const {
    //     data: textbooksCourse,
    //     isSuccess,
    //     isFetching,
    // } = useGetTextbooksInCourseQuery({
    //     keyword: textbookSearchCriteria.keyword,
    //     courseId: Number(scopeIdToSearch)
    // }, {skip: scopeToSearch != 'COURSE'});
    //
    // const {
    //     data: textbooksClass,
    //     isSuccess: isGetTextbooksInClassSuccess,
    //     isFetching: isGetTextbooksInClassFetching,
    // } = useGetTextbooksInClassQuery({
    //     keyword: textbookSearchCriteria.keyword,
    //     classId: Number(scopeIdToSearch)
    // }, {skip: scopeToSearch != 'CLASS'});

    const dispatch = useAppDispatch();

    const debouncedCriteriaChange = useDebounced((criteria) =>
        dispatch(changeCriteria(criteria))
    );

    // @ts-ignore
    return (
        <Select
            value={value}
            showSearch
            style={{width: width ? width : 200}}
            placeholder=""
            optionFilterProp="children"
            filterOption={(input, option) => {
                const optionContent = (option?.children || "") as string;
                return optionContent.toLowerCase?.().indexOf(input.toLowerCase()) >= 0;
            }}
            filterSort={(optionA, optionB) => {
                const optionAcontent = (optionA?.children || "") as string;
                const optionBcontent = (optionB?.children || "") as string;
                return optionAcontent
                    .toLowerCase()
                    .localeCompare(optionBcontent.toLowerCase());
            }}
            onChange={(value, option) => {
                // console.log("textbook SEARCH / SELECT", value, option);
                onSelect?.(value);
                onChange?.(value);
                // onBlur?.();
            }}
            onSearch={(value) => {
                const searchCriteria = {...textbookSearchCriteria, keyword: value};
                debouncedCriteriaChange(searchCriteria);
            }}
            onBlur={() => {
                onBlur?.();
            }}
        >
            {/*{(scopeToSearch == 'CLASS' ? textbooksClass : textbooksCourse)?.map((item, index) => {*/}
            {/*    return (*/}
            {/*        <Select.Option value={item.id} key={item.id}>*/}
            {/*            {item.name}*/}
            {/*        </Select.Option>*/}
            {/*    );*/}
            {/*})}*/}
            {textbooks.map((item, index) => {
                return (
                    <Select.Option value={item.id} key={item.id}>
                        {item.name}
                    </Select.Option>
                );
            })}
        </Select>
    );
};

export default TextbookSearchSelect;
