import {StudentSessionResult} from "../../../../../../redux/features/Quiz/examAPI";
import {programSelector} from "../../../../../../redux/features/Program/ProgramSlice";
import StageTag from "../../../../../../components/StateTag/StageTag";
import "./SessionReviewCard.less";
import {parseDate} from "../../../../../../components/Calendar/Utils/CalendarUtils";
import classnames from "classnames";
import DetailButton from "../../../../../../components/ActionButton/DetailButton";
import {Avatar, Typography} from "antd";
import {testSvg} from "../../../../../../components/Icons/SvgIcons";
import {useNavigate, useParams} from "react-router-dom";
import {replacePathParams} from "../../../../../../utils/PathPatcherHepler/PathPatcher";
import {ROUTING_CONSTANTS} from "../../../../../../navigation/ROUTING_CONSTANTS";
import {ROUTING_PATH_PARAM_KEY} from "../../../../../../navigation/ROUTING_PARAM_KEY";
import AntdNotifier from "../../../../../../utils/AntdAnnouncer/AntdNotifier";

const HIGH_SCORE_THRESH_HOLD = 0.9;
const MEDIUM_SCORE_THRESH_HOLD = 0.5;

const SessionReviewCard = (props: StudentSessionResult) => {
    const {
        sessionId,
        user,
        totalScore,
        score,
        attempt,
        startedAt,
        submittedAt,
        gradedState,
        finalVerdict,
        questions,
    } = props;
    const {username, firstName, lastName, email, avatar} = user;
    const navigate = useNavigate();
    // TODO: Either the student who is the subject of this submission or the teacher with permission can view the session result
    const hasViewingPermission = true;
    const isStudentOfThisClass = true;
    const isTeacherOfThisClass = true;

    const {quizItemId, classId} = useParams();

    return (
        <>
            <div className="session-review-card-container">
                <div className={"session-and-user-meta-box"}>
                    <Avatar src={avatar} size={"large"}></Avatar>
                    <span className={"user-name"}>{`${lastName} ${firstName}`}</span>
                    <div className="session-meta">
                        <span className="attempt-number">{`Attempt: ${attempt}`}</span>
                        <span className="submitted-time">
							{/*{`Nộp bài: ${parseDate(submittedAt).toLocaleDateString()}`}*/}
						</span>
                    </div>
                </div>
                <div className={"session-result-info"}>
                    <StageTag type={gradedState}/>
                    <StageTag type={finalVerdict}/>
                    <span
                        className={classnames({
                            "student-grade-info": true,
                            "low-score": score / totalScore < MEDIUM_SCORE_THRESH_HOLD,
                            "medium-score": score / totalScore >= MEDIUM_SCORE_THRESH_HOLD,
                            "high-score": score / totalScore > HIGH_SCORE_THRESH_HOLD,
                        })}
                    >{`${score} / ${totalScore}`}</span>
                    {/* <Typography.Link onClick={() => {}}>{testSvg}</Typography.Link> */}
                    {hasViewingPermission && <DetailButton style={{margin: "0 1em"}} onClick={() => {
                        if (!quizItemId || !sessionId || !classId) {
                            AntdNotifier.error("Bạn không có quyền để xem bài làm này. ");
                            return;
                        }
                        if (isTeacherOfThisClass) {
                            const pathToNav = replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_STUDENT_SESSION, [":classId", classId], [":quizItemId", Number(quizItemId)], [ROUTING_PATH_PARAM_KEY.TEST_SESSION_ID, sessionId]);
                            console.log("Navigating to: ", pathToNav);
                            navigate(pathToNav);
                        } else if (isStudentOfThisClass) {
                            // TODO: Change the ROUTE to student view instead of teacher view
                            navigate(replacePathParams(ROUTING_CONSTANTS.CLASS_QUIZ_RESULT_STUDENT_SESSION, [":classId", classId], [":quizItemId", Number(quizItemId)], [ROUTING_PATH_PARAM_KEY.TEST_SESSION_ID, sessionId]));
                        }
                    }}/>}
                </div>
            </div>
        </>
    );
};

export default SessionReviewCard;
