import { isEmpty } from "lodash";
import AntdNotifier from "../../../utils/AntdAnnouncer/AntdNotifier";

export const fileExtensionRegex = /(?:\.([^.]+))?$/;
/** Return file extension based on name
 *
 * @param file
 * @return file extesion from its name (Null if can't not find its extension)
 */
export const getFileExtension = (file: File) => {
	const exArr = fileExtensionRegex.exec(file.name);
	if (!exArr || exArr.length < 2) {
		return null;
	}
	return exArr[1];
};

export const getFileExtensionFromFileName = (filename: string) => {
	const exArr = fileExtensionRegex.exec(filename);
	if (!exArr || exArr.length < 2) {
		return null;
	}
	return exArr[1];
};
/** image/png or /video/mp3 ,... */
export const getFileType = (file: File) => {
	return file.type;
};

export const isImageFile = (file: File | undefined) => {
	if (!file) return false;
	return file.type.includes("image");
};
export const isVideoFile = (file: File | undefined) => {
	if (!file) return false;
	return file.type.includes("video");
};
export const isAudioFile = (file: File | undefined) => {
	if (!file) return false;
	return file.type.includes("audio");
};
export const isPlayableMedia = (file: File | undefined) => {
	return isAudioFile(file) || isVideoFile(file);
};
export const GENERAL_FILE_TYPES = {
	VIDEO: "video",
	IMAGE: "image",
	AUDIO: "audio",
	/** This special value also act as extension for unknown file type */
	UNKNOWN: "unk",
};

export function isFile(input: any): input is File {
	if ("File" in window && input instanceof File) return true; else return false;
}

export function isBlob(input: any): input is Blob {
	if ("Blob" in window && input instanceof Blob) return true;
	else return false;
}
export const getGeneralFileType = (file: File) => {
	if (isImageFile(file)) {
		return GENERAL_FILE_TYPES.IMAGE;
	} else if (isVideoFile(file)) {
		return GENERAL_FILE_TYPES.VIDEO;
	} else if (isAudioFile(file)) {
		return GENERAL_FILE_TYPES.AUDIO;
	} else {
		return GENERAL_FILE_TYPES.UNKNOWN;
	}
};


// * Input Sample: https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/audios%2Fthinh%40gmail.com%2FLight%20Switch%20-%20Charlie%20Puth.mp3?alt=media&token=8d6c9178-062f-4b01-8ced-1a5ae3be90c4";
// * Outpue: Light Switch - Charlie Puth.mp3
const fileNameRegex = /(.*?)(?=\?)/;
export const getFileNameFromFirebaseUrl = (firebaseUrl: string) => {
	const decodedUrl = decodeURIComponent(firebaseUrl);
	const lastPathItem = decodedUrl.match(/([^\/]*)\/*$/)?.[1];
	const fileNamesFound = lastPathItem?.match(fileNameRegex);

	// console.log(decodedUrl,fileNamesFound![0].toString());
	if (!fileNamesFound) {
		return "";
	}
	console.log("FILE NAME IS: ", fileNamesFound![0].toString());
	return fileNamesFound![0].toString();
};
