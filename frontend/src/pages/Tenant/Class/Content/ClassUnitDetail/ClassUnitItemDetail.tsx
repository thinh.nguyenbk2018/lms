import {Row, Space, Tooltip} from "antd";
import {useNavigate, useParams} from "react-router-dom";
import ContentHeader from "../../../../../components/ContentHeader/ContentHeader";
import TextEditor from "../../../../../components/TextEditor/TextEditor";
import {useAppDispatch} from "@hooks/Redux/hooks";
import useClassSidebar from "../../../../../hooks/Routing/useClassSidebar";
import {ROUTING_CONSTANTS} from "../../../../../navigation/ROUTING_CONSTANTS";
import {useGetClassInformationQuery, useGetClassUnitDetailQuery} from "@redux/features/Class/ClassAPI";
import SectionHeader from "../../../../../components/SectionHeader/SectionHeader";
import {AddUnitItemModal} from "../ClassContentPage";
import {replacePathParams} from "@utils/PathPatcherHepler/PathPatcher";
import BackButton from "@components/ActionButton/BackButton";
import React from "react";
import {Attachment} from "@components/Question/Attachment/AttachmentQuestion";
import AttachmentRender from "@components/Question/Attachment/AttachmentRender";
import PermissionRenderer from "@components/Util/PermissionRender/PermissionRender";
import {PermissionEnum} from "@typescript/interfaces/generated/PermissionMap";
import {classContentActions} from "@redux/features/Class/ClassContent/ClassContentSlice";
import EditButton from "@components/ActionButton/EditButton";
import LoadingPage from "@pages/UtilPages/LoadingPage";

const ClassUnitItemDetailPage = () => {
    // * Class specif routes
    const { unitItemId, chapterId, classId } = useParams();
    const [closeSidebar] = useClassSidebar();

    // * Redux query states
    const {
        data: classInfo,
        isSuccess: isSuccessClassInfo,
        isError: isErrorClassInfo,
        isFetching: isFetchingClassInfo,
    } = useGetClassInformationQuery(Number(classId));

    const {
        data: unitData,
        isSuccess,
        isError,
        isFetching,
    } = useGetClassUnitDetailQuery(
        {
            classId: Number(classId),
            unitId: Number(unitItemId),
        },
        {
            skip:
                isNaN(Number(unitItemId)) ||
                isNaN(Number(classId)),
        }
    );

    //  * Redux store
    const dispatch = useAppDispatch();

    // * Router
    const navigate = useNavigate();

    if (isSuccessClassInfo && isSuccess) {
        return (
            <>
                <ContentHeader
                    title={`${unitData.title}`}
                    action={
                        <Space>
                            <BackButton
                                type="primary"
                                danger
                                onClick={() => {
                                    navigate(
                                        replacePathParams(ROUTING_CONSTANTS.CLASS_CONTENT, [":classId", classId || ""]));
                                }}
                                className={"back-btn"}
                            />
                            {<PermissionRenderer permissions={[PermissionEnum.UPDATE_UNIT_CLASS]}>
                                {!unitData.isFromCourse && <EditButton
                                    onClick={() => {
                                        dispatch(
                                            classContentActions.updatingActivity({
                                                chapterToEdit: Number(chapterId),
                                                activity: unitData,
                                            })
                                        );
                                    }}
                                />}
                            </PermissionRenderer>}
                        </Space>
                    }
                />
                <SectionHeader title={"Thông tin bài học"} />
                <Row justify="center">
                    <div className="unit-item-info-card">
                        <div className="title">
                            <div className="field-value">{unitData.title}</div>
                        </div>
                        <div className="field-container">
                            <div className="field-name">Giáo trình</div>
                            {unitData.textbooks?.map((item, index) => {
                                return (
                                    <>
                                        <div className={"textbook-item-view-container"}>
                                            <Tooltip title={`${item.note}`}>
                                                <div
                                                    className="textbook-name"
                                                    onClick={() => {
                                                        navigate(ROUTING_CONSTANTS.TEXTBOOK_MANAGEMENT);
                                                    }}
                                                >
                                                    {item.name}
                                                </div>
                                            </Tooltip>
                                        </div>
                                    </>
                                );
                            })}
                        </div>
                    </div>
                </Row>
                <SectionHeader title={"Nội dung bài học"} />
                <div className="unit-content-container">
                    <TextEditor
                        value={unitData.content}
                        useBubbleTheme={true}
                        readOnly={true}
                        onChange={() => {
                        }}
                        additionalStyles={{
                            overflowY: "hidden",
                        }}
                    />
                </div>
                {
                    JSON.parse(unitData.attachment || "[]").map((a: Attachment, index: number) => <Row justify="center" key={index}>
                        <AttachmentRender attachment={a} />
                    </Row>)
                }
                <AddUnitItemModal />
            </>
        );
    } else if (isFetchingClassInfo || isFetching) {
        return <LoadingPage />;
    } else {
        return <></>;
    }
};

export default ClassUnitItemDetailPage;
