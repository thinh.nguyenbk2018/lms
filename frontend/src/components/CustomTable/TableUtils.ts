import {PaginatedResponse} from "../../redux/interfaces/types";

export const extractPaginationInformation = (response: PaginatedResponse<any> | undefined) => {
    if (!response){
        return {
            page: 1,
            perPage: 0,
            total: 0,
            totalPages: 0
        }
    }
    return {
        page: response.page,
        perPage: response.perPage,
        total: response.total,
        totalPages: response.totalPages
    }
};