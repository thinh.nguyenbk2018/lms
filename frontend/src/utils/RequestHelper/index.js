import axios from 'axios';
import queryString from 'query-string';
import {v4 as uuidv4} from 'uuid';
import {API_CONSTANT} from "../../constants/ApiConfigs";
import AntdNotifier from "../AntdAnnouncer/AntdNotifier";
import {tenantUtils} from "../TenantHelper/TenantUtils";

export const DEFAULT_ERROR_MESSAGE = "Default error message";
class RequestHelper {
    constructor() {
        this.baseUrl = API_CONSTANT.BASE_URL;
        this.defaultConfig = {};
        this.defaultHeaders = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods':'GET, PUT, POST, DELETE',
            responseType: 'application/json'
        };
    }

    // * Prepare required headers: Dealing with CORS, token attach ,...
    prepareHeaders(headers) {
        // const accessToken = Cookies.get('accessToken');\
        const accessToken = localStorage.getItem("token");
        const tenantID = tenantUtils.getTenantIdFromHostname();
        if (accessToken) {
            this.defaultHeaders.Authorization = `Bearer ${accessToken}`;
        }
        console.log(`X-TENANT-ID`,tenantID);
        if (tenantID){
            this.defaultHeaders[`X-TENANT-ID`] = tenantID;

        }
        this.defaultHeaders = {
            ...this.defaultHeaders,
            'Content-Type': 'application/json;charset=UTF-8',
            'X-Request-Id': uuidv4(),
            ...headers
        };
    }
    // * Convert Json query object into query param on URL Path
    prepareParams(payload) {
        if (!payload) return '';
        return `?${queryString.stringify(payload)}`;
    }

    // TODO: Handle response based on server design and protocols
    handleResponse(response, endPoint) {
        // * This handling should includes checking for error (Reaches the backend --> Code is Ok but logic error) and handling codes
        // * Now it only log the things out ==> Should be changed based on backend implementation and discussion
        console.log(`Response from endpoint ${endPoint}:`, response.data);
        return response.data;
    }
    handleError(error) {
        // * Lỗi không có response.data là lỗi từ front end / Hoặc là lỗi network
        if (!error.response) {
            AntdNotifier.error("Network error: Request could not reach backend");
            return;
        }

        // * Lỗi lấy được response.data là lỗi trả về từ backend
        const message = error?.response?.data?.message ?? DEFAULT_ERROR_MESSAGE;

        // * Lỗi Back end trả về không có trường message --> Log it for debugging
        if(message === DEFAULT_ERROR_MESSAGE){
        }
        AntdNotifier.apiError(message);
    }
    async getAsync(endPoint, payload, headers = {}) {
        this.prepareHeaders(headers);
        return axios
            .get(`${this.baseUrl}${endPoint}${this.prepareParams(payload)}`, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then((response) => this.handleResponse(response, endPoint, false))
            .catch((error) => this.handleError(error));
    }

    async postAsync(endPoint, payload, headers = {}) {
        this.prepareHeaders(headers);
        return axios
            .post(`${this.baseUrl}${endPoint}`, payload, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then((response) => this.handleResponse(response, endPoint))
            // .catch((error) => {
            //     this.handleError(error)
            // });
    }

    async postAsyncDelayed(endPoint, payload, headers = {}) {
        console.log("POSTED DELAY TO:", `${this.baseUrl}${endPoint}`, payload);
        this.prepareHeaders(headers);
        return axios
            .post(`${this.baseUrl}${endPoint}`, payload, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then(value => new Promise(resolve => {
                setTimeout(() => {
                    resolve(value);
                }, 50000);
            }))
            .then((response) => {
                console.log("RESPONSE DELAYED NOW RESOLEVE: ", response);
                this.handleResponse(response, endPoint)
            })
            .catch((error) => this.handleError(error.response));
    }

    async putAsync(endPoint, payload, headers = {}) {
        this.prepareHeaders(headers);
        return axios
            .put(`${this.baseUrl}${endPoint}`, payload, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then((response) => this.handleResponse(response, endPoint))
            .catch((error) => this.handleError(error.response));
    }

    async putAsyncWithId(endPoint,payload,headers = {}){
        this.prepareHeaders(headers);
        return axios
            .put(`${this.baseUrl}${endPoint}/${payload.id}`, payload, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then((response) => this.handleResponse(response, endPoint))
            .catch((error) => this.handleError(error.response));
    }

    // async deleteAsync(endPoint, payload, headers = {}) {
    //     this.prepareHeaders(headers);
    //     return axios
    //         .delete(`${this.baseUrl}${endPoint}${this.prepareParams(payload)}`, {
    //             ...this.defaultConfig,
    //             headers: this.defaultHeaders
    //         })
    //         .then((response) => this.handleResponse(response, endPoint))
    //         .catch((error) => this.handleError(error.response));
    // }
    async deleteAsync(endPoint, payload, headers = {}) {
        this.prepareHeaders(headers);
        return axios
            .delete(`${this.baseUrl}${endPoint}${this.prepareParams(payload)}`, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then((response) => this.handleResponse(response, endPoint))
            .catch((error) => this.handleError(error.response));
    }
    async deleteAsyncWithID(endPoint, {id}, headers = {}) {
        this.prepareHeaders(headers);
        return axios
            .delete(`${this.baseUrl}${endPoint}/${id}`, {
                ...this.defaultConfig,
                headers: this.defaultHeaders
            })
            .then((response) => this.handleResponse(response, endPoint))
            .catch((error) => this.handleError(error.response));
    }
}

const request = new RequestHelper();

export default request;
