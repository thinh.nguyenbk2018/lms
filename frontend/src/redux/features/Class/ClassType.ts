import { CourseItem } from "../Course/CourseAPI";
import { ID, PaginatedResponse } from "../../interfaces/types";
import { SearchCriteria } from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import { Chapter } from "../../../typescript/interfaces/courses/CourseTypes";
import { TagType } from "../../../components/StateTag/StageTag";

export const getKeyClassType = (v: string) => {
	const indexOfS = Object.values(ClassType).indexOf(v as unknown as ClassType);
	return Object.keys(ClassType)[indexOfS];
};

export const getKeyClassStatus = (v: string) => {
	const indexOfS = Object.values(ClassStatus).indexOf(
		v as unknown as ClassStatus
	);
	return Object.keys(ClassStatus)[indexOfS];
};

export enum ClassType {
	ONLINE = "ONLINE",
	OFFLINE = "OFFLINE",
	HYBRID = "HYBRID",
}

export enum ClassStatus {
	CREATED = "CREATED",
	ONGOING = "ONGOING",
	ENDED = "ENDED",
}

export const classStatus = {
	CREATED : "Mới mở",
	ONGOING: "Đang diễn ra",
	ENDED: "Đã kết thúc"
}

export const classType = {
	ONLINE : "Online",
	OFFLINE: "Offline",
	HYBRID: "Kết hợp"
}

export interface Class {
	id?: ID;
	name: string;
	code: string;
	avatar?: string;
	startedAt?: Date;
	endedAt?: Date;
	type: string;
	status: TagType;
	courseId?: number;
	chapters?: Chapter[];
	course?: CourseItem;
	daysOfWeek?: string;
}

export interface ClassMemberSearchCriteria extends SearchCriteria {
	classId: number;
}

export const initialClassDto: Class = {
	name: "",
	code: "",
	avatar: "",
	type: "OFFLINE" as const,
	status: "CREATED" as const,
	course: undefined,
	startedAt: undefined,
	endedAt: undefined,
	courseId: undefined,
};

export interface ClassMemberDto {
	firstName: string;
	lastName: string;
	username: string;
	userId: number;
	avatar: string;
	roles: string[];
}

export type TeacherMetaData = {
	educationBackGround: string;
	description: string;
};

export type ClassMemberType = "TEACHER" | "TEACHER_ASSISTANT" | "STUDENT";


export interface MemberRole {
	memberRole: ClassMemberType;
}

export interface AddMemberClassRequest {
	classId: number | undefined;
	memberId: number | undefined;
	memberRoles: MemberRole[];
}

export interface ClassMemberQuery {
	classId: number;
	memberId: number;
}

export type ClassListDto = PaginatedResponse<Class>;

export type ClassMemberListDto = PaginatedResponse<ClassMemberDto>;
