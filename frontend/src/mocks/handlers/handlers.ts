import { rest } from "msw";
import todoHandlers from "./todo/todoHandlers";

const exampleHandlers = [
    rest.post("/login_msw", (req, res, ctx) => {
        sessionStorage.setItem("is-authenticated", "true");
        return res(ctx.status(200));
    }),
    rest.get("/user_msw", (req, res, ctx) => {
        const isAuthenticated = sessionStorage.getItem("is-authenticated");

        if (!isAuthenticated) {
            return res(
                ctx.status(403),
                ctx.json({
                    errorMessage: "Not authorized",
                })
            );
        }

        return res(
            ctx.status(200),
            ctx.json({
                username: "admin",
            })
        );
    }),
];

const handlers = [...exampleHandlers, ...todoHandlers];

export default handlers; 
