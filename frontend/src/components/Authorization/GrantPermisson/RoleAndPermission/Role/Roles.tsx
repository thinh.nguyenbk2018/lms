import {Button, Col, Collapse, Divider, Form, Row, Typography} from "antd"
import {useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../../../../hooks/Redux/hooks";
import {RoleOfUser, useGetRolesOfUserQuery} from "../../../../../redux/features/Permission/PermissionAPI";
import {deleteRole, setRoles} from "../../../../../redux/features/Permission/PermissonSlice";
import CustomEmpty from "../../../../Util/CustomEmpty";
import AddRole from "./AddRole";
import "../RoleAndPermission.less";
import RoleDetails from "./RoleDetails";

const Roles = () => {
  let currUserId = useAppSelector(state => state.grantPermission.userId);
  let dispatch = useAppDispatch();

  let isEditting = useAppSelector(state => state.grantPermission.isEditing);

  let roles = useAppSelector(state => state.grantPermission.roles);

  let { data: getRolesOfUserResponse } = useGetRolesOfUserQuery(currUserId, { skip: currUserId === 0 });

  let [form] = Form.useForm();

  const generateValidFromFieldName = (roleId: number) => `role_${roleId}.validFrom`;
  const generateExpiresAtFieldName = (roleId: number) => `role_${roleId}.expiresAt`;

  useEffect(() => {
    dispatch(setRoles(getRolesOfUserResponse?.roles || []));
  }, [dispatch, getRolesOfUserResponse])

  const renderRoleHeader = (role: RoleOfUser, index: number) => (
    <Row style={{ width: '100%' }} align="middle">
      <Col lg={17} md={18}>
        {role.title}
      </Col>
      <Col lg={7} md={6}>
        <Row justify="end">
          {isEditting && <Button danger onClick={() => dispatch(deleteRole(index))}>Xóa</Button>}
        </Row>
      </Col>
    </Row >
  )

  return <>
    <Divider orientation="left" orientationMargin={5}><Typography.Title level={5}>Các vai trò nhân viên đang có</Typography.Title></Divider>
    <Row justify="center">
      <Form form={form} style={{ width: '90%' }}>
        <Collapse >
          {roles.map((role, index) => (
            <Collapse.Panel
              key={index}
              header={renderRoleHeader(role, index)}
            >
              <RoleDetails roleId={role.id} />
            </Collapse.Panel>
          ))}
          {roles.length === 0 && <CustomEmpty description={"Không có dữ liệu"} />}
        </Collapse>
      </Form>
    </Row>
    {currUserId !== 0 && isEditting && <AddRole />}
  </>
}

export default Roles;