import {
    createSliceEnhancedWithSearch,
    generateInitialSearchSlice,
    SearchCriteriaType
} from "../../utils/createSliceWithSearch/createSliceEnhancedWithSearch";
import {RootState} from "@redux_store";

const QUIZ_RESULT_MANAGEMENT = "quizResultManagement";

const initialState: SearchCriteriaType = {
    ...generateInitialSearchSlice()
}

export const quizResultManagementSlice = createSliceEnhancedWithSearch({
    name: QUIZ_RESULT_MANAGEMENT,
    initialState,
    reducers: {

    },
});

export const quizResultManagementSelector = {
    selectSearchCriteria: (state: RootState) => state.quizResultManagement.searchCriteria
}

export const {changeCriteria} = quizResultManagementSlice.actions;