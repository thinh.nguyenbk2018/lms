"use strict";
exports.__esModule = true;
var antd_1 = require("antd");
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var EditButton_1 = require("../../../../components/ActionButton/EditButton");
var ContentHeader_1 = require("../../../../components/ContentHeader/ContentHeader");
var TextEditor_1 = require("../../../../components/TextEditor/TextEditor");
var hooks_1 = require("../../../../hooks/Redux/hooks");
var useCourseSidebar_1 = require("../../../../hooks/Routing/useCourseSidebar");
var ROUTING_CONSTANTS_1 = require("../../../../navigation/ROUTING_CONSTANTS");
var CourseAPI_1 = require("../../../../redux/features/Course/CourseAPI");
var CourseContentSlice_1 = require("../../../../redux/features/Course/CourseContentSlice");
var SectionHeader_1 = require("../../../../SectionHeader/SectionHeader");
var LoadingPage_1 = require("../../../UtilPages/LoadingPage");
var CourseContent_1 = require("../CourseContentPage/CourseContentPage");
require("./CourseUnitItemDetail.less");
var CourseUnitItemDetailPage = function () {
    var _a = useCourseSidebar_1["default"](), courseDetail = _a.courseDetail, isCourseDetailFetching = _a.isCourseDetailFetching, isCourseDetailSuccess = _a.isCourseDetailSuccess;
    var _b = react_router_dom_1.useParams(), unitItemId = _b.unitItemId, chapterId = _b.chapterId, courseId = _b.courseId;
    var _c = CourseAPI_1.useGetUnitDetailQuery({
        courseId: Number(courseId),
        chapterId: Number(chapterId),
        unitId: Number(unitItemId)
    }, {
        skip: isNaN(Number(unitItemId)) ||
            isNaN(Number(chapterId)) ||
            isNaN(Number(courseId))
    }), unitData = _c.data, isSuccess = _c.isSuccess, isFetching = _c.isFetching;
    //  * Redux store
    var dispatch = hooks_1.useAppDispatch();
    // * Router
    var navigate = react_router_dom_1.useNavigate();
    if (isCourseDetailSuccess && isSuccess) {
        return (react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement(ContentHeader_1["default"], { title: (courseDetail === null || courseDetail === void 0 ? void 0 : courseDetail.name) + " - Chi ti\u1EBFt b\u00E0i h\u1ECDc: " + unitData.title, action: react_1["default"].createElement(antd_1.Space, null,
                    react_1["default"].createElement(EditButton_1["default"], { onClick: function () {
                            dispatch(CourseContentSlice_1.courseContentActions.updatingActivity({
                                chapterToEdit: Number(chapterId),
                                activity: unitData
                            }));
                        } })) }),
            react_1["default"].createElement(SectionHeader_1["default"], { title: "Thông tin bài học" }),
            react_1["default"].createElement(antd_1.Row, { justify: "center" },
                react_1["default"].createElement("div", { className: "unit-item-info-card" },
                    react_1["default"].createElement("div", { className: "title" },
                        react_1["default"].createElement("div", { className: "field-value" }, unitData.title)),
                    react_1["default"].createElement("div", { className: "field-container" },
                        react_1["default"].createElement("div", { className: "field-name" }, "T\u00E0i li\u1EC7u tham kh\u1EA3o "),
                        unitData.textbooks.map(function (item) {
                            return (react_1["default"].createElement(react_1["default"].Fragment, null,
                                react_1["default"].createElement("div", { className: "textbook-item-view-container" },
                                    react_1["default"].createElement(antd_1.Tooltip, { title: "" + item.note },
                                        react_1["default"].createElement("div", { className: "textbook-name", onClick: function () {
                                                navigate(ROUTING_CONSTANTS_1.ROUTING_CONSTANTS.TEXT_BOOK_MANAGEMENT);
                                            } }, item.name)))));
                        })))),
            react_1["default"].createElement(SectionHeader_1["default"], { title: "Nội dung bài học" }),
            react_1["default"].createElement("div", { className: "unit-content-container" },
                react_1["default"].createElement(TextEditor_1["default"], { value: unitData.content, useBubbleTheme: true, readOnly: true, onChange: function () { }, additionalStyles: {
                        overflowY: "hidden"
                    } })),
            react_1["default"].createElement(CourseContent_1.AddUnitItemModal, null)));
    }
    else if (isCourseDetailFetching || isFetching) {
        return react_1["default"].createElement(LoadingPage_1["default"], null);
    }
    else {
        return react_1["default"].createElement(react_1["default"].Fragment, null);
    }
};
exports["default"] = CourseUnitItemDetailPage;
