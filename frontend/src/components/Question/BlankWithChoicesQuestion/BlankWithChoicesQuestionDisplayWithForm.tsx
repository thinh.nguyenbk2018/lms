import React from "react";
import { QuestionViewAndTakingTestFormProps } from "../interface/question.interface";
import { BlankWithChoicesQuestion, BlankWithOption, Choice, } from "../../../redux/features/Quiz/CreateQuizSlice";
import { FormikProps, useFormikContext, withFormik } from "formik";
import { DEFAULT_TEXT_EDITOR_HTML } from "../DragAndDropQuestion/DragAndDropQuestionDrawer";
import * as Yup from "yup";
import { Form, Select } from "formik-antd";
import { DEFAULT_FRONTEND_ID } from "../../../redux/interfaces/types";
import { baseCreateQuestionYupSchema } from "../QuestionContainerItem/baseQuestionSchema";
import { QuestionAnswer } from "../../../redux/features/Quiz/examAPI";
import { DND_BLANK_SPAN_TAG_CLASS, } from "../DragAndDropQuestion/QuestionStatementWithBlanks";
import ReactHtmlParser from "html-react-parser";
import {
	getContentInsideSpecialSpanNode,
	replaceBlankWithSpecialSpanButKeepContent,
} from "./BlankWithChoicesQuestionDisplay";
import { Card, Divider, Row } from "antd";
import QuestionSubmissionButton from "../../ActionButton/QuestionSubmissionButton";
import AttachmentRender from "../Attachment/AttachmentRender";
import { Attachment } from "../Attachment/AttachmentQuestion";

const BlankWithChoices = (props: BlankWithOption & { index: number }) => {
	const {
		values,
		dirty,
		touched,
		setFieldTouched,
		setFieldValue,
		setFieldError,
		handleSubmit
	} = useFormikContext<QuestionAnswer>();
	const { index, hint = "", answerList, correctAnswerKey } = props;
	return (
		<>
			<Select
				placeholder={"Chọn đáp án"}
				name={`answers[${index}]`}
				onChange={(value, option) => {
					// console.log("CHANGED VALUE AND QUESTION",value,option);
					setFieldValue(`answers[${index}]`, value);
					handleSubmit();
				}}
				className={'fill-in-blank-with-choices-input'}
			>
				{answerList.map((item, index) => (
					<Select.Option key={item.answerKey} value={item.answerKey}>
						{item.content}
					</Select.Option>
				))}
			</Select>
		</>
	);
};

const renderChoiceAnswerBox = (
	questionHTML: string,
	callbakOnAnswerList?: Function
) => {
	const [questionReviewHTML, count] =
		replaceBlankWithSpecialSpanButKeepContent(questionHTML);
	let blankCounter = 0;

	const parseAnswersInBlank = (question: string) => {
		return question.substring(1, question.length - 1).split(";");
	};
	return ReactHtmlParser(questionReviewHTML, {
		replace: (node) => {
			if (
				(node as any).name === "span" &&
				(node as any).attribs.class === DND_BLANK_SPAN_TAG_CLASS
			) {
				blankCounter++;
				const blankIndex = blankCounter - 1;
				const answerTexts = parseAnswersInBlank(
					getContentInsideSpecialSpanNode(node)
				);
				const answerList: Choice[] = answerTexts.map((item, index) => {
					return {
						content: item,
						answerKey: index,
						isCorrect: false,
						isChosenByStudent: false
					};
				});
				return (
					<BlankWithChoices
						hint={""}
						answerList={answerList}
						correctAnswerKey={null}
						index={blankIndex}
					/>
				);
			}
			return node;
		},
	});
};
/** Display the test for review the question content and possibly retrieve the answer */
const WrappedFillInBlankWithChoicesDisplay = (
	props: BlankWithChoicesHocProps & FormikProps<QuestionAnswer>
) => {
	// * Question info to display the requirements
	const { description, blankList, attachment } = props;

	// * Form State from formik
	const {
		values,
		errors,
		dirty,
		touched,
		isValid,
		handleSubmit,
		setFieldValue,
	} = props;

	return (
		<>
			<Form>
				<Card>
					{
						JSON.parse(attachment || '[]').map((a: Attachment, index: number) => <Row justify="center" key={index}>
							<AttachmentRender attachment={a} />
						</Row>)
					}
					<Divider />
					{renderChoiceAnswerBox(description)}
					{/*<Row style={{ margin: "1em 0" }} justify="end">*/}
					{/*	<QuestionSubmissionButton*/}
					{/*		onClick={() => {*/}
					{/*			handleSubmit()*/}
					{/*		}}*/}
					{/*		title={"Gửi đáp án"}*/}
					{/*	/>*/}
					{/*</Row>*/}
				</Card>
			</Form>
		</>
	);
};

// * Question configuration props with Formik HOC
const fillInBlankAnswerSchema = Yup.object({
	answers: Yup.array().of(
		Yup.string().required("Câu trả lời cho một ô trống không được để trống")
	),
}).concat(baseCreateQuestionYupSchema);

type BlankWithChoicesHocProps =
	QuestionViewAndTakingTestFormProps<BlankWithChoicesQuestion>;
/** A question displayer need the Partial<QuestionDataType> to display the question statement
 * ? QuestionAnswer to record student choice quen taking the test
 *
 * */
const BlankWithChoicesQuestionDisplayWithForm = withFormik<
	BlankWithChoicesHocProps,
	QuestionAnswer
>({
	mapPropsToValues: (props) => {
		return {
			id: props.id || DEFAULT_FRONTEND_ID,
			description: props.description || DEFAULT_TEXT_EDITOR_HTML,
			point: props.point || 0,
			blankList: props.blankList || [],
			answers: props.blankList?.map(b => b?.studentAnswer == undefined ? "" : b?.studentAnswer) || [],
			optionalStudentNote: "",
		};
	},
	// validationSchema: fillInBlankAnswerSchema,
	handleSubmit: (values, formikBag) => {
		const valuesToSubmit: QuestionAnswer = {
			...values,
		};
		// console.log('values to submit blank with choices: ', valuesToSubmit);
		if (values.answers.filter(x => x !== "").length != 0) {
			formikBag.props.onConfirmSubmit?.(valuesToSubmit);
		}
	},
})(WrappedFillInBlankWithChoicesDisplay);
export default BlankWithChoicesQuestionDisplayWithForm;
